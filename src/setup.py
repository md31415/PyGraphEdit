#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
This setup-file tests if all required modules avalible and pre-compiles
some modules.

This script works only with linux. In Windows the dialog-modules must
compiled by starting the 'compile_gui.bat'

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 14.10.2013
Most recent amendment: 21.11.2015

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import configparser
import imp
import argparse
from os import chdir
from platform import system
import subprocess
import sys


# ====================================================================
def test():
  '''Start different tests'''
  # Test if this file is started from an other os as linux
  if system() != "Linux":
    print("This setup-file is only for Linux-Systems")
    print("")
    sys.exit()

  # Test the python version
  print("Test python-version....", end='')
  if sys.version_info < (3, 0, 0):
    print("This program is only for python3")
    sys.exit()
  if sys.version_info < (3, 3, 0):
    print("Please use python 3.3 or newer for all functions")
  print(" ok")

  # Searching for required modules
  required_modules = ['PyQt5', 'sympy', 'pydot']
  for module in required_modules:
    print("Searching %s..." % module, end='')
    try:
      imp.find_module(module)
      print(" Found")
    except ImportError:
      print(" Not found")
      print("Please install '%s' to use PyGraphEdit" % module)
      sys.exit()

  # Searching for cython (optional)
  cython = False
  module = "Cython"
  print("Searching %s..." % module, end='')
  try:
    imp.find_module(module)
    cython = True
    print(" Found")
  except ImportError:
    print(" Not found")
    cython = False

  # Searching for numpy (optional)
  numpy = False
  module = "numpy"
  print("Searching %s..." % module, end='')
  try:
    imp.find_module(module)
    numpy = True
    print(" Found")
  except ImportError:
    print(" Not found")
    print("Plotting not available")
    numpy = False

  # Searching for matplotlib (optional)
  matplotlib = False
  module = "matplotlib"
  print("Searching %s..." % module, end='')
  try:
    imp.find_module(module)
    matplotlib = True
    print(" Found")
  except ImportError:
    print(" Not found")
    print("Plotting not available")
    matplotlib = False

  # Searching for swig (optional)
  print("Searching %s..." % "swig", end='')
  swigargs = ['swig -help']
  sProcess = subprocess.Popen(swigargs, shell=True,
                              stderr=subprocess.PIPE,
                              stdout=subprocess.PIPE)
  return_code = sProcess.wait()
  if return_code == 0:
    print(" Found")
    SWIG = True
  else:
    print(" Not found")
    print("To use C-Libraries install swig")
    SWIG = False

  # Precompiling some modules with cython
  chdir('lib/')
  if cython and args.use_cython:
    print("Precompiling some modules...")
    cythonargs = ['make']
    sProcess = subprocess.Popen(cythonargs, shell=True,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
    return_code = sProcess.wait()
    cythonargs = ['make', 'clean']
    sProcess = subprocess.Popen(cythonargs, shell=True,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
    return_code = sProcess.wait()
    precomp_args = ['ls', '*.so']
    sProcess = subprocess.Popen(precomp_args, shell=True,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
    return_code = sProcess.wait()
    compiled_modules = []
    for line in sProcess.stdout:
      fileName = str(line.strip()).split('.')
      if fileName[len(fileName) - 1] == "so'":
        compiled_modules.append(fileName[0][2:])
    print("Compiled: %s" % compiled_modules)

  # compile c-modules with swig
  if SWIG:
    print("Compiling c-modules...", end='')
    chdir('ctools/')
    swigargs = ['./make']
    sProcess = subprocess.Popen(swigargs, shell=True,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
    return_code = sProcess.wait()
    print(" Done")

  # Compile dialogs
  print("Compiling dialogs...", end='')
  chdir('../../dialogs/')
  genargs = ['make']
  sProcess = subprocess.Popen(genargs, shell=True,
                              stderr=subprocess.PIPE,
                              stdout=subprocess.PIPE)
  return_code = sProcess.wait()
  print(" Done")

  # Write the configurationfile
  print("Writing the configurationfile...", end='')
  config = configparser.ConfigParser()
  settingsfile = 'settings.ini'
  config.read(settingsfile)
  if numpy and matplotlib:
    config["DEFAULT"]['use_matplotlib'] = "yes"
  else:
    config["DEFAULT"]['use_matplotlib'] = "no"
  with open(settingsfile, 'w') as configfile:
    config.write(configfile)
  print(" Done")


# ====================================================================
# ====================================================================
if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Process some integers.')
  parser.add_argument('--without_cython', action='store_false',
                      dest="use_cython", default=True,
                      help='Do not cython')
  args = parser.parse_args()

  # Print program info
  print("PyGraphEdit 0.63")
  print("Copyright 2013-2017 Markus Dod")
  print("")
  print("Setup for linux-systems")
  print("")
  print("")

  test()

  # Finishing
  print("Everything done.")
  print("")
  print("Start PyGraphEdit with: python3 PyGraphEdit.py")
  print("")
  print("You can edit the settings in 'settings.ini'")
  print("For more instructions and information read README.txt and LICENSE.txt")
  print("")
