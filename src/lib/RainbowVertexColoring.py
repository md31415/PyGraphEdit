'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 13.03.2015

@author: Markus Dod
'''


# =============================================================================
# ======== TRY TO FIND A MINIMUM RAINBOWCOLORING WITH SCIPOPT =================
# =============================================================================
def computeRainbowVertexSCIP(graph):
  '''
  Calculate the rainbow vertex coloring number of the graph
  @param graph: SGraph
  @return: Dict with the coloring of the edges 
  '''
  PyGraphEdit_log.info("starting scipopt to rainbow color the graph ...")

  try:
    import modules.scipopt.scip as scip
    from itertools import product
  except:
    PyGraphEdit_log.info("scipoptsuite not avalible")
    return None

  # create solver instance
  s = scip.Solver()
  s.create()
  s.includeDefaultPlugins()
  s.createProbBasic(b"Rainbow Coloring")

  # add some variables
  probVarsy = {}
  varNamesy = {}
  varBaseName = "y"
  for i in graph.getV():
    p_str = varBaseName + "_" + str(i)
    p_str = p_str.encode('ascii')
    varNamesy[i] = p_str
    probVarsy[i] = s.addIntVar(p_str, obj=1, lb=0.0)

  # Indexes for creating variables and constraints
  n = graph.order()
  rows = graph.getEdgeSet()

  # add some variables
  probVars = {}
  varNames = {}
  varBaseName = "x"
  for p in range(1, n):
    for q in range(p + 1, n + 1):
      for e in rows:
        i = e[0]
        j = e[1]
        if (i,j) not in probVars:
          probVars[(i,j)] = {}
          varNames[(i,j)] = {}
          probVars[(j,i)] = {}
          varNames[(j,i)] = {}
        p_str = varBaseName + "_" + str(i) + ":" + str(j) + "_" + str(p) + ":" + str(q)
        p_str = p_str.encode('ascii')
        varNames[(i,j)][(p,q)] = p_str
        probVars[(i,j)][(p,q)] = s.addIntVar(p_str, obj=1, lb=0.0, ub=1.0)

        p_str = varBaseName + "_" + str(j) + ":" + str(i) + "_" + str(p) + ":" + str(q)
        p_str = p_str.encode('ascii')
        varNames[(j,i)][(p,q)] = p_str
        probVars[(j,i)][(p,q)] = s.addIntVar(p_str, obj=1, lb=0.0, ub=1.0)

  # NB 1
  for p in range(1, n):
    for q in range(p + 1, n + 1):
      coeffs = {probVars[(p,i)][(p,q)]: 1.0 for i in range(1, n + 1) if graph.is_adjacent(p, i)}
      s.addCons(coeffs, lhs=1.0, rhs=1.0)

  # NB 2
  for p in range(1, n):
    for q in range(p + 1, n + 1):
      coeffs = {probVars[(i,q)][(p,q)]: 1.0 for i in range(1, n + 1) if graph.is_adjacent(i, q)}
      s.addCons(coeffs, lhs=1.0, rhs=1.0)

  # NB 3
  for k in range(1, n + 1):
    for p in range(1, n):
      for q in range(p + 1, n + 1):
        if k != p and k != q:
          coeffs1 = {probVars[(i,k)][(p,q)]: 1.0 for i in range(1, n + 1) if graph.is_adjacent(i, k)}
          coeffs2 = {probVars[(k,i)][(p,q)]: -1.0 for i in range(1, n + 1) if graph.is_adjacent(k, i)}
          coeffs1.update(coeffs2)
          s.addCons(coeffs1, lhs=0.0, rhs=0.0)

#   # NB 4
#   for p in range(1, n):
#     for q in range(p + 1, n + 1):
#       

  # solve problem
  s.solve()
  status = s.getStatus()

  if status == scip.scip_status.optimal:
    # retrieving the best solution
    solution = s.getBestSol()

    # print solution
    sol = dict((i, 0) for i in range(1, n + 1))
    for i in range(1, n + 1):
      solValue = round(s.getVal(solution, probVarsy[i]))
      sol[i] = solValue

      s.releaseVar(probVarsy[i])

    s.free()
    return sol

  else:
    return None


# =============================================================================
# =============================================================================
# =============================================================================
def compute_coloring(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate a rainbow coloring of the graph
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue to save the result
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing a rainbow coloring ...")
  PyGraphEdit_log.info("")

  colors = None
  num_colors = 0

  if m == None or m:
    PyGraphEdit_log.info("starting complete enumeration ...")
    colors = computeRainbowVertexSCIP(graph)
    print(colors)

  if q != None:
    q.put((colors, num_colors))

  return colors, num_colors


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  from sys import exit as sys_exit
  sys_exit("Use PyGraphEdit")
