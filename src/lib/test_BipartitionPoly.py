#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Tests for the calculation of the bipartition polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''
from sympy import symbols

from SGraph.SGraph import SGraph
from SGraph.SpecialGraphs import (cycle, path, diamant,  # @UnresolvedImport
                                  completeBipartite, completeGraph)  # @UnresolvedImport

from lib.BipartitionPoly import (compute_polynomial, calculateTree, calculate_s2Path,
                                 calculate_2Path, calculate_s2Path_wPartitions,
                                 calculate_2Path_wPartitions, calcCompleteGraph,
                                 calcCompleteBipartiteGraph, calcPath, calcCycle)

x, y, z = symbols('x y z')

def test_compute_polynomial():
  assert compute_polynomial(diamant()) == x**4 + 2*x**3*y*z**3 + 8*x**3*y*z**2 + 10*x**3*y*z + 4*x**3 + 2*x**2*y**2*z**4 + 12*x**2*y**2*z**3 + 16*x**2*y**2*z**2 + 8*x**2*y*z**2 + 20*x**2*y*z + 6*x**2 + 2*x*y**3*z**3 + 8*x*y**2*z**2 + 10*x*y*z + 4*x + 1

def test_calculateTree():
  pass

def test_calculate_s2Path():
  pass

def test_calculate_2Path():
  pass

def test_calculate_s2Path_wPartitions():
  pass

def test_calculate_2Path_wPartitions():
  pass

def test_calcCompleteGraph():
  pass

def test_calcCompleteBipartiteGraph():
  pass

def test_calcPath():
  pass

def test_calcCycle():
  pass
