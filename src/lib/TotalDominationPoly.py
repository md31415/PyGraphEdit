#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculats the total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
'''

from copy import deepcopy
from sympy import symbols, expand, degree, simplify
import sys

from modules.consts import method, graphType  # @UnresolvedImport

from lib.tools import powerset, binomial, argmax  # @UnresolvedImport

from SGraph.GraphFunctions import (isComplete, isPath, isCompleteBipartite,  # @UnresolvedImport
                                  getBipartition)  # @UnresolvedImport
from SGraph.GraphFunctions import inducedSubgraph, components  # @UnresolvedImport


# =============================================================================
# =============================================================================
x = symbols('x')
p = symbols('p')


# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the total domination polynomial using complete enumeration
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    if is_total_dom(graph, W):
      P = x ** len(W) + P

  return P

def compute_polynomial2(graph):
  '''
  Calculation of the total domination polynomial using complete enumeration
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    C = components(inducedSubgraph(graph, set(W)))
    I = 1
    for g in C:
      if g.order() != 1:
        I = I * (1 + (-x) ** (g.order()))
    P = P + (-1) ** (len(W)) * I

  return P

def compute_polynomial3(graph):
  '''
  Calculation of the total domination polynomial using complete enumeration
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    G = inducedSubgraph(graph, set(W))
    C = components(G)
    I = 1
    for g in C:
      if g.order() != 1:
        I = I * ((-1) ** (g.order()) + x ** (g.order()))
    P = P + (-1) ** (G.numIso()) * I

  return P

def compute_polynomial4(graph):
  '''
  C
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    G = inducedSubgraph(graph, set(W))
    P = P + (-1) ** len(W) * compute_polynomial(G)

  return P

def edgesum(graph, X=set([1, 2, 3]), Y=set([4, 5, 6])):
  edgesets = powerset(list(graph.getEdgeSet()))
  P = 0
  for F in edgesets:
    #print(".........")
    #print(F)
    g = graph.spanningSubgraph(F)
    # print(g)
    R = compute_polynomial(g)
    #print(R)
    P += (-1) ** len(F) * R

  return P

def is_total_dom(graph, W):
  '''
  Decides if the set W is a total dominating set
  '''
  W = set(W)
  for v in graph.getV():
    if len(W.intersection(graph.neighbors(v))) == 0:
      return False
  return True

def compute_inclusion_exclusion(graph):
  '''
  Calculation of the total domination polynomial using the inclusion-exclusion principle
  '''
  P = 0
  pset = powerset(graph.getV())
  n = graph.order()
  for k in range(0, n + 1):
    for W in pset:
      NW = len(graph.open_total_neighbors_set(W))
      if NW <= n - k:
        P = x ** k * (-1) ** len(W) * binomial(n - NW, k) + P

  return P

def essential_calculation(graph):
  '''
  Calculation of the total domination polynomial using total essential sets
  @param graph: Graph
  @return: The total domination polynomial of the graph
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    k = is_essential(graph, W)
    if k > 0:
      P = ((x + 1) ** k - 1) * (-1) ** len(W) + P

  P = P * (-1) ** len(graph.getV())
  return P

def essential_calculation_fast(graph):
  '''
  Calculation of the total domination polynomial using total essential sets
  @param graph: Graph
  @return: The total domination polynomial of the graph
  '''
  P = 0
  S = set([])
  for v in graph.getV():
    S.add(frozenset(graph.neighbors(v)))

  for i in range(0, graph.order()):
    T = S.copy()
    for t in T:
      for v in graph.getV():
        if v not in t:
          f = set(t)
          f.add(v)
          S.add(frozenset(f))

  MDNetwork_log.info("number of essential sets: %d" % len(S))

  d = {}
  n = graph.order()
  for i in range(1, n + 1):
    d[i] = 0

  for W in S:
    k = is_essential(graph, W)
    d[len(W)] += 1
    P = ((x + 1) ** k - 1) * (-1) ** len(W) + P

  # print(d)
  P = P * (-1) ** len(graph.getV())
  return P

def numberofessentials(graph):
  d = {}
  ess = []
  n = graph.order()
  for i in range(1, n + 1):
    d[i] = 0
  for v in graph.getV():
    flag = True
    for u in graph.getV():
      U = set(graph.neighbors(u))
      V = set(graph.neighbors(v))
      if v != u and U < V or (U == V and u in ess):
        flag = False
        break
      else:
        pass
    if flag:
      ess.append(v)
      d[graph.degree(v)] += 1

#   E = 1
#   for i in range(graph.get_min_degree(), n):
#     print("....")
#     print(i)
#     D = 0
#     for j in range(1,i):
#       D += d[j+1]*factorial(n-j-1)
#     D = D / factorial(n-i)
#     print(D)
#     print(binomial(n,i))
#     E += min(binomial(n,i), int(D))
#
#   MDNetwork_log.info("Erwartete Anzahl: %d" % E)


def essential_calculation_fast2(graph):
  '''
  Calculation of the total domination polynomial using total essential sets
  @param graph: Graph
  @return: The total domination polynomial of the graph
  '''
  P = 0
  S1 = set([])
  T = set([])
  for v in graph.getV():
    T.add(frozenset(graph.neighbors(v)))
    S1.add(frozenset(graph.neighbors(v)))

  for _ in range(0, graph.order()):
    S2 = set([])
    for t in T:
      for v in graph.getV():
        if v not in t:
          f = set(t)
          f.add(v)
          S2.add(frozenset(f))
    S1 = S1.union(S2)
    T = S2.copy()

  for W in S1:
    k = is_essential(graph, W)
    P = ((x + 1) ** k - 1) * (-1) ** len(W) + P

  P = P * (-1) ** len(graph.getV())
  return P

def is_essential(graph, W):
  '''
  Test if the set W is a total essential set in the graph
  @param graph: Graph
  @param W: Set to test
  @return: Returns the number of covered vertices
  '''
  k = 0
  for u in graph.getV():
    if set(graph.neighbors(u)) <= set(W):
      k += 1
  return k

def recSolve(graph, X=None, Y=None):
  '''
  TD(G,X,Y;x)
  X are vertices which can be dominating
  Y are the vertices which should be dominated
  '''
  if X == None or Y == None:
    X = set(graph.getV())
    Y = set(graph.getV())

  if len(X) == 0 and len(Y) == 0:
    return 1
  elif len(Y) == 0:
    return (1 + x) ** len(X)
  elif len(X) == 0 and len(Y) != 0:
    return 0
  elif len(X) <= 3:
    return compute_polynomialXY(graph, X, Y)

  NX = graph.open_total_neighbors_set(X)
  if not Y <= NX:
    return 0

  degrees = graph.get_degrees()
  for w in set(graph.getV()) - X:
    del degrees[w]
  v = argmax(degrees)
  return recSolve(graph, X - set([v]), deepcopy(Y)) + x * recSolve(graph, X - set([v]), Y - set(graph.neighbors(v)))

def compute_polynomialXY(graph, X, Y):
  '''
  TD(G,X,Y;x)
  X are vertices which can be dominating
  Y are the vertices which should be dominated
  '''
  P = 0
  pset = powerset(X)
  for W in pset:
    # if 5 not in W:
      if is_total_domXY(graph, W, Y):
        P = x ** len(W) + P

  return P

def is_total_domXY(graph, W, Y):
  W = set(W)
  for v in set(Y):
    if v in graph.getV():
      if len(W.intersection(graph.neighbors(v))) == 0:
        return False
  return True


# ==================================================================
# ========== SPECIAL GRAPH CLASSES =================================
# ==================================================================
def calcCompleteGraph(n):
  '''Calculates the total domination polynomial of the complete graph'''
  return (x + 1) ** n - x * n - 1

def calcCompleteBipartiteGraph(graph):
  '''Calculates the total domination polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  return (x + 1) ** (n + m) - (x + 1) ** n - (x + 1) ** m + 1

def calcPath(n):
  '''Calculates the total domination polynomial of the path'''
  from collections import deque
  poly = deque([0, x ** 2, x ** 3 + x ** 2 * 2, x ** 4 + x ** 3 * 2 + x ** 2])
  if n <= 4:
    return poly[n - 1]

  i = 4
  while i != n:
    i += 1
    poly.append(x ** 2 * poly[0] + x ** 2 * poly[1] + x * poly[3])
    poly.popleft()

  return poly[3]


# =============================================================================
# =============================================================================
# =============================================================================
def reltodom(DRel):
  '''Calculate the domination polynomial from the DRel'''
  n = degree(DRel, gen=p)
  return (1 + x) ** n * DRel.subs(p, x / (x + 1))

def domtorel(D):
  '''Calculate the DRel from the domination polynomial'''
  n = degree(D, gen=x)
  return (1 - p) ** n * D.subs(x, p / (1 - p))


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, q=None):
  '''
  Calculate the total domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue, only used if called from PyGraphEdit
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  TD = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      TD = calcCompleteGraph(graph.order())
    elif isPath(graph):
      TD = calcPath(graph.order())
    elif isCompleteBipartite:
      TD = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      TD = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      TD = calcPath(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      TD = calcCompleteBipartiteGraph(graph)

  if TD:
    if q != None:
      q.put(TD)
    return expand(TD)

  if m == method.STATE_SPACE or m == None:
    MDNetwork_log.info("starting complete enumeration...")
    TD = compute_polynomial(graph)
  elif m == method.INCLEXCL:
    MDNetwork_log.info("starting calculation with inclusion-exclusion ...")
    TD = compute_inclusion_exclusion(graph)
  elif m == method.ESSENTIAL:
    MDNetwork_log.info("starting calculation with essential subsets ...")
    TD = essential_calculation_fast(graph)
  elif m == method.DECOMP:
    MDNetwork_log.info("starting calculation with decomposition ...")
    TD = recSolve(graph)

  if q != None:
    q.put(TD)
  return expand(TD)

def compute_reliability(MDNetwork_log2, graph, m=None, q=None):
  '''
  Calculate the total domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue, only used if called from PyGraphEdit
  '''
  rel = expand(simplify(domtorel(compute_poly(MDNetwork_log2, graph, m, q))))
  if q != None and not q.empty():
    q.get()
    q.put(rel)

  return rel


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
