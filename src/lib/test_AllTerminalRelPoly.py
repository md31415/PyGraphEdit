#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Tests for the calculation of the all terminal reliability polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''
from sympy import symbols

from SGraph.SGraph import SGraph
from SGraph.SpecialGraphs import (cycle, path, diamant,wheel)  # @UnresolvedImport

from lib.AllTerminalRelPoly import (compute_polynomial, calcPath, calcCycle, calcWheel)

p = symbols('p')

def test_compute_polynomial():
  assert compute_polynomial(cycle(4)) == calcCycle(4)
  assert compute_polynomial(cycle(5)) == calcCycle(5)
  assert compute_polynomial(cycle(6)) == calcCycle(6)
  assert compute_polynomial(cycle(7)) == calcCycle(7)
  assert compute_polynomial(cycle(8)) == calcCycle(8)

  assert compute_polynomial(path(4)) == calcPath(4)
  assert compute_polynomial(path(5)) == calcPath(5)
  assert compute_polynomial(path(6)) == calcPath(6)
  assert compute_polynomial(path(7)) == calcPath(7)
  assert compute_polynomial(path(8)) == calcPath(8)
  assert compute_polynomial(path(9)) == calcPath(9)

  assert compute_polynomial(wheel(4)) == calcWheel(4)
  assert compute_polynomial(wheel(5)) == calcWheel(5)
  assert compute_polynomial(wheel(6)) == calcWheel(6)
  assert compute_polynomial(wheel(7)) == calcWheel(7)
  assert compute_polynomial(wheel(8)) == calcWheel(8)
  assert compute_polynomial(wheel(9)) == calcWheel(9)
