#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculate the general domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 15.01.2015

@author: Markus Dod
'''

from sympy import symbols, expand

x, y, z, w = symbols('x y z w')


def compute_polynomial(graph):
  pass

#calcCompleteGraph(graph):
#calcCompleteBipartiteGraph(graph):

