#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Some methods to calculate the matching polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 03.05.2013

@author: Markus Dod
'''

import sympy
import sys

from lib.tools import powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isCycle, isPath)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x, y = sympy.symbols('x y')


# =============================================================================
def compute_polynomial(graph):
  '''
  Compute the matching polynomial
  @param graph: SGraph
  '''
  P = 1
  n = graph.order() / 2
  for F in powerset(list(graph.getEdgeSet())):
    if len(F) != 0 and len(F) <= n and len(endVertices(F)) == 2 * len(F):
      P = x ** len(F) + P

  return P

def endVertices(F):
  V = set([])
  for e in F:
    V.add(e[0])
    V.add(e[1])
  return V

def compute_polynomial_extended(graph):
  '''
  Compute the extended matching polynomial
  @param graph: SGraph
  '''
  P = 1
  for F in powerset(list(graph.getEdgeSet())):
    if len(F) != 0 and len(F) <= graph.order() / 2 and len(endVertices(F)) == 2 * len(F):
      W = endVertices(F)
      P = x ** len(F) * y ** (len(graph.Delta(W)) - len(F) + len(graph.delta(W))) + P

  return P


# =============================================================================
# ============== SPECIAL GRAPHS ===============================================
# =============================================================================
def calcStar(n):
  '''
  Calculate the matching polynomial of the star
  @param n: Order of the star
  '''
  return (n - 1) * x

def calcPath(n):
  '''
  Calculate the matching polynomial of the path
  p(n) = x p(n-2) + p(n-1)
  @param n: Length of the path
  '''
  from collections import deque
  poly = deque([1, x + 1])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n - 1]

  i = 2
  while i != n:
    i += 1
    poly.append(x * poly[0] + poly[1])
    poly.popleft()

  return poly[1]

def calcCycle(n):
  '''
  Calculate the matching polynomial of the cycle
  c(n) = x c(n-1) - c(n-2)
  @param n: Length of the cycle
  '''
  if n == 1:
    return 0
  elif n == 2:
    return x + 1
  elif n == 3:
    return 3 * x + 1

  return x * calcPath(n - 3) + calcPath(n - 1)


# =============================================================================
def computeMatching(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Compute the matching polynomial
  @param graph: Graph
  @param m: Method
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  M = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isPath(graph):
      PyGraphEdit_log.info("identified a path...")
      M = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("identified a cycle...")
      M = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("identified complete star ...")
      M = calcStar(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("identified a path ...")
      M = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("identified a cycle  ...")
      M = calcCycle(graph.order())

  if M:
    if q != None:
      q.put(M)
    return M

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration...")
    M = compute_polynomial(graph)

  if q != None:
    q.put(M)
  return M

def computeExtendedMatching(MDNetwork_log2, graph, m=None, q=None):
  '''
  Compute extended matching polynomial
  @param graph: Graph
  @param m: Method
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  M = None
  if m == method.STATE_SPACE or m == None:
    MDNetwork_log.info("starting complete enumeration...")
    M = compute_polynomial_extended(graph)

  if q != None:
    q.put(M)
  return M


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")

