# Find maximum cardinality matching in general undirected graph
# D. Eppstein, UC Irvine, 6 Sep 2003
# http://code.activestate.com/recipes/221251-maximum-cardinality-matching-in-general-graphs/

import cython

cdef class UnionFind:
  cpdef __getitem__(self, item)
  cpdef union(self, *items)

@cython.locals(v=cython.int, w=cython.int, x=cython.int, matching=cython.dict)
cdef maxMatching(G, dict initialMatching=*, q=*):
  @cython.locals(base=cython.dict, S=cython.dict, T=cython.dict)
  cpdef augment()

    @cython.locals(a=cython.int, w=cython.int, base=cython.dict, S=cython.dict, T=cython.dict)
    cpdef blossom(int v, int w, int a)
      cpdef findSide(int v, int w)


    cpdef alternatingPath(int start, goal=*)

    @cython.locals(i=cython.int)
    cpdef pairs(L)

    @cython.locals(x=cython.int, y=cython.int)
    cpdef alternate(int v)
    cpdef addMatch(int v, int w)

    @cython.locals(head1=cython.int, head2=cython.int, path1=cython.dict, path2=cython.dict)
    cpdef ss(int v, int w)
