#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Verfahren zur Berechnung des total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
'''

from sympy import symbols, expand

from SGraph.SGraph import SGraph
from SGraph.SpecialGraphs import cycle, path, star, sunlet

from lib.ChromaticPoly import chromPoly, chromPolyTutte

x = symbols('x')


def test_chromPoly():
  assert chromPoly(Cycle(5)) == expand((-1)**5 * (x-1) + (x-1)**5)
  assert chromPoly(Cycle(6)) == expand((-1)**6 * (x-1) + (x-1)**6)
  assert chromPoly(Cycle(7)) == expand((-1)**7 * (x-1) + (x-1)**7)

  assert chromPoly(Path(5)) == expand(x * (x-1)**4)
  assert chromPoly(Path(6)) == expand(x * (x-1)**5)
  assert chromPoly(Path(7)) == expand(x * (x-1)**6)
  assert chromPoly(Path(8)) == expand(x * (x-1)**7)

  assert chromPoly(star(5)) == expand(x * (x-1)**4)
  assert chromPoly(star(6)) == expand(x * (x-1)**5)
  assert chromPoly(star(7)) == expand(x * (x-1)**6)
  assert chromPoly(star(8)) == expand(x * (x-1)**7)

  assert chromPoly(sunlet(3)) == expand((x-1)**(2*3) - (1-x)**(3-1))
  assert chromPoly(sunlet(4)) == expand((x-1)**(2*4) - (1-x)**(4-1))
  assert chromPoly(sunlet(5)) == expand((x-1)**(2*5) - (1-x)**(5-1))
  
def test_chromPolyTutte():
  assert chromPolyTutte(Cycle(5)) == expand((-1)**5 * (x-1) + (x-1)**5)
  assert chromPolyTutte(Cycle(6)) == expand((-1)**6 * (x-1) + (x-1)**6)
  assert chromPolyTutte(Cycle(7)) == expand((-1)**7 * (x-1) + (x-1)**7)

  assert chromPolyTutte(Path(5)) == expand(x * (x-1)**4)
  assert chromPolyTutte(Path(6)) == expand(x * (x-1)**5)
  assert chromPolyTutte(Path(7)) == expand(x * (x-1)**6)
  assert chromPolyTutte(Path(8)) == expand(x * (x-1)**7)

  assert chromPolyTutte(star(5)) == expand(x * (x-1)**4)
  assert chromPolyTutte(star(6)) == expand(x * (x-1)**5)
  assert chromPolyTutte(star(7)) == expand(x * (x-1)**6)
  assert chromPolyTutte(star(8)) == expand(x * (x-1)**7)

  assert chromPolyTutte(sunlet(3)) == expand((x-1)**(2*3) - (1-x)**(3-1))
  assert chromPolyTutte(sunlet(4)) == expand((x-1)**(2*4) - (1-x)**(4-1))
  assert chromPolyTutte(sunlet(5)) == expand((x-1)**(2*5) - (1-x)**(5-1))
