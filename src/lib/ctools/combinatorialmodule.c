/*
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <limits.h>
#include <stdbool.h> 
 
//typedef unsigned long ULONG;
 
unsigned long binomial(unsigned long n, unsigned long k)
{
	unsigned long r = 1, d = n - k;
 	
	if (k==n || k==0) return 1;
	if (k > n) return 0;
	if (k < 0) return 0;
	if (k > n/2)
	{
		k = n - k;
		d = n - k;
	}
 
	while (n > k)
	{
		if (r >= UINT_MAX / n) return 0; /* overflown */
		r *= n--;
 
		/* divide (n - k)! as soon as we can to delay overflows */
		while (d > 1 && !(r % d)) r /= d--;
	}
	return r;
}

unsigned long factorial(unsigned long n)
{
    unsigned long result = 1;
    for (int i = 1; i <= n; i++)
        result *= i;
    return result;
}

unsigned long stirling1(int n, int k)
{
	if (k==n || k==0) return 1;
	if (k>n) return 0;
	if (n==k+1) return n*(n-1)/2;
	if (k==1) return factorial(n-1);
	return stirling1(n-1, k-1) + (n-1) * stirling1(n-1, k);
}


unsigned long pow2(int n)
{
	if (n < 30)
		return 1L << n;
	bool odd = n & 1;
	unsigned long half = pow2(n >> 1);
	return odd ? 2 * half * half  : half * half;
}


unsigned long stirling2(int n, int k)
{
	if (n==k || k==1) return 1;
	if (k==0 || k>n) return 0;
	if (k==2) return pow2(n-1) - 1;
	return k * stirling2(n-1, k) + stirling2(n-1, k-1);
}

unsigned long bell(int n)
{
#if 0
	static const unsigned long Bell[]={
	/*  0 */ 1., 1., 2., 5.,
	/*  4 */ 15., 52., 203., 877.,
	/*  8 */ 4140., 21147., 115975., 678570.,
	/* 12 */ 4213597., 27644437., 190899322., 1382958545.,
	/* 16 */ .1048014215e11, .8286486980e11, .6820768062e12, .5832742205e13,
	/* 20 */ .5172415824e14, .4748698162e15, .4506715738e16, .4415200586e17,
	/* 24 */ .4459588693e18, .4638590332e19, .4963124652e20, .5457170479e21,
	/* 28 */ .6160539405e22, .7133980194e23, .8467490145e24, .1029335895e26,
	/* 32 */ .1280646700e27, .1629595893e28, .2119503939e29, .2816002030e30,
	/* 36 */ .3819714730e31, .5286836621e32, .7462898921e33, .1073882333e35,
	/* 40 */ .1574505884e36, .2351152508e37, .3574254920e38, .5529501188e39,
	/* 44 */ .8701963427e40, .1392585053e42, .2265418219e43, .3745005950e44,
	/* 48 */ .6289197963e45, .1072613715e47, .1857242688e48 };

	if (n>=0 && n<=50)
		return Bell[n];
#endif

	unsigned long b = 0;
	for (int k = 0; k <= n; k++) 
		b += stirling2(n, k);
	return b;
}


