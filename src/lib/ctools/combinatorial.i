%module combinatorial
extern unsigned long binomial(unsigned long n, unsigned long k);
extern unsigned long factorial(unsigned long n);
extern unsigned long stirling1(int n, int k);
extern unsigned long pow2(int n);
extern unsigned long stirling2(int n, int k);
extern unsigned long bell(int n);

