#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the independent domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 05.05.2014

@author: Markus Dod

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

from copy import deepcopy
from functools import reduce
import hashlib
import math
from random import random
from sympy import expand, symbols, simplify
from sys import exit as sys_exit

from lib.ChromaticPoly import compute_poly as compute_chrom_poly  # @UnresolvedImport
from lib.tools import binomial, Tree, kpowersetW, falling_factorial  # @UnresolvedImport
from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphFunctions import (isComplete, isPath, isCycle, isTree,  # @UnresolvedImport
                                  isCompleteBipartite, getBipartition)  # @UnresolvedImport
from SGraph.GraphFunctions import inducedSubgraph, components  # @UnresolvedImport
from SGraph.GraphParser import getGraphNamesdb, load_graphdb  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly', 'computeRelPoly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x = symbols('x')
p = symbols('p')
D = None


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the independent domination polynomial using complete enumeration
  @param graph: Graph
  '''
  P = 0
  for W in powerset(graph.getV()):
    if is_ind_dom(graph, W):
      P = x ** len(W) + P

  return P

def is_ind_dom(graph, W):
  '''
  Decides if the set W is a independent dominating set
  @param graph: Graph
  @param W: Vertex subset
  @return: Bool
  '''
  W = set(W)
  if len(graph.neighbors_set(W)) != graph.order():
    return False

  for v in W:
    if len(W.intersection(graph.neighbors(v, withloops=True))) != 0:
      return False
  return True

def is_ind(graph, W):
  '''
  Decides if the set W is a independent set
  @param graph: Graph
  @param W: Vertex subset
  @return: Bool
  '''
  if len(W) == 0:
    return True

  W = set(W)
  for v in W:
    if len(W.intersection(graph.neighbors(v))) != 0:
      return False
  return True

def edge_rec(graph):
  '''
  Calculates the independent domination polynomial using recurrences
  @param graph: SGraph
  '''
  # Check if the graph as an edge
  if graph.size() == 0:
    return x ** graph.order()
  if graph.order() == 0:
    return 1
  elif graph.order() == 1:
    return x

  # Calculate the polynomial for every component
  ID = []
  for g in components(graph):
    if g.order() == 1:
      ID.append(x)

    else:
      deg = g.get_degrees()
      deg = sorted(deg, key=deg.get, reverse=True)
      v = deg[0]
      u = 0
      for w in deg:
        if w != v and g.is_adjacent(v, w):
          u = w
          break
      if u == 0:
        ID.append(compute_polynomial(g))
      else:
        N_u = powerset(list(g.privateNeighbors(u, set([u, v]))))
        N_v = powerset(list(g.privateNeighbors(v, set([u, v]))))
        ID0 = 0
        for w in N_u:
          if len(w) != 0 and isIndependent(graph, w):
            ID0 += (-1) ** (len(w) + 1) * x ** len(w) * compute_polynomial(g - set(g.neighbors_set(set(w) | set([v]))))

        for w in N_v:
          if len(w) != 0 and isIndependent(graph, w):
            ID0 += (-1) ** (len(w) + 1) * x ** len(w) * compute_polynomial(g - set(g.neighbors_set(set(w) | set([u]))))

        ID.append(edge_rec(g - (u, v)) + \
                   x * (edge_rec(g - set(g.neighbors_set(set([u])))) + edge_rec(g - set(g.neighbors_set(set([v]))))) - \
                  x ** 2 * edge_rec(g - set(g.neighbors_set(set([u, v])))) - x * ID0)

  return reduce(lambda x, y: x * y, ID)

def isIndependent(graph, X):
  '''Check if the set X is independent in the graph'''
  for u in X:
    for v in X:
      if u != v:
        if graph.is_adjacent(u, v):
          return False
  return True

def recLoop(graph):
  '''
  Calculate the independent domination polynomial with the recurrence equation
  (using loops in the graph)
  '''
  # Check if the graph as an edge
  if not graph:
    return 0
  if graph.size(withloops=False) == 0:
    if graph.size(withloops=True) == 0:
      return x ** graph.order()
    else:
      return 0
  if graph.order() == 0:
    return 1
  elif graph.order() == 1 and graph.size(withloops=True) == 0:
    return x
  elif graph.order() == 1:
    return 0

  # Calculate the polynomial for every component
  ID = []
  for g in components(graph):
    if g.order() == 1 and g.size(withloops=True) == 0:
      ID.append(x)
    elif g.order() == 1:
      ID.append(0)

    else:
      deg = g.get_degrees(withloops=True)
      deg = sorted(deg, key=deg.get, reverse=True)
      v = deg[0]
      g1 = g.copy()
      g1.loopRemoveVertex(v)
      if g.hasLoop(v):
        k = recLoop(g - v) - recLoop(g1)
      else:
        k = x * recLoop(g - g.neighbors_set(set([v]))) + recLoop(g - v) - recLoop(g1)

      ID.append(k)

  return reduce(lambda x, y: x * y, ID)

def compute_forbiddenVertices(graph):
  '''
  Calculates the independent domination polynomial.
  Uses the forbidden vertices of the graph
  @param graph: Graph
  '''
  P = 0
  I = set([])

  n = graph.order()
  deg = graph.get_degrees()
  for v in deg:
    if deg[v] == n - 1:
      P = P + x
      I.add(v)

  V_new = set(graph.getV()) - I
  W = [1]
  i = 2
  while len(W) != 0:
    W = kpowersetW(V_new, i, I)
    for S in W:
      if is_ind_dom(graph, S):
        I.add(S)
        P = P + x ** len(S)
    i += 1

  return P

def compute_polynomial2(graph):
  '''
  Calculation of the independent domination polynomial 
  using the number of isolated vertices of the subgraphs
  '''
  P = 0
  for W in powerset(graph.getV()):
    P = (-1) ** len(W) * (1 - x) ** sum([1 for v in W if len(set(graph.neighbors(v)) & set(W)) == 0]) + P

  return P

def compute_polynomial3(graph):
  '''
  Calculation of the independent domination polynomial 
  using the number of isolated vertices of the subgraphs
  '''
  P = 0
  for k in range(graph.order() + 1):
    R = 0
    for W in powerset(graph.getV()):
      R = (-1) ** (len(W) + k) * binomial(sum([1 for v in W if len(set(graph.neighbors(v)) & set(W)) == 0]), k) + R
    P = P + x ** k * R

  return P

def decomp(graph):
  if graph.order() == 1:
    return x
  elif graph.order() == 0:
    return 1

  v = (graph.getV())[0]
  g1 = graph.copy()
  g1.deleteVertex(v)
  g2 = graph.copy()
  g2.removeNeighborEdges(v)
  g3 = graph.copy()
  g3.removeNeighborEdges(v)
  g3.deleteVertex(v)
  return compute_polynomial(g1) + compute_polynomial(g2) - compute_polynomial(g3)

def essential_calculation(graph):
  '''
  Calculation of the independent domination polynomial 
  using the independent essential sets of the graph
  @param graph: SGraph
  '''
  P = 0

  # Generate the independent essential sets
  S = set([])
  # Start with the neighbors of the vertices of the graph
  for v in graph.getV():
    S.add(frozenset(graph.neighbors(v)))

  # Add the supersets to S
  # This also generates set which are non-essential!
  # TODO::Exclude the non-essential sets.
  for _ in range(0, graph.order()):
    T = S.copy()
    for t in T:
      for v in graph.getV():
        if v not in t:
          f = set(t)
          f.add(v)
          S.add(frozenset(f))

  # Calculate the polynomial
  for U in S:
    k = isEssential(graph, set(U))
    if k >= 1:
      P = (-1) ** len(U) * ((1 - x) ** k - 1) + P

  P = (-1) ** graph.order() * P

  return P

def isEssential(graph, U):
  '''
  Check if the set U is a independent essential set
  @param graph: SGraph
  @param U: Vertex subset (set)
  @return: Number of essential vertices in U (k=0 if U is not a ind.essentail set)
  '''
  return sum([1 for v in set(graph.getV()) - U if set(graph.neighbors(v)) <= U])


# =============================================================================
# ================ Trees ======================================================
# =============================================================================
def calculateTree(graph):
  '''
  Calculates the independent domination polynomial of a tree.
  @param graph: Tree
  @return: The independent domination polynomial of the tree 
  '''
  t = Tree()
  t.fromSGraph(graph)

  states = calc_tree(t, t.get_root())

  return states.Y[0] + states.Y[1]

def calc_tree(tree, node):
  '''
  Recursiv function for the calculation of the independet domination
  polynomial of a tree
  '''
  if len(node.childs) == 0:
    return DecompState(node.number, [x, 0, 1])

  else:
    if len(node.childs) == 1:
      v = node.childs[0]
      s = calc_tree(tree, tree.getVertex(v))
      s.activate(node.number)

      return s

    else:
      Z = []
      for v in node.childs:
        s = calc_tree(tree, tree.getVertex(v))
        Z.append(s)

      new = [0, 0, 0]
      a = 1
      b = 1
      c = 1
      for state in Z:
        a = (state.Y[1] + state.Y[2]) * a
        b = (state.Y[0] + state.Y[1]) * b
        c = state.Y[1] * c

      new[0] = x * a
      new[1] = b - c
      new[2] = c

      return DecompState(node.number, new)

class DecompState():
  '''
  State for the calculation of the independet domination
  polynomial of a tree
  '''
  def __init__(self, v, poly=[0, 0, 0]):
    self.v = v
    self.Y = poly

  def activate(self, v):
    '''
    Activate a vertex
    @param v: Vertex of the graph
    '''
    self.v = v
    tmp0 = x * (self.Y[1] + self.Y[2])
    tmp1 = self.Y[0]
    tmp2 = self.Y[1]

    self.Y[0] = tmp0
    self.Y[1] = tmp1
    self.Y[2] = tmp2

  def copy(self):
    '''
    Return a copy of the state
    @return: Copy
    '''
    other = DecompState(deepcopy(self.partition))
    other.Y = self.Y
    return other

  def __str__(self):
    '''Return a string-representation of the state'''
    return "v: %s -> %s, %s, %s" % \
                        (str(self.v), str(self.Y[0]), str(self.Y[1]), str(self.Y[2]))


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcCompleteGraph(n):
  '''Calculates the independent domination polynomial of the complete graph'''
  return x * n

def calcCompleteBipartiteGraph(graph):
  '''Calculates the independent domination polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  return x ** n + x ** m

def calcPath(n):
  '''Calculates the independent domination polynomial of the path'''
  D = 0
  for k in range(1, math.floor((n + 1) / 2) + 1):
    D += binomial(k + 1, n - 2 * k + 1) * x ** k
  return D

def calcPathRec(n):
  '''Calculates the independent domination polynomial of the path (using recurrence equation, slow)'''
  from collections import deque
  poly = deque([x, 2 * x, x ** 2 + x])
  if n <= 0:
    return 0
  elif n <= 3:
    return poly[n - 1]

  i = 3
  while i != n:
    i += 1
    poly.append(x * poly[0] + x * poly[1])
    poly.popleft()

  return poly[2]

def calcCycle(n):
  '''Calculates the independent domination polynomial of the cycle'''
  if n == 3:
    return 3 * x

  return 2 * x * calcPath(n - 3) + x ** 2 * calcPath(n - 6)

def calcStar(n):
  '''Calculate the ind. dom. poly of a star'''
  return x + x ** (n - 1)

def calculateCentipede(graph):
  '''Calculate the ind. dom. poly of a centipede'''
  n = int(graph.order() / 2)
  return int(((5 - 3 * math.sqrt(5)) * (1 - math.sqrt(5)) ** n + \
              (5 + 3 * math.sqrt(5)) * (1 + math.sqrt(5)) ** n) / (2 ** (n + 1) * 5)) * x ** n

def calculateBananaTree(graph):
  '''Calculate the ind. dom. poly of a banana tree'''
  try:
    n = graph.data[0]
    k = graph.data[1]
  except:
    return 0

  return x * (calcStar(k - 1)) ** n + (calcStar(k)) ** n - x ** n

def calculateFirecracker(graph):
  '''Calculate the ind. dom. poly of a firecracker'''
  try:
    n = graph.data[0]
    k = graph.data[1]
  except:
    return 0

  def firecracker(n, k):
    if n == 0:
      return 1
    elif n == 1:
      return x + x ** (k - 1)
    elif n == 2:
      return 2 * x ** (2 * k - 3) + 2 * x ** k + x ** 2
    else:
      return (x ** (2 * k - 3) + x ** k) * firecracker(n - 2, k) + x * firecracker(n - 1, k) + (x ** (3 * k - 5) + x ** (2 * k - 2)) * firecracker(n - 3, k)

  if k == 2:
    return calculateCentipede(graph)
  else:
    return firecracker(n, k)


# =============================================================================
# ========= Product graphs ====================================================
# =============================================================================
def calculateCartesianKK(n, m):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of two complete graphs
  @param n: Order of the first graph
  @param m: Order of the second graph
  '''
  if n <= m:
    return falling_factorial(m, n) * x ** n
  else:
    return falling_factorial(n, m) * x ** m

def calculateCartesianKP(n, m):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of a complete graph and a path.
  @param n: Order of the complete graph
  @param m: Order of the path
  '''
  return n * (n - 1) ** (m - 1) * x ** m

def calculateCartesianKC(n, m):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of a complete graph and a cycle.
  @param n: Order of the complete graph
  @param m: Order of the cycle
  '''
  return ((-1) ** m * (n - 1) + (n - 1) ** m) * x ** m

def calculateCartesianK2G(graph):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of a complete graph with two vertices and a arbitrary graph.
  @param graph: Graph
  '''
  P = 0
  V = set(graph.getV())
  for W in powerset(graph.getV()):
    N = graph.neighbors_set(W)
    if len(W) != 0 and is_ind(graph, W) and is_ind(graph, V - N):
      X = set(W) | graph.neighbors_set(V - N)
      P = P + x ** (len(W) + graph.order() - len(N)) * compute_polynomial(graph - X)
  return P

def calculateCartesianK3G(graph):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of a complete graph with three vertices and a arbitrary graph.
  @param graph: Graph
  '''
  P = 0
  V = set(graph.getV())
  m = graph.order()

  # Choose dominating vertices in the first row
  for W in powerset(graph.getV()):
    NW = graph.neighbors_set(W)
    if len(W) != 0 and is_ind(graph, W):
      P2 = 0
      # Choose dominating vertices in the second row
      for U in powerset(list(V - NW)):
        NU = set(graph.neighbors_set(U))
        X = V - NW - set(U)  # Dominating vertices in the third row
        NX = graph.neighbors_set(X)
        if is_ind(graph, U) and is_ind(graph, X):
          Y = NW - set(W) - NU
          # Choose additional dominating vertices in the second row
          for Z in powerset(list(Y)):
            NZ = set(graph.neighbors_set(Z))
            if is_ind(graph, Z) and is_ind(graph, set(U) | set(Z)) and is_ind(graph, X | (Y - NZ)):
              O = NW - set(W) - set(Z) - NX - set(graph.neighbors_set(Y - NZ))  # Rest non-dominated vertices
              P2 = P2 + x ** (len(Z) + len(Y - NZ)) * \
                        compute_polynomial(inducedSubgraph(graph, O))

      P = P + x ** (m + len(W) - len(NW)) * P2

  return P

def calculateCartesianKG(n, graph):
  '''
  Calculates independent domination polynomial of the Cartesian product
  of a complete graph with some degree restrictions and a arbitrary graph.
  @param n: Order of the complete graph
  @param graph: Graph
  '''
  delta = graph.get_max_degree()
  m = graph.order()
  if n >= delta + 1:
    return compute_chrom_poly(MDNetwork_log, graph, method.TUTTE).subs({x: n}) * x ** m
  else:
    degree_seq = graph.get_degrees()
    count = 0
    v = None
    for w in degree_seq:
      # If its degree is high enough but it is not adjacent to all other vertices,
      # then return None
      if degree_seq[w] >= n and degree_seq[w] != m - 1:
        return None
      # If to many vertices with high degree exist
      elif degree_seq[w] >= n and count >= 1:
        return None
      # otherwise count it
      elif degree_seq[w] >= n:
        count += 1
        v = w

    # Calculate the poly
    if count == 1:
      D = compute_chrom_poly(MDNetwork_log, graph, method.TUTTE).subs({x: n}) * x ** m
      H = graph - v
      chrom_poly = compute_chrom_poly(MDNetwork_log, H, method.TUTTE)
      for k in range(0, n):
        D = D + x ** (m - 1) * (-1) ** k * binomial(n, k) * chrom_poly.subs({x:(n - k)})
      return D
    else:
      return None

def calculateLexiGH(graph1, graph2):
  '''
  Calculates independent domination polynomial of the lexicographic product
  of two arbitrary graphs.
  @param graph1: First graph
  @param graph2: Second graph
  '''
  ID2 = compute_polynomial(graph2)
  ID1 = compute_polynomial(graph1)
  return ID1.subs({x: ID2})

def calculateLexiPG(n, graph2):
  '''
  Calculates independent domination polynomial of the lexicographic product
  of a path with an arbitrary graph.
  @param n: Order of the path
  @param graph2: Second graph of the product
  '''
  if n <= 0:
    return 1

  global D
  if D == None:
    D = essential_calculation(graph2)

  if n == 1:
    return D

  return D * (calculateLexiPG(n - 2, graph2) + calculateLexiPG(n - 3, graph2))

def calculateLexiCG(n, graph2):
  '''
  Calculates independent domination polynomial of the lexicographic product
  of a cycle with an arbitrary graph.
  @param n: Order of the cycle
  @param graph2: Second graph of the product
  '''
  if n <= 0:
    return 1

  global D
  if D == None:
    D = essential_calculation(graph2)

  return D * (2 * calculateLexiPG(n - 3, graph2) + D * calculateLexiPG(n - 6, graph2))

def calculateTensorKnKm(n, m):
  '''
  Calculates the independent domination polynomial of the tensor product
  of two complete graphs
  @param n: Order of the first graph
  @param m: Order of the second graph 
  '''
  return n * x ** m + m * x ** n

def calculateTensorPnKm(n, m):
  '''
  Calculates the independent domination polynomial of the tensor product
  of a path and a complete graphs
  @param n: Order of the path
  @param m: Order of the complete graph 
  '''
  def f(n, m):
    '''Recursive function for P_n x K_m'''
    if n == 0:
      return 1
    elif n == 1:
      return x ** m
    elif n == 2:
      return n * x ** m + m * x ** n
    else:
      return x ** m * f(n - 2, m) + x ** m * f(n - 3, m) + m * x ** 2 * g(n - 2, m)

  def g(n, m):
    '''
    Recursive function for P_n-1 x K_m with an additional vertex
    adjacent to all but one vertices in the first row of the product
    '''
    if n == 0:
      return 1
    elif n == 1:
      return x
    elif n == 2:
      return x ** 2 + x ** m
    else:
      return x * g(n - 1, m) + (m - 1) * x ** 2 * g(n - 3, m) + x ** m * f(n - 3, m)

  return f(n, m)

def calculateTensorPnP3(n, m):
  '''
  Calculates the independent domination polynomial of the tensor product
  of a path and a path with 3 vertices
  @param n: Order of the first path
  @param m: Order of the second path 
  '''
  def g1(n):
    if n <= 0:
      return 0
    elif n == 2:
      return x ** 2 + x
    else:
      return x * g1(n - 2) + x ** 2 * g3(n - 3)

  def g2(n):
    if n <= 0:
      return 0
    elif n == 1:
      return x
    elif n == 3:
      return 2 * x ** 2
    else:
      return x * g2(n - 2) + x ** 2 * g1(n - 3)

  def g3(n):
    if n <= 0:
      return 0
    elif n == 1:
      return x ** 2
    elif n == 3:
      return x ** 4 + x
    else:
      return x ** 2 * g3(n - 2) + x * g1(n - 3)

  if m == 3:
    if n % 2 == 0:
      return g1(n) ** 2
    else:
      return g2(n) * g3(n)
  elif n == 3:
    if m % 2 == 0:
      return g1(m) ** 2
    else:
      return g2(m) * g3(m)
  else:
    return None


# =============================================================================
# =============================================================================
# =============================================================================
def calc_all_graphs(graph=None, fname=None):
  '''
  Calculates the independent domination polynomial all graphs in fname
  and search for equal polynomials
  '''
  if graph == None and fname == None:
    return "error no graph or file"

  graphs = []
  poly = set([])
  test = {}
  if graph == None:
    graphnames = getGraphNamesdb(fname)
    num = len(graphnames)
    print("Anzahl der Graphen: %d" % num)
    p = 0.0
    for name in graphnames:
      if name != 0:
        g = SGraph()
        g, _ = load_graphdb(fname, name)
        if g.order() > 0:
          D = compute_polynomial(g)
          str_D = str(D)
          m = hashlib.md5(str_D.encode()).hexdigest()
          if m in poly:
            print("identisch")
            print(g)
            print(test[m])
            # print(poly[m])
            sys_exit()

          else:
            # poly[m]= g
            poly.add(m)
            test[m] = name
        p += 100 / num
        print("%.4f Prozent" % p)

  else:
    graphs.append(graph)

  print("alle abgearbeitet....")

def calc_all_graphs_deck(fname=None):
  '''
  Calculates the independent domination polynomial all graphs in fname
  and search for equal polynomials
  '''
  if fname == None:
    return "error no file"

  poly = {}
  test = {}
  graphnames = getGraphNamesdb(fname)
  num = len(graphnames)
  print("Anzahl der Graphen: %d" % num)
  p = 0.0
  for name in graphnames:
    if name != 0:
      g = SGraph()
      g, _ = load_graphdb(fname, name)
      if g.order() > 0:
        D = compute_polynomial(g)
        str_D = str(D)
        m = hashlib.md5(str_D.encode()).hexdigest()

        # calculate the deck
        deck = []
        for v in g.getV():
          graph = g.copy()
          graph.deleteVertex(v)
          D = compute_polynomial(graph)
          str_D = str(D)
          deck.append(hashlib.md5(str_D.encode()).hexdigest())

        poly[m] = deck
        test[m] = name

      p += 100 / num
      print("%.4f Prozent" % p)

  print("starting investigation ...")
  hashpoly = list(poly.keys())
  for i, m1 in enumerate(hashpoly):
    deck1 = sorted(poly[m1])
#     print("--------")
#     print(test[m1])
#     print(deck1)
    for j, m2 in enumerate(hashpoly):
      if i < j and m1 != m2:
        deck2 = sorted(poly[m2])
        # print("....")
        # print(test[m2])
        # print(deck2)

        if deck1 == deck2:
          print("---------")
          print(test[m1])
          print(test[m2])


  print("alle abgearbeitet....")

def calc_all_graphs_irrelevant(fname=None):
  '''
  Calculates the independent domination polynomial all graphs in fname
  and search for equal polynomials
  '''
  if fname == None:
    # return "error no file"
    fname = "../data/graph7c.db"

  graphnames = getGraphNamesdb(fname)
  num = len(graphnames)
  print("Anzahl der Graphen: %d" % num)
  p = 0.0
  for name in graphnames:
    if name != 0:
      g = SGraph()
      g, _ = load_graphdb(fname, name)
      if g.order() > 0:
        D = expand(compute_polynomial(g))

        # calculate the deck
        for v in g.getV():
          graph = g.copy()
          graph.deleteVertex(v)
          D2 = expand(compute_polynomial(graph))
          if D == D2:
            print("------")
            print(name)
            print(graph)

      p += 100 / num
      # print("%.4f Prozent" % p)


  print("alle abgearbeitet....")



# =============================================================================
# ============== Reliability ==================================================
# =============================================================================
def compute_RelPoly(graph):
  '''
  Calculation of the independent domination polynomial using complete enumeration
  @param graph: Graph
  '''
  P = 0
  n = graph.order()
  for W in powerset(graph.getV()):
    if is_ind_dom(graph, W):
      P = p ** len(W) * (1 - p) ** (n - len(W)) + P

  return P

# def simulatePolynomial(graph):
#   '''Simulates the reliability function'''
#   rel = np.linspace(0.0, 0.45, num=graph.order()+1)
#   runnning = 300000
#   sol = []
#   V = graph.getV()
#
#   for r in rel:
#     suc = 0
#     for i in range(runnning):
#       W = chooseSet(V, r)
#       if is_ind_dom(graph, W):
#         suc += 1
#     sol.append((r, suc/runnning))
#
#   sol.append((0.6,0.0))
#   sol.append((0.7,0.0))
#   sol.append((0.8,0.0))
#   sol.append((0.9,0.0))
#   sol.append((1.0,0.0))
#
#   from sympy.polys.polyfuncs import interpolate
#   ID = interpolate(sol, p)
#
#   return ID

def simulatePolynomial(graph):
  '''Simulates the reliability function'''
  running = 300000
  r = 0.5
  V = graph.getV()
  suc = 0
  for _ in range(running):
    W = chooseSet(V, r)
    if is_ind_dom(graph, W):
      suc += 1

  return suc / running

def chooseSet(V, r):
  '''randomly choose a subset of V with the probability r'''
  W = set([])
  for v in V:
    if random() <= r:
      W.add(v)
  return W


# =============================================================================
# ============ Convert polynomials ============================================
# =============================================================================
def domtorel(D, n):
  '''
  Transform the independent domination polynomial in the corresponding reliability polynomial
  @param D: Independent domination polynomial
  @param n: Order of the graph
  '''
  return expand(simplify((1 - p) ** n * D.subs(x, p / (1 - p))))


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, graph1=None, graph2=None, q=None):
  '''
  Calculate the independent domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  ID = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      MDNetwork_log.info("starting calculation with algorithm for complete graphs ...")
      ID = calcCompleteGraph(graph.order())
    elif isPath(graph):
      MDNetwork_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif isCycle(graph):
      MDNetwork_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())
    elif isCompleteBipartite(graph):
      MDNetwork_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      ID = calcCompleteBipartiteGraph(graph)
    elif isTree(graph):
      MDNetwork_log.info("starting calculation with algorithm for trees ...")
      ID = calculateTree(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      MDNetwork_log.info("starting calculation with algorithm for complete graphs ...")
      ID = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      MDNetwork_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      MDNetwork_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      MDNetwork_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      ID = calcCompleteBipartiteGraph(graph)
    elif graph.graphtype == graphType.TREE:
      MDNetwork_log.info("starting calculation with algorithm for trees ...")
      ID = calculateTree(graph)
    elif graph.graphtype == graphType.CENTIPEDE:
      MDNetwork_log.info("starting calculation with algorithm for centipedes ...")
      ID = calculateCentipede(graph)
    elif graph.graphtype == graphType.BANANATREE:
      MDNetwork_log.info("starting calculation with algorithm for banana trees ...")
      ID = calculateBananaTree(graph)
    elif graph.graphtype == graphType.FIRECRACKER:
      MDNetwork_log.info("starting calculation with algorithm for firecrackers ...")
      ID = calculateFirecracker(graph)

    elif graph.graphtype == graphType.CARTESIAN and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the Cartesian product ...")
      if graph1.graphtype == graphType.COMPLETE and graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of two complete graphs.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKK(graph1.order(), graph2.order())
      elif graph1.graphtype == graphType.COMPLETE and graph2.graphtype == graphType.PATH:
        MDNetwork_log.info("recognized product of a complete graph and a path.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKP(graph1.order(), graph2.order())
      elif graph2.graphtype == graphType.COMPLETE and graph1.graphtype == graphType.PATH:
        MDNetwork_log.info("recognized product of a complete graph and a path.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKP(graph2.order(), graph1.order())
      elif graph1.graphtype == graphType.COMPLETE and graph2.graphtype == graphType.CYCLE:
        MDNetwork_log.info("recognized product of a complete graph and a cycle.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKC(graph1.order(), graph2.order())
      elif graph2.graphtype == graphType.COMPLETE and graph1.graphtype == graphType.CYCLE:
        MDNetwork_log.info("recognized product of a complete graph and a cycle.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKC(graph2.order(), graph1.order())
      elif (graph1.graphtype == graphType.COMPLETE or graph1.graphtype == graphType.PATH) and graph1.order() == 2:
        MDNetwork_log.info("recognized product of a complete graph of order two with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianK2G(graph2)
      elif (graph2.graphtype == graphType.COMPLETE or graph2.graphtype == graphType.PATH) and graph2.order() == 2:
        MDNetwork_log.info("recognized product of a complete graph of order two with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianK2G(graph1)
      elif graph1.graphtype == graphType.COMPLETE and graph1.order() == 3:
        MDNetwork_log.info("recognized product of a complete graph of order three with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianK3G(graph2)
      elif graph2.graphtype == graphType.COMPLETE and graph2.order() == 3:
        MDNetwork_log.info("recognized product of a complete graph of order three with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianK3G(graph1)
      elif graph1.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a complete graph with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKG(graph1.order(), graph2)
      elif graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a complete graph with an arbitrary graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateCartesianKG(graph2.order(), graph1)
      else:
        MDNetwork_log.info("Nothing found ...")
        MDNetwork_log.info("")

    elif graph.graphtype == graphType.LEXI and graph1 != None and graph2 != None:
      MDNetwork_log.info("calculating the lexicographic product of two graphs ...")
      ID = calculateLexiGH(graph1, graph2)

    elif graph.graphtype == graphType.TENSOR and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the tensor product ...")
      if graph1.graphtype == graphType.COMPLETE and graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of two complete graphs.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateTensorKnKm(graph1.order(), graph2.order())
      elif graph1.graphtype == graphType.PATH and graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a complete graph and a path.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateTensorPnKm(graph1.order(), graph2.order())
      elif graph2.graphtype == graphType.PATH and graph1.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a complete graph and a path.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateTensorPnKm(graph2.order(), graph1.order())
      elif (graph1.graphtype == graphType.PATH and graph2.graphtype == graphType.PATH
            and (graph1.order() == 3 or graph2.order() == 3)):
        MDNetwork_log.info("recognized product of a complete graph and a path.")
        MDNetwork_log.info("Starting calculation ...")
        ID = calculateTensorPnP3(graph1.order(), graph2.order())

    elif graph.graphtype == graphType.STRONG and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the strong product ...")
      if graph1.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a graph with a complete graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = essential_calculation(graph2).subs({x: graph1.order() * x})
      elif graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a graph with a complete graph.")
        MDNetwork_log.info("Starting calculation ...")
        ID = essential_calculation(graph1).subs({x: graph2.order() * x})

  if ID:
    if q != None:
      q.put(ID)
    return expand(ID)

  if m == method.STATE_SPACE or m == None or m == method.AUTOMATIC:
    MDNetwork_log.info("starting complete enumeration ...")
    ID = compute_polynomial(graph)
  elif m == method.DECOMP:
    MDNetwork_log.info("starting calculation with decomposition ...")
    # ID = edge_rec(graph)
    ID = recLoop(graph)
  elif m == method.TREE:
    MDNetwork_log.info("starting calculation with tree-algorithm ...")
    ID = calculateTree(graph)
  elif m == method.ESSENTIAL:
    MDNetwork_log.info("starting calculation with independent essential sets ...")
    ID = essential_calculation(graph)

  if q != None:
    q.put(ID)
  return expand(ID)

def computeRelPoly(MDNetwork_log2, graph, m=None, graph1=None, graph2=None, q=None):
  '''
  Calculate the independent domination reliability polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  ID = None
  if m == method.STATE_SPACE:
    MDNetwork_log.info("starting complete enumeration ...")
    ID = compute_RelPoly(graph)
  elif m == method.SIM:
    MDNetwork_log.info("starting simulating ....")
    ID = simulatePolynomial(graph)
  else:
    ID = domtorel(compute_poly(MDNetwork_log2, graph, m, graph1, graph2), graph.order())

  if q != None:
    q.put(ID)
  return expand(ID)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
