#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculation of the domination polynomial and the domination reliability polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

TODO:
- Baumzerlegungsalgorithmus programmieren
- Verfahren kommentieren
- Tests einzeln abschaltbar machen
- Bei nicht zusammenhängenden Graphen Komponenten einzeln betrachten
- Voll. enumeration für manche Graphen langsam

Created on 08.10.2012

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from copy import copy, deepcopy
from random import choice
from sympy import symbols, degree, simplify
import sys

from lib.Decompositions import generate_clique_decomposition  # @UnresolvedImport
from lib.tools import powerset, mset, argmax  # @UnresolvedImport

from SGraph.GraphFunctions import (iskTree, iskStar, isskPath, iskCycle,  # @UnresolvedImport
                                   isCycle, isPath)  # @UnresolvedImport

from modules.consts import method, ob_type, graphType  # @UnresolvedImport
from modules.error import GraphClassError  # @UnresolvedImport


# =============================================================================
# =============================================================================
MAX_STATES = 0
NODES_IN_USE = mset([])
NUM_PROCESSES = 4

# Globals for k-paths and k-cycles
CALC_F = {}
CALC_H = {}
CALC_P = {}

global_X = None
global_Y = None
global_graph = None

x = symbols('x')
p = symbols('p')


# =============================================================================
# =============================================================================
# =============================================================================
def state_space_enumeration(graph, X=None, Y=None):
  '''
  Calculates DRel(G) with complete enumeration
  @param graph: Graph
  @param X: Set of the (possible) dominating vertices
  @param Y: Set of the to dominate vertices
  @return DRel(G, X, Y; x)
  '''
  # Init the sets X and Y
  if Y == None:
    Y = set(graph.getV())
    X = graph.getV()
  DRel = 0
  size_x = len(X)

  neighbors = graph.neighbors
  for W in powerset(X):
    len_w = len(W)
    if len_w > 0:
      vertices = map(neighbors, W)
      vertices = set.union(set(W), *vertices)

      if Y <= vertices:
        DRel += p**len_w * (1 - p)**(size_x - len_w)

  return DRel

def state_space_enumeration2(graph):
  '''
  Calculates DRel(G) with complete enumeration
  @param graph: Graph
  @return DRel(G, x)
  '''
  D = 0
  V = set(graph.getV())

  for U in powerset(graph.getV()):
    flag = False
    for u in V - set(U):
      if len(set(U).intersection(set(graph.neighbors(u)))) == 0:
        flag = True
        break

    if flag:
      D += x ** len(U)

  return D

def domPoly(G, X, Y):
  '''
  Calculates DRel(G) with decomposition
  @param graph: Graph
  @param X: Set of the (possible) dominating vertices
  @param Y: Set of the to dominate vertices
  @return DRel(G, X, Y; x)
  '''
  if len(Y) == 0:
    return (x + 1) ** len(X)

  NX = G.neighbors_set(X)
  if not Y <= NX:
    return 0

  NY = G.neighbors_set(Y)
  if not X <= NY:
    return (x + 1) ** (len(X - NY)) * domPoly(G, X & NY, Y)
  else:
    degrees = G.get_degrees()
    for w in set(G.getV()) - X:
      del degrees[w]
    v = argmax(degrees)

    g = G.copy()
    g.deleteVertex(v)
    return x * domPoly(g, X - set([v]), Y - (G.neighbors(v) | set([v]))) + domPoly(G, X - set([v]), Y)

def essential_calculation(graph):
  '''
  Calculates the domination polynomial with essential subsets
  '''
  P = 0
  for W in powerset(graph.getV()):
    k = is_essential(graph, W)
    if k > 0:
      P += ((x + 1) ** k - 1) * (-1) ** len(W)

  P = P * (-1) ** graph.order()
  return P

def is_essential(graph, W):
  '''
  Tests if the vertex subset W is an essential set in the graph
  '''
  k = 0
  for u in W:
    if set(graph.neighbors(u)) <= set(W):
      k += 1
  return k


# =============================================================================
# ================ Clique-Decomposition =======================================
# =============================================================================
def clique_decomposition(graph):
  '''
  Calculates the DRel with cliqudecomposition
  @param graph: SGraph
  @return: DRel
  '''
  g = graph.copy()
  t = generate_clique_decomposition(g)

  # Start the calculation
  states = compute_node(t, t.get_root())

  MDNetwork_log.info("Maximum number of states: %s" % str(MAX_STATES))

  # Sum the results in the states
  rel = 0
  for state in states:
    if len(state.B) == len(state.X):
      rel = rel + state.p

  return rel

def compute_node(tree, node):
  '''
  Compute a vertex of the decomp tree
  @param tree: The decomp tree
  @param node: Vertex
  '''
  global NODES_IN_USE

  states = []
  if node.node_type == ob_type.NODE:
    states.append(CliqueDecompState(mset([int(node.data)]), mset([int(node.data)]), mset([int(node.data)]), p))
    states.append(CliqueDecompState(mset([int(node.data)]), mset([]), mset([]), 1 - p))

  elif node.node_type == ob_type.PLUS:
    tmp_states = compute_node(tree, tree.get_child(node.number, "left"))
    tmp_states2 = compute_node(tree, tree.get_child(node.number, "right"))
    for s in tmp_states:
      for t in tmp_states2:
        X = copy(s.X)
        X.union(t.X)
        A = copy(s.A)
        A.union(t.A)
        B = copy(s.B)
        B.union(t.B)
        state = CliqueDecompState(X, A, B)
        state.p = s.p * t.p
        states.append(state)

  elif node.node_type == ob_type.RENAME:
    s = node.data.split("-")
    u = int(s[0])
    v = int(s[1])
    NODES_IN_USE.add([u, v])

    states = compute_node(tree, tree.get_child(node.number, "left"))

    for i in range(len(states)):
      states[i].change_name(u, v)

    NODES_IN_USE.remove(u)
    NODES_IN_USE.remove(v)

  elif node.node_type == ob_type.EDGE:
    s = node.data.split("-")
    u = int(s[0])
    v = int(s[1])
    NODES_IN_USE.add([u, v])

    states = compute_node(tree, tree.get_child(node.number, "left"))

    for i in range(len(states)):
      states[i].set_edge([u, v])

    NODES_IN_USE.remove(u)
    NODES_IN_USE.remove(v)

  else:
    sys.exit("Typ unbekannt")

  states = delete_unused_states(states)
  states = compress_states(states)

  global MAX_STATES
  MAX_STATES = max(MAX_STATES, len(states))

  MDNetwork_log.debug("------------------------------")
  MDNetwork_log.debug(node)
  MDNetwork_log.debug(len(states))

  return states

def compress_states(tmp_states):
  '''
  Combine equal states
  '''
  flag = True
  while flag == True:
    flag = False
    len_states = len(tmp_states)
    for i in range(len_states - 1):
      for j in range(i + 1, len_states):
        if tmp_states[i] == tmp_states[j]:
          flag = True
          tmp_states[i].p = tmp_states[i].p + tmp_states[j].p
          del tmp_states[j]
          break
      if flag == True:
        break

  return tmp_states

def delete_unused_states(tmp_states):
  '''
  Deletes not required states in tmp_states
  @param tmp_states: Set of states
  @return: New set of states
  '''
  global NODES_IN_USE
  del_index = []
  el = []
  for node in tmp_states[0].X.get_elements():
    if node not in NODES_IN_USE:
      el.append(node)
  for i, _ in enumerate(tmp_states):
    for v in el:
      if tmp_states[i].X.get_num_element(v) != tmp_states[i].B.get_num_element(v):
        del_index.append(i)
        break
      else:
        tmp_states[i].X.remove_all(v)
        tmp_states[i].A.remove_all(v)
        tmp_states[i].B.remove_all(v)

  for i in range(len(del_index) - 1, -1, -1):
    del tmp_states[del_index[i]]

  return tmp_states

class CliqueDecompState():
  '''
  Class of a state of the clique decomposition
  @param X: Multiset of the active vertices
  @param A: Multiset of the dominating vertices
  @param B: Multiset of the to dominate vertices
  @param poly: Probability of the state
  '''

  def __init__(self, X=mset([]), A=mset([]), B=mset([]), poly=1):
    self.X = X
    self.A = A
    self.B = B
    self.p = poly

  def __eq__(self, other):
    '''
    Test if two states are equal
    '''
    if self.X == other.X and self.A == other.A and self.B == other.B:
      return True
    return False

  def set_p(self, p):
    '''
    Set the probability of the state
    @param p: Poly
    '''
    self.p = p

  def set_edge(self, e):
    '''
    Insert the edge e
    @param e: Edge of the graph
    '''
    if e[0] not in self.X:
      if len(self.X.get_elements()) > 1:
        while e[0] == e[1]:
          e[0] = choice(list(self.X))
      else:
        e[0] = self.X.get_elements()[0]
        e[1] = self.X.get_elements()[0]
    if e[1] not in self.X:
      if len(self.X.get_elements()) > 1:
        while e[0] == e[1]:
          e[1] = choice(list(self.X))
      else:
        e[1] = self.X.get_elements()[0]

    if e[0] in self.A:
      f = [e[1]] * (self.X.get_num_element(e[1]) - self.B.get_num_element(e[1]))
      self.B.union(mset(f))

    if e[1] in self.A:
      f = [e[0]] * (self.X.get_num_element(e[0]) - self.B.get_num_element(e[0]))
      self.B.union(mset(f))

  def change_name(self, u, v):
    '''
    Rename the vertex u to v
    '''
    self.X.rename(u, v)
    self.A.rename(u, v)
    self.B.rename(u, v)

  def print_me(self):
    '''Print this state'''
    MDNetwork_log.info("X: %s, A: %s, B: %s, p: %s" % (str(self.X), str(self.A), str(self.B), str(self.p)))


# =============================================================================
# ================ k-Cycle ====================================================
# =============================================================================
def calculate_kCycle(graph, k):
  '''
  Calculate the DRel of an k-cycle
  @param graph: SGraph
  @param k: The k of the k-cycle
  '''
  if graph.order() <= 2 * k + 1:
    return 1 - (1 - p) ** (graph.order())
  global CALC_F
  global CALC_H
  global CALC_P

  l = [i for i in range(1, k + 1)]
  pset = powerset(l)
  n = len(graph.getV())

  Rel = 0
  poly = 1
  for s in pset:
    if len(s) > 0:
      Rel = Rel + poly.from_string(str((-1) ** (len(s) + 1))) * p ** (len(s)) * kPath_twoSide(n - (max(s) - min(s) + 1), k)

  Rel2 = p * 2 * kPath_oneSide(n - k - 1, k) - p ** 2 * kPath_twoSide(n - k - 2, k)

  Rel3 = p
  for i in range(1, k):
    Rel3 = Rel3 + i * (1 - p) ** (i - 1) * kPath_twoSide(n - k - 3 - i, k)

  if len(Rel3) == 1 and (Rel3[0] == 0 or Rel3[0] == None):
    Rel3 = 1

  return Rel + (1 - p) ** k * (Rel2 + p ** 2 * (1 - p) ** 2 * Rel3)


# =============================================================================
# ================ k-Path =====================================================
# =============================================================================
def calculate_kPath(n, k):
  '''
  Calculates the DRel of a simple k-path
  @param n: Number of vertices of the k-path
  @param k: The k of the k-path
  '''
  global CALC_P
  if n in CALC_P.keys():
    return CALC_P[n]

  if n < 1:
    return 0
  elif n == 1:
    return p
  elif n < k + 2:
    return 1 - (1 - p) ** n
  else:
    S = p
    for i in range(0, k):
      S = S + (1 - p) ** i * p * kPath_oneSide(n - i - 2, k)
    S = S + (1 - p) ** k * p * calculate_kPath(n - k - 1, k)
    CALC_P[n] = S
    return S

def kPath_oneSide(n, k):
  '''
  Calculates the DRel of a k-path which have already k dominated vertices at his end
  @param n: Number of vertices of the k-path
  @param k: The k of the k-path
  '''
  global CALC_H
  if n in CALC_H.keys():
    return CALC_H[n]

  if n < 1:
    return 0
  elif n < k + 1:
    return 1
  elif n == k + 1:
    return 1 - (1 - p) ** (k + 1)
  else:
    S = 0
    for i in range(0, k):
      S = S + (1 - p) ** i * p * kPath_oneSide(n - i - 1, k)
    S = S + (1 - p) ** k * calculate_kPath(n - k, k)
    CALC_H[n] = S
    return S

def kPath_twoSide(n, k):
  '''
  Calculates the DRel of a k-path which have already k dominated vertices at each of his ends
  @param n: Number of vertices of the k-path
  @param k: The k of the k-path
  '''
  global CALC_F
  if n in CALC_F.keys():
    return CALC_F[n]

  if n < 1:
    return 0
  else:
    if n <= 2 * k:
      return 1
    else:
      if n == 2 * k + 1:
        return 1 - (1 - p) ** (2 * k + 1)
      else:
        S = 0
        for i in range(1, k + 1):
          S = S + (1 - p) ** (i - 1) * p * kPath_twoSide(n - i, k)
        S = S + (1 - p) ** k * kPath_oneSide(n - k, k)
        CALC_F[n] = S
        return S


# =============================================================================
# ================ k-Trees ====================================================
# =============================================================================
def calculate_kTrees(graph, vertexorder=None):
  '''
  Calculates the DRel of complete 2-trees
  @param graph: Graph
  @param vertexorder: Decomporder of the vertices
  @return: DRel
  '''
  if graph.graphtype != graphType.TWOTREE:
    MDNetwork_log.error("Wrong graphtype")
    return None

  if vertexorder == None:
    w, vertexorder = iskTree(graph)

  states = []
  current_vertices = []
  for s in vertexorder:
    # If no vertex is active, then generate an new state
    if len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 0:
      state = TreeState(s[1][0], s[1][1])
      state.first_state()
      states.append(state)
      current_vertices.append(s[1][0])
      current_vertices.append(s[1][1])

    # Two vertices are already active
    elif (len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 2
          and s[0] in current_vertices):
      if s[1][0] in current_vertices:
        u = s[1][0]
        v = s[0]
        state = None
        for st in states:
          if st.getu() == u and st.getv() == v:  # u is pivot element
            state = st
            state.nextVertexu(s[1][1])
            break
          elif st.getu() == v and st.getv() == u:  # v is pivot element
            state = st
            state.nextVertexv(s[1][1])
            break
        current_vertices.remove(s[0])
        current_vertices.append(s[1][1])
      else:
        u = s[1][1]
        v = s[0]
        state = None
        for st in states:
          if st.getu() == u and st.getv() == v:  # u is pivot element
            state = st
            state.nextVertexu(s[1][0])
            break
          elif st.getu() == v and st.getv() == u:  # v is pivot element
            state = st
            state.nextVertexv(s[1][0])
            break
        current_vertices.remove(s[0])
        current_vertices.append(s[1][0])

    # Two vertices are active
    elif s[0] not in current_vertices and (s[1][0] in current_vertices and s[1][1] in current_vertices):
      u = s[1][0]
      v = s[1][1]
      state = None
      for st in states:
        if st.getu() == u and st.getv() == v or st.getu() == v and st.getv() == u:
          state = st
          state.addBacknode()
          break

    # Three vertices are active
    elif len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 3:
      u = s[1][0]
      v = s[1][1]
      w = s[0]
      state = None
      state2 = None
      for st in states:
        if st.getu() == u and st.getv() == v or st.getu() == v and st.getv() == u:
          state = st
          n = None
          for i, st2 in enumerate(states):
            if (st2.getu() == u and st2.getv() == w or st2.getu() == w and st2.getv() == u 
                or st2.getu() == v and st2.getv() == w or st2.getu() == w and st2.getv() == v):
              state2 = st2
              n = i
              break
            current_vertices.remove(s[0])
          break

      if state != None and state2 != None:
        state.addState(state2)
        del states[n]

      if state == None:
        u = s[1][0]
        w = s[1][1]
        v = s[0]  # v=s[0] is the joint vertex of the two states
        state1 = None
        state2 = None
        n = None
        for i, st in enumerate(states):
          if st.getu() == u and st.getv() == v:
            state1 = st
          elif st.getu() == v and st.getv() == u:
            state1 = st
            state1.change()
          if st.getu() == w and st.getv() == v:
            state2 = st
            n = i
          elif st.getu() == v and st.getv() == w:
            state2 = st
            state2.change()
            n = i

        # If some of the states are None, then insert a third vertex
        if state1 == None:
          if state2.getu() == w:
            state2.nextVertexu(s[1][0])
          else:
            state2.nextVertexv(s[1][0])
          current_vertices.remove(s[0])
          if u not in current_vertices:
            current_vertices.append(s[1][0])

        elif state2 == None:
          if state1.getu() == u:
            state1.nextVertexu(s[1][1])
          else:
            state1.nextVertexv(s[1][1])
          current_vertices.remove(s[0])
          if w not in current_vertices:
            current_vertices.append(s[1][1])

        # If the two states exists, then join them
        else:
          state1.merge(state2)
          current_vertices.remove(s[0])
          del states[n]

    # Only one vertex is active
    else:
      state = TreeState(s[1][0], s[1][1])
      state.first_state()
      states.append(state)
      w = list(set([s[1][0], s[1][1]]).difference(set(current_vertices)))[0]
      current_vertices.append(w)

  if len(states) > 1:
    MDNetwork_log.error("something wrong: len(states) = %d" % len(states))
    return 0

  # Insert the last vertex
  state = states[0]
  v = list(set(graph.getV()) - set(current_vertices))[0]
  state.nextVertexv(v)

  return state.getRel()

def calculate_kStar(graph, k):
  '''
  Calculate the DRel of an n,k-star
  @param graph: n,k-star
  @param k: The k of the n,k-star
  @return: DRel
  '''
  return 1 - (1 - p) ** k * (1 - p ** (graph.order() - k))

class TreeState():
  '''
  Class for a state for the calculation of a k-tree
  '''
  def __init__(self, u=None, v=None):
    self.P = []
    self.u = u
    self.v = v

  def first_state(self):
    '''
    Generate the first state
    '''
    if len(self.P) != 0:
      MDNetwork_log.error("Use this function only before the calculation!!!")
      return False

    self.P = [0 for _ in range(1, 8)]
    self.P[0] = p ** 2
    self.P[1] = p * (1 - p)
    self.P[2] = p * (1 - p)
    self.P[3] = p - p ** 2 * 2 + p ** 3
    self.P[4] = 0
    self.P[5] = 0
    self.P[6] = 0

    return True

  def getu(self):
    return self.u

  def getv(self):
    return self.v

  def change(self):
    '''Change u and v in the state'''
    tmp = self.u
    self.u = self.v
    self.v = tmp

    tmp = deepcopy(self.P[1])
    self.P[1] = deepcopy(self.P[2])
    self.P[2] = deepcopy(tmp)

    tmp = deepcopy(self.P[5])
    self.P[5] = deepcopy(self.P[6])
    self.P[6] = deepcopy(tmp)

  def nextVertexu(self, new_v):
    '''Replace v with new_v'''
    P_tmp = [0 for _ in range(1, 8)]
    P_tmp[0] = p * (self.P[0] + self.P[1])
    P_tmp[1] = (1 - p) * (self.P[0] + self.P[1])
    P_tmp[2] = p * (self.P[2] + self.P[3] + self.P[4] + self.P[5] + self.P[6])
    P_tmp[3] = (1 - p) * self.P[2]
    P_tmp[4] = (1 - p) * self.P[6]
    P_tmp[5] = (1 - p) * self.P[3]
    P_tmp[6] = 0

    self.P = deepcopy(P_tmp)
    self.v = deepcopy(new_v)

  def nextVertexv(self, new_u):
    '''Replace u with new_u'''
    P_tmp = [0 for _ in range(1, 8)]
    P_tmp[0] = p * (self.P[0] + self.P[2])
    P_tmp[1] = p * (self.P[1] + self.P[3] + self.P[4] + self.P[5] + self.P[6])
    P_tmp[2] = (1 - p) * (self.P[0] + self.P[2])
    P_tmp[3] = (1 - p) * self.P[1]
    P_tmp[4] = (1 - p) * self.P[5]
    P_tmp[5] = 0
    P_tmp[6] = (1 - p) * self.P[3]

    self.P = deepcopy(P_tmp)
    self.u = deepcopy(new_u)

  def merge(self, other):
    '''Merge two states'''
    P2 = deepcopy(other.P)

    P_tmp = [0 for _ in range(1, 8)]
    P_tmp[0] = (self.P[0] * P2[0]).poly_div(p) + (self.P[1] * P2[1]) / ((1 - p))
    P_tmp[1] = (self.P[0] * P2[2]).poly_div(p) + (self.P[1] * (P2[3] + P2[4] + P2[5] + P2[6])) / ((1 - p))
    P_tmp[2] = (self.P[2] * P2[0]).poly_div(p) + (P2[1] * (self.P[3] + self.P[4] + self.P[5] + self.P[6])) / ((1 - p))
    P_tmp[3] = (self.P[2] * P2[2]).poly_div(p) + (self.P[3] * (P2[3] + P2[5])) / ((1 - p)) \
                + (self.P[5] * P2[3]) / ((1 - p))
    P_tmp[4] = (self.P[4] * P2[6] + self.P[6] * (P2[4] + P2[6])) / ((1 - p))
    P_tmp[5] = (self.P[5] * P2[6] + self.P[3] * (P2[4] + P2[6])) / ((1 - p))
    P_tmp[6] = (self.P[4] * P2[3] + self.P[6] * (P2[3] + P2[5])) / ((1 - p))

    self.P = deepcopy(P_tmp)
    self.v = deepcopy(other.getu())

  def addBacknode(self):
    '''Insert a back vertex'''
    self.P[3] = (self.P[3] + self.P[4] + self.P[5] + self.P[6]) * p
    self.P[4] = 0
    self.P[5] = 0
    self.P[6] = 0

  def addState(self, other):
    '''
    Merge two states, which have the same active vertex set
    '''
    if self.getv() == other.getv():
      pass
    elif self.getu() == other.getu():
      self.change()
      other.change()
    elif self.getu() == other.getv():
      self.change()
    else:
      other.change()

    P_tmp = [0 for _ in range(1, 8)]
    P_tmp[0] = (self.P[0] * (other.P[0] + other.P[2])) / (p)
    P_tmp[1] = (self.P[1] * (other.P[1] + other.P[3] + other.P[4] + other.P[5] + other.P[6])) / ((1 - p))
    P_tmp[2] = (self.P[2] * (other.P[0] + other.P[2])) / (p)
    P_tmp[3] = (self.P[3] * (other.P[1] + other.P[3] + other.P[5]) + self.P[5] * (other.P[1] + other.P[3]) \
                + (self.P[4] + self.P[6]) * other.P[1]) / ((1 - p))
    P_tmp[4] = (self.P[4] * other.P[5]) / ((1 - p))
    P_tmp[5] = (self.P[5] * other.P[5]) / ((1 - p))
    P_tmp[6] = ((self.P[4] + self.P[6]) * other.P[3]) / ((1 - p))

    self.P = deepcopy(P_tmp)

  def getRel(self):
    '''Return the reliability of this state'''
    return self.P[0] + self.P[1] + self.P[2] + self.P[3]

  def getRel_final(self):
    '''Calculate the reliability of the final state'''
    return self.P[0] + self.P[1] + self.P[2] + p * self.P[3]

  def print_me(self):
    '''Print the state'''
    s = "u: %d, v: %d -> %s" % (self.u, self.v, self.getRel())
    print(s)

  def print_me_details(self):
    '''Print the state with details'''
    for i in range(0, 7):
      print("P%d: %s" % (i + 1, str(self.P[i])))


# =============================================================================
# ================ Tree-Decomposition =========================================
# =============================================================================
def tree_decomposition(graph):
  pass

class DecompState():
  def __init__(self, A=set([]), B=set([])):
    self.A = A
    self.B = B
    self.p = 1

  def __eq__(self, other):
    '''
    Compare two states
    '''
    if self.A == other.A and self.B == other.B:
      return True

    return False

  def activate(self, v):
    '''
    Activate a vertex
    @param v: Vertex
    '''
    pass

  def deactivate(self, v):
    '''
    Deactivate a vertex
    @param v: Vertex
    '''
    try:
      self.A.remove(v)
      self.B.remove(v)
    except:
      self.print_me()
      sys.exit("something is wrong")

  def process_edge(self, edge, t):
    '''
    Process an edges
    @param edge: Edge of the graph
    '''
    if edge[0] in self.A and not edge[1] in self.B:
      self.B.add(edge[1])
    elif edge[1] in self.A and not edge[0] in self.B:
      self.B.add(edge[0])

  def copy(self):
    '''
    Return a copy
    '''
    other = DecompState(self.A, self.B)
    other.p = self.p
    return other

  def print_me(self):
    '''
    Print the information of this state
    '''
    print("%s, %s > %s" % (self.A, self.B, self.p))


# =============================================================================
# =========== SPECIAL GRAPH CLASSES ===========================================
# =============================================================================
def calcPath(n):
  '''Calculate the domination polynomial of a path'''
  from collections import deque
  poly = deque([x, x ** 2 + 2 * x, x ** 3 + 3 * x ** 2 + x])
  if n <= 0:
    return 0
  elif n <= 3:
    return poly[n - 1]

  i = 3
  #for _ in range(4, n + 1):
  while i != n:
    i += 1
    poly.append(x * (poly[0] + poly[1] + poly[2]))
    poly.popleft()

  return poly[2]

def calcCycle(n):
  '''Calculate the domination polynomial of a cycle'''
  from collections import deque
  poly = deque([x, x ** 2 + 2 * x, x ** 3 + 3 * x ** 2 + 3 * x])
  if n <= 0:
    return 0
  elif n <= 3:
    return poly[n - 1]

  i = 3
  while i != n:
    i += 1
    poly.append(x * (poly[0] + poly[1] + poly[2]))
    poly.popleft()

  return poly[2]

def calcFriendship(n):
  '''Calculate the domination polynomial of the friendship graph'''
  wings = (n - 1) / 2
  return (2 * x + x**2)**wings + x * (1 + x)**(2 * wings)

def calcBook(n):
  '''Calculate the domination polynomial of the book graph'''
  pages = n / 2  - 1
  return (x**2 + 2 * x)**pages * (2 * x + 1) + x**2 * (x + 1)**(2 * pages) - 2 * x**pages


# =============================================================================
# =============================================================================
# =============================================================================
def reltodom(DRel):
  '''Calculate the domination polynomial from the DRel'''
  n = degree(DRel, gen=p)
  return (1 + x) ** n * DRel.subs(p, x / (x + 1))

def domtorel(D):
  '''Calculate the DRel from the domination polynomial'''
  n = degree(D, gen=x)
  return (1 - p) ** n * D.subs(x, p / (1 - p))


# =============================================================================
# =============================================================================
# =============================================================================
def compute_reliability(MDNetwork_log2, graph, m=None, q=None):
  '''
  Calculate the domination reliability polynomial DRel(G, p)
  @param graph: Graph
  @param m: Method
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("starting reliability computing...")
  MDNetwork_log.info("")

  rel = None
  if m == None and graph.graphtype == None:
    if isPath(graph):
      MDNetwork_log.info("recognized path ...")
      rel = domtorel(calcPath(graph.order()))
    elif isCycle(graph):
      MDNetwork_log.info("recognized cycle ...")
      rel = domtorel(calcCycle(graph.order()))

    else:
      k = min(graph.get_degrees().values())
      _, vertexorder = iskTree(graph, k)
      if vertexorder != False:
        if iskStar(graph, k):
          MDNetwork_log.info("graph is %d-star" % k)
          rel = calculate_kStar(graph, k)
        elif k == 2:
          MDNetwork_log.info("graph is 2-tree")
          graph.graphtype = graphType.TWOTREE
          rel = calculate_kTrees(graph, vertexorder)

  elif m == None:
    if graph.graphtype == graphType.PATH:
      MDNetwork_log.info("recognized path ...")
      rel = domtorel(calcPath(graph.order()))
    elif graph.graphtype == graphType.CYCLE:
      MDNetwork_log.info("recognized cycle ...")
      rel = domtorel(calcCycle(graph.order()))
    elif graph.graphtype == graphType.FRIENDSHIP:
      MDNetwork_log.info("recognized friendship graph ...")
      rel = domtorel(calcFriendship(graph.order()))
    elif graph.graphtype == graphType.BOOK:
      MDNetwork_log.info("recognized book graph ...")
      rel = domtorel(calcBook(graph.order()))

  if rel:
    if q != None:
      q.put(rel)
    return simplify(rel)

  if m == method.STATE_SPACE or m == None:
    MDNetwork_log.info("starting complete enumeration...")
    rel = state_space_enumeration(graph)

  elif m == method.DECOMP:
    MDNetwork_log.info("starting decomposition...")
    rel = domtorel(domPoly(graph, set(graph.getV()), set(graph.getV())))

  elif m == method.TREE_DECOMP:
    MDNetwork_log.info("starting tree decomposition...")
    rel = tree_decomposition(graph)

  elif m == method.CLIQUE_DECOMP:
    MDNetwork_log.info("starting clique decomposition...")
    rel = clique_decomposition(graph)

  elif m == method.KTREE:
    try:
      _, vertexorder = iskTree(graph)
      if vertexorder != False:
        graph.graphtype = graphType.TWOTREE
        rel = calculate_kTrees(graph, vertexorder)
      else:
        raise GraphClassError("2-tree")
    except GraphClassError as e:
      return str(e)

  elif m == method.SKPATH:
    try:
      k = isskPath(graph)
      if k == 0:
        raise GraphClassError("simple k-path")

      rel = calculate_kPath(len(graph.getV()), k)
    except GraphClassError as e:
      return str(e)

  elif m == method.SKCYCLE:
    try:
      k = iskCycle(graph)
      if k == 0:
        raise GraphClassError("k-cycle")

      rel = calculate_kCycle(graph, k)
    except GraphClassError as e:
      return str(e)

  elif m == method.ESSENTIAL:
    MDNetwork_log.info("starting computing using essential sets...")
    rel = domtorel(essential_calculation(graph))

  else:
    try:
      raise NotImplementedError
    except NotImplementedError as e:
      return str(e)

  if q != None:
    q.put(rel)
  return simplify(rel)

def compute_poly(MDNetwork_log, graph, m=None, q=None):
  '''Calculates the domination polynomial of the graph'''
  D = None
  if m == None and graph.graphtype == None:
    if isPath(graph):
      MDNetwork_log.info("recognized path ...")
      D = calcPath(graph.order())
    elif isCycle(graph):
      MDNetwork_log.info("recognized cycle ...")
      D = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.PATH:
      MDNetwork_log.info("recognized path ...")
      D = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      MDNetwork_log.info("recognized cycle ...")
      D = calcCycle(graph.order())

  if D:
    if q != None:
      q.put(D)
    return simplify(D)

  if m == method.DECOMP:
    MDNetwork_log.info("starting decomposition...")
    D = simplify(domPoly(graph, set(graph.getV()), set(graph.getV())))
    if q != None:
      q.put(D)
    return D

  elif m == method.ESSENTIAL:
    MDNetwork_log.info("starting computing using essential sets...")
    D = simplify(essential_calculation(graph))
    if q != None:
      q.put(D)
    return D

  else:
    D = simplify(reltodom(compute_reliability(MDNetwork_log, graph, m, None)))
    if q != None:
      q.put(D)
    return D


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")