#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
Calculation of the edge domination reliability polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 26.10.2012

@author: Markus Dod
@license: GPL (see LICENSE.txt)

TODO:
- Dekomposition EDRel(G,A,d=1(?)) rechnet net richtig
'''

from copy import deepcopy
import sympy
import sys

from lib.tools import (kpowerset, minkpowerset, is_subset, binomial, powerset,  # @UnresolvedImport
                       arg, get_values)  # @UnresolvedImport

from SGraph.GraphFunctions import (find_shortest_path, shortestPath,  # @UnresolvedImport
                                   isComplete, components)  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport


# =============================================================================
# =============================================================================
num_processes = 8

global_A = None
global_B = None
global_graph = None
global_d = 0

p = sympy.symbols('p')

# =============================================================================
# ============= COMPLETE ENUMERATION ==========================================
# =============================================================================
def compute_with_art(graph, A, d, articulation):
  g = graph.copy()
  g.deleteVertex(articulation)
  C = components(g)
  if set(A).issubset(C[0].getV()):
    d_hat = 0
    for u in C[1].getV():
      _, length = shortestPath(graph, articulation, u)
      if length > d_hat:
        d_hat = length

    EDRel = 0
    g = graph.copy()
    g.deleteVertexSet(C[1].getV())
    p, f = shortestPath(graph, articulation, A)
    graph.deleteVertexSet(C[0].getV())
    for e in range(f, d - d_hat + 1):
      EDRel = EDRel + state_space_enumeration(g, A, d, {e:[articulation]}) * state_space_enumeration(graph, [articulation], d - e)

  elif set(A).issubset(C[1]):
    pass
  else:
    state_space_enumeration(graph, A, d)

  return EDRel

def state_space_enumeration(graph, A, d=1, cond={}):
  '''
  Calculates EDRel(G,A,p) and EDRel(G,A,d,p)
  @param graph: Graph
  @param A: Dominating vertices
  @param d: Domination distance
  '''
  EDRel = None

  if d == 1:
    graph = reduce_graph(graph, A, d)
    B = [v for v in graph.getV() if v not in A]
    EDRel = 1
    for v in B:
      EDRel = EDRel * (1 - (1 - p) ** len(graph.neighbors(v)))

  elif d > 1 and len(cond.keys()) == 0:
    if A == graph.getV():
      return 1

    # graph = reduce_graph(graph, A, d)
    EDRel = 0
    B = graph.open_neighbors_set(A)
    pset = minkpowerset(B, 1)
    for W in pset:
      EDRel1 = 1
      for v in W:
        EDRel1 = EDRel1 * (1 - (1 - p) ** len(graph.neighbors(v) & set(A)))

      EDRel2 = 1
      for v in B - set(W):
        EDRel2 = EDRel2 * (1 - p) ** len(graph.neighbors(v) & set(A))

      g = graph.copy()
      g.deleteVertexSet(A)
      EDRel = EDRel + EDRel1 * EDRel2 * state_space_enumeration(g, deepcopy(W), d - 1)

  else:
    EDRel = 0
    B = [v for v in graph.getV() if v not in A]
    edgesets = minkpowerset(graph.getEdgeSet(), 1)

    m = len(graph.getEdgeSet())
    for eset in edgesets:
      flag = False
      g = graph.inducedSubgraph(eset)
      for v in B:
        flag = False
        _, length = find_shortest_path(g, v, A)
        if v in get_values(cond) and arg(cond, v) != length:
          break
        elif length != None and length <= d:
          flag = True
        else:
          break

      if flag:
        pow1 = m - len(eset)
        EDRel = EDRel + (p ** len(eset)) * ((1 - p) ** pow1)

  return EDRel

"""def calc_poly_EDRelA(eset):
  '''
  Funktion zur parallelen Berechnung von EDRel(G,A,p) bzw. EDRel(G,A,d,p)
  @param eset: Element der Potenzmenge der Kantenmenge (Zustand)
  '''
  global global_A, global_B, global_d, global_graph

  EDRel = 0
  m = len(global_graph.getEdgeSet())

  flag = False
  kPath_oneSide = global_graph.inducedSubgraph(eset)
  for v in global_B:
    flag = False
    for w in global_A:
      path, length = shortestPath(kPath_oneSide, v, w)
      if length != None and length <= global_d:
        flag = True
        break
    if not flag:
      break

  if flag:
    pow1 = m - len(eset)
    EDRel = EDRel + (p ** len(eset)) * ((1 - p) ** pow1)

  return EDRel"""

def decomposition(graph, A, d, intact=set([])):
  C = components(graph)
  for c in C:
    if len(set(A).intersection(set(c.getV()))) == 0:
      return 0

  if len(intact) == graph.size():
    for v in graph.getV():
      _, length = find_shortest_path(graph, v, A)
      if v not in A and length == None or length > d:
        return 0
    return 1

  F = set([(u, v) for u in A for v in graph.neighbors(u)]) - intact
  if len(F) != 0:
    e = F.pop()
  else:
    edges = graph.getEdgeSet() - intact
    e = edges.pop()
  g = graph.copy()
  g.deleteEdge(e)

  intact2 = deepcopy(intact)
  intact2.add(e)
  return (1 - p) * decomposition(g, A, d, intact) + p * decomposition(graph, A, d, intact2)


# =============================================================================
def state_space_enumeration2(graph, k, A=None, B=None):
  '''
  Calculates EDRel(G,k,p) and EDRel(G,A,B,k,p)
  @param graph: Graph
  @param k: Order of the dominating set
  @param A: Vertices which can be dominating
  @param B: Vertices which should be dominated
  '''
  if A == None:
    A = deepcopy(graph.getV())
  if B == None:
    B = deepcopy(graph.getV())

  EDRel = 0
  edgesets = powerset(list(graph.getEdgeSet()))
  kset = kpowerset(A, k)
  n = graph.order()
  m = graph.size()
  for eset in edgesets:
    g = graph.spanningSubgraph(eset)
    flag = False
    for vset in kset:
      if len(g.neighbors_set(vset)) == n:
        flag = True
        break

    if flag:
      pow1 = m - len(eset)
      EDRel = EDRel + (p ** len(eset)) * ((1 - p) ** pow1)

  return EDRel

def decomp(graph, k, eset=set([]), edges=None):
  if edges == None:
    edges = graph.getEdgeSet()

  edges = deepcopy(edges)
  eset = deepcopy(eset)

  if len(edges) == 0:
    kset = kpowerset(graph.getV(), k)
    g = graph.spanningSubgraph(eset)
    for vset in kset:
      if len(g.neighbors_set(vset)) == g.order():
        return 1
    return 0
  else:
    e = edges.pop()
    eset2 = deepcopy(eset)
    eset.add(e)
    g = graph.copy()
    g.deleteEdge(e)
    return p * decomp(graph, k, eset, edges) + (1 - p) * decomp(g, k, eset2, edges)

def state_space_enumeration_impr(graph, k, A=None, B=None):
  if A == None:
    A = deepcopy(graph.getV())
  if B == None:
    B = deepcopy(graph.getV())

  tmp = graph.neighbors_set(B)
  A = list(set(A).intersection(tmp))

  kset = minkpowerset(A, k)
  del_nodes = []
  for v in graph.getV():
    if v not in A and v not in B:
      del_nodes.append(v)
  g = graph.copy()
  for v in del_nodes:
    g.deleteVertex(v)

  m = len(g.getEdgeSet())
  edgesets = minkpowerset(g.getEdgeSet(), 1)

  EDRel = 0
  for eset in edgesets:
    flag = False
    kPath_oneSide = g.inducedSubgraph(eset)
    for vset in kset:
      neighbors = list(kPath_oneSide.neighbors_set(vset))
      if is_subset(B, neighbors):
        flag = True
        break
    if flag:
      pow1 = m - len(eset)
      EDRel = EDRel + (p ** len(eset)) * ((1 - p) ** pow1)

  return EDRel


# =============================================================================
def reduce_graph(graph, A, d):
  '''
  Apply reductions of the graph (only for EDRel(G,A,d,p))
  '''
  E = graph.getEdgeSet()
  del_edges = []

  if d == 1:
    for e in E:
      if (e[0] in A and e[1] in A) or (e[0] not in A and e[1] not in A):
        del_edges.append(e)

  else:
    for e in E:
      if e[0] in A and e[1] in A:
        del_edges.append(e)
      else:
        p, l1 = find_shortest_path(graph, e[0], A, d)
        p, l2 = find_shortest_path(graph, e[1], A, d)
        if l1 == d and l2 == d:
          del_edges.append(e)

  for e in del_edges:
    graph.deleteEdge(e)

  return graph


# =============================================================================
# =============================================================================
# =============================================================================
def compute_reliability(MDNetwork_log2, graph, m, A=None, d=None, k=None, q=None):
  '''
  Compute the edge domination reliability
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("starting reliability computing...")
  MDNetwork_log.info("")

  rel = None

  if A and not k:  # EDRel(G,A,p) and EDRel(G,A,d,p)
    if m == method.STATE_SPACE or m == None or m== method.AUTOMATIC:
      graph = reduce_graph(graph, A, d)
  
      if d == 1 and len(graph.neighbors_set(A)) != graph.order():
        return "0"
  
      if d > 1:
        B = [v for v in graph.getV() if v not in A]
        for v in B:
          _, length = find_shortest_path(graph, v, A)
          if length > d:
            return "0"
  
      # articulation = find_artikulations(graph)
      # articulation = []
      # if len(articulation) != 0:
      #  rel = compute_with_art(graph, A, d, articulation[0])
      # else:
      rel = state_space_enumeration(graph, A, d)
  
    elif m == method.DECOMP:
      rel = decomposition(graph, A, d, set([(2, 4)]))

  elif k:  # EDRel(G,k,p)
    if m == method.STATE_SPACE or m == None or m== method.AUTOMATIC:
      if k == 1 and isComplete(graph):
        n = graph.order()
        m = graph.size()

        rel = 0
        for j in range(1, n + 1):
          f = binomial(n, j) * (-1) ** (j + 1)
          rel = rel + f * p ** (j * (n - j) + binomial(j, 2))

      else:
        rel = state_space_enumeration_impr(graph, k)

    elif m == method.DECOMP:
      rel = decomp(graph, k)

    else:
      pass

  if q != None:
    q.put(rel)
  return sympy.simplify(rel)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
