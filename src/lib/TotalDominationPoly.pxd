'''
Calculats the total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
'''
import cython

cdef compute_polynomial(graph)
cdef compute_polynomial2(graph)
cdef compute_polynomial3(graph)
cdef compute_polynomial4(graph)
cdef edgesum(graph, X=*, Y=*)

@cython.locals(v=cython.int)
cdef is_total_dom(graph, W)

@cython.locals(k=cython.int)
cdef compute_inclusion_exclusion(graph)

@cython.locals(k=cython.int)
cdef essential_calculation(graph)

@cython.locals(v=cython.int, i=cython.int, d=cython.dict)
cdef essential_calculation_fast(graph)

@cython.locals(i=cython.int, n=cython.int, d=cython.dict, v=cython.int,
               flag=cython.bint)
cdef numberofessentials(graph)

@cython.locals(k=cython.int, v=cython.int)
cdef essential_calculation_fast2(graph):

@cython.locals(k=cython.int, u=cython.int)
cdef is_essential(graph, W)

@cython.locals(v=cython.int, w=cython.int)
cdef recSolve(graph, X=*, Y=*)

cdef compute_polynomialXY(graph, X, Y)

@cython.locals(v=cython.int)
cdef is_total_domXY(graph, W, Y)

cdef calcCompleteGraph(int n)

@cython.locals(v=cython.int, m=cython.int, n=cython.int)
cdef calcCompleteBipartiteGraph(graph)

@cython.locals(i=cython.int)
cdef calcPath(n)

@cython.locals(i=cython.int)
cdef reltodom(DRel)

@cython.locals(i=cython.int)
cdef domtorel(D)

cpdef compute_poly(MDNetwork_log2, graph, m=*, q=*)
cpdef compute_reliability(MDNetwork_log2, graph, m=*, q=*)

