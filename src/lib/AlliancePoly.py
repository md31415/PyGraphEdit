#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Some methods to calculate the alliance polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 19.05.2016

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import math
import sys

import sympy

from lib.tools import powerset, binomial  # @UnresolvedImport

from SGraph.GraphFunctions import (isCycle, isPath, connected,  # @UnresolvedImport
                                   inducedSubgraph, getBipartition,  # @UnresolvedImport
                                   isCompleteBipartite, isComplete)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x = sympy.symbols('x')


# =============================================================================
def compute_polynomial(graph):
  '''
  Compute the alliance polynomial
  @param graph: SGraph
  '''
  n = graph.order()
  A = 0
  for W in powerset(graph.getV()):
    if len(W) > 0 and connected(inducedSubgraph(graph, set(W))):
      values = []
      for v in W:
        N = set(graph.neighbors(v))
        m = len(N.intersection(set(W)))
        values.append(m - (len(N) - m))
      A = A + x ** (n + min(values))

  return A


# =============================================================================
# ============== SPECIAL GRAPHS ===============================================
# =============================================================================
def calcCompleteGraph(n):
  '''
  Calculate the alliance polynomial of a complete graph
  '''
  return ((x**2 + 1)**n - 1) / x

def calcStar(n):
  '''
  Calculate the alliance polynomial of the star
  @param n: Order of the star
  '''
  poly = (n-1) * x**(n-1)
  for k in range(math.floor((n-1)/2) + 1):
    poly += binomial(n - 1, k) + x**(2*k+1)
  for k in range(math.ceil(n/2), n):
    poly += binomial(n - 1, k)
  return poly

def calcCompleteBipartite(graph):
  '''
  Calculate the alliance polynomial of a complete bipartite graph
  '''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1

  poly = n * x**n + m * x**m
  for k in range(2, n + m + 1):
    for i in range(n + 1):
      poly += binomial(n, i) * binomial(m, k-i) * x**(n+m+min([2*i-n, 2*(k-i)-m]))
  return poly

def calcPath(n):
  '''
  Calculate the alliance polynomial of the path
  @param n: Order of the path
  '''
  return (n-2) * x**(n-2) + 2 * x**(n-1) + (n-2)*(n+1)/(2) * x**n + x**(n+1)

def calcCycle(n):
  '''
  Calculate the matching polynomial of the cycle
  @param n: Order of the cycle
  '''
  return n * x**(n-2) + n * (n-2) * x**n + x**(n+2)


# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Compute the alliance polynomial
  @param graph: Graph
  @param m: Method
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  A = None
  # If no method is given, then test if the graph is in a special graph class
  if m is None and graph.graphtype is None:
    # Look if the graph is in a special graph class
    if isPath(graph):
      PyGraphEdit_log.info("identified a path...")
      A = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("identified a cycle...")
      A = calcCycle(graph.order())
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("identified a complete bipartite graph ...")
      A = calcCompleteBipartite(graph)
    elif isComplete(graph):
      PyGraphEdit_log.info("identified a complete graph ...")
      A = calcCompleteGraph(graph.order())

  elif m is None:
    if graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("identified complete star ...")
      A = calcStar(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("identified a complete bipartite graph ...")
      A = calcCompleteBipartite(graph)
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("identified a path ...")
      A = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("identified a cycle  ...")
      A = calcCycle(graph.order())
    elif graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("identified a complete graph ...")
      A = calcCompleteGraph(graph.order())

  if A:
    if q != None:
      q.put(A)
    return A

  if m == method.STATE_SPACE or m is None:
    PyGraphEdit_log.info("starting complete enumeration...")
    A = compute_polynomial(graph)

  if q != None:
    q.put(A)
  return A


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
