#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Implementation of different graph polynomials

Copyright (c) 2013-2017 Markus Dod, Peter Tittmann

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 27.05.2013

@author: Peter Tittmann, Markus Dod
'''

from sympy import symbols

from lib.tools import powerset  # @UnresolvedImport

from SGraph.GraphFunctions import connected, components, inducedSubgraph  # @UnresolvedImport
from SGraph.GraphFunctions import isBipartite, completeGraph  # @UnresolvedImport


# =============================================================================
# =============================================================================
p = symbols('p')
x, y, z, w = symbols('x y z w')


# =============================================================================
# =============================================================================
def SubgraphPoly(G, q=None):
  """The subgraph polynomial is the generating function for the number
  of connected vertex induced subgraphs with respect to the number
  of vertices"""
  def NGraph(G, v):
    N = list(G.neighbors(v))
    n = len(N)
    H = G - v
    for i in range(0, n - 1):
      for j in range(i + 1, n):
        H.insertEdge(N[i], N[j])
    return H

  def SGP(G):
    if G.order() == 0:
      return 0
    elif G.order() == 1:
      return x
    elif not connected(G):
      S = 0
      for H in components(G):
        S = SGP(H) + S
      return S
    else:
      m = -1
      for u in G.getVIt():
        d = G.degree(u)
        if d > m:
          v, m = u, d
      return x * (SGP(NGraph(G, v)) - SGP(G - (G.neighbors(v) | {v}))) + x + SGP(G - v)

  S = SGP(G)
  if q != None:
    q.put(S)
  return S

def SubgraphEnumeratingPoly(graph, q=None):
  u, v, p = symbols('u v p')
  P = 0
  n = graph.order()

  for F in powerset(list(graph.getEdgeSet())):
    g = graph.spanningSubgraph(F)
    iso = 0
    deg = sorted(list(graph.get_degrees().values()))
    for d in deg:
      if d == 0:
        iso += 1
      else:
        break

    k = len(components(g))
    P = u ** (n - iso) * v ** len(F) * p ** k + P

  if q != None:
    q.put(P)
  return P

def ExtendedSubgraphCountingPoly(graph, q=None):
  v, x, y, z = symbols('v x y z')
  S = 0
  for W in powerset(graph.getV()):
    G = inducedSubgraph(graph, set(W))
    n = G.order()
    G_edges = list(G.getEdgeSet())
    for F in powerset(G_edges):
      subgraph = G.copy()
      for e in G_edges:
        if e not in F:
          subgraph.deleteEdge(e)

      k = len(components(subgraph))
      S = v ** n * z ** (k - subgraph.numIso()) * y ** len(F) * x ** k + S

  if q != None:
    q.put(S)
  return S

def SubgraphComponentPoly(graph, q=None):
  '''
  Calculation of the subgraph component polynomial
  @param graph: Graph
  '''
  def SGC(graph):
    x, y = symbols('x y')
    if graph.order() == 0:
      return 1
    elif graph.order() == 1:
      return 1 + x * y
    elif graph.size() == 0:
      return (1 + x * y) ** graph.order()
    elif not connected(graph):
      S = 0
      for H in components(graph):
        S = SGC(H) + S
      return S
    else:
      m = -1
      for u in graph.getVIt():
        d = graph.degree(u)
        if d > m:
          v, m = u, d
      return SGC(graph - v) + x * (y - 1) * SGC(graph - graph.neighbors_set([v]))\
              + x * SGC(graph / v)

  S = SGC(graph)

  if q != None:
    q.put(S)
  return S

def GeneralSubgraphCountingPoly(graph, q=None):
  v, x, y, z = symbols('v x y z')
  S = 0
  for W in powerset(graph.getV()):
    G = inducedSubgraph(graph, set(W))
    n = G.order()
    m = G.size()
    G_edges = list(G.getEdgeSet())
    for F in powerset(G_edges):
      subgraph = G.copy()
      for e in G_edges:
        if e not in F:
          subgraph.deleteEdge(e)

      S = v ** n * z ** m * y ** len(F) * x ** len(components(subgraph)) + S

  if q != None:
    q.put(S)
  return S

def Reliability(graph, q=None):
  n = graph.order()
  R = 0
  for W in powerset(list(graph.getV())):
    g = inducedSubgraph(graph, set(W))
    if g.order() >= 1 and connected(g):
      R = p ** (len(W)) * (1 - p) ** (n - len(W)) + R

  if q != None:
    q.put(R)
  return R

def GeneralDomPoly(graph):
  '''
  Computes the general domination polynomial
  @type graph: SGraph
  @param graph: Graph
  '''
  P = 0
  for W in powerset(graph.getV()):
    P2 = 1
    for v in graph.open_neighbors_set(W):
      P2 *= y * ((1 + z) ** len(set(graph.neighbors(v)) & set(W)) - 1) + 1

    P += x ** len(W) * w ** (inducedSubgraph(graph, set(W)).numIso()) * P2

  return P

def computeCutPoly(graph, q=None):
  '''
  Computes the cut polynomial
  '''
  C = 0
  for W in powerset(graph.getV()):
    C = C + z ** len(graph.delta(W))

  C = 1/2 * C
  if q != None:
    q.put(C)
  return C

def computeBipartiteSubgraphPoly(graph, q=None):
  '''
  Computes the bipartite subgraph polynomial
  '''
  B = 1
  for F in powerset(list(graph.getEdgeSet())):
    if isBipartite(graph.spanningSubgraph(F)):
      B = B + x ** len(F)

  if q != None:
    q.put(B)
  return B


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  # G = SGraph(V=[1,2,3,4,5,6,7,8], E=[[1,2],[1,3],[2,3],[2,4],[3,4],[4,8],[8,5],[8,6],[5,6],[5,7],[6,7]])
  G = completeGraph(5)

  print(SubgraphPoly(G))
  print(GeneralSubgraphCountingPoly(G))
