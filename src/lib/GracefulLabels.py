'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 03.12.2016

@author: markus
'''

import itertools

def isGraceful(graph, labels):
  useLabels = set(labels)
  for e in graph.getEdgeSet():
    diff = abs(labels[e[0]-1] - labels[e[1]-1])
    if diff not in useLabels:
      useLabels.add(diff)
    else:
      return False
  return True

def testLabeling(graph, start=1):
  n = graph.order()
  m = graph.size()

  for labels in itertools.permutations(list(range(start, n+m+1)), n):
    if isGraceful(graph, labels):
      return labels

  return []


# =============================================================================
# =============================================================================
# =============================================================================
def compute_labels(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate a graceful labeling of the graph
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  labeling = testLabeling(graph)

  if q != None:
    q.put(labeling)
  return labeling


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
