#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

import sympy

from SGraph.GraphFunctions import (components, inducedSubgraph)  # @UnresolvedImport
from SGraph.SpecialGraphs import (complete, cycle, path)  # @UnresolvedImport

from lib.ConnectedDominationPoly import compute_polynomial, recurrence, path_decomposition

x = sympy.symbols('x')

def test_compute_polynomial():
  assert compute_polynomial(complete(4)) == calcCompleteGraph(4)
  assert compute_polynomial(complete(5)) == calcCompleteGraph(5)
  assert compute_polynomial(complete(6)) == calcCompleteGraph(6)

  assert compute_polynomial(path(4)) == calcPath(4)
  assert compute_polynomial(path(5)) == calcPath(5)
  assert compute_polynomial(path(6)) == calcPath(6)
  assert compute_polynomial(path(7)) == calcPath(7)
  assert compute_polynomial(path(8)) == calcPath(8)

  assert compute_polynomial(cycle(4)) == calcCycle(4)
  assert compute_polynomial(cycle(5)) == calcCycle(5)
  assert compute_polynomial(cycle(6)) == calcCycle(6)
  assert compute_polynomial(cycle(7)) == calcCycle(7)
  assert compute_polynomial(cycle(8)) == calcCycle(8)

def test_recurrence():
  assert recurrence(complete(4)) == calcCompleteGraph(4)
  assert recurrence(complete(5)) == calcCompleteGraph(5)
  assert recurrence(complete(6)) == calcCompleteGraph(6)

  assert recurrence(path(4)) == calcPath(4)
  assert recurrence(path(5)) == calcPath(5)
  assert recurrence(path(6)) == calcPath(6)
  assert recurrence(path(7)) == calcPath(7)
  assert recurrence(path(8)) == calcPath(8)

  assert recurrence(cycle(4)) == calcCycle(4)
  assert recurrence(cycle(5)) == calcCycle(5)
  assert recurrence(cycle(6)) == calcCycle(6)
  assert recurrence(cycle(7)) == calcCycle(7)
  assert recurrence(cycle(8)) == calcCycle(8)

def test_path_decomposition():
  assert path_decomposition(path(4)) == calcPath(4)
  assert path_decomposition(path(5)) == calcPath(5)
  assert path_decomposition(path(6)) == calcPath(6)
  assert path_decomposition(path(7)) == calcPath(7)
  assert path_decomposition(path(8)) == calcPath(8)
