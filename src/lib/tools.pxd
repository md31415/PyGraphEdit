'''
Some useful tools

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 23.03.2012
Most recent amendment: 30.05.2016

@author: Markus Dod
'''
import cython

cpdef binomial(int n, int k)

@cython.locals(i=cython.int)
cpdef falling_factorial(int n, int k)
cpdef factorial(int n)
cpdef stirlingSecondKind(int n, int k)
cpdef argmax(dict d)
cpdef argmin(dict d)
cpdef arg(dict d, e)
cpdef get_values(dict d)

@cython.locals(i=cython.int)
cpdef partition(set_):
  @cython.locals(j=cython.int)
  cdef visit(int n, list a)
  cdef f(int mu, int nu, int sigma, int n, list a)
  cdef b(int mu, int nu, int sigma, int n, list a)

@cython.locals(n=cython.int, j=cython.int, a=cython.list)
cpdef partition_kBlocks(ns, int k)
cpdef powerset(seq)
cpdef powerset_list(lst)
cpdef kpowerset(lst, int k)
cpdef kpowersetW(lst, int k, I)
cpdef minkpowerset(lst, int k)
cpdef maxkpowerset(lst, int k):

@cython.locals(i=cython.int, t=cython.int, m=cython.int, t1=cython.int,
               d=cython.dict)
cpdef determinant(M, n=*)
