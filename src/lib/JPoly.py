#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Some methods to calculate the J-polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.02.2015

@author: Markus Dod
'''

import sympy
import sys

from lib.tools import powerset, binomial  # @UnresolvedImport

from SGraph.GraphFunctions import isComplete, isPath  # @UnresolvedImport
from SGraph.GraphFunctions import minimumDegreeVertex  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x, z = sympy.symbols('x z')


# =============================================================================
def compute_polynomial(graph):
  '''
  J(G;x,z)
  '''
  P = 0
  pset = powerset(list(graph.getV()))
  for W in pset:
    P = P + x ** len(W) * z ** (len(graph.delta(W)) + len(graph.Delta(W)))

  return P

def computeRec(graph):
  '''
  Computes the J-polynomial
  '''
  def Jrec(g):
    if g.order() == 0:
      return 1
    elif g.order() == 1:
      return 1 + x * z ** int(g.numLoops(minimumDegreeVertex(g)))
    elif g.size() == 0:
      J = 1
      for v in g.getV():
        J = J * (1 + x * z ** int(g.numLoops(v)))
      return J

    # Choose a random edge for the decomposition
    e = g.randomEdge()
    h = g.copy()
    h.loopRemoveEdge(e)
    return z * Jrec(g - e) + (1 - z) * Jrec(h)

  return Jrec(graph)


# =============================================================================
# ============== SPECIAL GRAPHS ===============================================
# =============================================================================
def calcCompleteGraph(n):
  '''Calculate the J-polynomial for a complete graph'''
  J = 0
  for i in range(0, n +1):
    J += binomial(n, i) * x ** i * z **(binomial(i, 2)* i * (n - i))
  return J

def calcPath(n):
  '''Calculate the J-polynomial for a path'''
  from collections import deque
  poly = deque([1, 1 + x, x**2 * z + 2 * x * z + 1])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n]

  i = 2
  while i != n:
    i += 1
    J = 0
    for j in range(1, i - 1):
      J = J + poly[j]
    poly.append(x * z * poly[i-1] + x * z + 1 + x * z**2 * J)

  return poly[n]


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, q=None):
  '''
  Compute the J-polynomial
  @param graph: Graph
  @param m: Method
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  J = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      MDNetwork_log.info("starting calculation with algorithm for complete graphs ...")
      J = calcCompleteGraph(graph.order())
    elif isPath(graph):
      MDNetwork_log.info("starting calculation with algorithm for paths ...")
      J = calcPath(graph.order())

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      MDNetwork_log.info("starting calculation with algorithm for complete graphs ...")
      J = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      MDNetwork_log.info("starting calculation with algorithm for paths ...")
      J = calcPath(graph.order())
  
  if J:
    if q != None:
      q.put(J)
    return sympy.expand(J)
  
  if m == method.STATE_SPACE or m == None:
    MDNetwork_log.info("starting complete enumeration ...")
    J = compute_polynomial(graph)
  elif m == method.DECOMP:
    MDNetwork_log.info("starting decomposition ...")
    J = computeRec(graph)

  if q != None:
    q.put(J)
  return J


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")

