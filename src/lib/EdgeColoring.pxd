# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 15.10.2014

@author: Markus
'''
import cython

@cython.locals(i=cython.int, v=cython.int)
cdef solve_coloring(graph, int k)

@cython.locals(c=cython.int, v=cython.int)
cdef greedy(graph)

@cython.locals(i=cython.int)
cdef getSmallestColor(N1, N2, colors, int c):

@cython.locals(m=cython.int, i=cython.int, j=cython.int, k=cython.int)
cdef dsatur(graph)

cpdef compute_coloring(PyGraphEdit_log2, graph, k=*, m=*, q=*)
