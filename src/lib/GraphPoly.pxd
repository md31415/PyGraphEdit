'''
Implementation of different graph polynomials

Copyright (c) 2013-2017 Markus Dod, Peter Tittmann

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 27.05.2013

@author: Peter Tittmann, Markus Dod
'''
import cython

@cython.locals(n=cython.int, v=cython.int, i=cython.int, j=cython.int,
               m=cython.int, d=cython.int)
cpdef SubgraphPoly(G, q=None)

@cython.locals(n=cython.int, iso=cython.int, k=cython.int, d=cython.int)
cpdef SubgraphEnumeratingPoly(graph, q=*)

@cython.locals(n=cython.int, k=cython.int)
cpdef ExtendedSubgraphCountingPoly(graph, q=*)

@cython.locals(m=cython.int, d=cython.int, v=cython.int, m=cython.int, u=cython.int)
def SubgraphComponentPoly(graph, q=*)

@cython.locals(n=cython.int, m=cython.int)
cpdef GeneralSubgraphCountingPoly(graph, q=*)

@cython.locals(n=cython.int)
cpdef Reliability(graph, q=*)

@cython.locals(n=cython.int, m=cython.int)
cpdef AlliancePoly(graph, q=*)

@cython.locals(v=cython.int)
cpdef GeneralDomPoly(graph)
cpdef computeCutPoly(graph, q=*)
cpdef computeBipartiteSubgraphPoly(graph, q=*)
