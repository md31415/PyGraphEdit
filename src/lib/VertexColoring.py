# -*- coding: latin-1 -*-
'''
Implements methods to solve the vertex coloring problem.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 11.02.2014
'''

import sys


# =================================================================
# ================ Solve with SCIPOPT =============================
# =================================================================
def solve_coloring(graph, k):
  '''Using zibopt to find a coloring of the graph with k colors'''
  PyGraphEdit_log.info("starting scipopt to color the graph...")

  try:
    import modules.scipopt.scip as scip  # @UnresolvedImport
    from itertools import product
  except:
    PyGraphEdit_log.info("scipoptsuite not avalible")
    return None

  # create solver instance
  s = scip.Solver()
  s.create()
  s.includeDefaultPlugins()
  s.createProbBasic(b"Vertex Coloring")

  # Indexes for creating variables and constraints
  rows = graph.getV()
  k = int(k)
  vals = range(0, k)

  # add some variables
  probVars = {}
  varNames = {}
  varBaseName = "x"
  for i, k in product(rows, vals):
    if i not in probVars:
      probVars[i] = {}
      varNames[i] = {}
    p = varBaseName + "_" + str(i) + ":" + str(k)
    p = p.encode('ascii')
    varNames[i][k] = p
    probVars[i][k] = s.addIntVar(p, obj=1, lb=0.0, ub=1.0)

  # every vertex gets exactly one color
  for i in rows:
    coeffs = dict((probVars[i][k], 1.0) for k in vals)
    s.addCons(coeffs, lhs=1.0, rhs=1.0)

  # adjacent vertices getting different colors
  edges = graph.getEdgeSet()
  for e in edges:
    for k in vals:
      coeffs = {probVars[e[0]][k]: 1.0, probVars[e[1]][k]: 1.0}
      s.addCons(coeffs, rhs=1.0)

  # solve problem
  s.solve()
  status = s.getStatus()

  if status == scip.scip_status.optimal:
    # retrieving the best solution
    solution = s.getBestSol()

    # print solution
    sol = dict((i, 0) for i in rows)
    for i in rows:
      for k in vals:
        solValue = round(s.getVal(solution, probVars[i][k]))
        if solValue == 1:
          sol[i] = k + 1

        s.releaseVar(probVars[i][k])

    s.free()

    return sol

  else:
    return None

def greedy(graph):
  '''Colors the graph with the greedy-algorithm'''
  # get the degrees of the vertices
  degrees = graph.get_degrees()

  # generates a list and sort the vertices in respect to the degree
  deg = sorted([v for v in degrees], key=lambda v: degrees[v], reverse=True)

  # color the vertices starting with the vertex of smallest degree
  colors = {}
  c = 0
  for i, v in enumerate(deg):
    N = set(deg[:i]) & set(graph.neighbors(v))
    c_v = getSmallestColor(N, colors, c)
    colors[v] = c_v
    if c_v > c:
      c = c_v

  return colors, c

def getSmallestColor(N, colors, c):
  C = set([colors[v] for v in N])
  for i in range(1, c + 2):
    if i not in C:
      return i


# =======================================================================
# =============== DSATUR-ALGORITHM ======================================
# =======================================================================
def DSATUR_calculation(graph):
  """Start the calculation"""
  # Number of the used colors
  color_counter = 1

  # To some initialization
  degrees = graph.get_degrees()
  saturation_degrees = {v: 0 for v in graph.getV()}
  coloring = {v: 0 for v in graph.getV()}
  uncolored_vertices = set(graph.getV())

  # Find one vertex with the maximal degree
  maximum_degree_vertex = 0
  maximum_degree = 0
  for vertex in graph.getV():
    if degrees[vertex] > maximum_degree:
      maximum_degree = degrees[vertex]
      maximum_degree_vertex = vertex

  # Update saturation
  neighbors = graph.neighbors(maximum_degree_vertex)
  for v in neighbors:
    saturation_degrees[v] += 1

  # Coloring the first vertex with the maximum degree
  coloring[maximum_degree_vertex] = color_counter
  uncolored_vertices.remove(maximum_degree_vertex)

  # Color the remaining vertices
  while(len(uncolored_vertices) > 0):
    # Get maximum saturation degree
    maximum_satur_degree =- 1
    for v in uncolored_vertices:
      if saturation_degrees[v] > maximum_satur_degree:
        maximum_satur_degree = saturation_degrees[v]

    # Get list of vertices with maximum saturation degree
    maximum_satur_degree_vertices = [v for v in uncolored_vertices if saturation_degrees[v] == maximum_satur_degree]           

    # There are more then one vertex with maximum saturation
    coloring_index = maximum_satur_degree_vertices[0]
    if len(maximum_satur_degree_vertices) > 1:
      # Find vertex with maximum degree
      maximum_degree = -1
      for v in maximum_satur_degree_vertices:
        degree = degrees[v]
        if degree > maximum_degree:
          coloring_index = v
          maximum_degree = degree

    # Coloring
    neighbors = graph.neighbors(coloring_index)
    for number_color in range(1, color_counter + 1):
      if get_amount_color(neighbors, number_color, coloring) == 0:
        coloring[coloring_index] = number_color
        break

    # If it has not been colored
    if coloring[coloring_index] == 0:
      # Add new color
      color_counter += 1
      coloring[coloring_index] = color_counter

    # Remove vertex from uncolored set
    uncolored_vertices.remove(coloring_index)

    # Update degree of saturation
    for nvertex in neighbors:
      subneighbors = graph.neighbors(nvertex)

      if get_amount_color(subneighbors, coloring[coloring_index], coloring) == 1:
        saturation_degrees[nvertex] += 1

  return coloring, color_counter

def get_amount_color(vertices, color_number, coloring):
  """
  Counts how many vertices has color 'color_number'.
  @param vertices: List of vertices to check
  @param color_number: Number of color that is searched in vertices.
  @return: Number of found vertices with the specified color.
  """
  color_counter = 0  
  for v in vertices:
    if coloring[v] == color_number:
      color_counter += 1

  return color_counter


# =============================================================================
# =============================================================================
# =============================================================================
def compute_coloring(PyGraphEdit_log2, graph, k=None, m=None, q=None):
  '''
  Compute a proper coloring of the graph
  @param graph: Graph
  @param m: Method
  @param k: Number of colors (only for Scipopt)
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  colors = None
  if m == "greedy":
    PyGraphEdit_log.info("using greedy-heuristic ...")
    colors, k = greedy(graph)
  elif m == "SCIPOPT (B&B)":
    PyGraphEdit_log.info("using ScipOpt ...")
    colors = solve_coloring(graph, k)
  elif m == "DSATUR" or m == None:
    PyGraphEdit_log.info("using DSATUR heuristic ...")
    colors, k = DSATUR_calculation(graph)

  if q != None:
    q.put(colors)
    q.put(k)
  return (colors, k)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Use PyGraphEdit!")
