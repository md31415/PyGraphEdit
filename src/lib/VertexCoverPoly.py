#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the vertex cover polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 09.12.2015

@author: Markus Dod

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

import math
from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import binomial  # @UnresolvedImport
from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isComplete, isPath, isCycle,  # @UnresolvedImport
                                  isCompleteBipartite, getBipartition)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x = symbols('x')
D = None


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Computes the vertex cover polynomial
  '''
  def isVertexCover(W):
    for e in graph.getEdgeSet():
      if e[0] not in W and e[1] not in W:
        return False
    return True

  B = 0
  for W in powerset(graph.getV()):
    if isVertexCover(W):
      B = B + x**len(W)

  return B

def compute_polynomial_rec(graph):
  """Returns the vertex cover polynomial of G."""
  if graph.order() == 0:
    return 1
  elif graph.size() == 0:
    return (x + 1)**graph.order()
  else:
    m = -1
    for u in graph.getVIt():
      d = graph.degree(u)
      if d > m:
        v = u
        m = d
    return x**m * compute_polynomial_rec(graph - (graph.neighbors(v) | {v})) + x * compute_polynomial_rec(graph - v)


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcCompleteGraph(n):
  '''Calculates the vertex cover polynomial of the complete graph'''
  return x**n + n * x**(n-1)

def calcCompleteBipartiteGraph(graph):
  '''Calculates the vertex cover polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  return x**n * (1 + x)**m + x**m * (1 + x)**n - x**(n+m)

def calcPath(n):
  '''Calculates the vertex cover polynomial of the path'''
  poly = 0
  for k in range(n + 1):
    poly += binomial(k + 1, n - k) * x**k

  return poly

def calcCycle(n):
  '''Calculates the vertex cover polynomial of the cycle'''
  poly = 0
  for k in range(1, n + 1):
    poly += float(n)/k * binomial(k, n - k) * x**k

  return poly

def calcStar(n):
  '''Calculate the vertex cover polyomial of a star'''
  return x * (1 + x)**(n - 1) + x**(n-1)

def calcWheel(n):
  '''Calculates the vertex cover polynomial of the wheel graph'''
  poly = 0
  for k in range(math.ceil((n + 1) / 2), n + 1):
    poly += float(n - 1)/(k - 1) * binomial(k - 1, n - k) * x**k

  return x**(n-1) + poly

def calcCentipede(n):
  '''Calculate the vertex cover polyomial of a centipede'''
  from collections import deque
  poly = deque([2 * x + 1, 3*x**2 + 4*x + 1])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n - 1]

  i = 2
  while i != n:
    i += 1
    poly.append((x + 1) * x * poly[0] + (x + 1) * poly[1])
    poly.popleft()

  indpoly = poly[1]
  return x**n * indpoly.subs(x, 1 / x)

def calcBook(n):
  '''Calculates the vertex cover polynomial of the book graph'''
  indpoly = 2 * x * (1 + x)**n + (1 + 2 * x)**n
  return x**n * indpoly.subs(x, 1 / x)

def calcSun(n):
  '''Calculates the vertex cover polynomial of the sun graph'''
  indpoly = (x + 1)**(n - 2) * (1 + x * (x + n + 2))
  return x**n * indpoly.subs(x, 1 / x)

def calcSunlet(n):
  '''Calculates the vertex cover polynomial of the sunlet graph'''
  u = ((x + 1) * (5 * x + 1))**(1/2)
  indpoly = 2**(-n) * ((u + x + 1)**n + (-u + x + 1)**n)
  return x**n * indpoly.subs(x, 1 / x)


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the vertex cover polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing the vertex cover polynomial ...")
  PyGraphEdit_log.info("")

  VC = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      VC = calcCompleteGraph(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      VC = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      VC = calcCycle(graph.order())
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      VC = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      VC = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      VC = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      VC = calcCycle(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      VC = calcCompleteBipartiteGraph(graph)
    elif graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("starting calculation with algorithm for stars ...")
      VC = calcStar(graph.order())
    elif graph.graphtype == graphType.WHEEL:
      PyGraphEdit_log.info("starting calculation with algorithm for wheels ...")
      VC = calcWheel(graph.order())
    elif graph.graphtype == graphType.CENTIPEDE:
      PyGraphEdit_log.info("starting calculation with algorithm for centipedes ...")
      VC = calcCentipede(graph.order() / 2)
    elif graph.graphtype == graphType.BOOK:
      PyGraphEdit_log.info("starting calculation with algorithm for books ...")
      VC = calcBook(graph.order() / 2 - 1)
    elif graph.graphtype == graphType.SUN:
      PyGraphEdit_log.info("starting calculation with algorithm for suns ...")
      VC = calcSun(graph.order() / 2)
    elif graph.graphtype == graphType.SUNLET:
      PyGraphEdit_log.info("starting calculation with algorithm for sunlets ...")
      VC = calcSunlet(graph.order() / 2)

  if VC:
    if q != None:
      q.put(VC)
    PyGraphEdit_log.info("done ...")
    return expand(VC)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration ...")
    VC = compute_polynomial(graph)
  elif m == method.DECOMP:
    PyGraphEdit_log.info("starting recursive algorithm ...")
    VC = compute_polynomial_rec(graph)

  if q != None:
    q.put(VC)

  PyGraphEdit_log.info("done ...")
  return expand(VC)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
