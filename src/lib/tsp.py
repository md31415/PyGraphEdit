# -*- coding: latin-1 -*-
'''
Implements some methods to solve the symmetric tsp.
For the exact solution python-zibopt is used to call zibopt.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.04.2013
'''

from copy import deepcopy

from lib.tools import powerset  # @UnresolvedImport

from SGraph.GraphFunctions import diameter, find_artikulations  # @UnresolvedImport


# =================================================================
# =================================================================
inf = float('Inf')


# =================================================================
# ================ Tests ==========================================
# =================================================================
def hamTest(graph):
  if graph.get_min_degree() < 2:
    return False

  if diameter(graph) > graph.order() / 2:
    return False

  if len(find_artikulations(graph)) != 0:
    return False

  return True


# =================================================================
# ================ Solve with heuristic ===========================
# =================================================================
def NearestNeighbor(graph):
  '''
  Calculate a start solution with the NearestNeighbor-Heuristic
  @param graph: SGraph
  '''
  if not hamTest(graph):
    return [], inf

  n = graph.order()
  from random import randint
  flag = False
  rounds = 0
  while not flag and rounds < 5 * graph.order():
    rounds += 1
    flag = True
    start = randint(1, n)
    tour = []
    objective = 0.0
    tour.append(start)
    last = start

    while len(tour) != graph.order():
      min_weight = inf
      w = 0
      for v in graph.neighbors(last):
        if v not in tour:
          weight = graph.getWeight((last, v))
          if weight < min_weight:
            min_weight = weight
            w = v
      objective += min_weight
      tour.append(w)
      last = w
      if min_weight == inf:
        flag = False
        break

    if flag:
      objective += graph.getWeight((last, start))
      if graph.getWeight((last, start)) == inf:
        flag = False

  if objective == inf:
    return [], objective
  else:
    return tour, objective

def twoOpt(graph, tour, objective=None):
  '''
  2-opt-Heuristic
  @param graph: Graph
  @param tour: Start solution
  @param objective: Objective of the start solution
  @return: Solution, Objective
  '''
  flag = True
  while flag:
    flag = False
    for i in range(0, len(tour) - 3):
      for j in range(i + 2, len(tour) - 1):
        w1 = graph.getWeight([tour[i], tour[i + 1]]) + graph.getWeight([tour[j], tour[j + 1]])
        w2 = graph.getWeight([tour[i], tour[j]]) + graph.getWeight([tour[i + 1], tour[j + 1]])
        if w1 > w2:
          flag = True
          neueTour = [v for v in tour[:i + 1]]
          neueTour.append(tour[j])
          for k in range(j - 1, i, -1):
            neueTour.append(tour[k])
          for k in range(j + 1, len(tour)):
            neueTour.append(tour[k])
          tour = deepcopy(neueTour)
          break
      if flag:
        break

  objective = 0.0
  for i in range(len(tour) - 1):
    objective += graph.getWeight([tour[i], tour[i + 1]])
  objective += graph.getWeight([tour[len(tour) - 1], tour[0]])

  return tour, objective

def threeOpt(graph, tour, objective=None):
  '''
  3-opt-Heuristic
  @param graph: Graph
  @param tour: Start solution
  @param objective: Objective of the start solution
  @return: Solution, Objective
  '''
  flag = True
  while flag:
    flag = False
    for i in range(0, len(tour) - 5):
      for j in range(i + 2, len(tour) - 3):
        for k in range(j + 2, len(tour) - 1):
          w1 = graph.getWeight([tour[i], tour[i + 1]]) + graph.getWeight([tour[j], tour[j + 1]]) + graph.getWeight([tour[k], tour[k + 1]])
          w2 = graph.getWeight([tour[i], tour[j]]) + graph.getWeight([tour[i + 1], tour[k]]) + graph.getWeight([tour[j + 1], tour[k + 1]])
          w3 = graph.getWeight([tour[i], tour[j + 1]]) + graph.getWeight([tour[i + 1], tour[k]]) + graph.getWeight([tour[j], tour[k + 1]])
          w4 = graph.getWeight([tour[i], tour[j + 1]]) + graph.getWeight([tour[j], tour[k]]) + graph.getWeight([tour[i + 1], tour[k + 1]])
          w5 = graph.getWeight([tour[i], tour[j + 1]]) + graph.getWeight([tour[i + 1], tour[k]]) + graph.getWeight([tour[j], tour[k + 1]])

          if w2 is min([w1, w2, w3, w4, w5]):
            flag = True
            neueTour = [v for v in tour[:i + 1]]
            for l in range(j, i, -1):
              neueTour.append(tour[l])
            for l in range(k, j, -1):
              neueTour.append(tour[l])
            for l in range(k + 1, len(tour)):
              neueTour.append(tour[l])
            tour = deepcopy(neueTour)
            break
          elif w3 is min([w1, w2, w3, w4, w5]):
            flag = True
            neueTour = [v for v in tour[:i + 1]]
            for l in range(j + 1, k + 1):
              neueTour.append(tour[l])
            for l in range(i + 1, j + 1):
              neueTour.append(tour[l])
            for l in range(k + 1, len(tour)):
              neueTour.append(tour[l])
            tour = deepcopy(neueTour)
            break
          elif w4 is min([w1, w2, w3, w4, w5]):
            flag = True
            neueTour = [v for v in tour[:i + 1]]
            for l in range(j + 1, k + 1):
              neueTour.append(tour[l])
            for l in range(j, i, -1):
              neueTour.append(tour[l])
            for l in range(k + 1, len(tour)):
              neueTour.append(tour[l])
            tour = deepcopy(neueTour)
            break
          elif w5 is min([w1, w2, w3, w4, w5]):
            flag = True
            neueTour = [v for v in tour[:i + 1]]
            for l in range(j + 1, k + 1):
              neueTour.append(tour[l])
            for l in range(i + 1, j + 1):
              neueTour.append(tour[l])
            for l in range(k + 1, len(tour)):
              neueTour.append(tour[l])
            tour = deepcopy(neueTour)
            break
        if flag:
          break
      if flag:
        break

  objective = 0.0
  for i in range(len(tour) - 1):
    objective += graph.getWeight([tour[i], tour[i + 1]])
  objective += graph.getWeight([tour[len(tour) - 1], tour[0]])

  return tour, objective


# =================================================================
# ================ Solve with SCIPOPT =============================
# =================================================================
def solve_tsp(graph, distance):
  # We represent STSP as a lower triangular matrix with the diagonal.
  # Thus the first column and row represent connections to the first node.
  print("Starting solving TSP with scipopt ...")

  if not hamTest(graph):
    return [], inf

  try:
    import modules.scipopt.scip as scip  # @UnresolvedImport
  except:
    print("scipoptsuite not avalible")
    return None

  # create solver instance
  s = scip.Solver()
  s.create()
  s.includeDefaultPlugins()
  s.createProbBasic(b"TSP")

  n = graph.order()

  # add some variables
  probVars = {}
  varNames = {}
  varBaseName = "x"
  for v in graph.getV():
    for w in graph.neighbors(v):
      if v > w:
        if v not in probVars:
          probVars[v] = {}
          varNames[v] = {}
        p = varBaseName + "_" + str(v) + ":" + str(w)
        p = p.encode('ascii')
        varNames[v][w] = p
        probVars[v][w] = s.addIntVar(p, obj=distance[v - 1][w - 1], lb=0.0, ub=1.0)

  # every vertex has exactly two neighbors in the tour
  for v in graph.getV():
    coeffs = {}
    for w in graph.neighbors(v):
      if v > w:
        coeffs[probVars[v][w]] = 1.0
      elif w > v:
        coeffs[probVars[w][v]] = 1.0
    s.addCons(coeffs, lhs=2.0, rhs=2.0)

  # Subtour elemination
  pset = powerset(graph.getV())
  for S in pset:
    if len(S) != 0 and len(S) != n:
      coeffs = {}
      for v in S:
        for w in set(graph.neighbors(v)) - set(S):
          if v > w:
            coeffs[probVars[v][w]] = 1.0
          elif w > v:
            coeffs[probVars[w][v]] = 1.0
      s.addCons(coeffs, lhs=2.0)

  # solve problem
  s.solve()
  status = s.getStatus()

  if status == scip.scip_status.optimal:
    # retrieving the best solution
    solution = s.getBestSol()
    objectiv = s.getSolObjVal(solution, False)

    # store the edges in the solution
    solutionedges = []
    for v in graph.getV():
      for w in graph.neighbors(v):
        if v > w:
          solValue = round(s.getVal(solution, probVars[v][w]))
          if solValue == 1:
            solutionedges.append((v, w))
          # print(str(varNames[v][w]) + ": " + str(solValue))

          s.releaseVar(probVars[v][w])

    s.free()

    return transformSolution(solutionedges, n), objectiv

  else:
    return [], inf

def transformSolution(solution, n):
  vertexsolution = [1]
  v = 1
  while len(vertexsolution) != n:
    for e in solution:
      if e[0] == v and e[1] not in vertexsolution:
        vertexsolution.append(e[1])
        v = e[1]
        break
      elif e[1] == v and e[0] not in vertexsolution:
        vertexsolution.append(e[0])
        v = e[0]
        break
  return vertexsolution


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  import sys
  sys.exit("Hauptmodul zum starten verwenden")
