#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculation of the trivariate total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 04.02.2014

@author: Markus Dod
'''

from copy import deepcopy
import hashlib
from sqlite3 import connect, PARSE_DECLTYPES, register_adapter, register_converter  # @UnresolvedImport
from sympy import symbols, expand
import sys

from lib.Decompositions import get_path_decomposition, get_width  # @UnresolvedImport
from lib.tools import powerset, MarkedSet, Tree, binomial  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphFunctions import (isComplete, isPath, isCompleteBipartite,  # @UnresolvedImport
                                  getBipartition, getkPartiteSizes)  # @UnresolvedImport
from SGraph.GraphFunctions import inducedSubgraph  # @UnresolvedImport
from SGraph.GraphParser import (graphadapter, graphkonverter, dictadapter,  # @UnresolvedImport
                                dictkonverter, load_graphdb)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x, y, z = symbols('x y z')


# =============================================================================
# =============================================================================
def compute_tri_polynomial(graph):
  '''
  Calculation of the trivariate total domination polynomial using complete enumeration
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    iso = (inducedSubgraph(graph, set(W))).numIso()
    P = x ** len(W) * y ** len(graph.open_neighbors_set(W)) * z ** iso + P

  return P


# =============================================================================
# ================ Path decomposition =========================================
# =============================================================================
def path_decomposition(graph):
  '''
  Calculation of the trivariate total domination polynomial using a path decomposition
  @param graph: Graph
  @return: The trivariate total domination polynomial
  '''
  decomposition = get_path_decomposition(graph, min(graph.getV()), max(graph.getV()))
  global MDNetwork_log
  MDNetwork_log.debug("Decomposition: %s" % str(decomposition))
  MDNetwork_log.info("width of used path-decomposition: %s" % str(get_width(decomposition)))

  TTD = 0
  states = []

  # Aus der Dekomposition das Polynom berechnen
  states.append(PartState())
  for s in decomposition:
    try:
      if s > 0:
        MDNetwork_log.debug("activate %d" % s)
        activate(states, s)

      else:
        MDNetwork_log.debug("deactivate %d" % -s)
        s = -s
        deactivate(states, s)
        compress(states)

    except:
      MDNetwork_log.debug("activate %s" % str(s))
      processEdge(states, s)

#    for state in states:
#      print(state)
#    MDNetwork_log.info("----------------------")

  # Polynom aus dem aktuellen states aufsummieren
  for state in states:
    TTD = state.getPoly() + TTD

#   for state in states:
#     print(state)

  return TTD

def activate(states, element):
  tmp = []
  for state in states:
    new = state.activate(element)
    tmp.append(new)

  for state in tmp:
    states.append(state)

def deactivate(states, element):
  for state in states:
    state.deactivate(element)

def processEdge(states, edge):
  for state in states:
    state.processEdge(edge)

def compress(states):
  flag = True
  while flag:
    flag = False
    len_states = len(states)
    for i in range(len_states - 1):
      for j in range(i + 1, len_states):
        if states[i] == states[j]:
          flag = True
          states[i].poly = states[i].poly + states[j].poly
          del states[j]
          break
      if flag:
        break

class PartState():
  def __init__(self, elementset=MarkedSet(), poly=1):
    self.markedPartition = elementset
    self.poly = poly

  def activate(self, element):
    other = self.copy()
    self.markedPartition.addElement(element)
    other.markedPartition.addElement(element)
    self.markedPartition.markElement(element, 0)
    self.poly = self.poly * x
    return other

  def deactivate(self, element):
    if self.markedPartition.isMarked(element, 0) and not self.markedPartition.isMarked(element, 1):
      self.poly = self.poly * z
    self.markedPartition.removeElement(element)

  def processEdge(self, edge):
    u = edge[0]
    v = edge[1]
    if self.markedPartition.isMarked(u, 0) and not self.markedPartition.isMarked(v, 1):
      self.markedPartition.markElement(v, 1)
      if not self.markedPartition.isMarked(v, 0):
        self.poly = self.poly * y
    if self.markedPartition.isMarked(v, 0) and not self.markedPartition.isMarked(u, 1):
      self.markedPartition.markElement(u, 1)
      if not self.markedPartition.isMarked(u, 0):
        self.poly = self.poly * y

  def getPoly(self):
    return self.poly

  def copy(self):
    other = PartState()
    other.markedPartition = deepcopy(self.markedPartition)
    other.poly = deepcopy(self.poly)
    return other

  def __eq__(self, other):
    if self.markedPartition == other.markedPartition:
      return True
    else:
      return False

  def __str__(self):
    return "%s - %s" % (str(self.markedPartition), str(self.poly))


# =============================================================================
# ================ Trees ======================================================
# =============================================================================
def calculateTree(graph):
  '''
  Calculates the trivariate total domination polynomial of a tree.
  @param graph: Tree
  @return: The trivariate total domination polynomial of the tree 
  '''
  t = Tree()
  t.fromSGraph(graph)

  states = calc_tree(t, t.get_root())
  Y = 0

  for s in states.Y:
    if s != None:
      Y = s + Y
  return Y

def calc_all_trees(graph=None, fname=None):
  '''
  Calculates the trivariate total domination polynomial of all trees in
  fname and compares the polynomials. Stops if it found two trees with
  the same polynomial.
  '''
  if graph == None and fname == None:
    return "error no graph or file"

  graphs = []
  # poly = {}
  poly = set([])
  if graph == None:
    graphnames = getGraphNamesdb(fname)
    num = len(graphnames)
    print("Anzahl der Graphen: %d" % num)
    p = 0.0
    for name in graphnames:
      g = SGraph()
      g, _ = load_graphdb(fname, name)
      if g.order() > 0:
        D = calculateTree(g)
        str_D = str(D)
        m = hashlib.md5(str_D.encode()).hexdigest()
        if m in poly:
          print("identisch")
          print(g)
          # print(poly[m])
          sys.exit()

        else:
          # poly[m]= g
          poly.add(m)
      p += 100 / num
      print("%.4f Prozent" % p)

  else:
    graphs.append(graph)

  print("alle abgearbeitet....")

def calc_tree(tree, node):
  if len(node.childs) == 0:
    return DecompState(node.number, [0, x * z, 0, 1])

  else:
    if len(node.childs) == 1:
      v = node.childs[0]
      s = calc_tree(tree, tree.get_node(v))
      s.activate(node.number)

      return s

    else:
      Z = []
      for v in node.childs:
        s = calc_tree(tree, tree.get_node(v))
        Z.append(s)

      new = [0, 0, 0, 0]
      a = 1
      b = 1
      c = 1
      d = 1
      for state in Z:
        a = (state.Y[0] + state.Y[1] / z + state.Y[2] + y * state.Y[3]) * a
        b = (state.Y[2] + state.Y[3]) * b
        c = (state.Y[2] + y * state.Y[3]) * c
        d = (state.Y[0] + state.Y[1] + state.Y[2] + state.Y[3]) * d

      new[0] = x * (a - c)
      new[1] = x * z * c
      new[2] = y * (d - b)
      new[3] = b

      return DecompState(node.number, new)

class DecompState():
  def __init__(self, v, poly=[0, 0, 0, 0]):
    self.v = v
    self.Y = poly

  def activate(self, v):
    '''
    Einen Knoten aktivieren
    @param v: Knoten
    '''
    self.v = v
    tmp0 = x * (self.Y[0] + self.Y[1] / z)
    tmp1 = x * z * (self.Y[2] + y * self.Y[3])
    tmp2 = y * (self.Y[0] + self.Y[1])
    tmp3 = self.Y[2] + self.Y[3]

    self.Y[0] = tmp0
    self.Y[1] = tmp1
    self.Y[2] = tmp2
    self.Y[3] = tmp3

  def copy(self):
    '''
    Kopie anlegen und zur�ckgeben
    @return: Kopie
    '''
    other = DecompState(deepcopy(self.partition))
    other.Y = self.Y
    return other

  def __str__(self):
    return "v: %s -> %s\n%s\n%s\n%s" % \
                        (str(self.v), str(self.Y[0]), str(self.Y[1]), str(self.Y[2]), str(self.Y[3]))

def getGraphNamesdb(fname, numvertices=None):
  '''
  Alle Graphennamen aus SQLite-Datenbank auslesen
  @param fname: Dateiname der Datenbank
  @param numvertices: Anzahl der Knoten der auszulesenden Graphen
  @return: Namen der Graphen in der Datei
  '''
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    print("something wrong 1")
    return None
  cursor = connection.cursor()

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  if numvertices != None:
    cursor.execute("SELECT name FROM Graphen WHERE n=%d" % numvertices)
  else:
    try:
      cursor.execute("SELECT name FROM Graphen")
    except:
      print("something wrong 2")
      return None

  try:
    names = cursor.fetchall()
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None

  n = []
  for name in names:
    n.append(name[0])

  cursor.close()
  return n


# =============================================================================
def calc_all_graphs(graph=None, fname=None):
  '''
  Calculates the trivariate total domination polynomial all graphs in fname
  and search for equal polynomials
  '''
  if graph == None and fname == None:
    return "error no graph or file"

  graphs = []
  poly = set([])
  test = {}
  if graph == None:
    graphnames = getGraphNamesdb(fname)
    num = len(graphnames)
    print("Anzahl der Graphen: %d" % num)
    p = 0.0
    for name in graphnames:
      g = SGraph()
      g, _ = load_graphdb(fname, name)
      if g.order() > 0:
        D = compute_tri_polynomial(g)
        str_D = str(D)
        m = hashlib.md5(str_D.encode()).hexdigest()
        if m in poly:
          print("identisch")
          print(g)
          print(test[m])
          # print(poly[m])
          sys.exit()

        else:
          # poly[m]= g
          poly.add(m)
          test[m] = name
      p += 100 / num
      print("%.4f Prozent" % p)

  else:
    graphs.append(graph)

  print("alle abgearbeitet....")


# =============================================================================
# ========= Product graphs ====================================================
# =============================================================================
def calculateCartesianKK2(n):
  '''
  Calculates trivariate total domination polynomial of the Cartesian product
  of complete graph with order n and an other complete graph
  @param n: Order of the first graph
  '''
  Y1 = 2 * ((y + x * y)**n - y**n * (1 + n * x * (1 - z)))
  Y2 = y**(2 * n) * ((1 + x / y)**n - 1 - n * x / y)**2
  Y3 = 2 * n * x * y**n * ((z + x / y) * (y + x)**(n - 1) - y**(n - 1) * z + 1 / 2 * (n - 1) * x * y**(n - 2) * z**2 - x * y**(n - 2) * ((n - 1) * z + 1 / 2))
  return Y1 + Y2 + Y3 + 1

def calculateLexiKG(n, graph):
  '''
  Calculates trivariate total domination polynomial of the lexicographic product
  of a complete graph and an arbitrary graph.
  @param n: Order of the complete graph
  @param graph: Second graph
  '''
  m = graph.order()
  Y = compute_tri_polynomial(graph)
  S = sum([binomial(n, i) * ((1 + x/y)**m - 1)**i for i in range(2, n + 1)])
  return n * y**((n - 1)*m) * (Y - 1) + y**(m * n) * S + 1

def calculateLexiGK(n, graph):
  '''
  Calculates trivariate total domination polynomial of the lexicographic product
  of an arbitrary graph and a complete graph.
  @param n: Order of the complete graph
  @param graph: Second graph
  '''
  u, v, w = symbols('u v w')
  Y = compute_tri_polynomial(graph)
  subs_x = (v + u)**n - v**n
  subs_y = v**n
  subs_z = 1 + (n * u * (w - 1) / v) / ((1 + u / v)**n - 1)
  return expand(Y.subs({x: subs_x, y: subs_y, z: subs_z}).subs({u: x, v: y, w: z}))

def calculateLexiPH(n, graph):
  '''
  Calculates trivariate total domination polynomial of the lexicographic product
  of a path and an arbitrary graph.
  @param n: Order of the path
  @param graph: Second graph
  '''
  m = graph.order()
  Y = compute_tri_polynomial(graph)
  f = [0, Y - 1]
  h = [1, (y + x)**m - y**m]
  G = [1, Y]

  k = 1
  while k != n:
    k += 1
    h.append(((y + x)**m - y**m) * (y**m * G[k-2] + h[k-1]))
    f.append((Y - 1) * y**m * G[k-2] + ((y + x)**m - y**m) * h[k-1])
    G.append(f[k] + y**m * sum([f[k-i] for i in range(1, k)]) + 1)

  return G[n]

def calculateStrongKH(n, graph):
  '''
  Calculates trivariate total domination polynomial of the strong product
  of a complete graph and an arbitrary graph.
  @param n: Order of the path
  @param graph: Second graph
  '''
  u, v, w = symbols('u v w')
  Y = compute_tri_polynomial(graph)
  subs_x = (v + u)**n - v**n
  subs_y = v**n
  subs_z = 1 + (n * u * (w - 1) / v) / ((1 + u / v)**n - 1)
  return expand(Y.subs({x: subs_x, y: subs_y, z: subs_z}).subs({u: x, v: y, w: z}))


# ============================================================================
# ============= SPECIAL GRAPHS ===============================================
# ============================================================================
def calcCompleteGraph(n):
  '''Calculates the trivariate total domination polynomial of the complete graph'''
  return (x + y) ** n - y ** n + n * (z - 1) * x * y ** (n - 1) + 1

def calcCompleteBipartiteGraph(graph):
  '''Calculates the trivariate total domination polynomial of the complete bipartite graph'''
  flag, bipartition = getBipartition(graph)
  if not flag:
    return "error"

  # Terminate the size of the partitions
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1

  # Calculate the polynomial
  return ((x + y) ** n - y ** n) * ((x + y) ** m - y ** m) + y ** n * ((1 + x * z) ** m - 1) + y ** m * ((1 + x * z) ** n - 1) + 1

def calcCompletekPartiteGraph(graph):
  '''Calculates the trivariate total domination polynomial of the complete k-partite graph'''
  p = getkPartiteSizes(graph)
  return _calckPartiteGraph(p)

def _calckPartiteGraph(p):
  '''Recursive function to calculate the trivariate total domination polynomial 
  of a complete k-partite graph with the partition p'''
  if len(p) == 2:
    return ((x + y) ** p[0] - y ** p[0]) * ((x + y) ** p[1] - y ** p[1]) + y ** p[0] * ((1 + x * z) ** p[1] - 1) + y ** p[1] * ((1 + x * z) ** p[0] - 1) + 1

  k = len(p) - 1
  n = sum(p[:k])
  return y ** n * ((1 + x * z) ** p[k] - 1) + ((x + y) ** n - y ** n) * ((x + y) ** p[k] - y ** p[k]) + y ** p[k] * (_calckPartiteGraph(p[:k]) - 1) + 1

def calcPath(n):
  '''Calculates the trivariate total domination polynomial of the path'''
  poly_p = [1, 1 + x * z, 1 + 2 * x * y * z + x ** 2]
  if n <= 2:
    return poly_p[n]

  i = 2
  while i != n:
    poly_p.append(x ** (i + 1) + \
                  x * y * z * (1 + poly_p[i - 1]) + x * y * sum([x ** (j + 1) + y * z * poly_p[j] + x ** (j + 1) * poly_p[i - j - 2] for j in range(0, i - 1)]) + \
                  x * y ** 2 * sum([sum([x ** (k) * poly_p[j - k] for k in range(1, j + 1)]) for j in range(1, i - 1)]) + 1)
    i += 1

  return poly_p[n]


# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, graph1=None, graph2=None, q=None):
  '''
  Calculate the trivariate total domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  TD = None
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      MDNetwork_log.info("recognized complete graph ...")
      TD = calcCompleteGraph(graph.order())
    elif isPath(graph):
      MDNetwork_log.info("recognized path ...")
      TD = calcPath(graph.order())
    elif isCompleteBipartite(graph):
      MDNetwork_log.info("recognized complete bipartite graph ...")
      TD = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      MDNetwork_log.info("recognized an complete graph ...")
      TD = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      MDNetwork_log.info("recognized a path ...")
      TD = calcPath(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      MDNetwork_log.info("recognized an complete bipartite graph ...")
      TD = calcCompleteBipartiteGraph(graph)
    elif graph.graphtype == graphType.KPARTITE:
      MDNetwork_log.info("recognized an complete k-partite graph ...")
      TD = calcCompletekPartiteGraph(graph)
      
    elif graph.graphtype == graphType.CARTESIAN and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the Cartesian product ...")
      if (graph1.graphtype == graphType.COMPLETE and graph2.graphtype == graphType.COMPLETE
          and (graph1.order() == 2 or graph2.order() == 2)):
        MDNetwork_log.info("recognized product of two complete graphs.")
        MDNetwork_log.info("Starting calculation ...")
        if graph2.order() == 2:
          TD = calculateCartesianKK2(graph1.order())
        else:
          TD = calculateCartesianKK2(graph2.order())

    elif graph.graphtype == graphType.LEXI and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the lexicographic product ...")
      if graph1.graphtype == graphType.COMPLETE:
        TD = calculateLexiKG(graph1.order(), graph2)
      elif graph2.graphtype == graphType.COMPLETE:
        TD = calculateLexiGK(graph2.order(), graph1)
      elif graph1.graphtype == graphType.PATH:
        TD = calculateLexiPH(graph1.order(), graph2)

    elif graph.graphtype == graphType.STRONG and graph1 != None and graph2 != None:
      MDNetwork_log.info("searching for an algorithm for the strong product ...")
      if graph1.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a graph with a complete graph.")
        MDNetwork_log.info("Starting calculation ...")
        TD = calculateStrongKH(graph1.order(), graph2)
      elif graph2.graphtype == graphType.COMPLETE:
        MDNetwork_log.info("recognized product of a graph with a complete graph.")
        MDNetwork_log.info("Starting calculation ...")
        TD = calculateStrongKH(graph2.order(), graph1)

  if TD:
    if q != None:
      q.put(TD)
    return expand(TD)

  if m == method.STATE_SPACE or m == None or m == method.AUTOMATIC:
    MDNetwork_log.info("starting complete enumeration...")
    TD = compute_tri_polynomial(graph)
  elif m == method.PATH_DECOMP:
    MDNetwork_log.info("starting path decomposition...")
    TD = path_decomposition(graph)
  elif m == method.TREE:
    MDNetwork_log.info("starting tree-algorithm...")
    TD = calculateTree(graph)
  elif m == method.PATH:
    MDNetwork_log.info("starting path-algorithm...")
    TD = calcPath(graph.order())

  if q != None:
    q.put(TD)
  return expand(TD)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
