#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
Calculates the simple edge domination reliability

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 26.11.2012

@author: Markus Dod
'''

from copy import deepcopy
import sympy
import sys

from lib.tools import powerset, binomial  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport


# =============================================================================
# =============================================================================
num_processes = 8

global_A = None
global_B = None
global_graph = None
global_d = 0

p = sympy.symbols('p')


# =============================================================================
# =============================================================================
# =============================================================================
def state_space_enumeration(graph, A, d=1, k=1):
  '''
  Calculates SPEDRel(G,A,p) and SPEDRel(G,A,d,p)
  @param graph: Graph
  @param A: Dominating vertices
  @param d: Domination distance
  '''
  if len(A) < k:
    return 0
  elif len(A) == len(graph.getV()):
    return 1

  SPEDRel = 0
  if k == 1 and d == 1:
    e = len(set(graph.getV()) - set(A))
    f = int(1 / 2 * len(graph.Delta(A)))
    SPEDRel = p ** e * (1 - p) ** (len(graph.getEdgeSet()) - e - f)
    for v in set(graph.getV()) - set(A):
      SPEDRel = SPEDRel * graph.degree(v)

  elif d == 1:
    SPEDRel = 1
    for v in set(graph.getV()) - set(A):
      N = len(graph.neighbors(v) & set(A))
      SPEDRel = SPEDRel * binomial(N, k) * p ** k * (1 - p) ** (N - k)

  else:
    m = len(graph.getEdgeSet())
    esets = powerset(list(graph.getEdgeSet()))
    for F in esets:
      g = graph.spanningSubgraph(F)
      flag = True
      for v in set(graph.getV()) - set(A):
        if len(g.dist_d_neighbors(v, d) & set(A)) == k:
          pass
        else:
          flag = False
          break
      if flag:
        pow1 = m - len(F)
        SPEDRel = SPEDRel + p ** len(F) * (1 - p) ** pow1

  return SPEDRel

# =============================================================================
def decomp(graph, A, d=1, k=1, P=set([])):
  '''
  Calculates the SPEDRel with decomposition
  '''
  if len(graph.getEdgeSet()) == 0:
    if len(A) == len(graph.getV()):
      return 1
    else:
      return 0

  if len(A) == 0:
    return 0

  degs = graph.get_degrees()
  for v in graph.getV():
    if degs[v] == 0 and v in A:
      graph.deleteVertex(v)
      A.remove(v)
    elif degs[v] == 0 and v not in A:
      return 0

  SPEDRel = 0
  E = graph.getEdgeSet()
  E = E - P
  if len(E) == 0:
    g = graph.spanningSubgraph(P)
    for v in set(graph.getV()) - set(A):
      if len(g.dist_d_neighbors(v, d) & set(A)) == k:
        pass
      else:
        return 0
    return 1

  else:
    e = E.pop()
    g = graph.copy()
    g.deleteEdge(e)
    SPEDRel1 = decomp(g, deepcopy(A), d, k, deepcopy(P))
    P.add(e)
    SPEDRel2 = decomp(graph, deepcopy(A), d, k, deepcopy(P))
    SPEDRel = (1 - p) * SPEDRel1 + p * SPEDRel2

  return SPEDRel

# =============================================================================
def reduce_graph(graph, A, d=1, k=1):
  E = graph.getEdgeSet()
  del_edges = []

  for e in E:
    if e[0] not in A and e[1] not in A:
      del_edges.append(e)

  for e in del_edges:
    graph.deleteEdge(e)

  return graph


# =============================================================================
def compute_reliability(MDNetwork_log2, graph, A, m, d=1, k=1, q=None):
  '''
  Calculates the SPEDRel(G,A,d,k,p)
  @param graph: Graph
  @param A: Dominating vertices
  @param m: Method
  @param d: Domination distance
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("starting reliability computing...")
  MDNetwork_log.info("")
  MDNetwork_log.info("calculating SPEDRel(G, %s, %s, %s, p)" % (A, d, k))
  g = graph.copy()
  if d == 1:
    g = reduce_graph(g, A, d, k)

  rel = None
  if m == method.STATE_SPACE:
    if d == 1 and list(g.neighbors_set(A)) != g.getV():
      return 0

    rel = state_space_enumeration(g, A, d, k)

  elif m == method.DECOMP:
    rel = decomp(g, A, d, k)

  else:
    pass

  if q != None:
    q.put(rel)
  return sympy.simplify(rel)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
