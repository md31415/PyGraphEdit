#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the rank polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 09.12.2015

@author: Markus Dod

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isPath, isCycle)  # @UnresolvedImport
from SGraph.GraphFunctions import components  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x, y = symbols('x y')
D = None


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Computes the rank polynomial
  '''
  edges = powerset(list(graph.getEdgeSet()))
  B = 0
  n = graph.order()
  for S in edges:
    g = graph.spanningSubgraph(S)
    c = len(components(g))
    B = B + x**(n - c) * y**(len(S) - n + c)

  return B


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcPath(n):
  '''Calculates the rank polynomial of the path'''
  return (1 + x)**(n-1)

def calcCycle(n):
  '''Calculates the rank polynomial of the cycle'''
  return x**(n - 1) * (y - x) + (x + 1)**n

def calcStar(n):
  '''Calculate the rank polyomial of a star'''
  return (1 + x)**n

def calcWheel(n):
  '''
  Calculates the rank polynomial of the wheel graph
  TODO:: Anfangswerte stimmen noch net!
  '''
  from collections import deque
  poly = deque([2 * x + 1, 3*x**2 + 4*x + 1])
  if n <= 1:
    return 0
  elif n == 2:
    return 1 + x
  elif n == 3:
    return x**2 * (y - x) + (x + 1)**3
  elif n <= 6:
    return poly[n - 1]

  i = 3
  while i != n:
    i += 1
    poly.append((1+4*x+x*y)*poly[2] - x*(2*x+1)*(y+2)*poly[1] + x**2*(x+1)*(y+1)*poly[0])
    poly.popleft()

  return poly[2]

def calcBook(n):
  '''Calculates the rank polynomial of the book graph'''
  return ((1 + 3 * x * (x + 1))**n + x * (y + 1) * (1 + x * (3 + x *(3 + y)))**n) / y

def calcSunlet(n):
  '''Calculates the rank polynomial of the sunlet graph'''
  return (1 + x)**n * ((1 + x)**n + x**(n - 1) * (y - x))


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the rank polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing the rank polynomial ...")
  PyGraphEdit_log.info("")

  R = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      R = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      R = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      R = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      R = calcCycle(graph.order())
    elif graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("starting calculation with algorithm for stars ...")
      R = calcStar(graph.order())
    elif graph.graphtype == graphType.WHEEL:
      PyGraphEdit_log.info("starting calculation with algorithm for wheels ...")
      R = calcWheel(graph.order())
    elif graph.graphtype == graphType.BOOK:
      PyGraphEdit_log.info("starting calculation with algorithm for books ...")
      R = calcBook(graph.order() / 2 - 1)
    elif graph.graphtype == graphType.SUNLET:
      PyGraphEdit_log.info("starting calculation with algorithm for sunlets ...")
      R = calcSunlet(graph.order() / 2)

  if R:
    if q != None:
      q.put(R)
    PyGraphEdit_log.info("done ...")
    return expand(R)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration ...")
    R = compute_polynomial(graph)

  if q != None:
    q.put(R)

  PyGraphEdit_log.info("done ...")
  return expand(R)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
