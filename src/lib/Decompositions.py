#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 22.02.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from copy import deepcopy

from lib.tools import argmin, BinaryTree  # @UnresolvedImport
from SGraph.SGraph import SGraph  # @UnresolvedImport
from modules.consts import ob_type  # @UnresolvedImport


#=============================================================================
#================ BinaryTree-Decomposition =========================================
#=============================================================================
def get_tree_decomposition(graph):
  '''
  Baumzerlegung erzeugen
  Mit der min-fill-Heuristik wird eine Eleminationsordnung der Knoten erzeugt
  und anschlie�end mit Bucket-Elimination der Dekompositionsbaum aufgebaut
  '''
  g = graph.copy()
  s = minfill(g)
  B = {}
  for v in graph.getV():
    B[v] = set([])

  for e in graph.getEdgeSet():
    v = None
    for w in s:
      if w in e:
        v = w
        break
    B[v] = B[v] | set(e)

  E = set([])
  t = deepcopy(s)
  t.reverse()
  for i in range(graph.order(), 0, -1):
    A = B[s[i - 1]] - set([s[i - 1]])
    v = None
    for w in s:
      if w in A:
        v = w
        break
    if v != None:
      B[v] = B[v] | A
      E.add((s[i - 1], v))

  g = SGraph(deepcopy(list(B.keys())), E)
  for v in g.getV():
    if len(B[v]) == 1 and g.degree(v) == 1:
      g.deleteVertex(v)
  return g

def minfill(graph):
  '''Eleminationsordnung der Knoten mit der min-fill-Heuristik ermitteln'''
  s = []
  fcount = fillcount(graph)
  for i in range(1, graph.order() + 1):
    v = argmin(fcount)
    s.append(v)
    addFillEdges(graph, v, fcount)
  return s

def fillcount(graph):
  '''Zu jedem Knoten die Anzahl der nicht vorhandenen Kanten zwischen Nachbarknoten z�hlen'''
  fcount = {}
  for u in graph.getV():
    count = 0
    for v in graph.getAdja()[u]:
      for w in graph.getAdja()[u]:
        if v != w:
          if not graph.is_adjacent(v, w):
            count += 1
    fcount[u] = count
  return fcount

def addFillEdges(graph, v, fcount):
  '''Kanten zwischen allen adjazenten Knoten von v einf�gen und fcount aktualisieren'''
  for u in graph.getAdja()[v]:
    for w in graph.getAdja()[v]:
      if u != w:
        if not graph.is_adjacent(u, w):
          for x in graph.getAdja()[u]:
            if graph.is_adjacent(x, w):
              fcount[x] -= 1
            else:
              fcount[u] += 1
          for x in graph.getAdja()[w]:
            if not graph.is_adjacent(x, u):
              fcount[w] += 1
          graph.insertEdge(u, w)

  for u in graph.getAdja()[v]:
    for y in graph.getAdja()[u]:
      if y != v:
        if not graph.is_adjacent(v, y):
          fcount[u] -= 1

  graph.deleteVertex(v)
  del fcount[v]
  return fcount


#=============================================================================
#================ Path-Decomposition =========================================
#=============================================================================
def get_path_decomposition(graph, s, t):
  '''
  Wegzerlegung eines Graphen ermitteln
  @param graph: Graph
  @param s: Startknoten
  @param t: Endknoten
  @return: Wegzerlegung
  '''
  active_nodes = [s]
  decomposition = [s]
  g = graph.copy()
  while len(active_nodes) != 0:
    flag = False

    # �berpr�fen ob bei einem Knoten alle Kanten abgearbeitet sind
    deg = g.get_degrees()
    for i, v in enumerate(active_nodes):
      if len(active_nodes) == 1 or v != t:
        if deg[v] == 0:
          del active_nodes[i]
          decomposition.append(-v)
          g.deleteVertex(v)
          flag = True
          break

    # Kante hinzuf�gen
    if not flag:
      for v in active_nodes:
        for e in g.getAdja()[v]:
          if e in active_nodes:
            decomposition.append([v, e])
            g.deleteEdge([v, e])
            flag = True
            break
        if flag:
          break

    # Wenn keine Kante hinzugef�gt wurde dann neuen Knoten aufnehmen
    if not flag:
      for v in active_nodes:
        for w in g.getAdja()[v]:
          if not w in active_nodes and w != t:
            active_nodes.append(w)
            decomposition.append(w)
            flag = True
            break
        if flag:
          break
      if not flag:
        active_nodes.append(t)
        decomposition.append(t)

  return decomposition

def get_width(path_decomp):
  '''
  Weite einer Wegzerlegung bestimmen
  @param path_decomp: Wegzerlegung
  @return: Weite der Wegzerlegung
  '''
  decomp = set([])
  max_set = 0
  for v in path_decomp:
    if isinstance(v, int):
      if v > 0:
        decomp.add(v)
        if len(decomp) > max_set:
          max_set = len(decomp)
      else:
        decomp.remove(-v)

  return max_set - 1


#=============================================================================
#================ Clique-Decomposition =======================================
#=============================================================================
def generate_clique_decomposition(graph):
  '''
  Eine Cliquenzerlegung des Graphen erzeugen
  @param graph: Graph
  @return: Cliquenzerlegung
  '''
  neighbor = {}
  node_order = []
  max_label = 0

  # Nachbarschaft f�r alle Knoten abspeichern
  for v in graph.getV():
    neighbor[v] = graph.neighbors(v)

  # Reihenfolge der Knoten anhand der H�ufigkeit in den Nachbarschaften ermitteln
  flag = True
  while flag:
    v = best_node(neighbor)
    node_order.append((v, deepcopy(neighbor[v])))
    del neighbor[v]
    for w in neighbor.keys():
      try:
        neighbor[w].remove(v)
      except:
        pass

    flag = False
    for w in neighbor.keys():
      if len(neighbor[w]) > 0:
        flag = True
        break

  # Zerlegungsbaum aufbauen
  t = BinaryTree()
  labels = {}
  label = 1
  labels_in_use = []
  m = None

  # Die ersten zwei Knoten einf�gen
  if len(neighbor.keys()) >= 2:
    current_node = 3
    t.set_child(2, str(label), ob_type.NODE, "left")
    labels[list(neighbor.keys())[0]] = label
    labels_in_use.append(label)
    label += 1
    t.setVertex("", ob_type.PLUS, "left", (1, 3))
    t.set_child(2, str(label), ob_type.NODE, "right")
    labels[list(neighbor.keys())[1]] = label
    labels_in_use.append(label)
    label += 1
    # die restlichen Knoten ohne Kanten einf�gen
    for i in range(2, len(neighbor.keys())):
      current_node += 1
      t.setVertex("", ob_type.PLUS, "left", (current_node - 2, current_node + 1))
      t.set_child(current_node, str(label), ob_type.NODE, "right")
      labels[list(neighbor.keys())[i]] = label
      labels_in_use.append(label)
      label += 1
      current_node += 1
    m = 1

  else:
    current_node = 1
    t.setVertex(str(label), ob_type.NODE, "left", (None, None))
    labels[list(neighbor.keys())[0]] = label
    labels_in_use.append(label)
    m = 2

  if label > max_label:
    max_label = label

  # Die Knoten und Kanten aus node_order einf�gen
  while len(node_order) != 0:
    (v, e) = node_order.pop()

    # Als label f�r den aktuellen Knoten immer das kleinst m�gliche Label nehmen
    label = None
    n = 1
    while label == None:
      if n not in labels_in_use:
        label = n
      else:
        n += 1
    if label > max_label:
      max_label = label

    # Knoten hinzuf�gen
    current_node += 1
    if m == 1:
      t.setVertex("", ob_type.PLUS, "left", (current_node - 2, current_node + 1))
    else:
      t.setVertex("", ob_type.PLUS, "left", (current_node - 1, current_node + 1))
    t.set_child(current_node, str(label), ob_type.NODE, "right")
    labels[v] = label
    labels_in_use.append(label)
    current_node += 1

    # Kanten einf�gen
    edges_label = set([])
    for f in e:
      edges_label.add(labels[f])
    for i, edge in enumerate(edges_label):
      current_node += 1
      if i == 0:
        t.setVertex("%s-%s" % (labels[v], edge), ob_type.EDGE, "left", (current_node - 2, None))
      else:
        t.setVertex("%s-%s" % (labels[v], edge), ob_type.EDGE, "left", (current_node - 1, None))

    if len(node_order) == 0:
      t.set_root_number(current_node)

    # Relabeln
    if len(node_order) > 0:
      relabel = []
      flag = True
      v, adj = node_order[0]
      for w in adj:
        flag = True
        for v, e in node_order:
          if w in e:
            pass
          else:
            flag = False
            break
        if flag:
          relabel.append(w)

      if len(relabel) > 1:
        nach_label = labels[relabel[0]]
        for i in range(1, len(relabel)):
          if nach_label != labels[relabel[i]]:
            current_node += 1
            t.setVertex("%s-%s" % (str(labels[relabel[i]]), str(nach_label)), ob_type.RENAME, "left", (current_node - 1, None))
            try:
              labels_in_use.remove(labels[relabel[i]])
            except:
              pass
            labels[relabel[i]] = nach_label

    m += 1

  print("Cliquenweite: %s" % str(max_label))

  return t

def best_node(neighbors):
  '''
  Ermittelt den Knoten, der in den meisten Nachbarschaften vorkommt
  @param neighbors: Nachbarschaften der Knoten
  @return Knoten, der in den meisten Nachbarschaften vorkommt
  '''
  num = {}
  for k in neighbors.keys():
    num[k] = 0

  for k in neighbors.keys():
    for v in neighbors[k]:
      num[v] += 1

  max_value = -1
  w = None
  for v in num.keys():
    if num[v] > max_value:
      max_value = num[v]
      w = v

  return w


#=============================================================================
#=============================================================================
#=============================================================================
#=============================================================================
if __name__ == '__main__':

  # sys.exit("Hauptmodul zum Starten verwenden")

  # g = SGraph([1,2,3,4,5,6,7,8],[[1,2],[1,3],[2,3],[2,4],[3,4],[4,8],[8,5],[8,6],[5,6],[5,7],[6,7]])
  g = SGraph()
  # g = completeGraph(10)
  # datei = "../Data/graphs.xml"
  datei = "../Data/graphs.db"
  name = "Bridge"
  # name = "Bridgenetwork3"
  # name = "Petersengraph"
  # name = "6-acyclic"
  # name = "PC-graph"
  # name = "6-acyclic"
  # name = "8-regular"
  g.load_graphdb(datei, name)

  graph = get_path_decomposition(g, 1, 4)

  print(graph)

