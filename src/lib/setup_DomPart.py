'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
'''

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(name='DominatedPartitionsPoly',
      ext_modules=[Extension('DominatedPartitionsPoly', ['DominatedPartitionsPoly.py'])],
      cmdclass={'build_ext': build_ext})
