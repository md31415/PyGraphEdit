#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

from sympy import expand, symbols, simplify

from src.GraphFunctions import path, cycle, wheel, star

from lib.RankPoly import (compute_polynomial, calcPath, calcCycle,
                          calcStar, calcWheel)


x, y = symbols('x y')

def test_compute_polynomial():
  assert compute_polynomial(path(4)) == calcPath(4)
  assert compute_polynomial(path(5)) == calcPath(5)
  assert compute_polynomial(path(6)) == calcPath(6)
  assert compute_polynomial(path(7)) == calcPath(7)
  assert compute_polynomial(path(8)) == calcPath(8)
  assert compute_polynomial(path(9)) == calcPath(9)
  assert compute_polynomial(path(10)) == calcPath(10)

  assert compute_polynomial(cycle(4)) == calcCycle(4)
  assert compute_polynomial(cycle(5)) == calcCycle(5)
  assert compute_polynomial(cycle(6)) == calcCycle(6)
  assert compute_polynomial(cycle(7)) == calcCycle(7)
  assert compute_polynomial(cycle(8)) == calcCycle(8)
  assert compute_polynomial(cycle(9)) == calcCycle(9)
  assert compute_polynomial(cycle(10)) == calcCycle(10)

  assert compute_polynomial(wheel(5)) == calcWheel(5)
  assert compute_polynomial(wheel(6)) == calcWheel(6)
  assert compute_polynomial(wheel(7)) == calcWheel(7)
  assert compute_polynomial(wheel(8)) == calcWheel(8)

  assert compute_polynomial(star(4)) == calcStar(4)
  assert compute_polynomial(star(5)) == calcStar(5)
  assert compute_polynomial(star(6)) == calcStar(6)
  assert compute_polynomial(star(7)) == calcStar(7)
  assert compute_polynomial(star(8)) == calcStar(8)
  assert compute_polynomial(star(9)) == calcStar(9)


#calcCompleteGraph(n)
#calcCompleteBipartiteGraph(graph)
#calcPath(n)
#calcCycle(n)
#calcStar(n)
#calcWheel(n)
