'''
Implements the calculation of the rainbow polynomial and
the rainbow generating function for the usage in PyGraphEdit.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''
import cython

@cython.locals(s=cython.int)
cdef testRainbow(G, coloring)
cdef RainbowConnection(G, phi, int s)

@cython.locals(c=cython.int)
cdef SearchPath(G, int v, U, Z, phi)

cdef class Coloring:
  cpdef addColor(self, int v, int w, int color)
  cpdef getColor(self, int v, int w)

@cython.locals(i=cython.int)
cdef partition(set_)

@cython.locals(c=cython.int)
cdef calcRainbowGenFunction(graph)

@cython.locals(i=cython.int)
cdef calcRainbowPoly(graph)

@cython.locals(i=cython.int)
def calcCompleteGraph(int m)

cpdef calcPoly(PyGraphEdit_log2, graph, m=*, q=*)
cpdef calcGenFunction(PyGraphEdit_log2, graph, m=*, q=*)
