#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Tests for different graph polynomials

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

from sympy import symbols


p = symbols('p')
x, y, z, w = symbols('x y z w')


def SubgraphPoly(G, q=None):
  pass

def SubgraphEnumeratingPoly(graph, q=None):
  pass

def ExtendedSubgraphCountingPoly(graph, q=None):
  pass

def SubgraphComponentPoly(graph, q=None):
  pass

def GeneralSubgraphCountingPoly(graph, q=None):
  pass

def AllTermialReliability(graph, q=None):
  pass

def Reliability(graph, q=None):
  pass

def AlliancePoly(graph, q=None):
  pass

def GeneralDomPoly(graph):
  pass

def computeCutPoly(graph, q=None):
  pass

def computeBipartiteSubgraphPoly(graph, q=None):
  pass
