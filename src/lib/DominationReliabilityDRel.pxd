'''
Calculation of the domination polynomial and the domination reliability polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 08.10.2012

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import cython

@cython.locals(w=cython.int)
cdef state_space_enumeration(graph, X=*, Y=*)

@cython.locals(u=cython.int, flag=cython.bint)
cdef state_space_enumeration2(graph)

@cython.locals(v=cython.int, w=cython.int)
cdef domPoly(G, X, Y)

@cython.locals(k=cython.int)
cdef essential_calculation(graph)

@cython.locals(k=cython.int, u=cython.int)
cdef inline is_essential(graph, W)

cdef clique_decomposition(graph)

@cython.locals(v=cython.int, u=cython.int, i=cython.int)
cdef compute_node(tree, node)

@cython.locals(flag=cython.bint, i=cython.int, j=cython.int)
cdef compress_states(tmp_states)

@cython.locals(node=cython.int, i=cython.int, v=cython.int)
cdef delete_unused_states(tmp_states)

cdef class CliqueDecompState:
  cpdef __eq__(self, other)
  cpdef set_p(self, p)
  cpdef set_edge(self, e)
  cpdef change_name(self, int u, int v)

@cython.locals(n=cython.int)
cdef calculate_kCycle(graph, int k)

@cython.locals(i=cython.int)
cdef calculate_kPath(int n, int k)

@cython.locals(i=cython.int)
cdef kPath_oneSide(int n, int k)

@cython.locals(i=cython.int)
cdef kPath_twoSide(int n, int k)

@cython.locals(u=cython.int, v=cython.int, w=cython.int, i=cython.int)
cdef calculate_kTrees(graph, vertexorder=*)

cdef calculate_kStar(graph, int k):

cdef class TreeState:
  cpdef first_state(self)
  cpdef getu(self)
  cpdef getv(self)

  @cython.locals(tmp=cython.int)
  cpdef change(self)
  cpdef nextVertexu(self, int new_v)
  cpdef nextVertexv(self, int new_u)
  cpdef merge(self, other)
  cpdef addBacknode(self)
  cpdef addState(self, other)
  cpdef getRel(self)
  cpdef getRel_final(self)

@cython.locals(i=cython.int)
cdef calcPath(int n)

@cython.locals(i=cython.int)
cdef calcCycle(int n)
cdef calcFriendship(n)

@cython.locals(pages=cython.int)
cdef calcBook(n)

@cython.locals(n=cython.int)
cdef reltodom(DRel)

@cython.locals(n=cython.int)
cdef domtorel(D)

cpdef compute_reliability(MDNetwork_log2, graph, m=*, q=*)
