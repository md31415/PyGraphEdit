#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Some useful tools

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 23.03.2012
Most recent amendment: 30.05.2016

@author: Markus Dod
'''

from enum import Enum
from functools import reduce
import heapq
from heapq import nlargest
from itertools import combinations
from operator import itemgetter
from random import random, randint, choice
from sys import exit as sys_exit

# Try to load the combinatorial functions from ctools
try:
  import ctools.combinatorial as ext_combinatorial
  EXT_COMBINATORIAL = True
except:
  EXT_COMBINATORIAL = False


# =============================================================================
# =============================================================================
def binomial(n, k):
  '''
  Calculates the binomial coefficient of n over k
  '''
  if n < k:
    return 0
  elif n == k:
    return 1
  if k < 0:
    return 0
  elif k == 0:
    return 1

  if n <= 30 and EXT_COMBINATORIAL:
    return ext_combinatorial.binomial(n, k)

  from operator import mul
  if k > n - k:
    k = n - k
  return int(reduce(mul, range((n - k + 1), n + 1), 1) / reduce(mul, range(1, k + 1), 1))

def falling_factorial(n, k):
  '''
  Calculate the k-th. falling factorial of n
  '''
  if n == k or n - 1 == k:
    return factorial(n)

  result = 1
  for i in range(0, k):
    result *= (n - i)

  return result

def factorial(n):
  '''Calculates the factorial of n'''
  if n <= 12 and EXT_COMBINATORIAL:
    return ext_combinatorial.factorial(n)

  return reduce(lambda k, m:k * m, range(1, n + 1))

def stirlingSecondKind(n, k):
  '''Calculates the stirling number of second kind'''
  n1 = n
  k1 = k
  if n <= 0:
    return 1
  elif k <= 0:
    return 0
  elif n == 0 and k == 0:
    return -1
  elif n != 0 and n == k:
    return 1
  elif n < k:
    return 0
  else:
    return (k1 * (stirlingSecondKind(n1 - 1, k1))) + stirlingSecondKind(n1 - 1, k1 - 1)

def argmax(d):
  '''Returns the maximum item of a dict'''
  return max(d.items(), key=itemgetter(1))[0]

def argmin(d):
  '''Returns the minimum item of a dict'''
  return min(d.items(), key=itemgetter(1))[0]

def arg(d, e):
  '''Returns the key where a value is included in the item
  @param d: Dict
  @param e: Value
  @return: Key where the value occur 
  '''
  for key in d.keys():
    if e in d[key]:
      return key

def get_values(d):
  '''Get the values of a dict as a set. The items must be convertable in a set'''
  values = set([])
  for key in d:
    values = values.union(set(d[key]))
  return values

def partition(set_):
  '''Iterativ function to generate the partitions of a set'''
  if not set_:
    yield []
    return
  for i in range(int(2**len(set_) / 2)):
    parts = [set(), set()]
    for item in set_:
      parts[i&1].add(item)
      i >>= 1
    for b in partition(parts[1]):
      yield [parts[0]] + b

def partition_kBlocks(ns, k):
  '''
  Generate all partitions of ns with k blocks
  Implements the algortihm described by Knuth 
  in the Art of Computer Programming, Volume 4, Fascicle 3B
  @param ns: List
  @param k: Number of blocks
  @return: A list of all partitions with k blocks
  '''
  def visit(n, a):
    ps = [[] for _ in range(k)]
    for j in range(n):
      ps[a[j + 1]].append(ns[j])
    return ps

  def f(mu, nu, sigma, n, a):
    if mu == 2:
      yield visit(n, a)
    else:
      for v in f(mu - 1, nu - 1, (mu + sigma) % 2, n, a):
        yield v
    if nu == mu + 1:
      a[mu] = mu - 1
      yield visit(n, a)
      while a[nu] > 0:
        a[nu] = a[nu] - 1
        yield visit(n, a)
    elif nu > mu + 1:
      if (mu + sigma) % 2 == 1:
        a[nu - 1] = mu - 1
      else:
        a[mu] = mu - 1
      if (a[nu] + sigma) % 2 == 1:
        for v in b(mu, nu - 1, 0, n, a):
          yield v
      else:
        for v in f(mu, nu - 1, 0, n, a):
          yield v
      while a[nu] > 0:
        a[nu] = a[nu] - 1
        if (a[nu] + sigma) % 2 == 1:
          for v in b(mu, nu - 1, 0, n, a):
            yield v
        else:
          for v in f(mu, nu - 1, 0, n, a):
            yield v

  def b(mu, nu, sigma, n, a):
    if nu == mu + 1:
      while a[nu] < mu - 1:
        yield visit(n, a)
        a[nu] = a[nu] + 1
      yield visit(n, a)
      a[mu] = 0
    elif nu > mu + 1:
      if (a[nu] + sigma) % 2 == 1:
        for v in f(mu, nu - 1, 0, n, a):
          yield v
      else:
        for v in b(mu, nu - 1, 0, n, a):
          yield v
      while a[nu] < mu - 1:
        a[nu] = a[nu] + 1
        if (a[nu] + sigma) % 2 == 1:
          for v in f(mu, nu - 1, 0, n, a):
            yield v
        else:
          for v in b(mu, nu - 1, 0, n, a):
            yield v
      if (mu + sigma) % 2 == 1:
        a[nu - 1] = 0
      else:
        a[mu] = 0
    if mu == 2:
      yield visit(n, a)
    else:
      for v in b(mu - 1, nu - 1, (mu + sigma) % 2, n, a):
        yield v

  n = len(ns)
  a = [0] * (n + 1)
  for j in range(1, k + 1):
    a[n - k + j] = j - 1
  return f(k, n, 0, n, a)

def powerset(seq):
  '''Returns all the subsets of the list. This is a generator.'''
  if len(seq) == 0:
    yield seq
  if len(seq) == 1:
    yield seq
    yield []
  elif len(seq) > 1:
    for item in powerset(seq[1:]):
      yield [seq[0]] + item
      yield item

def powerset_list(lst):
  '''
  Returns the powerset of the list.
  Better use powerset!
  '''
  return reduce(lambda result, x: result + [subset + [x] for subset in result], lst, [[]])

def kpowerset(lst, k):
  '''Returns the all powersets of lst with k elements'''
  return list(combinations(lst, k))

def kpowersetW(lst, k, I):
  '''Returns the powerset with k elements without the elements of I'''
  return [s for s in kpowerset(lst, k) if s not in I]

def minkpowerset(lst, k):
  '''Returns the all powersets of lst with at least k elements'''
  pset = reduce(lambda result, x: result + [subset + [x] for subset in result], lst, [[]])
  return [s for s in pset if len(s) >= k]

def maxkpowerset(lst, k):
  '''Returns the all powersets of lst with at most k elements'''
  pset = reduce(lambda result, x: result + [subset + [x] for subset in result], lst, [[]])
  return [s for s in pset if len(s) <= k]

def randomUniqueSubset(seq, m):
  '''Return m unique elements from seq. seq can be a multiset.'''
  targets = set()
  while len(targets) < m:
    x = choice(seq)
    targets.add(x)
  return targets

def is_equal(A, B):
  '''Test if two lists are equal'''
  if len(A) != len(B):
    return False
  for a in A:
    if a not in B:
      return False

  return True

def is_subset(A, B):
  '''
  Tests if the list A is a sublist of B
  @param A: First list
  @param B: Second list
  '''
  if len(A) > len(B):
    return False

  for a in A:
    if not a in B:
      return False

  return True

def is_true_subset(A, B):
  '''
  Tests if the list A is a true sublist of B
  @param A: First list
  @param B: Second list
  '''
  if len(A) >= len(B):
    return False

  for a in A:
    if not a in B:
      return False

  return True

def intersection(A, B):
  '''
  Calculates the intersection of two lists
  @param A: First list
  @param B: Second list
  @return: Intersection
  '''
  return [a for a in A if a in B]

def union(A, B):
  '''
  Calculate the union of two lists
  @param A: First list
  @param B: Second list
  @return: List of the union
  '''
  return list(set(A) | set(B))

def munion(A, B):
  '''
  Calculate the union of two lists (Multisets)
  @param A: First list
  @param B: Second list
  @return: List of the union
  '''
  C = deepcopy(A)
  C.extend(B)
  return C

def diff(A, B):
  '''
  Calculates the difference A-B
  '''
  return [v for v in A if v not in B]

def dict_union(A, B):
  '''
  Create the union of two dictionaries.
  Only the keys will be checked!
  @param A: First dict
  @param B: Second dict
  '''
  for v in B.keys():
    if not v in A.keys():
      A[v] = B[v]
    else:
      pass

def m_add(A, B):
  '''
  Extended set addition r({aUb: a in A, b in B})
  @param A: First set
  @param B: Second set
  '''
  w = []
  U = set([])
  V = set([])
  for s in A:
    U.clear()
    for i in s:
      U.add(i)

    for t in B:
      V.clear()
      for j in t:
        V.add(j)
      W = U.union(V)
      W = sorted(W)

      w.append("".join(W))

  w = clean_list(w)

  return w

def clean_list(w):
  '''
  Remove sets which contain another element as subset
  @param w: List of sets
  '''
  r = set([])
  for e in w:
    for i, f in enumerate(w):
      if e != f:
        flag = True
        for s in e:
          if not s in f:
            flag = False
            break
        if flag:
          r.add(i)

  r_new = sorted(r)

  for i in range(len(r_new) - 1, -1, -1):
    del w[r_new[i]]

  return w

def m_multi(A, B):
  '''
  Extended set multiplication r({aUb: a in A, b in B})
  @param A: First set
  @param B: Second set
  '''
  U = A[:]
  for x in B:
    if not x in U:
      U.append(x)

  for i in range(len(U) - 1, -1, -1):
    if len(U[i]) == 0:
      del U[i]

  U = clean_list(U)

  return U

def determinant(M, n=None):
  '''
  Calculates the determinant of a matrix
  @param M: Matrix (linked list [[],[],...])
  @param n: Size of the matrix or the submatrix
  '''
  use_numpy = True
  try:
    from numpy import array, linalg, vstack
  except:
    print("No numpy installed, using own methods...")
    print("Please install numpy for faster calculation.")
    use_numpy = False

  if n == None:
    n = len(M)

  if use_numpy:
    matrix = array(M[0][:n])
    for i in range(1, n):
      matrix = vstack([matrix, M[i][:n]])
    return linalg.det(matrix)

  else:
    if n > 2:
      i = 1
      t = 0
      det = 0
      while t <= n - 1:
        d = {}
        t1 = 1
        while t1 <= n - 1:
          m = 0
          d[t1] = []
          while m <= n - 1:
            if m == t:
              pass
            else:
              d[t1].append(M[t1][m])
            m += 1
          t1 += 1
        l1 = [d[x] for x in d]
        det = det + i * M[0][t] * determinant(l1)
        i = i * -1
        t += 1
      return det
    else:
      return M[0][0] * M[1][1] - M[0][1] * M[1][0]


# ============================================================
# ============= PRIORITY QUEUE ===============================
# ============================================================
class PriorityQueue:
  '''Implements a priority queue'''
  def __init__(self):
    self.elements = []

  def empty(self):
    return len(self.elements) == 0

  def put(self, item, priority):
    heapq.heappush(self.elements, (priority, item))

  def get(self):
    return heapq.heappop(self.elements)[1]


# ============================================================
# ============= ENUM =========================================
# ============================================================
class enum(Enum):
  '''
  An enum class with auto numbering of the elements
  With python 3.6 may could simply use entry = auto()
  '''
  def __new__(cls):
    value = len(cls.__members__) + 1
    obj = object.__new__(cls)
    obj._value_ = value
    return obj


# ============================================================
# ============= PARTITION ====================================
# ============================================================
class Partition():
  '''
  Implements a partition of objects (objects must be static).
  '''
  def __init__(self, blocks=[], elements=[]):
    self.p = {}
    if len(blocks) != 0:
      for i, p in enumerate(blocks):
        self.addElement(p, elements[i])

  def addElement(self, block, element):
    '''
    Add an element
    @param block: Add the element in block
    @param element: Element to add
    '''
    if block in self.p.keys():
      self.p[block].add(element)
    else:
      self.p[block] = set([element])

  def addElementNewBlock(self, element):
    '''
    Add an Element to the partition in a new block
    @param element: New Element
    '''
    try:
      m = max(list(self.p.keys()))
    except:
      m = 0
    for i in range(1, m + 2):
      if i not in self.p.keys():
        self.p[i] = set([element])
        break

  def getElements(self):
    '''Return the elements of the partition'''
    E = set([])
    for e in self.p.values():
      E |= e
    return list(E)

  def getElementsInBlock(self, block):
    '''
    Get the elements in a specific block
    @param block: Block of the partition
    '''
    if block in self.p.keys():
      return self.p[block]
    else:
      return []

  def removeElement(self, element):
    '''
    Remove an element from the partition
    @param element: Element of the partition
    '''
    for key in self.p.keys():
      if element in self.p[key]:
        self.p[key].remove(element)
        break

  def komponent(self, element):
    '''
    Return in which block an element is
    @param element: Element of the partition
    '''
    for key in self.p.keys():
      if element in self.p[key]:
        return key
    return 0

  def unionBlocks(self, block1, block2):
    '''
    Meld to blocks
    @param block1: First block
    @param block2: Second block
    '''
    if block1 not in self.p.keys() or block2 not in self.p.keys():
      return

    self.p[block1] = self.p[block1].union(self.p[block2])
    del self.p[block2]

  def moveElement(self, element, toblock):
    '''
    Move an element to an other block
    @param element: Element of the partition
    @param toblock: New block of the element
    '''
    k = self.komponent(element)
    self.p[k].remove(element)
    self.p[toblock].add(element)

  def size(self, block):
    '''
    Returns the size of a block
    @param block: Block of the partition
    '''
    try:
      return len(self.p[block])
    except:
      return None

  def __eq__(self, other):
    '''
    Test if self is equal to other
    @param other: Partition
    '''
    if len(self.getElements()) == 0 and len(other.getElements()) == 0:
      return True
    if len(self.getElements()) != len(other.getElements()):
      return False
    for key in self.p:
      flag = False
      for key2 in other.p:
        if self.p[key] == other.p[key2]:
          flag = True
          break
      if not flag:
        return False
    return True

  def toString(self):
    '''Convert the partition to a string'''
    return str(self)

  def __str__(self):
    '''String representation of the partition'''
    p_str = "|"
    for block in self.p:
      p_str += "%s|" % self.p[block]
    return p_str


# ============================================================
# ============ MARKED PARTITION ==============================
# ============================================================
class MarkedPartition(Partition):
  '''
  Implements a marked partition of objects (objects must be static).
  The markedSet can have two marks
  '''
  def __init__(self, blocks=[], elements=[], mark={}):
    Partition.__init__(self, blocks, elements)
    if len(mark) == 0:
      self.mark = {}
      for element in self.getElements():
        self.mark[element] = [0, 0]
    else:
      self.mark = mark

  def addElement(self, block, element, mark=[0, 0]):
    '''
    Add an element
    @param block: Add the element in block
    @param element: Element to add
    @param mark: List of length two with marker (default=[0,0])
    '''
    Partition.addElement(self, block, element)
    self.mark[element] = mark

  def addElementNewBlock(self, element, mark=[0, 0]):
    '''
    Add an Element to the partition in a new block
    @param element: New Element
    @param mark: List of length two with marker (default=[0,0])
    '''
    Partition.addElementNewBlock(self, element)
    self.mark[element] = mark

  def numBlocks(self):
    '''Returns the number of different blocks'''
    return len(self.p.keys())

  def removeElement(self, element):
    '''
    Remove an element from the partition
    @param element: Element of the partition
    '''
    Partition.removeElement(self, element)
    del self.mark[element]

  def __eq__(self, other):
    '''
    Test if self is equal to other
    @param other: Partition
    '''
    if Partition.__eq__(self, other) and self.markEqual(other):
      return True
    else:
      return False

  def markEqual(self, other):
    '''
    Test if the marks of two partitions are equal
    @param other: Partition
    '''
    if self.mark.keys() != other.mark.keys():
      return False
    else:
      for key in self.mark.keys():
        if not (self.mark[key][0] == other.mark[key][0] and self.mark[key][1] == other.mark[key][1]):
          return False
    return True

  def markElement(self, e, bit):
    '''
    Mark the bit bit of the element e
    @param e: Element of the partition
    @param bit: Set the bit to True
    '''
    if e not in self.p:
      return False

    if self.mark[e][bit] == 0:
      self.mark[e][bit] = 1

  def isMarked(self, e, bit):
    '''
    Check if the element is marked with bit
    @param e: Element of the partition
    @param bit: Marked bit
    '''
    if e not in self.p or self.mark[e][bit] == 0:
      return False
    else:
      return True

  def copy(self):
    '''
    Return a copy of this partition
    '''
    other = MarkedPartition()
    other.p = self.p
    other.mark = self.mark
    return other

  def __str__(self):
    '''Return a string representation of this partition'''
    return "%s -- %s" % (str(self.p), str(self.mark))


# ============================================================
# ============== MARKED SET ==================================
# ============================================================
class MarkedSet(set):
  '''Class for a set with marked elements'''
  def __init__(self, elements=set([]), mark={}):
    set.__init__(self, elements)
    self.mark = {}
    for e in elements:
      if e in mark:
        self.mark[e] = mark[e]
      else:
        self.mark[e] = [0, 0]

  def addElement(self, element, mark=None):
    '''
    Add an element to the set
    @param element: New Element
    @param mark: Mark of the new element
    '''
    self.add(element)
    if mark == None:
      self.mark[element] = [0, 0]
    else:
      self.mark[element] = mark

  def removeElement(self, element):
    '''
    Remove an element from the set
    @param element: Element of the set
    '''
    if element not in self:
      return False
    self.remove(element)
    del self.mark[element]

  def getElements(self):
    '''Return the elements of the set'''
    return set(self)

  def markElement(self, e, bit):
    '''
    Mark the bit of the element e
    @param e: Element of the set
    @param bit: Bit to mark
    '''
    if e not in self:
      return False
    if self.mark[e][bit] == 0:
      self.mark[e][bit] = 1

  def isMarked(self, e, bit):
    '''
    Check if the bit bit is marked
    @param e: Element of the set
    @param bit: Bit
    '''
    if self.mark[e][bit] == 0:
      return False
    else:
      return True

  def __eq__(self, other):
    '''
    Check if to marked sets are equal
    @param other: Other MarkedSet
    '''
    if set(self) == set(other) and self.mark == other.mark:
      return True
    else:
      return False

  def __str__(self):
    '''Returns the string representation of this set'''
    return "%s -- %s" % (set.__str__(self), str(self.mark))


# ============================================================
# =============== MULTISET ===================================
# ============================================================
class mset(object):
  '''
  Multiset class
  '''
  def __init__(self, iterable=()):
    self._data = {}  # Dict with the informations about the multiplicity
    self._len = 0  # Number of elements in the multiset
    self.union(iterable)

  def rename(self, u, v):
    '''Rename the element u in v'''
    if v in self._data and u in self._data:
      self._data[v] += self._data[u]
      del self._data[u]
    elif u in self._data:
      self._data[v] = self._data[u]
      del self._data[u]

  def union(self, iterable):
    '''
    Calculate the union with an iterable object
    '''
    if isinstance(iterable, dict):
      for elem, n in iterable.iteritems():
        self[elem] += n
    else:
      for elem in iterable:
        self[elem] += 1

  def add(self, elements):
    '''
    Add an list of elements or an single element to the set
    @param elements: List or int
    '''
    if type(elements) == list:
      for el in elements:
        if el in self._data:
          self._data[el] += 1
        else:
          self._data[el] = 1
    elif type(elements) == int:
      if elements in self._data:
        self._data[elements] += 1
      else:
        self._data[elements] = 1
    else:
      raise TypeError

  def remove(self, element):
    '''
    Remove an element from the set
    @param element: Element of the set
    '''
    if element in self._data:
      if self._data[element] > 1:
        self._data[element] -= 1
      else:
        del self._data[element]
    else:
      return False

  def remove_all(self, element):
    '''
    Remove all elements of type element
    @param element: Element of the set
    '''
    if element in self._data:
      del self._data[element]
    else:
      return False

  def get_elements(self):
    '''Return the elements of the set'''
    return self._data.keys()

  def get_num_element(self, element):
    '''
    Return the multiplicity of the element in the set
    @param element: Element of the set
    '''
    if element in self._data:
      return self._data[element]
    else:
      return 0

  def __contains__(self, element):
    '''
    Check if the element is included in the set
    @param element: Element
    '''
    return element in self._data

  def __getitem__(self, element):
    '''
    Return the multiplicity of the element
    @param element: Element of the set
    '''
    return self._data.get(element, 0)

  def __setitem__(self, element, n):
    '''
    Set the multiplicity of the element
    @param element: Element of the set
    @param n: Multiplicity of the element
    '''
    self._len += n - self[element]
    self._data[element] = n
    if n == 0:
      del self._data[element]

  def __delitem__(self, element):
    '''
    Delete an element of the set
    @param element: Element of the set
    '''
    if element not in self._data:
      return False
    self._len -= self[element]
    del self._data[element]

  def __len__(self):
    '''Returns the number of elements of the multiset'''
    return self._len

  def __eq__(self, other):
    '''Check if two multisets are equal'''
    if not isinstance(other, mset):
      return False
    return self._data == other._data

  def __ne__(self, other):
    '''Check if two multisets are not equal'''
    if not isinstance(other, mset):
      return True
    return self._data != other._data

  def __hash__(self):
    '''Return the hash value of the multiset'''
    raise TypeError

  def __repr__(self):
    '''Returns the string representation of the set'''
    s = ""
    for k in self._data.keys():
      for i in range(self._data[k]):
        if len(s) != 0:
          s += ", "
        s += str(k)
    return '{%s}' % s

  def copy(self):
    '''Returns a copy of this instance'''
    return self.__class__(self)

  __copy__ = copy  # For the copy module

  def __deepcopy__(self, memo):
    '''Deepcopy of self'''
    result = self.__class__()
    memo[id(self)] = result
    data = result._data
    result._data = deepcopy(self._data, memo)
    result._len = self._len
    return result

  def __getstate__(self):
    return self._data.copy(), self._len

  def __setstate__(self, data):
    self._data = data[0].copy()
    self._len = data[1]

  def clear(self):
    '''Remove all elements'''
    self._data.clear()
    self._len = 0

  def __iter__(self):
    '''Iter throw the multiset'''
    for elem, cnt in self._data.items():
      for i in range(cnt):
        yield elem

  def iterunique(self):
    '''Iter throw the single elements without multiplicity'''
    return self._data.iterkeys()

  def itercounts(self):
    '''Iter throw the multiplicity of the elements'''
    return self._data.items()

  def mostcommon(self, n=None):
    '''
    Returns a list with tuples of the elements with their multiplicity
    [(element, multiplicity),...]
    @param n: Return the first n elements with the highest multiplicity (default: None (return all))
    '''
    if n is None:
      return sorted(self.itercounts(), key=itemgetter(1), reverse=True)
    it = enumerate(self.itercounts())
    nl = nlargest(n, ((cnt, i, elem) for (i, (elem, cnt)) in it))
    return [(elem, cnt) for cnt, i, elem in nl]


# ============================================================
# ============== BINARY TREE =================================
# ============================================================
class BinaryTree():
  '''
  Class for a binary tree
  '''
  def __init__(self):
    self.vertices = []
    self.root = None
    self.last_number = 0

  def set_root(self, data, vertex_type):
    '''Insert a vertex at the root of the tree'''
    if self.root == None:
      self.last_number += 1
      vertex = BinaryTreeVertex(data, vertex_type)
      vertex.number = self.last_number
      self.vertices.append(vertex)
      self.root = self.last_number
    else:
      print("root-vertex already exist")

  def set_root_number(self, number):
    self.root = number

  def get_root(self):
    '''Return the root of the tree'''
    return self.vertices[self.get_index(self.root)]

  def set_child(self, father, data, vertex_type, direction):
    '''
    Set the child of an node
    @param father: Father of the new node
    @param data: Data of the new node
    @param vertex_type: Type of the new node
    @param direction: Left or right
    '''
    child = BinaryTreeVertex(data, vertex_type)
    child.father = father
    self.last_number += 1
    child.number = self.last_number
    self.vertices.append(child)

    if self.get_index(father) != None:
      if direction == "right":
        self.vertices[self.get_index(father)].right = child.number
      else:
        self.vertices[self.get_index(father)].left = child.number

  def setVertex(self, data, vertex_type, direction=None, child=None):
    vertex = BinaryTreeVertex(data, vertex_type)
    vertex.father = None
    self.last_number += 1
    vertex.number = self.last_number
    if child != None:
      if child[0] != None:
        vertex.left = child[0]
        if self.get_index(child[0]) != None:
          self.vertices[self.get_index(child[0])].father = vertex.number
      if child[1] != None:
        vertex.right = child[1]
        if self.get_index(child[1]) != None:
          self.vertices[self.get_index(child[1])].father = vertex.number
    self.vertices.append(vertex)

  def get_child(self, father, direction):
    '''Get the child of a node in the given direction'''
    child = None
    if direction == "right":
      child = self.vertices[self.get_index(father)].right
    else:
      child = self.vertices[self.get_index(father)].left

    if child != None:
      return self.vertices[self.get_index(child)]
    else:
      sys_exit("none child in this direction")

  def get_father(self, child):
    '''Returns the father of a node'''
    father = self.vertices[self.get_index(child)].father
    if father != None:
      return self.vertices[self.get_index(father)]
    else:
      sys_exit("none father")

  def get_leaf(self):
    vertex = self.vertices[self.get_index(self.root)]
    while vertex.left != None:
      vertex = self.vertices[self.get_index(vertex.left)]

    return vertex

  def get_index(self, number):
    for i, v in enumerate(self.vertices):
      if v.number == number:
        return i

  def __repr__(self):
    s = "%s %s [%s, %s, %s]\n" % ("Number", "Data", "Father", "LeftChild", "RightChild")
    for n in self.vertices:
      s += "%s\n" % str(n)
    return s

class BinaryTreeVertex():
  '''Class for a vertex in the binary tree'''
  def __init__(self, data=None, vertex_type=None):
    self.data = data
    self.vertex_type = vertex_type
    self.number = None
    self.left = None
    self.right = None
    self.father = None

  def __repr__(self):
    return "[%2s]: %5s %7s [%-5s,%-5s,%-5s]" % (str(self.number), self.data, \
                                                self.vertex_type, str(self.father).center(5), \
                                                 str(self.left).center(5), str(self.right).center(5))


# ============================================================
# ================ ROOTED TREE ===============================
# ============================================================
class Tree():
  '''
  Class for a rooted tree
  '''
  def __init__(self):
    self.vertices = []  # List of vertices of type TreeVertex()
    self.vertexnumbers = []  # List with the numbers of the vertices
    self.root = None  # Root of the tree

  def fromSGraph(self, g):
    '''
    Generates a tree from a SGraph
    @param g: SGraph
    '''
    self.set_root(number=1)
    while len(self.vertices) != g.order():
      for v in self.vertices:
        flag = False
        for w in g.getV():
          if v.number != w and w not in self.vertexnumbers and g.is_adjacent(v.number, w):
            self.set_child(v.number, str(w), w)
            flag = True
        if flag:
          break

  def set_root(self, data=None, number=0):
    '''Set the root of the tree'''
    if self.root == None:
      vertex = TreeVertex(data, number)
      self.vertices.append(vertex)
      self.vertexnumbers.append(number)
      self.root = number
    else:
      print("root-vertex already exist")

  def set_root_number(self, number):
    self.root = number

  def get_root(self):
    '''Returns the root of the tree'''
    return self.vertices[self.get_index(self.root)]

  def set_child(self, father, data, number):
    '''
    Insert a new vertex as the child of a given vertex
    @param father: Father of the new vertex
    @param data: Data of the new vertex
    @param number: Number of the new vertex
    '''
    child = TreeVertex(data, number)
    child.father = father
    self.vertices.append(child)
    self.vertexnumbers.append(number)

    if self.get_index(father) != None:
      self.vertices[self.get_index(father)].childs.append(child.number)

  def setVertex(self, data, number):
    '''
    Insert a new vertex
    @param data: Data of the new vertex
    @param number: Number of the new vertex
    '''
    vertex = TreeVertex(data, number)
    vertex.father = None
    self.vertices.append(vertex)
    self.vertexnumbers.append(number)

  def getChilds(self, father):
    '''Returns the childs of a given vertex'''
    return self.vertices[self.get_index(father)].childs

  def getVertex(self, number):
    '''Returns the vertex of a given number'''
    return self.vertices[self.get_index(number)]

  def get_father(self, vertex):
    '''
    Returns the father of a gives vertex
    @param vertex: Vertex of the tree
    '''
    father = self.vertices[self.get_index(vertex)].father
    if father != None:
      return self.vertices[self.get_index(father)]
    else:
      return None

  def get_leaf(self):
    '''Get a leaf of the tree'''
    vertex = self.vertices[self.get_index(self.root)]
    while len(vertex.childs) != 0:
      vertex = self.vertices[self.get_index(vertex.childs[0])]
    return vertex

  def get_index(self, number):
    for i, v in enumerate(self.vertices):
      if v.number == number:
        return i

  def __repr__(self):
    '''Returns a string representation of the tree'''
    s = "%s %s [%s, %s]\n" % ("Number", "Data", "Father", "Childs")
    for n in self.vertices:
      s += "%s\n" % str(n)
    return s

class TreeVertex():
  '''Class for the vertex of a tree'''
  def __init__(self, data=None, number=0):
    self.data = data
    self.number = number
    self.childs = []
    self.father = None

  def __repr__(self):
    return "[%2s]: %5s [%-5s,%-5s]" % (str(self.number), self.data, \
                                                str(self.father).center(5), \
                                                str(self.childs).center(5))


# ============================================================
def RandomTree(n, k):
  '''Generates a random binary tree'''
  op_type = enum(NODE="node", EDGE="edge", RENAME="rename", PLUS="+")

  t = BinaryTree()
  (u, v) = get_two_nodes(k)
  t.set_root("%s-%s" % (str(u), str(v)), op_type.EDGE)
  current_node = 1
  father = 1

  while n != 0:
    if n == 2:
      (u, v) = get_two_nodes(k)
      t.set_child(father, "", op_type.PLUS, "left")
      current_node += 1
      father = current_node
      t.set_child(father, str(u), op_type.NODE, "left")
      t.set_child(father, str(v), op_type.NODE, "right")
      break

    r = random()
    if r < 0.5:
      t.set_child(father, "", op_type.PLUS, "left")
      current_node += 1
      father = current_node
      v = randint(1, k)
      t.set_child(father, str(v), op_type.NODE, "right")
      current_node += 1
      n = n - 1
    elif r < 0.65:
      (u, v) = get_two_nodes(k)
      t.set_child(father, "%s-%s" % (str(u), str(v)), op_type.RENAME, "left")
      current_node += 1
      father = current_node
    else:
      (u, v) = get_two_nodes(k)
      t.set_child(father, "%s-%s" % (str(u), str(v)), op_type.EDGE, "left")
      current_node += 1
      father = current_node

  return t

def testEdges(node, k):
  op_type = enum(NODE="node", EDGE="edge", RENAME="rename", PLUS="+")
  global NODES_IN_USE
  global tree

  if node.vertex_type == op_type.NODE:
    NODES_IN_USE.add(int(node.data))

  elif node.vertex_type == op_type.PLUS:
    tmp_states2 = testEdges(tree.get_child(node.number, "right"), k)
    testEdges(tree.get_child(node.number, "left"), k)

  elif node.vertex_type == op_type.RENAME:
    s = node.data.split("-")
    u = int(s[0])
    v = int(s[1])

    testEdges(tree.get_child(node.number, "left"), k)

    while u not in NODES_IN_USE or v not in NODES_IN_USE:
      if len(NODES_IN_USE) == 1:
        u = list(NODES_IN_USE)[0]
        v = u
        break
      elif len(NODES_IN_USE) == 2:
        w = list(NODES_IN_USE)
        u = w[0]
        v = w[1]
        break
      (u, v) = get_two_nodes(k)
    node._data = "%s-%s" % (str(u), str(v))

    NODES_IN_USE.add(v)
    NODES_IN_USE.discard(u)

  elif node.vertex_type == op_type.EDGE:
    s = node.data.split("-")
    u = int(s[0])
    v = int(s[1])

    testEdges(tree.get_child(node.number, "left"), k)

    while u not in NODES_IN_USE or v not in NODES_IN_USE:
      if len(NODES_IN_USE) == 1:
        u = list(NODES_IN_USE)[0]
        v = u
        break
      elif len(NODES_IN_USE) == 2:
        w = list(NODES_IN_USE)
        u = w[0]
        v = w[1]
        break
      (u, v) = get_two_nodes(k)
    node._data = "%s-%s" % (str(u), str(v))

  else:
    sys_exit("Typ unbekannt")

  return True

def get_two_nodes(k):
  '''
  Return two random numbers between 1 and k
  '''
  if k <= 1:
    return (False, False)
  if k == 2:
    return (1, 2)

  u = randint(1, k)
  v = u
  while u == v:
    v = randint(1, k)
  return (u, v)


# ============================================================
# ============================================================
# ============================================================
if __name__ == '__main__':
  sys_exit("Do not use this module directly!")
