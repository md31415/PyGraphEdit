'''
Implements the calculation of the two types of the dominated
partition polynomials for the usage in PyGraphEdit.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import cython

@cython.locals(flag=cython.bint, flag2=cython.bint, v=cython.int)
cdef calcDominatedPartPoly_d(graph)

@cython.locals(k=cython.int, j=cython.int)
cdef calcPath_d(int n)

@cython.locals(i=cython.int, j=cython.int)
cdef calcBipartite_d(int n)
cdef calcStar_d(int n)

@cython.locals(flag=cython.bint, flag2=cython.bint, v=cython.int)
cdef calcDominatedPartPoly_e(graph)

@cython.locals(i=cython.int)
cdef calcPath_e(int n)
cdef inline d(int n)

cpdef calcPoly_d(MDNetwork_log, graph, m=*, q=*)
cpdef calcPoly_e(graph, q=*)
