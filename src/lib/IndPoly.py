#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the independence polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 04.12.2015

@author: Markus Dod

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isComplete, isPath, isCycle,  # @UnresolvedImport
                                  isCompleteBipartite, getBipartition)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x = symbols('x')
D = None


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the independent domination polynomial using complete enumeration
  @param graph: Graph
  '''
  P = 0
  for W in powerset(graph.getV()):
    if is_ind(graph, W):
      P = x ** len(W) + P

  return P

def is_ind(graph, W):
  '''
  Decides if the set W is a independent set
  @param graph: Graph
  @param W: Vertex subset
  @return: Bool
  '''
  if len(W) == 0:
    return True

  W = set(W)
  for v in W:
    if len(W.intersection(graph.neighbors(v))) != 0:
      return False
  return True

def IndPoly(G):
  """Returns the independence polynomial of G."""
  def Ind(G):
    if G.order() == 0:
      return 1
    elif G.order == 1:
      return x + 1
    else:
      m = -1
      for u in G.getAdja().keys():
        d = G.degree(u)
        if d > m:
          v = u
          m = d
      return x * Ind(G - (G.neighbors(v) | {v})) + Ind(G - v)

  I = Ind(G)
  return I


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcCompleteGraph(n):
  '''Calculates the independence polynomial of the complete graph'''
  return 1 + x * n

def calcCompleteBipartiteGraph(graph):
  '''Calculates the independence polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  return (1 + x)**n + (1 + x)**m - 1

def calcPath(n):
  '''Calculates the independence polynomial of the path (using recurrence equation, slow)'''
  from collections import deque
  poly = deque([x + 1, 2 * x + 1])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n - 1]

  i = 2
  while i != n:
    i += 1
    poly.append(x * poly[0] + poly[1])
    poly.popleft()

  return poly[1]

def calcCycle(n):
  '''Calculates the independence polynomial of the cycle'''
  from collections import deque
  poly = deque([2 * x + 1, 3 * x + 1])
  if n <= 0:
    return 0
  elif n == 1:
    return x + 1
  elif n == 2:
    return 2 * x + 1
  elif n == 3:
    return 3 * x + 1

  i = 3
  while i != n:
    i += 1
    poly.append(x * poly[0] + poly[1])
    poly.popleft()

  return poly[1]

def calcStar(n):
  '''Calculate the ind. poly of a star'''
  return x + (1 + x)**(n - 1)

def calcCentipede(n):
  '''Calculate the ind. poly of a centipede'''
  from collections import deque
  poly = deque([2 * x + 1, 3*x**2 + 4*x + 1])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n - 1]

  i = 2
  while i != n:
    i += 1
    poly.append((x + 1) * x * poly[0] + (x + 1) * poly[1])
    poly.popleft()

  return poly[1]

def calcBook(n):
  '''Calculates the independence polynomial of the book graph'''
  return 2 * x * (1 + x)**n + (1 + 2 * x)**n

def calcSun(n):
  '''Calculates the independence polynomial of the sun graph'''
  return (x + 1)**(n - 2) * (1 + x * (x + n +2))

def calcSunlet(n):
  '''Calculates the independence polynomial of the sunlet graph'''
  u = ((x + 1) * (5 * x + 1))**(1/2)
  return 2**(-n) * ((u + x + 1)**n + (-u + x + 1)**n)

def calcWheel(n):
  '''Calculates the independence polynomial of the wheel graph'''
  t = (1 + 4 * x)**(1/2)
  return (-(1 - t)**n - (1 - t)**n * t + (t - 1) * (1 + t)**n + 2**(n - 1) * x**2)/(2**(n + 1) * x)


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the independence polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  ID = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      ID = calcCompleteGraph(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      ID = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      ID = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      ID = calcCompleteBipartiteGraph(graph)
    elif graph.graphtype == graphType.CENTIPEDE:
      PyGraphEdit_log.info("starting calculation with algorithm for centipedes ...")
      ID = calcCentipede(graph.order() / 2)
    elif graph.graphtype == graphType.BOOK:
      PyGraphEdit_log.info("starting calculation with algorithm for books ...")
      ID = calcBook(graph.order() / 2 - 1)
    elif graph.graphtype == graphType.SUN:
      PyGraphEdit_log.info("starting calculation with algorithm for suns ...")
      ID = calcSun(graph.order() / 2)
    elif graph.graphtype == graphType.SUNLET:
      PyGraphEdit_log.info("starting calculation with algorithm for sunlets ...")
      ID = calcSunlet(graph.order() / 2)
    elif graph.graphtype == graphType.WHEEL:
      PyGraphEdit_log.info("starting calculation with algorithm for wheels ...")
      ID = calcWheel(graph.order())

  if ID:
    if q != None:
      q.put(ID)
    return expand(ID)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration ...")
    ID = compute_polynomial(graph)
  elif m == method.DECOMP:
    PyGraphEdit_log.info("starting calculation with decomposition ...")
    ID = IndPoly(graph)

  if q != None:
    q.put(ID)
  return expand(ID)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
