'''
Two methods to calculate the tutte polynomial.

The first uses the program tuttepoly-v0.9.16 from David James Pearce and Gary Haggard
and the second based on the c-program from Michael Barany.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.09.2013
'''
import cython

cdef Tutte(graph, q=*)

@cython.locals(d=cython.int, n=cython.int, d1=cython.int, n1=cython.int,
               i=cython.int)
cdef tutte(graph, q=*)

@cython.locals(i=cython.int, j=cython.int)
cdef transposeMatrix(m)
cdef imatrix(d, n, start)

@cython.locals(i=cython.int)
cdef printimatrix(m, d, n, start)

@cython.locals(i=cython.int, j=cython.int, count=cython.int)
cdef getpoly(m, int d, int n)

@cython.locals(i=cython.int, j=cython.int)
cdef copyimatrix(m, newmat, int d, int n, int start)

@cython.locals(tmp=cython.int)
cdef swaprows(m, int row1, int row2, int n)

@cython.locals(i=cython.int)
cdef swapcols(m, int col1, int col2, int d)

@cython.locals(tmp=cython.int)
cdef setuppivot(m, int pivcol, int pivrow, int d, int n)

@cython.locals(i=cython.int, j=cython.int, tmp1=cython.int, tmp2=cython.int)
cdef elim(m, int pivcol, int pivrow, int d, int n)

@cython.locals(i=cython.int, j=cython.int)
cdef rowreduce(m, int d, int n, primes):

@cython.locals(i=cython.int, j=cython.int, test=cython.int, b=cython.int)
cdef lowestterms(m, int d, int n, primes):

@cython.locals(j=cython.int)
cdef killfactor(factor, int i, int test, m, int n)

@cython.locals(i=cython.int, j=cython.int, count=cython.int, indicator=cython.int)
cdef justloopsnbridges(m, int d, int n)

@cython.locals(i=cython.int, count=cython.int)
cdef done(record, int n)

@cython.locals(i=cython.int, count=cython.int)
cdef lastreczero(record, int n)

@cython.locals(i=cython.int, count=cython.int)
cdef lastrecmod(record, int n)

@cython.locals(i=cython.int, j=cython.int, a=cython.int, b=cython.int)
cdef sendtotutte(m, tutte, int d, int n)

@cython.locals(j=cython.int, count=cython.int)
cdef d1totutte(m, tutte, int n):

@cython.locals(j=cython.int, i=cython.int)
cdef deletein(m, int d, int n, edge):

@cython.locals(i=cython.int, k=cython.int)
cdef contractin(m, int d, int n, edge)

@cython.locals(v=cython.int)
cdef toobig (m, int d, int n):

@cython.locals(j=cython.int, i=cython.int)
cdef rebuild(m1, m, int d, int n, int d1, int n1, record, primes)
cdef calcPath(int n)
cdef calcCycle(int n)
cdef calcCentipede(int n)
cdef calcStar(int n)
cdef calcSunlet(int n)
cdef calcWheel(int n)

@cython.locals(i=cython.int)
cdef calcBook(int n)

cpdef compute_poly(MDNetwork_log2, graph, m=*, q=*)
