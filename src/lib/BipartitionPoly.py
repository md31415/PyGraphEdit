#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods to calculate the bipartition polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 14.02.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import sys
from copy import deepcopy, copy
import hashlib
import math
from sqlite3 import connect, PARSE_DECLTYPES, register_adapter, register_converter  # @UnresolvedImport
from sympy import symbols, expand

from lib.Decompositions import get_path_decomposition, get_width  # @UnresolvedImport
from lib.tools import powerset, Tree, Partition, binomial  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphFunctions import (isskPath, iskTree, isTree, iskPath, isComplete,  # @UnresolvedImport
                                   isCompleteBipartite, getBipartition,  # @UnresolvedImport
                                   isPath, isCycle)  # @UnresolvedImport
from SGraph.GraphParser import (graphadapter, graphkonverter, dictadapter,  # @UnresolvedImport
                                dictkonverter, load_graphdb)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport
from modules.error import GraphClassError, QueerError  # @UnresolvedImport


# =============================================================================
# =============================================================================
x, y, z, w = symbols('x y z w')


# =============================================================================
def compute_polynomial(graph):
  '''
  Compute the bipartition polynomial
  @type graph: SGraph
  @param graph: Graph
  '''
  P = 0
  neighbors = graph.open_neighbors_set
  for W in powerset(graph.getV()):
    P2 = 1
    for v in neighbors(W):
      P2 = P2 * (y * ((1 + z)**(len(set(graph.neighbors(v)) & set(W))) - 1) + 1)

    P = P + x ** len(W) * P2

  return P


# =============================================================================
# ================ TREES ======================================================
# =============================================================================
def calculateTree(graph):
  '''
  Compute the bipartition polynomial of a tree
  @type graph: SGraph
  @param graph: Tree
  '''
  t = Tree()
  t.fromSGraph(graph)

  states = calc_tree(t, t.get_root())
  B = 0

  for s in states:
    if s.p != None:
      B = s.p + B
  return B

def calc_all_trees(fname=None):
  '''Calculate all trees in the file fname
  @param fname: Name of the SQL-File with the trees
  '''
  if fname == None:
    return "error no file given"

  poly = set([])
  graphnames = getGraphNamesdb(fname)
  num = len(graphnames)
  print("Number of graphs in the file: %d" % num)
  p = 0.0
  for name in graphnames:
    g = SGraph()
    g, _ = load_graphdb(fname, name)
    if g.order() > 0:
      D = calculateTree(g)
      str_D = str(D)
      m = hashlib.md5(str_D.encode()).hexdigest()
      if m in poly:
        print("found equal polys")
        print(g)
        # print(poly[m])
        sys.exit()

      else:
        # poly[m]= g
        poly.add(m)
    p += 100 / num
    print("%.4f percent" % p)

  print("All calculation done ...")

def calc_tree(tree, node):
  '''
  Recursive function to calculate the bipatition polynomial of a rooted tree
  @param tree: (Sub)Tree
  @param node: Root of the (sub)tree
  '''
  # If the tree has only one vertex
  if len(node.childs) == 0:
    return [DecompState(Partition([1], [node.number]), x), DecompState(Partition([3], [node.number]))]

  else:
    Z = []
    # Calc for every child of the node polys
    for v in node.childs:
      s = calc_tree(tree, tree.getVertex(v))
      new = []
      for z in s:
        d = z.activate(node.number)
        new.append(d)
      for z in new:
        s.append(z)

      new = []
      for z in s:
        d = z.process_edge(node.number, v)
        if d != None:
          new.append(d)
        z.deactivate(v)
      for z in new:
        z.deactivate(v)
        s.append(z)

      # compress_states(s)

      Z.append(s)

    # Join the states of the childs and return the result
    if len(Z) == 1:
      return Z[0]
    elif len(Z) > 1:
      state = Z[0].copy()
      for i, s in enumerate(Z):
        if i > 0:
          state = joinStates(state, s, node.number)
      return state

def compress_states(states):
  '''
  Combine equal states
  '''
  flag = True
  while flag:
    flag = False
    len_states = len(states)
    for i in range(len_states - 1):
      for j in range(i + 1, len_states):
        if states[i] == states[j]:
          flag = True
          states[i].p = states[i].p + states[j].p
          del states[j]
          break
      if flag:
        break

def joinStates(state, s, number):
  '''
  Join two states
  '''
  # possiblePartitions = [Partition([1],[number]), Partition([2],[number]), Partition([3],[number])]
  Z = []
  p1 = 0
  p2 = 0
  p3 = 0
  for t in state:
    if t.partition.komponent(number) == 1:
      p1 = t.p + p1
    elif t.partition.komponent(number) == 2:
      p2 = t.p + p2
    elif t.partition.komponent(number) == 3:
      p3 = t.p + p3
  r1 = 0
  r2 = 0
  r3 = 0
  for t in s:
    if t.partition.komponent(number) == 1:
      r1 = t.p + r1
    elif t.partition.komponent(number) == 2:
      r2 = t.p + r2
    elif t.partition.komponent(number) == 3:
      r3 = t.p + r3

  current_state = DecompState(Partition([1], [number]))
  current_state.p = (p1 * r1) / (x)
  Z.append(current_state)

  current_state = DecompState(Partition([2], [number]))
  current_state.p = (p2 * r2) / (y) + p2 * r3 + p3 * r2
  Z.append(current_state)

  current_state = DecompState(Partition([3], [number]))
  current_state.p = p3 * r3
  Z.append(current_state)

  return Z

class DecompState():
  '''
  Class for a state
  @param partition: Partition of the active vertices of the state
  @param p: Polynomial of the state
  '''

  def __init__(self, partition=None, poly=1):
    self.partition = partition
    self.p = poly

  def __eq__(self, other):
    '''
    Test if self and other are equal
    @type self: DecompState
    @type other: DecompState
    '''
    if other == None:
      return False
    if self.partition == other.partition:
      return True
    else:
      return False

  def activate(self, v):
    '''
    Activate a vertex
    @param v: Vertex of the tree
    '''
    # Copy the state and place the new element in partition 3 (is not dominated)
    other = self.copy()
    other.partition.addElement(3, v)

    # Set the vertex as dominating
    self.partition.addElement(1, v)
    self.p = x * self.p

    return other

  def deactivate(self, v):
    '''
    Deactivate a vertex of the tree
    @param v: Vertex of the tree
    '''
    self.partition.removeElement(v)

  def process_edge(self, u, v):
    '''
    Process an edge of the tree
    @param u: Start vertex of the edge
    @param v: End vertex of the edge
    '''
    other = None
    if (self.partition.komponent(u) == 1 and self.partition.komponent(v) == 1
        or self.partition.komponent(u) == 2 and self.partition.komponent(v) == 2
        or self.partition.komponent(u) == 3 and self.partition.komponent(v) == 3
        or self.partition.komponent(u) == 2 and self.partition.komponent(v) == 3
        or self.partition.komponent(u) == 3 and self.partition.komponent(v) == 2):
      pass

    elif (self.partition.komponent(u) == 1 and self.partition.komponent(v) == 2
          or self.partition.komponent(u) == 2 and self.partition.komponent(v) == 1):
      self.p = (z + 1) * self.p

    elif self.partition.komponent(u) == 1 and self.partition.komponent(v) == 3:
      other = self.copy()
      other.partition.removeElement(v)
      other.partition.addElement(2, v)
      other.p = y * z * other.p

    elif self.partition.komponent(u) == 3 and self.partition.komponent(v) == 1:
      other = self.copy()
      other.partition.removeElement(u)
      other.partition.addElement(2, u)
      other.p = y * z * other.p

    else:
      print(u, v)
      print(self.partition)
      sys.exit("Something crazy happends!!!")

    return other

  def copy(self):
    '''
    Return a copy of the current state
    '''
    other = DecompState(deepcopy(self.partition))
    other.p = self.p
    return other

  def __str__(self):
    '''Return a string representation'''
    return "P: %s -> %s" % (str(self.partition), str(self.p))

def getGraphNamesdb(fname, numvertices=None):
  '''
  Read all names from the SQLite-database
  @param fname: Filename of the database
  @param numvertices: Number of vertices of the graphs
  @return: Name of the graphs in the file
  '''
  # Try to open the file
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    return None
  cursor = connection.cursor()

  # Set the adapter and converter
  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  # Search for graphs in the database
  if numvertices != None:
    cursor.execute("SELECT name FROM Graphen WHERE n=%d" % numvertices)
  else:
    try:
      cursor.execute("SELECT name FROM Graphen")
    except:
      return None

  try:
    names = cursor.fetchall()
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None
  n = []
  for name in names:
    n.append(name[0])

  cursor.close()
  return n


# =============================================================================
# ================ CALCULATION WITH PATH DECOMPOSITION ========================
# =============================================================================
def path_decomposition(graph):
  '''
  Calculate the bipartition polynomial with the path decomposition of the graph
  @param graph: SGraph
  '''
  decomposition = get_path_decomposition(graph, min(graph.getV()), max(graph.getV()))
  global PyGraphEdit_log
  PyGraphEdit_log.debug("Decomposition: %s" % str(decomposition))
  PyGraphEdit_log.debug("width of this path-decomposition: %s" % str(get_width(decomposition)))

  B = 0
  states = {}

  # Calculate the polynomial
  states[PartState()] = 1
  for s in decomposition:
    try:
      if s > 0:
        PyGraphEdit_log.debug("activate %d" % s)
        states = activate(states, s)

      else:
        PyGraphEdit_log.debug("deactivate %d" % -s)
        s = -s
        if s != 3 and s != 4:
          states = deactivate(states, s)

    except:
      PyGraphEdit_log.debug("activate %s" % str(s))
      states = process_edge(states, s[0], s[1])

  # Sum the polys of the states
  for state in states:
    B = B + states[state]

  return B

def activate(states, v):
  '''
  Activate a vertex
  @param states: List of the current states
  @param v: Vertex of the graph
  @return: List of the new states
  '''
  new_states = {}
  for state in states:
    # Copy the state and set the vertex as not dominating
    other = state.copy()
    other.addElement(3, v)
    if other in new_states:
      new_states[other] = new_states[other] + states[state]
    else:
      new_states[other] = states[state]

    # Set the vertex as dominating
    p = x * states[state]
    state.addElement(1, v)
    if state in new_states:
      new_states[state] = new_states[state] + p
    else:
      new_states[state] = p

  return new_states

def deactivate(states, v):
  '''
  Deactivate a vertex
  @param states: List of the current states
  @param v: Vertex of the graph
  @return: List of the new states
  '''
  new_states = {}
  for state in states:
    other = state.copy()
    other.removeElement(v)
    if other in new_states:
      new_states[other] = new_states[other] + states[state]
    else:
      new_states[other] = states[state]

  return new_states

def process_edge(states, u, v):
  '''
  Process an edge of the graph
  @param states: List of the current states
  @param u: Start vertex of the edge
  @param v: End vertex of the edge
  '''
  try:
    new_states = {}
    for state in states:
      other = None
      if (state.komponent(u) == 1 and state.komponent(v) == 1
          or state.komponent(u) == 2 and state.komponent(v) == 2
          or state.komponent(u) == 3 and state.komponent(v) == 3
          or state.komponent(u) == 2 and state.komponent(v) == 3
          or state.komponent(u) == 3 and state.komponent(v) == 2):
        if state in new_states:
          new_states[state] = new_states[state] + states[state]
        else:
          new_states[state] = states[state]

      elif (state.komponent(u) == 1 and state.komponent(v) == 2
          or state.komponent(u) == 2 and state.komponent(v) == 1):
        if state in new_states:
          new_states[state] = (z + 1) * states[state] + new_states[state]
        else:
          new_states[state] = (z + 1) * states[state]

      elif state.komponent(u) == 1 and state.komponent(v) == 3:
        new_states[state] = states[state]

        other = state.copy()
        p = y * z * states[other]
        other.removeElement(v)
        other.addElement(2, v)
        if other in new_states:
          new_states[other] = new_states[other] + p
        else:
          new_states[other] = p

      elif state.komponent(u) == 3 and state.komponent(v) == 1:
        new_states[state] = states[state]

        other = state.copy()
        p = states[other] * y * z
        other.removeElement(u)
        other.addElement(2, u)
        if other in new_states:
          new_states[other] = new_states[other] + p
        else:
          new_states[other] = p

      else:
        raise QueerError([u, v, state])
  except QueerError as e:
    print(e)
    sys.exit()

  return new_states


# =============================================================================
# ================ 2-PATH =====================================================
# =============================================================================
def calculate_s2Path(graph):
  '''
  Calculates the bipartition polynomial of simple 2-paths
  @param graph: SGraph
  @return: Bipartition polynomial
  '''
  V = graph.getV()
  state = KPathState(V[0], V[1])
  state.first_state()
  for v in V[2:]:
    state.nextVertexu(v)

  return state.getRel()

def calculate_2Path(graph):
  '''
  Calculates the bipartition polynomial of 2-paths
  @param graph: SGraph
  @return: Bipartition polynomial
  '''
  _, vertexorder = iskTree(graph, 2)
  if vertexorder != False:
    deg = graph.get_degrees()
    try:
      if list(deg.values()).count(2) != 2:
        raise Exception.NotImplemented
    except Exception.NotImplemented as e:
      return str(e)

  state = None
  current_vertices = []
  for s in vertexorder:
    # If no vertex is active, then set an new state
    if len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 0:
      state = KPathState(s[0], s[1][1])
      state.first_state()
      state.nextVertexu(s[1][0])
      current_vertices.append(s[1][0])
      current_vertices.append(s[1][1])

    # Two vertices are already active
    elif len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 2 and s[0] in current_vertices:
      if s[1][0] in current_vertices:
        u = s[1][0]
        v = s[0]
        if state.getu() == u and state.getv() == v:  # u is the pivot point
          state.nextVertexv(s[1][1])
        elif state.getu() == v and state.getv() == u:  # v is the pivot point
          state.nextVertexu(s[1][1])
        current_vertices.remove(s[0])
        current_vertices.append(s[1][1])
      else:
        u = s[1][1]
        v = s[0]
        if state.getu() == u and state.getv() == v:  # u is the pivot point
          state.nextVertexv(s[1][0])
        elif state.getu() == v and state.getv() == u:  # v is the pivot point
          state.nextVertexu(s[1][0])
        current_vertices.remove(s[0])
        current_vertices.append(s[1][0])

  v = list(set(graph.getV()) - set(current_vertices))[0]
  state.nextVertexv(v)

  return state.getRel()

class KPathState():
  '''
  Class for the state of a k-path
  '''
  def __init__(self, u=None, v=None):
    self.P = []
    self.u = u
    self.v = v

  def first_state(self):
    '''
    Generates the first state
    '''
    if len(self.P) != 0:
      PyGraphEdit_log.error("Only use this function before calculation!!!")
      return False

    self.P = [0 for _ in range(0, 9)]
    self.P[0] = x ** 2
    self.P[1] = x * y * z
    self.P[2] = x
    self.P[3] = x * y * z
    self.P[4] = x
    self.P[5] = 0
    self.P[6] = 0
    self.P[7] = 0
    self.P[8] = 1

    return True

  def getu(self):
    '''Return u'''
    return self.u

  def getv(self):
    '''Return v'''
    return self.v

  def change(self):
    '''Changes the vertices u and v in the state'''
    tmp = self.u
    self.u = self.v
    self.v = tmp

    tmp = copy(self.P[1])
    self.P[1] = copy(self.P[3])
    self.P[3] = copy(tmp)

    tmp = copy(self.P[2])
    self.P[2] = copy(self.P[4])
    self.P[4] = copy(tmp)

    tmp = copy(self.P[5])
    self.P[5] = copy(self.P[6])
    self.P[6] = copy(tmp)

  def nextVertexu(self, new_u):
    '''Replace u with new_u'''
    P_tmp = [0 for _ in range(0, 9)]
    P_tmp[0] = x * (self.P[0] + self.P[3] * (1 + z) + self.P[4] * (1 + y * z))
    P_tmp[1] = self.P[0] * (2 * y * z + y * z ** 2) + self.P[3] * y * z + self.P[4] * y * z
    P_tmp[2] = self.P[0] + self.P[3] + self.P[4]
    P_tmp[3] = x * (self.P[1] * (1 + z) + self.P[2] * y * z + self.P[5] * (1 + z) * y * z + \
                  self.P[6] * ((1 + z) * (1 + y * z)) + self.P[7] * (1 + 2 * z + z ** 2) + \
                  self.P[8] * (y ** 2 * z ** 2 + y * z))
    P_tmp[4] = x * (self.P[2] + self.P[8] * (1 + y * z) + self.P[5] * (1 + z))
    P_tmp[5] = self.P[1] + self.P[6] + self.P[7]
    P_tmp[6] = self.P[2] * y * z
    P_tmp[7] = self.P[1] * y * z
    P_tmp[8] = self.P[2] + self.P[8] + self.P[5]

    self.P = deepcopy(P_tmp)

    self.u = copy(self.v)
    self.v = copy(new_u)

  def nextVertexv(self, new_v):
    '''Replace v with new_v'''
    P_tmp = [0 for _ in range(0, 9)]
    P_tmp[0] = x * (self.P[0] + self.P[1] * (1 + z) + self.P[2] * (1 + y * z))
    P_tmp[1] = self.P[0] * (2 * y * z + y * z ** 2) + self.P[1] * y * z + self.P[2] * y * z
    P_tmp[2] = self.P[0] + self.P[1] + self.P[2]
    P_tmp[3] = x * (self.P[3] * (1 + z) + self.P[4] * y * z + self.P[6] * (1 + z) * y * z + \
                  self.P[5] * ((1 + z) * (1 + y * z)) + self.P[7] * (1 + 2 * z + z ** 2) + \
                  self.P[8] * (y ** 2 * z ** 2 + y * z))
    P_tmp[4] = x * (self.P[4] + self.P[8] * (1 + y * z) + self.P[6] * (1 + z))
    P_tmp[5] = self.P[3] + self.P[5] + self.P[7]
    P_tmp[6] = self.P[4] * y * z
    P_tmp[7] = self.P[3] * y * z
    P_tmp[8] = self.P[4] + self.P[8] + self.P[6]

    self.P = deepcopy(P_tmp)
    self.v = copy(new_v)

  def getRel(self):
    '''Return the poly of this state'''
    return sum(self.P)

  def print_me(self):
    '''Print this state'''
    s = "u: %d, v: %d -> %s" % (self.u, self.v, self.getRel())
    PyGraphEdit_log.info(s)

  def print_me_details(self):
    '''Print the state with details'''
    for i in range(0, 9):
      PyGraphEdit_log.info("P%d: %s" % (i + 1, str(self.P[i])))


# =============================================================================
def calculate_s2Path_wPartitions(graph):
  '''Calculates a simple 2-path with partitions'''
  V = graph.getV()
  state = PathState(1, 2)
  state.firstState()
  for i in range(3, len(V) + 1):
    state.next(i)

  return state.getD()

def calculate_2Path_wPartitions(graph):
  '''
  Calculates the bipartition polynomial of 2-paths
  @param graph: SGraph
  @return: Bipartition polynomial
  '''
  _, vertexorder = iskTree(graph, 2)
  if vertexorder != False:
    deg = graph.get_degrees()
    try:
      if list(deg.values()).count(2) != 2:
        raise Exception.NotImplemented
    except Exception.NotImplemented as e:
      return str(e)

  state = None
  current_vertices = []
  for s in vertexorder:
    # If no vertex is active, then generate a new state
    if len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 0:
      state = PathState(s[0], s[1][1])
      state.firstState()
      state.next(s[1][0])
      current_vertices.append(s[1][0])
      current_vertices.append(s[1][1])

    # Two vertices allready active
    elif len(set([s[0], s[1][0], s[1][1]]).intersection(set(current_vertices))) == 2 and s[0] in current_vertices:
      if s[1][0] in current_vertices:
        # u = s[1][0], v = s[0]
        state.next(s[1][1], s[1][0])
        current_vertices.remove(s[0])
        current_vertices.append(s[1][1])
      else:
        # u = s[1][1], v = s[0]
        state.next(s[1][0], s[1][1])
        current_vertices.remove(s[0])
        current_vertices.append(s[1][0])

  v = list(set(graph.getV()) - set(current_vertices))[0]
  state.next(v)

  return state.getD()

# possible partitions
possible_partitions = [[1, 1], [1, 2], [1, 3], [2, 1], [3, 1], [2, 3], [3, 2], [2, 2], [3, 3]]

class PathState():
  def __init__(self, u, v):
    self.u = u
    self.v = v
    self.D = []  # partitions of the state

  def firstState(self):
    '''Generate the first state'''
    for part in possible_partitions:
      s = PartState(self.u, self.v, part)
      if part.count(1) >= 1:
        D = x ** part.count(1)
      if part.count(1) == 1:
        D = D * (y * z) ** part.count(2)
      elif part.count(1) == 0 and part.count(2) != 0:
        D = 0
      elif part.count(1) == 0 and part.count(2) == 0:
        D = x ** 0
      s.D = D
      self.D.append(s)

  def next(self, w, new_u=None):
    '''Add the next vertex to the state'''
    if new_u == None:
      new_u = self.v

    new_D = copy(self.D)
    for s in new_D:
      if new_u == self.v:
        s.vtou(self.u, self.v, w)
      else:
        s.newV(self.v, w)
      D = 0
      for _, pi in enumerate(self.D):
        if (self.v in s.p.get(1, []) and self.v in pi.p.get(1, []) \
            or self.v in s.p.get(2, []) and w in s.p.get(1, []) and self.v not in pi.p.get(1, [])
            or self.v in s.p.get(2, []) and w in s.p.get(2, []) and self.u in pi.p.get(1, []) and self.v in pi.p.get(2, [])
            or self.v in s.p.get(2, []) and w in s.p.get(3, []) and self.v in pi.p.get(2, [])
            or self.v in s.p.get(3, []) and w not in s.p.get(2, []) and self.v in pi.p.get(3, [])
            or self.v in s.p.get(3, []) and w in s.p.get(2, []) and self.u in pi.p.get(1, []) and self.v in pi.p.get(3, [])):
          D = self.d(s, pi, w) * pi.D + D

      if D != 0:
        s.D = x ** (list(s.p.get(1, [])).count(w)) * D
      else:
        s.D = 0

    self.D = new_D
    if new_u == self.v:
      self.u = copy(self.v)
      self.v = copy(w)
    else:
      self.v = copy(w)

  def d(self, s, pi, w):
    if w in s.p.get(1, []):
      return (y * z) ** (pi.komponent(self.v) - s.komponent(self.v)) \
            * (y * z + 1) ** int(math.floor(pi.komponent(self.u) / 3)) \
            * (z + 1) ** pi.size(2)
    elif w in s.p.get(2, []):
      return (y * z * 2 + y * z ** 2) ** (pi.size(1) - 1) \
            * (y * z) ** (pi.size(2) + pi.size(3))
    else:
      return x ** 0

  def getD(self):
    '''Calculate the bipartition polynomial of this state'''
    D = 0
    for pi in self.D:
      D = pi.D + D
    return D

  def __str__(self):
    '''Returns the string representation of this state'''
    return "u: " + str(self.u) + " v: " + str(self.v) + " -> "

class PartState():
  '''
  Partition class for the states
  WARNING: Blocks without element are allowed
  '''
  def __init__(self, u=None, v=None, blocks=[]):
    self.D = 0
    self.p = {}
    if len(blocks) != 0:
      self.addElement(blocks[0], u)
      self.addElement(blocks[1], v)

  def copy(self):
    '''Returns a copy of this partition'''
    other = PartState()
    other.p = deepcopy(self.p)
    return other

  def getD(self):
    '''Return the polynomial of this partition'''
    return self.D

  def addElement(self, block, element):
    '''Insert the element in the block'''
    if block in self.p.keys():
      self.p[block].add(element)
    else:
      self.p[block] = set([element])

  def removeElement(self, element):
    '''Delete the element from the partition'''
    for key in self.p.keys():
      if element in self.p[key]:
        self.p[key].remove(element)
        break

  def komponent(self, element):
    '''Returns the block in which the element is'''
    for key in self.p.keys():
      if element in self.p[key]:
        return key
    return 0

  def size(self, block):
    '''Return the size of the block'''
    return len(self.p.get(block, []))

  def __eq__(self, other):
    '''Compare two partitions'''
    for k in self.p:
      if self.p.get(k, set([])) != other.p.get(k, set([])):
        return False
    return True

  def __hash__(self):
    '''Calculates the hash of the partition'''
    return hash((frozenset(self.p.get(0, set([]))),
                 frozenset(self.p.get(1, set([]))),
                 frozenset(self.p.get(2, set([])))))

  def newV(self, v, new_v):
    '''Replace the vertex v with new_v in the partition'''
    for key in self.p:
      if v in self.p[key]:
        self.p[key].add(new_v)
        self.p[key].remove(v)

  def vtou(self, u, v, new_v):
    '''Replace the vertex u with v and the vertex v with new_v'''
    for key in self.p:
      if v in self.p[key]:
        self.p[key].add(new_v)
        self.p[key].remove(v)
      if u in self.p[key]:
        self.p[key].add(v)
        self.p[key].remove(u)

  def __str__(self):
    return str(self.p)


# =============================================================================
# ============ SPECIAL GRAPH CLASSES =========================================
# =============================================================================
def calcCompleteGraph(graph):
  '''Calculate the bipartition polynomial of a complete graph'''
  B = 1
  n = graph.order()
  for k in range(1, n + 1):
    B = B + binomial(n, k) * x ** k * (y * (z + 1) ** k - y + 1) ** (n - k)

  return B

def calcCompleteBipartiteGraph(graph):
  '''Calculates the bipartition polynomial of a complete bipartite graph'''
  B = 0
  _, bipartition = getBipartition(graph)
  s = list(bipartition.values()).count(0)
  t = list(bipartition.values()).count(1)

  for j in range(0, s + 1):
    for k in range(0, t + 1):
      binom = binomial(s, j) * binomial(t, k)
      first = (y * ((z + 1) ** j - 1) + 1) ** (t - k)
      second = (y * ((z + 1) ** k - 1) + 1) ** (s - j)
      B = B + binom * x ** (j + k) * first * second

  return B

def calcPath(n):
  '''Calculates the bipartition polynomial of a path with n vertices'''
  if n == 0:
    return 1
  elif n == 1:
    return x + 1
  elif n == 2:
    return (x + 1) ** 2 + x * y * z * 2
  else:
    return (x + 1) * calcPath(n - 1) + x * y * z * (z + 2) * calcPath(n - 2) + x * y * z ** 2 * (y - 1) * calcPath(n - 3)

def calcCycle(n):
  '''Calculates the bipartition polynomial of a cycle with n vertices'''
  B = calcPath(n)
  for k in range(2, n + 1):
    B = B + (k - 1) * (x ** math.floor(k / 2) * y ** math.ceil(k / 2) + y ** math.floor(k / 2) * x ** math.ceil(k / 2)) * z ** (k - 1) * calcPath(n - k)

  if n % 2 == 0:
    B = B + 2 * x ** (n / 2) * y ** (n / 2)

  return B


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the bipartition polynomial
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue to store the result
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing the bipartition polynomial ...")
  PyGraphEdit_log.info("")

  B = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      B = calcCompleteGraph(graph)
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      B = calcCycle(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      B = calcPath(graph.order())
    elif isTree(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      graph.graphtype = graphType.TREE
      B = calculateTree(graph)
    elif isskPath(graph) == 2:
      PyGraphEdit_log.info("starting calculation with algorithm for simple 2-paths ...")
      graph.graphtype = graphType.SKPATH
      B = calculate_s2Path(graph)
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      B = calcCompleteBipartiteGraph(graph)

  # If the graph has a special type, then use the corresponding algorithm
  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      B = calcCompleteGraph(graph)
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      B = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      B = calcCycle(graph.order())
    elif graph.graphtype == graphType.TREE:
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      B = calculateTree(graph)
    elif graph.graphtype == graphType.SKPATH:
      PyGraphEdit_log.info("starting calculation with algorithm for simple 2-paths ...")
      B = calculate_s2Path(graph)
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      B = calcCompleteBipartiteGraph(graph)

  if B and q != None:
    q.put(B)
    return expand(B)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration...")
    B = compute_polynomial(graph)

  elif m == method.PATH_DECOMP:
    PyGraphEdit_log.info("starting calculating with path decomposition...")
    B = path_decomposition(graph)

  elif m == method.TREE:
    try:
      if not (graph.graphtype == graphType.TREE or isTree(graph)):
        raise GraphClassError("tree")
      PyGraphEdit_log.info("starting tree algorithm...")
      B = calculateTree(graph)
    except GraphClassError as e:
      return str(e)

  elif m == method.SKPATH:
    try:
      k = isskPath(graph)
      if k > 2:
        raise NotImplementedError()
      elif k == 0:
        raise GraphClassError("simple k-path")
      B = calculate_s2Path(graph)
      #B = calculate_s2Path_wPartitions(graph)
    except NotImplementedError as e:
      return str(e)
    except GraphClassError as e:
      return str(e)
 
  elif m == method.KPATH:
    try:
      if not (graph.graphtype == graphType.KPATH or iskPath(graph)):
        raise GraphClassError("k-path")
    except GraphClassError as e:
      return str(e)
    #B = calculate_2Path_wPartitions(graph)
    B = calculate_2Path(graph)

  else:
    try:
      raise NotImplementedError
    except NotImplementedError as e:
      return str(e)

  if q != None:
    q.put(expand(B))

  return expand(B)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
