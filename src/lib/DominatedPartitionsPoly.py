'''
Implements the calculation of the two types of the dominated
partition polynomials for the usage in PyGraphEdit.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from copy import deepcopy
from sympy import symbols  # @UnresolvedImport

from SGraph.GraphFunctions import (iskStar, isCompleteBipartite,  # @UnresolvedImport
                                   getBipartition, isPath)  # @UnresolvedImport
from lib.tools import binomial, partition  # @UnresolvedImport


# ==========================================================
x = symbols('x')


# ==========================================================
# ========== DOMINATED PARTITION POLYNOMIAL R_d(G,x) =======
# ==========================================================
def calcDominatedPartPoly_d(graph):
  '''
  Calculate the dominated partition polynomial R_d(G,x)using the definition
  @param graph: SGraph
  '''
  if graph.order() == 0:
    return 1
  neighbors = {v: set(graph.neighbors_set(set([v]))) for v in graph.getVIt()}
  poly = 0
  for pi in partition(set(graph.getV())):
    flag2 = True
    for B in pi:
      flag = False
      for v in B:
        if B <= neighbors[v]:
          flag = True
          break
      if not flag:
        flag2 = False
        break
    if flag2:
      poly += x**(len(pi))
  return poly


# ==========================================================
# =========== SPECIAL GRAPH CLASSES ========================
# ==========================================================
def calcPath_d(n):
  '''
  Calculate R_d(G,x) of a path with n vertices
  @param n: Number of the vertices of the path
  '''
  if n == 0:
    return 1
  elif n <= -1:
    return 0

  poly = 0
  for k in range(0, n + 1):
    a = 0
    for j in range(0, k +1):
      a += binomial(j, n - 3 * k + 2 * j) * binomial(k, j)
    poly += a * x**k
  return poly

def calcBipartite_d(n):
  '''Calculate the complete bipartite graph K_{2,n}'''
  poly = 0
  for i in range(0, n + 1):
    for j in range(0, n - i + 1):
      poly += binomial(n, i) * binomial(n - i, j) * x**(n - i - j)
  poly = poly * x**2
  poly += n * x**n
  return poly

def calcStar_d(n):
  '''
  Calculate R_d(G,x) of a star with n vertices
  @param n: Number of the vertices of the star
  '''
  return x * (1 + x)**n


# ==========================================================
# ========== DOMINATED PARTITION POLYNOMIAL R_e(G,x) =======
# ==========================================================
def calcDominatedPartPoly_e(graph):
  '''
  Calculate the dominated partition polynomial R_e(G,x)using the definition
  @param graph: SGraph
  '''
  if graph.order() == 0:
    return 1
  neighbors = {v: set(graph.closedneighbors(v)) for v in graph.getVIt()}
  orderedVertices = sorted(deepcopy(graph.getV()), key=graph.degree, reverse=True)
  part = partition(set(graph.getV()))
  poly = 0
  for pi in part:
    flag2 = True
    for B in pi:
      flag = False
      for v in orderedVertices:
        if B <= neighbors[v]:
          flag = True
          break
      if not flag:
        flag2 = False
        break
    if flag2:
      poly += x**(len(pi))
  return poly


# ==========================================================
# =========== SPECIAL GRAPH CLASSES ========================
# ==========================================================
def calcPath_e(n):
  '''
  Calculate R_e(G,x) of a path with n vertices
  @param n: Number of the vertices of the path
  '''
  poly = 0
  for i in range(0, n + 1):
    poly += calcPath_e(i) * d(n - i)

  return poly

def d(n):
  if n == 0:
    return 1
  elif n <= 2:
    return 0
  return x**2 * calcPath_e(n - 3) + x**2 * calcPath_e(n - 4)


# ==========================================================
# ==========================================================
# ==========================================================
def calcPoly_d(MDNetwork_log, graph, m=None, q=None):
  '''
  Calc the dominated partition polynomial R_d(G,x)
  '''
  poly = 0
  if isPath(graph):
    MDNetwork_log.info("recognized path ...")
    poly = calcPath_d(graph.order())
  elif iskStar(graph, 1):
    MDNetwork_log.info("recognized star ...")
    poly = calcStar_d(graph.order())
  elif isCompleteBipartite(graph):
    _, bipartition = getBipartition(graph)
    if bipartition[0].order() == 2:
      poly = calcBipartite_d(bipartition[1].order())
    elif bipartition[1].order() == 2:
      poly = calcBipartite_d(bipartition[0].order())
    else:
      poly = calcDominatedPartPoly_d(graph)
  else:
    MDNetwork_log.info("Starting calculating with complete enumeration ...")
    poly = calcDominatedPartPoly_d(graph)

  if q != None:
    q.put(poly)

  return poly

def calcPoly_e(graph, q=None):
  '''
  Calc the dominated partition polynomial R_e(G,x)
  '''
  poly = 0
  if isPath(graph):
    poly = calcPath_e(graph.order())
  else:
    poly = calcDominatedPartPoly_e(graph)

  if q != None:
    q.put(poly)

  return poly


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  from sys import exit as sys_exit
  sys_exit("Use PyGraphEdit")
