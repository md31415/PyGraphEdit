'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 18.09.2014

@author: markus
'''

from tools import (binomial, falling_factorial, factorial, Partition, MarkedSet, mset)  # @UnresolvedImport


def test_binomial():
  assert binomial(5, 3) == 10
  assert binomial(3, 5) == 0
  assert binomial(50, 25) == 126410606437752
  assert binomial(60, 30) == 118264581564861424
  assert binomial(80, 20) == 3535316142212174320

def test_falling_factorial():
  assert falling_factorial(5, 2) == 20
  assert falling_factorial(10, 3) == 720
  assert falling_factorial(5, 0) == 1

def test_factorial():
  assert factorial(10) == 3628800
  assert factorial(50) == 30414093201713378043612608166064768844377641568960512000000000000
  assert factorial(1) == 1

def test_Partition():
  p = Partition()
  assert len(p.getElements()) == 0

  p.addElementNewBlock(1)
  p.addElementNewBlock(2)
  assert set(p.getElements()) == set([1, 2])

  p.addElement(1, 3)
  assert set(p.getElements()) == set([1, 2, 3])
  assert set(p.getElementsInBlock(1)) == set([1, 3])

  p.removeElement(3)
  p.addElement(1, 4)
  p.addElement(2, 5)
  assert set(p.getElementsInBlock(1)) == set([1, 4])
  assert set(p.getElementsInBlock(2)) == set([2, 5])

  p.moveElement(4, 2)
  assert set(p.getElementsInBlock(1)) == set([1])
  assert set(p.getElementsInBlock(2)) == set([2, 4, 5])
  assert p.size(2) == 3

  p.unionBlocks(1, 2)
  assert set(p.getElementsInBlock(1)) == set([1, 2, 4, 5])
  assert set(p.getElements()) == set([1, 2, 4, 5])


def test_MarkedSet():
  s = MarkedSet(set([1, 2, 3, 4, 5]))
  assert s.getElements() == set([1, 2, 3, 4, 5])

  t = MarkedSet()
  t.addElement(1)
  t.addElement(2)
  t.addElement(3)
  t.addElement(4)
  t.markElement(3, 1)
  t.markElement(2, 0)
  assert t.getElements() == set([1, 2, 3, 4])
  assert t.isMarked(3, 0) == False
  assert t.isMarked(3, 1) == True
  assert t.isMarked(2, 0) == True
  assert t.isMarked(2, 1) == False

  t.removeElement(2)
  assert t.getElements() == set([1, 3, 4])
  assert t.isMarked(3, 0) == False
  assert t.isMarked(3, 1) == True
  assert 3 in t
  assert 2 not in t
  assert s != t

def test_mset():
  s = mset([1, 2, 3, 4, 5])
  assert set(s.get_elements()) == set([1, 2, 3, 4, 5])

  s.add([1, 2])
  assert set(s.get_elements()) == set([1, 2, 3, 4, 5])
  assert s.get_num_element(1) == 2
  assert s.get_num_element(2) == 2
  assert s.get_num_element(3) == 1
  assert s.get_num_element(4) == 1
  assert s.get_num_element(7) == 0

  s.add(1)
  assert s.get_num_element(1) == 3
  assert s.get_num_element(2) == 2
  assert s.get_num_element(3) == 1
  assert s.get_num_element(4) == 1
  assert s.get_num_element(5) == 1

  s.remove(4)
  assert s.get_num_element(1) == 3
  assert s.get_num_element(2) == 2
  assert s.get_num_element(3) == 1
  assert s.get_num_element(4) == 0
  assert s.get_num_element(5) == 1

  s.remove(4)
  assert s.get_num_element(1) == 3
  assert s.get_num_element(2) == 2
  assert s.get_num_element(3) == 1
  assert s.get_num_element(4) == 0
  assert s.get_num_element(5) == 1
