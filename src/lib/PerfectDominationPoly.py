#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Compute the perfect domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
'''

from sympy import symbols, expand
import sys

from lib.tools import powerset  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport


# =============================================================================
# =============================================================================
x = symbols('x')

# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the perfect domination polynomial using complete enumeration
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    # if ((4 not in W and 6 not in W and 8 not in W) or (3 not in W and 4 not in W and 11 not in W)) and 2 in W and 5 in W:
    # if 1 not in W and 8 not in W:# and 8 not in W and 11 not in W and 12 not in W and 14 not in W:
      if is_perfect_dom(graph, W):
        # print(W)
        P = x ** len(W) + P

  return P

def is_perfect_dom(graph, W):
  '''
  Decides if the set W is a total dominating set
  '''
  W = set(W)
  for v in set(graph.getV()) - W:
    if len(W.intersection(graph.neighbors(v))) != 1:
      return False
  return True


# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, q=None):
  '''
  Calculate the perfect domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing...")
  MDNetwork_log.info("")

  PD = None
  if m == method.STATE_SPACE or m == None:
    MDNetwork_log.info("starting complete enumeration...")
    PD = compute_polynomial(graph)

  if q != None:
    q.put(PD)
  return expand(PD)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
