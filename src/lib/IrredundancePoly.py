#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the irredundance polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 05.05.2014

@author: Markus Dod
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import powerset  # @UnresolvedImport


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x = symbols('x')


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the irredundance polynomial using complete enumeration
  @param graph: Graph
  '''
  P = 0
  for W in powerset(graph.getV()):
    if is_irred(graph, W):
      P = x ** len(W) + P

  return P

def is_irred(graph, W):
  '''
  Decides if the set W is a irredundance set
  @param graph: Graph
  @param W: Vertex subset
  @return: Bool
  '''
  W = set(W)
  for v in W:
    if len(graph.neighbors_set(set([v])) - graph.neighbors_set(W - set([v]))) == 0:
      return False
  return True


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(graph, q=None):
  '''
  Calculate the irredundance polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  #global MDNetwork_log
  #MDNetwork_log = MDNetwork_log2
  #MDNetwork_log.info("start computing...")
  #MDNetwork_log.info("")

  ID = None
  #if m == method.STATE_SPACE or m == None:
  #  print(graph)
    #MDNetwork_log.info("starting complete enumeration ...")
  ID = compute_polynomial(graph)

  if q != None:
    q.put(ID)
  return expand(ID)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
