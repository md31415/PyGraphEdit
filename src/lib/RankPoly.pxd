#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the rank polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
'''

import cython

@cython.locals(n=cython.int, c=cython.int)
cdef compute_polynomial(graph)
cdef calcPath(int n)

@cython.locals(k=cython.int)
cdef calcCycle(int n)
cdef calcStar(int n)

@cython.locals(i=cython.int)
cdef calcWheel(int n)
cdef calcBook(int n)
cdef calcSunlet(int n)

cpdef compute_poly(PyGraphEdit_log2, graph, m=*, q=*)
