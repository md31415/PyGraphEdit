#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculation of the domination polynomial and the domination reliability polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

from sympy import symbols, degree, simplify

x = symbols('x')
p = symbols('p')


def state_space_enumeration(graph, X=None, Y=None):
  pass

def domPoly(G, X, Y):
  pass

def essential_calculation(graph):
  pass


#calculate_kCycle(graph, k)
#calculate_kPath(n, k)
#calculate_kTrees(graph, vertexorder=None)
#calculate_kStar(graph, k)
#calcPath(n)
#calcCycle(n)
#calcFriendship(n)
#calcBook(n)
#reltodom(DRel)
#domtorel(D)
