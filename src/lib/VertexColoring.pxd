'''
Implements methods to solve the vertex coloring problem.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 11.02.2014
'''
import cython

@cython.locals(k=cython.int, i=cython.int)
cdef solve_coloring(graph, int k)

@cython.locals(c=cython.int, i=cython.int, c=cython.int, colors=cython.dict,
               c_v=cython.int)
cdef greedy(graph)

@cython.locals(i=cython.int)
cdef getSmallestColor(N, colors, int c)

@cython.locals(color_counter=cython.int, saturation_degrees=cython.dict,
               coloring=cython.dict, maximum_degree_vertex=cython.int,
               maximum_degree=cython.int, vertex=cython.int, v=cython.int,
               number_color=cython.int, nvertex=cython.int)
cdef DSATUR_calculation(graph)

@cython.locals(color_counter=cython.int, v=cython.int)
cdef get_amount_color(vertices, int color_number, coloring)

cpdef compute_coloring(PyGraphEdit_log2, graph, k=*, m=*, q=*)
