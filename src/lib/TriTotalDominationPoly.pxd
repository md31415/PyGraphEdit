'''
Calculation of the trivariate total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 04.02.2014

@author: Markus Dod
'''
import cython

cdef compute_tri_polynomial(graph)

cdef calculateCartesianKK2(int n)

@cython.locals(m=cython.int)
cdef calculateLexiKG(int n, graph)
cdef calculateLexiGK(int n, graph)

@cython.locals(m=cython.int, k=cython.int)
cdef calculateLexiPH(int n, graph)
cdef calculateStrongKH(int n, graph)

def calcCompleteGraph(int n)

@cython.locals(m=cython.int, n=cython.int, v=cython.int)
cdef calcCompleteBipartiteGraph(graph)
cdef calcCompletekPartiteGraph(graph)

@cython.locals(k=cython.int, n=cython.int)
cdef _calckPartiteGraph(p)

@cython.locals(i=cython.int)
cdef calcPath(n)

cpdef compute_poly(MDNetwork_log2, graph, m=*, graph1=*, graph2=*, q=*)
