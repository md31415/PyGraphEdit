'''
Verfahren zur Berechnung des trivariaten Dominationspolynoms
Starten �ber DominationMain.py

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 14.02.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import cython

cdef compute_polynomial(graph)

cdef calculateTree(graph)
cpdef calc_all_trees(fname=*)

@cython.locals(i=cython.int)
cdef calc_tree(tree, node)

@cython.locals(flag=cython.bint, i=cython.int, j=cython.int, len_states=cython.int)
cdef compress_states(states)

cdef joinStates(state, s, number)

cdef class DecompState:
  cpdef __eq__(self, other)

  cpdef activate(self, int v)
  cpdef deactivate(self, int v)
  cpdef process_edge(self, int u, int v)
  cpdef copy(self)

cdef path_decomposition(graph)
cdef activate(states, int v)
cdef deactivate(states, int v)
cdef process_edge(states, int u, int v)

@cython.locals(v=cython.int)
cdef calculate_s2Path(graph)

@cython.locals(vertexorder=cython.bint, u=cython.int, v=cython.int)
cdef calculate_2Path(graph)

cdef class KPathState:
  cpdef first_state(self)
  cpdef getu(self)
  cpdef getv(self)

  @cython.locals(tmp=cython.int)
  cpdef change(self)
  cpdef nextVertexv(self, int new_v)
  cpdef getRel(self)
  cpdef print_me(self)
  cpdef print_me_details(self)

@cython.locals(i=cython.int)
cdef calculate_s2Path_wPartitions(graph)

@cython.locals(vertexorder=cython.bint, v=cython.int)
def calculate_2Path_wPartitions(graph):

cdef class PathState:
  cpdef firstState(self)
  cpdef next(self, int w, new_u=*)
  cpdef d(self, s, pi, w)
  cpdef getD(self)

cdef class PartState:
  cpdef copy(self)
  cpdef getD(self)
  cpdef addElement(self, int block, element)

  @cython.locals(key=cython.int)
  cpdef removeElement(self, element)

  @cython.locals(key=cython.int)
  cpdef komponent(self, element)

  cpdef size(self, int block)
  cpdef __eq__(self, other)
  cpdef __hash__(self)

  @cython.locals(key=cython.int)
  cpdef newV(self, int v, int new_v)

  @cython.locals(key=cython.int)
  cpdef vtou(self, int u, int v, int new_v)
  cpdef __str__(self)

@cython.locals(n=cython.int, k=cython.int)
cdef calcCompleteGraph(graph)

@cython.locals(j=cython.int, k=cython.int, s=cython.int, t=cython.int)
cdef calcCompleteBipartiteGraph(graph)

cdef calcPath(int n)

@cython.locals(k=cython.int)
cdef calcCycle(int n)

cpdef compute_poly(PyGraphEdit_log2, graph, m=*, q=*)
