'''
Methods for the calculation of the independent domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 05.05.2014

@author: Markus Dod
'''
import cython

cdef compute_polynomial(graph)

@cython.locals(v=cython.int)
cdef is_ind_dom(graph, W)

@cython.locals(v=cython.int)
cdef is_ind(graph, W)

@cython.locals(u=cython.int, v=cython.int, w=cython.int)
cdef edge_rec(graph)
    
@cython.locals(u=cython.int, v=cython.int)
cdef isIndependent(graph, X)

@cython.locals(v=cython.int)
cdef recLoop(graph)

@cython.locals(v=cython.int, n=cython.int, i=cython.int)
cdef compute_forbiddenVertices(graph):
cdef compute_polynomial2(graph)
cdef compute_polynomial3(graph)

@cython.locals(v=cython.int)
cdef decomp(graph)

@cython.locals(v=cython.int, k=cython.int)
cdef essential_calculation(graph):
cdef isEssential(graph, U)

cdef calculateTree(graph)

@cython.locals(v=cython.int)
def calc_tree(tree, int node):

cdef class DecompState:
  cpdef activate(self, int v)
  cpdef copy(self)

cdef calcCompleteGraph(int n)

@cython.locals(n=cython.int, m=cython.int, v=cython.int)
cdef calcCompleteBipartiteGraph(graph)

@cython.locals(k=cython.int)
cdef calcPath(int n)

@cython.locals(i=cython.int)
cdef calcPathRec(int n)
cdef calcCycle(int n)
cdef calcStar(int n)

@cython.locals(n=cython.int)
cdef calculateCentipede(graph)

@cython.locals(n=cython.int, k=cython.int)
def calculateBananaTree(graph)

@cython.locals(n=cython.int, k=cython.int)
cdef calculateFirecracker(graph)
  cpdef inline firecracker(int n, int k)


cdef calculateCartesianKK(int n, int m)
cdef calculateCartesianKP(int n, int m)
cdef calculateCartesianKC(int n, int m)
cdef calculateCartesianK2G(graph)

@cython.locals(m=cython.int)
cdef calculateCartesianK3G(graph)

@cython.locals(m=cython.int, count=cython.int, w=cython.int, k=cython.int,
               delta=cython.int)
cdef calculateCartesianKG(n, graph)
cdef calculateLexiGH(graph1, graph2)
cdef calculateLexiPG(int n, graph2)
cdef calculateLexiCG(int n, graph2)
cdef calculateTensorKnKm(int n, int m)

cdef calculateTensorPnKm(int n, int m)
  cdef f(int n, int m)
  cdef g(int n, int m)

cdef calculateTensorPnP3(int n, int m)
  cdef g1(int n)
  cdef g2(int n)
  cdef g3(int n)

@cython.locals(n=cython.int)
cdef compute_RelPoly(graph)

@cython.locals(suc=cython.int, r=cython.double, running=cython.int)
cdef simulatePolynomial(graph)

@cython.locals(v=cython.int)
cdef chooseSet(V, double r)

cdef domtorel(D, int n)

cpdef compute_poly(MDNetwork_log2, graph, m=*, graph1=*, graph2=*, q=*)
cpdef computeRelPoly(MDNetwork_log2, graph, m=*, graph1=*, graph2=*, q=*)
