#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the all terminal reliabilty polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 09.12.2015

@author: Markus Dod
@license: GPL (see LICENSE.txt)

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isPath, isCycle)  # @UnresolvedImport
from SGraph.GraphFunctions import connected  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
p = symbols('p')


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Calculation of the all terminal reliabilty polynomial using complete enumeration
  @param graph: Graph
  '''
  m = graph.size()
  R = 0
  for F in powerset(list(graph.getEdgeSet())):
    if connected(graph.spanningSubgraph(F)):
      R += p ** (len(F)) * (1 - p) ** (m - len(F))
  return R


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcPath(n):
  '''Calculates the all terminal reliabilty polynomial of the path'''
  return p**(n - 1)

def calcCycle(n):
  '''Calculates the all terminal reliabilty polynomial of the cycle'''
  return p**(n - 1) * (1 + (n - 1) * (1 - p))

def calcStar(n):
  '''Calculate the all terminal reliabilty poly of a star'''
  return p**(n - 1)

def calcSunlet(n):
  '''Calculates the all terminal reliabilty polynomial of the sunlet graph'''
  return p**(2 * n - 1) * (1 + (n - 1) * (1 - p))

def calcWheel(n):
  '''Calculates the all terminal reliabilty polynomial of the wheel graph'''
  from collections import deque
  poly = deque([p, -2*p**3 + 3*p**2, -6*p**6 + 24*p**5 - 33*p**4 + 16*p**3])
  if n <= 1:
    return 0
  elif n <= 4:
    return poly[n - 1]

  i = 3
  while i != n:
    i += 1
    poly.append(-(3*(1-p)+1)*((1-p)-1)*poly[2] - 2*(1-p)*((1-p)+1)*((1-p)-1)**2*poly[1] - p**2*((1-p)-1)**3*poly[0])
    poly.popleft()

  return poly[2]


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the all terminal reliabilty polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  ID = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      ID = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      ID = calcCycle(graph.order())
    elif graph.graphtype == graphType.SUNLET:
      PyGraphEdit_log.info("starting calculation with algorithm for sunlets ...")
      ID = calcSunlet(graph.order() / 2)
    elif graph.graphtype == graphType.WHEEL:
      PyGraphEdit_log.info("starting calculation with algorithm for wheels ...")
      ID = calcWheel(graph.order())

  if ID:
    if q != None:
      q.put(ID)
    return expand(ID)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration ...")
    ID = compute_polynomial(graph)

  if q != None:
    q.put(ID)
  return expand(ID)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
