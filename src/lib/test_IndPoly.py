#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

from sympy import expand, symbols, simplify

from src.GraphFunctions import path,cycle, book

from lib.IndPoly import (compute_polynomial, calcPath, calcCycle
                         calcStar, calcBook, calcSun, calcSunlet,
                         calcCentipede)


x = symbols('x')

def test_compute_polynomial():
  assert compute_polynomial(path(4)) == calcPath(4)
  assert compute_polynomial(path(5)) == calcPath(5)
  assert compute_polynomial(path(6)) == calcPath(6)
  assert compute_polynomial(path(7)) == calcPath(7)
  assert compute_polynomial(path(8)) == calcPath(8)
  assert compute_polynomial(path(9)) == calcPath(9)
  assert compute_polynomial(path(10)) == calcPath(10)

  assert compute_polynomial(cycle(4)) == calcCycle(4)
  assert compute_polynomial(cycle(5)) == calcCycle(5)
  assert compute_polynomial(cycle(6)) == calcCycle(6)
  assert compute_polynomial(cycle(7)) == calcCycle(7)
  assert compute_polynomial(cycle(8)) == calcCycle(8)
  assert compute_polynomial(cycle(9)) == calcCycle(9)
  assert compute_polynomial(cycle(10)) == calcCycle(10)

  assert compute_polynomial(book(3)) == calcBook(3)
  assert compute_polynomial(book(4)) == calcBook(4)
  assert compute_polynomial(book(5)) == calcBook(5)

def test_IndPoly():
  assert IndPoly(path(4)) == calcPath(4)
  assert IndPoly(path(5)) == calcPath(5)
  assert IndPoly(path(6)) == calcPath(6)
  assert IndPoly(path(7)) == calcPath(7)
  assert IndPoly(path(8)) == calcPath(8)
  assert IndPoly(path(9)) == calcPath(9)
  assert IndPoly(path(10)) == calcPath(10)

  assert IndPoly(cycle(4)) == calcCycle(4)
  assert IndPoly(cycle(5)) == calcCycle(5)
  assert IndPoly(cycle(6)) == calcCycle(6)
  assert IndPoly(cycle(7)) == calcCycle(7)
  assert IndPoly(cycle(8)) == calcCycle(8)
  assert IndPoly(cycle(9)) == calcCycle(9)
  assert IndPoly(cycle(10)) == calcCycle(10)

  assert IndPoly(book(3)) == calcBook(3)
  assert IndPoly(book(4)) == calcBook(4)
  assert IndPoly(book(5)) == calcBook(5)


#calcCompleteGraph(n)
#calcCompleteBipartiteGraph(graph)
#calcPath(n)
#calcCycle(n)
#calcStar(n)
#calcCentipede(graph)
#calcBook(n)
#calcSun(n)
#calcSunlet(n)
#calcWheel(n)