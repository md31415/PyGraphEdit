'''
Two methods to calculate the tutte polynomial.

The first uses the program tuttepoly-v0.9.16 from David James Pearce and Gary Haggard
and the second based on the c-program from Michael Barany.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.09.2013
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from SGraph.GraphFunctions import (isPath, isCycle)  # @UnresolvedImport

from modules.consts import graphType  # @UnresolvedImport


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
x, y = symbols('x y')

# =============================================================================
# =============================================================================
def Tutte(graph, q=None):
  '''
  Computing the tutte-polynomial with tuttepoly-v0.9.16:
  
  (C) Copyright David James Pearce and Gary Haggard, 2007. 
  Permission to copy, use, modify, sell and distribute this software 
  is granted provided this copyright notice appears in all copies. 
  This software is provided "as is" without express or implied 
  warranty, and with no claim as to its suitability for any purpose.
 
  Email: david.pearce@mcs.vuw.ac.nz
  
  @param graph: Graph
  @return: Tuttepolynomial
  '''
  import tempfile
  from subprocess import Popen, PIPE

  _, path = tempfile.mkstemp()
  file = open(path, "w")
  s = ""
  for edge in graph.getEdgeSet():
    if len(s) != 0:
      s += ","
    s += str(edge[0]) + "--" + str(edge[1])
  file.write(s)
  file.close()

  poly = Popen(["lib/tutte", path], stdin=PIPE, stdout=PIPE)
  (out, _) = poly.communicate()

  T = str(out)[2:len(out) - 1]
  if q != None:
    q.put(T)
  return T


# =============================================================================
# =============================================================================
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23]
NUMPRIMES = 9
TH = 100

def tutte(graph, q=None):
  '''
  ***********Disclaimer************************************
  This work was supported by the University of Minnesota   
  Undergraduate Research Opportunities Program       
  Under the direction of Professor Victor Reiner.     
  Programmed by Michael Barany.  No liability.
  Thanks for your support!         
  *********************************************************
  '''
  d = graph.order()
  n = graph.size()
  d1 = d
  n1 = n
  m1 = graph.getIncidenceMatrix()
  m1 = transposeMatrix(m1)

  # store a reference copy
  m = imatrix(d, n, 1)
  copyimatrix(m1, m, d1, n1, 1)

  # initialize Tutte
  tutte = imatrix(d, n, 0)

  # create del/mod recording vector
  record = [0 for i in range(n + 1)]

  notfinished = 1  # indicates not finished
  while notfinished:  # while not finished
    rowreduce(m1, d1, n1, primes)
    nextedge = justloopsnbridges(m1, d1, n1)
    while nextedge > 0:
      i = lastreczero(record, n)
      record[i] = 1
      d1, n1 = contractin(m1, d1, n1, nextedge)
      rowreduce(m1, d1, n1, primes)
      nextedge = justloopsnbridges (m1, d1, n1)

    if nextedge == -1:
      d1totutte(m1, tutte, n1)
    else:
      sendtotutte(m1, tutte, d1, n1)
    notfinished, d1, n1 = rebuild(m1, m, d, n, d1, n1, record, primes)

  s = getpoly(tutte, d, n)

  from sympy.parsing.sympy_parser import parse_expr
  t = parse_expr(s)

  if q != None:
    q.put(t)
  return t

def transposeMatrix(m):
  return [[m[j][i] for j in range(len(m))] for i in range(len(m[0]))]

def imatrix(d, n, start):
  return [[0 for _ in range(n + 1)] for _ in range(d + 1)]

def printimatrix(m, d, n, start):
  for i in range(start, d + 1):
    print("%s" % str(m[i][start:n + 1]))

def getpoly(m, d, n):
  s = ""
  count = 0
  for i in range(d + 1):
    for j in range(n + 1):
      if m[i][j]:
        if count == 0:
          if m[i][j] != 1:
            s = s + "%d*" % (m[i][j])
          if i:
            if i == 1:
              s = s + " x"
            else:
              s = s + " x**%d" % (i)
            if j:
              s += "*"
          if j:
            if j == 1:
              s = s + " y"
            else:
              s = s + " y**%d" % (j)
        else:
          s = s + " + %d*" % (m[i][j])
          if i:
            if i == 1:
              s = s + " x"
            else:
              s = s + " x**%d" % (i)
            if j:
              s += "*"
          if j:
            if j == 1:
              s = s + " y"
            else:
              s = s + " y**%d" % (j)
        count += 1
  return s

def copyimatrix(m, newmat, d, n, start):
  for i in range(start, d + 1):
    for j in range(start, n + 1):
      newmat[i][j] = m[i][j]

def swaprows(m, row1, row2, n):
  tmp = m[row1]
  m[row1] = m[row2]
  m[row2] = tmp

def swapcols(m, col1, col2, d):
  for i in range(1, d + 1):
    tmp = m[i][col1]
    m[i][col1] = m[i][col2]
    m[i][col2] = tmp

def setuppivot(m, pivcol, pivrow, d, n):
  if pivrow < d:
    tmp = pivrow + 1
    # put a non-zero entry in the pivot position
    while m[pivrow][pivcol] == 0 and tmp <= d:
      swaprows(m, pivrow, tmp, n)
      tmp = tmp + 1

def elim(m, pivcol, pivrow, d, n):
  for i in range(1, d + 1):
    if i != pivrow:
      tmp1 = m[i][pivcol]
      tmp2 = m[pivrow][pivcol]
      for j in range(1, n + 1):
        m[i][j] = tmp2 * m[i][j] - tmp1 * m[pivrow][j]

def rowreduce(m, d, n, primes):
  i = 1
  j = 1
  while i <= d and j <= n:
    setuppivot(m, j, i, d, n)
    if m[i][j]:
      elim(m, j, i, d, n)
      if toobig(m, d, n):
        lowestterms(m, d, n, primes)
      i += 1
      j += 1
    else:
      j += 1

def lowestterms(m, d, n, primes):
  for i in range(1, d + 1):
    test = 0
    b = 0
    for j in range(1, n + 1):
      if m[i][j]:
        b += 1
    if b == 0:
      test = 1

    # slaughterhouse
    for j in range(NUMPRIMES - 1 + 1):
      killfactor(primes[j], i, test, m, n)

def killfactor(factor, i, test, m, n):
  while test == 0:
    for j in range(1, n + 1):
      test += abs(m[i][j]) % factor
    if test == 0:
      for j in range(1, n + 1):
        m[i][j] /= factor

def justloopsnbridges(m, d, n):
  i = 1
  j = 1
  count = 0
  indicator = 0
  if d == 1 and n >= 1:
    return -1
  else:
    while i <= d and indicator == 0:
      count = 0
      j = 1
      while j <= n and count <= 1:
        if m[i][j]:
          count += 1
        j += 1
      if count > 1:
        indicator += 1
      else:
        i += 1
    if indicator:
      j = 1
      count = 0
      while count == 0:
        if m[i][j]:
          count += 1
        else:
          j += 1
      return j
    else:
      return 0

def done(record, n):
  # returns 0 if record is entirely nulls and deletions
  i = 1
  count = 0
  while i <= n and count == 0:
    count += record[i] % 2
    i += 1
  return count

def lastreczero(record, n):
  i = 0
  count = 0
  while count == 0:
    i += 1
    if i == n + 1:
      count = 1
    else:
      count += record[i]

  i -= 1
  return i

def lastrecmod(record, n):
  i = 0
  count = 0
  while i < n and count == 0:
    i += 1
    count += record[i] % 2
  return i

def sendtotutte(m, tutte, d, n):
  i = 1
  j = 1
  a = 0
  b = 0
  # takes a forest with loops and converts to a tutte term

  if d == 0:
    b += n
  elif n == 0:
    a += 1
  else:
    while j <= n:
      if m[i][j] == 0:
        i += 1
      else:
        j += 1
        a += 1
        i = 1
      if i == d + 1:
        j += 1
        b += 1
        i = 1

  tutte[a][b] += 1

def d1totutte(m, tutte, n):
  count = 0
  for j in range(1, n + 1):
      if m[1][j]:
        count += 1
  if count == 0:
    tutte[0][n] += 1
  else:
    tutte[1][n - count] += 1
    for j in range(1, count - 1 + 1):
      tutte[0][n - j] += 1

def deletein(m, d, n, edge):
  for i in range(1, d + 1):
    for j in range(edge + 1, n + 1):
      m[i][j - 1] = m[i][j]
  n -= 1
  return n

def contractin(m, d, n, edge):
  # find the row corresponding to edge
  i = 1
  j = edge
  notzero = 0
  while notzero == 0:
    notzero = m[i][j]
    i += 1  # this will give 1 larger than the desired row
  row = i - 1
  for i in range(1, row):
    for k in range(edge + 1, n + 1):
      m[i][k - 1] = m[i][k]
  for i in range(row + 1, d + 1):
    for k in range(edge + 1, n + 1):
      m[i - 1][k - 1] = m[i][k]
  d -= 1
  n -= 1
  return d, n

def toobig (m, d, n):
  # returns 1 if max*max>2000000/d, else returns 0
#   max = 0
#   for i in range(1, d+1):
#     for j in range(1, n+1):
#       if m[i][j] > max:
#         max = m[i][j]
  v = max([max(m[i]) for i in range(0, len(m))])
  if v > TH:
    return 1
  else:
    return 0

def rebuild(m1, m, d, n, d1, n1, record, primes):
  d1 = d
  n1 = n
  copyimatrix(m, m1, d, n, 1)
  j = lastrecmod(record, n)
  for i in range(n, j, -1):
    rowreduce(m1, d1, n1, primes)
    nextedge = justloopsnbridges(m1, d1, n1)
    if record[i] == 2:
      n1 = deletein(m1, d1, n1, nextedge)
    if record[i] == 1:
      d1, n1 = contractin(m1, d1, n1, nextedge)

  rowreduce(m1, d1, n1, primes)
  nextedge = justloopsnbridges (m1, d1, n1)
  n1 = deletein(m1, d1, n1, nextedge)
  nextedge = done(record, n)  # actually, just reusing the var
  for i in range(1, j):
    record[i] = 0
  record[j] = 2

  return nextedge, d1, n1


# =============================================================================
# ================= SPECIAL GRAPH CLASSES =====================================
# =============================================================================
def calcPath(n):
  '''Calculate the Tutte polynomial of a path'''
  return x**(n - 1)

def calcCycle(n):
  '''Calculate the Tutte polynomial of a cycle'''
  return (x**n - x) / (x - 1) + y

def calcCentipede(n):
  '''Calculate the Tutte polynomial of a centipede'''
  return x**(2 * n - 1)

def calcStar(n):
  '''Calculate the Tutte polynomial of a star'''
  return x**(n - 1)

def calcSunlet(n):
  '''Calculate the Tutte polynomial of a sunlet'''
  return (x**(2 * n) + x**n * (x * (-1 + y) - y)) / (x - 1)

def calcWheel(n):
  '''Calculate the Tutte polynomial of a wheel'''
  t = ((x + y + 1)**2 - 4 * x * y)**(1/2)
  return x * y - x - y - 1 + 2**(1-n)*((x+y+1+t)**(n-1) + (x+y+1-t)**(n-1))

def calcBook(n):
  '''Calculate the Tutte polynomial of a book'''
  from collections import deque
  poly = deque([x**3 + x**2 + x + y, x**5 + 2*x**4 + 3*x**3 + 2*x**2*y + 2*x**2 + 2*x*y + x + y**2 + y])
  if n <= 0:
    return 0
  elif n <= 2:
    return poly[n - 1]

  i = 2
  while i != n:
    i += 1
    poly.append((2 * x**2 + 2 * x + y + 1) * poly[1] - (x**2+x+1)*(x**2+x+y) * poly[0])
    poly.popleft()

  return poly[1]


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(MDNetwork_log2, graph, m=None, q=None):
  '''
  Calculate the Tutte polynomial
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue, only used if called from PyGraphEdit
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("start computing ...")
  MDNetwork_log.info("")

  TD = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isPath(graph):
      TD = calcPath(graph.order())
    elif isCycle(graph):
      TD = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.PATH:
      TD = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      TD = calcCycle(graph.order())

  if TD:
    if q != None:
      q.put(TD)
    return expand(TD)

  if m == "tuttepoly":
    MDNetwork_log.info("starting calculation with tuttepoly-v0.9.16 (Pearce and Haggard) ...")
    TD = Tutte(graph)
  elif m == "Barany" or m == None:
    MDNetwork_log.info("starting calculation with an algortihm of Michael Barany ...")
    TD = tutte(graph)

  if q != None:
    q.put(TD)
  return expand(TD)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
