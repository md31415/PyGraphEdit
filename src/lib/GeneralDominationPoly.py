#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculate the general domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 15.01.2015

@author: Markus Dod
'''

import hashlib
from sqlite3 import (connect, PARSE_DECLTYPES, register_adapter,  # @UnresolvedImport
                     register_converter)  # @UnresolvedImport
import sys

from sympy import symbols, expand

from lib.tools import powerset  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphFunctions import (isComplete, isCompleteBipartite,  # @UnresolvedImport
                                   getBipartition, inducedSubgraph,  # @UnresolvedImport
                                   connectedComponents)  # @UnresolvedImport
from SGraph.GraphParser import (graphadapter, graphkonverter, dictadapter,  # @UnresolvedImport
                                dictkonverter, load_graphdb)  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x, y, z, w = symbols('x y z w')


# =============================================================================
def compute_polynomial(graph):
  '''
  Compute the general domination polynomial
  @type graph: SGraph
  @param graph: Graph
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    g = inducedSubgraph(graph, set(W))
    P += x ** len(W) * y ** len(graph.open_neighbors_set(W)) * z ** g.numIso() * w ** (len(connectedComponents(g)))

  return P


# =============================================================================
def calc_all_graphs(graph=None, fname=None):
  '''
  Calculates the general domination polynomial all graphs in fname
  and search for equal polynomials
  '''
  if graph is None and fname is None:
    return "error no graph or file"

  graphs = []
  poly = set([])
  test = {}
  if graph is None:
    graphnames = getGraphNamesdb(fname)
    num = len(graphnames)
    print("Anzahl der Graphen: %d" % num)
    p = 0.0
    for name in graphnames:
      g = SGraph()
      g, _ = load_graphdb(fname, name)
      if g.order() > 0:
        D = compute_polynomial(g)
        str_D = str(D)
        m = hashlib.md5(str_D.encode()).hexdigest()
        if m in poly:
          print("identisch")
          print(g)
          print(test[m])
          # print(poly[m])
          sys.exit()

        else:
          # poly[m]= g
          poly.add(m)
          test[m] = name
      p += 100 / num
      print("%.4f Prozent" % p)

  else:
    graphs.append(graph)

  print("alle abgearbeitet....")

def getGraphNamesdb(fname, numvertices=None):
  '''
  Alle Graphennamen aus SQLite-Datenbank auslesen
  @param fname: Dateiname der Datenbank
  @param numvertices: Anzahl der Knoten der auszulesenden Graphen
  @return: Namen der Graphen in der Datei
  '''
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    print("something wrong 1")
    return None
  cursor = connection.cursor()

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  if numvertices != None:
    cursor.execute("SELECT name FROM Graphen WHERE n=%d" % numvertices)
  else:
    try:
      cursor.execute("SELECT name FROM Graphen")
    except:
      print("something wrong 2")
      return None

  try:
    names = cursor.fetchall()
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None

  n = []
  for name in names:
    n.append(name[0])

  cursor.close()
  return n


# =============================================================================
# ============ SPECIAL GRAPH CLASSES =========================================
# =============================================================================
def calcCompleteGraph(graph):
  '''Calculate the general domination polynomial of a complete graph'''
  n = graph.order()
  return y ** n * w * ((1 + x / y) ** n - n * x / y - 1) + n * x * y ** (n - 1) * z + 1

def calcCompleteBipartiteGraph(graph):
  '''Calculates the general domination polynomial of a complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  s = list(bipartition.values()).count(0)
  t = list(bipartition.values()).count(1)

  return y ** s * ((1 + x * z) ** t - 1) + \
    y ** t * (((1 + x * z) ** s - 1) + \
            w * y ** (s + t) * ((1 + x / y) ** s - 1) * ((1 + x / y) ** t - 1)) + 1


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the general domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue to store the result
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing the general domination polynomial ...")
  PyGraphEdit_log.info("")

  B = None
  # If no method is given, then test if the graph is in a special graph class
  if m is None and graph.graphtype is None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      B = calcCompleteGraph(graph)
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      B = calcCompleteBipartiteGraph(graph)

  # If the graph has a special type, then use the corresponding algorithm
  elif m is None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      B = calcCompleteGraph(graph)
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      B = calcCompleteBipartiteGraph(graph)

  if B and q != None:
    q.put(B)
    return expand(B)

  if m == method.STATE_SPACE or m is None:
    PyGraphEdit_log.info("starting complete enumeration...")
    B = compute_polynomial(graph)

  else:
    try:
      raise Exception.NotImplementedError
    except Exception.NotImplementedError as e:
      return str(e)

  if q != None:
    q.put(expand(B))

  return expand(B)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
