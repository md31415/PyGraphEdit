# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 15.10.2014

@author: Markus
'''

from SGraph.SGraph import SGraph  # @UnresolvedImport
from lib.VertexColoring import DSATUR_calculation  # @UnresolvedImport


# =================================================================
# ================ Solve with SCIPOPT =============================
# =================================================================
def solve_coloring(graph, k):
  '''Using zibopt to find a coloring of the graph with k colors'''
  PyGraphEdit_log.info("starting using scipopt to color the graph ...")

  try:
    import modules.scipopt.scip as scip
    from itertools import product
  except:
    PyGraphEdit_log.info("scipoptsuite not avalible")
    return None

  # create solver instance
  s = scip.Solver()
  s.create()
  s.includeDefaultPlugins()
  s.createProbBasic(b"Edge Coloring")

  # Indexes for creating variables and constraints
  rows = graph.getEdgeSet()
  vals = range(0, k)

  # add some variables
  probVars = {}
  varNames = {}
  varBaseName = "x"
  for i, k in product(rows, vals):
    if i not in probVars:
      probVars[i] = {}
      varNames[i] = {}
    p = varBaseName + "_" + str(i) + ":" + str(k)
    p = p.encode('ascii')
    varNames[i][k] = p
    probVars[i][k] = s.addIntVar(p, obj=1, lb=0.0, ub=1.0)

  # every edge gets exactly one color
  for i in rows:
    coeffs = {probVars[i][k]: 1.0 for k in vals}
    s.addCons(coeffs, lhs=1.0, rhs=1.0)

  # adjacent vertices getting different colors
  edges = graph.getEdgeSet()
  for v in graph.getV():
    E = []
    for e in graph.getIncidentEdges(v):
      if e in edges:
        E.append(e)
      elif (e[1], e[0]) in edges:
        E.append((e[1], e[0]))
    for k in vals:
      coeffs = {probVars[e][k]: 1.0 for e in E}  # {probVars[e[0]-1][k]: 1.0, probVars[e[1]-1][k]: 1.0}
      s.addCons(coeffs, rhs=1.0)

  # solve problem
  s.solve()
  status = s.getStatus()

  if status == scip.scip_status.optimal:
    # retrieving the best solution
    solution = s.getBestSol()

    # print solution
    sol = dict((i, 0) for i in rows)
    for i in rows:
      for k in vals:
        solValue = round(s.getVal(solution, probVars[i][k]))
        if solValue == 1:
          sol[i] = k + 1

        s.releaseVar(probVars[i][k])

    s.free()
    return sol

  else:
    return None


# ============================================================================
# =============== GREEDY =====================================================
# ============================================================================
def greedy(graph):
  '''Colors the graph with the greedy-algorithm'''
  # get the degrees of the vertices
  degrees = graph.get_degrees()

  # generates a list and sort the vertices in respect to the degree
  deg = sorted([v for v in degrees], key=lambda v: degrees[v], reverse=True)

  # color the edges, starting with the vertex of smallest degree
  colors = {}
  c = 0
  for v in deg:
    for e in graph.getIncidentEdges(v):
      if e not in colors and (e[1], e[0]) not in colors:
        c_e = getSmallestColor(graph.getIncidentEdges(e[0]), graph.getIncidentEdges(e[1]), colors, c)
        colors[e] = c_e
        if c_e > c:
          c = c_e

  return colors, c

def getSmallestColor(N1, N2, colors, c):
  C = set([])
  for e in N1 | N2:
    if e in colors:
      C.add(colors[e])
    elif (e[1], e[0]) in colors:
      C.add(colors[(e[1], e[0])])
  for i in range(1, c + 2):
    if i not in C:
      return i


# ============================================================================
# =============== DSATUR =====================================================
# ============================================================================
def dsatur(graph):
  '''Calculate a coloring with the DSATUR-Heuristic in the line graph'''
  # Calculate the line graph
  X = list(map(set, list(graph.getEdgeSet())))
  m = graph.size()
  H = SGraph(set(range(1, m + 1)))
  for i in range(0, m - 1):
    for j in range(i + 1, m):
      if X[i] & X[j] != set():
        H.insertEdge(i + 1, j + 1)

  # Calculate a proper vertex coloring with the DSATUR-heuristic
  vcolors, k = DSATUR_calculation(H)

  # Color the edges of the original graph and return the result
  ecolors = {tuple(e): vcolors[i + 1] for i,e in enumerate(X)}
  return ecolors, k


# =============================================================================
# =============================================================================
# =============================================================================
def compute_coloring(PyGraphEdit_log2, graph, k=None, m=None, q=None):
  '''
  Compute a proper edge coloring of the graph
  @param graph: Graph
  @param m: Method
  @param k: Number of colors (only for Scipopt)
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  colors = None
  if m == "greedy":
    PyGraphEdit_log.info("using greedy-heuristic ...")
    colors, k = greedy(graph)
  elif m == "SCIPOPT (B&B)":
    PyGraphEdit_log.info("using ScipOpt ...")
    colors = solve_coloring(graph, k)
  elif m == "DSATUR" or m == None:
    PyGraphEdit_log.info("using DSATUR heuristic ...")
    colors, k = dsatur(graph)

  if q != None:
    q.put(colors)
    q.put(k)
  return (colors, k)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  import sys
  sys.exit("Hauptmodul zum starten verwenden")
