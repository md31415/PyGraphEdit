'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
'''

from sympy import symbols
from SGraph.SGraph import SGraph
from SGraph.SpecialGraphs import (cycle, path, diamant,  # @UnresolvedImport
                                  completeBipartite, completeGraph)  # @UnresolvedImport

from RainbowColoringPoly import calcRainbowPoly  # @UnresolvedImport

x = symbols('x')

def testCycles():
  assert calcRainbowPoly(cycle(3)) == x**3
  assert calcRainbowPoly(cycle(4)) == x**4 - 2*x**2 + x
  assert calcRainbowPoly(cycle(5)) == x**5 - 10*x**3 + 15*x**2 - 6*x
  assert calcRainbowPoly(cycle(6)) == x**6 - 30*x**4 + 91*x**3 - 94*x**2 + 32*x
  assert calcRainbowPoly(cycle(7)) == x**7 - 70*x**5 + 357*x**4 - 693*x**3 + 567*x**2 - 162*x

def testDiamant():
  assert calcRainbowPoly(diamant()) == x**5 - 3*x**2 + 2*x

def testComplete():
  assert calcRainbowPoly(completeGraph(5)) == x**10
  g = completeGraph(5)
  g.deleteEdge((1,2))
  assert calcRainbowPoly(g) == x**9 - x**4 - 20*x**3 + 36*x**2 - 16*x

def testCompleteBipartite():
  assert calcRainbowPoly(completeBipartite(2,3)) == x**6 - 19*x**3 + 38*x**2 - 20*x
  assert calcRainbowPoly(completeBipartite(2,4)) == x**8 - 6*x**5 - 151*x**4 + 582*x**3 - 714*x**2 + 288*x
  assert calcRainbowPoly(completeBipartite(3,3)) == x**9 - 138*x**4 + 346*x**3 - 195*x**2 - 14*x

def testRainbowPoly():
  # Rainbow poly
  assert calcRainbowPoly(path(2).product(path(3))) == x**7 - 79*x**4 + 235*x**3 - 215*x**2 + 58*x
  assert calcRainbowPoly(path(2).product(path(4))) == x**10 - 63*x**7 - 2500*x**6 + 18662*x**5 - 34745*x**4 - 23447*x**3 + 114584*x**2 - 72492*x
  assert calcRainbowPoly(path(3).product(path(3))) == x**12 - 14*x**9 - 508*x**8 - 10798*x**7 + 92332*x**6 - 87407*x**5 - 950238*x**4 + 3273473*x**3 - 3973627*x**2 + 1656786*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5,6],[(1,2),(1,4),(2,3),(2,4),(2,5),(3,5),(3,6),(4,5),(5,6)])) == x**9 - x**6 - 18*x**5 - 440*x**4 + 1463*x**3 - 1263*x**2 + 258*x
  assert calcRainbowPoly(cycle(8).insertEdge(1,5)) == x**9 - 4*x**7 - 618*x**6 + 4358*x**5 - 9702*x**4 + 3061*x**3 + 12516*x**2 - 9612*x
  assert calcRainbowPoly(cycle(8).insertEdge(1,5).insertEdge(3,7)) == x**10 - 14*x**7 - 1723*x**6 + 11486*x**5 - 25490*x**4 + 15804*x**3 + 12512*x**2 - 12576*x  
  assert calcRainbowPoly(SGraph([1,2,3,4],[(1,2),(1,3),(2,3),(3,4)])) == x**4 - 5*x**2 + 4*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,3),(2,4),(3,4),(4,5)])) == x**5 - 16*x**3 + 33*x**2 - 18*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,3),(2,3),(3,4),(3,5)])) == x**5 - x**4 - 10*x**3 + 22*x**2 - 12*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(2,3),(2,4),(3,4),(4,5)])) == x**5 - x**4 - 14*x**3 + 34*x**2 - 20*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,3),(2,3),(3,4),(4,5)])) == x**5 - x**4 - 16*x**3 + 40*x**2 - 24*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,4),(1,5),(2,3),(3,4),(4,5)])) == x**6 - 19*x**3 + 31*x**2 - 13*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,3),(1,4),(2,4),(3,4),(4,5)])) == x**6 - 2*x**4 - 26*x**3 + 63*x**2 - 36*x
  assert calcRainbowPoly(SGraph([1,2,3,4,5],[(1,2),(1,3),(2,3),(2,4),(3,4),(4,5)])) == x**6 - 2*x**4 - 42*x**3 + 109*x**2 - 66*x
