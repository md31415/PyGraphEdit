# -*- coding: latin-1 -*-
'''
Implementation of different graph embedding algorithms

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 28.08.2014

@author: Markus Dod
'''

def springEmbedding(graph, coord):
  '''
  Calculates the coordinates with spring embedding
  '''
  for i in range(1, 500):
    coord = _springEmbedding(graph, coord, damping=0.85)
  return coord

def _springEmbedding(graph, coord_old, repulsionfactor=300, attractionfactor=0.06, damping=0.85):
  net_force = {}
  velocity = {}
  coord = {}
  for v in graph.getVIt():
    net_force[v] = [0, 0]
    velocity[v] = [0, 0]
    coord[v] = list(coord_old[v])

  for v in graph.getVIt():
    for u in graph.getVIt():
      if(u != v):

        # squared distance between "u" and "v" in 2D space
        rsq = (coord[v][0] - coord[u][0]) * (coord[v][0] - coord[u][0]) + \
               (coord[v][1] - coord[u][1]) * (coord[v][1] - coord[u][1])

        # counting the repulsion between two vertices
        net_force[v][0] += repulsionfactor * (coord[v][0] - coord[u][0]) / rsq
        net_force[v][1] += repulsionfactor * (coord[v][1] - coord[u][1]) / rsq

    for u in graph.neighbors(v):  # loop through edges
      # counting the attraction
      net_force[v][0] += attractionfactor * (coord[u][0] - coord[v][0])
      net_force[v][1] += attractionfactor * (coord[u][1] - coord[v][1])

    # counting the velocity (with damping 0.85)
    velocity[v][0] = (velocity[v][0] + net_force[v][0]) * damping
    velocity[v][1] = (velocity[v][1] + net_force[v][1]) * damping

  for v in graph.getVIt():  # set new positions
    coord[v][0] += velocity[v][0]
    coord[v][1] += velocity[v][1]

  return coord


# =====================================================================
def gridLayout(graph, length=40, heigth=40, numberx=0, numbery=0):
  '''
  Calculates the coordinates of the vertices in a grid layout
  @param graph: SGraph
  @param length: Length of the grid
  @param heigth: Height of the grid
  @param numberx: Number of columns
  @param numbery: Number of rows
  @return: Coordinates
  '''
  import math
  if numberx != 0 and numbery != 0:
    pass
  elif numberx == 0 and numbery != 0:
    numberx = math.ceil(graph.order() / numbery)
  elif numberx != 0 and numbery == 0:
    numbery = math.ceil(graph.order() / numberx)
  else:
    numberx = math.floor(math.sqrt(graph.order()))
    numbery = math.ceil(graph.order() / numberx)

  if numberx == 1:
    distx = length / 2
  else:
    distx = length / (numberx - 1)

  if numbery == 1:
    disty = length / 2
  else:
    disty = heigth / (numbery - 1)

  coords = {}
  for i, v in enumerate(graph.getV()):
    coords[v] = [0, 0]
    coords[v][0] = (i) % numberx * distx - length / 2
    coords[v][1] = int(i / numberx) * disty - heigth / 2
  return coords


# ======================================================================
def circleLayout(graph, radius=40):
  '''
  Calculates the coordinates of the vertices embedded on a cycle
  @param graph: SGraph
  @param radius: Radius of the cycle (Default: 50)
  @return: Coordinates
  '''
  import math
  if graph.order() != 0:
    interval = 2.0 * 3.14159265 / graph.order()
    coords = {}

    for index, v in enumerate(graph.getV()):
      angle = interval * index
      x = radius * math.cos(angle)
      y = radius * math.sin(angle)
      coords[v] = (x, y)

    return coords
  else:
    return {}


# ====================================================================
# ====================================================================
if __name__ == '__main__':
  import sys
  sys.exit("Use PyGraphEdit")
