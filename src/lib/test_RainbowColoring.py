'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 16.03.2015

@author: markus
'''

from RainbowColoring import isVaildList, countRainbowListColorings  # @UnresolvedImport

def test_isVaildList():
  assert isVaildList(4, {1: set([1,2,3]), 2: set([1,2,3]), 3: set([1,2,3])})
  
  assert not isVaildList(4, {1: set([1,2]), 2: set([1,2]), 3: set([1,2])})
  assert isVaildList(4, {1: set([1,2]), 2: set([1,2]), 3: set([1,3])})
  assert isVaildList(4, {1: set([1,2]), 2: set([1,3]), 3: set([1,2])})
  assert isVaildList(4, {1: set([1,2]), 2: set([1,3]), 3: set([1,3])})
  assert isVaildList(4, {1: set([1,3]), 2: set([1,2]), 3: set([1,2])})
  assert isVaildList(4, {1: set([1,3]), 2: set([1,2]), 3: set([1,3])})
  assert isVaildList(4, {1: set([1,3]), 2: set([1,3]), 3: set([1,2])})
  assert not isVaildList(4, {1: set([1,3]), 2: set([1,3]), 3: set([1,3])})
  
  assert not isVaildList(5, {1: set([1,2]), 2: set([1,2]), 3: set([2,3]), 4: set([2,3])})
  assert not isVaildList(5, {1: set([1,2]), 2: set([1,2]), 3: set([2,3]), 4: set([1,3])})
  assert isVaildList(5, {1: set([1,2]), 2: set([1,2]), 3: set([2,3]), 4: set([2,4])})
  assert isVaildList(5, {1: set([1,2]), 2: set([1,3]), 3: set([2,3]), 4: set([2,4])})
  assert not isVaildList(5, {1: set([1,2]), 2: set([1,3]), 3: set([2,3]), 4: set([2,3])})
  assert isVaildList(5, {1: set([1,2]), 2: set([1,2]), 3: set([3,4]), 4: set([3,4])})
  assert isVaildList(5, {1: set([1,2]), 2: set([1,2]), 3: set([3,4]), 4: set([1,4])})

def test_countRainbowListColorings():
  assert countRainbowListColorings(3, 2, 2) == 1
