#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Some methods to calculate the alliance polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 19.05.2016

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import cython

@cython.locals(n=cython.int, m=cython.int)
cdef compute_polynomial(graph)

cdef calcCompleteGraph(int n)
cdef calcStar(int n)

@cython.locals(n=cython.int, m=cython.int)
cdef calcCompleteBipartite(graph)
cdef calcPath(int n)
cdef calcCycle(int n)

cpdef compute_poly(PyGraphEdit_log2, graph, m, q)


