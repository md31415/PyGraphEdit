'''
Calculation of the perfect edge domination reliability

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 26.11.2012

@author: Markus Dod
'''
import cython

cdef state_space_enumeration(graph, A, int d=*, int k=*)

@cython.locals(v=cython.int)
cdef decomp(graph, A, int d=*, int k=*, int P=*)
cdef reduce_graph(graph, A, int d=*, int k=*)

cpdef compute_reliability(MDNetwork_log2, graph, A, m, int d=*, int k=*, int q=*)
