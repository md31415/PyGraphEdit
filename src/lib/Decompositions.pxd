'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 22.02.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import cython

@cython.locals(v=cython.int, w=cython.int, i=cython.int)
cpdef get_tree_decomposition(graph)

@cython.locals(v=cython.int, i=cython.int)
cdef minfill(graph)

@cython.locals(v=cython.int, w=cython.int, u=cython.int, count=cython.int)
cdef fillcount(graph)

@cython.locals(u=cython.int, w=cython.int, x=cython.int, y=cython.int)
cdef addFillEdges(graph, int v, fcount)

@cython.locals(flag=cython.bint, i=cython.int, v=cython.int, w=cython.int)
cpdef get_path_decomposition(graph, int s, int t)

@cython.locals(v=cython.int, max_set=cython.int)
cdef get_width(path_decomp)

@cython.locals(v=cython.int, max_label=cython.int, flag=cython.bint, w=cython.int,
               label=cython.int, m=cython.int, n=cython.int, i=cython.int)
cpdef generate_clique_decomposition(graph)

@cython.locals(k=cython.int, v=cython.int, max_value=cython.int)
cdef best_node(neighbors)


