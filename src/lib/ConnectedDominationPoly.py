#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the connected domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 04.11.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from copy import deepcopy
import sympy
import sys

from SGraph.GraphFunctions import (components, inducedSubgraph,  # @UnresolvedImport
                                   isComplete, isCompleteBipartite, isCycle,  # @UnresolvedImport
                                   isPath, isskPath, isAntiCycle, getBipartition)  # @UnresolvedImport
from lib.tools import powerset, Partition  # @UnresolvedImport
from lib.Decompositions import get_path_decomposition, get_width  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport
import modules.error as err  # @UnresolvedImport


# =====================================================================
x = sympy.symbols('x')
NUM_ACTIV = 0


# =====================================================================
def compute_polynomial(graph):
  '''
  Calculation of the connected domination polynomial using complete enumeration
  '''
  if graph.order() == 0:
    return 1

  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    if is_conn_dom(graph, W):
      P = x ** len(W) + P

  return P

def is_conn_dom(graph, W):
  '''
  Decides if the set W is a connected dominating set
  '''
  W = set(W)
  if len(graph.neighbors_set(W)) == graph.order() and len(components(inducedSubgraph(graph, W))) == 1:
    return True
  return False

def recurrence(graph):
  '''
  Calculation of the connected domination polynomial using recurrence equations
  @param graph: Graph
  @return: The connected domination polynomial of the graph
  '''
  u = 1
  g = graph.copy()
  g.deleteVertex(u)
  P = compute_polynomial(g)

  V = set(graph.getV())
  V.remove(u)
  pset = powerset(list(V))
  for W in pset:
    U = set(W)
    U.add(u)
    if len(components(inducedSubgraph(graph, U))) == 1:
      g = graph.copy()
      g.deleteVertexSet(graph.neighbors_set(U))
      P = compute_polynomial(g) * (-1) ** len(U) + P
      if len(graph.neighbors_set(U)) == graph.order():
        P = x ** len(U) + P

  return P

def test(graph):
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    G = inducedSubgraph(graph, set(W))
    P = compute_polynomial(G) * (-1) ** (len(W)) + P

  return P

def test2(graph):
  '''
  Calculate the number of connected dominating sets
  '''
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    G = components(inducedSubgraph(graph, set(W)))
    e = 0
    o = 0
    for g in G:
      if g.order() % 2 == 0:
        e += 1
      else:
        o += 1
    P = (-1) ** len(W) * (e - o) + P

  return P

def test3(graph):
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    G = components(inducedSubgraph(graph, set(W)))
    R = 0
    for g in G:
      R = (x * (-1)) ** (g.order()) + R
    P = R * (-1) ** (len(W)) + P

  return P

def test4(graph):
  P = 0
  pset = powerset(graph.getV())
  for W in pset:
    if len(W) != 0:
      G = components(inducedSubgraph(graph, set(W)))
      R = 0
      if len(G) == 1:
        R = x ** len(W)
#       else:
#         for g in G:
#           R = (x * (-1))**(g.order()) + R
#         R = R * (-1)**(len(W))

      P = R + P

  return P

def edgesum(graph):
  edgesets = powerset(list(graph.getEdgeSet()))
  P = 0
  for F in edgesets:
    print(".........")
    print(F)
    g = graph.spanningSubgraph(F)
    # print(g)
    R = compute_polynomial(g)
    print(R)
    P += (-1) ** len(F) * R

  return P


# ============================================================================
# ================ PATH DECOMPOSITION ========================================
# ============================================================================
def path_decomposition(graph):
  '''
  Calculation of the connected domination polynomial using a path decomposition
  @param graph: Graph
  @return: The connected domination polynomial
  '''
  decomposition = get_path_decomposition(graph, min(graph.getV()), max(graph.getV()))
  global PyGraphEdit_log
  PyGraphEdit_log.debug("Decomposition: %s" % str(decomposition))
  PyGraphEdit_log.info("width of used path-decomposition: %s" % str(get_width(decomposition)))

  CD = 0
  global NUM_ACTIV
  NUM_ACTIV = 0
  states = []

  states.append(PartState())
  for s in decomposition:
    if isinstance(s, int):
      if s > 0:
        NUM_ACTIV += 1
        PyGraphEdit_log.debug("activate %d" % s)
        PyGraphEdit_log.debug("number %d" % NUM_ACTIV)
        activate(states, s)

      else:
        PyGraphEdit_log.debug("deactivate %d" % -s)
        s = -s
        states = deactivate(states, s, graph.order(), graph.degree(s))
        compress(states)

    elif isinstance(s, list):
      PyGraphEdit_log.debug("activate %s" % str(s))
      processEdge(states, s)

  # Sum the current states to calculate the polynomial
  for state in states:
    CD = state.getPoly() + CD

  return CD

def activate(states, element):
  '''Activate a vertex'''
  tmp = []
  for state in states:
    new = state.activate(element)
    tmp.append(new)

  for state in tmp:
    states.append(state)

def deactivate(states, element, n, elementdegree):
  '''Deactivate a vertex'''
  new_states = []
  for state in states:
    if state.deactivate(element, n, elementdegree):
      new_states.append(state)
  return new_states

def processEdge(states, edge):
  '''Insert Edge'''
  for state in states:
    state.processEdge(edge)

def compress(states):
  '''Look for equal states and union this states'''
  # Muss noch schneller gemacht werden
  flag = True
  while flag:
    flag = False
    len_states = len(states)
    for i in range(len_states - 1):
      for j in range(i + 1, len_states):
        if states[i] == states[j]:
          flag = True
          states[i].poly = states[i].poly + states[j].poly
          del states[j]
          break
      if flag:
        break

class PartState():
  '''Class for a state in the algorithm.
  Saves the dominating vertices in a partition (self.X). Two vertices are in one
  block, if there are connected. In self.marked the dominating state of the vertices
  will be stored. marked[0] = 1 means that the vertex is dominating and marked[1] = 1
  means that the vertex will be dominated.'''
  def __init__(self, X=Partition(), poly=1):
    self.X = X
    self.marked = {}
    self.poly = poly

  def activate(self, element):
    '''Activate the vertex element
    @param element: Vertex to activate
    @return: A new state
    '''
    other = self.copy()
    self.X.addElementNewBlock(element)  # vertex v is dominating
    self.marked[element] = [1, 0]
    self.poly = self.poly * x
    
    other.marked[element] = [0, 0]
    return other

  def deactivate(self, element, n, elementdegree):
    '''Deactivate the vertex element
    @param element: Vertex to activate
    @param n: Number of vertices in the graph
    @param elementdegree: Degree of the vertex element
    '''
    if NUM_ACTIV != n:
      if self.marked[element][0] == 0 and self.marked[element][1] == 0:
        return False
      elif self.marked[element][0] == 0 and self.marked[element][1] == 1:
        self.X.removeElement(element)
        del self.marked[element]
        return True
      elif self.marked[element][0] == 1 and self.marked[element][1] == 0:
        return False
      elif self.marked[element][0] == 1 and self.marked[element][1] == 1:
        blockelement = self.X.komponent(element)
        self.X.removeElement(element)
        del self.marked[element]
        for el in self.X.getElements():
          if blockelement == self.X.komponent(el):
            return True
        return False

    else:  # if all vertices activated
      if self.marked[element][0] == 0 and self.marked[element][1] == 0:
        return False
      elif self.marked[element][0] == 0 and self.marked[element][1] == 1:
        self.X.removeElement(element)
        del self.marked[element]
        return True
      elif self.marked[element][0] == 1 and self.marked[element][1] == 0:
        if elementdegree == n - 1:
          self.X.removeElement(element)
          del self.marked[element]
          return True
        return False
      elif self.marked[element][0] == 1 and self.marked[element][1] == 1:
        blockelement = self.X.komponent(element)
        self.X.removeElement(element)
        del self.marked[element]
        flag = True
        for el in self.X.getElements():
          if self.marked[el][0] == 1 and blockelement == self.X.komponent(el):
            return True
          elif self.marked[el][0] == 1:
            flag = False
        if flag:
          return True
        else:
          return False

  def processEdge(self, edge):
    '''Insert the edge
    @param edge: Edge of the graph
    '''
    u = edge[0]
    v = edge[1]
    if self.marked[u][0] and self.marked[v][0]:
      blocku = self.X.komponent(u)
      blockv = self.X.komponent(v)
      if blocku != blockv:
        self.X.unionBlocks(blocku, blockv)
    if self.marked[u][0] and not self.marked[v][1]:
      self.marked[v][1] = 1
    if self.marked[v][0] and not self.marked[u][1]:
      self.marked[u][1] = 1

  def getPoly(self):
    '''Return the polynomial of the state
    @return: Polynomial of the state'''
    return self.poly

  def copy(self):
    '''Copy the current state
    @return: A copy of the state
    '''
    other = PartState()
    other.X = deepcopy(self.X)
    other.marked = deepcopy(self.marked)
    other.poly = deepcopy(self.poly)
    return other

  def __eq__(self, other):
    '''Test if two states are equal
    @param other: A other state to compare
    @return: Boolean
    '''
    for key in self.marked.keys():
      if key not in other.marked.keys() or self.marked[key][0] != other.marked[key][0] or self.marked[key][1] != other.marked[key][1]:
        return False
    if len(self.marked.keys()) != len(other.marked.keys()):
      return False
    if self.X == other.X:
      return True
    else:
      return False

  def __str__(self):
    '''Gives the description for this state'''
    return "%s - %s - %s" % (str(self.X), str(self.marked), str(self.poly))


# =======================================================================
# =============== SEPCIAL GRAPH CLASSES =================================
# =======================================================================
def calcPath(n):
  '''
  Calculate the connected domination polynomial for the path
  @param n: Order of the graph
  '''
  return x ** n + x ** (n - 1) * 2 + x ** (n - 2)

def calcCycle(n):
  '''
  Calculate the connected domination polynomial for the cycle
  @param n: Order of the graph
  '''
  return x ** n + x ** (n - 1) * n + x ** (n - 2) * n

def calcAntiCycle(n):
  '''
  Calculate the connected domination polynomial for the cycle
  @param n: Order of the graph
  '''
  return (1 + x)**n - 1 - n * x - 2 * n * x**2 - n * x**3

def calcCompleteGraph(n):
  '''
  Calculate the complete graph
  @param n: Order of the graph
  '''
  return (x + 1) ** n - 1

def calcCompleteBipartiteGraph(graph):
  '''Calculates the connected domination polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  return ((x + 1) ** n - 1) * ((x + 1) ** m - 1)

def calcskpath(graph, k):
  '''
  Calculates the connected domination polynomial for a k-path
  '''
  def sumlist(l, start=0):
    v = 0
    for i in range(start, len(l)):
      v += l[i]
    return v

  f = [(1 + x) ** i for i in range(0, k + 1)]
  for i in range(k + 1, graph.order()):
    f.append(x * sumlist(f, len(f) - k))
  return x * sumlist(f, graph.order() - k - 1)

def calculateCartesianPP2(n):
  '''Calculates the Cartesian product of P_n and P_2'''
  p1 = 2 * x**2 + 2*x**3
  p2 = x**2 + 2 * x**3 + x**4
  for _ in range(3, n):
    p1_tmp = x * p1 + 2 * x * p2
    p2_tmp = x**2 * (p1 + p2)
    p1 = p1_tmp
    p2 = p2_tmp
  return x * (1 + x) * p1 + (1 + x)**2 * p2

def calculateLexiPP(n, m):
  '''Calculates the lexicographic product of two paths'''
  return (1 + x)**(2 * m) * ((1 + x)**m - 1)**(n - 2)

def calculateStrongKH(n, graph):
  '''
  Calculates trivariate total domination polynomial of the strong product
  of a complete graph and an arbitrary graph.
  @param n: Order of the path
  @param graph: Second graph
  '''
  Y = compute_polynomial(graph)
  subs_x = (1 + x)**n - 1
  return sympy.expand(Y.subs({x: subs_x}))


# ===================================================================
# ===================================================================
# ===================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, graph1=None, graph2=None, q=None):
  '''
  Calculate the total domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  CD = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("identified complete graph...")
      CD = calcCompleteGraph(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("identified a path...")
      CD = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("identified a cycle...")
      CD = calcCycle(graph.order())
    elif isAntiCycle(graph):
      PyGraphEdit_log.info("identified  an anti-cycle...")
      CD = calcAntiCycle(graph.order())
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("identified complete bipartite graph...")
      CD = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("identified complete graph...")
      CD = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("identified a path...")
      CD = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("identified a cycle...")
      CD = calcCycle(graph.order())
    elif graph.graphtype == graphType.ANTICYCLE:
      PyGraphEdit_log.info("identified  an anti-cycle...")
      CD = calcAntiCycle(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("identified complete bipartite graph...")
      CD = calcCompleteBipartiteGraph(graph)

    elif graph.graphtype == graphType.CARTESIAN and graph1 != None and graph2 != None:
      PyGraphEdit_log.info("searching for an algorithm for the Cartesian product ...")
      if (graph1.graphtype == graphType.PATH and graph2.graphtype == graphType.PATH
          and (graph1.order() == 2 or graph2.order() == 2)):
        PyGraphEdit_log.info("recognized product of two paths.")
        PyGraphEdit_log.info("Starting calculation ...")
        if graph2.order() == 2:
          CD = calculateCartesianPP2(graph1.order())
        else:
          CD = calculateCartesianPP2(graph2.order())

    elif graph.graphtype == graphType.LEXI and graph1 != None and graph2 != None:
      PyGraphEdit_log.info("searching for an algorithm for the lexicographic product ...")
      if (graph1.graphtype == graphType.PATH and graph2.graphtype == graphType.PATH
          and graph1.order() >= 3 and graph2.order() >= 3):
        PyGraphEdit_log.info("recognized product of two paths.")
        PyGraphEdit_log.info("Starting calculation ...")
        CD = calculateLexiPP(graph1.order(), graph2.order())

    elif graph.graphtype == graphType.STRONG and graph1 != None and graph2 != None:
      PyGraphEdit_log.info("searching for an algorithm for the strong product ...")
      if graph1.graphtype == graphType.COMPLETE:
        PyGraphEdit_log.info("recognized product of a graph with a complete graph.")
        PyGraphEdit_log.info("Starting calculation ...")
        CD = calculateStrongKH(graph1.order(), graph2)
      elif graph2.graphtype == graphType.COMPLETE:
        PyGraphEdit_log.info("recognized product of a graph with a complete graph.")
        PyGraphEdit_log.info("Starting calculation ...")
        CD = calculateStrongKH(graph2.order(), graph1)

  if CD:
    if q != None:
      q.put(CD)
    return CD

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration...")
    CD = compute_polynomial(graph)
  elif m == method.DECOMP:
    PyGraphEdit_log.info("starting calculation with recurrence equation...")
    CD = recurrence(graph)
  elif m == method.PATH_DECOMP:
    PyGraphEdit_log.info("starting calculation with path decomposition...")
    CD = path_decomposition(graph)
  elif m == method.SKPATH:
    PyGraphEdit_log.info("starting calculation of the simple k-path...")
    try:
      k = isskPath(graph)
      if k == 0:
        raise err.GraphClassError("simple k-path")

      CD = calcskpath(graph, k)
    except err.GraphClassError as e:
      return str(e)

  else:
    raise err.NotImplementedError()

  if q != None:
    q.put(CD)

  return CD


# ===================================================================
# ===================================================================
# ===================================================================
# ===================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")
