#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Verfahren zur Berechnung des total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import sys
from sympy import symbols, expand

from SGraph.GraphFunctions import connectedComponents  # @UnresolvedImport
from SGraph.GraphFunctions import isPath, isTree, isCycle, isComplete  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


# =============================================================================
# =============================================================================
x = symbols('x')


# =============================================================================
# =============================================================================
def chromPoly(G):
  """
  Chromatic polynomial of the graph G
  @param graph: Graph G
  @return: Chromatic polynomial
  """
  def chrom(G):
    if G.size() == 0:
      return x ** G.order()
    else:
      e = next(iter(G.getEdgeSet()))
      return chrom(G - e) - chrom(G / e)

  return chrom(G)

def chromPolyTutte(G):
  """
  Chromatic polynomial of the graph G
  @param graph: Graph G
  @return: Chromatic polynomial
  """
  from lib.TuttePoly import tutte as computeTuttePoly  # @UnresolvedImport
  poly = computeTuttePoly(G)
  y = symbols('y')
  c = len(connectedComponents(G))
  n = G.order()
  return (-1) ** (n - c) * x ** c * poly.subs({x: 1 - x, y: 0})


# =============================================================================
# ================= SPECIAL GRAPHS ============================================
# =============================================================================
def calcCompleteGraph(n):
  poly = 1
  for i in range(n):
    poly = poly * (x - i)
  return poly

def calcStar(n):
  return x * (x - 1)**(n-1)

def calcSun(n):
  return calcCompleteGraph(n) * (x - 2)**n

def calcSunlet(n):
  return (x - 1)**(2 * n) - (1 - x)**(n - 1)

def calcCycle(n):
  return (-1)**n * (x - 1) + (x - 1)**n

def calcPath(n):
  return x * (x - 1)**(n - 1)

def calcTree(n):
  return (-1)**(n - 1) * x * (1 - x)**(n - 1)

def calcCentipede(n):
  return x * (x - 1)**(2 * n - 1)

def calcWheel(n):
  return x * ((x - 2)**(n - 1) - (-1)**n * (x - 2))

def calcBook(n):
  return (x - 1) * x * (x**2 - 3 * x + 3)**n


# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the perfect domination polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing...")
  PyGraphEdit_log.info("")

  C = None
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("identified complete graph ...")
      C = calcCompleteGraph(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("identified a path ...")
      C = calcPath(graph.order())
    elif isTree(graph):
      PyGraphEdit_log.info("identified a tree ...")
      C = calcTree(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("identified a cycle ...")
      C = calcCycle(graph.order())

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("identified complete graph ...")
      C = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("identified a path ...")
      C = calcPath(graph.order())
    elif graph.graphtype == graphType.TREE:
      PyGraphEdit_log.info("identified a tree ...")
      C = calcTree(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("identified a cycle ...")
      C = calcCycle(graph.order())
    elif graph.graphtype == graphType.WHEEL:
      PyGraphEdit_log.info("identified a wheel ...")
      C = calcWheel(graph.order())
    elif graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("identified a star ...")
      C = calcStar(graph.order())
    elif graph.graphtype == graphType.BOOK:
      PyGraphEdit_log.info("identified a book...")
      C = calcBook(graph.order() / 2 - 1)
    elif graph.graphtype == graphType.SUN:
      PyGraphEdit_log.info("identified a sun ...")
      C = calcSun(graph.order() / 2)
    elif graph.graphtype == graphType.SUNLET:
      PyGraphEdit_log.info("identified a sunlet ...")
      C = calcSunlet(graph.order() / 2)
    elif graph.graphtype == graphType.CENTIPEDE:
      PyGraphEdit_log.info("identified a centipede ...")
      C = calcCentipede(graph.order() / 2)

  if C:
    if q != None:
      q.put(C)
    return C

  if m == method.DECOMP or m == None:
    PyGraphEdit_log.info("starting calculation with decomposition ...")
    C = chromPoly(graph)

  elif m == method.TUTTE:
    PyGraphEdit_log.info("starting calculation using the Tutte polynomial ...")
    C = chromPolyTutte(graph)

  if q != None:
    q.put(expand(C))

  return expand(C)


#=============================================================================
#=============================================================================
#=============================================================================
#=============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum starten verwenden")

