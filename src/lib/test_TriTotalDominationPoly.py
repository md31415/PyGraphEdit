#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculation of the trivariate total domination polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 04.02.2014

@author: Markus Dod
'''

from sympy import symbols, expand


x, y, z = symbols('x y z')


def compute_tri_polynomial(graph):
  pass


def path_decomposition(graph):
  pass

def calculateTree(graph):
  pass


#calcCompleteGraph(n)
#calcCompleteBipartiteGraph(graph)
#calcCompletekPartiteGraph(graph)
#calcPath(n)
