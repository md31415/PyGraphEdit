#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Methods for the calculation of the edge cover polynomial

Created on 09.12.2015

@author: Markus Dod

@note: Use PyGraphEdit.py or DominationMain.py to use the methods in this file!
'''

from sympy import expand, symbols
from sys import exit as sys_exit

from lib.tools import binomial  # @UnresolvedImport
from lib.tools import powerset_list as powerset  # @UnresolvedImport

from SGraph.GraphFunctions import (isComplete, isPath, isCycle,  # @UnresolvedImport
                                  isCompleteBipartite, getBipartition)  # @UnresolvedImport
from SGraph.GraphFunctions import inducedSubgraph  # @UnresolvedImport

from modules.consts import method, graphType  # @UnresolvedImport


__all__ = ['compute_poly']


# =============================================================================
# ================ Globals ====================================================
# =============================================================================
z = symbols('z')


# =============================================================================
# =============================================================================
# =============================================================================
def compute_polynomial(graph):
  '''
  Computes the edges cover polynomial
  '''
  B = 0
  for W in powerset(graph.getV()):
    B = B + (-1)**len(W) * (z + 1) ** (inducedSubgraph(graph, set(W))).size()
  return B

def compute_poly_rec(graph):
  """Returns the edge cover polynomial of G."""
  if graph.order() == 0:
    return 1
  elif graph.size() == 0:
    return 0
  else:
    e = graph.randomEdge()
    u = e[0]
    v = e[1]
    poly1 = compute_poly_rec(graph - e)
    poly2 = compute_poly_rec(graph - v)
    poly3 = compute_poly_rec(graph - u)
    poly4 = compute_poly_rec(graph - u - v)
    return (z + 1) * poly1 + z * (poly2 + poly3 + poly4)


# ============================================================================
# ============= Special graphs ===============================================
# ============================================================================
def calcCompleteGraph(n):
  '''Calculates the edge cover polynomial of the complete graph'''
  poly = 0
  for k in range(n + 1):
    poly += (-1)**(n - k) * binomial(n, k) * (z + 1)**binomial(k, 2)

  return poly

def calcCompleteBipartiteGraph(graph):
  '''Calculates the edge cover polynomial of the complete bipartite graph'''
  _, bipartition = getBipartition(graph)
  n = 0
  m = 0
  for v in bipartition:
    if bipartition[v] == 0:
      n += 1
    else:
      m += 1
  poly = 0
  for k in range(m + 1):
    poly += (-1)**(k) * binomial(m, k) * ((z + 1)**(m - k) - 1)**n

  return poly

def calcPath(n):
  '''Calculates the edge cover polynomial of the path'''
  if n == 1:
    return 0
  elif n == 2:
    return z

  poly = 0
  for k in range(1, n):
    poly += binomial(k - 1, n - k - 1) * z**k

  return poly

def calcCycle(n):
  '''Calculates the edge cover polynomial of the cycle'''
  if n == 1:
    return 0
  elif n == 2:
    return z
  elif n == 3:
    return z**3 + 3 * z**2
  elif n == 4:
    return z**4 + 4 * z**3 + 2 * z**2

  poly = 0
  for k in range(3, n):
    poly += float(n)/(n - k) * binomial(k - 1, n - k - 1) * z**k

  return poly * z**n

def calcStar(n):
  '''Calculate the edge cover polyomial of a star'''
  return z**(n-1)


# =============================================================================
# =============================================================================
# =============================================================================
def compute_poly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate the edge cover polynomial
  @param graph: Graph
  @param m: Method for the calculation
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing the edge cover polynomial ...")
  PyGraphEdit_log.info("")

  EC = None
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      EC = calcCompleteGraph(graph.order())
    elif isPath(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      EC = calcPath(graph.order())
    elif isCycle(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      EC = calcCycle(graph.order())
    elif isCompleteBipartite(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      EC = calcCompleteBipartiteGraph(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      EC = calcCompleteGraph(graph.order())
    elif graph.graphtype == graphType.PATH:
      PyGraphEdit_log.info("starting calculation with algorithm for paths ...")
      EC = calcPath(graph.order())
    elif graph.graphtype == graphType.CYCLE:
      PyGraphEdit_log.info("starting calculation with algorithm for cycles ...")
      EC = calcCycle(graph.order())
    elif graph.graphtype == graphType.COMPBIPARTITE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete bipartite graphs ...")
      EC = calcCompleteBipartiteGraph(graph)
    elif graph.graphtype == graphType.STAR:
      PyGraphEdit_log.info("starting calculation with algorithm for stars ...")
      EC = calcStar(graph.order())

  if EC:
    if q != None:
      q.put(EC)
    PyGraphEdit_log.info("done ...")
    return expand(EC)

  if m == method.STATE_SPACE or m == None:
    PyGraphEdit_log.info("starting complete enumeration ...")
    EC = compute_polynomial(graph)
  elif m == method.DECOMP:
    PyGraphEdit_log.info("starting recursive algorithm ...")
    EC = compute_poly_rec(graph)

  if q != None:
    q.put(EC)

  PyGraphEdit_log.info("done ...")
  return expand(EC)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys_exit("Hauptmodul zum starten verwenden")
