#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Methods for the calculation of the chromatic polynomial

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 29.07.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import cython

cdef chromPoly(G)

@cython.locals(c=cython.int, n=cython.int)
cdef chromPolyTutte(G)

@cython.locals(i=cython.int)
cdef calcCompleteGraph(int n)
cdef calcStar(int n)
cdef calcSun(int n)
cdef calcSunlet(int n)
cdef calcCycle(int n)
cdef calcPath(int n)
cdef calcTree(int n)
cdef calcCentipede(int n)
cdef calcWheel(int n)
cdef calcBookGraph(int n)

cpdef compute_poly(PyGraphEdit_log2, graph, m=*, q=*)
