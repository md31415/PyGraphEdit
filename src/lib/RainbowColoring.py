'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 13.03.2015

@author: Markus Dod
'''

from SGraph.GraphFunctions import minimalSpanningTree, edgeInducedSubgraph  # @UnresolvedImport
from SGraph.GraphFunctions import isComplete, isTree  # @UnresolvedImport

from modules.consts import graphType  # @UnresolvedImport


# =============================================================================
# ======== TRY TO FIND A MINIMUM RAINBOWCOLORING WITH SCIPOPT =================
# =============================================================================
def computeRainbowSCIP(graph, k):
  '''
  Find a rainbow coloring of the graph with maximal k colors
  @param graph: SGraph
  @param k: Maximum number of colors
  @return: Dict with the coloring of the edges 
  '''
  PyGraphEdit_log.info("starting scipopt to rainbow color the graph ...")

  try:
    import modules.scipopt.scip as scip
    from itertools import product
  except:
    PyGraphEdit_log.info("scipoptsuite not avalible")
    return None

  # create solver instance
  s = scip.Solver()
  s.create()
  s.includeDefaultPlugins()
  s.createProbBasic(b"Rainbow Coloring")

  # Indexes for creating variables and constraints
  rows = graph.getEdgeSet()
  vals = range(0, k)

  # add some variables
  probVars = {}
  varNames = {}
  varBaseName = "x"
  for i, k in product(rows, vals):
    if i not in probVars:
      probVars[i] = {}
      varNames[i] = {}
    p = varBaseName + "_" + str(i) + ":" + str(k)
    p = p.encode('ascii')
    varNames[i][k] = p
    probVars[i][k] = s.addIntVar(p, obj=1, lb=0.0, ub=1.0)

  # every edge gets exactly one color
  for i in rows:
    coeffs = {probVars[i][k]: 1.0 for k in vals}
    s.addCons(coeffs, lhs=1.0, rhs=1.0)

  # adjacent vertices getting different colors
  edges = graph.getEdgeSet()
  for v in graph.getV():
    E = []
    for e in graph.getIncidentEdges(v):
      if e in edges:
        E.append(e)
      elif (e[1], e[0]) in edges:
        E.append((e[1], e[0]))
    for k in vals:
      coeffs = {probVars[e][k]: 1.0 for e in E}  # {probVars[e[0]-1][k]: 1.0, probVars[e[1]-1][k]: 1.0}
      s.addCons(coeffs, rhs=1.0)

  # solve problem
  s.solve()
  status = s.getStatus()

  if status == scip.scip_status.optimal:
    # retrieving the best solution
    solution = s.getBestSol()

    # print solution
    sol = dict((i, 0) for i in rows)
    for i in rows:
      for k in vals:
        solValue = round(s.getVal(solution, probVars[i][k]))
        if solValue == 1:
          sol[i] = k + 1

        s.releaseVar(probVars[i][k])

    s.free()
    return sol

  else:
    return None


# =============================================================================
# ============== TRY TO FIND A MINIMUM RAINBOWCOLORING ========================
# =============================================================================
def computeRainbowColSpanningTree(graph):
  # Calculate a minimum spanning tree and color the edges with different colors
  treeEdges, _ = minimalSpanningTree(graph)
  colors = {e: i + 1 for i, e in enumerate(treeEdges)}

  # Try to minimize the colors (color edges between leafs)
  degrees = edgeInducedSubgraph(treeEdges).get_degrees()
  leafs = [v for v in degrees if degrees[v] == 1]
  for v in leafs:
    if degrees[v] == 1:
      for w in leafs:
        if v != w and degrees[w] == 1 and graph.hasEdge((v,w)) and (v, w) not in colors and (w, v) not in colors:
          # Get the neighbor of v in the tree
          for e in treeEdges:
            if v == e[0]:
              u = e[1]
              break
            elif v == e[1]:
              u = e[0]
              break

          # Degree-two-reduction
          if degrees[u] == 2:
            commonColor = 0
            for e in treeEdges:
              if e[0] == u and e[1] != v:
                commonColor = colors[e]
                break
              elif e[1] == u and e[0] != v:
                commonColor = colors[e]
                break

            if (u, v) in colors:
              colors[(u, v)] = commonColor
            elif (v, u) in colors:
              colors[(v, u)] = commonColor
            colors[(v, w)] = commonColor

          else:  # triangle-reduction
            # Get the neihbor of w in the tree
            for e in treeEdges:
              if w == e[0]:
                w_1 = e[1]
                break
              elif w == e[1]:
                w_1 = e[0]
                break
            if u == w_1:
              if (u, v) in colors:
                commonColor = colors[(u,v)]
              else:
                commonColor = colors[(v,u)]
              colors[(v,w)] = commonColor
              if (w,u) in colors:
                colors[(w,u)] = commonColor
              else:
                colors[(u,w)] = commonColor
              degrees[v] += 1
              degrees[w] += 1
              break

  # Recolor the edges beginning with color one
  normalizeColors = {c: i + 1 for i, c in enumerate(list(set(colors.values())))}
  for e in colors:
    colors[e] = normalizeColors[colors[e]]

  # Color the rest edges
  for e in graph.getEdgeSet():
    if e not in colors and (e[1], e[0]) not in colors:
      colors[e] = 1
  return colors, len(normalizeColors)


# =============================================================================
# =================== SPECIAL GRAPH CLASSES ===================================
# =============================================================================
def calcCompleteGraph(graph):
  '''Rainbow coloring of a complete graph'''
  return {e: 1 for e in graph.getEdgeSet()}, 1

def calcTree(graph):
  '''Rainbow coloring of a path'''
  return {e: i + 1 for i,e in enumerate(graph.getEdgeSet())}, graph.size()


# =============================================================================
# =============================================================================
# =============================================================================
def compute_coloring(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calculate a rainbow coloring of the graph
  @param graph: Graph
  @param m: Method for the calculation
  @param q: Queue to save the result
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing a rainbow coloring ...")
  PyGraphEdit_log.info("")

  colors = None
  num_colors = 0
  # If no method is given, then test if the graph is in a special graph class
  if m == None and graph.graphtype == None:
    # Look if the graph is in a special graph class
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      colors, num_colors = calcCompleteGraph(graph)
    elif isTree(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      colors, num_colors = calcTree(graph)

  elif m == None:
    if graph.graphtype == graphType.COMPLETE:
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      colors, num_colors = calcCompleteGraph(graph)
    elif graph.graphtype == graphType.PATH or graph.graphtype == graphType.TREE:
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      colors, num_colors = calcTree(graph)

  if colors:
    if q != None:
      q.put((colors, num_colors))
    return colors, num_colors

  if m == None or m == "greedy":
    PyGraphEdit_log.info("starting complete enumeration ...")
    colors, num_colors = computeRainbowColSpanningTree(graph)

  if q != None:
    q.put((colors, num_colors))

  return colors, num_colors


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  from sys import exit as sys_exit
  sys_exit("Use PyGraphEdit")
