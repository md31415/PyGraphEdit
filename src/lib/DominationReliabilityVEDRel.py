#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Calculates the vertex-edge domination reliability

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 19.02.2013

@author: Markus Dod
'''

import sympy
import sys

from lib.tools import powerset  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport


# =============================================================================
# =============================================================================
# =============================================================================
def state_space_enumeration(graph, d=1):
  '''
  Calculates the VEDRel(G,p) and VEDRel(G,d,p)
  @param graph: Graph
  @param d: Domination distance
  '''
  VEDRel = 0
  p1, p2 = sympy.symbols('p1 p2')

  if d == 1:
    pset = powerset(graph.getV())

    n = graph.order()
    for W in pset:
      # F�r jedes W EDRel(G,W,p) berechnen
      B = [v for v in graph.getV() if v not in W]
      EDRel = 0
      for v in B:
        EDRel = EDRel * (1 - (1 - p2) ** len(graph.neighbors(v) & set(W)))

      VEDRel = VEDRel + p1 ** len(W) * (1 - p1) ** (n - len(W)) * EDRel

  else:
    return "not implemented"

  return VEDRel


# =============================================================================
# =============================================================================
def compute_reliability(MDNetwork_log2, graph, m, d=1, q=None):
  '''
  Calculates the VEDRel(G,p) and VEDRel(G,d,p)
  @param graph: Graph
  @param m: Method
  @param d: Domination distance
  '''
  global MDNetwork_log
  MDNetwork_log = MDNetwork_log2
  MDNetwork_log.info("starting reliability computing...")
  MDNetwork_log.info("")
  MDNetwork_log.info("calculating VEDRel(G, %s, p)" % (d))

  rel = None
  if m == method.STATE_SPACE:
    rel = state_space_enumeration(graph, d)

  else:
    pass

  if q != None:
    q.put(rel)
  return sympy.simplify(rel)


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
