'''
Implements the calculation of the rainbow polynomial and
the rainbow generating function for the usage in PyGraphEdit.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
'''

from copy import copy
from itertools import combinations as comb
from sympy import symbols, degree, expand

from SGraph.GraphFunctions import isComplete, isTree  # @UnresolvedImport
from SGraph.GraphFunctions import components  # @UnresolvedImport

from lib.tools import binomial  # @UnresolvedImport


# ==========================================================
X = set([])
Y = set([])
neighbors = dict()

x = symbols('x')

__all__ = ["calcRainbowPoly"]


# ==========================================================
# ==========================================================
# ==========================================================
def testRainbow(G, coloring):
  '''
  Is a coloring of G rainbow
  @param G: SGraph
  @param coloring: Instance of the class Coloring
  @return: Bool
  '''
  global Y
  Y = set(G.getV())  # set of the not handled vertices
  for s in G.getVIt():
    if not RainbowConnection(G, coloring, s):
      return False
    Y.remove(s)
  return True

def RainbowConnection(G, phi, s):
  '''
  Tests if the vertices s and all other vertices of the
  graph are rainbow connected.
  @param phi: Instance of the class Coloring
  @param s: First vertex
  '''
  global X
  X = set([])
  SearchPath(G, s, set([frozenset([])]), set([]), phi)
  if Y <= X:
    return True
  else:
    return False

def SearchPath(G, v, U, Z, phi):
  '''
  Search a rainbow path between v and all other vertices of the graph
  '''
  global X
  X.add(v)
  if Y <= X:
    return True

  for w in neighbors[v] - Z:
    A = set([])
    for B in U:
      c = phi.getColor(v, w)
      if c not in B:
        A = A | set([B | set([c])])
    if len(A) != 0:
      Z_new = copy(Z)
      Z_new.add(w)
      SearchPath(G, w, A, Z_new, phi)


# ==========================================================
# ========== EDGE-COLORING-CLASS ===========================
# ==========================================================
class Coloring:
  '''Representation of a edge-coloring of a graph'''
  def __init__(self, coloring=dict([])):
    self.coloring = coloring

  def addColor(self, v, w, color):
    '''
    Color an edge of the graph
    @param e: Edge of the graph (tuple)
    @param color: Color of the edge
    '''
    if v in self.coloring:
      self.coloring[v][w] = color
    else:
      self.coloring[v] = {w: color}
    if w in self.coloring:
      self.coloring[w][v] = color
    else:
      self.coloring[w] = {v: color}

  def getColor(self, v, w):
    '''
    Get the color of the edge {v,w}
    @param v: The first vertex of the edge
    @param w: The second vertex of the edge
    @return: The color of the edge
    '''
    return self.coloring[v][w]

  def __str__(self):
    '''Return the string-representation of the coloring'''
    return str(self.coloring)


# ==========================================================
# ========== FUNCTIONS =====================================
# ==========================================================
def partition(set_):
  '''Returns a partition of the elements in the set'''
  if not set_:
    yield []
    return
  for i in range(int(2**len(set_) / 2)):
    parts = [set(), set()]
    for item in set_:
      parts[i&1].add(item)
      i >>= 1
    for b in partition(parts[1]):
      yield [parts[0]] + b

def orderedPartitions(*args):
  '''Returns the ordered partition'''
  def minus(s1, s2): return [x for x in s1 if x not in s2]
  def p(s, *args):
    if not args:
      return [[]]
    return [[c] + r for c in comb(s, args[0]) for r in p(minus(s, c), *args[1:])]

  return p(range(1, sum(args) + 1), *args)

def stirling(n, k):
  '''Calculates the stirling number of second kind'''
  n1 = n
  k1 = k
  if n <= 0:
    return 1
  elif k <= 0:
    return 0
  elif n == 0 and k == 0:
    return -1
  elif n != 0 and n == k:
    return 1
  elif n < k:
    return 0
  else:
    return (k1 * (stirling(n1 - 1, k1))) + stirling(n1 - 1, k1 - 1)

def fallingFactorial(n):
  '''Falling factorial x^{_n_}=x(x-1)(x-2)...(x-n+1)'''
  if n == 0:
    return 1
  elif n == 1:
    return x
  tmp = 1
  for i in range(n):
    tmp = tmp * (x - i)
  return tmp


# ==========================================================
# ==========================================================
# ==========================================================
def calcRainbowGenFunction(graph):
  '''
  Calculate the rainbow generating function using the definition
  @param graph: SGraph
  @return: The rainbow generating function of the graph
  '''
  global neighbors
  neighbors = dict((v, set(graph.neighbors(v))) for v in graph.getV())
  
  rainbowpoly = 0
  edges = graph.getEdgeSet()
  part = partition(edges)
  for p in part:
    coloring = Coloring()
    c = 0
    for e in p:
      c += 1
      for edge in e:
        coloring.addColor(edge[0], edge[1], c)

    if testRainbow(graph, coloring):
      rainbowpoly += x**c

  return rainbowpoly

def calcRainbowPoly(graph):
  '''
  Calculates the rainbow polynomial using the rainbow generating function
  @param graph: SGraph
  '''
  if graph.order() == 1 and graph.size() == 0:
    return 1
  elif graph.order() == 2 and graph.size() == 1:
    return x
  elif len(components(graph)) != 1:
    return 0

  rainbow = calcRainbowGenFunction(graph)
  poly = 0
  for i in range(1, degree(rainbow, x) + 1):
    poly = poly + rainbow.coeff(x**i) * fallingFactorial(i)
  return expand(poly)


# ==========================================================
# =========== SPECIAL GRAPH CLASSES ========================
# ==========================================================
def calcCompleteGraph(m):
  '''Calclates the rainbow generating polynomial of the complete graph K_m'''
  poly = 0
  for i in range(1, m + 1):
    poly += stirling(m, i) * x**(i)
  return poly


# ==========================================================
# ==========================================================
# ==========================================================
def calcPoly(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calc the rainbow polynomial
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing a rainbow generating function ...")
  PyGraphEdit_log.info("")

  poly = 0
  if m == None or m != None:
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      poly = x**binomial(graph.order(), 2)
    elif isTree(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for trees ...")
      poly = fallingFactorial(graph.order() - 1)
    else:
      poly = calcRainbowPoly(graph)
  else:
    poly = calcRainbowPoly(graph)

  if q != None:
    q.put(poly)

  return poly

def calcGenFunction(PyGraphEdit_log2, graph, m=None, q=None):
  '''
  Calc the rainbow generating function
  '''
  global PyGraphEdit_log
  PyGraphEdit_log = PyGraphEdit_log2
  PyGraphEdit_log.info("start computing a rainbow generating function ...")
  PyGraphEdit_log.info("")

  poly = 0
  if m == None or m != None:
    if isComplete(graph):
      PyGraphEdit_log.info("starting calculation with algorithm for complete graphs ...")
      poly = calcCompleteGraph(graph.order())
    else:
      poly = calcRainbowGenFunction(graph)
  else:
      poly = calcRainbowGenFunction(graph)

  if q != None:
    q.put(poly)

  return poly


# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
  from sys import exit as sys_exit
  sys_exit("Use PyGraphEdit")