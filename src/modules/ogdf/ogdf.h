class BM {
  public:
    BM();
    ~BM();
    int test(char *filename);
};


/*
-------------------------------
Multilevel Layouts
-------------------------------
*/
// Fast Multipole Multilevel Method
class FMMM {
  public:
    FMMM();
    ~FMMM();
    int startEmbedding(char *filename);
};


/*
-------------------------------
Energy-based Layouts
-------------------------------
*/
// SpringEmbedderFR algorithm
class SEFR {
	public:
		SEFR();
		~SEFR();
		int startEmbedding(char *filename);
};

// GEM layout algorithm
class GEM {
	public:
		GEM();
		~GEM();
		int startEmbedding(char *filename);
};


/*
-------------------------------
Planar drawing algorithms
-------------------------------
*/
// PlanarStraightLayout
class PSL {
  public:
    PSL();
    ~PSL();
    int startEmbedding(char *filename);
};

// PlanarDrawLayout
class PDL
{
	public:
		PDL();
		~PDL();
		int startEmbedding(char *filename);
};

// MixedModelLayout
class MML
{
	public:
		MML();
		~MML();
		int startEmbedding(char *filename);
};

