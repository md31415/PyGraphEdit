#include <stdlib.h>

#include <ogdf/basic/Graph.h>
#include <ogdf/planarity/BoyerMyrvold.h>
#include <ogdf/energybased/FMMMLayout.h>
#include <ogdf/planarity/PlanarizationLayout.h>
#include <ogdf/planarity/VariableEmbeddingInserter.h>
#include <ogdf/planarity/FastPlanarSubgraph.h>
#include <ogdf/orthogonal/OrthoLayout.h>
#include <ogdf/planarity/EmbedderMinDepthMaxFaceLayers.h>

#include <ogdf/energybased/SpringEmbedderFR.h>
#include <ogdf/energybased/GEMLayout.h>

#include <ogdf/planarlayout/PlanarStraightLayout.h>
#include <ogdf/planarlayout/PlanarDrawLayout.h>
#include <ogdf/planarlayout/MixedModelLayout.h>

#include <ogdf/tree/TreeLayout.h>

#include "test_planarity.hpp"
 
using namespace ogdf;
 
bool test_planarity(char *filename)
{
	Graph G;
	G.readGML(filename);

	BoyerMyrvold bm;
	return bm.isPlanar(G);
}


/*
-------------------------------
Multilevel Layouts
-------------------------------
*/
int fmmm(char *filename)
{
	// Fast Multipole Multilevel Method
	
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	node v;
	forall_nodes(v,G)
		GA.width(v) = GA.height(v) = 10.0;
 
	FMMMLayout fm;
 
	fm.useHighLevelOptions(true);
	fm.unitEdgeLength(15.0); 
	fm.newInitialPlacement(true);
	fm.qualityVersusSpeed(FMMMLayout::qvsGorgeousAndEfficient);
 
	fm.call(GA);
	GA.writeGML(filename); 
 
	return 1;
}


/*
-------------------------------
Energy-based Layouts
-------------------------------
*/
int springEmbedderFR(char *filename)
{
	// Implementation of the SpringEmbedderFR layout algorithm
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	SpringEmbedderFR sefr;
	
	// set node sizes
	node v;
	forall_nodes(v, G)
	{
		GA.width(v) = 10;
		GA.height(v) = 10;
	} // forall_nodes
	
	// initialize some node position
	GA.x(G.firstNode()) = 18;
	GA.y(G.firstNode()) = 18;
	
	sefr.call(GA);
	GA.writeGML(filename);
 
	return 1;
}

int gemLayout(char *filename)
{
	// Implementation of the GEM-layout algorithm
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	GEMLayout geml;
	
	// set initial node positions
	node v;
	forall_nodes(v, G)
	{
		GA.x(v) = rand() % 50;
		GA.y(v) = rand() % 50;
	} // forall_nodes

	geml.attractionFormula(2);
	geml.call(GA);
	GA.writeGML(filename);
 
	return 1;
}


/*
-------------------------------
Planar drawing algorithms
-------------------------------
*/
int straightLayout(char *filename)
{
	// Implementation of the Planar-Straight layout algorithm
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	BoyerMyrvold bm;
	if(bm.isPlanar(G)){
		PlanarStraightLayout psl;

		psl.call(GA);
		GA.writeGML(filename);
	}
	else return 0;
 
	return 1;
	
}

int planarDrawLayout(char *filename)
{
	// Implementation of the PlanarDrawLayout algorithm
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	BoyerMyrvold bm;
	if(bm.isPlanar(G)){
		PlanarDrawLayout psl;

		psl.call(GA);
		GA.writeGML(filename);
	}
	else return 0;
 
	return 1;
}

int mixedModelLayout(char *filename)
{
	// Implementation of the MixedModelLayout algorithm
	Graph G;
	GraphAttributes GA(G);
	G.readGML(filename);
	
	BoyerMyrvold bm;
	if(bm.isPlanar(G)){
		MixedModelLayout mml;

		mml.call(GA);
		GA.writeGML(filename);
	}
	else return 0;
 
	return 1;
}


