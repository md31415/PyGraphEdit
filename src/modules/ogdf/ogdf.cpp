#include "ogdf.h"
#include "test_planarity.hpp"

BM::BM()
{
}

BM::~BM()
{
}

int BM::test(char *filename)
{
	// Planarity test using the algorithm of Boyer-Myrvold
	return test_planarity(filename);
}


/*
-------------------------------
Multilevel Layouts
-------------------------------
*/

FMMM::FMMM()
{
}

FMMM::~FMMM()
{
}

int FMMM::startEmbedding(char *filename)
{
	// Fast Multipole Multilevel Method
	return fmmm(filename);
}


/*
-------------------------------
Energy-based Layouts
-------------------------------
*/
SEFR::SEFR()
{
}

SEFR::~SEFR()
{
}

int SEFR::startEmbedding(char *filename)
{
	// Implementation of the SpringEmbedderFR algorithm
	return springEmbedderFR(filename);
}

GEM::GEM()
{
}

GEM::~GEM()
{
}

int GEM::startEmbedding(char *filename)
{
	// Implementation of the GEM layout algorithm
	return gemLayout(filename);
}


/*
-------------------------------
Planar drawing algorithms
-------------------------------
*/

// Implementation of the Planar-Straight layout algorithm
PSL::PSL()
{
}

PSL::~PSL()
{
}

int PSL::startEmbedding(char *filename)
{
	return straightLayout(filename);
}

// Implementation of the PlanarDrawingLayout algorithm
PDL::PDL()
{
}

PDL::~PDL()
{
}

int PDL::startEmbedding(char *filename)
{
	return planarDrawLayout(filename);
}

// Implementation of the MixedModelLayout algorithm
MML::MML()
{
}

MML::~MML()
{
}

int MML::startEmbedding(char *filename)
{
	return mixedModelLayout(filename);
}

