#ifndef test_plan__
#define test_plan__

bool test_planarity(char *filename);

/*
-------------------------------
Multilevel Layouts
-------------------------------
*/
int fmmm(char *filename);

/*
-------------------------------
Energy-based Layouts
-------------------------------
*/
int springEmbedderFR(char *filename);
int gemLayout(char *filename);

/*
-------------------------------
Planar drawing algorithms
-------------------------------
*/
int straightLayout(char *filename);
int planarDrawLayout(char *filename);
int mixedModelLayout(char *filename);


#endif

