'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.02.2015

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
from random import random as random_rand, choice as random_choice  # @UnresolvedImport

from SGraph.GraphFunctions import inducedSubgraph, isComplete  # @UnresolvedImport

from modules.consts import simulation_type  # @UnresolvedImport
from modules.SaveLoadSimulation import SimulationData  # @UnresolvedImport


# =============================================================================
# ============ BFS and DFS ====================================================
# =============================================================================
def BFS(graph, start):
  '''BFS-Simulation'''
  simdata = SimulationData()
  simdata.setSimType(simulation_type.MARKDIFF)
  simdata.setNextStep(dict([]))
  simdata.setNextStep(dict([(start,"red")]))

  visited = {i: False for i in graph.getV()}

  from collections import deque
  queue = deque()
  queue.append(start)
  visited[start] = True
  while len(queue) != 0:
    v = queue.popleft()
    for w in graph.neighbors(v):
      if visited[w] == False:
        queue.append(w)
        visited[w] = True
        simdata.setNextStep({w: "red"})

  return simdata, simulation_type.MARKDIFF

def DFS(graph, start):
  '''DFS-Simulation'''
  def DFS_visit(u):
    '''Recursive DFS-function'''
    col[u] = 'g'
    simdata.setNextStep({u: "cyan"})
    for v in graph.neighbors(u):
      if col[v] == 'w':
        pi[v] = u
        DFS_visit(v)

    col[u] = 's'
    simdata.setNextStep({u: "red"})

  # Init the simulation
  simdata = SimulationData()
  simdata.setSimType(simulation_type.MARKDIFF)
  simdata.setNextStep(dict([]))

  # Init some variables
  col = {v: "w" for v in graph.getV()}
  pi = {v: None for v in graph.getV()}

  # Start the algorithm
  DFS_visit(start)
  for u in graph.getV():
    if col[u] == 'w':
      DFS_visit(u)

  return simdata, simulation_type.MARKDIFF


# =============================================================================
# ============ BROADCAST ======================================================
# =============================================================================
def broadcasting(graph, startVertices, everyVertex=False, steps=100, probability=1.0,
                 wholeNeighborhood=False, randomNeighbor=False, hiddenNode=False):
  '''
  Simulate the way of a message in a graph
  @param graph: SGraph
  @param startVertices: Start vertices for the broadcasting
  @param everyVertex: All vertices which have the information sends it in every step
  @param steps: Maximum number of steps
  @param probability: Probability
  @param wholeNeighborhood: Send to the whole neighborhood?
  @param randomNeighbor: Send to a random neighbor?
  @param hiddenNode: Respect the hidden-node-problem in the simulation
  '''
  n = graph.order()
  
  # Init the simulation
  simdata = SimulationData()
  simdata.setSimType(simulation_type.MARKDIFF)
  simdata.setNextStep(dict(zip(startVertices, ["green"] * len(startVertices))))
  
  startVertices = set(startVertices)
  infVertices = startVertices  # Informed vertices
  lastVertices = startVertices  # Informed vertices in the last step
  newVertices = startVertices  # New informed vertices
  sendVertices = startVertices  # Vertices which get the information (for hidden node functionality)
  sender = dict([])
  step = 0
  while len(infVertices) != n and step < steps and (everyVertex or len(newVertices) != 0):
    newVertices = set([])
    sendVertices = set([])
    sender = dict([])
    
    # Every vertex sends on its whole neighborhood
    if everyVertex and wholeNeighborhood:
      for v in infVertices:
        if probability >= random_rand():
          N = graph.neighbors(v) - infVertices
          sender[v] = N
          if hiddenNode:  # Respect the hidden node problem
            newVertices = newVertices ^ (N - (sendVertices - newVertices))
            sendVertices = sendVertices | N
          else:
            newVertices |= N

    # Every vertex sends to one random vertex in its neighborhood
    elif everyVertex and not wholeNeighborhood and randomNeighbor:
      for v in infVertices:
        w = random_choice(list(graph.neighbors(v)))
        sender[v] = set([w])
        if not hiddenNode and w not in infVertices:
          newVertices.add(w)
        elif hiddenNode and w not in infVertices and w not in sendVertices:
          newVertices.add(w)
          sendVertices.add(w)
        elif hiddenNode and w not in infVertices and w in sendVertices:
          newVertices.discard(w)

    # Every vertex sends on each of its neighbors with the given probability
    elif everyVertex and not wholeNeighborhood and not randomNeighbor:
      for v in infVertices:
        sender[v] = set([])
        for w in graph.neighbors(v):
          if probability >= random_rand() and w not in infVertices:
            sender[v].add(w)
            if not hiddenNode:
              newVertices.add(w)
            elif hiddenNode and w not in sendVertices:
              newVertices.add(w)
              sendVertices.add(w)
            elif hiddenNode and w in sendVertices:
              newVertices.discard(w)

    # Only the in the last step informed vertices sends the information
    elif not everyVertex and wholeNeighborhood:
      for v in lastVertices:
        if probability >= random_rand():
          N = graph.neighbors(v) - infVertices
          sender[v] = N
          if not hiddenNode:
            newVertices |= N
          else:
            newVertices ^= (N - (sendVertices - newVertices))
            sendVertices |= N

    # Only the in the last step informed vertices sends to one random vertex in its neighborhood
    elif not everyVertex and not wholeNeighborhood and randomNeighbor:
      for v in lastVertices:
        w = random_choice(list(graph.neighbors(v)))
        sender[v] = set([w])
        if not hiddenNode and w not in infVertices:
          newVertices.add(w)
        elif hiddenNode and w not in infVertices and w not in sendVertices:
          newVertices.add(w)
          sendVertices.add(w)
        elif hiddenNode and w in sendVertices:
          newVertices.discard(w)

    # Only the in the last step informed vertices sends the information (edge probability)
    elif not everyVertex and not wholeNeighborhood and not randomNeighbor:
      for v in lastVertices:
        sender[v] = set([])
        for w in graph.neighbors(v):
          if probability >= random_rand():
            sender[v].add(w)
            if not hiddenNode and w not in infVertices:
              newVertices.add(w)
            elif hiddenNode and w not in infVertices and w not in sendVertices:
              newVertices.add(w)
              sendVertices.add(w)
            elif hiddenNode and w in sendVertices:
              newVertices.discard(w)

    # Check if the neighbors of the hiddenNodes in the sending vertices induce a clique in the graph
    if hiddenNode:
      hiddenNodes = sendVertices - newVertices
      for v in hiddenNodes:
        hiddenSender = set([w for w in sender if v in sender[w]])
        H = inducedSubgraph(graph, set(graph.neighbors(v)) & hiddenSender)
        if isComplete(H):
          newVertices.add(v)

    # Set the infos for the new step (new vertices red, old cyan)
    d = dict(zip(newVertices, ["red"] * len(newVertices)))
    d.update(dict(zip(lastVertices - startVertices, ["cyan"] * len(lastVertices - startVertices))))
    lastVertices = newVertices
    simdata.setNextStep(d)

    # Set the informed vertices
    infVertices = infVertices | newVertices
    step += 1
  
  return simdata, simulation_type.MARKDIFF


# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
    pass
