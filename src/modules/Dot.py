'''
Created on 30.10.2013

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''
import os
from platform import system
import tempfile
import subprocess

import pydot
from pydot import InvocationException


# =========================================================================================
class Dot(pydot.Dot):
  '''
  Overwrites some methods from pydot
  '''
  def __init__(self, graphvizdir, graph_type='graph', outputorder="edgesfirst"):
    pydot.Dot.__init__(self, graph_type='graph', outputorder="edgesfirst")
    self.plotstyle = 'twopi'
    self.graphvizdir = graphvizdir

  def write(self, path, prog=None, format='raw'):
    self.mode = 1
    if system() == "Windows" and self.graphvizdir:    
      self.set_graphviz_executables({'dot': self.graphvizdir + 'dot.exe', 
                                     'twopi': self.graphvizdir + 'twopi.exe', 
                                     'neato': self.graphvizdir + 'neato.exe', 
                                     'circo': self.graphvizdir + 'circo.exe', 
                                     'fdp': self.graphvizdir + 'fdp.exe'})
    else:
      self.progs = ({'dot': 'dot', 'twopi': 'twopi', 'neato': 'neato', 
                     'circo': 'circo', 'fdp': 'fdp'})
    if prog is None:
      prog = self.prog

    if path == None:
      _, path = tempfile.mkstemp()
    dot_fd = open(path, "w+b")
    if format == 'raw':
      dot_fd.write(self.to_string().encode("utf-8"))
    else:
      line = str(self.create(prog, format)).split("\\n")
      dot_fd.close()
      return line
    dot_fd.close()

    return True

  def write2_img(self, path, prog=None, imageformat='png'):
    self.mode = 2

    if system() == "Windows" and self.graphvizdir:    
      self.set_graphviz_executables({'dot': self.graphvizdir + 'dot.exe', 
                                     'twopi': self.graphvizdir + 'twopi.exe', 
                                     'neato': self.graphvizdir + 'neato.exe', 
                                     'circo': self.graphvizdir + 'circo.exe', 
                                     'fdp': self.graphvizdir + 'fdp.exe'})
    else:
      self.progs = ({'dot': 'dot', 'twopi': 'twopi', 'neato': 'neato', 
                     'circo': 'circo', 'fdp': 'fdp'})

    if prog is None:
      prog = self.prog

    if path == None:
      _, path = tempfile.mkstemp()
    dot_fd = open(path, "w+b")
    if imageformat == 'raw':
      dot_fd.write(self.to_string().encode("utf-8"))
    else:
      dot_fd.write(self.create(prog, imageformat))
    dot_fd.close()

    return True

  def create(self, prog=None, format='ps'):
    """Creates and returns a Postscript representation of the graph.
 
    create will write the graph to a temporary dot file and process
    it with the program given by 'prog' (which defaults to 'twopi'),
    reading the Postscript output and returning it as a string is the
    operation is successful.
    On failure None is returned.
     
    There's also the preferred possibility of using:
     
        create_'format'(prog='program')
         
    which are automatically defined for all the supported formats.
    [create_ps(), create_gif(), create_dia(), ...]
    """  
    if prog is None:
        prog = self.prog
 
    if self.progs is None:
      #self.progs = find_graphviz()
      if self.progs is None:
        raise InvocationException('GraphViz\'s executables not found' )
 
    if not prog in self.progs:
      raise InvocationException('GraphViz\'s executable "%s" not found' % prog)
 
#    if not os.path.exists( self.progs[prog] ) or not os.path.isfile( self.progs[prog] ):
#      raise InvocationException('GraphViz\'s executable "%s" is not a file or doesn\'t exist' % self.progs[prog])
 
    tmp_fd, tmp_name = tempfile.mkstemp()
    os.close(tmp_fd)
    if self.mode == 1:
      self.write(tmp_name)
    else:
      self.write2_img(tmp_name, imageformat='raw')
    tmp_dir = os.path.dirname(tmp_name)
 
    # For each of the image files...
    for img in self.shape_files:
      # Get its data
      f = open(img, 'rb')
      f_data = f.read()
      f.close()
 
      # And copy it under a file with the same name in the temporary directory
      f = open( os.path.join(tmp_dir, os.path.basename(img)), 'wb')
      f.write(f_data)
      f.close()
 
    # global plot style
    if self.mode == 1:
      p = subprocess.Popen((self.progs[prog], '-T'+format, '-K'+self.plotstyle, tmp_name),
                           cwd=tmp_dir,
                           stderr=subprocess.PIPE,
                           stdout=subprocess.PIPE)
    else:
      p = subprocess.Popen((self.progs[prog], '-T'+format, '-Kneato', '-n', tmp_name),
                           cwd=tmp_dir,
                           stderr=subprocess.PIPE,
                           stdout=subprocess.PIPE)
 
    stderr = p.stderr
    stdout = p.stdout
 
    stdout_output = list()
    while True:
      data = stdout.read()
      if not data:
        break
      stdout_output.append(data)
    stdout.close()
 
    stdout_output = b''.join(stdout_output)
 
    if not stderr.closed:
      stderr_output = list()
      while True:
        data = stderr.read()
        if not data:
          break
        stderr_output.append(str(data, 'ascii'))
      stderr.close()
 
      if stderr_output:
        stderr_output = ''.join(stderr_output)
 
    status = p.wait()
 
    if status != 0 :
      raise InvocationException('Program terminated with status: %d. stderr follows: %s' % (status, stderr_output))
    elif stderr_output:
      print(stderr_output)
 
    # For each of the image files...
    for img in self.shape_files:
      # remove it
      os.unlink( os.path.join( tmp_dir, os.path.basename(img) ) )
 
    os.unlink(tmp_name)
 
    return stdout_output
