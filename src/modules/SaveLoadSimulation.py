'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 03.11.2014

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from sqlite3 import connect, register_adapter, register_converter, PARSE_DECLTYPES #@UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphParser import (graphtograph6, graphkonverter,  # @UnresolvedImport
                                dictadapter, dictkonverter)  # @UnresolvedImport


# =====================================================================
def saveSimulation(filename, graph, coord, simulationName, simulationData, simulationType, 
                   overwriteGraph=True, overwriteSimulation=True):
  '''
  Save a simulation and the corresponding graph in a sqlite-database
  @param filename: Name of the database file
  @param graph: SGraph
  @param coord: Coordinates of the vertices of the graph
  @param simulationName: Name of the simulation
  @param simulationData: Dict with the simulation data
  @param simulationType: Type of the simulation
  @param overwriteGraph: Overwrite the graph in the file if it already exists?
  '''
  name = graph.name

  connection = connect(filename, detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()

  try:
    cursor.execute("""CREATE TABLE Graphen (name TEXT, graph SGraph, n INTEGER, m INTEGER, koords dict)""")
    cursor.execute("""CREATE TABLE Simulation (graphname TEXT, simname TEXT, simtype TEXT, data SimulationData)""")
  except:
    pass

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)
  register_adapter(SimulationData, simulationdataadapter)
  register_converter("SimulationData", simulationdatakonverter)

  cursor.execute("SELECT graph FROM Graphen WHERE name='%s'" % name)
  r = cursor.fetchall()
  if len(r) != 0 and overwriteGraph:
    cursor.execute("DELETE FROM Graphen WHERE name='%s'" % name)

  if len(r) == 0 or overwriteGraph:
    werte = (name, SGraph(graph.getV(), graph.getEdgeSet()), len(graph.getV()), graph.size(), coord)
    sql = "INSERT INTO Graphen VALUES (?, ?, ?, ?, ?)" 
    cursor.execute(sql, werte)

  cursor.execute("SELECT data FROM Simulation WHERE graphname='%s' AND simname='%s'" % (name, simulationName))
  r = cursor.fetchall()
  if len(r) != 0 and overwriteSimulation:
    cursor.execute("DELETE FROM Simulation WHERE graphname='%s' AND simname='%s'" % (name, simulationName))
  elif len(r) != 0:
    cursor.close()
    return

  werte = (name, simulationName, str(simulationType), simulationData)
  sql = "INSERT INTO Simulation VALUES (?, ?, ?, ?)" 
  cursor.execute(sql, werte)

  connection.commit()
  cursor.close()

def graphadapter(graph):
  return ">>graph6<<" + graphtograph6(graph)

def loadSimulation(filename, graphname, simulationName):
  '''
  Load a simulation from a sqlite database
  @param filename: Filename of the database
  @param graphname: Name of the graph
  @param simulationName: Name of the simulation
  @return: simulationData, simulationType
  '''
  connection = connect(filename, detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()

  register_adapter(SimulationData, simulationdataadapter)
  register_converter("SimulationData", simulationdatakonverter)

  try:
    cursor.execute("SELECT data, simtype FROM Simulation WHERE graphname='%s' AND simname='%s'" % (graphname, simulationName))
    data, simtype = cursor.fetchall()[0]
  except:
    print("Datei nicht vorhanden oder falsches Format - Sim")
    return None

  cursor.close()

  return data, simtype

def simulationdataadapter(d):
  '''Adapter for the Simulation class'''
  return d.getString()

def simulationdatakonverter(st):
  '''Konverter for the simulation class'''
  d = SimulationData()
  d.fromString(st)
  return d

def getSimNames(fname, graphname):
  '''
  Return all simulation names
  @param fname: Filename
  @param graphname: Name of the graph
  @return: List of the simulation names of the graph in the file
  '''
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    return None
  cursor = connection.cursor()

  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)
  register_adapter(SimulationData, simulationdataadapter)
  register_converter("SimulationData", simulationdatakonverter)

  try:
    cursor.execute("SELECT simname FROM Simulation WHERE graphname='%s'" % graphname)
  except:
    return None

  try:
    names = cursor.fetchall()
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None
  n = [0]
  for name in names:
    n.append(name[0])

  if len(n) == 1:
    n.append(None)

  cursor.close()
  return n

def deleteSimulations(fname, graphname):
  '''
  Delete all simulations of the graph graphname
  @param fname: Name of the database
  @param graphname: Name of the graph
  '''
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    return None
  cursor = connection.cursor()

  try:
    cursor.execute("DELETE FROM Simulation WHERE graphname='%s'" % graphname)
  except:
    pass

  connection.commit()
  cursor.close()


# =====================================================================
# ========== SimulationObject =========================================
# =====================================================================
class SimulationData(object):
  '''
  A class to manage the simulation data
  '''
  def __init__(self, data=None, simType=None):
    if data:
      self.data = data
    else:
      self.data = []
    self.simType = simType

  def setData(self, data):
    '''Set the simulation data'''
    self.data = data

  def getData(self):
    '''Return the simulation data'''
    return self.data

  def getDataIterate(self):
    '''An iterator for the simulation data'''
    for d in self.data:
      yield d

  def setSimType(self, simType):
    '''Set the simulation type'''
    self.simType = simType

  def getSimType(self):
    '''Return the simulation type'''
    return self.simType

  def setNextStep(self, step):
    '''Add a new step to the simulation data'''
    self.data.append(step)

  def getStep(self, step):
    '''Get a step of the simulation'''
    if step < len(self.data):
      return self.data[step]
    else:
      return 0

  def getNumSteps(self):
    '''Return the number of simulation steps'''
    return len(self.data)  

  def getString(self):
    '''Get a string representation of the simulation data'''
    return str(self.data)

  def fromString(self, s):
    '''Generate the data from a string'''
    import ast
    s_str = str(s)
    self.data = ast.literal_eval(s_str[2:len(s_str) - 1])
    return True


# =====================================================================
# =====================================================================
# =====================================================================
if __name__ == '__main__':
  import sys.exit as sys_exit
  sys_exit("Use PyGraphEdit")
  