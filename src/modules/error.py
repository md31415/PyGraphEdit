'''
Implements some basic exceptions for PyGraphEdit

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 22.08.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

class Error(Exception):
  '''Base class for exceptions in this module.'''
  pass

class GraphClassError(Error):
  '''Wrong graph class'''
  def __init__(self, requestedClass, currentClass=None):
    super().__init__()
    self.currentClass = currentClass
    self.requestedClass = requestedClass

  def __str__(self):
    return "Your graph is not a %s!" % (self.requestedClass)

class QueerError(Error):
  '''Queer error happens'''
  def __init__(self, values=None):
    super().__init__()
    self.values = values

  def __str__(self):
    return "Some queer error happens! -> %s" %self.values

class InputError(Error):
  '''Input error happens'''
  def __init__(self, values=None):
    super().__init__()
    self.values = values

  def __str__(self):
    return "User input is not valid! -> %s" %self.values
