'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 28.11.2015

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from sqlite3 import connect, register_converter, PARSE_DECLTYPES  # @UnresolvedImport
import sys
import time
import xml.dom.minidom as dom

from PyQt5 import QtCore  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphParser import (load_graphdb, parse_graph6,  # @UnresolvedImport
                                graphtograph6, getGraphNamesdb)  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport

STREAMWRITER = None
PyGraphEdit_log = None
IMPLEMENTEDMODULES = None


# =====================================================================
class TestUM(QtCore.QObject):
  '''Class for the calculation of automatic tests'''
  finished = QtCore.pyqtSignal()

  def __init__(self, streamwriter, log, modules):
    super(TestUM, self).__init__(None)

    self.testresult = {}
    self.testcase = None
    self.newResults = False
    self.testok = True
    self.start = False
    global STREAMWRITER, PyGraphEdit_log, IMPLEMENTEDMODULES
    STREAMWRITER = streamwriter
    PyGraphEdit_log = log
    IMPLEMENTEDMODULES = modules

  def setup_tests(self, filename, testmodule, testfunction, testmethod):
    '''Setup the test'''
    self.filename = filename
    self.module = testmodule
    self.function = testfunction
    self.method = testmethod

    STREAMWRITER.write("Reading testfile %s ..." % self.filename)
    STREAMWRITER.write("")

    try:
      tests = dom.parse(self.filename)
    except:
      STREAMWRITER.write("unable to load file %s" % self.filename)
      return

    tests_for_fkt = []
    for test in tests.firstChild.childNodes:
      if test.nodeName != "#text":
        # Load the known result
        if test.hasAttribute("result"):
          knownresult = test.getAttribute("result")
        else:
          knownresult = False

        # Load the graph
        if test.hasAttribute("graph6"):
          graph = parse_graph6(test.getAttribute("graph6"))
          if test.hasAttribute("graph"):
            tests_for_fkt.append((test.getAttribute("graph"),
                                  graph, knownresult))
          else:
            tests_for_fkt.append((test.getAttribute("graph6"),
                                  graph, knownresult))

        else:
          graph, _ = load_graphdb("../data/" + test.getAttribute("graphfile"),
                                  test.getAttribute("graph"))
          tests_for_fkt.append((test.getAttribute("graph"),
                                graph, knownresult))

    self.testcase = tests_for_fkt
    self.start = True

  def test_poly(self):
    '''Start the test'''
    import importlib
    from sympy.parsing import sympy_parser

    while True:
      if self.start:
        break
      time.sleep(1)

    STREAMWRITER.write("Starting test for %s ..." % self.module)
    sys.stdout.flush()
    STREAMWRITER.write("")
    self.start = time.time()

    function_string = self.module + '.' + self.function
    mod_name, func_name = function_string.rsplit('.', 1)
    mod = importlib.import_module(mod_name)
    func = getattr(mod, func_name)

    result_stat = {}
    for test in self.testcase:
      graphname = test[0]
      STREAMWRITER.write(graphname, end="")

      reference_result = test[2]
      start_time = time.time()
      if self.module in IMPLEMENTEDMODULES:  # Is it an intern (poly) function?
        if self.method == method.AUTOMATIC:
          res = func(PyGraphEdit_log, test[1], None)
        else:
          res = func(PyGraphEdit_log, test[1], self.method)
      else:  # For user defined functions call it only with the SGraph as parameter
        res = func(test[1])

      running_time = time.time() - start_time
      try:
        res = res.expand()
      except:
        pass

      correct = None
      if reference_result:
        knownres = sympy_parser.parse_expr(reference_result)
        if res == knownres:
          STREAMWRITER.write(" ... OK", end="")
          correct = True
        else:
          STREAMWRITER.write(" ... failed", end="")
          correct = False
          self.testok = False
      else:
        STREAMWRITER.write(" ... new", end="")
        correct = "New"
        self.newResults = True

      sys.stdout.flush()

      STREAMWRITER.write("     running-time: %.4f" % running_time)
      result_stat[graphname] = {
          "time": running_time,
          "correct": correct,
          "result": res}
    self.testresult = result_stat
    self.end = time.time()

    STREAMWRITER.write("")
    STREAMWRITER.write("All tests passed: %s" % str(self.testok))
    STREAMWRITER.write("running time: %.3f" % (self.end - self.start))

    if self.newResults:
      self.savePoly()

    self.start = False
    self.finished.emit()

  def saveResult(self, filename=None):
    '''Save the results in a new file'''
    if filename is None:
      filename = self.filename + "-result.xml"

    # Create the xml-file
    tests = dom.DOMImplementation().createDocument(None, "Result", None)
    root = tests.documentElement
    root.setAttribute("Module", self.module)
    root.setAttribute("Method", self.method.name)
    root.setAttribute("Function", self.function)
    for i, test in enumerate(self.testresult.keys()):
      testnode = tests.createElement("Test" + str(i))
      testnode.setAttribute("graph", test)
      testnode.setAttribute("time", str(self.testresult[test]["time"]))
      testnode.setAttribute("correct", str(self.testresult[test]["correct"]))
      testnode.setAttribute("result", str(self.testresult[test]["result"]))
      root.appendChild(testnode)

    # Write the xml-file
    with open(filename, "w") as f:
      tests.writexml(f, "", "\t", "\n")

  def savePoly(self):
    '''Saves the calculated polynomials in the testfile'''
    STREAMWRITER.write("")
    STREAMWRITER.write("starting saving the new results ....", end="")
    tests = dom.parse(self.filename)
    for test in tests.firstChild.childNodes:
      if test.nodeName != "#text":
        if test.hasAttribute("graph"):
          graphname = test.getAttribute("graph")
        else:
          graphname = test.getAttribute("graph6")

        if self.testresult[graphname]["correct"] == 'New':
          test.setAttribute("result", str(self.testresult[graphname]["result"]))

    # Write the xml-file
    with open(self.filename, "w") as f:
      tests.writexml(f, "", "\t", "\n")

    STREAMWRITER.write("done")


# =====================================================================
class TestsCompare:
  '''Class to compare result-files'''
  def __init__(self):
    self.results = None
    self.fileNames = []

  def CompareResults(self, filenames):
    '''
    Compares the results in the list filenames
    @param filenames: List of the names of the testfiles
    '''
    self.fileNames = filenames

    methods = []  # Used method-strings
    self.results = {}
    for file in filenames:
      tests = dom.parse(file)
      if tests.firstChild.nodeName == "Result":  # Check if the file is a result file
        # Load the method from the file
        method = tests.firstChild.getAttribute("Method")

        # Check if a method was defined (all user def-function have no method)
        if len(method) == 0:
          method = tests.firstChild.getAttribute("Module") + "." + tests.firstChild.getAttribute("Function")

        # Check if this method-string already exists
        if method in methods:
          filepath = file.split("/")
          method = method + " (%s)" % filepath[len(filepath) - 1]
        methods.append(method)

        # Get the informations from the calculated graphs
        for test in tests.firstChild.childNodes:
          if test.nodeName != "#text":
            # Set the graphname
            if test.hasAttribute("graph"):
              graphname = test.getAttribute("graph")
            else:
              graphname = test.getAttribute("graph6")

            # Get the time and the result
            time = float(test.getAttribute("time"))
            result = str(test.getAttribute("result"))

            # Save the information in self.results
            if graphname in self.results.keys():
              self.results[graphname][method] = (time, result)
            else:
              self.results[graphname] = {method: (time, result)}

  def writeResult(self, key="time"):
    '''
    Write the result
    @param key: Which information should be shown (time oder result)?
    '''
    STREAMWRITER.write("Comparing files %s" % str(self.fileNames))
    STREAMWRITER.write("")

    # Save the name of all used methods in a list
    methods = []
    for graphname in self.results:
      for m in self.results[graphname].keys():
        if m not in methods:
          methods.append(m)

    # Get the length of the longest graphname
    len_name = max([len(name) for name in self.results]) + 2
    len_result = max((max([len(self.results[graphname][m][1]) for m in methods if m in self.results[graphname]]), len("Result"))) + 2

    # Print the first line of the table
    s = format("Name", "%ds" % len_name)
    if key == "time (with result)":
      s += format("Result", "%ds" % len_result)

    for m in methods:
      s += format(m, "%ds" % (len(m) + 2))
    STREAMWRITER.write(s)

    # Print the rest of the lines
    for graphname in self.results:
      s = format(graphname, "%ds" % len_name)
      # Write the result column if necessary
      if key == "time (with result)":
        for m in methods:
          if m in self.results[graphname]:
            result = self.results[graphname][m][1]
            break
        s += format(result, ">%ds" % len_result)

      # Write the rest of the columns
      for m in methods:
        if m in self.results[graphname].keys():
          if key == "result":
            s += format(self.results[graphname][m][1],
                        ">%ds" % (len(m) + 2))
          else:
            s += format("%.4f" % self.results[graphname][m][0],
                        ">%ds" % (len(m) + 2))
        else:
          s += format("", "%ds" % (len(m) + 2))

      STREAMWRITER.write(s)

  def writeLaTeXTable(self, outputfile, key="time"):
    '''
    Write a LaTeX-table
    @param outputfile: Name of the LaTeX-file to write the table
    @param key: Which information should be shown (time oder result)?
    '''
    if ".tex" not in outputfile:
      outputfile += ".tex"

    # Definition of some strings
    begin = "\\begin{table*}[h]\n\
  \\centering\n\
  \\ra{1.3}\n\
  \\begin{tabular}{@{}l%s@{}}\n\
  \\toprule\n"
    end = "\\bottomrule\n\
  \\end{tabular}\n\
  \\caption{Results}\n\
  \\end{table*}"

    # Save the name of all used methods in a list
    methods = []
    for graphname in self.results:
      for m in self.results[graphname].keys():
        if m not in methods:
          methods.append(m)

    # Write the things in the file
    with open(outputfile, 'w') as file:
      # Write the head of the table
      if key == "time (with result)":
        file.write(begin % ((len(methods) + 1) * "c"))
      else:
        file.write(begin % (len(methods) * "c"))

      # Write the first line
      s = ""
      if key == "time (with result)":
        s += " & Result"
      for m in methods:
        s += " & %s" % m
      s += "\\\\\n\\midrule\n"
      file.write(s)

      # Write the results for every graph
      for graphname in self.results:
        s = graphname
        # Write the result column if necessary
        if key == "time (with result)":
          for m in methods:
            if m in self.results[graphname]:
              result = self.results[graphname][m][1]
              break
          s += " & %s" % result

        # Write the rest of the columns
        for m in methods:
          if m in self.results[graphname].keys():
            if key == "result":
              s += " & %s" % self.results[graphname][m][1]
            else:
              s += " & %.4f" % self.results[graphname][m][0]
          else:
            s += " & "
        s += "\\\\\n"
        file.write(s)

      # Write the bottom of the table
      file.write(end)


def generateTestFile(database, outputfile, module, mth, function):
  def graphtograph6konverter(st):
      '''Gives the graph6-string'''
      vertexset = set([])
      edgeset = set([])
      st = str(st, encoding='utf8')
      if st.startswith('>>graph6<<'):
        return st[10:]
      else:
        s = st.split(';')
        for t in s:
          if len(t) != 0:
            v, edges = t.split(':', 1)
            vertexset.add(int(v))
            edges = edges.split(',')
            for e in edges:
              if len(e) != 0:
                edgeset.add((int(v), int(e)))
        g = SGraph(V=vertexset, E=edgeset)
        return graphtograph6(g)

  if (len(database) == 0 or
          len(outputfile) == 0):
    return False

  testSetFilename = outputfile

  # Read the graphnames
  graphnames = getGraphNamesdb(database)

  # Create the xml-file
  tests = dom.DOMImplementation().createDocument(None, "TestCase", None)
  root = tests.documentElement
  root.setAttribute("Module", module)
  root.setAttribute("Method", mth)
  root.setAttribute("Function", function)

  # Load the graph6-strings from the file
  connection = connect(database,
                       detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()
  register_converter("SGraph", graphtograph6konverter)

  for i, name in enumerate(graphnames[1:]):
    # Load the graph
    cursor.execute("SELECT graph FROM Graphen WHERE name='%s'" % name)
    graph6 = cursor.fetchall()[0]

    # Create the test in the xml-file
    if graph6 is not None:
      test = tests.createElement("Test" + str(i + 1))
      test.setAttribute("graph", name)
      test.setAttribute("graph6", graph6[0])

      root.appendChild(test)

  # Write the xml-file
  with open(testSetFilename, "w") as f:
    tests.writexml(f, "", "\t", "\n")

  return True


# =====================================================================
# =====================================================================
# =====================================================================
if __name__ == '__main__':
    pass
