#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.


Created on 05.11.2017

@author: Markus Dod
@copyright: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from modules.consts import method # @UnresolvedImport

from SGraph.GraphFunctions import alpha as calcAlpha  # @UnresolvedImport
from SGraph.GraphFunctions import omega as calcOmega  # @UnresolvedImport
from SGraph.GraphFunctions import find_all_cliques as findCliques  # @UnresolvedImport
from SGraph.GraphFunctions import pagerank as calcPageRank  # @UnresolvedImport
from SGraph.GraphFunctions import longestIsometricCycle as calcLongestIsometricCycle  # @UnresolvedImport
from SGraph.GraphFunctions import adjacencySpectrum as calcAdjacencySpectrum  # @UnresolvedImport
from SGraph.GraphFunctions import laplacianSpectrum as calcLaplacianSpectrum  # @UnresolvedImport
from SGraph.GraphFunctions import betweennessCentrality  # @UnresolvedImport
from SGraph.GraphFunctions import closenessCentrality  # @UnresolvedImport
from SGraph.GraphFunctions import distanceMatrix  # @UnresolvedImport
from SGraph.GraphFunctions import edgeconnectivity  # @UnresolvedImport
from SGraph.GraphFunctions import stressCentrality  # @UnresolvedImport
from SGraph.GraphFunctions import connectivity  # @UnresolvedImport

from lib.AlliancePoly import compute_poly as calcAlliancePoly  # @UnresolvedImport
from lib.GeneralDominationPoly import compute_poly as compute_gendompoly  # @UnresolvedImport
from lib.BipartitionPoly import compute_poly as compute_poly  # @UnresolvedImport
from lib.ChromaticPoly import compute_poly as computeChromaticPoly  # @UnresolvedImport
from lib.DominationReliabilityDRel import compute_reliability as compute_reliability_drel  # @UnresolvedImport
from lib.DominationReliabilityDRel import compute_poly as compute_D  # @UnresolvedImport
from lib.IndDominationPoly import computeRelPoly as compute_reliability_idrel  # @UnresolvedImport
from lib.TotalDominationPoly import compute_poly as compute_total_domination  # @UnresolvedImport
from lib.TotalDominationPoly import compute_reliability as compute_total_reldomination  # @UnresolvedImport
from lib.TriTotalDominationPoly import compute_poly as compute_tri_total_domination  # @UnresolvedImport
from lib.ConnectedDominationPoly import compute_poly as compute_connected_domination  # @UnresolvedImport
from lib.IndDominationPoly import compute_poly as compute_independent_domination  # @UnresolvedImport
from lib.PerfectDominationPoly import compute_poly as compute_perfect_domination  # @UnresolvedImport
from lib.DominationReliabilityVEDRel import compute_reliability as compute_reliability_vedrel  # @UnresolvedImport

from lib.MatchingPoly import computeMatching, computeExtendedMatching  # @UnresolvedImport
from lib.IndPoly import compute_poly as computeIndependencePoly  # @UnresolvedImport
from lib.GraphPoly import SubgraphPoly as computeSubgraphPoly  # @UnresolvedImport
from lib.GraphPoly import SubgraphEnumeratingPoly as computeSubgraphEnumPoly  # @UnresolvedImport
from lib.GraphPoly import ExtendedSubgraphCountingPoly as computeExtendedSubgraphCountingPoly  # @UnresolvedImport
from lib.GraphPoly import GeneralSubgraphCountingPoly as computeGeneralSubgraphCountingPoly  # @UnresolvedImport
from lib.GraphPoly import SubgraphComponentPoly as computeSubgraphCompPoly  # @UnresolvedImport
from lib.GraphPoly import Reliability as compute_Reliability  # @UnresolvedImport
from lib.GraphPoly import computeCutPoly as computeCutPoly  # @UnresolvedImport
from lib.JPoly import compute_poly as computeExtentedCutPolynomial  # @UnresolvedImport
from lib.GraphPoly import computeBipartiteSubgraphPoly as computeBipartiteSubgraphPoly  # @UnresolvedImport
from lib.EdgeCoverPoly import compute_poly as computeEdgeCoverPoly  # @UnresolvedImport
from lib.VertexCoverPoly import compute_poly as computeVertexCoverPoly  # @UnresolvedImport
from lib.IrredundancePoly import compute_poly as computeIrredundancePoly  # @UnresolvedImport
from lib.DominatedPartitionsPoly import calcPoly_d as calcDominatedPartitionsPoly_d  # @UnresolvedImport
from lib.RankPoly import compute_poly as computeRankPoly  # @UnresolvedImport
from lib.AllTerminalRelPoly import compute_poly as compute_allTerminal  # @UnresolvedImport
from lib.GracefulLabels import compute_labels as compute_gracefulLabels  # @UnresolvedImport

from lib.VertexColoring import compute_coloring as compute_coloring  # @UnresolvedImport
from lib.EdgeColoring import compute_coloring as compute_edge_coloring  # @UnresolvedImport
from lib.RainbowColoringPoly import calcPoly as computeRainbowPoly  # @UnresolvedImport
from lib.RainbowColoringPoly import calcGenFunction as computeGenFunction  # @UnresolvedImport


class Globals():
  def __init__(self):
    self.USERTYPE = 65000

    # default plot style
    self.PLOTSTYLE = 'twopi'

    # list with the different plot styles
    self.PLOTMODES = []

    # Default color to mark objects
    self.COLOR = "red"

    self.USE_GRAPHVIZ = True  # Use graphviz (must be installed, path in settings.ini)
    self.USE_MATPLOTLIB = True  # Use matplotlib
    self.USE_OGDF = False  # Use the ogdf-library
    self.USE_GRID = True  # Use a invisible grid for drawing

    self.STREAMWRITER = None  # Var for the writting stream

    # Variables for the load-dialog
    self.FNAME = None  # Global for the filename (load and save)
    self.GRAPHNAME = None  # Global for the graphname (load and save)
    self.SIMNAME = None  # Global for the simulation name (load and save)

    # Variables for the generation of product graphs
    self.FIRSTORDER = 0  # Order of the first graph in the product graph
    self.SECODORDER = 0  # Order of the second graph in the product graph

    # Implemented modules for testcases
    self.IMPLEMENTEDMODULES = ['lib.AlliancePoly',
                               'lib.AllTerminalRelPoly',
                               'lib.BipartitionPoly',
                               'lib.ChromaticPoly',
                               'lib.ConnectedDominationPoly',
                               'lib.DominatedPartitionsPoly_d',
                               'lib.DominationReliabilityDRel',
                               'lib.DominationReliabilityEDRel',
                               'lib.EdgeColoring',
                               'lib.EdgeCoverPoly',
                               'lib.GeneralDominationPoly',
                               'lib.GraphPoly.Reliability',
                               'lib.IndDomRelPoly',
                               'lib.IndDominationPoly',
                               'lib.IndPoly',
                               'lib.JPoly',
                               'lib.PerfektDominationPoly',
                               'lib.RainbowGenFunction',
                               'lib.RainbowPoly',
                               'lib.RankPoly',
                               'lib.TotalDominationPoly',
                               'lib.TriTotalDominationPoly',
                               'lib.VertexColoring',
                               'lib.VertexCoverPoly',
                               'lib.GracefulLabels']

    # Implemented methods in modules
    # Add in this dict new methods for the calculation
    self.METHODSFORMODULES = {'lib.AlliancePoly': [method.AUTOMATIC,
                                                   method.STATE_SPACE],
                              "lib.AllTerminalRelPoly": [method.AUTOMATIC,
                                                         method.STATE_SPACE],
                              "lib.BipartitionPoly": [method.AUTOMATIC,
                                                      method.STATE_SPACE,
                                                      method.PATH_DECOMP,
                                                      method.TREE,
                                                      method.SKPATH,
                                                      method.KPATH],
                              "lib.ChromaticPoly": [method.DECOMP,
                                                    method.TUTTE],
                              "lib.ConnectedDominationPoly": [method.AUTOMATIC,
                                                              method.STATE_SPACE,
                                                              method.PATH_DECOMP,
                                                              method.DECOMP,
                                                              method.SKPATH],
                              "lib.DominatedPartitionsPoly": [method.AUTOMATIC,
                                                              method.STATE_SPACE],
                              "lib.DominationReliabilityDRel": [method.AUTOMATIC,
                                                                method.STATE_SPACE,
                                                                method.DECOMP,
                                                                method.KTREE,
                                                                method.SKPATH,
                                                                method.SKCYCLE,
                                                                method.ESSENTIAL],
                              "lib.DominationReliabilityEDRel": [method.STATE_SPACE,
                                                                 method.DECOMP],
                              "lib.EdgeColoring": [method.AUTOMATIC,
                                                   method.GREEDY,
                                                   method.DSATUR],
                              "lib.EdgeCoverPoly": [method.AUTOMATIC,
                                                    method.STATE_SPACE,
                                                    method.DECOMP],
                              "lib.ExtMatching": [method.AUTOMATIC,
                                                  method.STATE_SPACE],
                              "lib.GeneralDominationPoly": [method.AUTOMATIC,
                                                            method.STATE_SPACE],
                              "lib.GraphPoly.Reliability": [method.AUTOMATIC,
                                                            method.STATE_SPACE],
                              "lib.IndDomRelPoly": [method.AUTOMATIC,
                                                    method.STATE_SPACE,
                                                    method.DECOMP,
                                                    method.TREE,
                                                    method.ESSENTIAL,
                                                    method.SIM],
                              "lib.IndDominationPoly": [method.AUTOMATIC,
                                                        method.STATE_SPACE,
                                                        method.DECOMP,
                                                        method.TREE,
                                                        method.ESSENTIAL],
                              "lib.IndPoly": [method.AUTOMATIC,
                                              method.STATE_SPACE,
                                              method.DECOMP],
                              "lib.JPoly": [method.AUTOMATIC,
                                            method.STATE_SPACE,
                                            method.DECOMP],
                              "lib.Matching": [method.AUTOMATIC,
                                               method.STATE_SPACE],
                              "lib.PerfectDominationPoly": [method.AUTOMATIC,
                                                            method.STATE_SPACE],
                              "lib.RainbowGenFunction": [method.AUTOMATIC,
                                                         method.STATE_SPACE],
                              "lib.RainbowPoly": [method.AUTOMATIC,
                                                  method.STATE_SPACE],
                              "lib.RankPoly": [method.AUTOMATIC,
                                               method.STATE_SPACE],
                              "lib.TotalDominationPoly": [method.AUTOMATIC,
                                                          method.STATE_SPACE,
                                                          method.INCLEXCL,
                                                          method.ESSENTIAL,
                                                          method.DECOMP],
                              "lib.TriTotalDominationPoly": [method.AUTOMATIC,
                                                             method.STATE_SPACE,
                                                             method.PATH_DECOMP,
                                                             method.TREE,
                                                             method.PATH],
                              "lib.VertexColoring": [method.AUTOMATIC,
                                                     method.GREEDY,
                                                     method.DSATUR],
                              "lib.VertexCoverPoly": [method.AUTOMATIC,
                                                      method.STATE_SPACE,
                                                      method.DECOMP]}

    # Dict with methods which called after the slot-signal
    self.STARTMETHODS = {"AdjacencySpectrum": calcAdjacencySpectrum,
                         "AlliancePoly": calcAlliancePoly,
                         "AllTerminalRelPoly": compute_allTerminal,
                         "BetweennessCentrality": betweennessCentrality,
                         "BipartiteSubgraphPoly": computeBipartiteSubgraphPoly,
                         "BipartitionPoly": compute_poly,
                         "ChromaticPoly": computeChromaticPoly,
                         "CliqueNumber": calcOmega,
                         "ClosenessCentrality": closenessCentrality,
                         "ConnectedDomPoly": compute_connected_domination,
                         "CutPoly": computeCutPoly,
                         "DistanceMatrix": distanceMatrix,
                         "DominatedPartitionsPoly_d": calcDominatedPartitionsPoly_d,
                         "DominationPoly": compute_D,
                         "DomRelPoly": compute_reliability_drel,
                         "EdgeColoring": compute_edge_coloring,
                         "EdgeConnectivity": edgeconnectivity,
                         "EdgeCoverPoly": computeEdgeCoverPoly,
                         "ExtendedSubgraphCountingPoly": computeExtendedSubgraphCountingPoly,
                         "ExtCutPoly": computeExtentedCutPolynomial,
                         "ExtMatchingPoly": computeExtendedMatching,
                         "GeneralDominationPoly": compute_gendompoly,
                         "GeneralSubgraphCountingPoly": computeGeneralSubgraphCountingPoly,
                         "GracefulLabels": compute_gracefulLabels,
                         "IndDomPoly": compute_independent_domination,
                         "IndDomRelPoly": compute_reliability_idrel,
                         "IndependenceNumber": calcAlpha,
                         "IndependencePoly": computeIndependencePoly,
                         "IrredundancePoly": computeIrredundancePoly,
                         "LaplacianSpectrum": calcLaplacianSpectrum,
                         "LongestIsometricCycle": calcLongestIsometricCycle,
                         "MatchingPoly": computeMatching,
                         "MaxClique": findCliques,
                         "PerfectDomPoly": compute_perfect_domination,
                         "PageRank": calcPageRank,
                         "RainbowGenFunction": computeGenFunction,
                         "RainbowPoly": computeRainbowPoly,
                         "RankPoly": computeRankPoly,
                         "Reliability": compute_Reliability,
                         "StressCentrality": stressCentrality,
                         "SubgraphComponentPoly": computeSubgraphCompPoly,
                         "SubgraphEnumeratingPoly": computeSubgraphEnumPoly,
                         "SubgraphPoly": computeSubgraphPoly,
                         "TotalDomPoly": compute_total_domination,
                         "TotalDomRelPoly": compute_total_reldomination,
                         "TriTotalDomPoly": compute_tri_total_domination,
                         "VEDRel": compute_reliability_vedrel,
                         "VertexColoring": compute_coloring,
                         "VertexConnectivity": connectivity,
                         "VertexCoverPoly": computeVertexCoverPoly}

  def __get_usertype(self):
    return self.USERTYPE

  def __get_plotstyle(self):
    return self.PLOTSTYLE

  def __get_plotmodes(self):
    return self.PLOTMODES

  def __get_color(self):
    return self.COLOR

  def __get_use_graphviz(self):
    return self.USE_GRAPHVIZ

  def __get_use_matplotlib(self):
    return self.USE_MATPLOTLIB

  def __get_use_ogdf(self):
    return self.USE_OGDF

  def __get_use_grid(self):
    return self.USE_GRID

  def __get_streamwriter(self):
    return self.STREAMWRITER

  def __get_fname(self):
    return self.FNAME

  def __get_graphname(self):
    return self.GRAPHNAME

  def __get_simname(self):
    return self.SIMNAME

  def __get_firstorder(self):
    return self.FIRSTORDER

  def __get_secodorder(self):
    return self.SECODORDER

  def __get_implementedmodules(self):
    return self.IMPLEMENTEDMODULES

  def __get_methodsformodules(self):
    return self.METHODSFORMODULES

  def __get_startmethods(self):
    return self.STARTMETHODS

  def __set_usertype(self, value):
    self.USERTYPE = value

  def __set_plotstyle(self, value):
    self.PLOTSTYLE = value

  def __set_plotmodes(self, value):
    self.PLOTMODES = value

  def __set_color(self, value):
    self.COLOR = value

  def __set_use_graphviz(self, value):
    self.USE_GRAPHVIZ = value

  def __set_use_matplotlib(self, value):
    self.USE_MATPLOTLIB = value

  def __set_use_ogdf(self, value):
    self.USE_OGDF = value

  def __set_use_grid(self, value):
    self.USE_GRID = value

  def __set_streamwriter(self, value):
    self.STREAMWRITER = value

  def __set_fname(self, value):
    self.FNAME = value

  def __set_graphname(self, value):
    self.GRAPHNAME = value

  def __set_simname(self, value):
    self.SIMNAME = value

  def __set_firstorder(self, value):
    self.FIRSTORDER = value

  def __set_secodorder(self, value):
    self.SECODORDER = value

  def __set_implementedmodules(self, value):
    self.IMPLEMENTEDMODULES = value

  def __set_methodsformodules(self, value):
    self.METHODSFORMODULES = value

  def __set_startmethods(self, value):
    self.STARTMETHODS = value
