'''
Custom logging classes.
It is possible to write a log on screen or in a file.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 07.01.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

try:
  from PyQt5 import QtCore
  qtobject = QtCore.QObject
except:
  qtobject = object
  QtCore = None
from logging import StreamHandler, Formatter, getLogger, ERROR, FileHandler, INFO,\
                    DEBUG, WARNING, CRITICAL
from sys import stdout


# ============================================================================
class EmittingStream(qtobject):
  '''
  Class for the output of the testStatus-dialog
  '''
  textStream = QtCore.pyqtSignal(str)

  def write(self, text, end="\n"):
    self.textStream.emit(str(text + end))

  def flush(self):
    pass


# ============================================================================
class StreamToLogger:
  '''
  Class to redirect streams to stderr or stdout or ...
  '''
  def __init__(self, logger, level):
    self.logger = logger
    self.level = level

  def write(self, message):
    if message != '\n':
      self.logger.log(self.level, message)
    
  def flush(self):
    self.logger.flush()


# ============================================================================
class PyGraphEditLog(object):
  '''
  Logging class to write the output in the console or in a logging file.
  '''
  def __init__(self, mode="stream", filename=None):
    '''
    @param mode: "stream" for output in the console and "file" for logging in a file
    @param filename: Name of the logging file
    '''
    self.mode = mode
    if self.mode == "file":
      if filename != None:
        handler = FileHandler(filename, mode='w')
      else:  
        handler = FileHandler("PyGraphEdit.log", mode='w')
        
    elif self.mode == "stream":
      handler = StreamHandler(stdout)
    
    self.logger = getLogger("MDNetwork_log")
    frm = Formatter("PyGraphEdit %(asctime)s %(levelname)s: %(message)s","%H:%M:%S") 
    handler.setFormatter(frm)
    
    self.logger.addHandler(handler)
    self.logger.setLevel(ERROR)
    
  def changeFile(self, filename):
    '''
    Change the logging file
    @param filename: New filename
    '''
    if self.mode == "file":
      handler = FileHandler(filename, mode='w')
      self.logger = getLogger("MDNetwork_log")
      frm = Formatter("PyGraphEdit %(asctime)s %(levelname)s: %(message)s","%H:%M:%S") 
      handler.setFormatter(frm)
      self.logger.addHandler(handler)
      self.logger.setLevel(ERROR)
    
  def log(self, level, msg):
    '''
    Handle a message with a given level
    @param level: The log-level of the message
    @param msg: Message
    '''
    if level == INFO:
      self.info(msg)
    elif level == DEBUG:
      self.debug(msg)
    elif level == ERROR:
      self.error(msg)
    elif level == WARNING:
      self.warning(msg)
    elif level == CRITICAL:
      self.critical(msg)
  
  def flush(self):
    pass  
  
  def setLevel(self, level):
    '''
    Set the log-level
    @param level: Log-level
    '''
    self.logger.setLevel(level)
    
  def info(self, msg):
    '''
    Writes an info-message
    @param msg: Message
    '''
    self.logger.info(msg)
    
  def debug(self, msg):
    '''
    Writes an debug-message
    @param msg: Message
    '''
    self.logger.debug(msg)
  
  def error(self, msg):
    '''
    Writes an error-message
    @param msg: Message
    '''
    self.logger.error(msg)
  
  def critical(self, msg):
    '''
    Writes an critical-message
    @param msg: Message
    '''
    self.logger.critical(msg)
    
  def warning(self, msg):
    '''
    Writes an warning-message
    @param msg: Message
    '''
    self.logger.warning(msg)
