// UTM.c

// Source: http://alephnull.net/software/gis/UTM_WGS84_C_plus_plus.shtml
// Bug-Fixed by Veit Leonhardt (lve) 2017

// Original Javascript by Chuck Taylor
// Port to C++ by Alex Hajnal
// Port to Rust by Markus Dod
//
// This is a simple port of the code on the Geographic/UTM Coordinate Converter (1) page from Javascript to C++.
// Using this you can easily convert between UTM and WGS84 (latitude and longitude).
// Accuracy seems to be around 50cm (I suspect rounding errors are limiting precision).
// This code is provided as-is and has been minimally tested; enjoy but use at your own risk!
// The license for UTM.cpp and UTM.h is the same as the original Javascript:
// "The C++ source code in UTM.cpp and UTM.h may be copied and reused without restriction."
//
// 1) http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

use std::f64::consts::PI;
use std::convert::From;

/* Ellipsoid model consf64::tants (actual values here are for WGS84) */
const sm_a: f64 =  6378137.0;
const sm_b: f64 = 6356752.314;
const UTMScaleFactor: f64 = 0.9996;


// A struct that can be passed between C and Rust
#[repr(C)]
pub struct Tuple2 {
  x: f64,
  y: f64,
}

// Conversion functions
impl From<(f64, f64)> for Tuple2 {
  fn from(tup: (f64, f64)) -> Tuple2 {
    Tuple2 { x: tup.0, y: tup.1 }
  }
}

impl From<Tuple2> for (f64, f64) {
  fn from(tup: Tuple2) -> (f64, f64) {
    (tup.x, tup.y)
  }
}

#[repr(C)]
pub struct Tuple3 {
  x: f64,
  y: f64,
  z: i32,
}

// Conversion functions
impl From<(f64, f64, i32)> for Tuple3 {
  fn from(tup: (f64, f64, i32)) -> Tuple3 {
    Tuple3 { x: tup.0, y: tup.1, z: tup.2 }
  }
}

impl From<Tuple3> for (f64, f64, i32) {
  fn from(tup: Tuple3) -> (f64, f64, i32) {
    (tup.x, tup.y, tup.z)
  }
}

fn DegToRad(deg: f64) -> f64 {
  deg / 180.0 * PI
}

fn RadToDeg(rad: f64) -> f64 {
  rad / PI * 180.0
}

// ArcLengthOfMeridian
// Computes the ellipsoidal distance from the equator to a point at a
// given latitude.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
// GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//     phi - Latitude of the point, in radians.
//
// Globals:
//     sm_a - Ellipsoid model major axis.
//     sm_b - Ellipsoid model minor axis.
//
// Returns:
//     The ellipsoidal disf64::tance of the point from the equator, in meters.
#[no_mangle]
pub extern fn ArcLengthOfMeridian (phi: f64) -> f64 {
  /* Precalculate n */
  let n = (sm_a - sm_b) / (sm_a + sm_b);

  /* Precalculate alpha */
  let alpha = ((sm_a + sm_b) / 2.0)
        * (1.0 + (f64::powi(n, 2) / 4.0) + (f64::powi(n, 4) / 64.0));

  /* Precalculate beta */
  let beta = (-3.0 * n / 2.0) + (9.0 * f64::powi(n, 3) / 16.0)
       + (-3.0 * f64::powi(n, 5) / 32.0);

  /* Precalculate gamma */
  let gamma = (15.0 * f64::powi(n, 2) / 16.0)
        + (-15.0 * f64::powi(n, 4) / 32.0);

  /* Precalculate delta */
  let delta = (-35.0 * f64::powi(n, 3) / 48.0)
      + (105.0 * f64::powi(n, 5) / 256.0);

  /* Precalculate epsilon */
  let epsilon = 315.0 * f64::powi(n, 4) / 512.0;

  /* Now calculate the sum of the series and return */
  alpha * (phi + (beta * f64::sin(2.0 * phi))
         + (gamma * f64::sin(4.0 * phi))
         + (delta * f64::sin(6.0 * phi))
         + (epsilon * f64::sin(8.0 * phi)))
}



// UTMCentralMeridian
// Determines the central meridian for the given UTM zone.
//
// Inputs:
//     zone - An integer value designating the UTM zone, range [1,60].
//
// Returns:
//   The central meridian for the given UTM zone, in radians
//   Range of the central meridian is the radian equivalent of [-177,+177].
#[no_mangle]
pub extern fn UTMCentralMeridian(zone: i32) -> f64 {
  DegToRad(-183.0 + (zone as f64 * 6.0))
}


// FootpointLatitude
//
// Computes the footpoint latitude for use in converting transverse
// Mercator coordinates to ellipsoidal coordinates.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
//   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//   y - The UTM northing coordinate, in meters.
//
// Returns:
//   The footpoint latitude, in radians.
fn FootpointLatitude(y: f64)-> f64 {
  /* Precalculate n (Eq. 10.18) */
  let n = (sm_a - sm_b) / (sm_a + sm_b);

  /* Precalculate alpha_ (Eq. 10.22) */
  /* (Same as alpha in Eq. 10.17) */
  let alpha_ = ((sm_a + sm_b) / 2.0)
         * (1.0 + (f64::powi(n, 2) / 4.0) + (f64::powi(n, 4) / 64.0));

  /* Precalculate y_ (Eq. 10.23) */
  let y_ = y / alpha_;

  /* Precalculate beta_ (Eq. 10.22) */
  let beta_ = (3.0 * n / 2.0) + (-27.0 * f64::powi(n, 3) / 32.0)
        + (269.0 * f64::powi(n, 5) / 512.0);

  /* Precalculate gamma_ (Eq. 10.22) */
  let gamma_ = (21.0 * f64::powi(n, 2) / 16.0)
         + (-55.0 * f64::powi(n, 4) / 32.0);

  /* Precalculate delta_ (Eq. 10.22) */
  let delta_ = (151.0 * f64::powi(n, 3) / 96.0)
         + (-417.0 * f64::powi(n, 5) / 128.0);

  /* Precalculate epsilon_ (Eq. 10.22) */
  let epsilon_ = 1097.0 * f64::powi(n, 4) / 512.0;

  /* Now calculate the sum of the series (Eq. 10.21) */
  y_ + (beta_ * f64::sin(2.0 * y_))
         + (gamma_ * f64::sin(4.0 * y_))
         + (delta_ * f64::sin(6.0 * y_))
         + (epsilon_ * f64::sin(8.0 * y_))
}



// MapLatLonToXY
// Converts a latitude/longitude pair to x and y coordinates in the
// Transverse Mercator projection.  Note that Transverse Mercator is not
// the same as UTM; a scale factor is required to convert between them.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
// GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//    phi - Latitude of the point, in radians.
//    lambda - Longitude of the point, in radians.
//    lambda0 - Longitude of the central meridian to be used, in radians.
//
// Outputs:
//    x - The x coordinate of the computed point.
//    y - The y coordinate of the computed point.
//
// Returns:
//    Tuple2(x,y)
#[no_mangle]
pub extern fn MapLatLonToXY (phi: f64, lambda: f64, lambda0: f64) -> Tuple2 {
    /* Precalculate ep2 */
    let ep2 = (f64::powi(sm_a, 2) - f64::powi(sm_b, 2)) / f64::powi(sm_b, 2);

    /* Precalculate nu2 */
    let nu2 = ep2 * f64::powi(f64::cos(phi), 2);

    /* Precalculate N */
    let N = f64::powi(sm_a, 2) / (sm_b * f64::sqrt(1.0 + nu2));

    /* Precalculate t */
    let t = f64::tan(phi);
    let t2 = t * t;
    let t4 = t2 * t2;
    let t6 = t4 * t2 ;

    /* Precalculate l */
    let l = lambda - lambda0;

    /* Precalculate coefficients for l**n in the equations below
       so a normal human being can read the expressions for easting
       and northing
       -- l**1 and l**2 have coefficients of 1.0 */
    let l3coef = 1.0 - t2 + nu2;

    let l4coef = 5.0 - t2 + 9.0 * nu2 + 4.0 * (nu2 * nu2);

    let l5coef = 5.0 - 18.0 * t2 + t4 + 14.0 * nu2
        - 58.0 * t2 * nu2;

    let l6coef = 61.0 - 58.0 * t2 + t4 + 270.0 * nu2
        - 330.0 * t2 * nu2;

    let l7coef = 61.0 - 479.0 * t2 + 179.0 * t4 - t6;

    let l8coef = 1385.0 - 3111.0 * t2 + 543.0 * t4 - t6;

    /* Calculate easting (x) */
    /*
    x = N * f64::f64::cos(phi) * l
        + (N / 6.0 * f64::powi(f64::cos(phi), 3.0) * l3coef * f64::powi(l, 3.0))
        + (N / 120.0 * f64::powi(f64::cos(phi), 5.0) * l5coef * f64::powi(l, 5.0))
        + (N / 5040.0 * f64::powi(f64::cos(phi), 7.0) * l7coef * f64::powi(l, 7.0));
    */
    // lve: -> Reordered factors to avoid x become inf because of factors as f64::powi(, 7.0) exceeding the numerical limits
    let cosPhiL = f64::cos(phi) * l;
    let cosPhi2L2 = cosPhiL * cosPhiL;

    let x = ((((((N / 5040.0 * l7coef * cosPhi2L2) +
	         (N / 120.0 * l5coef)) * cosPhi2L2) +
	         (N / 6.0 * l3coef)) * cosPhi2L2) +
	         (N)) * cosPhiL;

    /* Calculate northing (y) */
    /*
    y = ArcLengthOfMeridian (phi)
        + (t / 2.0 * N * f64::powi(f64::cos(phi), 2.0) * f64::powi(l, 2.0))
        + (t / 24.0 * N * f64::powi(f64::cos(phi), 4.0) * l4coef * f64::powi(l, 4.0))
        + (t / 720.0 * N * f64::powi(f64::cos(phi), 6.0) * l6coef * f64::powi(l, 6.0))
        + (t / 40320.0 * N * f64::powi(f64::cos(phi), 8.0) * l8coef * f64::powi(l, 8.0));
    */
    // lve: -> Reordered factors to avoid y become inf because of factors as f64::powi(, 8.0) exceeding the numerical limits
    let y = (((((((N * t / 40320.0 * l8coef * cosPhi2L2) +
	          (N * t / 720.0 * l6coef)) * cosPhi2L2) +
	          (N * t / 24.0 * l4coef)) * cosPhi2L2) +
	          (N * t / 2.0)) * cosPhi2L2) + ArcLengthOfMeridian(phi);

    (x, y).into()
}



// MapXYToLatLon
// Converts x and y coordinates in the Transverse Mercator projection to
// a latitude/longitude pair.  Note that Transverse Mercator is not
// the same as UTM; a scale factor is required to convert between them.
//
// Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
//   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
//
// Inputs:
//   x - The easting of the point, in meters.
//   y - The northing of the point, in meters.
//   lambda0 - Longitude of the central meridian to be used, in radians.
//
// Outputs:
//   phi    - Latitude in radians.
//   lambda - Longitude in radians.
//
// Returns:
//   The function does not return a value.
//
// Remarks:
//   The local variables Nf, nuf2, tf, and tf2 serve the same purpose as
//   N, nu2, t, and t2 in MapLatLonToXY, but they are computed with respect
//   to the footpoint latitude phif.
//
//   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
//   to optimize computations.
#[no_mangle]
pub extern fn MapXYToLatLon (x: f64, y: f64, lambda0: f64) -> Tuple2
{
  /* Get the value of phif, the footpoint latitude. */
  let phif = FootpointLatitude (y);

  /* Precalculate ep2 */
  let ep2 = (f64::powi(sm_a, 2) - f64::powi(sm_b, 2)) / f64::powi(sm_b, 2);

  /* Precalculate cos (phif) */
  let cf = f64::cos(phif);

  /* Precalculate nuf2 */
  let nuf2 = ep2 * f64::powi(cf, 2);

  /* Precalculate Nf and initialize Nfpow */
  let Nf = f64::powi(sm_a, 2) / (sm_b * f64::sqrt(1.0 + nuf2));
  let mut Nfpow = Nf;

  /* Precalculate tf */
  let tf = f64::tan(phif);
  let tf2 = tf * tf;
  let tf4 = tf2 * tf2;

  /* Precalculate fractional coefficients for x**n in the equations
     below to simplify the expressions for latitude and longitude. */
  let x1frac = 1.0 / (Nfpow * cf);

  Nfpow *= Nf;   /* now equals Nf**2) */
  let x2frac = tf / (2.0 * Nfpow);

  Nfpow *= Nf;   /* now equals Nf**3) */
  let x3frac = 1.0 / (6.0 * Nfpow * cf);

  Nfpow *= Nf;   /* now equals Nf**4) */
  let x4frac = tf / (24.0 * Nfpow);

  Nfpow *= Nf;   /* now equals Nf**5) */
  let x5frac = 1.0 / (120.0 * Nfpow * cf);

  Nfpow *= Nf;   /* now equals Nf**6) */
  let x6frac = tf / (720.0 * Nfpow);

  Nfpow *= Nf;   /* now equals Nf**7) */
  let x7frac = 1.0 / (5040.0 * Nfpow * cf);

  Nfpow *= Nf;   /* now equals Nf**8) */
  let x8frac = tf / (40320.0 * Nfpow);

  /* Precalculate polynomial coefficients for x**n.
     -- x**1 does not have a polynomial coefficient. */
  let x2poly = -1.0 - nuf2;

  let x3poly = -1.0 - 2.0 * tf2 - nuf2;

  let x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2
         - 3.0 * (nuf2 * nuf2) - 9.0 * tf2 * (nuf2 * nuf2);

  let x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;

  let x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2
         + 162.0 * tf2 * nuf2;

  let x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);

  let x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575.0 * (tf4 * tf2);

  /* Calculate latitude */
  /*
  phi = phif + x2frac * x2poly * (x * x)
      + x4frac * x4poly * f64::powi(x, 4.0)
      + x6frac * x6poly * f64::powi(x, 6.0)
      + x8frac * x8poly * f64::powi(x, 8.0);
  */
  // lve: -> Reordered factors to avoid phi become inf because of factors as f64::powi(, 8.0) exceeding the numerical limits
  let powX2: f64 = x * x;
  let phi = phif + ((x2frac * x2poly + ((x4frac * x4poly + ((x6frac * x6poly + (x8frac * x8poly * powX2)) * powX2)) * powX2)) * powX2);

  /* Calculate longitude */
  /*
  lambda = lambda0 + x1frac * x
         + x3frac * x3poly * f64::powi(x, 3.0)
         + x5frac * x5poly * f64::powi(x, 5.0)
         + x7frac * x7poly * f64::powi(x, 7.0);
  */
  // lve: -> Reordered factors to avoid lambda become inf because of factors as f64::powi(, 7.0) exceeding the numerical limits
  let lambda = lambda0 + (x1frac + ((x3frac * x3poly + ((x5frac * x5poly + (x7frac * x7poly * powX2)) * powX2)) * powX2)) * x;

  (phi, lambda).into()
}


// LatLonToUTMXY
// Converts a latitude/longitude pair to x and y coordinates in the
// Universal Transverse Mercator projection.
//
// Inputs:
//   lat - Latitude of the point, in degrees.
//   lon - Longitude of the point, in degrees.
//   zone - UTM zone to be used for calculating values for x and y.
//          If zone is less than 1 or greater than 60, the routine
//          will determine the appropriate zone from the value of lon.
//
// Outputs:
//   x - The x coordinate (easting) of the computed point. (in meters)
//   y - The y coordinate (northing) of the computed point. (in meters)
//
// Returns:
//   The UTM zone used for calculating the values of x and y.
#[no_mangle]
pub extern fn LatLonToUTMXY (lat: f64, lon: f64, mut zone: i32) -> Tuple3 {
    if (zone < 1) || (zone > 60)  {
        zone = f64::floor((lon + 180.0) / 6.0) as i32 + 1;
    }

  let xy = MapLatLonToXY (DegToRad(lat), DegToRad(lon), UTMCentralMeridian(zone));
  let mut x = xy.x;
  let mut y = xy.y;

  /* Adjust easting and northing for UTM system. */
  x = x * UTMScaleFactor + 500000.0;
  y = y * UTMScaleFactor;
  if y < 0.0 {
    y = y + 10000000.0;
}

  (x, y, zone).into()
}

// UTMXYToLatLon
//
// Converts x and y coordinates in the Universal Transverse Mercator
// projection to a latitude/longitude pair.
//
// Inputs:
// x - The easting of the point, in meters.
// y - The northing of the point, in meters.
// zone - The UTM zone in which the point lies.
// southhemi - True if the point is in the southern hemisphere;
//               false otherwise.
//
// Outputs:
// lat - The latitude of the point, in degrees.
// lon - The longitude of the point, in degrees.
//
// Returns:
// The function does not return a value.
#[no_mangle]
pub extern fn UTMXYToLatLon (x: f64, y: f64, zone: i32, southhemi: bool) -> Tuple2 {
  let cmeridian: f64;

  let mut x = x - 500000.0;
  x /= UTMScaleFactor;

  /* If in southern hemisphere, adjust y accordingly. */
  let mut y = y;
  if southhemi {
    y -= 10000000.0;
  }

  y /= UTMScaleFactor;

  cmeridian = UTMCentralMeridian (zone);
  let lonlat = MapXYToLatLon (x, y, cmeridian);
  let lon = lonlat.x;
  let lat = lonlat.y;

  (RadToDeg(lat), RadToDeg(lon)).into()
}

