#ifndef LIBUTM_H
#define LIBUTM_H

typedef struct {
	double x;
	double y;
} tupel2_t;

typedef struct {
	double x;
	double y;
	int z;
} tupel3_t;

extern "C" double ArcLengthOfMeridian(double);
extern "C" tupel2_t MapLatLonToXY (double, double, double);
extern "C" tupel2_t MapXYToLatLon (double, double, double);
extern "C" tupel3_t LatLonToUTMXY (double, double, int);
extern "C" tupel2_t UTMXYToLatLon (double, double, int, bool);

#endif