#![allow(non_snake_case)]

extern crate utm;

macro_rules! assert_delta {
    ($x:expr, $y:expr, $d:expr) => {
        if ($x - $y).abs() > $d { panic!(); }
    }
}

#[cfg(test)]
mod tests {
    use utm::LatLonToUTMXY;
    use utm::Tuple3;

    #[test]
    fn test_LatLonToUTMXY1() {
      let xyz: Tuple3 = LatLonToUTMXY(12.703781, 50.782138, 0);
      //let z: i32 = getz(xyz);
      let (x,y,z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 476346.9907915019, 0.0001);
      assert_delta!(y, 1404388.2379187196, 0.0001);
      assert_eq!(z, 39);
    }

    #[test]
    fn test_LatLonToUTMXY2() {
      let xyz: Tuple3 = LatLonToUTMXY(13.764169, 51.121019, 0);
      //let z: i32 = getz(xyz);
      let (x,y,z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 513082.1618285275, 0.0001);
      assert_delta!(y, 1521648.107705408, 0.0001);
      assert_eq!(z, 39);
    }

    #[test]
    fn test_LatLonToUTMXY3() {
      let xyz: Tuple3 = LatLonToUTMXY(13.762377, 51.120056, 0);
      //let z: i32 = getz(xyz);
      let (x,y,z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 512978.16012554744, 0.0001);
      assert_delta!(y, 1521449.872737395, 0.0001);
      assert_eq!(z, 39);
    }

    #[test]
    fn test_LatLonToUTMXY4() {
      let xyz: Tuple3 = LatLonToUTMXY(13.758987, 51.118426, 0);
      //let z: i32 = getz(xyz);
      let (x,y,z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 512802.13977980416, 0.0001);
      assert_delta!(y, 1521074.8749394342, 0.0001);
      assert_eq!(z, 39);
    }

    #[test]
    fn test_LatLonToUTMXY5() {
      let xyz: Tuple3 = LatLonToUTMXY(70.57927709, 45.59941973, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 522244.0435984505, 0.001);
      assert_delta!(y, 7830584.451357924, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY6() {
      let xyz: Tuple3 = LatLonToUTMXY(10.01889371, 23.31332382, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 753580.4094390129, 0.001);
      assert_delta!(y, 1108392.4055633575, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY7() {
      let xyz: Tuple3 = LatLonToUTMXY(19.47989559, 75.66204923, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 569479.6323869015, 0.001);
      assert_delta!(y, 2154062.092325577, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY8() {
      let xyz: Tuple3 = LatLonToUTMXY(21.07246482, 29.82868439, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 793928.8250756521, 0.001);
      assert_delta!(y, 2332777.768978597, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY9() {
      let xyz: Tuple3 = LatLonToUTMXY(5.458957393, 36.38523737, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 210253.78416523454, 0.001);
      assert_delta!(y, 604026.4102635789, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY10() {
      let xyz: Tuple3 = LatLonToUTMXY(70.1754537, 22.86535023, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 570594.2505159728, 0.001);
      assert_delta!(y, 7786520.634460997, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY11() {
      let xyz: Tuple3 = LatLonToUTMXY(61.96560497, 58.93137085, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 601263.2841014254, 0.001);
      assert_delta!(y, 6871855.195279498, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY12() {
      let xyz: Tuple3 = LatLonToUTMXY(11.11604988, 20.90106919, 0);
      //let z: i32 = getz(xyz);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 489196.6628083524, 0.001);
      assert_delta!(y, 1228812.9700428993, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY13() {
      let xyz: Tuple3 = LatLonToUTMXY(32.21054315, 60.70584911, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 283776.68562653894, 0.001);
      assert_delta!(y, 3566081.3701539743, 0.001);
    }

    #[test]
    fn test_LatLonToUTMXY14() {
      let xyz: Tuple3 = LatLonToUTMXY(79.187450900634, 61.532382486867, 0);
      let (x,y,_z): (f64, f64, i32) = xyz.into();
      assert_delta!(x, 469267.50105420774, 0.001);
      assert_delta!(y, 8791281.841439592, 0.001);
    }
}
