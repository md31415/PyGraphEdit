'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 03.11.2014

@author: Markus Dod
'''


# =====================================================================
# ========== ReturnObject =============================================
# =====================================================================
class MarkData(object):
  '''
  A class to manage the return informations to mark objects
  '''
  def __init__(self, data=None, returnType=None):
    if data:
      self.data = data
    else:
      self.data = {}
    self.returnType = returnType

  def setData(self, data):
    '''Set the data'''
    self.data = data

  def getData(self):
    '''Return the data'''
    return self.data

  def addRule(self, element, color="red"):
    '''Add a mark rule'''
    self.data[element] = color

  def setReturnType(self, returnType):
    '''Set the return type'''
    self.returnType = returnType

  def getReturnType(self):
    '''Return the return type'''
    return self.simType
  
  def __str__(self):
    return str(self.data)


# =====================================================================
# =====================================================================
# =====================================================================
if __name__ == '__main__':
  import sys.exit as sys_exit
  sys_exit("Use PyGraphEdit")
  