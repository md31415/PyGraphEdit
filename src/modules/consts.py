#!/usr/bin/python
# -*- coding: latin-1 -*-
'''
Consts for PyGraphEdit and DominationMain

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 02.01.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from PyQt5 import QtWidgets, QtCore  # @UnresolvedImport

from lib.tools import enum  # @UnresolvedImport


# ===============================================================================
# ===============================================================================
VERSION = 0.63

# ===============================================================================
# ===============================================================================
class time_stamp_str(enum):
  '''
  Enum for the time stamps
  '''
  START = ()
  BEGIN_PREPROCESSING = ()
  END_PREPROCESSING = ()
  START_COMPUTING = ()
  END_COMPUTING = ()

class whichpoly(enum):
  '''
  Enum for the polynomial to be calculated (only for DominationMain)
  '''
  B = ()  # Bipartition poly
  DREL = ()  # Domination reliabilty poly
  VEDREL = ()
  EDRELk = ()
  EDRELA = ()
  PEDREL = ()
  SPEDREL = ()
  M = ()  # Matching polynomial
  R = ()  # AllTerminal reliability
  SEP = ()  # Subgraph enumeration poly
  TD = ()  # Total domination poly
  TTD = ()  # Trivariate total domination
  T = ()  # Tutte poly
  CD = ()  # Connected domination poly
  A = ()  # Alliance poly
  ID = ()  # Independent domination poly
  GD = ()  # GeneralDomination poly

class method(enum):
  '''
  Enum for the calculation method
  '''
  AUTOMATIC = ()  # Choose automaticaly the calculation method
  STATE_SPACE = ()  # Complete Enumeration
  TREE_DECOMP = ()  # Tree-Decomposition
  PATH_DECOMP = ()  # Path-Decomposition
  CLIQUE_DECOMP = ()  # Clique-Decomposition
  KTREE = ()  # k-Trees
  SKPATH = ()  # simple k-Paths
  KPATH = ()  # k-Paths
  SKCYCLE = ()  # simple k-Cycles
  DECOMP = ()  # Decomposition
  TREE = ()  # Tree
  INCLEXCL = ()  # Inclusion-Exclusion
  ESSENTIAL = ()  # Essential subsets
  PATH = ()  # Path
  TUTTE = ()  # tutte
  SIM = ()  # Simulate
  GREEDY = ()  # greedy
  DSATUR = ()
  SCIPOPT = ()
  NEARESTNEIGHBOR = ()
  NEARESTNEIGHBOR2OPT = ()
  NEARESTNEIGHBOR3OPT = ()
  NEARESTNEIGHBOR2OPT3OPT = ()

class graphType(enum):
  '''
  Enum for graph classes
  '''
  COMPLETE = ()
  STAR = ()
  WHEEL = ()
  TREE = ()
  BIPARTITE = ()
  COMPBIPARTITE = ()
  PATH = ()
  CYCLE = ()
  ANTICYCLE = ()
  TWOTREE = ()
  KPATH = ()
  SKPATH = ()
  KCYCLE = ()
  KSTAR = ()
  LEXI = ()
  CARTESIAN = ()
  STRONG = ()
  TENSOR = ()
  JOIN = ()
  KPARTITE = ()
  FAN = ()
  CENTIPEDE = ()
  BANANATREE = ()
  FIRECRACKER = ()
  CORONA = ()
  FRIENDSHIP = ()
  BOOK = ()
  SUN = ()
  SUNLET = ()

class return_type(enum):
  '''
  Return type for extern python scripts
  '''
  DISPLAY = ()
  MARK = ()
  DISMARK = ()
  SGRAPH = ()

class ob_type(enum):
  '''
  Return type for extern python scripts
  '''
  NODE = ()
  EDGE = ()
  RENAME = ()
  PLUS = ()

class mode(enum):
  '''
  Running mode of the program (only for DominationMain)
  '''
  NORMAL = ()
  CPROFILE = ()
  TRACER = ()

class solution_presentation(enum):
  '''
  Enum for the representation of the solution (only for DominationMain)
  '''
  PROBABILITY = ()
  POLYNOM = ()

class simulation_type(enum):
  '''
  Type of the simulation
  '''
  MARK = ()
  MARKDIFF = ()
  SHOW = ()
  SHOWDIFF = ()

# Available embedding algorithms of graphviz
PLOTMODES_GRAPHVIZ = ['dot', 'twopi', 'neato', 'circo', 'fdp']
# Own embedding algorithms
PLOTMODES_INTERN = ['circo-intern', 'spring embedding', 'grid']
# All together
PLOTMODES = ['dot', 'twopi', 'neato', 'circo', 'fdp',
             'circo-intern', 'spring embedding', 'grid']

# List of possible colors of vertices and edges
COLORLIST = ["red", "blue", "green", "gray", "magenta",
             "yellow", "cyan", "black"]

# Dicts to convert Qt-colors in RGB and strings
COLORRGB = {QtCore.Qt.red: "#ff0000",
            QtCore.Qt.green: "#00ff00",
            QtCore.Qt.blue: "#0000ff",
            QtCore.Qt.cyan: "#00ffff",
            QtCore.Qt.magenta: "#ff00ff",
            QtCore.Qt.yellow: "#ffff00",
            QtCore.Qt.gray: "#a0a0a4"}
COLORTIKZ = {QtCore.Qt.red: "red",
             QtCore.Qt.darkRed: "red",
             QtCore.Qt.green: "green",
             QtCore.Qt.darkGreen: "green",
             QtCore.Qt.blue: "blue",
             QtCore.Qt.darkBlue: "blue",
             QtCore.Qt.cyan: "cyan",
             QtCore.Qt.darkCyan: "cyan",
             QtCore.Qt.magenta: "magenta",
             QtCore.Qt.darkMagenta: "magenta",
             QtCore.Qt.yellow: "yellow",
             QtCore.Qt.darkYellow: "black",
             QtCore.Qt.gray: "gray",
             QtCore.Qt.darkGray: "gray",
             QtCore.Qt.black: "black"}

# Dict with the names of the functions to display in the result window
CALCNAMES = {"DistanceMatrix": QtWidgets.QApplication.translate("MainWindow",
                                                                "Distanzmatrix",
                                                                None),
             "AdjacencySpectrum": QtWidgets.QApplication.translate("MainWindow",
                                                                   "Spektrum der Adjazenzmatrix",
                                                                   None),
             "LaplacianSpectrum": QtWidgets.QApplication.translate("MainWindow",
                                                                   "Spektrum der Laplacematrix",
                                                                   None),
             "StressCentrality": QtWidgets.QApplication.translate("MainWindow",
                                                                  "Stress-Zentralität",
                                                                  None),
             "BetweennessCentrality": QtWidgets.QApplication.translate("MainWindow",
                                                                       "Betweenness-Zentralität",
                                                                      None),
             "ClosenessCentrality": QtWidgets.QApplication.translate("MainWindow",
                                                                     "Closeness-Zentralität",
                                                                     None),
             "PageRank": "PageRank",
             "LongestIsometricCycle": QtWidgets.QApplication.translate("MainWindow",
                                                                       "Längster isometrischer Kreis",
                                                                       None),
             "MaxClique": QtWidgets.QApplication.translate("MainWindow",
                                                           "Maximale Cliquen im Graphen",
                                                           None),
             "IndependencePoly":QtWidgets.QApplication.translate("MainWindow",
                                                                  "Unabhänigkeitspolynom:",
                                                                  None),
             "GeneralSubgraphCountingPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                             "General subgraph counting polynomial:",
                                                                             None),
             "ExtendedSubgraphCountingPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                              "Extended subgraph counting polynomial:",
                                                                              None),
             "SubgraphPoly": QtWidgets.QApplication.translate("MainWindow",
                                                              "Subgraph polynomial:",
                                                              None),
             "SubgraphEnumeratingPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                         "Subgraph enumerating polynomial:",
                                                                         None),
             "SubgraphComponentPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                       "Subgraph component polynomial:",
                                                                       None),
             "AlliancePoly": QtWidgets.QApplication.translate("MainWindow",
                                                              "Alliance polynomial:",
                                                              None),
             "IrredundancePoly": QtWidgets.QApplication.translate("MainWindow",
                                                                  "Irredundance Polynom:",
                                                                 None),
             "CutPoly": QtWidgets.QApplication.translate("MainWindow",
                                                         "Cut Polynom:",
                                                         None),
             "VertexConnectivity": QtWidgets.QApplication.translate("MainWindow",
                                                                    "Knotenzusammenhangszahl:",
                                                                    None),
             "EdgeConnectivity": QtWidgets.QApplication.translate("MainWindow",
                                                                  "Kantenzusammenhangszahl:",
                                                                  None),
             "IndependenceNumber": QtWidgets.QApplication.translate("MainWindow",
                                                                    "Unabhänigkeitszahl:",
                                                                    None),
             "CliqueNumber": QtWidgets.QApplication.translate("MainWindow",
                                                              "Cliquenzahl:",
                                                              None),
             "GeneralDominationPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                       "General domination Polynom",
                                                                       None),
             "BipartitionPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                 "Bipartition Polynom",
                                                                 None),
             "DominationPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                "Dominationspolynom:",
                                                                None),
             "TotalDomPoly": QtWidgets.QApplication.translate("MainWindow",
                                                              "Totales Dominationspolynom:",
                                                              None),
             "ConnectedDomPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                  "Zusammenhängendes Dominationspolynom:",
                                                                  None),
             "TriTotalDomPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                 "Trivariates totales Dominationspolynom:",
                                                                 None),
             "IndDomPoly": QtWidgets.QApplication.translate("MainWindow",
                                                            "Unabhängiges Dominationspolynom:",
                                                            None),
             "PerfectDomPoly": QtWidgets.QApplication.translate("MainWindow",
                                                               "Perfektes Dominationspolynom:",
                                                               None),
             "Reliability": QtWidgets.QApplication.translate("MainWindow",
                                                             "Zusammenhangswahrscheinlichkeit:",
                                                             None),
             "AllTerminalRelPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                    "Zusammenhangswahrscheinlichkeit (Kantenausfall):",
                                                                    None),
             "DomRelPoly": QtWidgets.QApplication.translate("MainWindow",
                                                            "Dominationszuverlässigkeitspolynom:",
                                                            None),
             "TotalDomRelPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                 "Totales Dominationszuverlässigkeitspolynom:",
                                                                 None),
             "IndDomRelPoly": QtWidgets.QApplication.translate("MainWindow",
                                                               "Unabh. Dominationszuverlässigkeitspolynom:",
                                                               None),
             "VEDRel": "VEDRel",
             "MatchingPoly": QtWidgets.QApplication.translate("MainWindow", "Matchingpolynom:",
                                                              None),
             "ExtMatchingPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                 "Erweitertes Matchingpolynom:",
                                                                 None),
             "ExtCutPoly": QtWidgets.QApplication.translate("MainWindow",
                                                           "Erweitertes Schnittpolynom:",
                                                            None),
             "ChromaticPoly": QtWidgets.QApplication.translate("MainWindow",
                                                               "Chromatisches Polynom:",
                                                              None),
             "RainbowPoly": QtWidgets.QApplication.translate("MainWindow",
                                                              "Rainbow polynomial:",
                                                              None),
             "RainbowGenFunction": QtWidgets.QApplication.translate("MainWindow",
                                                                     "Rainbow generating function:",
                                                                     None),
             "DominatedPartitionsPoly_d": QtWidgets.QApplication.translate("MainWindow",
                                                                           "Dominated Partition Poly_d:",
                                                                           None),
             "VertexCoverPoly": QtWidgets.QApplication.translate("MainWindow",
                                                                 "Vertex-Cover-Polynom:",
                                                                 None),
             "EdgeCoverPoly": QtWidgets.QApplication.translate("MainWindow",
                                                               "Edge-Cover-Polynom:",
                                                               None),
             "RankPoly": QtWidgets.QApplication.translate("MainWindow",
                                                          "Rankpolynom:",
                                                          None)
             }
