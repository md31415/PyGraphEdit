#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
With PyGraphEdit it is possible to draw graphs and
calculate different graph invariants. The graphs can be saved in a xml-file
or in a SQLite-database. It is also possible to export the graph as a
jpg-picture or in TikZ.
For more information see /help/PyGraphEdit.html.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.


Created on 23.02.2018
Most recent amendment: 04.05.2018

@author: Jensun Ravichandran
'''

import xml.etree.ElementTree as ET
import math
from os import stat
from pathlib import Path

try:
  from utm import from_latlon
  PYUTM = True

except ModuleNotFoundError:
  PYUTM = False
  
  # from os import name as osname
  # from pathlib import Path
  from platform import system
  from ctypes import cdll, c_double, Structure, POINTER
  
  try:
    import configparser
    config = configparser.ConfigParser()
    settingsfile = "settings.ini"
    if system() == "Windows":
      if Path(settingsfile).is_file():
        pass
      else:
        settingsfile = 'settings_win.ini'
    elif system() == "Linux":
      if Path(settingsfile).is_file():
        pass
      else:
        settingsfile = 'settings.ini'
    else:
      print("Unknwon operating system! Trying to use the default config file.")
      settingsfile = 'settings.ini'
    config.read(settingsfile)
    utmpath = config["Paths"]["utm_dir"]
  except:
    utmpath = "..//src/modules/utm/target/debug/"
    # utmpath = "D:/Work/Workspace/utm/target/debug/"
    # utmpath = "/home/markus/workspace/utm_test/src/"
  
  if system() == "Windows":
    ext = "utm.dll"
  elif system() == "Linux":
    ext = "libutm.so"
  else:
    # Default option
    ext = "utm.dll"
  utmpath = utmpath + ext

  class double_row_element(Structure):
    pass

  double_row_element._fields_=[("x", c_double),("y", c_double)]

  UTM_LIB = cdll.LoadLibrary(utmpath)
  UTM_LIB.LatLonToUTMXY.argtypes = [c_double, c_double, c_double]
  UTM_LIB.LatLonToUTMXY.restype = double_row_element

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.SWGraph import SWGraph  # @UnresolvedImport
from SGraph.GraphFunctions import isComplete  # @UnresolvedImport

from modules.consts import return_type, simulation_type  # @UnresolvedImport
from modules.SaveLoadSimulation import SimulationData  # @UnresolvedImport
from modules.UserfilesMark import MarkData  # @UnresolvedImport

# Priority dictionary values obtained from SUMO:
# SOURCE: https://github.com/DLR-TS/sumo/blob/master/data/typemap/osmNetconvert.typ.xml
PRIORITY_DICT = {"motorway": 13,
                "motorway_link": 12,
                "trunk": 11,
                "trunk_link": 10,
                "primary": 9,
                "primary_link": 8,
                "secondary": 7,
                "secondary_link": 6,
                "tertiary": 6,
                "tertiary_link": 5,
                "unclassified": 5,
                "residential": 4,
                # "road": 7,
                "living_street": 3,
                "service": 2,
                "track": 1,
                "services": 1,
                "unsurfaced": 1,
                "footway": 1,
                "pedestrian": 1,
                "path": 1,
                "bridleway": 1,
                "cycleway": 1,
                "step": 1,
                "steps": 1,
                "stairs": 1,
                "bus_guideway": 1,
                "raceway": 14,
                "ford": 1 }

def convertBytes(num):
  '''
  This function will convert bytes to MB, GB etc...
  '''
  # for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
  #   if num < 1024.0:
  #     return "%3.1f %s" % (num, x)
  #   num /= 1024.0
  num /= 1024.0 # Convert to KB
  num /= 1024.0 # Convert to MB
  return num

def fileSize(file_path):
  '''
  This function will return the file size
  '''
  if Path(file_path).is_file():
    file_info = stat(file_path)
    return convertBytes(file_info.st_size)

def isMapLarge(fname, priority):
  '''
  Check the size of the mapfile and the priority level to determine
  whether or not to parse the mapfile
  @param fname: name of the map file to be parsed
  @param priority: min priority for the streets to be included
  '''
  sz = fileSize(fname)
  if sz >= 100:
    return True
  if sz <= 10:
    return False
  else:
    if priority >= 10:
      return False
    else:
      return True

def parseMapfile(fname, priority=None):
  '''
  Parse the OSM XML and return relevant map features for
  builing a graph
  @param fname: name of the map file to be parsed
  @param priority: min priority for the streets to be included
  REQUIRES: utm.dll
  '''
  allNodeSet = set()
  allEdgeSet = set()
  embedding = dict()
  mapRoot = ET.parse(fname).getroot()
  for node in mapRoot.iter('node'):
    allNodeSet.add(node.attrib['id'])
    lat = float(node.attrib['lat'])
    lon = float(node.attrib['lon'])

    if PYUTM:
      easting, northing, _, _ = from_latlon(lat, lon)
      embedding[node.attrib['id']] = (easting, northing)

    else:
      X = UTM_LIB.LatLonToUTMXY(lat, lon, 0.0)
      embedding[node.attrib['id']] = (X.x, X.y)          

  for way in mapRoot.iter('way'):
    routeList = list()
    for nd in way.iter('nd'):
      routeList.append(nd.attrib['ref'])
    
    # Add all edges if there no priority value is supplied
    if not priority:
      for i in range(0, len(routeList)-1):
        allEdgeSet.add( (routeList[i], routeList[i+1]) )
    
    # If a priority limit is set, only add edges with priority
    # higher or equal
    else:
      for tag in way.iter('tag'):
        if tag.attrib['k'] == 'highway':
          highwayType = tag.attrib['v']
          if highwayType in PRIORITY_DICT.keys():
            if PRIORITY_DICT[highwayType] >= priority:
              for i in range(0, len(routeList)-1):
                allEdgeSet.add( (routeList[i], routeList[i+1]) )

  return allNodeSet, allEdgeSet, embedding
  
def reduceOsmGraph(G, d):
  '''
  Reduces the graph G to a graph with no nodes of degree 2 or 0
  @param G: SGraph
  @param d: Coordinates for the graph verticies
  '''
  reiterate = True
  dReduced = dict()
  while(reiterate):
    searchSpace = G.getV()
    for v in searchSpace:
      deg = G.degree(v)
      if deg == 0:
        G.deleteVertex(v)
      elif deg == 2:
        G.contractNode(v)
      else:
        pass
    # Loop through the nodes to determine whether or not to run
    # the deletion/contraction procedure again 
    for v in G.getV():
      deg = G.degree(v)
      if deg == 0:
        reiterate = True
        break
      elif deg == 2:
        reiterate = True
        break
      else:
        reiterate = False
  
  # Deprecated:
  # for v in G.getV():
  #   dReduced[v] = d[v]

  dReduced = {v: d[v] for v in G.getV()}
  
  return G, dReduced
