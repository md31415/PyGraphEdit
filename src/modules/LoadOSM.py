# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod, Christian Bausch

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 21.05.2015

@author: Christian Bausch, Markus Dod
@license: MIT (see LICENSE.txt)

Hier ist noch was komisch. Die Funktionen muessen neu programmiert werden
und mit einer Referenzimplementierung verglichen werden.
Zunaechst mal mit mindom implementieren und dann mit sax.
Vielleicht erst mal sogar bei Varianten dabei lassen.
Reduktionslevel muss variabel sein.
'''

import copy
import math
import xml.sax as sax

from lib.UndirectedGraph import SWGraph, SGraph, component  #  @UnresolvedImport


__all__ = ['loadOSM']


def Main_OSM_Extraction(mapName):
  """
  Extracts the streetdatas out of osm-datas in structure of a graph,
  reduces the graph, merges identically streets between crossings,
  creates a turned graph.
  Called by string with name of osm-data. Example: Main_OSM_Extraction("map_chemnitz.osm")
  """
  # Create first versions of graph, weights, nodeWeights, limits
  preGraph, preWeights, preNodeWeights, limits = create_first_graph(mapName)
   
  # reduce graph, weights and nodeWeights   
  graph, weights, nodeWeights = reduce_graph_with_merging(preGraph, preWeights, preNodeWeights)
   
  # Turn the graph
  #turnedGraph = turn_graph(graph)
   
  #return limits, preWeights, preNodeWeights, preGraph
  return limits, weights, nodeWeights, graph

# weights includes: 0: drivability   1: MaxSpeed   2: list with original nodes  3: time for passing the street

def geo_time(lat1, lat2, lon1, lon2, speed, distance=False):
  """
  Calculates distance between two points IN A TOWN because of curvature of the earth.
  Source: http://www.kompf.de/gps/distcalc.html
  Time in hours
  """
  lat = 0.008725 * (lat1 + lat2)
  dx = math.cos(lat) * (lon1 - lon2)

  if distance:
    return math.sqrt(19.820304 * (dx**2 + (lat1 - lat2)**2))
  else:
    return math.sqrt(19.820304 * (dx**2 + (lat1 - lat2)**2)) / speed

def load_streetmap(dateiname):
  """ Use osm extraction. """
  handler = StreetmapHandler() 
  parser = sax.make_parser() 
  parser.setContentHandler(handler) 
  parser.parse(dateiname)
  return handler.graph, handler.limes, handler.edgesDict, handler.nodeWeights

def turn_graph(graph):
  """ Turn a graph. """
  turnedGraph = SGraph(graph.getV(), set([(e[1], e[0]) for e in graph.getEdgeSet()]))
  return turnedGraph  

def create_first_graph(mapName):
  """ Exctract datas out of the osm datas and delete graph components. """
  # Create graph and other data   
  graph, limits, weights, nodeWeights = load_streetmap(mapName)

  # Delete overlooked streets for example buildings without mark
  V = set(graph.getV())
  while V != set():
    v = V.pop()
    H = component(graph, v)
    if H.order() > 500:
      graph = H
      break
    V -= set(H.getV())

  return graph, weights, nodeWeights, limits

def reduce_graph_with_merging(graph, weights, nodeWeights, strict=False):
  """
  Reduce the grap with merging of the edges.
  We reduce degree 2 nodes if both incident streets have identically properties.
  We create a complete new graph because this is faster than deleting of informations.
  @param graph: SGraph for the reduction
  @param weights: Dict with the edge weights
  @param nodeWeights: Dict with the vertex weights
  @param strict: Reduce all degree-two vertices
  """
  # Caches
  eSet = copy.deepcopy(graph.getEdgeSet())
  forbiddenNodes = set([])
  wSet = set(weights.keys())     # For asking for direction of drivability
  PotentialMergeNodes = set([v for v in graph.getV() if graph.degree(v) == 2])
 
  while PotentialMergeNodes:
    vertex = PotentialMergeNodes.pop()
 
    # extract neighbor without deleting
    nSet = graph.neighbors(vertex) - forbiddenNodes
    n1 = nSet.pop()
    n2 = nSet.pop()           
     
    # Variant: both streets drivable in both directions (and identically weights)
    if ((n1, vertex) in wSet) and ((vertex, n1) in wSet):
      if ((n2, vertex) in wSet) and ((vertex, n2) in wSet):
        if (strict or (weights[(n1, vertex)][0:2] == weights[(vertex, n2)][0:2]
            and weights[(n2, vertex)][0:2] == weights[(vertex, n1)][0:2]
            and weights[(n1, vertex)][0:2] == weights[(vertex, n1)][0:2])):
          # Avoid: creation of parallel edges (avoided overwriting of weights)
          if (n1, n2) not in eSet:
            weights[(n1, n2)] = [weights[(n1, vertex)][0], weights[(n1, vertex)][1],
                                 weights[(n1, vertex)][2] + weights[(vertex, n2)][2][1:],
                                 weights[(n1, vertex)][3] + weights[(vertex, n2)][3]]
            weights[(n2, n1)] = copy.deepcopy(weights[(n1, n2)])
            weights[(n2, n1)][2].reverse()
            forbiddenNodes.add(vertex)
            eSet.add((n1, n2))
            eSet.add((n2, n1))
            wSet.add((n1, n2))
            wSet.add((n2, n1))
            graph.insertEdge(n1, n2)
            graph.insertEdge(n2, n1)
 
    # Variant: both streets drivable in direction 'right' (and identically weights)
    else:
      if (n1, vertex) in wSet:
        if (vertex, n2) in wSet and (n2, vertex) not in wSet:
          if strict or weights[(n1, vertex)] == weights[(vertex, n2)]:
            # Avoid: creation of parallel edges (avoided overwriting of weights)
            if (n1, n2) not in eSet:
              weights[(n1, n2)] = [weights[(n1, vertex)][0], weights[(n1, vertex)][1],
                                   weights[(n1, vertex)][2] + weights[(vertex, n2)][2][1:],
                                   weights[(n1, vertex)][3] + weights[(vertex, n2)][3]]
              forbiddenNodes.add(vertex)
              eSet.add((n1, n2))
              eSet.add((n2, n1))
              wSet.add(n1, n2)
              graph.insertEdge(n1, n2)
              graph.insertEdge(n2, n1)     # deleted through creating of new graph
 
      # Variant: both streets drivable in direction 'left' (and identically weights)    
      else:
        if (n2, vertex) in wSet and (vertex, n2) not in wSet:
          if strict or weights[(n2, vertex)] == weights[(vertex, n1)]:
            # Avoid: creation of parallel edges (avoided overwriting of weights)
            if (n1,n2) not in eSet:
              weights[(n2, n1)] = [weights[(vertex, n1)][0], weights[(vertex, n1)][1],
                                   weights[(vertex, n1)][2] + weights[(n2, vertex)][2][1:],
                                   weights[(vertex, n1)][3] + weights[(n2, vertex)][3]]
              forbiddenNodes.add(vertex)
              eSet.add((n1, n2))
              eSet.add((n2, n1))
              wSet.add((n2, n1))
              graph.insertEdge(n1, n2)     # deleted through creating of new graph
              graph.insertEdge(n2, n1)
 
  # Create new reduced Graph
  newNodes = set(graph.getV()) - forbiddenNodes
  newEdges = set([edge for edge in (graph.getEdgeSet() & set(weights.keys())) if ((edge[0] in newNodes) and (edge[1] in newNodes)) ])
  graph = SGraph(newNodes, newEdges)
 
  # Delete useless weights
  newWeights = {edge: weights[edge] for edge in newEdges }
 
  # Reduce nodeWeights
  newNodeWeights = {vertex: nodeWeights[vertex] for vertex in set(nodeWeights.keys()) & newNodes}
 
  return graph, newWeights, newNodeWeights


class StreetmapHandler(sax.handler.ContentHandler):
  """
  Creates graph out of osm-datas. Also return informations about street and node.
  Reads osm datas linewise. Special functions are called if opened/closed
  element is found.
  """
  def __init__(self): 
    """ Initialize all values. """
    self.graph = "emptygraph"
    # Properties for streets. "yes" if oneway, "no" if not, integer for maximal speed. Properties will be extended
    self.streetDict = {"motorway": ["yes", 130], "trunk": ["no", 130], "primary": ["no", 100],
                       "secondary": ["no", 100], "tertiary": ["no", 50], "unclassified": ["no", 50],
                       "residential": ["no",50], "service": ["no", 12], "motorway_link": ["yes", 60],
                       "trunk_link": ["yes", 60], "primary_link": ["yes", 60], "secondary_link": ["yes", 60],
                       "tertiary_link": ["yes", 50], "living_street": ["no", 30], "track": ["no", 30],
                       "road": ["no", 50]}
    self.wayScore = {"motorway": 1.0, "trunk": 1.0, "primary": 2.0, "secondary": 3.0, "tertiary": 4.0,
                     "unclassified": 5.0, "residential": 6.0, "service": 7.0, "motorway_link": 1.0,
                     "trunk_link": 1.0, "primary_link": 2.0, "secondary_link": 3.0, "tertiary_link": 4.0,
                     "living_street": 8.0, "track": 9.0, "road": 9.0}
    self.limes = []            # Borders of osm datas
    self.nodeDict = {}         # Node ID with geocoordinates
    self.nodes = set([])          
    self.nodeInfo = {}           
    self.nodeWeights = {}      # Similar to nodeInfo, but with geocoordinates instead of node ID
    self.edgesDict = {}        # Streets with informations: drive direction, speed, original street nodes, time for using street
    self.badEdges = set([])    # Edges which are included in graph, but which will be deleted because of oneway
    self.way = []              # Temporarily cache for street
    self.wayInfo = ["no", 50]  # Standard information for streets

    # Types of streets which are useless areas
    self.badWaysK = set(["playground", "building", "landuse", "leisure", "amenity", "rail", "natural",
                         "water", "waterway", "area", "attraction", "power", "man_made", "aeroway",
                         "aerialway", "office","shop", "craft", "tourism", "historic", "military",
                         "geological", "boundary"])

    # Types of streets which are not drivable with car
    self.badWaysV = set(["corridor", "footway", "cycleway", "pedestrian", "steps", "path", "bridleway",
                         "bus_guideway", "raceway", "rest_area", "proposed"])

    # Types of streets which are good
    self.goodWaysV = set(["motorway", "trunk", "primary", "secondary", "tertiary", "unclassified",
                          "residential", "service", "motorway_link", "trunk_link", "primary_link",
                          "secondary_link", "tertiary_link", "living_street", "track", "road"])

  def startElement(self, name, attrs):
    """ What to do if an element is opened. """
    if name == "node":
      self.nodeDict[attrs["id"]] = (float(attrs["lat"]), float(attrs["lon"]))
      self.nodeInfo[attrs["id"]] = 5.0

    elif name == "nd":
      self.way.append(attrs["ref"])

    #Create street informations or delete it if it's useless street
    elif name == "tag" and self.way != []:

      if attrs["k"] == "highway":

        # Bad attribute
        if attrs["v"] in self.badWaysV:
          self.way = []
          self.wayInfo = ["no", 50]

        # good attribute
        elif attrs["v"] in self.goodWaysV:  
          self.wayInfo = self.streetDict[attrs["v"]]
          wayClass = self.wayScore[attrs["v"]]

          for node in self.way:
            if self.nodeInfo[node] > wayClass:
              self.nodeInfo[node] = wayClass

      elif attrs["k"] in self.badWaysK:
        self.way = []
        self.wayInfo = ["no", 50]

      elif attrs["k"]== "maxspeed" and self.way != [] and (attrs["v"] not in set(["none", "signals"])):
        self.wayInfo[1] = int(attrs["v"])

      elif attrs["k"] == "oneway" and self.way != []:
        self.wayInfo[0]=attrs["v"]

      elif attrs["k"] == "junction" and attrs["v"] == "roundabout":
        self.wayInfo = ["yes", 30]

      elif attrs["k"] == "railway" and attrs["v"] != "tram":
        self.way = []
        self.wayInfo = ["no", 50]

    elif name == "bounds":
      self.limes = [float(attrs["minlat"]), float(attrs["minlon"]), float(attrs["maxlat"]), float(attrs["maxlon"])]

  def endElement(self, name): 
    """ What to do if an element is closed: Save the street. """
    if name == "way" and self.way != []:
      self.includeWay()
      self.way = []
      self.wayInfo = ["no", 50]

  def endDocument(self):  # erzeuge am Dateiende den graphen und ergänze Straßengewichte
    """ What to do if osm datas are at end. """
    self.wayInfo = ["no", 60]

    # create undirected graph
    self.graph = SGraph(self.nodes,set(self.edgesDict.keys()) | self.badEdges) 
    for w in self.edgesDict:
      self.edgesDict[w] += [[w[0], w[1]], geo_time(float(w[0][0]), float(w[1][0]), float(w[0][1]), float(w[1][1]), self.edgesDict[w][1])]

    # Replace node ID through geo coordinates
    self.nodeWeights = {self.nodeDict[node]: self.nodeInfo[node] for node in self.nodeInfo}

  def includeWay(self):
    """ Insert street nodes and edges and properties in cache for graph creation."""
    # Insert first node
    self.nodes.add(self.nodeDict[self.way[0]]) 

    # Oneway
    if self.wayInfo[0] == "yes":
      for i in range(1, len(self.way)):
        self.nodes.add(self.nodeDict[self.way[i]])
        self.edgesDict[(self.nodeDict[self.way[i-1]], self.nodeDict[self.way[i]])] = copy.deepcopy(self.wayInfo)  
        self.badEdges.add((self.nodeDict[self.way[i]], self.nodeDict[self.way[i-1]]))

    # Not oneway
    else:
      for i in range(1, len(self.way)):
        self.nodes.add(self.nodeDict[self.way[i]])
        self.edgesDict[(self.nodeDict[self.way[i-1]], self.nodeDict[self.way[i]])] = copy.deepcopy(self.wayInfo)            
        self.edgesDict[(self.nodeDict[self.way[i]], self.nodeDict[self.way[i-1]])] = copy.deepcopy(self.wayInfo)

# weights includes: 0: drivability   1: MaxSpeed   2: list with original nodes  3: time for passing the street

def loadOSM(filename):
  '''Load the graph from the osm-file and rename the vertices'''
  limits, weights, nodeWeights, graph = Main_OSM_Extraction(filename)

  # Calculate a mapping to the vertices {1,..,n}
  mapping = dict((k, i+1) for i,k in enumerate(graph.getV()))

  # Generate a new graph with this vertices
  new_graph = SWGraph()
  coord = dict([])
  for m in mapping:
    new_graph.insertVertex(mapping[m])
    x = 6371.032 * math.cos(m[0]) * math.cos(m[1])
    y = 6371.032 * math.cos(m[0]) * math.sin(m[1])
    coord[mapping[m]] = (x, y)
  
  # Add the weights to the graph
  for e in graph.getEdgeSet():
    if e in weights:
      weight = weights[e][3]
    new_graph.insertEdge(mapping[e[0]], mapping[e[1]], weight)

  return new_graph, coord
