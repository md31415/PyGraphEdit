'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
'''

from sympy import symbols

from SGraph.SWGraph import SWGraph  # @UnresolvedImport
from SGraph.GraphFunctions import isComplete  # @UnresolvedImport

from modules.consts import return_type, simulation_type  # @UnresolvedImport
from modules.SaveLoadSimulation import SimulationData  # @UnresolvedImport
from modules.UserfilesMark import MarkData  # @UnresolvedImport


'''
return_type is an enum to specify the return type of the function.
If return_type.DISPLAY is returned, then the result will be shown in the result window.

If return_type.MARK is returned, then PyGraphEdit instance of MarkData
and tries to mark the elements with the given color.
possible colors: "red", "blue", "green", "gray", "magenta", "yellow", "cyan", "black"

If return_type.DISMARK is returned, then the result will be shown and PyGraphEdit tries
to mark the returned elements. It is necessary that the graph is normalized before calling
the simulation method!

return_type = enum(DISPLAY="display", MARK="mark", DISMARK="dismark", SGRAPH="SGraph")
simulation_type = enum(MARK="mark", MARKDIFF="markdiff", SHOW="show", SHOWDIFF="showdiff")
'''

__all__ = ['Hello', 'returnGraph', 'testSim', 'testSim2', 'testSim3', 'testSim4']

x = symbols('x')

def Hello(graph):
  '''
  The function have exactly one parameter: SGraph
  '''
  if isComplete(graph):
    return x**graph.order(), return_type.DISPLAY
  else:
    m = MarkData(returnType=return_type.DISMARK)
    m.addRule(1, "red")
    m.addRule((1,3), "blue")
    m.addRule(4, "cyan")
    return m, return_type.DISMARK

def returnGraph(graph):
  d = dict([(1,(-100,0)),(2,(0,100)),(3,(0,-100)),(4,(100,0))])
  return (SWGraph(V=[1,2,3,4],E=[(1,2),(1,3),(2,3),(2,4),(3,4)]), d), return_type.SGRAPH

def testSim(graph):
  # possible colors: "red", "blue", "green", "gray", "magenta", "yellow", "cyan", "black"
  simdata = SimulationData()
  simdata.setSimType(simulation_type.MARK)
  simdata.setNextStep(dict([]))
  simdata.setNextStep(dict([(1,"red")]))
  N = [1]
  M = set([1])
  while len(N) != graph.order():
    N = graph.neighbors_set(N)
    d = [(v, "red") for v in set(N) - M]
    if len(M) != 0:
      d.extend([(v, "cyan") for v in M])
    simdata.setNextStep(dict(d))
    M = M | set(N)
  
  return simdata, simulation_type.MARK

def testSim2(graph):
  simdata = SimulationData([dict([]),dict([(1,"red")]), dict([(1,"cyan"),((1,2),"red"),(2,"red"),(3,"red")]), 
                            dict([(2,"cyan"),(3,"cyan"),(4,"red")])],
                           simulation_type.MARKDIFF)
  
  return simdata, simulation_type.MARKDIFF

def testSim3(graph):
  '''Only the given elements will be showed'''
  simdata = SimulationData([[], [1], [1,2,3,(1,2),(1,3),(2,3)], [1,2,3,(1,2),(1,3),4], 
                            [1,2,3,(1,2),(1,3),4,(2,4),(3,4)]])
  simdata.setSimType(simulation_type.SHOW)

  return simdata, simulation_type.SHOW

def testSim4(graph):
  '''The same simulation as testSim3'''
  simdata = SimulationData([[], [1], [2,3,(1,2),(1,3),(2,3)], [4], [(2,4),(3,4)]])
  simdata.setSimType(simulation_type.SHOWDIFF)

  return simdata, simulation_type.SHOWDIFF
