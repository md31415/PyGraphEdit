#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
With PyGraphEdit it is possible to draw graphs and
calculate different graph invariants. The graphs can be saved in a xml-file
or in a SQLite-database. It is also possible to export the graph as a
jpg-picture or in TikZ.
For more information see /help/PyGraphEdit.html.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.


Created on 19.01.2018
Most recent amendment: 19.02.2018

@author: Markus Dod, Jensun Ravichandran
@contact: mdod@hs-mittweida.de
@version: 0.63
@requires: Python 3.x, PyQt5, pydot, sympy; optional: Graphviz, numpy, matplotlib, scipopt, ogdf
@copyright: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import xml.etree.ElementTree as ET
import math
from ctypes import cdll, c_double, Structure, POINTER

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.SWGraph import SWGraph  # @UnresolvedImport
from SGraph.GraphFunctions import isComplete  # @UnresolvedImport

from modules.consts import return_type, simulation_type  # @UnresolvedImport
from modules.SaveLoadSimulation import SimulationData  # @UnresolvedImport
from modules.UserfilesMark import MarkData  # @UnresolvedImport

'''
return_type is an enum to specify the return type of the function.
If return_type.DISPLAY is returned, then the result will be shown in the result window.

If return_type.MARK is returned, then PyGraphEdit instance of MarkData
and tries to mark the elements with the given color.
possible colors: "red", "blue", "green", "gray", "magenta", "yellow", "cyan", "black"

If return_type.DISMARK is returned, then the result will be shown and PyGraphEdit tries
to mark the returned elements. It is necessary that the graph is normalized before calling
the simulation method!

return_type = enum(DISPLAY="display", MARK="mark", DISMARK="dismark", SGRAPH="SGraph")
simulation_type = enum(MARK="mark", MARKDIFF="markdiff", SHOW="show", SHOWDIFF="showdiff")
'''

__all__ = ['testOut']

FNAME = "userfiles/oberlungwitz_copy.osm"
# RADIUS = 6371 # ~Radius (in km) of the Earth
RADIUS = 15000

PRIORITY_DICT = {"motormay": 13,
                "motorway_link": 8,
                "trunk": 12,
                "trunk_link": 8,
                "primary": 11,
                "primary_link": 8,
                "secondary": 10,
                "secondary_link": 8,
                "tertiary": 9,
                "unclassified": 8,
                "residential": 7,
                "road": 7,
                "living_street": 6,
                "pedestrian": 5,
                "service": 5,
                "bus_guideway": 5,
                "track": 4,
                "path": 2,
                "cycleway": 2,
                "footway": 2,
                "bridleway": 2,
                "steps": 2 }

def testOut(graph):
  '''
  Test if imports and other meta details are working
  '''
  G, d = reduceGraph()
  dEmbed = dict()
  scaledEmbed = dict()
  scaleFactor = 4

  for v in G.getV():
    dEmbed[v] = d[v]
  print("Embedding:", len(G.getV()))

  coordinates = list(dEmbed.values())
  max_lon = coordinates[0][1]
  min_lon = coordinates[0][1]
  max_lat = coordinates[0][0]
  min_lat = coordinates[0][0]
  # Rescale embedding
  for tup in coordinates:
    if tup[0] >= max_lat:
      max_lat = tup[0]
    if tup[0] <= min_lat:
      min_lat = tup[0]
    if tup[1] >= max_lon:
      max_lon = tup[1]
    if tup[1] <= min_lon:
      min_lon = tup[1]

  for v in G.getV():
    x =  scaleFactor * (d[v][0] - min_lat) / (max_lat - min_lat)
    y =  scaleFactor * (d[v][1] - min_lon) / (max_lon - min_lon)
    tup = (x, y)
    scaledEmbed[v] = tup

  d1 = dict([(1,(-100,0)),(2,(0,100)),(3,(0,-100)),(4,(100,0))])
  # return (SWGraph(V=[1,2,3,4],E=[(1,2),(1,3),(2,3),(2,4),(3,4)]), d1), return_type.SGRAPH
  
  # return (G, scaledEmbed), return_type.SGRAPH
  return (G, dEmbed), return_type.SGRAPH

def parseMapfile(fname, priority = None):
  '''
  Parse the OSM XML and return relevant map features for
  builing a graph
  @param fname: name of the map file to be parsed
  '''
  allNodeSet = set()
  allEdgeSet = set()
  embedding = dict()
  namedNodeSet = set()
  namedEdgeSet = set()
  mapRoot = ET.parse(fname).getroot()
  for node in mapRoot.iter('node'):
    allNodeSet.add(node.attrib['id'])
    lat = float(node.attrib['lat'])
    lon = float(node.attrib['lon'])
    
    class double_row_element(Structure):
     pass

    double_row_element._fields_=[("x",c_double),("y",c_double)]

    # lib = cdll.LoadLibrary("/home/markus/workspace/utm_test/src/libutm.so")
    lib = cdll.LoadLibrary("D:/Work/Workspace/utm/target/debug/utm.dll")
    lib.LatLonToUTMXY.argtypes = [c_double, c_double, c_double]
    lib.LatLonToUTMXY.restype = double_row_element
    X = lib.LatLonToUTMXY(lat, lon, 0.0)
    embedding[node.attrib['id']] = (X.x, X.y)

    # EXTRA: Parse named nodes
    for tag in node.iter('tag'):
      if tag.attrib['k'] == 'name':
        # print(node.attrib['id'])
        # print(tag.attrib['v'])
        namedNodeSet.add(tag.attrib['v'])
  
  for way in mapRoot.iter('way'):
    routeList = list()
    for nd in way.iter('nd'):
      # if nd.attrib['ref'] not in allNodeSet:
      #   print("Not found!")
      routeList.append(nd.attrib['ref'])
    
    # EXTRA: Parse named edges
    for tag in way.iter('tag'):
      if tag.attrib['k'] == 'name':
        namedEdgeSet.add(tag.attrib['v'])

    # Add all edges if there no priority value is supplied
    if priority==None:
      for i in range(0, len(routeList)-1):
        allEdgeSet.add( (routeList[i], routeList[i+1]) )
    
    # If a priority limit is set, only add edges with priority
    # higher or equal
    else:
      for tag in way.iter('tag'):
        if tag.attrib['k'] == 'highway':
          highwayType = tag.attrib['v']
          if highwayType in PRIORITY_DICT.keys():
            if PRIORITY_DICT[highwayType] >= priority:
              for i in range(0, len(routeList)-1):
                allEdgeSet.add( (routeList[i], routeList[i+1]) )

  return allNodeSet, allEdgeSet, embedding

def generateGraph():
  '''
  Uses the information returned from pareseMapfile function
  to construct a simple graph
  '''
  V, E, d = parseMapfile(FNAME, priority=8)
  G = SGraph(V, E)
  return G, d

def reduceGraph():
  '''
  Calls the generateGraph function to reduces the returned graph
  to a graph with no nodes of degree 2 or 0
  '''
  G, d = generateGraph()
  reiterate = True
  while(reiterate):
    print("Iterating...")
    searchSpace = G.getV()
    for v in searchSpace:
      deg = G.degree(v)
      if deg == 0:
        G.deleteVertex(v)
      elif deg == 2:
        G.contractNode(v)
      else:
        pass
    # Loop through the nodes to determine whether or not to run
    # the deletion/contraction procedure again 
    for v in G.getV():
      deg = G.degree(v)
      if deg == 0:
        reiterate = True
        break
      elif deg == 2:
        reiterate = True
        break
      else:
        reiterate = False
  return G, d