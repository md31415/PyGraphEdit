# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialogs/PyGraphEdit_LoadOSM.ui'
#
# Created by: PyQt5 UI code generator 5.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(435, 169)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(21, 31, 391, 109))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButtonLoad = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButtonLoad.setObjectName("pushButtonLoad")
        self.gridLayout.addWidget(self.pushButtonLoad, 3, 0, 1, 5)
        self.pushButtonClose = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButtonClose.setObjectName("pushButtonClose")
        self.gridLayout.addWidget(self.pushButtonClose, 3, 5, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 3)
        self.pushButtonLoadFile = QtWidgets.QPushButton(self.layoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonLoadFile.sizePolicy().hasHeightForWidth())
        self.pushButtonLoadFile.setSizePolicy(sizePolicy)
        self.pushButtonLoadFile.setObjectName("pushButtonLoadFile")
        self.gridLayout.addWidget(self.pushButtonLoadFile, 0, 11, 1, 1)
        self.lineEditFile = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEditFile.setObjectName("lineEditFile")
        self.gridLayout.addWidget(self.lineEditFile, 0, 3, 1, 8)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 3)
        spacerItem = QtWidgets.QSpacerItem(14, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 3, 6, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(14, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 3, 11, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(29, 17, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 1, 9, 1, 3)
        self.spinBoxPriority = QtWidgets.QSpinBox(self.layoutWidget)
        self.spinBoxPriority.setMinimum(1)
        self.spinBoxPriority.setMaximum(14)
        self.spinBoxPriority.setProperty("value", 7)
        self.spinBoxPriority.setObjectName("spinBoxPriority")
        self.gridLayout.addWidget(self.spinBoxPriority, 1, 3, 1, 3)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 1, 6, 1, 3)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem4, 3, 7, 1, 4)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButtonLoad.setText(_translate("Dialog", "Laden"))
        self.pushButtonClose.setText(_translate("Dialog", "Abbrechen"))
        self.label.setText(_translate("Dialog", "OSM map file: "))
        self.pushButtonLoadFile.setText(_translate("Dialog", "..."))
        self.label_2.setText(_translate("Dialog", "Priority: "))

