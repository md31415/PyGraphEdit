# -*- coding: utf-8 -*-
"""
formlayout
==========

Module creating PyQt4 form dialogs/layouts to edit various type of parameters


formlayout License Agreement (MIT License)
------------------------------------------

Copyright (c) 2009 Pierre Raybaut

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.
"""

__version__ = '1.0.9'
__license__ = __doc__

DEBUG = False

import sys
STDERR = sys.stderr

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QLineEdit, QComboBox, QLabel, QSpinBox, 
                            QStyle, QDialogButtonBox, QVBoxLayout,
                            QDialog, QCheckBox, QFormLayout, QWidget,
                            QTabWidget, QStackedWidget)
from PyQt5.QtCore import (Qt, pyqtSignal, pyqtSlot)


class FormWidget(QWidget):
    def __init__(self, data, comment="", parent=None):
        super(FormWidget, self).__init__(parent)
        from copy import deepcopy
        self.data = deepcopy(data)
        self.widgets = []
        self.formlayout = QFormLayout(self)
        if comment:
            self.formlayout.addRow(QLabel(comment))
            self.formlayout.addRow(QLabel(" "))
        if DEBUG:
            print("\n"+("*"*80))
            print ("DATA:", self.data)
            print ("*"*80)
            print ("COMMENT:", comment)
            print ("*"*80)
        self.setup()

    def setup(self):
      if self.data != None:
        for element in self.data:
            label = element[0]
            value = element[1]
            try:
              comment = element[2]
            except:
              comment = ''
            if DEBUG:
                print("value:", value)
            if label is None and value is None:
                # Separator: (None, None)
                self.formlayout.addRow(QLabel(" "), QLabel(" "))
                self.widgets.append(None)
                continue
            elif label is None:
                # Comment
                self.formlayout.addRow(QLabel(value))
                self.widgets.append(None)
                continue
            elif isinstance(value, str):
                field = QLineEdit(value, self)
            elif isinstance(value, (list, tuple)):
                selindex = value.pop(0)
                field = QComboBox(self)
                if isinstance(value[0], (list, tuple)):
                    keys = [ key for key, _val in value ]
                    value = [ val for _key, val in value ]
                else:
                    keys = value
                field.addItems(value)
                if selindex in value:
                    selindex = value.index(selindex)
                elif selindex in keys:
                    selindex = keys.index(selindex)
                elif not isinstance(selindex, int):
                    print("Warning: '%s' index is invalid (label: " \
                                    "%s, value: %s)" % (selindex, label, value))
                    selindex = 0
                field.setCurrentIndex(selindex)
            elif isinstance(value, bool):
                field = QCheckBox(self)
                field.setCheckState(Qt.Checked if value else Qt.Unchecked)
            elif isinstance(value, float):
                field = QLineEdit(repr(value), self)
            elif isinstance(value, int):
                field = QSpinBox(self)
                field.setRange(-1e9, 1e9)
                field.setValue(value)
            else:
                field = QLineEdit(repr(value), self)
            field.setToolTip(comment)
            self.formlayout.addRow(label, field)
            self.widgets.append(field)
            
    def get(self):
      valuelist = []
      if self.data != None:
        for index,element in enumerate(self.data):
            label = element[0]
            value = element[1]
            field = self.widgets[index]
            if label is None:
                # Separator / Comment
                continue
            elif isinstance(value, str):
                value = field.text()
            elif isinstance(value, (list, tuple)):
                index = int(field.currentIndex())
                if isinstance(value[0], (list, tuple)):
                    value = value[index][0]
                else:
                    value = value[index]
            elif isinstance(value, bool):
                value = field.checkState() == Qt.Checked
            elif isinstance(value, float):
                value = float(field.text())
            elif isinstance(value, int):
                value = int(field.value())
            else:
                value = eval(str(field.text()))
            valuelist.append(value)
        return valuelist

class FormComboWidget(QWidget):
    def __init__(self, datalist, comment="", parent=None):
        super(FormComboWidget, self).__init__(parent)
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.combobox = QComboBox()
        layout.addWidget(self.combobox)
         
        self.stackwidget = QStackedWidget(self)
        layout.addWidget(self.stackwidget)
        self.combobox.currentIndexChanged.connect(self.stackwidget.setCurrentIndex)
         
        self.widgetlist = []
        for data, title, comment in datalist:
            self.combobox.addItem(title)
            widget = FormWidget(data, comment=comment, parent=self)
            self.stackwidget.addWidget(widget)
            self.widgetlist.append(widget)
 
    def get(self):
        return [ widget.get() for widget in self.widgetlist]
        
class FormTabWidget(QWidget):
  def __init__(self, datalist, comment="", parent=None):
    super(FormTabWidget, self).__init__(parent)
    layout = QVBoxLayout()
    self.tabwidget = QTabWidget()
    layout.addWidget(self.tabwidget)
    self.setLayout(layout)
    self.widgetlist = []
    for data, title, comment in datalist:
      if len(data[0])==3:
        widget = FormComboWidget(data, comment=comment, parent=self)
      else:
        widget = FormWidget(data, comment=comment, parent=self)
      index = self.tabwidget.addTab(widget, title)
      self.tabwidget.setTabToolTip(index, comment)
      self.widgetlist.append(widget)
          
  def get(self):
    return [widget.get() for widget in self.widgetlist]

class FormDialog(QDialog):
  """Form Dialog"""
  def __init__(self, data, title="", comment="",
               icon=None, parent=None, apply=None):
    super(FormDialog, self).__init__(parent)

    self.apply_callback = apply
    
    layout = QVBoxLayout()
      
    # Form
    if data == None:
      self.formwidget = FormWidget(data, comment=comment, parent=self)
    elif data == "yesorno":
      self.formwidget = FormWidget(None, comment=comment, parent=self)
    elif isinstance(data[0][0], (list, tuple)):
      self.formwidget = FormTabWidget(data, comment=comment, parent=self)
    else:
      self.formwidget = FormWidget(data, comment=comment, parent=self)
    
    layout.addWidget(self.formwidget)
    
    # Button box
    if data == "yesorno":
      bbox = QDialogButtonBox(QDialogButtonBox.Yes | QDialogButtonBox.No)
      bbox.accepted.connect(self.ok)
      bbox.rejected.connect(self.cancel)
    elif data != None:
      bbox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
      if self.apply_callback is not None:
          apply_btn = bbox.addButton(QDialogButtonBox.Apply)
          apply_btn.clicked.connect(self.apply)
      bbox.accepted.connect(self.accept)
      bbox.rejected.connect(self.reject)
    else:
      bbox = QDialogButtonBox(QDialogButtonBox.Ok)
      if self.apply_callback is not None:
          apply_btn = bbox.addButton(QDialogButtonBox.Apply)
          apply_btn.clicked.connect(self.apply)
      bbox.accepted.connect(self.accept)
      
    layout.addWidget(bbox)

    self.setLayout(layout)
      
    self.setWindowTitle(title)
    if not isinstance(icon, QIcon):
        icon = QWidget().style().standardIcon(QStyle.SP_MessageBoxQuestion)
    self.setWindowIcon(icon)
    
  def ok(self):
    self.data = True
    QDialog.accept(self)
    
  def cancel(self):
    self.data = False
    QDialog.accept(self)  
    
  def accept(self):
    self.data = self.formwidget.get()
    QDialog.accept(self)
      
  def reject(self):
    self.data = None
    QDialog.reject(self)
      
  def apply(self):
    self.apply_callback(self.formwidget.get())
      
  def get(self):
    """Return form result"""
    return self.data


def fedit(data, title="", comment="", icon=None, parent=None, apply=None):
    """
    Create form dialog and return result
    (if Cancel button is pressed, return None)
    
    data: datalist, datagroup, "yesorno"
    title: string
    comment: string
    icon: QIcon instance
    parent: parent QWidget
    apply: apply callback (function)
    
    datalist: list/tuple of (field_name, field_value)
    datagroup: list/tuple of (datalist *or* datagroup, title, comment)
    "yesorno": Dialog with a comment who sends True or False
    
    -> one field for each member of a datalist
    -> one tab for each member of a top-level datagroup
    -> one page (of a multipage widget, each page can be selected with a combo
       box) for each member of a datagroup inside a datagroup
       
    Supported types for field_value:
      - int, float, str, unicode, bool
      - colors: in Qt-compatible text form, i.e. in hex format or name (red,...)
                (automatically detected from a string)
      - list/tuple:
          * the first element will be the selected index (or value)
          * the other elements can be couples (key, value) or only values
    """
    dialog = FormDialog(data, title, comment, icon, parent, apply)
    if dialog.exec_():
      return dialog.get()
