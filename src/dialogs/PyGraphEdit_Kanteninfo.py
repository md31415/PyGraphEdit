# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Kanteninfo.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(253, 188)
    self.groupBox = QtWidgets.QGroupBox(Dialog)
    self.groupBox.setGeometry(QtCore.QRect(30, 20, 201, 121))
    self.groupBox.setObjectName("groupBox")
    self.label_5 = QtWidgets.QLabel(self.groupBox)
    self.label_5.setGeometry(QtCore.QRect(20, 30, 111, 16))
    self.label_5.setObjectName("label_5")
    self.lineEditStart = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditStart.setGeometry(QtCore.QRect(140, 30, 61, 20))
    self.lineEditStart.setReadOnly(True)
    self.lineEditStart.setObjectName("lineEditStart")
    self.lineEditEnd = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditEnd.setGeometry(QtCore.QRect(140, 60, 61, 20))
    self.lineEditEnd.setReadOnly(True)
    self.lineEditEnd.setObjectName("lineEditEnd")
    self.label_6 = QtWidgets.QLabel(self.groupBox)
    self.label_6.setGeometry(QtCore.QRect(20, 60, 111, 16))
    self.label_6.setObjectName("label_6")
    self.lineEditGewicht = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditGewicht.setGeometry(QtCore.QRect(140, 90, 61, 20))
    self.lineEditGewicht.setReadOnly(False)
    self.lineEditGewicht.setObjectName("lineEditGewicht")
    self.label_7 = QtWidgets.QLabel(self.groupBox)
    self.label_7.setGeometry(QtCore.QRect(20, 90, 111, 16))
    self.label_7.setObjectName("label_7")
    self.pushClose = QtWidgets.QPushButton(Dialog)
    self.pushClose.setGeometry(QtCore.QRect(160, 150, 75, 23))
    self.pushClose.setObjectName("pushClose")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Kanteneigenschaften"))
    self.groupBox.setTitle(_translate("Dialog", "Eigenschaften"))
    self.label_5.setText(_translate("Dialog", "Startknoten"))
    self.label_6.setText(_translate("Dialog", "Endknoten"))
    self.label_7.setText(_translate("Dialog", "Kantengewicht"))
    self.pushClose.setText(_translate("Dialog", "OK"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

