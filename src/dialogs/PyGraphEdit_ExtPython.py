# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_ExtPython.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(335, 139)
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setGeometry(QtCore.QRect(30, 10, 81, 17))
    self.label.setObjectName("label")
    self.label_2 = QtWidgets.QLabel(Dialog)
    self.label_2.setGeometry(QtCore.QRect(30, 50, 81, 17))
    self.label_2.setObjectName("label_2")
    self.lineEditFunction = QtWidgets.QLineEdit(Dialog)
    self.lineEditFunction.setEnabled(True)
    self.lineEditFunction.setGeometry(QtCore.QRect(130, 50, 171, 27))
    self.lineEditFunction.setObjectName("lineEditFunction")
    self.comboBoxModules = QtWidgets.QComboBox(Dialog)
    self.comboBoxModules.setGeometry(QtCore.QRect(130, 10, 171, 27))
    self.comboBoxModules.setObjectName("comboBoxModules")
    self.comboBoxFunctions = QtWidgets.QComboBox(Dialog)
    self.comboBoxFunctions.setEnabled(True)
    self.comboBoxFunctions.setGeometry(QtCore.QRect(130, 50, 171, 27))
    self.comboBoxFunctions.setObjectName("comboBoxFunctions")
    self.pushButtonOk = QtWidgets.QPushButton(Dialog)
    self.pushButtonOk.setGeometry(QtCore.QRect(200, 100, 98, 27))
    self.pushButtonOk.setObjectName("pushButtonOk")
    self.pushButtonClose = QtWidgets.QPushButton(Dialog)
    self.pushButtonClose.setGeometry(QtCore.QRect(90, 100, 98, 27))
    self.pushButtonClose.setObjectName("pushButtonClose")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Run extern python function"))
    self.label.setText(_translate("Dialog", "Module"))
    self.label_2.setText(_translate("Dialog", "Function"))
    self.pushButtonOk.setText(_translate("Dialog", "OK"))
    self.pushButtonClose.setText(_translate("Dialog", "Abbrechen"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

