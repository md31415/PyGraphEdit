# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Solution.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(383, 314)
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("E:/workspace/HD_Steg/symbole/2075.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Dialog.setWindowIcon(icon)
    self.pushButtonOK = QtWidgets.QPushButton(Dialog)
    self.pushButtonOK.setGeometry(QtCore.QRect(290, 190, 81, 23))
    self.pushButtonOK.setObjectName("pushButtonOK")
    self.textBrowser = QtWidgets.QTextBrowser(Dialog)
    self.textBrowser.setGeometry(QtCore.QRect(10, 40, 361, 131))
    self.textBrowser.setObjectName("textBrowser")
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setGeometry(QtCore.QRect(10, 10, 281, 16))
    font = QtGui.QFont()
    font.setBold(True)
    font.setWeight(75)
    self.label.setFont(font)
    self.label.setObjectName("label")
    self.label_2 = QtWidgets.QLabel(Dialog)
    self.label_2.setGeometry(QtCore.QRect(10, 190, 81, 17))
    font = QtGui.QFont()
    font.setFamily("Times New Roman")
    font.setPointSize(10)
    font.setItalic(True)
    self.label_2.setFont(font)
    self.label_2.setObjectName("label_2")
    self.labelZeit = QtWidgets.QLabel(Dialog)
    self.labelZeit.setGeometry(QtCore.QRect(110, 190, 66, 17))
    font = QtGui.QFont()
    font.setFamily("Times New Roman")
    font.setPointSize(10)
    font.setItalic(True)
    self.labelZeit.setFont(font)
    self.labelZeit.setText("")
    self.labelZeit.setObjectName("labelZeit")
    self.pushButtonCopy = QtWidgets.QPushButton(Dialog)
    self.pushButtonCopy.setGeometry(QtCore.QRect(180, 190, 91, 23))
    self.pushButtonCopy.setObjectName("pushButtonCopy")
    self.pushButtonCopyLaTeX = QtWidgets.QPushButton(Dialog)
    self.pushButtonCopyLaTeX.setGeometry(QtCore.QRect(180, 220, 91, 23))
    self.pushButtonCopyLaTeX.setObjectName("pushButtonCopyLaTeX")
    self.pushButtonShowPlot = QtWidgets.QPushButton(Dialog)
    self.pushButtonShowPlot.setEnabled(True)
    self.pushButtonShowPlot.setGeometry(QtCore.QRect(294, 220, 81, 23))
    self.pushButtonShowPlot.setObjectName("pushButtonShowPlot")
    self.pushButtonX1 = QtWidgets.QPushButton(Dialog)
    self.pushButtonX1.setGeometry(QtCore.QRect(10, 270, 51, 27))
    self.pushButtonX1.setObjectName("pushButtonX1")
    self.pushButtonXM1 = QtWidgets.QPushButton(Dialog)
    self.pushButtonXM1.setGeometry(QtCore.QRect(70, 270, 51, 27))
    self.pushButtonXM1.setObjectName("pushButtonXM1")
    self.pushButtonSub = QtWidgets.QPushButton(Dialog)
    self.pushButtonSub.setGeometry(QtCore.QRect(260, 270, 111, 27))
    self.pushButtonSub.setObjectName("pushButtonSub")
    self.lineEditSub = QtWidgets.QLineEdit(Dialog)
    self.lineEditSub.setGeometry(QtCore.QRect(140, 270, 113, 27))
    self.lineEditSub.setObjectName("lineEditSub")
    self.line = QtWidgets.QFrame(Dialog)
    self.line.setGeometry(QtCore.QRect(10, 250, 361, 20))
    self.line.setFrameShape(QtWidgets.QFrame.HLine)
    self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
    self.line.setObjectName("line")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Grapheninfos"))
    self.pushButtonOK.setText(_translate("Dialog", "OK"))
    self.label.setText(_translate("Dialog", "Dominationszuverlässigkeitspolynom:"))
    self.label_2.setText(_translate("Dialog", "Benötigte Zeit:"))
    self.pushButtonCopy.setToolTip(_translate("Dialog", "Copy to Clipboard"))
    self.pushButtonCopy.setText(_translate("Dialog", "Copy"))
    self.pushButtonCopyLaTeX.setToolTip(_translate("Dialog", "Copy to Clipboard"))
    self.pushButtonCopyLaTeX.setText(_translate("Dialog", "Copy LaTeX"))
    self.pushButtonShowPlot.setText(_translate("Dialog", "Show Plot"))
    self.pushButtonX1.setText(_translate("Dialog", "x=1"))
    self.pushButtonXM1.setText(_translate("Dialog", "x=-1"))
    self.pushButtonSub.setText(_translate("Dialog", "Substituieren"))
    self.lineEditSub.setToolTip(_translate("Dialog", "<html><head/><body><p>Syntax: x=-1 or x=y or x=y;z=x*y**2 or x=y+z</p></body></html>"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

