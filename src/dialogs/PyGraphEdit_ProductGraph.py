# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_ProductGraph.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(400, 264)
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setGeometry(QtCore.QRect(20, 20, 101, 17))
    self.label.setObjectName("label")
    self.label_2 = QtWidgets.QLabel(Dialog)
    self.label_2.setGeometry(QtCore.QRect(20, 50, 101, 17))
    self.label_2.setObjectName("label_2")
    self.label_3 = QtWidgets.QLabel(Dialog)
    self.label_3.setGeometry(QtCore.QRect(20, 80, 141, 17))
    self.label_3.setObjectName("label_3")
    self.label_4 = QtWidgets.QLabel(Dialog)
    self.label_4.setGeometry(QtCore.QRect(20, 110, 141, 17))
    self.label_4.setObjectName("label_4")
    self.label_5 = QtWidgets.QLabel(Dialog)
    self.label_5.setGeometry(QtCore.QRect(20, 140, 141, 17))
    self.label_5.setObjectName("label_5")
    self.label_6 = QtWidgets.QLabel(Dialog)
    self.label_6.setGeometry(QtCore.QRect(20, 170, 101, 17))
    self.label_6.setObjectName("label_6")
    self.lineEditFirstGraphVertices = QtWidgets.QLineEdit(Dialog)
    self.lineEditFirstGraphVertices.setGeometry(QtCore.QRect(200, 80, 171, 27))
    self.lineEditFirstGraphVertices.setObjectName("lineEditFirstGraphVertices")
    self.lineEditSecondGraphVertices = QtWidgets.QLineEdit(Dialog)
    self.lineEditSecondGraphVertices.setEnabled(True)
    self.lineEditSecondGraphVertices.setGeometry(QtCore.QRect(200, 140, 171, 27))
    self.lineEditSecondGraphVertices.setObjectName("lineEditSecondGraphVertices")
    self.comboBoxProduct = QtWidgets.QComboBox(Dialog)
    self.comboBoxProduct.setGeometry(QtCore.QRect(200, 20, 171, 27))
    self.comboBoxProduct.setObjectName("comboBoxProduct")
    self.comboBoxFirstGraph = QtWidgets.QComboBox(Dialog)
    self.comboBoxFirstGraph.setGeometry(QtCore.QRect(200, 50, 171, 27))
    self.comboBoxFirstGraph.setObjectName("comboBoxFirstGraph")
    self.comboBoxSecondGraph = QtWidgets.QComboBox(Dialog)
    self.comboBoxSecondGraph.setGeometry(QtCore.QRect(200, 110, 171, 27))
    self.comboBoxSecondGraph.setObjectName("comboBoxSecondGraph")
    self.comboBoxDrawMode = QtWidgets.QComboBox(Dialog)
    self.comboBoxDrawMode.setGeometry(QtCore.QRect(200, 170, 171, 27))
    self.comboBoxDrawMode.setObjectName("comboBoxDrawMode")
    self.pushButtonOk = QtWidgets.QPushButton(Dialog)
    self.pushButtonOk.setGeometry(QtCore.QRect(280, 220, 98, 27))
    self.pushButtonOk.setObjectName("pushButtonOk")
    self.pushButtonClose = QtWidgets.QPushButton(Dialog)
    self.pushButtonClose.setGeometry(QtCore.QRect(170, 220, 98, 27))
    self.pushButtonClose.setObjectName("pushButtonClose")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Produktgraph"))
    self.label.setText(_translate("Dialog", "Produktart"))
    self.label_2.setText(_translate("Dialog", "Erster Graph"))
    self.label_3.setText(_translate("Dialog", "Anzahl der Knoten"))
    self.label_4.setText(_translate("Dialog", "Zweiter Graph"))
    self.label_5.setText(_translate("Dialog", "Anzahl der Knoten"))
    self.label_6.setText(_translate("Dialog", "Zeichenstil"))
    self.pushButtonOk.setText(_translate("Dialog", "OK"))
    self.pushButtonClose.setText(_translate("Dialog", "Abbrechen"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

