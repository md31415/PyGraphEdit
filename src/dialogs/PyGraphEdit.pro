SOURCES      = ../PyGraphEdit.py \
../modules/consts.py \
PyGraphEdit_Broadcasting.py \
PyGraphEdit_Close.py \
PyGraphEdit_CompareResults.py \
PyGraphEdit_Config.py \
PyGraphEdit_Display.py \
PyGraphEdit_EdgeWeights.py \
PyGraphEdit_Export.py \
PyGraphEdit_GenerateTest.py \
PyGraphEdit_Graphclasses.py \
PyGraphEdit_Info.py \
PyGraphEdit_Knoteninfo.py \
PyGraphEdit_Kanteninfo.py \
PyGraphEdit_LadenDialog.py \
PyGraphEdit_LoadAnimationDialog.py \
PyGraphEdit_LoadDialog2Graphs.py \
PyGraphEdit_main.py \
PyGraphEdit_ProductGraph.py \
PyGraphEdit_SaveSimulation.py \
PyGraphEdit_Solution.py \
PyGraphEdit_StartTest.py \
PyGraphEdit_SVGexport.py \
PyGraphEdit_TestStatus.py \
PyGraphEdit_Tikzexport.py \
PyGraphEdit_Wait.py
TRANSLATIONS = PyGraphEdit_en.ts
CODECFORTR = UTF-8
