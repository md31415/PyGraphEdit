# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_CompareResults.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(320, 215)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
    Dialog.setSizePolicy(sizePolicy)
    Dialog.setSizeGripEnabled(False)
    self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
    self.verticalLayout.setObjectName("verticalLayout")
    self.label_5 = QtWidgets.QLabel(Dialog)
    self.label_5.setObjectName("label_5")
    self.verticalLayout.addWidget(self.label_5)
    self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_3.setObjectName("horizontalLayout_3")
    self.lineEditResultfile = QtWidgets.QLineEdit(Dialog)
    self.lineEditResultfile.setText("")
    self.lineEditResultfile.setObjectName("lineEditResultfile")
    self.horizontalLayout_3.addWidget(self.lineEditResultfile)
    self.pushButtonFilemenu = QtWidgets.QPushButton(Dialog)
    self.pushButtonFilemenu.setMaximumSize(QtCore.QSize(21, 16777215))
    self.pushButtonFilemenu.setObjectName("pushButtonFilemenu")
    self.horizontalLayout_3.addWidget(self.pushButtonFilemenu)
    self.verticalLayout.addLayout(self.horizontalLayout_3)
    self.horizontalLayout = QtWidgets.QHBoxLayout()
    self.horizontalLayout.setObjectName("horizontalLayout")
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setObjectName("label")
    self.horizontalLayout.addWidget(self.label)
    self.comboBoxKey = QtWidgets.QComboBox(Dialog)
    self.comboBoxKey.setObjectName("comboBoxKey")
    self.comboBoxKey.addItem("")
    self.comboBoxKey.addItem("")
    self.comboBoxKey.addItem("")
    self.horizontalLayout.addWidget(self.comboBoxKey)
    self.verticalLayout.addLayout(self.horizontalLayout)
    self.line = QtWidgets.QFrame(Dialog)
    self.line.setFrameShape(QtWidgets.QFrame.HLine)
    self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
    self.line.setObjectName("line")
    self.verticalLayout.addWidget(self.line)
    self.checkBoxLaTeX = QtWidgets.QCheckBox(Dialog)
    self.checkBoxLaTeX.setObjectName("checkBoxLaTeX")
    self.verticalLayout.addWidget(self.checkBoxLaTeX)
    self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_2.setObjectName("horizontalLayout_2")
    self.lineEditLaTeX = QtWidgets.QLineEdit(Dialog)
    self.lineEditLaTeX.setEnabled(False)
    self.lineEditLaTeX.setText("")
    self.lineEditLaTeX.setObjectName("lineEditLaTeX")
    self.horizontalLayout_2.addWidget(self.lineEditLaTeX)
    self.pushButtonFilemenuLaTeX = QtWidgets.QPushButton(Dialog)
    self.pushButtonFilemenuLaTeX.setEnabled(False)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonFilemenuLaTeX.sizePolicy().hasHeightForWidth())
    self.pushButtonFilemenuLaTeX.setSizePolicy(sizePolicy)
    self.pushButtonFilemenuLaTeX.setMinimumSize(QtCore.QSize(0, 0))
    self.pushButtonFilemenuLaTeX.setMaximumSize(QtCore.QSize(21, 16777215))
    self.pushButtonFilemenuLaTeX.setObjectName("pushButtonFilemenuLaTeX")
    self.horizontalLayout_2.addWidget(self.pushButtonFilemenuLaTeX)
    self.verticalLayout.addLayout(self.horizontalLayout_2)
    self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_4.setObjectName("horizontalLayout_4")
    spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout_4.addItem(spacerItem)
    self.pushButtonStart = QtWidgets.QPushButton(Dialog)
    self.pushButtonStart.setMaximumSize(QtCore.QSize(85, 16777215))
    self.pushButtonStart.setObjectName("pushButtonStart")
    self.horizontalLayout_4.addWidget(self.pushButtonStart)
    self.pushButtonCancel = QtWidgets.QPushButton(Dialog)
    self.pushButtonCancel.setMaximumSize(QtCore.QSize(85, 16777215))
    self.pushButtonCancel.setObjectName("pushButtonCancel")
    self.horizontalLayout_4.addWidget(self.pushButtonCancel)
    self.verticalLayout.addLayout(self.horizontalLayout_4)

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Ergebnisse vergleichen"))
    self.label_5.setText(_translate("Dialog", "Ergebnisdateien"))
    self.pushButtonFilemenu.setText(_translate("Dialog", "..."))
    self.label.setText(_translate("Dialog", "Vergleichswert"))
    self.comboBoxKey.setItemText(0, _translate("Dialog", "time"))
    self.comboBoxKey.setItemText(1, _translate("Dialog", "time (with result)"))
    self.comboBoxKey.setItemText(2, _translate("Dialog", "result"))
    self.checkBoxLaTeX.setText(_translate("Dialog", "Erzeuge LaTeX-Tabelle"))
    self.pushButtonFilemenuLaTeX.setText(_translate("Dialog", "..."))
    self.pushButtonStart.setText(_translate("Dialog", "Start"))
    self.pushButtonCancel.setText(_translate("Dialog", "Cancel"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

