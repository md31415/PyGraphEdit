# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Wait.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(332, 136)
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setGeometry(QtCore.QRect(40, 30, 251, 21))
    font = QtGui.QFont()
    font.setFamily("Arial")
    font.setPointSize(12)
    self.label.setFont(font)
    self.label.setObjectName("label")
    self.progressBar = QtWidgets.QProgressBar(Dialog)
    self.progressBar.setGeometry(QtCore.QRect(40, 70, 251, 20))
    self.progressBar.setMaximum(0)
    self.progressBar.setProperty("value", 0)
    self.progressBar.setInvertedAppearance(False)
    self.progressBar.setObjectName("progressBar")
    self.pushButtonStopp = QtWidgets.QPushButton(Dialog)
    self.pushButtonStopp.setGeometry(QtCore.QRect(167, 100, 161, 27))
    font = QtGui.QFont()
    font.setFamily("Arial")
    font.setPointSize(10)
    font.setBold(False)
    font.setItalic(False)
    font.setWeight(50)
    self.pushButtonStopp.setFont(font)
    self.pushButtonStopp.setObjectName("pushButtonStopp")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Bitte warten..."))
    self.label.setText(_translate("Dialog", "Berechung läuft. Bitte warten..."))
    self.pushButtonStopp.setText(_translate("Dialog", "Berechnung abbrechen"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

