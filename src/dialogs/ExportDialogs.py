'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 06.11.2017

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from PyQt5 import QtWidgets, QtCore  # @UnresolvedImport

from dialogs.PyGraphEdit_Export import Ui_Dialog as Ui_Export  # @UnresolvedImport
from dialogs.PyGraphEdit_Graph6toDatabase import Ui_Dialog as Ui_Graph6toDB  # @UnresolvedImport
from dialogs.PyGraphEdit_SVGexport import Ui_Dialog as Ui_SVGexport  # @UnresolvedImport
from dialogs.PyGraphEdit_Tikzexport import Ui_Dialog as Ui_Tikzexport  # @UnresolvedImport


# ===========================================================================
class SvgExportDialog(QtWidgets.QDialog, Ui_SVGexport):
  '''Export the current graph as a svg-picture'''
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self, config):
    super(SvgExportDialog, self).__init__(None)
    self.setupUi(self)

    self.config = config

    # set local variables
    self.vertexSize = 7.0
    self.edgeSize = 0.5
    self.fillingColor = "blue"
    self.colorLine = "blue"
    self.edgeColor = "black"
    self.fillingColorMarked = "red"
    self.colorLineMarked = "red"
    self.edgeColorMarked = "red"
    self.file = ""
    self.showVertexNum = True
    self.simStepTime = 1.5
    self.outputfile = None

    # show the default values in the dialog
    self.lineEditVertexSize.setText(str(self.vertexSize))
    self.lineEditEdgeSize.setText(str(self.edgeSize))

    # set the slots
    self.pushButtonClose.clicked.connect(self.onCancel)
    self.pushButtonOk.clicked.connect(self.onOk)
    self.pushButtonFile.clicked.connect(self.onFile)
    self.checkBoxOwnColors.stateChanged.connect(self.changeColor)

  def changeColor(self):
    '''Activate or deactivate the color comboboxes'''
    self.comboBoxVertexColorFilling.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorLine.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxEdgeColor.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorFillingMarked.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorLineMarked.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxEdgeColorMarked.setEnabled(not self.checkBoxOwnColors.isChecked())

  def show(self):
    '''Overwrites the show-function'''
    self.checkBoxShowVertexNum.setChecked(self.showVertexNum)
    super(SvgExportDialog, self).show()

  def onFile(self):
    '''Save the picture'''
    self.outputfile, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                     'Save',
                                                                     self.config["Paths"]["output_dir"],
                                                                     "svg-files (*.svg)")
    if not self.outputfile or len(self.outputfile) == 0:
      return

    if "svg" in ffilter and ".svg" not in self.outputfile:
      # Add the correct filetype
      self.outputfile += ".svg"

    self.lineEditFile.setText(str(self.outputfile))

  def onOk(self):
    '''Start export svg-file'''
    # set the variables
    self.vertexSize = float(self.lineEditVertexSize.text())
    self.edgeSize = float(self.lineEditEdgeSize.text())
    self.fillingColor = self.comboBoxVertexColorFilling.currentText()
    self.colorLine = self.comboBoxVertexColorLine.currentText()
    self.edgeColor = self.comboBoxEdgeColor.currentText()
    self.fillingColorMarked = self.comboBoxVertexColorFillingMarked.currentText()
    self.colorLineMarked = self.comboBoxVertexColorLineMarked.currentText()
    self.edgeColorMarked = self.comboBoxEdgeColorMarked.currentText()
    self.file = self.lineEditFile.text()
    self.showVertexNum = self.checkBoxShowVertexNum.isChecked()
    self.simStepTime = self.lineEditSimStepTime.text()

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ============================================================================
class TikzExportDialog(QtWidgets.QDialog, Ui_Tikzexport):
  '''Export the current graph a TikZ-file'''
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self, config):
    super(TikzExportDialog, self).__init__(None)
    self.setupUi(self)

    self.config = config

    # set local variables
    self.vertexSize = 3.0
    self.edgeSize = 0.5
    self.fillingColor = "blue"
    self.colorLine = "blue"
    self.edgeColor = "black"
    self.fillingColorMarked = "red"
    self.colorLineMarked = "red"
    self.edgeColorMarked = "red"
    self.file = ""
    self.caption = ""
    self.label = ""
    self.edgesBackground = True
    self.scale = 1.0
    self.showVertexNum = True
    self.fontSize = 8
    self.outputfile = None

    # show the default values in the dialog
    self.lineEditVertexSize.setText(str(self.vertexSize))
    self.lineEditFontSize.setText(str(self.fontSize))
    self.lineEditScale.setText(str(self.scale))
    self.lineEditEdgeSize.setText(str(self.edgeSize))

    # set the slots
    self.pushButtonClose.clicked.connect(self.onCancel)
    self.pushButtonOk.clicked.connect(self.onOk)
    self.pushButtonFile.clicked.connect(self.onFile)

    self.checkBoxCaption.stateChanged.connect(self.changeCaption)
    self.checkBoxLabel.stateChanged.connect(self.changeLabel)
    self.checkBoxOwnColors.stateChanged.connect(self.changeColor)

  def changeColor(self):
    '''Activate or deactivate the color comboboxes'''
    self.comboBoxVertexColorFilling.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorLine.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxEdgeColor.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorFillingMarked.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxVertexColorLineMarked.setEnabled(not self.checkBoxOwnColors.isChecked())
    self.comboBoxEdgeColorMarked.setEnabled(not self.checkBoxOwnColors.isChecked())

  def changeCaption(self):
    '''Show and hide the caption-line if the checkbox is clicked'''
    self.lineEditCaption.setEnabled(self.checkBoxCaption.isChecked())

  def changeLabel(self):
    '''Show and hide the label-line if the checkbox is clicked'''
    self.lineEditLabel.setEnabled(self.checkBoxLabel.isChecked())

  def show(self):
    '''Overwrites the show-function'''
    self.lineEditScale.setText("%.3f" % self.scale)
    self.checkBoxShowVertexNum.setChecked(self.showVertexNum)
    super(TikzExportDialog, self).show()

  def onFile(self):
    '''Save the picture'''
    self.outputfile, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                     'Save',
                                                                     self.config["Paths"]["output_dir"],
                                                                     "tex-files (*.tex)")

    if not self.outputfile or len(self.outputfile) == 0:
      return

    if "tex" in ffilter and ".tex" not in self.outputfile:
      # Add the correct filetype
      self.outputfile += ".tex"

    self.lineEditFile.setText(str(self.outputfile))

  def onOk(self):
    '''Start export TikZ-file'''
    # set the variables
    self.vertexSize = float(self.lineEditVertexSize.text())
    self.fontSize = float(self.lineEditFontSize.text())
    self.scale = float(self.lineEditScale.text())
    self.edgeSize = float(self.lineEditEdgeSize.text())
    self.file = self.lineEditFile.text()
    self.caption = self.lineEditCaption.text()
    self.label = self.lineEditLabel.text()
    self.edgesBackground = self.checkBoxEdgesBackground.isChecked()
    self.showVertexNum = self.checkBoxShowVertexNum.isChecked()

    if self.checkBoxOwnColors.isChecked():
      self.fillingColor = None
      self.colorLine = None
      self.edgeColor = None
      self.fillingColorMarked = None
      self.colorLineMarked = None
      self.edgeColorMarked = None
    else:
      self.fillingColor = self.comboBoxVertexColorFilling.currentText()
      self.colorLine = self.comboBoxVertexColorLine.currentText()
      self.edgeColor = self.comboBoxEdgeColor.currentText()
      self.fillingColorMarked = self.comboBoxVertexColorFillingMarked.currentText()
      self.colorLineMarked = self.comboBoxVertexColorLineMarked.currentText()
      self.edgeColorMarked = self.comboBoxEdgeColorMarked.currentText()

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ============================================================================
class Graph6toDBDialog(QtWidgets.QDialog, Ui_Graph6toDB):
  '''Export the current graph a TikZ-file'''
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self, config):
    super(Graph6toDBDialog, self).__init__(None)
    self.setupUi(self)

    self.config = config
    self.fname = None
    self.outputfile = None
    self.graph6File = None
    self.DBfile = None

    # set the slots
    self.pushButtonClose.clicked.connect(self.onCancel)
    self.pushButtonOk.clicked.connect(self.onOk)
    self.pushButtonGraph6File.clicked.connect(self.onGraph6File)
    self.pushButtonDBFile.clicked.connect(self.onDBFile)

  def show(self):
    self.pushButtonClose.setEnabled(True)
    self.pushButtonOk.setEnabled(True)

    super(Graph6toDBDialog, self).show()

  def onGraph6File(self):
    '''Load the file with the graph6-strings'''
    self.fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',
                                                          self.config["Paths"]["data_dir"],
                                                          "Textdatei mit graph6-strings (*)")

    if self.fname is None or len(self.fname) == 0:
      return

    self.lineEditGraph6file.setText(str(self.fname))

  def onDBFile(self):
    '''Database to save the graphs'''
    self.outputfile, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                     'Save',
                                                                     self.config["Paths"]["output_dir"],
                                                                     "database-files (*.db)")

    if not self.outputfile or len(self.outputfile) == 0:
      return

    if "db" in ffilter and ".db" not in self.outputfile:
      # Add the correct filetype
      self.outputfile += ".db"

    self.lineEditDBFile.setText(str(self.outputfile))

  def onOk(self):
    '''Start export TikZ-file'''
    # set the variables
    self.graph6File = self.lineEditGraph6file.text()
    self.DBfile = self.lineEditDBFile.text()

    self.pushButtonClose.setEnabled(False)
    self.pushButtonOk.setEnabled(False)

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ===========================================================================
class ExportDialog(QtWidgets.QDialog, Ui_Export):
  '''Choose a program for the export'''
  graphviz = QtCore.pyqtSignal()
  qt = QtCore.pyqtSignal()
  intern = QtCore.pyqtSignal()
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self):
    super(ExportDialog, self).__init__(None)
    self.setupUi(self)

    self.vertexSize = None
    self.edgeSize = None
    self.fillingColor = None
    self.colorLine = None
    self.edgeColor = None
    self.fillingColorMarked = None
    self.colorLineMarked = None
    self.edgeColorMarked = None
    self.file = None
    self.showVertexNum = None

    # set the slots
    self.pushButtonGraphviz.clicked.connect(self.onGraphviz)
    self.pushButtonQt.clicked.connect(self.onQt)
    self.pushButtonIntern.clicked.connect(self.onIntern)

  def show(self, usableButtons=["graphivz", "intern", "Qt"]):
    if "graphivz" in usableButtons:
      self.pushButtonGraphviz.setEnabled(True)
    else:
      self.pushButtonGraphviz.setEnabled(False)
    if "Qt" in usableButtons:
      self.pushButtonQt.setEnabled(True)
    else:
      self.pushButtonQt.setEnabled(False)
    if "intern" in usableButtons:
      self.pushButtonIntern.setEnabled(True)
    else:
      self.pushButtonIntern.setEnabled(False)
    super(ExportDialog, self).show()

  def onGraphviz(self):
    '''Use graphviz'''
    self.graphviz.emit()
    self.close()

  def onQt(self):
    '''Use Qt'''
    self.qt.emit()
    self.close()

  def onIntern(self):
    '''Use an intern method'''
    self.intern.emit()
    self.close()

  def onOk(self):
    '''Start export svg-file'''
    # set the variables
    self.vertexSize = float(self.lineEditVertexSize.text())
    self.edgeSize = float(self.lineEditEdgeSize.text())
    self.fillingColor = self.comboBoxVertexColorFilling.currentText()
    self.colorLine = self.comboBoxVertexColorLine.currentText()
    self.edgeColor = self.comboBoxEdgeColor.currentText()
    self.fillingColorMarked = self.comboBoxVertexColorFillingMarked.currentText()
    self.colorLineMarked = self.comboBoxVertexColorLineMarked.currentText()
    self.edgeColorMarked = self.comboBoxEdgeColorMarked.currentText()
    self.file = self.lineEditFile.text()
    self.showVertexNum = self.checkBoxShowVertexNum.isChecked()

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()
