# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Display.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(354, 97)
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("E:/workspace/HD_Steg/symbole/2075.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Dialog.setWindowIcon(icon)
    self.pushButtonOK = QtWidgets.QPushButton(Dialog)
    self.pushButtonOK.setGeometry(QtCore.QRect(260, 60, 75, 23))
    self.pushButtonOK.setObjectName("pushButtonOK")
    self.label_2 = QtWidgets.QLabel(Dialog)
    self.label_2.setGeometry(QtCore.QRect(20, 20, 141, 16))
    font = QtGui.QFont()
    font.setPointSize(9)
    font.setBold(False)
    font.setWeight(50)
    self.label_2.setFont(font)
    self.label_2.setObjectName("label_2")
    self.comboBoxDraw = QtWidgets.QComboBox(Dialog)
    self.comboBoxDraw.setGeometry(QtCore.QRect(190, 20, 69, 22))
    self.comboBoxDraw.setObjectName("comboBoxDraw")
    self.comboBoxDraw.addItem("")
    self.comboBoxDraw.addItem("")
    self.comboBoxDraw.addItem("")
    self.comboBoxDraw.addItem("")
    self.comboBoxDraw.addItem("")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)
    Dialog.setTabOrder(self.comboBoxDraw, self.pushButtonOK)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Darstellungsart"))
    self.pushButtonOK.setText(_translate("Dialog", "OK"))
    self.label_2.setText(_translate("Dialog", "Darstellungsart"))
    self.comboBoxDraw.setItemText(0, _translate("Dialog", "twopi"))
    self.comboBoxDraw.setItemText(1, _translate("Dialog", "neato"))
    self.comboBoxDraw.setItemText(2, _translate("Dialog", "circo"))
    self.comboBoxDraw.setItemText(3, _translate("Dialog", "fdp"))
    self.comboBoxDraw.setItemText(4, _translate("Dialog", "dot"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

