<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB" sourcelanguage="de">
<context>
    <name>Dialog</name>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="81"/>
        <source>Grapheninfos</source>
        <translation>Graph infos</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="458"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="83"/>
        <source>Dominationszuverlässigkeitspolynom:</source>
        <translation>Domination reliability polynomial:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="84"/>
        <source>Benötigte Zeit:</source>
        <translation>required time:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="124"/>
        <source>Anzahl der Kanten</source>
        <translation>Number of edges</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="167"/>
        <source>Durchmesser</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="168"/>
        <source>Radius</source>
        <translation>Radius</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="69"/>
        <source>Anzahl der Knoten</source>
        <translation>Number of vertices</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="170"/>
        <source>Minimalgrad</source>
        <translation>minimum degree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="171"/>
        <source>Maximalgrad</source>
        <translation>maximum degree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="172"/>
        <source>Ã¸ Knotenabstand</source>
        <translation>average vertex distance</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="173"/>
        <source>Komponentenanzahl</source>
        <translation>number of components</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LadenDialog.py" line="125"/>
        <source>Graph laden</source>
        <translation>Load graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="121"/>
        <source>Graph zum Laden</source>
        <translation>Load graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="457"/>
        <source>Abbrechen</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Wait.py" line="44"/>
        <source>Bitte warten...</source>
        <translation>Please wait ...</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Wait.py" line="45"/>
        <source>Berechung läuft. Bitte warten...</source>
        <translation>Calculation is running. Please wait ...</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Wait.py" line="46"/>
        <source>Berechnung abbrechen</source>
        <translation>Cancel calculation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="179"/>
        <source>Einstellungen</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="182"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="183"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="184"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="185"/>
        <source>Critical</source>
        <translation>Critical</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="186"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="187"/>
        <source>Loglevel</source>
        <translation>Loglevel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="188"/>
        <source>Logdatei</source>
        <translation>Logfile</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="189"/>
        <source>Sprache</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="190"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wird erst beim Neustart aktiv&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Activated not before restart&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="191"/>
        <source>Deutsch</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="192"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="193"/>
        <source>Allgemein</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="194"/>
        <source>Graphviz verwenden</source>
        <translation>Use Graphviz</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="406"/>
        <source>Knotengröße</source>
        <translation>Size of vertices</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="454"/>
        <source>Knotennummern anzeigen</source>
        <translation>Show vertex numbers</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="198"/>
        <source>Darstellung</source>
        <translation>Design</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="199"/>
        <source>Startknotengewicht</source>
        <translation>Default vertex weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="200"/>
        <source>Startkantengewicht</source>
        <translation>Default edge weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="201"/>
        <source>Knotengewichte anzeigen</source>
        <translation>Show vertex weights</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="202"/>
        <source>Kantengewichte anzeigen</source>
        <translation>Show edge weights</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Config.py" line="203"/>
        <source>Gewichte</source>
        <translation>Weights</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="79"/>
        <source>Test auf Graphenklassen</source>
        <translation>Test on graph classes</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="80"/>
        <source>Schließen</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="81"/>
        <source>Graphenklassen</source>
        <translation>Graph classes</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="82"/>
        <source>Vollständiger Graph</source>
        <translation>Complete Graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="83"/>
        <source>bipartiter Graph</source>
        <translation>bipartite graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="84"/>
        <source>vollständig bipartiter Graph</source>
        <translation>Complete bipartite graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="85"/>
        <source>k-Baum</source>
        <translation>k-tree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="86"/>
        <source>k-Weg</source>
        <translation>k-path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="87"/>
        <source>einfacher k-Weg</source>
        <translation>simple k-path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="62"/>
        <source>Knoteneigenschaften</source>
        <translation>Vertex properties</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Kanteninfo.py" line="49"/>
        <source>Eigenschaften</source>
        <translation>Properties</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="64"/>
        <source>Knotengrad</source>
        <translation>Degree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="66"/>
        <source>Gesamtdistanz</source>
        <translation>Total distance</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="67"/>
        <source>Knotennummer</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="68"/>
        <source>Knotengewicht</source>
        <translation>Weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Kanteninfo.py" line="50"/>
        <source>Startknoten</source>
        <translation>Start vertex</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Kanteninfo.py" line="51"/>
        <source>Endknoten</source>
        <translation>End vertex</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Kanteninfo.py" line="52"/>
        <source>Kantengewicht</source>
        <translation>edge weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="88"/>
        <source>Baum</source>
        <translation>Tree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="89"/>
        <source>Weg</source>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="90"/>
        <source>regulärer Graph</source>
        <translation>regular graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="91"/>
        <source>planarer Graph</source>
        <translation>planar graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="92"/>
        <source>eulerscher Graph</source>
        <translation>Eulerian graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="93"/>
        <source>chordaler Graph</source>
        <translation>chordal graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="87"/>
        <source>Copy to Clipboard</source>
        <translation>Copy to clipboard</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="86"/>
        <source>Copy</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="88"/>
        <source>Copy LaTeX</source>
        <translation>Copy LaTeX</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="89"/>
        <source>Show Plot</source>
        <translation>Show plot</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="174"/>
        <source>Density</source>
        <translation>Density</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="175"/>
        <source>Graph6</source>
        <translation>Graph6</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="119"/>
        <source>Knotenanzahl zum filtern eingeben oder löschen für frei.</source>
        <translation>Number of vertices for filtering or free for no filter.</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LadenDialog.py" line="129"/>
        <source>Einschränken</source>
        <translation>Restrict</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="125"/>
        <source>Kantenanzahl zum filtern eingeben oder löschen für frei.</source>
        <translation>Number of edges for filtering or free for no filter.</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="126"/>
        <source>Graph löschen</source>
        <translation>Delete graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="127"/>
        <source>Darstellungsart</source>
        <translation>Drawing mode</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LadenDialog.py" line="136"/>
        <source>Alle anzeigen</source>
        <translation>Show all</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="128"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="405"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="130"/>
        <source>Vorschau anzeigen</source>
        <translation>Show preview</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Graphclasses.py" line="94"/>
        <source>selbstzentraler Graph</source>
        <translation>self-centered graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="64"/>
        <source>Produktgraph</source>
        <translation>Product graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="65"/>
        <source>Produktart</source>
        <translation>Product type</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="66"/>
        <source>Erster Graph</source>
        <translation>First graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="68"/>
        <source>Zweiter Graph</source>
        <translation>Second graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_ProductGraph.py" line="70"/>
        <source>Zeichenstil</source>
        <translation>Drawing style</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Close.py" line="34"/>
        <source>Achtung</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Close.py" line="35"/>
        <source>Beenden</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Close.py" line="37"/>
        <source>Speichern und Schließen</source>
        <translation>Save and close</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Close.py" line="38"/>
        <source>Wollen Sie das Programm wirklich beenden?</source>
        <translation>Do you really want to exit the program?</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Display.py" line="47"/>
        <source>twopi</source>
        <translation>twopi</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Display.py" line="48"/>
        <source>neato</source>
        <translation>neato</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Display.py" line="49"/>
        <source>circo</source>
        <translation>circo</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Display.py" line="50"/>
        <source>fdp</source>
        <translation>fdp</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Display.py" line="51"/>
        <source>dot</source>
        <translation>dot</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_TestStatus.py" line="54"/>
        <source>Tests running ...</source>
        <translation>Test running ...</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Info.py" line="176"/>
        <source>Number of triangles</source>
        <translation>Number of triangles</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="410"/>
        <source>Kantenstärke</source>
        <translation>edge size</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Knoteninfo.py" line="65"/>
        <source>Exzentrizität</source>
        <translation>Eccentricity</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Kanteninfo.py" line="48"/>
        <source>Kanteneigenschaften</source>
        <translation>Edge properties</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="118"/>
        <source>Startknoten:</source>
        <translation>Start vertex:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="119"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Knotenliste in der Form: 1,2,3,10&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>List of vertices in the form: 1,2,3,10</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="120"/>
        <source>Ausbreitungswahrscheinlickeit p:</source>
        <translation>Diffusion probability p:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="121"/>
        <source>0.2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="122"/>
        <source>Maximale Anzahl von Schritten:</source>
        <translation>Maximal number of cuts:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="123"/>
        <source>100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="124"/>
        <source>Ausbreitungsart</source>
        <translation>Diffusion type</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="125"/>
        <source>Einzelnen Nachbarn mit Wahrscheinlickeit p informieren</source>
        <translation>Inform a single neighbor with probability p</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="126"/>
        <source>Ganze Nachbarschaft mit Wahrscheinlichkeit p informieren</source>
        <translation>Inform the whole neighborhood with probability p</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="127"/>
        <source>Einen zufälligen Nachbarn von jedem Knoten pro Zeitschritt</source>
        <translation>A random neighbor from every vertex per time step</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="128"/>
        <source>Sendungswiederholung</source>
        <translation>Repeated sending</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="129"/>
        <source>Information einmal senden</source>
        <translation>Send information once</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="130"/>
        <source>Information in jedem Zeitschritt senden</source>
        <translation>Send the information in every time step</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Broadcasting.py" line="131"/>
        <source>Hidden-Node</source>
        <translation>Hidden-Node</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="95"/>
        <source>Ergebnisse vergleichen</source>
        <translation>Compare results</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="96"/>
        <source>Ergebnisdateien</source>
        <translation>Result files</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="98"/>
        <source>Vergleichswert</source>
        <translation>Reference value</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="99"/>
        <source>time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="100"/>
        <source>time (with result)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="101"/>
        <source>result</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_CompareResults.py" line="102"/>
        <source>Erzeuge LaTeX-Tabelle</source>
        <translation>Generate LaTeX-table</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="130"/>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="131"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="245"/>
        <source>Zu ändernde Kanten:</source>
        <translation>Edges for changing:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="246"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kantenliste in der Form: 1,2;2,3&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;List of the edges in the form: 1,2;2,3&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="247"/>
        <source>Alle Kanten ändern</source>
        <translation>Change all Edges</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="248"/>
        <source>Festes Gewicht:</source>
        <translation>Fixed weight:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="249"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Entweder festen Wert oder zufälliger Wert aus Intervall (für [0,1]: 0-1)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choose a fixed value or a random value from an interval (for [0,1]: 0-1)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="250"/>
        <source>Euklidische Distanz</source>
        <translation>Euclidean distance</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="260"/>
        <source>fallend mit Abstand</source>
        <translation>decreasing with distance</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="252"/>
        <source>Exponenielles Gewicht</source>
        <translation>Exponential weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="262"/>
        <source>Randwerte:</source>
        <translation>Boundary values:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="265"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="258"/>
        <source>= b * exp(-a *</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="256"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="259"/>
        <source>)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="261"/>
        <source>Lineares Gewicht</source>
        <translation>Linear weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="267"/>
        <source>= m *</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_EdgeWeights.py" line="268"/>
        <source>+ n</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Export.py" line="68"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Export.py" line="69"/>
        <source>Welches Programm soll zum Export verwendet werden?</source>
        <translation>Which program should be used for export?</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Export.py" line="70"/>
        <source>Graphviz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Export.py" line="71"/>
        <source>Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Export.py" line="72"/>
        <source>Intern</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_GenerateTest.py" line="154"/>
        <source>Generate Test</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_GenerateTest.py" line="155"/>
        <source>Database for generating</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="127"/>
        <source>Module:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="128"/>
        <source>Function:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="129"/>
        <source>Method:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_GenerateTest.py" line="160"/>
        <source>Output-file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_GenerateTest.py" line="162"/>
        <source>Generate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="118"/>
        <source>Graph und Simulation laden</source>
        <translation>Load graph and simulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadAnimationDialog.py" line="131"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="143"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="144"/>
        <source>1. Graph:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="146"/>
        <source>2. Graph:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="148"/>
        <source>Shift:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="149"/>
        <source>50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_LoadDialog2Graphs.py" line="150"/>
        <source>Laden</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SaveSimulation.py" line="110"/>
        <source>Simulation speichern</source>
        <translation>Save simulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SaveSimulation.py" line="111"/>
        <source>Name des Graphen</source>
        <translation>Name of the graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SaveSimulation.py" line="112"/>
        <source>Name der Simulation</source>
        <translation>Simulation name</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SaveSimulation.py" line="113"/>
        <source>In Datei vorhandene Graphen</source>
        <translation>Current graphs in file</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SaveSimulation.py" line="114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;In Datei vorhandene Simulationen&lt;br/&gt;(für ausgewählten Graphen)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Simulatioins in the file&lt;br/&gt;(for the choosen graph)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="124"/>
        <source>Start Test</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_StartTest.py" line="125"/>
        <source>Test-file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="316"/>
        <source>Simulations-Export</source>
        <translation>Export simulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="317"/>
        <source>Datei:</source>
        <translation>File:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="407"/>
        <source>mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="411"/>
        <source>pt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="414"/>
        <source>Farben wie dargestellt?</source>
        <translation>Use colors as displayed?</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="415"/>
        <source>Knotenfarbe - Füllung</source>
        <translation>Vertex color - filling</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="448"/>
        <source>black</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="326"/>
        <source>yellow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="449"/>
        <source>blue</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="450"/>
        <source>red</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="451"/>
        <source>gray</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="452"/>
        <source>magenta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="421"/>
        <source>Knotenfarbe - Linie</source>
        <translation>Vertex color - line</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="427"/>
        <source>Knotenfarbe - Füllung - markiert</source>
        <translation>Vertex color - filling - marked</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="433"/>
        <source>Knotenfarbe - Linie - markiert</source>
        <translation>Vertex color - line - marked</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="439"/>
        <source>Kantenfarbe</source>
        <translation>Edge color</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="447"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kantenfarbe falls Kante nicht markiert ist&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Color of the edge if the edge is not marked&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="446"/>
        <source>Kantenfarbe - markiert</source>
        <translation>Edge color - marked</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="455"/>
        <source>Kanten hinter oder vor den Knoten?</source>
        <translation>Edge behind or in front of the vertices?</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="365"/>
        <source>Zeit pro Simulationsschritt</source>
        <translation>Time per simulation step</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="366"/>
        <source>1.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_SVGexport.py" line="367"/>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_TestStatus.py" line="55"/>
        <source>Ergebnis speichern</source>
        <translation>Save result</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="403"/>
        <source>TikZ-Export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="404"/>
        <source>TeX-Datei:</source>
        <translation>TeX-file:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="408"/>
        <source>Schriftgröße</source>
        <translation>Font size</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="409"/>
        <source>Skalierungsfaktor</source>
        <translation>Scaling factor</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="412"/>
        <source>Caption</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="413"/>
        <source>Label</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Tikzexport.py" line="456"/>
        <source>Kanten im Hintergrund</source>
        <translation>Edges in the background</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="90"/>
        <source>x=1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="91"/>
        <source>x=-1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="92"/>
        <source>Substituieren</source>
        <translation>Substitute</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_Solution.py" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Syntax: x=-1 or x=y or x=y;z=x*y**2 or x=y+z&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Syntax: x=-1 or x=y or x=y;z=x*y**2 or x=y+z&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="PyGraphEdit_main.py" line="1197"/>
        <source>Weg</source>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1174"/>
        <source>Zufallsgraph</source>
        <translation>Random graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1191"/>
        <source>Stern</source>
        <translation>Star</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1198"/>
        <source>Produktgraph</source>
        <translation>Product graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1214"/>
        <source>einfacher k-Weg</source>
        <translation>simple k-path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1232"/>
        <source>zufälliger k-Weg</source>
        <translation>random k-path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1215"/>
        <source>k-Kreis</source>
        <translation>k-cycle</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1237"/>
        <source>k-Stern</source>
        <translation>k-star</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1211"/>
        <source>k-Kantengraph</source>
        <translation>k-line graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1201"/>
        <source>Induzierter Untergraph</source>
        <translation>induced subgraph</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="199"/>
        <source>Bipartition Polynom</source>
        <translation>Bipartition polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1194"/>
        <source>Dominationspolynom</source>
        <translation>Domination polynomial</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="236"/>
        <source>Matchingpolynom:</source>
        <translation>Matching polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="244"/>
        <source>Chromatisches Polynom:</source>
        <translation>Chromatic polynomial:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1185"/>
        <source>Beenden</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1129"/>
        <source>Operationen</source>
        <translation>Operations</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1130"/>
        <source>Knoten einfügen</source>
        <translation>Insert vertex</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1131"/>
        <source>Kante einfügen</source>
        <translation>Insert edge</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1132"/>
        <source>Kante kontrahieren</source>
        <translation>Contract</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1133"/>
        <source>Element markieren</source>
        <translation>Mark element</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1145"/>
        <source>Ansicht</source>
        <translation>View</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1146"/>
        <source>Darstellung aktualisieren</source>
        <translation>Update the picture</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1147"/>
        <source>Aktualisieren</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1148"/>
        <source>Darstellungsarten nach Graphviz</source>
        <translation>Display modes (Graphviz)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1149"/>
        <source>Darstellungsart:</source>
        <translation>Display mode:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1151"/>
        <source>Neu nummerieren</source>
        <translation>New numbering</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1142"/>
        <source>Markierung aller Knoten aufheben</source>
        <translation>Unmark all vertices</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1140"/>
        <source>markierte löschen</source>
        <translation>delete marked</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1143"/>
        <source>Markierungen aufheben</source>
        <translation>Unmark</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1137"/>
        <source>alle markieren</source>
        <translation>mark all</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1156"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1217"/>
        <source>Hilfe</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1158"/>
        <source>Analyse</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1159"/>
        <source>Zuverlässigkeiten</source>
        <translation>Reliabilities</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1160"/>
        <source>Externe Scripte</source>
        <translation>Extern scripts</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1161"/>
        <source>Graphenpolynome</source>
        <translation>Graph polynomials</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1171"/>
        <source>Erzeugen</source>
        <translation>Generate</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1172"/>
        <source>k-Graphen</source>
        <translation>k-graphs</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1177"/>
        <source>Manipulieren</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1178"/>
        <source>Gewichte ändern</source>
        <translation>Change weights</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1180"/>
        <source>Neu</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1181"/>
        <source>Öffnen</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1183"/>
        <source>Speichern</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1186"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1187"/>
        <source>Grapheninfos</source>
        <translation>Graph infos</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1190"/>
        <source>Kreis</source>
        <translation>Cycle</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1192"/>
        <source>Rad</source>
        <translation>Wheel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1193"/>
        <source>Vollst. Bipartiter Graph</source>
        <translation>Complete bipartite graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1189"/>
        <source>Vollständiger Graph</source>
        <translation>Complete graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1306"/>
        <source>Fächergraph</source>
        <translation>Fan</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1196"/>
        <source>Dominationszuverlässigkeit</source>
        <translation>Domination reliability</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1199"/>
        <source>Komplementgraph</source>
        <translation>Complement graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1200"/>
        <source>Kantengraph</source>
        <translation>Line graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1202"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1203"/>
        <source>Drucken</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1205"/>
        <source>PEDRel</source>
        <translation>PEDRel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1206"/>
        <source>EDRel-k</source>
        <translation>EDRel-k</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1207"/>
        <source>Unabhängigkeitszahl</source>
        <translation>Independence number</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1208"/>
        <source>Zentrum anzeigen</source>
        <translation>Show center</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1209"/>
        <source>Rand anzeigen</source>
        <translation>Show border</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1210"/>
        <source>Einstellungen</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1212"/>
        <source>Test auf Graphenklassen</source>
        <translation>Test on graph classes</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1216"/>
        <source>Median anzeigen</source>
        <translation>Show median</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1219"/>
        <source>Antikreis</source>
        <translation>Anticycle</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1220"/>
        <source>Maple-Script auführen</source>
        <translation>Execute Maple script</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1221"/>
        <source>Maple</source>
        <translation>Maple</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1222"/>
        <source>Sage</source>
        <translation>Sage</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1223"/>
        <source>Maxima</source>
        <translation>Maxima</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1224"/>
        <source>Gerüstanzahl</source>
        <translation>Number of spanning trees</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1225"/>
        <source>Rückgängig</source>
        <translation>undo</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1226"/>
        <source>Diamant</source>
        <translation>Diamond</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1229"/>
        <source>VEDRel</source>
        <translation>VEDRel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1230"/>
        <source>EDRel-A</source>
        <translation>EDRel-A</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1231"/>
        <source>SPEDRel</source>
        <translation>SPEDRel</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1233"/>
        <source>Baumzerlegung</source>
        <translation>Tree decomposition</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1234"/>
        <source>Wegzerlegung</source>
        <translation>Path decomposition</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1370"/>
        <source>TSP</source>
        <translation>TSP</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1235"/>
        <source>Knotengewichte</source>
        <translation>Vertex weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1236"/>
        <source>Kantengewichte</source>
        <translation>edge weight</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1238"/>
        <source>Export TikZ</source>
        <translation>Export TikZ</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1239"/>
        <source>Matchingpolynom</source>
        <translation>Matching polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1241"/>
        <source>Unabhänigkeitspolynom</source>
        <translation>Independence polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1243"/>
        <source>Subgraph-Polynom</source>
        <translation>Subgraph polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1245"/>
        <source>Chromatisches Polynom</source>
        <translation>Chromatic polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1248"/>
        <source>Tutte Polynom</source>
        <translation>Tutte polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1227"/>
        <source>Bipartitions Polynom</source>
        <translation>Bipartition polynomial</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="202"/>
        <source>Dominationspolynom:</source>
        <translation>Domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="205"/>
        <source>Totales Dominationspolynom:</source>
        <translation>Total domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="220"/>
        <source>Zusammenhangswahrscheinlichkeit:</source>
        <translation>Reliability:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="226"/>
        <source>Dominationszuverlässigkeitspolynom:</source>
        <translation>Domination reliability polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="157"/>
        <source>Unabhänigkeitspolynom:</source>
        <translation>Independence polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="169"/>
        <source>Subgraph enumerating polynomial:</source>
        <translation>Subgraph enumerating polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="190"/>
        <source>Unabhänigkeitszahl:</source>
        <translation>Independence number:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="193"/>
        <source>Cliquenzahl:</source>
        <translation>Number of cliques:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1252"/>
        <source>Subgraph Enumerating Polynom</source>
        <translation>Subgraph enumerating polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1253"/>
        <source>Max-Cliquen</source>
        <translation>max-cliques</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1254"/>
        <source>Cliquenzahl</source>
        <translation>Number of cliques</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1150"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Knoten neu nummerieren&lt;/p&gt;&lt;p&gt;beginnend mit 1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Renumber vertices&lt;/p&gt;&lt;p&gt;beginning with 1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1117"/>
        <source>Graphinfos</source>
        <translation>graph info</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1127"/>
        <source>Programinfos</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1135"/>
        <source>Markieren</source>
        <translation>Mark</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1256"/>
        <source>General Subgraph Counting Polynomial</source>
        <translation>General subgraph counting polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1260"/>
        <source>Splitgraph</source>
        <translation>Split graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1261"/>
        <source>Extended Subgraph Counting Poly</source>
        <translation>Extended subgraph counting polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1263"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1264"/>
        <source>Minimalgerüst</source>
        <translation>Minimal spanning tree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1265"/>
        <source>Maximalgerüst</source>
        <translation>Maximal spanning tree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1266"/>
        <source>Distanzmatrix</source>
        <translation>Distance matrix</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1284"/>
        <source>graph6</source>
        <translation>graph6</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1268"/>
        <source>Auf Updates prüfen</source>
        <translation>Check for updates</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1112"/>
        <source>Neuer Graph</source>
        <translation>New graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1114"/>
        <source>Graph speichern</source>
        <translation>Save graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1115"/>
        <source>In TikZ speichern</source>
        <translation>Save in TikZ</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1118"/>
        <source>Start Python-Script</source>
        <translation>Start Python script</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1152"/>
        <source>Programm beenden</source>
        <translation>Exit program</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1134"/>
        <source>Elemente verschieben</source>
        <translation>Shift elements</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1136"/>
        <source>Alle Knoten markieren</source>
        <translation>Mark all vertices</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1138"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1139"/>
        <source>Markierte Knoten löschen</source>
        <translation>Delete marked vertices</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1141"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1144"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1163"/>
        <source>Nachbarschaft</source>
        <translation>Neighborhood</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1244"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1188"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1271"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1213"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1218"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1184"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1204"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1272"/>
        <source>Alliance Polynom</source>
        <translation>Alliance polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1274"/>
        <source>Offene Nachbarschaft</source>
        <translation>Open neighborhood</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1275"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1276"/>
        <source>Totale Nachbarschaft</source>
        <translation>Total neighborhood</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1277"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1278"/>
        <source>Geschlossene Nachbarschaft</source>
        <translation>Closed neighborhood</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1279"/>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1291"/>
        <source>Queens graph</source>
        <translation>Queen graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1292"/>
        <source>Rooks graph</source>
        <translation>Rook graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1288"/>
        <source>Anzahl der kürzesten Wege</source>
        <translation>Number of shortest paths</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1255"/>
        <source>Totales Dominationspolynom</source>
        <translation>Total domination polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1258"/>
        <source>Trivariates totales Dominationspolynom</source>
        <translation>Trivariate total domination polynomial</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="211"/>
        <source>Trivariates totales Dominationspolynom:</source>
        <translation>Trivariate total domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="160"/>
        <source>General subgraph counting polynomial:</source>
        <translation>General subgraph counting polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="163"/>
        <source>Extended subgraph counting polynomial:</source>
        <translation>Extended subgraph counting polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="166"/>
        <source>Subgraph polynomial:</source>
        <translation>Subgraph polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="175"/>
        <source>Alliance polynomial:</source>
        <translation>Alliance polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="184"/>
        <source>Knotenzusammenhangszahl:</source>
        <translation>Vertex connectivity:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1164"/>
        <source>Wege</source>
        <translation>Paths</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1166"/>
        <source>Zentralität</source>
        <translation>Centrality</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1173"/>
        <source>Schachgraphen</source>
        <translation>Chess graphs</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1280"/>
        <source>Artikulationen</source>
        <translation>Articulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1281"/>
        <source>Kürzester Weg</source>
        <translation>Shortest path</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1282"/>
        <source>Erzeuge Testfile</source>
        <translation>Creating test file</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1283"/>
        <source>Starte Berechnung</source>
        <translation>Starting calculation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1285"/>
        <source>Vergleiche Ergebnisse</source>
        <translation>Comparing results</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1287"/>
        <source>Betweenness centrality</source>
        <translation>Betweenness centrality</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1289"/>
        <source>Knotenzusammenhangszahl</source>
        <translation>Vertex connectivity</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1293"/>
        <source>Bishop graph</source>
        <translation>Bishop graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1312"/>
        <source>Kantenwahrscheinlichkeit</source>
        <translation>Edge probability</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1313"/>
        <source>Anzahl der Kanten</source>
        <translation>Number of edges</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1302"/>
        <source>PageRank</source>
        <translation>Page rank</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="232"/>
        <source>Unabh. Dominationszuverlässigkeitspolynom:</source>
        <translation>Ind. domination reliability polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="172"/>
        <source>Subgraph component polynomial:</source>
        <translation>Subgraph component polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="178"/>
        <source>Irredundance Polynom:</source>
        <translation>Irredundance polynomial:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1120"/>
        <source>Bipartitionspolynom</source>
        <translation>Bipartition polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1121"/>
        <source>Unabhängigkeitspolynom</source>
        <translation>Independence polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1123"/>
        <source>Tuttepolynom</source>
        <translation>Tutte polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1125"/>
        <source>Markierungsfarbe</source>
        <translation>Color of the mark</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1154"/>
        <source>Eine Animation laden</source>
        <translation>Load an animation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1155"/>
        <source>Die Animation speichern</source>
        <translation>Save the animation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1167"/>
        <source>OGDF</source>
        <translation>OGDF</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1286"/>
        <source>Closeness centrality</source>
        <translation>Closeness centrality</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1294"/>
        <source>Vollst. k-partiter Graph</source>
        <translation>Complete k-partite graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1295"/>
        <source>Unabh. Dominationszuverlässigkeit</source>
        <translation>Ind. domination reliability</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1296"/>
        <source>Irredundance Polynom</source>
        <translation>Irredundance polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1297"/>
        <source>Planaritätstest (BM)</source>
        <translation>Test planarity (BM)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1298"/>
        <source>sdf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1299"/>
        <source>FMMM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1300"/>
        <source>OL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1301"/>
        <source>PSL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1303"/>
        <source>Subgraph Component Polynomial</source>
        <translation>Subgraph component polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1304"/>
        <source>Öffnen (zwei Graphen)</source>
        <translation>Open (two graphs)</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="151"/>
        <source>Längster isometrischer Kreis</source>
        <translation>Longest isometric cycle</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1311"/>
        <source>Geometrisch</source>
        <translation>Geometric</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1331"/>
        <source>Firecracker</source>
        <translation>Firecracker</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1332"/>
        <source>Banana tree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1325"/>
        <source>Kürzester Weg (A*)</source>
        <translation>Shortest path (A*)</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="196"/>
        <source>General domination Polynom</source>
        <translation>General domination polynomial</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="187"/>
        <source>Kantenzusammenhangszahl:</source>
        <translation>Edge connectivity:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1165"/>
        <source>Automatisierte Berechnung</source>
        <translation>Automated calculation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1168"/>
        <source>Minimale Schnitte</source>
        <translation>Min-cuts</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1175"/>
        <source>Bäume</source>
        <translation>Trees</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1343"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1182"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1195"/>
        <source>Alt+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1228"/>
        <source>Alt+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1240"/>
        <source>Alt+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1242"/>
        <source>Alt+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1246"/>
        <source>Alt+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1249"/>
        <source>Alt+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1251"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1257"/>
        <source>Alt+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1259"/>
        <source>Zusammenh. Dominationspolynom</source>
        <translation>Connected domination polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1262"/>
        <source>Alt+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1269"/>
        <source>Knotenfärbung</source>
        <translation>Vertex coloring</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1270"/>
        <source>Perfektes Dominationspolynom</source>
        <translation>Perfect domination polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1273"/>
        <source>Alt+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1290"/>
        <source>Unabh. Dominationspolynom</source>
        <translation>Independent domination polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1310"/>
        <source>Kantenfärbung</source>
        <translation>Edge coloring</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1314"/>
        <source>PDL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1315"/>
        <source>Planar draw layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1316"/>
        <source>MML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1317"/>
        <source>MixedModelLayout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1318"/>
        <source>SEFR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1319"/>
        <source>SpringEmbedderFR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1320"/>
        <source>GEM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1321"/>
        <source>GEMLayout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1322"/>
        <source>TreeLayout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1323"/>
        <source>Total Dominationszuverlässigkeit</source>
        <translation>Total domination reliability</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1324"/>
        <source>Extended cut polynomial</source>
        <translation>Extended cut polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1326"/>
        <source>Maximales Matching</source>
        <translation>Maximum matching</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1327"/>
        <source>Minimale Kantenüberdeckung</source>
        <translation>Minmal edge covering</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1328"/>
        <source>Min-Cut (Stoer und Wagner)</source>
        <translation>Min-Cut (Stoer and Wagner)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1329"/>
        <source>Ford-Fulkerson</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1330"/>
        <source>Min-st-Cut (Ford-Fulkerson)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1333"/>
        <source>Centipede</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1334"/>
        <source>zufälliger Baum</source>
        <translation>random tree</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1335"/>
        <source>zufälliger Baum (Pruefercode)</source>
        <translation>random tree (Pruefercode)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1336"/>
        <source>Kantenzusammenhangszahl</source>
        <translation>Edge connectivity</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1337"/>
        <source>Allg. Dominationspolynom</source>
        <translation>General domination polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1338"/>
        <source>Stress centrality</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1339"/>
        <source>J-Polynom</source>
        <translation>J-polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1342"/>
        <source>Erweitertes Matchingpolynom</source>
        <translation>Extended matching polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1344"/>
        <source>Breitensuche</source>
        <translation>Breadth-first search</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1345"/>
        <source>Tiefensuche</source>
        <translation>Depth-first search</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1346"/>
        <source>Anzahl der kantendisjunkten Wege</source>
        <translation>Number of edge disjoint paths</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1250"/>
        <source>Zusammenhangsw. (Kantenausfall)</source>
        <translation>Reliability (Edge failure)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1347"/>
        <source>Zusammenhangsw. (Knotenausfall)</source>
        <translation>Reliability (Vertex failure)</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1349"/>
        <source>Spektrum der Adjazenzmatrix</source>
        <translation>Spectrum of the adjacency matrix</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1350"/>
        <source>Spektrum der Laplacematrix</source>
        <translation>Spectrum of the laplace matrix</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="154"/>
        <source>Maximale Cliquen im Graphen</source>
        <translation>Maximal cliques in the graph</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="181"/>
        <source>Cut Polynom:</source>
        <translation>Cut polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="217"/>
        <source>Perfektes Dominationspolynom:</source>
        <translation>Perfect domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="238"/>
        <source>Erweitertes Matchingpolynom:</source>
        <translation>Extended matching polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="241"/>
        <source>Erweitertes Schnittpolynom:</source>
        <translation>Extended cut polynomial:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1169"/>
        <source>Spektrum</source>
        <translation>Spectrum</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1170"/>
        <source>Färbungen</source>
        <translation>Colorings</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1340"/>
        <source>Bipartite subgraph Polynom</source>
        <translation>Bipartite subgraph polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1348"/>
        <source>BarabÃ¡si-Albert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1351"/>
        <source>Powerlaw-Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1352"/>
        <source>Lobster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1353"/>
        <source>Regulärer Graph</source>
        <translation>Regular graph</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1354"/>
        <source>Broadcasting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1355"/>
        <source>Rainbowcoloring</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1356"/>
        <source>List-Rainbow-Coloring Weg Wahrscheinlichkeit</source>
        <translation>List rainbow coloring path reliabilty</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1357"/>
        <source>Anzahl rainbow-list-colorings Weg</source>
        <translation>Number of rainbow list colorings paths</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1358"/>
        <source>Edge-Cover-Polynom</source>
        <translation>Edges cover polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1359"/>
        <source>Vertex-Cover-Polynom</source>
        <translation>Vertex cover polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1360"/>
        <source>Cut Polynom</source>
        <translation>Cut polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1309"/>
        <source>Rising sun graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1367"/>
        <source>Friendship graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1368"/>
        <source>Book graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="141"/>
        <source>Stress-Zentralität</source>
        <translation>Stress centrality</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="144"/>
        <source>Betweenness-Zentralität</source>
        <translation>Betweenness centrality</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="208"/>
        <source>Zusammenhängendes Dominationspolynom:</source>
        <translation>Connected domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="214"/>
        <source>Unabhängiges Dominationspolynom:</source>
        <translation>Independent domination polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="223"/>
        <source>Zusammenhangswahrscheinlichkeit (Kantenausfall):</source>
        <translation>Reliability (edge failure):</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="229"/>
        <source>Totales Dominationszuverlässigkeitspolynom:</source>
        <translation>Total dominations reliability polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="247"/>
        <source>Rainbow polynomial:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="250"/>
        <source>Rainbow generating function:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="253"/>
        <source>Dominated Partition Poly_d:</source>
        <translation>Dominated partition polynomial_d:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="256"/>
        <source>Vertex-Cover-Polynom:</source>
        <translation>Veretx cover polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="259"/>
        <source>Edge-Cover-Polynom:</source>
        <translation>Edge cover polynomial:</translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="262"/>
        <source>Rankpolynom:</source>
        <translation>Rank polynomial:</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1162"/>
        <source>Dominationspolynome</source>
        <translation>Domination polynomials</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1176"/>
        <source>Datenbank erzeugen</source>
        <translation>Generate database</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1307"/>
        <source>Sun-graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1308"/>
        <source>Sunlet-graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1341"/>
        <source>Dominated Partitions Poly_d</source>
        <translation>Dominated partitions polynomial_d</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1361"/>
        <source>Import OSM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1362"/>
        <source>Bugtracker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1363"/>
        <source>Aus graph6-file</source>
        <translation>From graph6 file</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1364"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Erzeugt eine Datenbank aus einer Datei mit graph6-strings.&lt;/p&gt;&lt;p&gt;Zum Beispiel von der Ausgabe von plantri.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generate a database from a file with graph6-strings&lt;/p&gt;&lt;p&gt;E.g. the output-file from plantri.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1365"/>
        <source>Rainbow Polynomial</source>
        <translation>Rainbow polynomial</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1366"/>
        <source>Rainbow generating function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1369"/>
        <source>Graceful labeling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../modules/consts.py" line="147"/>
        <source>Closeness-Zentralität</source>
        <translation>Closeness-centrality</translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1111"/>
        <source>PyGraphEdit v0.62</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PyGraphEdit_main.py" line="1247"/>
        <source>Rank Polynom</source>
        <translation>Rank polynomial</translation>
    </message>
</context>
<context>
    <name>PyGraphEdit</name>
    <message>
        <location filename="../PyGraphEdit.py" line="683"/>
        <source>Vor Benutzung der Simulationsfunktion den Graphen normalisieren!</source>
        <translation>Please normalize the graph before starting a simulation!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="706"/>
        <source>Wollen Sie den Graph wirklich ändern?

      Alle Simulationsdaten gehen dabei verloren!</source>
        <translation>Do you really want to change the graph?

      All simulation data will be lost!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1035"/>
        <source>Datei existiert nicht oder hat das falsche Format!</source>
        <translation>File does not exist or has the wrong format!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1266"/>
        <source>Graph wirklich überschreiben</source>
        <translation>Really overwrite graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1269"/>
        <source>Graphenname</source>
        <translation>Name of the graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="925"/>
        <source>Simulation wirklich überschreiben</source>
        <translation>Really overwrite the simulation?</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="928"/>
        <source>Simulationsname</source>
        <translation>Simulation name</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1302"/>
        <source>Speichern war erfolgreich</source>
        <translation>Saving was successful</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="995"/>
        <source>Wollen Sie den aktuellen Graph wirklich überschreiben?

        Alle nicht gespeicherten Änderungen gehen dabei verloren!</source>
        <translation>Do you really want to overwrite the excisting graph?

        All not saved changings will be lost!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1145"/>
        <source>Unbekanntes Dateiformat!</source>
        <translation>Unkknown file format!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1192"/>
        <source>Wollen Sie den aktuellen Graph wirklich überschreiben?

        Im gml-Format kann nur ein Graph pro Datei gespeichert werden!
        Wenn Sie mehr Graphen in einer Datei speichern wollen, dann
        speichern Sie bitte in einer Datenbank (.db) oder einer xml-Datei.</source>
        <translation>Do you really want to overwrite the current graph?
        
        In graphml format only one graph per file can be stored!
        If you wish to store more graphs, please use the database
        (.db) or xml format.</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1214"/>
        <source>Wollen Sie den aktuellen Graph wirklich überschreiben?

        Im graphml-Format kann nur ein Graph pro Datei gespeichert werden!
        Wenn Sie mehr Graphen in einer Datei speichern wollen, dann
        speichern Sie bitte in einer Datenbank (.db) oder einer xml-Datei.</source>
        <translation>Do you really want to overwrite the current graph?
        
        In graphml format only one graph per file can be stored!
        If you wish to store more graphs, please use the database
        (.db) or xml format.</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1252"/>
        <source>Name des Graphen</source>
        <translation>Name of the graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1247"/>
        <source>In Datei vorhandene Graphen</source>
        <translation>Current graphs in file</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1627"/>
        <source>Export war erfolgreich</source>
        <translation>Export was successful</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1685"/>
        <source>Wollen Sie das Programm wirklich beenden?

      Alle nicht gespeicherten Änderungen gehen dabei verloren!</source>
        <translation>Do you really want to close the program?

        All not saved changings will be lost!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1690"/>
        <source>Wollen Sie das Programm wirklich beenden?</source>
        <translation>Do you really want to exit the program?</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3154"/>
        <source>Anzahl der Knoten</source>
        <translation>Number of vertices</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3339"/>
        <source>Zeichenstil</source>
        <translation>Drawing style</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1742"/>
        <source>Weg</source>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="7350"/>
        <source>Achtung</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3437"/>
        <source>Keine gültige Zahl eingegeben</source>
        <translation>No valid number</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1779"/>
        <source>Vollständiger Graph</source>
        <translation>Complete graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1854"/>
        <source>Komplement eines Kreises</source>
        <translation>Complement of a cycle</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1889"/>
        <source>Kantenwahrscheinlichkeit</source>
        <translation>Edge probability</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2015"/>
        <source>Zufallsgraph</source>
        <translation>Random graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2265"/>
        <source>Keine gültige Knotenzahl angegeben</source>
        <translation>No valid vertex number</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2279"/>
        <source>Keine gültige Wahrscheinlichkeit angegeben</source>
        <translation>No valid probability</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="1947"/>
        <source>Anzahl der Kanten</source>
        <translation>Number of edges</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2206"/>
        <source>Keine gültige Kantenanzahl angegeben</source>
        <translation>No valid number of edges</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2005"/>
        <source>Adjazensfactor (0,...,1)</source>
        <translation>adjacency-factor</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2008"/>
        <source>Länge des Quadrats</source>
        <translation>Lenght of the square</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2040"/>
        <source>Kein gültiger Adjazensfactor angegeben</source>
        <translation>No valid adjacency-factor</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2052"/>
        <source>Keine gültige Länge des Quadrats angegeben</source>
        <translation>No valid length of the square</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2077"/>
        <source>Knotengrad</source>
        <translation>Degree</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2083"/>
        <source>Zufälliger d-regulärer Graph</source>
        <translation>Random d-regular graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2098"/>
        <source>Keine gültigen Werte angegeben</source>
        <translation>No valid value</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2170"/>
        <source>Anzahl der Kanten an neuen Knoten</source>
        <translation>Number of edges on the new vertex</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2129"/>
        <source>Barabasi-Albert-Graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2146"/>
        <source>Keine gültigen Werte angegeben (m &gt;= n)</source>
        <translation>No valid value (m &gt;= n)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2173"/>
        <source>Wahrscheinlichkeit für ein Dreieck</source>
        <translation>Probability of a triangle</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2252"/>
        <source>Powerlaw-cluster-Graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2242"/>
        <source>Kantenwahrscheinlichkeit (Kante an Rückgrat)</source>
        <translation>Edge probability (edge on backbone)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2245"/>
        <source>Kantenwahrscheinlichkeit (Kante ein Level nach Rückgrat)</source>
        <translation>Edge probability (edge on level after backbone)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2306"/>
        <source>Stern</source>
        <translation>Star</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2339"/>
        <source>Anzahl der Knoten (&gt;=4)</source>
        <translation>Number of vertices (&gt;=4)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2345"/>
        <source>Radgraph</source>
        <translation>Wheel graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2384"/>
        <source>Fächergraph</source>
        <translation>Fan graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2423"/>
        <source>Sun graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2462"/>
        <source>Sunlet graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2501"/>
        <source>Rising sun graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2533"/>
        <source>Anzahl der Flügel</source>
        <translation>Number of wings</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2538"/>
        <source>Friendship graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2570"/>
        <source>Anzahl der Seiten</source>
        <translation>Number of pages</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2575"/>
        <source>Book graph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2655"/>
        <source>Anzahl der Sterne</source>
        <translation>Number of stars</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2657"/>
        <source>Größe der Sterne</source>
        <translation>Size of the stars</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2616"/>
        <source>Firecracker</source>
        <translation>Firecracker</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2663"/>
        <source>Banana tree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2702"/>
        <source>Länge des Tausendfüßer</source>
        <translation>Length of the centipede</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2708"/>
        <source>Tausendfüßer</source>
        <translation>Centipede</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2794"/>
        <source>Zufälliger Baum</source>
        <translation>Random tree</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2829"/>
        <source>Prüfercode des zufälligen Baums</source>
        <translation>Pruefercode of the random tree</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2839"/>
        <source>Knoten in der ersten Knotenmenge</source>
        <translation>Vertices in the first set</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2841"/>
        <source>Knoten in der zweiten Knotenmenge</source>
        <translation>Vertices in the second set</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2847"/>
        <source>Bipartiter Graph</source>
        <translation>Bipartite graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="7350"/>
        <source>Keine gültige Knotenzahl eingegeben</source>
        <translation>No valid number of vertices</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2880"/>
        <source>Größe der Knotenmengen (getrennt durch Komma)</source>
        <translation>Size of the vertex sets (separate by comma)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2886"/>
        <source>k-partiter Graph</source>
        <translation>k-partite graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="2905"/>
        <source>Keine gültige Knotenzahlen eingegeben</source>
        <translation>No valid number of vertices</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3038"/>
        <source>einfacher k-Weg</source>
        <translation>simple k-path</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3081"/>
        <source>zufälliger k-Weg</source>
        <translation>random k-path</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3120"/>
        <source>k-Kreis</source>
        <translation>k-cycle</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3161"/>
        <source>k-Stern</source>
        <translation>k-star</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3195"/>
        <source>Size of the clique</source>
        <translation>Size of the clique</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3197"/>
        <source>Size of the independent set</source>
        <translation>Size of the independent set</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3203"/>
        <source>Splitgraph</source>
        <translation>Split graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3236"/>
        <source>Size of the queens graph</source>
        <translation>Size of the queen graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3240"/>
        <source>Queens graph</source>
        <translation>Queen graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3270"/>
        <source>Size of the rooks graph</source>
        <translation>Size of the rook graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3274"/>
        <source>Rooks graph</source>
        <translation>Rook graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3304"/>
        <source>Size of the bishops graph</source>
        <translation>Size of the bishop graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3308"/>
        <source>Bishops graph</source>
        <translation>Bishops graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3342"/>
        <source>Graph6</source>
        <translation>Graph6</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3356"/>
        <source>Keine gültiger graph6-String</source>
        <translation>No valid graph6 string</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3371"/>
        <source>Done</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3425"/>
        <source>Cliquengröße k</source>
        <translation>Size of the clique</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3428"/>
        <source>k-Kantengraph</source>
        <translation>k-line graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3456"/>
        <source>Induzierter Untergraph</source>
        <translation>induced subgraph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3460"/>
        <source>Keine Knoten markiert oder angegeben</source>
        <translation>No vertices marked or denoted</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3492"/>
        <source>Wegzerlegung des Graphen</source>
        <translation>Path decomposition of the graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3523"/>
        <source>Zu ändernde Knoten</source>
        <translation>Vertices to be modified</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3526"/>
        <source>Knotengewicht</source>
        <translation>Edge weight</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3530"/>
        <source>Knotengewichte ändern</source>
        <translation>Change vertex weights</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4470"/>
        <source>Keine gültigen Angaben</source>
        <translation>No valid inputs</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="7125"/>
        <source>Ungülitge Eingaben</source>
        <translation>None valid input</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4845"/>
        <source>Methode</source>
        <translation>Method</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3841"/>
        <source>Maximales Matching im Graphen:</source>
        <translation>Maximum matching:</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3858"/>
        <source>Maximum Matching</source>
        <translation>Maximum matching</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3895"/>
        <source>Min-cut:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3916"/>
        <source>Min-cut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3925"/>
        <source>Min-st-cut:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3936"/>
        <source>Bitte zwei Knoten für die Berechnung auswählen!</source>
        <translation>Please choose two vertices for the computing!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3957"/>
        <source>Min-st-cut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3966"/>
        <source>Minimum edge cover im Graphen:</source>
        <translation>Minimum edge cover:</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="3983"/>
        <source>Minimum edge cover</source>
        <translation>Minimum edge cover</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4051"/>
        <source>Bitte Knoten für Berechnung der Nachbarschaft auswählen!</source>
        <translation>Please choose vertices for computing the neighborhood!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4080"/>
        <source>Artikulationen des Graphen</source>
        <translation>Articulations of the graph</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4184"/>
        <source>Bitte zwei Knoten für die Berechnung des kürzesten Weges auswählen!</source>
        <translation>Please choose two vertices for computing the shortest path!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4122"/>
        <source>Kürzester Weg (Dijkstra)</source>
        <translation>Shortest path (Dijkstra)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4169"/>
        <source>Kürzester Weg (A*)</source>
        <translation>Shortest path (A*)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4202"/>
        <source>Anzahl der kürzesten Wege</source>
        <translation>Number of shortest paths</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4217"/>
        <source>Bitte zwei Knoten für die Berechnung der Anzahl auswählen!</source>
        <translation>Please choose two vertices for the computing!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4235"/>
        <source>Anzahl der kantendisjunkten Wege</source>
        <translation>Number of edge disjoint paths</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4247"/>
        <source>max. Größe der dom. Menge (k)</source>
        <translation>max. size of the dominating set (k)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4257"/>
        <source>Kein gültiges k angegeben</source>
        <translation>No valid k given</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4444"/>
        <source>Dominierende Knoten</source>
        <translation>Dominating vertices</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4446"/>
        <source>Distanz</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4447"/>
        <source>k (Dominationshäufigkeit)</source>
        <translation>k (domination frequency)</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4489"/>
        <source>Tutte polynomial:</source>
        <translation>Tutte polynomial:</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4519"/>
        <source>Gerüstanzahl:</source>
        <translation>Number of spanning trees:</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="4906"/>
        <source>Fehler beim Ausführen des externen Skripts.
                                                      Ausgabe in PyGraphEdit.log beachten</source>
        <translation>An error occured will running the extern script.
                                                      Please see PyGraphEdit.log</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5207"/>
        <source>Erzeugen des Testfiles erfolgreich!</source>
        <translation>Test file successfully created!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5211"/>
        <source>Erzeugen des Testfiles nicht erfolgreich!</source>
        <translation>Generation of the text file was not successful!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5270"/>
        <source>Speichern erfolgreich</source>
        <translation>Saving successful</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5293"/>
        <source>Bitte einen Knoten für den Start des BFS auswählen!</source>
        <translation>Please choose a vertex for the start of the algorithm!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5319"/>
        <source>Bitte einen Knoten für den Start des DFS auswählen!</source>
        <translation>Please choose a vertex for the start of the algorithm!</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="5870"/>
        <source>Einstellungen</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="6888"/>
        <source>Die Änderungen sind nach dem nächsten Neustart verfügbar.</source>
        <translation>The changings will be applied after a restart of PyGraphEdit.</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="6977"/>
        <source>Bitte Startknoten angeben</source>
        <translation>Please specify the start vertex</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="6989"/>
        <source>Unzulässige maximale Schrittanzahl</source>
        <translation>Not a valid maximal stet number</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="7000"/>
        <source>Unzulässige Wahrscheinlickeit</source>
        <translation>Not a valid probability</translation>
    </message>
    <message>
        <location filename="../PyGraphEdit.py" line="7426"/>
        <source>Kantengewicht hat falsches Zahlformat!</source>
        <translation>Edge weight has wrong number format!</translation>
    </message>
</context>
</TS>
