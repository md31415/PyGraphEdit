'''
Created on 03.11.2017

@author: markus
'''

from PyQt5 import QtWidgets, QtCore, QtGui  # @UnresolvedImport
import xml.dom.minidom as dom

from dialogs.PyGraphEdit_CompareResults import Ui_Dialog as Ui_CompareResults  # @UnresolvedImport
from dialogs.PyGraphEdit_GenerateTest import Ui_Dialog as Ui_GenerateTest  # @UnresolvedImport
from dialogs.PyGraphEdit_StartTest import Ui_Dialog as Ui_StartTest  # @UnresolvedImport
from dialogs.PyGraphEdit_TestStatus import Ui_Dialog as Ui_TestStatus  # @UnresolvedImport

from modules.consts import method  # @UnresolvedImport


# =====================================================================
class GenerateTestDialog(QtWidgets.QDialog, Ui_GenerateTest):
  '''Generate the testfile'''
  start = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self, globalsClass, config):
    super(GenerateTestDialog, self).__init__(None)
    self.setupUi(self)

    self.database = None
    self.outputfile = None
    self.module = None
    self.function = None
    self.method = None
    self.globalsClass = globalsClass
    self.config = config

    self.pushButtonCancel.clicked.connect(self.onCancel)
    self.pushButtonStart.clicked.connect(self.onStart)

    self.pushButtonFilemenu.clicked.connect(self.onDatabase)
    self.pushButtonOutput.clicked.connect(self.onOutputFile)

    self.comboBoxModules.currentIndexChanged.connect(self.__changeMethods)

    self.comboBoxModules.clear()
    self.comboBoxModules.addItems(globalsClass.IMPLEMENTEDMODULES)
    self.comboBoxModules.addItem("Other module")

  def __changeMethods(self):
    if self.comboBoxModules.currentText() == "Other module":
      self.comboBoxMethods.clear()
      self.comboBoxMethods.setEnabled(False)
      self.lineEditModule.setEnabled(True)
      self.lineEditFunction.setEnabled(True)
    else:
      self.comboBoxMethods.setEnabled(True)
      self.lineEditModule.setEnabled(False)
      self.lineEditFunction.setEnabled(False)
      self.lineEditFunction.setText("compute_poly")
      self.comboBoxMethods.clear()
      self.comboBoxMethods.addItems([m.name for m in self.globalsClass.METHODSFORMODULES[str(self.comboBoxModules.currentText())]])

  def onDatabase(self):
    '''Load a new file'''
    self.database, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',
                                                             self.config["Paths"]["data_dir"],
                                                             "Datenbank (*.db)")

    if self.database is None or len(self.database) == 0:
      return

    self.lineEditDatabase.setText(str(self.database))

  def onOutputFile(self):
    '''Save a graph'''
    self.outputfile, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                     'Save',
                                                                     self.config["Paths"]["test_dir"],
                                                                     "XML-files (*.xml)")
    if self.outputfile is None or len(self.outputfile) == 0:
      return

    if "xml" in ffilter and ".xml" not in self.outputfile:
      # Add the correct filetype
      self.outputfile += ".xml"

    self.lineEditTestfile.setText(str(self.outputfile))

  def onStart(self):
    '''Start generating test'''
    if self.comboBoxModules.currentText() == "Other module":
      self.module = str(self.lineEditModule.text())
    else:
      self.module = self.comboBoxModules.currentText()
      self.method = self.comboBoxMethods.currentText()

    self.function = str(self.lineEditFunction.text())
    self.outputfile = str(self.lineEditTestfile.text())
    self.database = str(self.lineEditDatabase.text())
    self.start.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# =====================================================================
class StartTestDialog(QtWidgets.QDialog, Ui_StartTest):
  '''Start the test dialog'''
  start = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self, globalsClass, config):
    super(StartTestDialog, self).__init__(None)
    self.setupUi(self)

    self.testfile = None
    self.module = None
    self.function = None
    self.method = None
    
    self.globalsClass = globalsClass
    self.config = config

    self.pushButtonCancel.clicked.connect(self.onCancel)
    self.pushButtonStart.clicked.connect(self.onStart)

    self.pushButtonFilemenu.clicked.connect(self.onTestFile)

  def onTestFile(self):
    '''Load a new file'''
    def get_infos(filename):
      '''Read infos from the test file'''
      try:
        tests = dom.parse(filename)
      except OSError.FileNotFoundError:
        print("%s file is missing" % filename)
        return
      except OSError.PermissionError:
        print("You are not allowed to read %s" % filename)
        return
      except:
        print("unable to load file %s" % filename)
        return

      try:
        testmodule = tests.firstChild.getAttribute("Module")
        testmethod = tests.firstChild.getAttribute("Method")
        testfunction = tests.firstChild.getAttribute("Function")
      except:
        return None, None, None
      return testmodule, testfunction, testmethod

    # Load a file
    self.testfile, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',
                                                             self.config["Paths"]["test_dir"],
                                                             "Test-file (*.xml)")

    if self.testfile is None or len(self.testfile) == 0:
      return

    # display the informations from the test file
    self.lineEditTestfile.setText(str(self.testfile))
    self.module, self.function, self.method = get_infos(self.testfile)
    self.lineEditModul.setText(str(self.module))
    self.lineEditFunction.setText(str(self.function))
    self.comboBoxMethods.clear()

    if self.module in self.globalsClass.IMPLEMENTEDMODULES:
      self.comboBoxMethods.setEnabled(True)
      self.comboBoxMethods.addItems([m.name for m in self.globalsClass.METHODSFORMODULES[self.module]])
    else:
      self.comboBoxMethods.setEnabled(False)

  def onStart(self):
    '''Start the test'''
    self.method = method[self.comboBoxMethods.currentText()]
    self.start.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# =====================================================================
class CompareResultsDialog(QtWidgets.QDialog, Ui_CompareResults):
  '''Compare two or more testresults'''
  start = QtCore.pyqtSignal()

  def __init__(self, globalsClass, config):
    super(CompareResultsDialog, self).__init__(None)
    self.setupUi(self)

    self.testFiles = ""
    self.latexOutputfile = None
    self.generateLatexFile = False
    self.key = "time"
    
    self.globalsClass = globalsClass
    self.config = config

    self.pushButtonCancel.clicked.connect(self.onCancel)
    self.pushButtonStart.clicked.connect(self.onStart)
    self.pushButtonFilemenu.clicked.connect(self.onFile)
    self.checkBoxLaTeX.clicked.connect(self.onEnableLatex)
    self.pushButtonFilemenuLaTeX.clicked.connect(self.onFileLatex)

    # Remove old files
    self.lineEditResultfile.clear()

  def onEnableLatex(self):
    '''Enable the LaTeX-file-line'''
    if self.checkBoxLaTeX.isChecked():
      self.lineEditLaTeX.setEnabled(True)
      self.pushButtonFilemenuLaTeX.setEnabled(True)
    else:
      self.lineEditLaTeX.setEnabled(False)
      self.pushButtonFilemenuLaTeX.setEnabled(False)

  def onFileLatex(self):
    '''Open the file-menu for a LaTeX-file'''
    self.latexOutputfile, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                          'Save',
                                                                          self.config["Paths"]["test_dir"],
                                                                          "TeX-files (*.tex)")

    if self.latexOutputfile is None or len(self.latexOutputfile) == 0:
      return

    if "tex" in ffilter and ".tex" not in self.latexOutputfile:
      # Add the correct filetype
      self.latexOutputfile += ".tex"

    self.lineEditLaTeX.setText(str(self.latexOutputfile))

  def onFile(self):
    '''Open the file-menu'''
    # Load a file
    self.testFiles, _ = QtWidgets.QFileDialog.getOpenFileNames(self,
                                                               'Open file',
                                                               self.config["Paths"]["test_dir"],
                                                               "Test-file (*.xml)")

    if self.testFiles is None or len(self.testFiles) == 0:
      return

    # display the informations from the test file
    self.lineEditResultfile.setText(str(self.testFiles))

  def onStart(self):
    '''Start'''
    self.key = self.comboBoxKey.currentText()
    self.generateLatexFile = self.checkBoxLaTeX.isChecked()
    self.start.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.close()


# =====================================================================
class TestStatusDialog(QtWidgets.QDialog, Ui_TestStatus):
  '''Shows the test status'''
  save = QtCore.pyqtSignal()

  def __init__(self):
    super(TestStatusDialog, self).__init__(None)
    self.setupUi(self)

    self.testum = None
    self.testfile = None
    self.module = None
    self.method = None
    self.returnValue = None

    self.pushButtonSave.clicked.connect(self.onSave)
    self.pushButtonOk.clicked.connect(self.onOk)

  def onOk(self):
    '''Close the dialog'''
    self.returnValue = "Ok"
    self.close()

  def onSave(self):
    '''Save the result'''
    self.save.emit()

  def writeStream(self, text):
    '''Append text to the QTextEdit'''
    cursor = self.textEditStatus.textCursor()
    cursor.movePosition(QtGui.QTextCursor.End)
    cursor.insertText(text)
    self.textEditStatus.setTextCursor(cursor)
    self.textEditStatus.ensureCursorVisible()

  def showResultButton(self):
    '''Show the result button'''
    self.pushButtonOk.setEnabled(True)
    self.pushButtonSave.setEnabled(True)

  def hideResultButton(self):
    '''Hide the result button'''
    self.pushButtonOk.setEnabled(False)
    self.pushButtonSave.setEnabled(False)
