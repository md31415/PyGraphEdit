# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_TestStatus.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(456, 299)
    self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
    self.verticalLayout.setObjectName("verticalLayout")
    self.textEditStatus = QtWidgets.QTextEdit(Dialog)
    self.textEditStatus.setReadOnly(True)
    self.textEditStatus.setObjectName("textEditStatus")
    self.verticalLayout.addWidget(self.textEditStatus)
    self.horizontalLayout = QtWidgets.QHBoxLayout()
    self.horizontalLayout.setObjectName("horizontalLayout")
    self.pushButtonSave = QtWidgets.QPushButton(Dialog)
    self.pushButtonSave.setEnabled(False)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonSave.sizePolicy().hasHeightForWidth())
    self.pushButtonSave.setSizePolicy(sizePolicy)
    self.pushButtonSave.setMinimumSize(QtCore.QSize(145, 0))
    self.pushButtonSave.setMaximumSize(QtCore.QSize(145, 16777215))
    self.pushButtonSave.setCheckable(False)
    self.pushButtonSave.setObjectName("pushButtonSave")
    self.horizontalLayout.addWidget(self.pushButtonSave)
    spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem)
    self.pushButtonOk = QtWidgets.QPushButton(Dialog)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonOk.sizePolicy().hasHeightForWidth())
    self.pushButtonOk.setSizePolicy(sizePolicy)
    self.pushButtonOk.setMinimumSize(QtCore.QSize(85, 0))
    self.pushButtonOk.setMaximumSize(QtCore.QSize(85, 16777215))
    self.pushButtonOk.setObjectName("pushButtonOk")
    self.horizontalLayout.addWidget(self.pushButtonOk)
    self.verticalLayout.addLayout(self.horizontalLayout)

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Tests running ..."))
    self.pushButtonSave.setText(_translate("Dialog", "Ergebnis speichern"))
    self.pushButtonOk.setText(_translate("Dialog", "OK"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

