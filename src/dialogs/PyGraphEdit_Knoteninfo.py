# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Knoteninfo.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(267, 251)
    self.groupBox = QtWidgets.QGroupBox(Dialog)
    self.groupBox.setGeometry(QtCore.QRect(30, 20, 211, 181))
    self.groupBox.setObjectName("groupBox")
    self.label = QtWidgets.QLabel(self.groupBox)
    self.label.setGeometry(QtCore.QRect(20, 60, 81, 16))
    self.label.setObjectName("label")
    self.lineEditKnotengrad = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditKnotengrad.setGeometry(QtCore.QRect(140, 60, 61, 20))
    self.lineEditKnotengrad.setReadOnly(True)
    self.lineEditKnotengrad.setObjectName("lineEditKnotengrad")
    self.lineEditExcentrizitaet = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditExcentrizitaet.setGeometry(QtCore.QRect(140, 120, 61, 20))
    self.lineEditExcentrizitaet.setReadOnly(True)
    self.lineEditExcentrizitaet.setObjectName("lineEditExcentrizitaet")
    self.label_2 = QtWidgets.QLabel(self.groupBox)
    self.label_2.setGeometry(QtCore.QRect(20, 120, 101, 16))
    self.label_2.setObjectName("label_2")
    self.lineEditGesDist = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditGesDist.setGeometry(QtCore.QRect(140, 150, 61, 20))
    self.lineEditGesDist.setReadOnly(True)
    self.lineEditGesDist.setObjectName("lineEditGesDist")
    self.label_3 = QtWidgets.QLabel(self.groupBox)
    self.label_3.setGeometry(QtCore.QRect(20, 150, 111, 16))
    self.label_3.setObjectName("label_3")
    self.lineEditNummer = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditNummer.setGeometry(QtCore.QRect(140, 30, 61, 20))
    self.lineEditNummer.setReadOnly(True)
    self.lineEditNummer.setObjectName("lineEditNummer")
    self.label_4 = QtWidgets.QLabel(self.groupBox)
    self.label_4.setGeometry(QtCore.QRect(20, 30, 111, 16))
    self.label_4.setObjectName("label_4")
    self.label_5 = QtWidgets.QLabel(self.groupBox)
    self.label_5.setGeometry(QtCore.QRect(20, 90, 111, 16))
    self.label_5.setObjectName("label_5")
    self.lineEditGewicht = QtWidgets.QLineEdit(self.groupBox)
    self.lineEditGewicht.setGeometry(QtCore.QRect(140, 90, 61, 20))
    self.lineEditGewicht.setReadOnly(False)
    self.lineEditGewicht.setObjectName("lineEditGewicht")
    self.pushClose = QtWidgets.QPushButton(Dialog)
    self.pushClose.setGeometry(QtCore.QRect(160, 210, 75, 23))
    self.pushClose.setObjectName("pushClose")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Knoteneigenschaften"))
    self.groupBox.setTitle(_translate("Dialog", "Eigenschaften"))
    self.label.setText(_translate("Dialog", "Knotengrad"))
    self.label_2.setText(_translate("Dialog", "Exzentrizität"))
    self.label_3.setText(_translate("Dialog", "Gesamtdistanz"))
    self.label_4.setText(_translate("Dialog", "Knotennummer"))
    self.label_5.setText(_translate("Dialog", "Knotengewicht"))
    self.pushClose.setText(_translate("Dialog", "OK"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

