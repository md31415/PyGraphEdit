'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.


Created on 06.11.2017
Last modified on 23.03.2018

@copyright: Markus Dod
@license: GPL (see LICENSE.txt)
'''

from PyQt5 import QtWidgets, QtCore, QtGui  # @UnresolvedImport
from random import uniform
from sqlite3 import connect, register_adapter, register_converter, PARSE_DECLTYPES  # @UnresolvedImport
import tempfile

from dialogs.formlayout import fedit  # @UnresolvedImport

from dialogs.PyGraphEdit_LadenDialog import Ui_Dialog as Laden_Dialog  # @UnresolvedImport
from dialogs.PyGraphEdit_LoadAnimationDialog import Ui_Dialog as Ui_LoadAnimation  # @UnresolvedImport
from dialogs.PyGraphEdit_LoadDialog2Graphs import Ui_Dialog as Ui_Load2Graphs  # @UnresolvedImport
from dialogs.PyGraphEdit_SaveSimulation import Ui_Dialog as Ui_SaveSimulation  # @UnresolvedImport
from dialogs.PyGraphEdit_LoadOSM import Ui_Dialog as Ui_LoadOSM  # @UnresolvedImport

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.GraphParser import (getGraphNamesdb, load_graphdb,  # @UnresolvedImport
                                getGraphNamesGraphml, getGraphNamesGML,  # @UnresolvedImport
                                getGraphNames, graphadapter,  # @UnresolvedImport
                                graphkonverter, dictadapter, dictkonverter,  # @UnresolvedImport
                                delete_graphdb)  # @UnresolvedImport

from lib.GraphEmbedding import springEmbedding  # @UnresolvedImport

from modules.consts import (PLOTMODES_GRAPHVIZ, PLOTMODES_INTERN)  # @UnresolvedImport
from modules.SaveLoadSimulation import getSimNames, deleteSimulations  # @UnresolvedImport
from modules.SVGExport import generateSVG  # @UnresolvedImport


class LoadDialog(QtWidgets.QDialog, Laden_Dialog):
  '''Load a graph from a SQLite-database or a XML-file.'''
  accept = QtCore.pyqtSignal()

  def __init__(self, globalsClass, config):
    super(LoadDialog,self).__init__(None)
    self.setupUi(self)

    self.fname = ""
    self.graphname = ""
    self.graphNames = ""
    self.db = False
    self.graphml = False
    self.gml = False
    self.globalsClass = globalsClass
    self.config = config

    # Slot einrichten
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonAbbrechen.clicked.connect(self.onClose)
    self.pushButtonAkt.clicked.connect(self.onAktualisieren)
    self.pushButtonDelete.clicked.connect(self.onDelete)
    self.comboBoxGraphen.activated.connect(self.__akt__)
    self.pushButtonShowAll.clicked.connect(self.onShowAll)
    self.pushButtonFile.clicked.connect(self.onFileDialog)
    self.checkBoxShowPreview.stateChanged.connect(self.__showPreview__)

    self.comboBoxDarstellung.clear()
    for mode in PLOTMODES_GRAPHVIZ:
      self.comboBoxDarstellung.addItem(mode)
    for mode in PLOTMODES_INTERN:
      self.comboBoxDarstellung.addItem(mode)

  def show(self, *args, **kwargs):
    '''Show the dialog'''
    if self.onFileDialog():
      return QtWidgets.QDialog.show(self, *args, **kwargs)
    else:
      return

  def __showPreview__(self):
    '''Enables or disables the preview'''
    if self.checkBoxShowPreview.isChecked():
      self.aktPicture()
    else:
      self.PictureLabel.clear()

  def __akt__(self):
    '''Load for the current graph the order, the size and the picture'''
    if self.db:
      connection = connect(self.fname, detect_types=PARSE_DECLTYPES)
      cursor = connection.cursor()

      register_adapter(SGraph, graphadapter)
      register_converter("SGraph", graphkonverter)
      register_adapter(dict, dictadapter)
      register_converter("dict", dictkonverter)

      item = str(self.comboBoxGraphen.currentText())
      try:
        cursor.execute("SELECT n, m FROM Graphen WHERE name='%s'" % item)
      except:
        print("Datei nicht vorhanden oder falsches Format __akt__")
        return None
      r = cursor.fetchall()
      if len(r) >= 1:
        n, m = r[0]
      else:
        n = None
        m = None

      cursor.close()

    else:
      n = None
      m = None

    self.lineEditKnoten.setText(str(n))
    self.lineEditKanten.setText(str(m))

    if self.checkBoxShowPreview.isChecked():
      self.aktPicture()
    else:
      self.PictureLabel.clear()

  def onAktualisieren(self):
    '''
    Update the graph list with the current values for
    the number of vertices and edges
    '''
    if self.db:
      connection = connect(self.fname, detect_types=PARSE_DECLTYPES)
      cursor = connection.cursor()

      register_adapter(SGraph, graphadapter)
      register_converter("SGraph", graphkonverter)
      register_adapter(dict, dictadapter)
      register_converter("dict", dictkonverter)

      try:
        n = int(self.lineEditKnoten.text())
      except:
        n = None
      try:
        m = int(self.lineEditKanten.text())
      except:
        m = None

      try:
        if n is not None and m is None:
          cursor.execute("SELECT name FROM Graphen WHERE n='%d'" % n)
          names = cursor.fetchall()
        elif n is None and m is not None:
          cursor.execute("SELECT name FROM Graphen WHERE m='%d'" % m)
          names = cursor.fetchall()
        elif n is not None and m is not None:
          cursor.execute("SELECT name FROM Graphen WHERE n='%d' AND m='%d'" % (n, m))
          names = cursor.fetchall()
        else:
          self.onShowAll()
      except:
        print("Datei nicht vorhanden oder falsches Format")
        raise

      self.comboBoxGraphen.clear()
      for name in names:
        self.comboBoxGraphen.addItem(name[0])

      cursor.close()

    else:
      fedit(None, title="Info", comment="Nur für Datenbanken verfügbar!")

  def onShowAll(self):
    '''Show all graphs'''
    graphnames = None
    if self.db:
      graphnames = getGraphNamesdb(self.fname)
    elif self.graphml:
      graphnames = getGraphNamesGraphml(self.fname)
    elif self.gml:
      graphnames = getGraphNamesGML(self.fname)
    else:
      graphnames = getGraphNames(self.fname)

    self.graphNames = graphnames[1:]
    self.comboBoxGraphen.clear()
    for name in self.graphNames:
      self.comboBoxGraphen.addItem(name)

  def onFileDialog(self):
    '''Load a new file'''
    self.fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',
                                                          self.config["Paths"]["data_dir"],
                                                          "Datenbank (*.db);;Datenbank (*.sdb);;XML-files (*.xml);;GraphML (*.graphml);;GML (*.gml)")

    if self.fname is None or len(self.fname) == 0:
      return

    self.db = False
    self.graphml = False
    self.gml = False
    if ".db" in self.fname or ".sdb" in self.fname:
      self.db = True
    elif ".gml" in self.fname:
      self.gml = True
    elif ".graphml" in self.fname:
      self.graphml = True

    self.lineEditFile.setText(str(self.fname))

    graphnames = None
    try:
      if self.db:
        graphnames = getGraphNamesdb(self.fname)
      elif self.graphml:
        graphnames = getGraphNamesGraphml(self.fname)
      elif self.gml:
        graphnames = getGraphNamesGML(self.fname)
      else:
        graphnames = getGraphNames(self.fname)
    except:
      fedit(None, title="Error", comment="Error while loading file!")
      return False

    if not graphnames:
      fedit(None, title="Error", comment="Error while loading file!")
      return False

    self.graphNames = graphnames[1:]
    self.comboBoxGraphen.clear()
    for name in self.graphNames:
      self.comboBoxGraphen.addItem(name)

    self.comboBoxGraphen.activated.emit(0)
    return True

  def onDelete(self):
    '''Delete current graph (only for sqlite)'''
    if self.db:
      com = """Wollen Sie den aktuellen Graph wirklich löschen?
          
          Das Löschen kann nicht mehr rückgängig gemacht werden!"""
      values = fedit("yesorno", title="Warnung", comment=com)
      if not values:
        return
      else:
        # Delete the graph and update the graph list
        name = str(self.comboBoxGraphen.currentText())
        delete_graphdb(self.fname, name)

        if self.db:
          graphnames = getGraphNamesdb(self.fname)
        else:
          graphnames = getGraphNames(self.fname)

        self.graphNames = graphnames[1:]
        self.comboBoxGraphen.clear()
        for name in self.graphNames:
          self.comboBoxGraphen.addItem(name)
    else:
      fedit(None, title="Info",
            comment="Only implemented for SQLite-database!")

  def onOk(self):
    '''Save the infos, load the graph and close the dialog'''
    self.globalsClass.PLOTSTYLE = str(self.comboBoxDarstellung.currentText())
    self.globalsClass.FNAME = self.fname
    self.globalsClass.GRAPHNAME = str(self.comboBoxGraphen.currentText())
    self.graphname = self.globalsClass.GRAPHNAME
    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()

  def aktPicture(self):
    '''Update the preview picture'''
    if self.db:
      connection = connect(self.fname, detect_types=PARSE_DECLTYPES)
      cursor = connection.cursor()

      item = str(self.comboBoxGraphen.currentText()).strip()
      try:
        file = cursor.execute("SELECT bin FROM Pictures WHERE name='%s'" % item).fetchone()
      except:
        file = None

      try:
        pictureType = cursor.execute("SELECT type FROM Pictures WHERE name='%s'" % item).fetchone()
      except:
        pictureType = "png"

      if file is not None:
        if pictureType == "png":
          _, path = tempfile.mkstemp(suffix='.png')
        else:
          _, path = tempfile.mkstemp(suffix='.svg')
        f = open(path, "wb")
        f.write(file[0])
        f.close()
        cursor.close()

        pixmap = QtGui.QPixmap(path)

      # Generate the preview picture if the graph has not more than 50 vertices
      elif int(self.lineEditKnoten.text()) <= 20:
        graph, coord = load_graphdb(self.fname,
                                    str(self.comboBoxGraphen.currentText()),
                                    graphtype="SWGraph")

        if coord is None:
          coord = {}
          for v in graph.getVIt():
            coord[v] = (uniform(-385, 730), uniform(-285, 570))
          coord = springEmbedding(graph, coord)

        _, tmp_path = tempfile.mkstemp(suffix='.svg')
        generateSVG(graph, coord, tmp_path)

        pixmap = QtGui.QPixmap(tmp_path)

        try:
          cursor.execute("""CREATE TABLE Pictures (name TEXT, bin BINARY, type STRING)""")
        except:
          pass

        from sqlite3 import Binary as sqliteBinary
        file = open(tmp_path, 'rb')
        try:
          cursor.execute('insert into Pictures (name, bin, type) values (?,?,?)',
                         (str(self.comboBoxGraphen.currentText()),
                          sqliteBinary(file.read()),
                          "svg"))
        except:
          pass

        file.close()
        connection.commit()
        cursor.close()

      else:
        pixmap = QtGui.QPixmap(self.config["Paths"].get("data_dir") + "keinBild.png")

    else:
      pixmap = QtGui.QPixmap(self.config["Paths"].get("data_dir") + "keinBild.png")

    self.PictureLabel.setPixmap(pixmap)
    self.PictureLabel.setScaledContents(True)


# =========================================================================
class SaveSimulationDialog(QtWidgets.QDialog, Ui_SaveSimulation):
  '''
  Dialog to save the current simulation in a database (*.sdb)
  '''
  accept = QtCore.pyqtSignal()
  
  def __init__(self):
    super(SaveSimulationDialog, self).__init__(None)
    self.setupUi(self)

    self.graphname = ""
    self.simname = ""
    self.fname = ""

    # Slot einrichten
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonCancel.clicked.connect(self.onClose)
    self.comboBoxGraphs.activated.connect(self.__akt__)

  def onOk(self):
    '''Close the dialog an emit the accept-Signal'''
    self.graphname = str(self.lineEditGraphName.text())
    self.simname = str(self.lineEditSimName.text())
    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()

  def __akt__(self):
    '''Write the graphnames of the current file in the combo-box'''
    if self.fname and len(self.fname) != 0:
      graphname = str(self.comboBoxGraphs.currentText())
      simulations = getSimNames(self.fname, graphname)
      self.comboBoxSims.clear()
      if simulations:
        for name in simulations[1:]:
          self.comboBoxSims.addItem(name)

  def show(self, *args, **kwargs):
    '''Show the dialog. Overwrite the show-function of the QDialog.'''
    self.lineEditGraphName.setText(self.graphname)
    graphnames = getGraphNamesdb(self.fname)
    self.comboBoxGraphs.clear()
    if graphnames:
      for name in graphnames[1:]:
        self.comboBoxGraphs.addItem(name)
    self.comboBoxGraphs.activated.emit(0)

    return QtWidgets.QDialog.show(self, *args, **kwargs)


# =========================================================================
class LoadSimulationDialog(QtWidgets.QDialog, Ui_LoadAnimation):
  '''Load a graph and a simulation from a SQLite-database (*.sdb).'''
  accept = QtCore.pyqtSignal()

  def __init__(self, globalsClass, config):
    super(LoadSimulationDialog, self).__init__(None)
    self.setupUi(self)

    self.fname = ""
    self.graphname = ""
    self.simname = ""
    self.graphNames = ""
    self.globalsClass = globalsClass
    self.config = config

    # Slot einrichten
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonAbbrechen.clicked.connect(self.onClose)
    self.pushButtonDelete.clicked.connect(self.onDelete)
    self.comboBoxGraphen.activated.connect(self.__akt__)
    self.pushButtonFile.clicked.connect(self.onFileDialog)
    self.checkBoxShowPreview.stateChanged.connect(self.__showPreview__)

    self.comboBoxDarstellung.clear()
    for mode in PLOTMODES_GRAPHVIZ:
      self.comboBoxDarstellung.addItem(mode)
    for mode in PLOTMODES_INTERN:
      self.comboBoxDarstellung.addItem(mode)

  def show(self, *args, **kwargs):
    '''Show the dialog'''
    self.onFileDialog()
    return QtWidgets.QDialog.show(self, *args, **kwargs)

  def __showPreview__(self):
    '''Enables or disables the preview'''
    if self.checkBoxShowPreview.isChecked():
      self.aktPicture()
    else:
      self.PictureLabel.clear()

  def __akt__(self):
    '''Load for the current graph the order, the size and the picture'''
    connection = connect(self.fname, detect_types=PARSE_DECLTYPES)
    cursor = connection.cursor()

    register_adapter(SGraph, graphadapter)
    register_converter("SGraph", graphkonverter)
    register_adapter(dict, dictadapter)
    register_converter("dict", dictkonverter)

    item = str(self.comboBoxGraphen.currentText())
    try:
      cursor.execute("SELECT n, m FROM Graphen WHERE name='%s'" % item)
    except:
      print("Datei nicht vorhanden oder falsches Format __akt__")
      return None
    r = cursor.fetchall()
    if len(r) >= 1:
      n, m = r[0]
    else:
      n = None
      m = None

    cursor.close()

    self.lineEditKnoten.setText(str(n))
    self.lineEditKanten.setText(str(m))

    simnames = getSimNames(self.fname, item)
    self.graphNames = simnames[1:]
    self.comboBoxAnimations.clear()
    for name in self.graphNames:
      self.comboBoxAnimations.addItem(name)

    if self.checkBoxShowPreview.isChecked():
      self.aktPicture()
    else:
      self.PictureLabel.clear()

  def onFileDialog(self):
    '''Load a new file'''
    self.fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',
                                                          self.config["Paths"]["data_dir"],
                                                          "Datenbank (*.sdb)")

    if self.fname is None or len(self.fname) == 0:
      return

    self.lineEditFile.setText(str(self.fname))

    graphnames = getGraphNamesdb(self.fname)

    self.graphNames = graphnames[1:]
    self.comboBoxGraphen.clear()
    for name in self.graphNames:
      self.comboBoxGraphen.addItem(name)

    simnames = getSimNames(self.fname, graphnames[1])
    self.graphNames = simnames[1:]
    self.comboBoxAnimations.clear()
    for name in self.graphNames:
      self.comboBoxAnimations.addItem(name)

    self.comboBoxGraphen.activated.emit(0)

  def onDelete(self):
    '''Delete current graph (only for sqlite)'''
    com = """Wollen Sie den aktuellen Graph wirklich löschen?
        
        Das Löschen kann nicht mehr rückgängig gemacht werden!"""
    values = fedit("yesorno", title="Warnung", comment=com)

    if not values:
      return
    else:
      # Delete the graph and update the graph list
      name = str(self.comboBoxGraphen.currentText())
      delete_graphdb(self.fname, name)
      deleteSimulations(self.fname, name)

      graphnames = getGraphNamesdb(self.fname)

      self.graphNames = graphnames[1:]
      self.comboBoxGraphen.clear()
      for name in self.graphNames:
        self.comboBoxGraphen.addItem(name)

  def onOk(self):
    '''Save the infos, load the graph and close the dialog'''
    self.globalsClass.PLOTSTYLE = str(self.comboBoxDarstellung.currentText())
    self.globalsClass.FNAME = self.fname
    self.globalsClass.GRAPHNAME = str(self.comboBoxGraphen.currentText())
    self.globalsClass.SIMNAME = str(self.comboBoxAnimations.currentText())
    self.graphname = self.globalsClass.GRAPHNAME
    self.simname = self.globalsClass.SIMNAME
    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()

  def aktPicture(self):
    '''Update the preview picture'''
    connection = connect(self.fname, detect_types=PARSE_DECLTYPES)
    cursor = connection.cursor()

    item = str(self.comboBoxGraphen.currentText()).strip()
    try:
      file = cursor.execute("SELECT bin FROM Pictures WHERE name='%s'" % item).fetchone()
    except:
      file = None

    try:
      pictureType = cursor.execute("SELECT type FROM Pictures WHERE name='%s'" % item).fetchone()
    except:
      pictureType = "png"

    if file is not None:
      if pictureType == "png":
        _, path = tempfile.mkstemp(suffix='.png')
      else:
        _, path = tempfile.mkstemp(suffix='.svg')
      f = open(path, "wb")
      f.write(file[0])
      f.close()
      cursor.close()

      pixmap = QtGui.QPixmap(path)

    # Generate the preview picture if the graph has not more than 50 vertices
    elif int(self.lineEditKnoten.text()) <= 20:
      graph, coord = load_graphdb(self.fname,
                                  str(self.comboBoxGraphen.currentText()),
                                  graphtype="SWGraph")

      if coord is None:
        coord = {}
        for v in graph.getVIt():
          coord[v] = (uniform(-385, 730), uniform(-285, 570))
        coord = springEmbedding(graph, coord)

      _, tmp_path = tempfile.mkstemp(suffix='.svg')
      generateSVG(graph, coord, tmp_path)

      pixmap = QtGui.QPixmap(tmp_path)

      try:
        cursor.execute("""CREATE TABLE Pictures (name TEXT, bin BINARY, type STRING)""")
      except:
        pass

      from sqlite3 import Binary as sqliteBinary
      file = open(tmp_path, 'rb')
      cursor.execute('insert into Pictures (name, bin, type) values (?,?,?)',
                     (str(self.comboBoxGraphen.currentText()),
                      sqliteBinary(file.read()), "svg"))

      file.close()
      connection.commit()
      cursor.close()

    else:
      pixmap = QtGui.QPixmap(self.config["Paths"].get("data_dir") + "keinBild.png")

    self.PictureLabel.setPixmap(pixmap)
    self.PictureLabel.setScaledContents(True)


# =========================================================================
class LoadOSMDialog(QtWidgets.QDialog, Ui_LoadOSM):
  '''Load the OSM map file and display it as a graph'''
  accept = QtCore.pyqtSignal()
  
  def __init__(self, globalsClass, config):
    super(LoadOSMDialog, self).__init__(None)
    self.setupUi(self)

    self.fname = ""
    self.graphName = ""
    self.globalsClass = globalsClass
    # self.config = config
    # self.shift = None
    # self.graphNames = None
    self.priority = None

    # Init the slots
    self.pushButtonLoad.clicked.connect(self.onOk)
    self.pushButtonClose.clicked.connect(self.onClose)
    self.pushButtonLoadFile.clicked.connect(self.onFileDialog)

  def onFileDialog(self):
    '''Load a new file'''
    # fname, ffilter = QtWidgets.QFileDialog.getOpenFileName(self,
    #                                                        'Open file',
    #                                                        self.config["Paths"]["data_dir"],
    #                                                        "XML-files (*.xml)")
    fname, _ = QtWidgets.QFileDialog.getOpenFileName(self,\
                        'Open file', ".", "OSM-mapfile (*.osm)")

    if fname is None or len(fname) == 0:
      return
    self.lineEditFile.setText(fname)

  def onOk(self):
    '''Save the infos, load the graph and close the dialog'''
    self.fname = self.lineEditFile.text()
    self.priority = self.spinBoxPriority.value()
    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()


# =========================================================================
class Load2GraphsDialog(QtWidgets.QDialog, Ui_Load2Graphs):
  '''Load two graphs and display them'''
  accept = QtCore.pyqtSignal()
  
  def __init__(self, globalsClass, config):
    super(Load2GraphsDialog, self).__init__(None)
    self.setupUi(self)

    self.graph1Name = ""
    self.graph1File = ""
    self.graph2Name = ""
    self.graph2File = ""
    self.globalsClass = globalsClass
    self.config = config
    self.shift = None
    self.graphNames = None

    # Init the slots
    self.pushButtonLoad.clicked.connect(self.onOk)
    self.pushButtonClose.clicked.connect(self.onClose)
    self.pushButtonGraph1File.clicked.connect(self.onFileDialog1)
    self.pushButtonGraph2File.clicked.connect(self.onFileDialog2)

  def onFileDialog1(self):
    '''Load a new file'''
    fname, ffilter = QtWidgets.QFileDialog.getOpenFileName(self,
                                                           'Open file',
                                                           self.config["Paths"]["data_dir"],
                                                           "Datenbank (*.db);;XML-files (*.xml);;GraphML (*.graphml);;GML (*.gml)")

    if fname is None or len(fname) == 0:
      return

    self.lineEditGraph1File.setText(fname)

    # Fetch the graphnames in the file
    graphnames = None
    if "db" in ffilter:
      graphnames = getGraphNamesdb(fname)
    elif "graphml" in ffilter:
      graphnames = getGraphNamesGraphml(fname)
    elif "gml" in ffilter:
      graphnames = getGraphNamesGML(fname)
    else:
      graphnames = getGraphNames(fname)

    # Add the names to the combo box
    self.graphNames = graphnames[1:]
    self.comboBoxGraph1Name.clear()
    for name in self.graphNames:
      self.comboBoxGraph1Name.addItem(name)

  def onFileDialog2(self):
    '''Load a new file'''
    fname, ffilter = QtWidgets.QFileDialog.getOpenFileName(self,
                                                           'Open file',
                                                           self.config["Paths"]["data_dir"],
                                                           "Datenbank (*.db);;XML-files (*.xml);;GraphML (*.graphml);;GML (*.gml)")

    if fname is None or len(fname) == 0:
      return

    self.lineEditGraph2File.setText(fname)

    # Fetch the graphnames in the file
    graphnames = None
    if "db" in ffilter:
      graphnames = getGraphNamesdb(fname)
    elif "graphml" in ffilter:
      graphnames = getGraphNamesGraphml(fname)
    elif "gml" in ffilter:
      graphnames = getGraphNamesGML(fname)
    else:
      graphnames = getGraphNames(fname)

    # Add the names to the combo box
    self.graphNames = graphnames[1:]
    self.comboBoxGraph2Name.clear()
    for name in self.graphNames:
      self.comboBoxGraph2Name.addItem(name)

  def onOk(self):
    '''Save the infos, load the graph and close the dialog'''
    self.graph1Name = str(self.comboBoxGraph1Name.currentText())
    self.graph1File = str(self.lineEditGraph1File.text())
    self.graph2Name = str(self.comboBoxGraph2Name.currentText())
    self.graph2File = str(self.lineEditGraph2File.text())
    self.shift = float(self.lineEditShift.text())
    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()
