# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Close.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(462, 152)
    self.pushButtonClose = QtWidgets.QPushButton(Dialog)
    self.pushButtonClose.setGeometry(QtCore.QRect(350, 110, 98, 27))
    self.pushButtonClose.setObjectName("pushButtonClose")
    self.pushButtonAbort = QtWidgets.QPushButton(Dialog)
    self.pushButtonAbort.setGeometry(QtCore.QRect(240, 110, 98, 27))
    self.pushButtonAbort.setObjectName("pushButtonAbort")
    self.pushButtonSaveClose = QtWidgets.QPushButton(Dialog)
    self.pushButtonSaveClose.setEnabled(True)
    self.pushButtonSaveClose.setGeometry(QtCore.QRect(47, 110, 181, 27))
    self.pushButtonSaveClose.setObjectName("pushButtonSaveClose")
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setGeometry(QtCore.QRect(40, 20, 381, 61))
    self.label.setObjectName("label")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Achtung"))
    self.pushButtonClose.setText(_translate("Dialog", "Beenden"))
    self.pushButtonAbort.setText(_translate("Dialog", "Abbrechen"))
    self.pushButtonSaveClose.setText(_translate("Dialog", "Speichern und Schließen"))
    self.label.setText(_translate("Dialog", "Wollen Sie das Programm wirklich beenden?"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

