# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Export.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(459, 85)
    self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
    self.verticalLayout.setObjectName("verticalLayout")
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setObjectName("label")
    self.verticalLayout.addWidget(self.label)
    self.horizontalLayout = QtWidgets.QHBoxLayout()
    self.horizontalLayout.setObjectName("horizontalLayout")
    spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem)
    self.pushButtonGraphviz = QtWidgets.QPushButton(Dialog)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonGraphviz.sizePolicy().hasHeightForWidth())
    self.pushButtonGraphviz.setSizePolicy(sizePolicy)
    self.pushButtonGraphviz.setMinimumSize(QtCore.QSize(98, 0))
    self.pushButtonGraphviz.setMaximumSize(QtCore.QSize(98, 16777215))
    self.pushButtonGraphviz.setObjectName("pushButtonGraphviz")
    self.horizontalLayout.addWidget(self.pushButtonGraphviz)
    spacerItem1 = QtWidgets.QSpacerItem(30, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem1)
    self.pushButtonQt = QtWidgets.QPushButton(Dialog)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonQt.sizePolicy().hasHeightForWidth())
    self.pushButtonQt.setSizePolicy(sizePolicy)
    self.pushButtonQt.setMinimumSize(QtCore.QSize(98, 0))
    self.pushButtonQt.setMaximumSize(QtCore.QSize(98, 16777215))
    self.pushButtonQt.setObjectName("pushButtonQt")
    self.horizontalLayout.addWidget(self.pushButtonQt)
    spacerItem2 = QtWidgets.QSpacerItem(30, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem2)
    self.pushButtonIntern = QtWidgets.QPushButton(Dialog)
    self.pushButtonIntern.setEnabled(True)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.pushButtonIntern.sizePolicy().hasHeightForWidth())
    self.pushButtonIntern.setSizePolicy(sizePolicy)
    self.pushButtonIntern.setMinimumSize(QtCore.QSize(98, 0))
    self.pushButtonIntern.setMaximumSize(QtCore.QSize(98, 16777215))
    self.pushButtonIntern.setObjectName("pushButtonIntern")
    self.horizontalLayout.addWidget(self.pushButtonIntern)
    spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem3)
    self.verticalLayout.addLayout(self.horizontalLayout)

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Export"))
    self.label.setText(_translate("Dialog", "Welches Programm soll zum Export verwendet werden?"))
    self.pushButtonGraphviz.setText(_translate("Dialog", "Graphviz"))
    self.pushButtonQt.setText(_translate("Dialog", "Qt"))
    self.pushButtonIntern.setText(_translate("Dialog", "Intern"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

