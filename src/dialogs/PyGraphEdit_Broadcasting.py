# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Broadcasting.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(462, 352)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
    Dialog.setSizePolicy(sizePolicy)
    Dialog.setMaximumSize(QtCore.QSize(16777215, 454))
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("E:/workspace/HD_Steg/symbole/2075.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Dialog.setWindowIcon(icon)
    self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
    self.verticalLayout.setObjectName("verticalLayout")
    self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_2.setObjectName("horizontalLayout_2")
    self.label = QtWidgets.QLabel(Dialog)
    self.label.setObjectName("label")
    self.horizontalLayout_2.addWidget(self.label)
    self.lineEditStartVertices = QtWidgets.QLineEdit(Dialog)
    self.lineEditStartVertices.setObjectName("lineEditStartVertices")
    self.horizontalLayout_2.addWidget(self.lineEditStartVertices)
    self.verticalLayout.addLayout(self.horizontalLayout_2)
    self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_3.setObjectName("horizontalLayout_3")
    self.label_2 = QtWidgets.QLabel(Dialog)
    self.label_2.setMinimumSize(QtCore.QSize(220, 0))
    self.label_2.setMaximumSize(QtCore.QSize(2220, 16777215))
    self.label_2.setObjectName("label_2")
    self.horizontalLayout_3.addWidget(self.label_2)
    self.lineEditProb = QtWidgets.QLineEdit(Dialog)
    self.lineEditProb.setMaximumSize(QtCore.QSize(50, 16777215))
    self.lineEditProb.setToolTip("")
    self.lineEditProb.setObjectName("lineEditProb")
    self.horizontalLayout_3.addWidget(self.lineEditProb)
    spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout_3.addItem(spacerItem)
    self.verticalLayout.addLayout(self.horizontalLayout_3)
    self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_4.setObjectName("horizontalLayout_4")
    self.label_3 = QtWidgets.QLabel(Dialog)
    self.label_3.setMinimumSize(QtCore.QSize(220, 0))
    self.label_3.setMaximumSize(QtCore.QSize(220, 16777215))
    self.label_3.setObjectName("label_3")
    self.horizontalLayout_4.addWidget(self.label_3)
    self.lineEditMaxNumSteps = QtWidgets.QLineEdit(Dialog)
    self.lineEditMaxNumSteps.setMaximumSize(QtCore.QSize(50, 16777215))
    self.lineEditMaxNumSteps.setToolTip("")
    self.lineEditMaxNumSteps.setObjectName("lineEditMaxNumSteps")
    self.horizontalLayout_4.addWidget(self.lineEditMaxNumSteps)
    spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout_4.addItem(spacerItem1)
    self.verticalLayout.addLayout(self.horizontalLayout_4)
    self.groupBox = QtWidgets.QGroupBox(Dialog)
    self.groupBox.setMinimumSize(QtCore.QSize(444, 90))
    self.groupBox.setMaximumSize(QtCore.QSize(16777215, 90))
    self.groupBox.setObjectName("groupBox")
    self.checkBoxSingleNeighbor = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxSingleNeighbor.setGeometry(QtCore.QRect(10, 40, 411, 22))
    self.checkBoxSingleNeighbor.setAutoExclusive(True)
    self.checkBoxSingleNeighbor.setObjectName("checkBoxSingleNeighbor")
    self.checkBoxAllNeighbors = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxAllNeighbors.setGeometry(QtCore.QRect(10, 20, 427, 22))
    self.checkBoxAllNeighbors.setChecked(True)
    self.checkBoxAllNeighbors.setAutoExclusive(True)
    self.checkBoxAllNeighbors.setObjectName("checkBoxAllNeighbors")
    self.checkBoxRandomNeighbor = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxRandomNeighbor.setGeometry(QtCore.QRect(10, 60, 431, 22))
    self.checkBoxRandomNeighbor.setAutoExclusive(True)
    self.checkBoxRandomNeighbor.setObjectName("checkBoxRandomNeighbor")
    self.verticalLayout.addWidget(self.groupBox)
    self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
    self.groupBox_2.setMinimumSize(QtCore.QSize(444, 70))
    self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 70))
    self.groupBox_2.setObjectName("groupBox_2")
    self.checkBoxInfosOneTime = QtWidgets.QCheckBox(self.groupBox_2)
    self.checkBoxInfosOneTime.setGeometry(QtCore.QRect(10, 20, 427, 22))
    self.checkBoxInfosOneTime.setChecked(True)
    self.checkBoxInfosOneTime.setAutoExclusive(True)
    self.checkBoxInfosOneTime.setObjectName("checkBoxInfosOneTime")
    self.checkBoxInfosEveryTime = QtWidgets.QCheckBox(self.groupBox_2)
    self.checkBoxInfosEveryTime.setGeometry(QtCore.QRect(10, 40, 427, 22))
    self.checkBoxInfosEveryTime.setAutoExclusive(True)
    self.checkBoxInfosEveryTime.setObjectName("checkBoxInfosEveryTime")
    self.verticalLayout.addWidget(self.groupBox_2)
    self.checkBoxHiddenNode = QtWidgets.QCheckBox(Dialog)
    self.checkBoxHiddenNode.setObjectName("checkBoxHiddenNode")
    self.verticalLayout.addWidget(self.checkBoxHiddenNode)
    self.horizontalLayout = QtWidgets.QHBoxLayout()
    self.horizontalLayout.setObjectName("horizontalLayout")
    spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
    self.horizontalLayout.addItem(spacerItem2)
    self.pushButtonAbort = QtWidgets.QPushButton(Dialog)
    self.pushButtonAbort.setObjectName("pushButtonAbort")
    self.horizontalLayout.addWidget(self.pushButtonAbort)
    self.pushButtonOK = QtWidgets.QPushButton(Dialog)
    self.pushButtonOK.setObjectName("pushButtonOK")
    self.horizontalLayout.addWidget(self.pushButtonOK)
    self.verticalLayout.addLayout(self.horizontalLayout)

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Darstellungsart"))
    self.label.setText(_translate("Dialog", "Startknoten:"))
    self.lineEditStartVertices.setToolTip(_translate("Dialog", "<html><head/><body><p>Knotenliste in der Form: 1,2,3,10</p></body></html>"))
    self.label_2.setText(_translate("Dialog", "Ausbreitungswahrscheinlickeit p:"))
    self.lineEditProb.setText(_translate("Dialog", "0.2"))
    self.label_3.setText(_translate("Dialog", "Maximale Anzahl von Schritten:"))
    self.lineEditMaxNumSteps.setText(_translate("Dialog", "100"))
    self.groupBox.setTitle(_translate("Dialog", "Ausbreitungsart"))
    self.checkBoxSingleNeighbor.setText(_translate("Dialog", "Einzelnen Nachbarn mit Wahrscheinlickeit p informieren"))
    self.checkBoxAllNeighbors.setText(_translate("Dialog", "Ganze Nachbarschaft mit Wahrscheinlichkeit p informieren"))
    self.checkBoxRandomNeighbor.setText(_translate("Dialog", "Einen zufälligen Nachbarn von jedem Knoten pro Zeitschritt"))
    self.groupBox_2.setTitle(_translate("Dialog", "Sendungswiederholung"))
    self.checkBoxInfosOneTime.setText(_translate("Dialog", "Information einmal senden"))
    self.checkBoxInfosEveryTime.setText(_translate("Dialog", "Information in jedem Zeitschritt senden"))
    self.checkBoxHiddenNode.setText(_translate("Dialog", "Hidden-Node"))
    self.pushButtonAbort.setText(_translate("Dialog", "Abbrechen"))
    self.pushButtonOK.setText(_translate("Dialog", "OK"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

