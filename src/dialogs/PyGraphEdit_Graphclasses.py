# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PyGraphEdit_Graphclasses.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
  def setupUi(self, Dialog):
    Dialog.setObjectName("Dialog")
    Dialog.resize(533, 264)
    self.pushButtonClose = QtWidgets.QPushButton(Dialog)
    self.pushButtonClose.setGeometry(QtCore.QRect(210, 230, 75, 23))
    self.pushButtonClose.setObjectName("pushButtonClose")
    self.groupBox = QtWidgets.QGroupBox(Dialog)
    self.groupBox.setGeometry(QtCore.QRect(20, 20, 481, 201))
    self.groupBox.setObjectName("groupBox")
    self.checkBoxComplete = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxComplete.setGeometry(QtCore.QRect(30, 30, 181, 17))
    self.checkBoxComplete.setCheckable(True)
    self.checkBoxComplete.setObjectName("checkBoxComplete")
    self.checkBoxBipartite = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxBipartite.setGeometry(QtCore.QRect(30, 50, 211, 17))
    self.checkBoxBipartite.setCheckable(True)
    self.checkBoxBipartite.setObjectName("checkBoxBipartite")
    self.checkBoxCompleteBipartit = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxCompleteBipartit.setGeometry(QtCore.QRect(30, 70, 221, 17))
    self.checkBoxCompleteBipartit.setCheckable(True)
    self.checkBoxCompleteBipartit.setObjectName("checkBoxCompleteBipartit")
    self.checkBoxkTree = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxkTree.setGeometry(QtCore.QRect(30, 130, 131, 17))
    self.checkBoxkTree.setCheckable(True)
    self.checkBoxkTree.setObjectName("checkBoxkTree")
    self.checkBoxkPath = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxkPath.setGeometry(QtCore.QRect(30, 150, 131, 17))
    self.checkBoxkPath.setCheckable(True)
    self.checkBoxkPath.setObjectName("checkBoxkPath")
    self.checkBoxSimplekPath = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxSimplekPath.setGeometry(QtCore.QRect(30, 170, 201, 17))
    self.checkBoxSimplekPath.setCheckable(True)
    self.checkBoxSimplekPath.setObjectName("checkBoxSimplekPath")
    self.checkBoxTree = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxTree.setGeometry(QtCore.QRect(30, 90, 211, 17))
    self.checkBoxTree.setCheckable(True)
    self.checkBoxTree.setObjectName("checkBoxTree")
    self.checkBoxPath = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxPath.setGeometry(QtCore.QRect(30, 110, 211, 17))
    self.checkBoxPath.setCheckable(True)
    self.checkBoxPath.setObjectName("checkBoxPath")
    self.checkBoxRegGraph = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxRegGraph.setGeometry(QtCore.QRect(280, 30, 181, 17))
    self.checkBoxRegGraph.setCheckable(True)
    self.checkBoxRegGraph.setObjectName("checkBoxRegGraph")
    self.checkBoxPlanar = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxPlanar.setGeometry(QtCore.QRect(280, 50, 181, 17))
    self.checkBoxPlanar.setCheckable(True)
    self.checkBoxPlanar.setObjectName("checkBoxPlanar")
    self.checkBoxEulersch = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxEulersch.setGeometry(QtCore.QRect(280, 70, 181, 17))
    self.checkBoxEulersch.setCheckable(True)
    self.checkBoxEulersch.setObjectName("checkBoxEulersch")
    self.checkBoxChordal = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxChordal.setGeometry(QtCore.QRect(280, 90, 181, 17))
    self.checkBoxChordal.setCheckable(True)
    self.checkBoxChordal.setObjectName("checkBoxChordal")
    self.checkBoxSelfcentral = QtWidgets.QCheckBox(self.groupBox)
    self.checkBoxSelfcentral.setGeometry(QtCore.QRect(280, 110, 181, 17))
    self.checkBoxSelfcentral.setCheckable(True)
    self.checkBoxSelfcentral.setObjectName("checkBoxSelfcentral")

    self.retranslateUi(Dialog)
    QtCore.QMetaObject.connectSlotsByName(Dialog)

  def retranslateUi(self, Dialog):
    _translate = QtCore.QCoreApplication.translate
    Dialog.setWindowTitle(_translate("Dialog", "Test auf Graphenklassen"))
    self.pushButtonClose.setText(_translate("Dialog", "Schließen"))
    self.groupBox.setTitle(_translate("Dialog", "Graphenklassen"))
    self.checkBoxComplete.setText(_translate("Dialog", "Vollständiger Graph"))
    self.checkBoxBipartite.setText(_translate("Dialog", "bipartiter Graph"))
    self.checkBoxCompleteBipartit.setText(_translate("Dialog", "vollständig bipartiter Graph"))
    self.checkBoxkTree.setText(_translate("Dialog", "k-Baum"))
    self.checkBoxkPath.setText(_translate("Dialog", "k-Weg"))
    self.checkBoxSimplekPath.setText(_translate("Dialog", "einfacher k-Weg"))
    self.checkBoxTree.setText(_translate("Dialog", "Baum"))
    self.checkBoxPath.setText(_translate("Dialog", "Weg"))
    self.checkBoxRegGraph.setText(_translate("Dialog", "regulärer Graph"))
    self.checkBoxPlanar.setText(_translate("Dialog", "planarer Graph"))
    self.checkBoxEulersch.setText(_translate("Dialog", "eulerscher Graph"))
    self.checkBoxChordal.setText(_translate("Dialog", "chordaler Graph"))
    self.checkBoxSelfcentral.setText(_translate("Dialog", "selbstzentraler Graph"))


if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  Dialog = QtWidgets.QDialog()
  ui = Ui_Dialog()
  ui.setupUi(Dialog)
  Dialog.show()
  sys.exit(app.exec_())

