# -*- coding: latin-1 -*-
'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import re
from sqlite3 import (connect, register_adapter, register_converter,  # @UnresolvedImport
                     PARSE_DECLTYPES)  # @UnresolvedImport
import xml.dom.minidom as dom

from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.SWGraph import SWGraph  # @UnresolvedImport


# =========================================================================
__all__ = ['delete_graphdb', 'dictadapter', 'dictkonverter', 'getGraphNames',
           'getGraphNamesdb', 'getGraphNamesGraphml', 'graphadapter',
           'graphkonverter', 'GraphMLParser', 'graphtograph6',
           'load_graph_TSPLib', 'load_graphdb', 'load_graph', 'parse_graph6',
           'save_graph', 'save_graphdb', 'GMLParser', 'getGraphNamesGML',
           'graph6FiletoDB', 'LEDAParser']


# =========================================================================
# =========================================================================
# =========================================================================
def save_graph(G, filename, name=None, coord=None):
  '''
  Save the graph in a xml-file
  @param filename: Filename of the xml-file
  @param name: Name of the graph (default=None)
  @param coord: Dict with the coordinates of the vertices (default=None)
  '''
  if name is None:
    name = G.name

  root = None
  try:
    graphs = dom.parse(filename)
    root = graphs.firstChild
  except:
    graphs = dom.DOMImplementation().createDocument(None, "graphs", None)
    root = graphs.documentElement

  # If the graph already exists, then overwrite it
  for graph in root.childNodes:
    if graph.nodeName == "graph" and graph.getAttribute("name") == name:
      root.removeChild(graph)

  # Generate a new graph in the xml-tree
  graph = graphs.createElement("graph")
  graph.setAttribute("name", str(name))

  nodes = graphs.createElement("nodes")
  for v in G.getVIt():
    node = graphs.createElement("node")
    node.setAttribute("name", str(v))
    if isinstance(G, SWGraph):
      node.setAttribute("weight", "%.4f" % G.getWeight(v))
    if coord != None:
      node.setAttribute("x", str(coord[v][0]))
      node.setAttribute("y", str(coord[v][1]))
    nodes.appendChild(node)

  graph.appendChild(nodes)

  edges = graphs.createElement("edges")
  neighbors = G.neighbors
  degree = G.degree
  for v in G.getVIt():
    if degree(v) != 0:
      edge = graphs.createElement("edge")
      edge.setAttribute("from", str(v))
      for w in neighbors(v, withloops=True):
        end_node = graphs.createElement("end")
        end_node.setAttribute("name", str(w))
        if isinstance(G, SWGraph):
          end_node.setAttribute("weight", "%.4f" % G.getWeight((v, w)))
        edge.appendChild(end_node)
      edges.appendChild(edge)

  graph.appendChild(edges)
  root.appendChild(graph)

  # Write the xml-file
  with open(filename, "w") as f:
    graphs.writexml(f, "", "\t", "\n")

def save_graphdb(G, filename, name=None, coord=None, graph6=False):
  '''
  Save the graph in a sqlLite database
  @param filename: Name of the database
  @param name: Name of the graph
  @param coord: Dict with the coordinates of the vertices (default=None)
  @param graph6: Use the graph6-format (default=False)
  '''
  if name is None:
    name = G.name

  global tograph6
  tograph6 = graph6

  connection = connect(filename, detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()

  try:
    cursor.execute("""CREATE TABLE Graphen (name TEXT, graph SGraph, n INTEGER, m INTEGER, koords dict)""")
  except:
    pass

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  cursor.execute("SELECT graph FROM Graphen WHERE name='%s'" % name)
  r = cursor.fetchall()
  if len(r) != 0:
    cursor.execute("DELETE FROM Graphen WHERE name='%s'" % name)

  werte = (name, SGraph(G.getV(), G.getEdgeSet()), len(G.getV()), G.size(), coord)
  sql = "INSERT INTO Graphen VALUES (?, ?, ?, ?, ?)"
  cursor.execute(sql, werte)

  connection.commit()
  cursor.close()

def delete_graphdb(filename, name):
  '''
  Delete the graph in the sqlLite-database
  @param filename: Name of the database
  @param name: Name of the graph
  @return: SGraph
  '''
  if name is None:
    return

  connection = connect(filename, detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  cursor.execute("SELECT graph, koords FROM Graphen WHERE name='%s'" % name)
  r = cursor.fetchall()
  if len(r) != 0:
    cursor.execute("DELETE FROM Graphen WHERE name='%s'" % name)

  try:
    cursor.execute("SELECT name FROM Pictures WHERE name='%s'" % name)
    r = cursor.fetchall()
    if len(r) != 0:
      cursor.execute("DELETE FROM Pictures WHERE name='%s'" % name)
  except:
    pass

  connection.commit()
  cursor.close()

def load_graph(filename, name, graphtype="SGraph"):
  '''
  Load a graph from a xml-file
  @param name: Name of the graph
  @param filename: Filename
  @param graphtype: Type of the graph to return (default=SGraph)
  @return: SGraph or SWGraph
  '''
  if graphtype == "SGraph":
    G = SGraph()
  elif graphtype == "SWGraph":
    G = SWGraph()
  else:
    print("wrong type")
    return

  try:
    graphs = dom.parse(filename)
  except OSError.FileNotFoundError:
    print("%s file is missing"  % filename)
    return
  except OSError.PermissionError:
    print("You are not allowed to read %s"  % filename)
    return
  except:
    print("unable to load file %s" % filename)
    return

  if graphs.firstChild.nodeName == "travellingSalesmanProblemInstance":
    return load_graph_TSPLib(filename), None
  elif graphs.firstChild.nodeName != "graphs":
    print("wrong XML-Format")
    return

  coord = {}
  for graph in graphs.firstChild.childNodes:
    if graph.nodeName == "graph" and graph.getAttribute("name") == name:
      G.name = name
      for infos in graph.childNodes:
        # Read a vertex
        if infos.nodeName == "nodes":
          for node in infos.childNodes:
            if node.nodeName == "node":
              G.insertVertex(int(node.getAttribute("name")))

              # Check if coordinates exists
              if node.hasAttribute("x") and node.hasAttribute("y"):
                coord[int(node.getAttribute("name"))] = (float(node.getAttribute("x")),
                                                         float(node.getAttribute("y")))

        # Read an edge
        elif infos.nodeName == "edges":
          for edge in infos.childNodes:
            if edge.nodeName == "edge":
              for endnodes in edge.childNodes:
                if endnodes.nodeName == "end":
                  G.insertEdge(int(edge.getAttribute("from")),
                               int(endnodes.getAttribute("name")))

  if G.order() == 0:
    print("error while loading the graph")
    return False

  if len(coord) == 0:
    coord = None

  return G, coord

def load_graphdb(filename, name, graphtype="SGraph"):
  '''
  Load a graph from a sqlLite database
  @param name: Name of the graph
  @param filename: Filename
  @param graphtype: Type of the graph to return (default=SGraph)
  @return: SGraph or SWGraph
  '''
  connection = connect(filename, detect_types=PARSE_DECLTYPES)
  cursor = connection.cursor()

  # Init the adapter and converter
  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  # Try to get the wanted graph
  try:
    cursor.execute("SELECT graph, koords FROM Graphen WHERE name='%s'" % name)
    graph, coord = cursor.fetchall()[0]
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None, []

  if graphtype == "SGraph":
    G = SGraph(V=graph.getV(), E=graph.getEdgeSet())
  elif graphtype == "SWGraph":
    G = SWGraph(V=graph.getV(), E=graph.getEdgeSet())
  else:
    print("wrong type")
    return None, []
  G.name = name

  cursor.close()

  return G, coord

def load_graph_TSPLib(filename):
  '''
  Load a graph from a xml-file (TSPLib)
  @param name: Name of the graph
  @param filename: Filename
  @return: SGraph
  '''
  try:
    graphs = dom.parse(filename)
  except OSError.FileNotFoundError:
    print("%s file is missing"  % filename)
    return
  except OSError.PermissionError:
    print("You are not allowed to read %s"  % filename)
    return
  except:
    print("unable to load file %s" % filename)
    return

  G = SWGraph()
  adja = {}
  v = 0

  for graph in graphs.firstChild.childNodes:
    if graph.nodeName == "name":
      G.name = graph.firstChild.data.strip()
    if graph.nodeName == "graph":
      for infos in graph.childNodes:
        if infos.nodeName == "vertex":
          v += 1
          adja[v] = set([])
          G.insertVertex(v)
          for node in infos.childNodes:
            if node.nodeName == "edge":
              adja[v].add((int(node.firstChild.data.strip()) + 1, float(node.getAttribute("cost"))))

  for v in adja:
    for w in adja[v]:
      G.insertEdge(v, w[0], w[1])

  if G.order() == 0:
    print("error while loading the graph")
    return False

  return G

class GMLParser:
  '''
  Parser for gml-files
  '''
  def __init__(self):
    pass

  def write_gml(self, graph, filename, coord=None):
    '''
    Write the graph to a gml-file
    @param graph: SGraph
    @param filename: Filename to save the graph
    '''
    space = "  "
    with open(filename, 'w') as f:
      #f.write('"Graph generated by PyGraphEdit"\n')

      # Write the infos about the graph
      f.write("graph [\n")
      f.write('%slabel "%s"\n' % (space, graph.name))

      # Write the vertices
      for v in graph.getVIt():
        f.write("%snode [\n" % space)
        f.write("%4sid %d\n" % (space, int(v) - 1))

        # Write the vertex weight
        if isinstance(graph, SWGraph):
          f.write("%4sparameter %f\n" % (space, graph.getWeight(v)))

        # Write the coordinates
        if coord:
          f.write("%4sgraphics [\n" % space)
          f.write("%6sx %.2f\n" % (space, coord[v][0]))
          f.write("%6sy %.2f\n" % (space, coord[v][1]))
          f.write("%4s]\n" % space)

        f.write("%s]\n\n" % space)

      # Write the edges
      for e in graph.getEdgeSet():
        f.write("%sedge [\n" % space)
        f.write("%4ssource %d\n" % (space, int(e[0]) - 1))
        f.write("%4starget %d\n" % (space, int(e[1]) - 1))

        # Write the edge weight
        if isinstance(graph, SWGraph):
          f.write("%4sparameter %f\n" % (space, graph.getWeight(e)))

        f.write("%s]\n\n" % space)

      # Close the graph
      f.write("]\n")

  def read_gml(self, filename, graphtype="SWGraph"):
    '''
    Load the graph from the gml-file.
    @param filename: Name of the gml-file
    @param graphtype: Graphtype to generate
    @return: SGraph or SWGraph
    '''
    if graphtype == "SGraph":
      g = SGraph()
    else:
      g = SWGraph()

    coords = {}
    n = None
    x = None
    y = None
    e = None

    with open(filename, 'r') as f:
      inGraph = False
      inVertex = False
      inEdge = False
      inGraphics = False

      for line in f:
        line = line.strip()
        data = line.split(" ")

        # parse the header
        if data[0] == "graph" and data[1] == "[":
          # Starting a new graph
          inGraph = True

        elif data[0] == "node" and data[1] == "[":
          # Starting a new node
          inGraph = False
          inEdge = False
          inVertex = True

        elif data[0] == "edge" and data[1] == "[":
          # Starting a new edge
          inVertex = False
          inEdge = True
          e = None

        elif data[0] == "]":
          # End of the section
          if inGraphics:
            inGraphics = False

          elif inVertex:
            inVertex = False
            n = None

          elif inEdge:
            inEdge = False
            e = None

        elif data[0] == "graphics" and data[1] == "[":
          inGraphics = True

        elif data[0] == "LabelGraphics" and data[1] == "[":
          pass

        elif data[0] == "Line" and data[1] == "[":
          pass

        elif data[0] == "point" and data[1] == "[":
          pass

        else:
          # If the line and the value is not empty.
          if line and data[1] != "":
            # Set the types
            if '"' in data[1]:
              # If contains ", then clear it and set type to str.
              data[1] = str(data[1].replace('"', ''))

            elif '.' in data[1]:
              # If contains ., then set the type to float
              data[1] = float(data[1])
            else:
              try:
                data[1] = int(data[1])
              except:
                pass

            # Set the attributes
            if inVertex and data[0] == "id":
              g.insertVertex(data[1] + 1)
              n = data[1] + 1

            elif inVertex and inGraphics:
              if data[0] == "x":
                x = float(data[1])
              elif data[0] == "y":
                y = float(data[1])
              if x != None and y != None:
                coords[n] = (x, y)
                x = None
                y = None

            elif inVertex and data[0] == "parameter":
              g.setWeight(n, data[1])

            elif inEdge and (data[0] == "source" or data[0] == "target"):
              if e is None:
                e = data[1] + 1
              else:
                g.insertEdge(e, data[1] + 1)
                e = (e, data[1] + 1)
            elif inEdge and data[0] == "parameter":
              g.setWeight(e, data[1])

            elif inGraph and data[0] == "label":
              g.name = data[1]

    return g, coords

class GraphMLParser:
  """
  Parser for graphml-files
  """
  def __init__(self):
    pass

  def write(self, graph, fname, name=None):
    '''Save a graph in graphml-File'''
    doc = dom.Document()

    root = doc.createElement('graphml')
    doc.appendChild(root)

    graph_node = doc.createElement('graph')
    if name is None:
      graph_node.setAttribute('id', graph.name)
    else:
      graph_node.setAttribute('id', name)
    graph_node.setAttribute('edgedefault', 'undirected')
    root.appendChild(graph_node)

    # Add vertices
    for v in graph.getVIt():
      node = doc.createElement('node')
      node.setAttribute('id', str(v))
      graph_node.appendChild(node)

    # Add edges
    for e in graph.getEdgeSet():
      edge = doc.createElement('edge')
      edge.setAttribute('source', str(e[0]))
      edge.setAttribute('target', str(e[1]))
      graph_node.appendChild(edge)

    with open(fname, 'w') as f:
      f.write(doc.toprettyxml())#indent = '    '))

  def parse(self, fname, graphname, graphtype="SGraph"):
    """
    Load a graph from a file
    """
    doc = dom.parse(open(fname, 'r'))
    root = doc.getElementsByTagName("graphml")[0]
    graphs = root.getElementsByTagName("graph")
    for graph in graphs:
      name = graph.getAttribute('id')
      if name == graphname:
        if graphtype == "SGraph":
          g = SGraph(name=name)
        elif graphtype == "SWGraph":
          g = SWGraph(name=name)
        else:
          print("wrong type")
          return
        break

    # Get nodes
    for node in graph.getElementsByTagName("node"):
      g.insertVertex(int(node.getAttribute('id')))

    # Get edges
    for edge in graph.getElementsByTagName("edge"):
      source = int(edge.getAttribute('source'))
      dest = int(edge.getAttribute('target'))
      g.insertEdge(source, dest)

    return g

def getGraphNames(fname):
  '''
  Return all graphnames from a xml-file
  @param fname: Filename
  @return: List of the graph names in the file
  '''
  try:
    graphs = dom.parse(fname)
  except:
    print("unable to load file %s" % fname)
    return None

  graphnames = [0]
  for graph in graphs.firstChild.childNodes:
    if graph.nodeName == "graph" and graph.hasAttribute("name"):
      graphnames.append(graph.getAttribute("name"))

  if len(graphnames) == 1:
    graphnames.append(None)

  return graphnames

def getGraphNamesGML(fname):
  '''
  Return all graphnames from a gml-file
  @param fname: Filename
  @return: List of graph names
  '''
  graphnames = [0]
  try:
    with open(fname) as f:
      found_graph = False

      for line in f:
        line = line.strip("\n").strip(" ").strip("\r").strip("\t")
        parts = line.split(" ")

        if parts[0] == "graph" and parts[1] == "[":
          found_graph = True
        elif found_graph and parts[0] == "label":
          graphnames.append(parts[1])
  except:
    pass

  return graphnames

def getGraphNamesGraphml(fname):
  '''
  Return all graphnames from a graphml-file
  @param fname: Filename
  @return: List of the graph names in the file
  '''
  try:
    doc = dom.parse(open(fname, 'r'))
  except:
    return None

  graphnames = [0]
  root = doc.getElementsByTagName("graphml")[0]
  graphs = root.getElementsByTagName("graph")
  for graph in graphs:
    if graph.nodeName == "graph" and graph.hasAttribute("id"):
      graphnames.append(graph.getAttribute("id"))

  if len(graphnames) == 1:
    graphnames.append(None)

  return graphnames

def getGraphNamesdb(fname, numvertices=None):
  '''
  Return all graphnames from a database with a given order
  @param fname: Filename
  @param numvertices: Order of the graphs (default=None)
  @return: List of the graph names in the file
  '''
  try:
    connection = connect(fname, detect_types=PARSE_DECLTYPES)
  except:
    return None
  cursor = connection.cursor()

  register_adapter(SGraph, graphadapter)
  register_converter("SGraph", graphkonverter)
  register_adapter(dict, dictadapter)
  register_converter("dict", dictkonverter)

  if numvertices != None:
    cursor.execute("SELECT name FROM Graphen WHERE n=%d" % numvertices)
  else:
    try:
      cursor.execute("SELECT name FROM Graphen")
    except:
      return None

  try:
    names = cursor.fetchall()
  except:
    print("Datei nicht vorhanden oder falsches Format")
    return None
  n = [0]
  for name in names:
    n.append(name[0])

  if len(n) == 1:
    n.append(None)

  cursor.close()
  connection.close()
  return n

def graphadapter(graph):
  string = ""
  global tograph6
  if tograph6:
    string = ">>graph6<<" + graphtograph6(graph)
  else:
    for v in graph.getVIt():
      string += str("%s:" % str(v))
      for e in graph.getAdja()[v]:
        string += str("%s," % str(e))
      string += ";"

  return string

def graphkonverter(st):
  vertexset = set([])
  edgeset = set([])
  st = str(st, encoding='utf8')
  if st.startswith('>>graph6<<'):
    st = st[10:]
    G = parse_graph6(st)
    return G

  else:
    s = st.split(';')
    for t in s:
      if len(t) != 0:
        v, edges = t.split(':', 1)
        vertexset.add(int(v))
        edges = edges.split(',')
        for e in edges:
          if len(e) != 0:
            edgeset.add((int(v), int(e)))
    return SWGraph(V=vertexset, E=edgeset)

def dictadapter(d):
  string = ""
  for v in d.keys():
    string += str("%s:" % str(v))
    string += str("%s=%s" % (str(d[v][0]), str(d[v][1])))
    string += ";"

  return string

def dictkonverter(st):
  d = {}
  st = str(st, encoding='utf8')
  s = st.split(';')
  for t in s:
    if len(t) != 0:
      v, coords = t.split(':', 1)
      coords = coords.split('=')
      d[int(v)] = (float(coords[0]), float(coords[1]))
  return d

def graphtograph6(g):
  n = g.order()
  s = encodegraphsize(n)
  s += encodeAdja(g)
  return s

def encodegraphsize(n):
  BIAS6 = 63
  C6MASK = 63
  MAXBYTE = 126

  p = ""

  if n <= 62:
    p = str(chr(BIAS6 + n))
  elif n <= 258047:
    p = str(chr(126))
    p += str(chr(BIAS6 + (n >> 12)))
    p += str(chr(BIAS6 + ((n >> 6) & C6MASK)))
    p += str(chr(BIAS6 + (n & C6MASK)))
  else:
    p = str(chr(MAXBYTE))
    p += str(chr(MAXBYTE))
    p += str(chr(BIAS6 + (n >> 30)))
    p += str(chr(BIAS6 + ((n >> 24) & C6MASK)))
    p += str(chr(BIAS6 + ((n >> 18) & C6MASK)))
    p += str(chr(BIAS6 + ((n >> 12) & C6MASK)))
    p += str(chr(BIAS6 + ((n >> 6) & C6MASK)))
    p += str(chr(BIAS6 + (n & C6MASK)))

  return p

def encodeAdja(graph):
  result = ""
  s = ""
  k = 6
  for i, u in enumerate(graph.getV()):
    for j, v in enumerate(graph.getV()):
      if i > j:
        if graph.is_adjacent(u, v):
          s += "1"
        else:
          s += "0"
        k -= 1
        if k == 0:
          k = 6
          result += frombits(s)
          s = ""

  if len(s) != 0:
    s += (6 - len(s) % 6) * "0"
    result += frombits(s)

  return result

def frombits(bits):
  chars = []
  for b in range(int(len(bits) / 6)):
    byte = bits[b * 6 : (b + 1) * 6]
    chars.append(chr(int(''.join([str(bit) for bit in byte]), 2) + 63))
  return ''.join(chars)

def parse_graph6(s):
  """
  Read a simple undirected graph in graph6 format from string.
  @return: SGraph
  """
  def bits():
    """
    Return sequence of individual bits from 6-bit-per-value
    list of data values.
    """
    for d in data:
      for i in [5, 4, 3, 2, 1, 0]:
        yield (d >> i) & 1

  if s.startswith('>>graph6<<'):
    s = s[10:]

  data = graph6data(s)
  n, data = graph6n(data)
  nd = (n * (n - 1) // 2 + 5) // 6
  if len(data) != nd:
    print('Expected %d bits but got %d in graph6' % (n * (n - 1) // 2, len(data) * 6))
    return False

  G = SGraph()
  G.insertVertexSet([i for i in range(1, n + 1)])
  for (i, j), b in zip([(i, j) for j in range(1, n) for i in range(j)], bits()):
    if b:
      G.insertEdge(i + 1, j + 1)
  return G

def graph6data(s):
  """Convert graph6 character sequence to 6-bit integers."""
  v = [ord(c) - 63 for c in s]
  if min(v) < 0 or max(v) > 63:
    return None
  return v

def graph6n(data):
  """Read initial one or four-unit value from graph6 sequence.
  Return value, rest of seq."""
  if data[0] <= 62:
    return data[0], data[1:]
  return (data[1]<<12) + (data[2]<<6) + data[3], data[4:]

def graph6FiletoDB(filename, database):
  '''Saves all graphs in filename in a database'''
  with open(filename, 'r') as file:
    k = 0
    for graph6 in file:
      if len(graph6) != 0:
        graph6 = graph6.strip()

        graph = parse_graph6(graph6)
        gname = str(graph.order()) + "-" + "-%d" % k
        save_graphdb(graph, database, gname, graph6=True)
        k += 1
  return k

class LEDAParser:
  '''
  Parser for LEDA-files
  Only unweighted graphs!
  '''
  def __init__(self):
    pass

  def load_graph_LEDA(self, filename, graphtype="SWGraph"):
    '''
    Load the graph from the LEDA-file.
    @param filename: Name of the LEDA-file
    @return: SGraph
    '''
    graph = None
    vertices = False
    edges = False
    with open(filename, 'r') as f:
      for line in f:
        line = line.strip()

        if line == "LEDA.GRAPH":
          if graphtype == "SWGraph":
            graph = SWGraph()
          else:
            graph = SGraph()
        elif re.search("nodes", line):
          vertices = True
          edges = False
        elif re.search("edges", line):
          vertices = False
          edges = True

        if graph and vertices and line[0] == "|":
          l = line.split("|{")
          l = l[1].split("}|")
          v = int(l[0])
          graph.insertVertex(v)
        elif graph and edges and line[len(line) - 1] == "|":
          l = line.split(" ")
          graph.insertEdge(int(l[0]), int(l[1]))

      return graph

  def safe_graph_LEDA(self, graph, filename):
    '''
    Save a graph in the LEDA-format
    @param graph: SGraph, SWGraph is also possible but the weights will not be saved
    @param filename: Filename
    '''
    with open(filename, 'w') as f:
      f.write("# This file is generate by PyGraphEdit\n")

      # Write the header of the file
      f.write("LEDA.GRAPH\n")
      f.write("int\n")  # Type of the vertex-number
      f.write("void\n")  # Type of the edge-number
      f.write("-2\n")  # -1 directed graph; -2 undirected graph
      f.write("\n")

      # Write the vertices
      f.write("# nodes\n")
      f.write("%d\n" % graph.order())
      for v in graph.getV():
        f.write("|{%s}|\n" % str(v))

      # Write the edges
      f.write("# edges\n")
      f.write("%d\n" % graph.size())
      for e in graph.getEdgeSet():
        f.write("%d %d 0 |{}|\n" % (e[0], e[1]))
