# -*- coding: latin-1 -*-
"""
Classes for weighted undirected graph

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
"""

from copy import copy
import sys

from SGraph.SGraph import SGraph  # @UnresolvedImport


# ===========================================================================
inf = float('Inf')


# ===========================================================================
# ============= SWGraph =====================================================
# ===========================================================================
class SWGraph(SGraph):
  '''
  Class for an undirected graph with vertex and edge weights.
  Overwrites the SGraph-class.
  @param V: Vertex set
  @param weights: Dict with the vertex weights
  @param E: Set of the edges (set((u,v,weight)))
  @param gtype: Type of the graph (see modules.consts.py)
  @param name: Name of the graph
  '''
  def __init__(self, V=set(), weights={}, E=set(), gtype=None,
               name=None, loops=None):
    SGraph.__init__(self, V, E, name=name, loops=loops)
    if len(weights) != 0:
      self.__weights = weights
    else:
      self.__weights = {v: 1.0 for v in V}

    # Insert the edge weights
    self.__edgeweight = {v: {} for v in V}
    for e in E:
      if len(e) == 3:
        if e[0] <= e[1]:
          self.__edgeweight[e[0]][e[1]] = float(e[2])
        else:
          self.__edgeweight[e[1]][e[0]] = float(e[2])
      else:
        if e[0] <= e[1]:
          self.__edgeweight[e[0]][e[1]] = 1.0
        else:
          self.__edgeweight[e[1]][e[0]] = 1.0

  def copy(self):
    '''Return a copy of the current graph'''
    return SWGraph(set(self.getV()), copy(self.__weights),
                  copy(self.getEdgeSetWeight(withloops=True)),
                  self.graphtype, loops=copy(self.getLoops()))

  def insertEdge(self, u, v, weight=1.0):
    '''
    Insert the edge uv  with a given weight in the graph
    @param u: Endvertex of the edge
    @param v: Endvertex of the edge
    @param weight: Weight of the edge (default=1.0)
    '''
    SGraph.insertEdge(self, u, v)
    if u not in self.__edgeweight.keys() or v not in self.__edgeweight.keys():
      return False

    if self.is_adjacent(u, v):
      if u <= v:
        self.__edgeweight[u][v] = float(weight)
      else:
        self.__edgeweight[v][u] = float(weight)
    elif u == v and not self.hasLoop(v):
      self.__edgeweight[u][u] = float(weight)
    self.graphtype = None

  def insertVertex(self, v, weight=1.0):
    '''
    Insert the vertex v with a given weight in the graph
    @param v: New vertex
    @param weight: Weight of the vertex (default=1.0)
    '''
    SGraph.insertVertex(self, v)
    if not v in self.__weights.keys():
      self.__weights[v] = float(weight)
      self.__edgeweight[v] = {}
    self.graphtype = None

  def deleteEdge(self, e):
    '''
    Deletes the edge e in the graph
    @param e: Edge of the graph
    '''
    u = e[0]
    v = e[1]
    if u in self.getV() and v in self.getV() and self.is_adjacent(u, v):
      SGraph.deleteEdge(self, e)
      if u < v:
        del self.__edgeweight[u][v]
      elif u > v:
        del self.__edgeweight[v][u]
      self.graphtype = None

    elif u == v and self.numLoops(u) == 0:
      SGraph.deleteEdge(self, e)
      del self.__edgeweight[u][u]

      self.graphtype = None

  def deleteEdgeSet(self, F):
    '''
    Deletes the edge set F in the graph
    @param F: Edge subset of the graph
    '''
    for e in F:
      self.deleteEdge(e)

  def deleteVertex(self, v):
    '''
    Deletes the vertex v in the graph
    @param v: Vertex of the graph
    '''
    if v in self.getV():
      neighbors = copy(self.neighbors(v))
      for w in neighbors:
        self.deleteEdge((v, w))
      del self.__edgeweight[v]
      del self.__weights[v]
      SGraph.deleteVertex(self, v)
      self.graphtype = None
      return True
    else:
      return False

  def deleteVertexSet(self, X):
    '''
    Deletes the vertex subset X in the graph
    @param X: Vertex subset of the graph
    '''
    for v in X:
      self.deleteVertex(v)
    self.graphtype = None

  def contractEdge(self, e, withloop=False):
    '''
    Contract the edge e of the graph
    @param e: Edge e of the graph (tuple)
    '''
    u = e[0]
    v = e[1]
    self.deleteEdge(e)
    if v == u:  # If edge is a loop
      return

    loops_u = self.numLoops(u)
    loops_v = self.numLoops(v)

    for w in self.neighbors(v):
      if w not in self.neighbors(u):
        self.insertEdge(u, w, self.getWeight((v, w)))
      else:
        self.setWeight((u, w), self.getWeight((v, w)) + self.getWeight((u, w)))
    self.deleteVertex(v)
    if loops_u > 0 or loops_v > 0:
      self.insertEdge(u, u)
    self.graphtype = None

  def merge(self, u, v):
    '''
    Merge the vertices u and v
    '''
    for w in self.neighbors(v):
      if w != u and self.is_adjacent(u, w):
        self.setWeight((u, w), self.getWeight((u, w)) + self.getWeight((v, w)))
      elif w != u:
        self.insertEdge(u, w, self.getWeight((v, w)))
    self.deleteVertex(v)

  def setWeight(self, e, weight):
    '''
    Set the weight of an element (vertex or edge) of the graph
    @param e: Element of the graph (int oder tuple)
    @param w: New weight of the element
    '''
    if type(e) == int:
      self.__weights[e] = float(weight)
    else:
      if e[0] <= e[1]:
        self.__edgeweight[e[0]][e[1]] = float(weight)
      else:
        self.__edgeweight[e[1]][e[0]] = float(weight)

  def getWeight(self, e):
    '''
    Get the weight of an element (vertex or edge) of the graph
    @param e: Element of the graph (int oder tuple)
    '''
    if type(e) == int:
      return self.__weights.get(e, None)
    else:
      if not self.is_adjacent(e[0], e[1]):
        return inf
      else:
        if e[0] <= e[1]:
          return self.__edgeweight[e[0]][e[1]]
        else:
          return self.__edgeweight[e[1]][e[0]]

  def getEdgeSetWeight(self, withloops=False):
    '''
    Returns the edge set of the graph
    @return: Edge set (set((u,v,weight)))
    '''
    if withloops:
      return set([(u, v, self.getWeight([u, v]))
                  for u in self.__edgeweight.keys()
                  for v in self.__edgeweight[u] if u <= v])
    else:
      return set([(u, v, self.getWeight([u, v]))
                  for u in self.__edgeweight.keys()
                  for v in self.__edgeweight[u] if u < v])

  def renameVertex(self, u, v):
    '''Rename the vertex u in v'''
    if u not in self.getV() or v in self.getV():
      return False

    SGraph.renameVertex(self, u, v)
    self.__weights[v] = self.__weights[u]
    del self.__weights[u]

    self.__edgeweight[v] = self.__edgeweight[u]
    del self.__edgeweight[u]
    for w in self.__edgeweight.keys():
      if u in self.__edgeweight[w]:
        self.__edgeweight[w][v] = self.__edgeweight[w][u]
        del self.__edgeweight[w][u]

  def normalize(self, shift=0):
    '''
    Rename the vertices in the graph in increasing order starting with 1
    @param shift: Shift for the start value of the vertices
    '''
    V = list(self.getV())
    T = {V[i - 1]: i + shift for i in range(1, len(V) + 1)}
    self.__weights = {T[v]: self.__weights[v] for v in V}
    SGraph.normalize(self, shift)

  def convertSGraph(self, G):
    '''
    Convert a SGraph in a SWGraph
    @param G: SGraph
    '''
    SGraph.__init__(self, G.getV(), G.getEdgeSet(withloops=True))
    self.__weights = {v: 1.0 for v in G.getVIt()}
    self.__edgeweight = {v: {} for v in G.getVIt()}

    # Set the default edge weights
    for v, w in G.getEdgeSet(withloops=True):
      if v <= w:
        self.__edgeweight[v][w] = 1.0
      else:
        self.__edgeweight[w][v] = 1.0
    self.name = G.name
    self.graphtype = G.graphtype

  def dijkstra(self, s):
    '''
    Dijkstra-Algorithm to calculate shortest paths in the graph
    @param s: Startvertex
    @return: Dict with the distances from s
    '''
    import heapq as hq
    s -= 1
    n = self.order()
    Q = [(0, s)]
    d = [inf] * n
    d[s] = 0

    while len(Q) != 0:
      (_, u) = hq.heappop(Q)

      for v in range(n):
        if d[v] > d[u] + self.getWeight((u + 1, v + 1)):
          d[v] = d[u] + self.getWeight((u + 1, v + 1))
          hq.heappush(Q, (d[v], v))
    return d

  def __str__(self):
    '''Convert the graph as a string (it is possible to write print(SWGraph))'''
    if self.name == None:
      return "V: %s E: %s Loops: %s" % (str(self.__weights),
                                        str(self.__edgeweight),
                                        str(self.getLoops()))
    else:
      return "%s - V: %s E: %s Loops: %s" % (str(self.name),
                                             str(self.__weights),
                                             str(self.__edgeweight),
                                             str(self.getLoops()))


#===========================================================================
#===========================================================================
#===========================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
