'''
Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 18.09.2014

@author: Markus
@license: GPL (see LICENSE.txt)
@note: Usage: nosetests -v TestGraph.py
'''
from GraphFunctions import (components, connectedComponents, countTriangles,  # @UnresolvedImport
                            edgeconnectivity)  # @UnresolvedImport
from SGraph import SGraph  # @UnresolvedImport
from SpecialGraphs import diamant  # @UnresolvedImport


# ====================================================
# ========= Function from GraphParser ================
# ====================================================
def parse_graph6(s):
  """
  Read a simple undirected graph in graph6 format from string.
  @return: SGraph
  """
  def bits():
    """Return sequence of individual bits from 6-bit-per-value
    list of data values."""
    for d in data:
      for i in [5, 4, 3, 2, 1, 0]:
        yield (d >> i) & 1

  if s.startswith('>>graph6<<'):
    s = s[10:]

  data = graph6data(s)
  n, data = graph6n(data)
  nd = (n * (n - 1) // 2 + 5) // 6
  if len(data) != nd:
    print('Expected %d bits but got %d in graph6' % (n * (n - 1) // 2, len(data) * 6))
    return False

  G = SGraph()
  G.insertVertexSet([i for i in range(1, n + 1)])
  for (i, j), b in zip([(i, j) for j in range(1, n) for i in range(j)], bits()):
    if b:
      G.insertEdge(i + 1, j + 1)
  return G

def graph6data(s):
  """Convert graph6 character sequence to 6-bit integers."""
  v = [ord(c) - 63 for c in s]
  if min(v) < 0 or max(v) > 63:
    return None
  return v

def graph6n(data):
  """Read initial one or four-unit value from graph6 sequence.  
  Return value, rest of seq."""
  if data[0] <= 62:
    return data[0], data[1:]
  return (data[1] << 12) + (data[2] << 6) + data[3], data[4:]


# ====================================================
# ========= TEST =====================================
# ====================================================
def test_add_Vertex1():
  G = SGraph()
  G.insertVertex(1)
  G.insertVertex(2)
  G.insertVertex(3)
  assert set(G.getV()) == set([1, 2, 3])

def test_add_Vertex2():
  G = diamant()
  G.insertVertex(5)
  G.insertVertex(6)
  G.insertVertex(7)
  assert set(G.getV()) == set([1, 2, 3, 4, 5, 6])

def test_delete_Vertex1():
  G = SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)])
  assert G.deleteVertex(2) == True
  assert set(G.getV()) == set([1, 3, 4])
  assert G.getEdgeSet() == set([(1, 3), (3, 4)])

def test_delete_Vertex2():
  G = SGraph()
  assert G.deleteVertex(1) == False

def test_num_components():
  assert len(components(SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]))) == 1
  assert len(components(SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (3, 4)]))) == 1
  assert len(components(SGraph([1, 2, 3, 4, 5], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]))) == 2
  assert len(components(SGraph([1, 2, 3, 4, 5, 6], [(2, 3), (2, 4), (3, 4), (3, 5), (4, 5)]))) == 3

def test_num_connectedComponents():
  assert len(connectedComponents(SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]))) == 1
  assert len(connectedComponents(SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (3, 4)]))) == 1
  assert len(connectedComponents(SGraph([1, 2, 3, 4, 5], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]))) == 1
  assert len(connectedComponents(SGraph([1, 2, 3, 4, 5, 6], [(2, 3), (2, 4), (3, 4), (3, 5), (4, 5)]))) == 1
  assert len(connectedComponents(SGraph([1, 2, 3, 4, 5, 6], [(2, 3), (2, 4), (3, 4), (3, 5), (4, 5), (1, 6)]))) == 2

def test_countTriangles():
  assert countTriangles(SGraph([1, 2, 3, 4], [(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)])) == 2

def test_EdgeConnectivity():
  assert edgeconnectivity(parse_graph6("C~")) == 3
  assert edgeconnectivity(parse_graph6("Es\o")) == 3
  assert edgeconnectivity(parse_graph6("E`~w")) == 3
  assert edgeconnectivity(parse_graph6("E{Sw")) == 3
  assert edgeconnectivity(parse_graph6("Dr{")) == 3
  assert edgeconnectivity(parse_graph6("D^{")) == 3
  assert edgeconnectivity(parse_graph6("F?~v_")) == 3
  assert edgeconnectivity(parse_graph6("FwC^w")) == 3

  assert edgeconnectivity(parse_graph6("L`?M@_LBf_Q~Y}")) == 4
  assert edgeconnectivity(parse_graph6("H{_yqgj")) == 4
  assert edgeconnectivity(parse_graph6("H?B~vrw")) == 4
  assert edgeconnectivity(parse_graph6("Gs`zro")) == 4
  assert edgeconnectivity(parse_graph6("FqN~w")) == 4
  assert edgeconnectivity(parse_graph6("F}hXw")) == 4
  assert edgeconnectivity(parse_graph6("FN~~w")) == 4
  assert edgeconnectivity(parse_graph6("E}lw")) == 4
  assert edgeconnectivity(parse_graph6("Er~w")) == 4
  assert edgeconnectivity(parse_graph6("E^~w")) == 4
  assert edgeconnectivity(parse_graph6("D~{")) == 4
  assert edgeconnectivity(parse_graph6("F}oxw")) == 4

  assert edgeconnectivity(parse_graph6("IsaBzx{^?")) == 5
  assert edgeconnectivity(parse_graph6("F}l~w")) == 5
  assert edgeconnectivity(parse_graph6("F^~~w")) == 5
  assert edgeconnectivity(parse_graph6("E~~w")) == 5

  assert edgeconnectivity(parse_graph6("G~z\z{")) == 6
  assert edgeconnectivity(parse_graph6("H}q|r|}")) == 6
  assert edgeconnectivity(parse_graph6("F~~~w")) == 6

  assert edgeconnectivity(parse_graph6("G~~~~{")) == 7
  assert edgeconnectivity(parse_graph6("H~z\z~~")) == 7

