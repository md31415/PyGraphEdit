# -*- coding: latin-1 -*-
"""
Classes for simple undirected graphs

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
"""

from copy import copy
from random import (choice as random_choice)
import sys
from itertools import combinations

from modules.consts import graphType  # @UnresolvedImport


# ===========================================================================
inf = float('Inf')


# ===========================================================================
# ============= SGraph ======================================================
# ===========================================================================
class SGraph():
  '''
  Class for an undirected graph without vertex and edge weights
  @param V: Vertex set
  @param E: Edge set
  @param gtype: Type of the graph (see modules.consts.py)
  @param name: Name of the graph
  '''
  def __init__(self, V=set(), E=set(), gtype=None, name=None,
               data=None, loops=None):
    self.__adja = {v: set() for v in V}
    for e in E:
      self.__adja[e[0]].add(e[1])
      self.__adja[e[1]].add(e[0])

    # The loops dict gives the multiplicity of the loops of the graph
    if loops is None:
      self.__loops = {v: 0 for v in V}
    else:
      self.__loops = loops
    self.name = name
    self.graphtype = gtype
    self.__maxcliquesize__ = None
    self.data = data  # Var for additional data

  def getAdja(self):
    '''Returns the adjacency dictionary of the graph'''
    return self.__adja

  def delAdja(self):
    '''Deletes all edges of the graph'''
    self.__adja = {v: set() for v in self.getVIt()}

  def getVIt(self):
    '''Returns a iterator of the vertex set'''
    return iter(self.__adja.keys())

  def getV(self):
    '''Returns the vertex set'''
    return list(self.__adja.keys())

  def delV(self):
    '''Deletes the vertex set'''
    self.__adja = {}

  def getEdgeSet(self, withloops=False):
    '''Returns the edge set'''
    if withloops:
      return set([(u, v) for u in self.__adja.keys()
                  for v in self.__adja[u] if u <= v])
    else:
      return set([(u, v) for u in self.__adja.keys()
                  for v in self.__adja[u] if u < v])

  def getLoops(self):
    '''Return the loops dict'''
    return self.__loops

  def getIncidenceMatrix(self):
    '''Returns the incidence matrix of the graph as a list of lists'''
    n = self.order()
    m = [[0 for i in range(0, n + 1)]]
    edges = self.getEdgeSet()
    for e in edges:
      d = [0]
      for i in range(1, n + 1):
        if i == e[0] and e[0] < e[1] or i == e[1] and e[1] < e[0]:
          d.append(1)
        elif i == e[0] and e[0] > e[1] or i == e[1] and e[1] > e[0]:
          d.append(-1)
        else:
          d.append(0)
      m.append(d)
    return m

  def order(self):
    '''Returns the order of the graph'''
    return len(self.__adja)

  def size(self, withloops=False):
    '''Returns the size of the graph'''
    return len(self.getEdgeSet(withloops))

  def getIncidentEdges(self, v, withloops=False):
    '''
    Returns the set of the incident edges of v
    @param v: Vertex of the graph
    '''
    if v not in self.__adja:
      return set([])
    return set([(v, w) for w in self.neighbors(v, withloops)])

  def numLoops(self, w):
    '''
    Return the number of loops incident to the vertex w
    '''
    return self.__loops[w]

  def neighbors(self, v, withloops=False):
    '''
    Returns the open neighborhood of the vertex v
    @param v: Vertex of the graph
    @return: Open neighborhood of v
    '''
    try:
      if withloops:
        return self.__adja[v]
      else:
        return set(self.__adja[v]) - set([v])
    except Exception as e:
      print("Error '{0}' occured. Arguments {1}.".format(e.message, e.args))
      return []

  def dist_d_neighbors(self, v, d=1):
    '''
    Returns the closed d-neighborhood of v
    @param v: Vertex of the graph
    @param d: Distance
    '''
    pass
#     N = set([])
#     for u in self.getVIt():
#       _, length = find_shortest_path(self, v, u)
#       if length != None and length <= d:
#         N.add(u)
#     return N

  def neighbors_set(self, A):
    '''
    Returns the closed neighborhood of the vertex subset A
    @param A: Vertex subset A
    '''
    vertices = map(self.neighbors, A)
    return set.union(set(A), *vertices)

  def open_neighbors_set(self, A):
    '''
    Returns the open neighborhood of the vertex subset A
    @param A: Vertex subset A
    '''
    vertices = map(self.neighbors, A)
    return set.union(set(A), *vertices) - set(A)

  def open_total_neighbors_set(self, A, withloops=False):
    '''
    Returns the open total neighborhood of the vertex subset A
    @param A: Vertex subset A
    '''
    vertices = map(self.neighbors, A, [withloops]*len(A))
    return set.union(*vertices)

  def privateNeighbors(self, v, X):
    '''
    Returns the private neighbor set of the vertex v in respect to the set X
    @param v: Vertex of the graph
    @param X: Vertex subset of the graph
    @return: Set of private neighbors of v
    '''
    if v not in self.__adja or v not in X:
      return set([])

    Y = X - set([v])
    N = set([])
    for w in self.getVIt():
      if (w not in Y and (w == v or w in self.neighbors(v))
          and not self.neighbors_set(set([w])) & Y):
        N.add(w)
    return N

  def Delta(self, W, withloops=False):
    '''
    Returns the set of the edges which are completely in W
    @param A: Vertex subset W
    '''
    F = set([])
    for v in W:
      for w in self.__adja[v]:
        if w in W and v < w:
          F.add((v, w))
        elif w in W and v == w and withloops:
          F.union(set([(w, w) for _ in range(1, self.numLoops(w))]))
    return F

  def delta(self, W):
    '''
    Returns the set of the edges which leave W
    @param A: Vertex subset W
    '''
    F = []
    for v in W:
      for w in self.__adja[v]:
        if w not in W:
          F.append((v, w))
    return F

  def degree(self, v, withloops=False):
    '''
    Returns the degree of v
    @param v: Vertex of the graph
    '''
    try:
      if withloops:
        return len(self.__adja[v])
      else:
        return len(set(self.__adja[v]) - set([v]))
    except Exception as e:
      print("Error '{0}' occured. Arguments {1}.".format(e.message, e.args))
      return 0

  def get_degrees(self, withloops=False):
    '''Returns the degree of the graph as a dict with the vertices as the keys of it'''
    return {v: self.degree(v, withloops) for v in self.__adja}

  def get_min_degree(self, withloops=False):
    '''Returns the minimum degree of the graph'''
    if self.order() > 0:
      return min(self.get_degrees(withloops).values())
    else:
      return 0

  def get_max_degree(self, withloops=False):
    '''Returns the maximum degree of the graph'''
    if self.order() > 0:
      return max(self.get_degrees(withloops).values())
    else:
      return 0

  def hasEdge(self, e):
    '''
    Tests if the graph has the edge e
    @param e: Edge e (tuple)
    @return: Bool
    '''
    if len(e) != 2:
      raise TypeError
    if e[0] not in self.__adja or e[1] not in self.__adja:
      return False
    return bool(self.is_adjacent(e[0], e[1]))

  def hasLoop(self, v):
    '''Has the vertex v a loop'''
    return v in self.__adja[v]

  def is_adjacent(self, u, v):
    '''
    Test if the vertices u and v are adjacent
    @param u: Vertex u
    @param v: Vertex v
    @return: Bool
    '''
    return (u in self.__adja and v in self.__adja and
            (u in self.__adja[v] or v in self.__adja[u]))

  def is_clique(self, W):
    '''
    Tests if the vertex subset W is a clique in the graph
    @param W: Vertex subset
    @return: Bool
    '''
    n = len(W)
    for u in W:
      if len(set(self.neighbors_set([u])) & set(W)) != n:
        return False
    return True

  def insertEdge(self, u, v):
    '''
    Insert the edge uv in the graph
    @param u: End vertex of the edge
    @param v: End vertex of the edge
    '''
    if u not in self.__adja or v not in self.__adja:  # or u == v:
      return False
    self.graphtype = None
    if u == v:
      self.__adja[u].add(v)
      self.__loops[u] += 1
      return True
    if not self.is_adjacent(u, v):
      self.__adja[u].add(v)
      self.__adja[v].add(u)
      return True

  def insertVertex(self, v):
    '''
    Insert the vertex v in the graph
    @param v: New vertex
    '''
    if not v in self.__adja:
      self.__adja[v] = set()
      self.__loops[v] = 0
      self.graphtype = None
      return True
    else:
      return False

  def insertVertexSet(self, W):
    '''
    Insert all vertices of W in the graph
    @param W: Set of new vertices
    '''
    for v in W:
      if v not in self.__adja:
        self.insertVertex(v)

  def __add__(self, x):
    '''
    Overwrites the +-operator to add an edge to the graph
    @param e: New edge of the graph
    @return: SGraph
    '''
    H = self.copy()
    H.graphtype = None
    if type(x).__name__ == "tuple":
      H.insertEdge(x[0], x[1])
    elif x in self.__adja:
      H.insertVertex(x)
    elif type(x).__name__ == "set" or type(x).__name__ == "list":
      H.insertVertexSet(x)
    else:
      return None
    return H

  def randomVertex(self):
    '''Returns a random vertex of the graph'''
    return random_choice(self.getV())

  def randomEdge(self):
    '''Returns a random edge of the graph'''
    return random_choice(list(self.getEdgeSet(withloops=False)))

  def deleteEdge(self, e):
    '''
    Deletes the edge e in the graph
    @param e: Edge e
    '''
    if not self.hasEdge(e):
      return
    u = e[0]
    v = e[1]
    self.graphtype = None
    try:
      if u == v:
        if self.__loops[u] == 1:
          self.__adja[u] -= {u}
        self.__loops[u] -= 1
        return
      if u in self.neighbors(v):
        self.__adja[u] -= {v}
        self.__adja[v] -= {u}
    except Exception as ex:
      print("Error '{0}' occured. Arguments {1}.".format(ex.message, ex.args))

  def deleteEdgeSet(self, F):
    '''
    Deletes the edge set F in the graph
    @param F: Edge subset of the graph
    '''
    for e in F:
      self.deleteEdge(e)

  def deleteVertex(self, v):
    '''
    Deletes the vertex v in the graph
    @param v: Vertex of the graph
    '''
    if v in self.__adja:
      for w in self.__adja[v]:
        if w != v:
          self.__adja[w] -= {v}
      del self.__adja[v]
      del self.__loops[v]
      self.graphtype = None
      return True
    else:
      return False

  def deleteVertexSet(self, X):
    '''
    Deletes the vertex subset X in the graph
    @param X: Vertex subset of the graph
    '''
    for v in X:
      self.deleteVertex(v)
    self.graphtype = None

  def __sub__(self, x):
    '''
    Overwrites the --operator to delete an edge or a vertex or a vertex subset
    @param x: Vertex (int) or edge (tuple) or vertex subset (set)
    @return: SGraph
    '''
    H = self.copy()
    if (isinstance(x, tuple) and
        tuple(sorted(x)) in self.getEdgeSet(withloops=True)):
      H.deleteEdge(x)
    elif isinstance(x, int) and x in self.__adja:
      H.deleteVertex(x)
    elif x <= set(self.getV()):
      H.deleteVertexSet(x)
    H.graphtype = None
    return H

  def contractEdge(self, e, withloops=False):
    '''
    Contract the edge e of the graph
    @param e: Edge e of the graph (tuple)
    '''
    if not self.hasEdge(e):
      return
    u = e[0]
    v = e[1]

    self.deleteEdge(e)
    if u == v:  # if e is a loop
      return

    for w in self.neighbors(v):
      self.__adja[w] = (self.__adja[w] - {v}) | {u}
    self.__adja[u] |= self.__adja[v]
    self.deleteVertex(v)
    if withloops:
      self.insertEdge(u, u)
    self.graphtype = None

  def contractNode(self, v):
    '''
    Contract the node v of the graph
    That is to remove the node and add edges
    pairwise between the neighbors of v
    @param v: Node to be contracted
    REQUIRES: itertools.combinations
    '''
    if v not in self.__adja.keys():
      return
    r = self.degree(v)
    if r <= 1:
      return
    task = combinations(self.neighbors(v), 2)
    self.deleteVertex(v)
    # Insert an edge between each pair of 
    # neighborhood vertices of v
    for (u, w) in task:
      self.insertEdge(u, w)
    self.graphtype = None
    
  def subdivision(self, e, w=None):
    '''
    Subdivision of the edge e
    @param e: Edge of the graph
    @param w: Name of the new vertex
    '''
    if not self.hasEdge(e):
      return
    u = e[0]
    v = e[1]
    self.deleteEdge(e)
    if u == v:  # if e is a loop
      return

    if w is None:
      w = max(self.getV()) + 1
    self.insertVertex(w)
    self.insertEdge(u, w)
    self.insertEdge(v, w)
    self.graphtype = None

  def loopRemoveVertex(self, v):
    '''
    Remove the vertex v and add loops to every adjacent vertex
    @param v: Vertex of the graph
    '''
    if v not in self.getV():
      return

    # Insert the loops
    for w in self.neighbors(v, withloops=False):
      self.insertEdge(w, w)
    self.deleteVertex(v)

  def loopRemoveEdge(self, e):
    '''
    Extract the edge e and add to loop remove to adjacent vertices
    '''
    if e not in self.getEdgeSet(withloops=False):
      return

    # Delete the edge
    self.deleteEdge(e)

    # Insert the loops
    for w in self.neighbors(e[0], withloops=False):
      self.insertEdge(w, w)
    for w in self.neighbors(e[1], withloops=False):
      self.insertEdge(w, w)

    # Delete the adjacent vertices
    self.deleteVertex(e[0])
    self.deleteVertex(e[1])

  def removeNeighborEdges(self, v):
    '''
    Deletes all incident edges of v
    @param v: Vertex of the graph
    '''
    N = list(self.neighbors(v))
    for u in N:
      for w in N:
        if u != w and self.is_adjacent(u, w):
          self.deleteEdge((u, w))

  def __truediv__(self, e):
    '''Contract the vertex or the edge
    @param e: Vertex (int) or edge (tuple) of the graph
    '''
    if isinstance(e, int):
      if e in self.__adja:
        H = self.copy()
        N = self.neighbors(e)
        H.deleteVertex(e)
        for v in N:
          for w in N:
            if v != w and not H.is_adjacent(v, w):
              H.insertEdge(v, w)
        return H
    else:
      if self.hasEdge(e):
        H = self.copy()
        H.contractEdge(e)
        H.graphtype = None
        return H
    return None

  def product(self, H):
    '''
    Calculate the Cartesian product of self and H
    @param H: SGraph
    @return: SGraph
    '''
    V = [(u, v) for u in self.__adja for v in H.getAdja()]
    T = {V[i]:i + 1 for i in range(0, len(V))}
    E = set([(T[x], T[y]) for x in V for y in V
           if x < y and ((x[0] == y[0] and x[1] in H.neighbors(y[1])) or
           (x[0] in self.neighbors(y[0]) and x[1] == y[1]))])
    return SGraph(set(range(1, len(V) + 1)), E, graphType.CARTESIAN)

  def tensorProduct(self, H):
    '''
    Calculates the tensor product of self and H
    @param H: SGraph
    @return: SGraph
    '''
    V = [(u, v) for u in self.__adja for v in H.getAdja()]
    T = {V[i]: i + 1 for i in range(0, len(V))}
    E = set([(T[x], T[y]) for x in V for y in V
           if (x < y and (x[1] in H.neighbors(y[1])
                          and x[0] in self.neighbors(y[0])))])
    return SGraph(set(range(1, len(V) + 1)), E, graphType.TENSOR)

  def lexicographicalProduct(self, H):
    '''
    Calculate the lexicographical product of self and H
    @param H: SGraph
    @return: SGraph
    '''
    V = [(u, v) for u in self.__adja for v in H.getAdja()]
    T = {V[i]: i + 1 for i in range(0, len(V))}
    E = set([(T[x], T[y]) for x in V for y in V
           if (x < y and (x[0] in self.neighbors(y[0]) or
                          (x[0] == y[0] and x[1] in H.neighbors(y[1]))))])
    return SGraph(set(range(1, len(V) + 1)), E, graphType.LEXI)

  def strongProduct(self, H):
    '''
    Calculate the strong product of self and H
    @param H: SGraph
    @return: SGraph
    '''
    V = [(u, v) for u in self.__adja for v in H.getAdja()]
    T = {V[i]: i + 1 for i in range(0, len(V))}
    E = set([(T[x], T[y]) for x in V for y in V
           if x < y and ((x[0] == y[0] and x[1] in H.neighbors(y[1])) or
           (x[0] in self.neighbors(y[0]) and x[1] == y[1]) or
           (x[0] in self.neighbors(y[0]) and x[1] in H.neighbors(y[1])))])
    return SGraph(set(range(1, len(V) + 1)), E, graphType.STRONG)

  def join(self, H):
    ''''
    Generates the join of self and H
    @param H: SGraph
    @return: SGraph
    '''
    G = self.copy()
    n = self.order()
    G.insertVertexSet([n + v for v in H.getAdja()])
    for e in H.getEdgeSet(withloops=True):
      G.insertEdge(n + e[0], n + e[1])

    V = G.getV()
    for i in range(1, n + 1):
      for j in range(n + 1, len(V) + 1):
        G.insertEdge(V[i - 1], V[j - 1])
    G.graphtype = graphType.JOIN
    return G

  def corona(self, H):
    '''
    Generates the corona of self and H
    @param H: SGraph
    @return: SGraph
    '''
    G = self.copy()
    n = self.order()
    m = H.order()
    for i in range(0, n):
      G.insertVertexSet([n + i * m + v for v in H.getAdja()])
      for e in H.getEdgeSet(withloops=True):
        G.insertEdge(n + i * m + e[0], n + i * m + e[1])
      for v in H.getVIt():
        G.insertEdge(i + 1, n + i * m + v)
    G.graphtype = graphType.CORONA
    return G

  def __mul__(self, H):
    '''
    Overwrite the *-operator and calculates the Cartesian product of self and H
    @param H: SGraph
    @return: SGraph
    '''
    return self.product(H)

  def disjointUnion(self, H):
    ''''
    Calculate the union of self and H
    @param H: SGraph
    @return: SGraph
    '''
    A = self.copy()
    A.normalize()
    B = H.copy()
    B.normalize(A.order())
    return SGraph(set(A.getV()) | set(B.getV()),
                  set(A.getEdgeSet(withloops=True)) | set(B.getEdgeSet(withloops=True)))

  def __or__(self, H):
    '''
    Overwrites the |-operator and calculate the union of self and H
    @param H: SGraph
    @return: SGraph
    '''
    return self.disjointUnion(H)

  def renameVertex(self, u, v):
    '''
    Rename a vertex u
    @param u: The vertex which should be renamed
    @param v: The new name
    '''
    if u not in self.__adja or v in self.__adja:
      return False

    self.__adja[v] = self.__adja[u]
    self.__loops[v] = self.__loops[u]
    del self.__adja[u]
    del self.__loops[u]
    for w in self.__adja.keys():
      if u in self.__adja[w]:
        self.__adja[w].add(v)
        self.__adja[w].remove(u)

    return True

  def normalize(self, shift=0):
    '''
    Rename the vertices in the graph in increasing order starting with 1
    @param shift: Shift for the start value of the vertices
    '''
    V = list(self.getV())
    T_n = {V[i - 1]: i + shift for i in range(1, self.order() + 1)}
    self.__adja = {T_n[v]: set(map(lambda x: T_n[x], self.__adja[v])) for v in V}

  def spanningSubgraph(self, edges):
    '''
    Returns the spanning subgraph
    @param edges: Edge subset of the graph
    @return: SGraph
    '''
    other = SGraph(self.getV(), set(edges))
    return other

  def numIso(self):
    '''Returns the number of isolated vertices of the graph'''
    deg = self.get_degrees(withloops=False)
    return sum([1 for d in deg if deg[d] == 0])

  def copy(self):
    '''Returns a copy of the graph'''
    return SGraph(self.getV(), self.getEdgeSet(withloops=True),
                  self.graphtype, loops=copy(self.getLoops()))

  def print_me(self):
    '''Print the graph'''
    print(self)

  def __str__(self):
    '''Convert the graph as a string (it is possible to write print(SGraph))'''
    if self.name is None:
      return_string = "V: %s E: %s" % (str(self.getV()), str(self.__adja))
    else:
      return_string = "%s - V: %s E: %s" % (str(self.name), str(self.getV()),
                                            str(self.__adja))
    return return_string


#===========================================================================
#===========================================================================
#===========================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
