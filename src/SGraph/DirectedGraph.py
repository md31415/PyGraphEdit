# -*- coding: latin-1 -*-
"""
Class for simple directed Graphs

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
"""

from sqlite3 import connect, register_adapter, register_converter, PARSE_DECLTYPES
import sys

from modules.consts import graphType


# =========================================================================================
inf = float('Inf')


# =========================================================================================
# ============= DGraph ====================================================================
# =========================================================================================
class DGraph():
  '''
  Class for a directed graph without vertex and edge weights
  @param V: Vertex set
  @param E: Edge set (set((u,v)))
  @param gtype: Graph type (see modules.consts.py)
  '''
  def __init__(self, V=set(), E=set(), gtype=None, name=None):
    self.__adja = dict((v, set()) for v in V)
    for e in E:
      self.__adja[e[0]].add(e[1])

    self.name = name
    self.graphtype = gtype
    self.__maxcliquesize__ = None

  def getVIt(self):
    '''Returns a iterator of the vertex set'''
    return iter(self.__adja.keys())

  def getV(self):
    '''
    Returns the vertex set of the graph
    '''
    return list(self.__adja.keys())

  def getEdgeSet(self):
    '''
    Returns the edge set of the graph
    '''
    return set([(u, v) for u in self.__adja.keys() for v in self.__adja[u]])

  def neighbors(self, v):
    '''
    Returns the open neighborhood of the graph
    @param v: Vertex of the graph
    '''
    if not v in self.__adja:
      return []
    return self.__adja[v]

  def is_adjacent(self, u, v):
    '''
    Tests if the vertices u and v are adjacent
    @param v: First vertex
    @param u: Second vertex
    '''
    return v in self.neighbors(u)

  def insertEdge(self, u, v):
    '''
    Insert an edge between u and v
    @param u: First vertex
    @param v: Second vertex
    '''
    if u not in self.getV() or v not in self.getV() or u == v:
      return False
    if not self.is_adjacent(u, v):
      self.__adja[u].add(v)
    self.graphtype = None

  def deleteEdge(self, e):
    '''
    Delete the edge e in the graph
    @param e: Edge of the graph
    '''
    u = e[0]
    v = e[1]
    try:
      if v in self.neighbors(u):
        self.__adja[u] -= {v}
    except:
      pass

    self.graphtype = None

  def copy(self):
    '''
    Returns a copy of the graph
    @return: DGraph
    '''
    return DGraph(self.getV(), self.getEdgeSet())

  def __str__(self):
    '''
    Convert the graph in a string
    @return: String
    '''
    if self.name == None:
      return "V: %s E: %s" % (str(self.getV()), str(self.__adja))
    else:
      return "%s - V: %s E: %s" % (str(self.name), str(self.getV()), str(self.__adja))


#======================================================================================
#======================================================================================
#======================================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
