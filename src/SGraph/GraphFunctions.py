# -*- coding: latin-1 -*-
"""
Functions to manipulate graphs and calculate invariants.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

@author: Markus Dod
@license: GPL (see LICENSE.txt)
"""

from copy import copy
from math import sqrt
import queue
from random import (choice as random_choice, randint,
                    random as random_rand,
                    shuffle as random_shuffle)
import sys

from SGraph.DirectedGraph import DGraph  # @UnresolvedImport
from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.SWGraph import SWGraph  # @UnresolvedImport

from lib.MaximumMatching import maxMatching as computeMaxMatching  # @UnresolvedImport
from lib.tools import (binomial, kpowerset,  # @UnresolvedImport
                       PriorityQueue, randomUniqueSubset)  # @UnresolvedImport

from modules.consts import graphType  # @UnresolvedImport


# ==================================================================
inf = float('Inf')


# ==================================================================
# =========== Generate special graphs ==============================
# ==================================================================
def diamant():
  '''
  Generates the diamond graph
  @return: SGraph
  '''
  return SGraph(V=[1, 2, 3, 4], E=set([(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]))

def path(n):
  '''
  Generates a path with n vertices
  @param n: Length of the path
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)), set((u, u + 1) for u in range(1, n)),
                graphType.PATH)

def cycle(n):
  ''''
  Generates a cycle with n vertices
  @param n: Length of the cycle
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)),
                set((u, u + 1) for u in range(1, n)) | {(1, n)},
                graphType.CYCLE)

def anticycle(n):
  '''
  Generates a anti-cycle with n vertices
  @param n: Length of the anti-cycle
  @return: SGraph
  '''
  if n == 3:
    return SGraph(set([1, 2, 3]))
  else:
    graph = SGraph(set([v for v in range(1, n + 1)]))
    for v in range(2, n):
      for u in range(1, n + 1):
        if u != v - 1 and u != v + 1 and u != v:
          graph.insertEdge(u, v)
  graph.graphtype = graphType.ANTICYCLE
  return graph

def completeGraph(n):
  '''
  Generates a complete graph with n vertices
  @param n: Length of the complete graph
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)),
                set((u, v) for u in range(1, n) for v in range(u + 1, n + 1)),
                graphType.COMPLETE)

def completekPartite(p):
  '''
  Generates a complete k-partite graph
  @param p: List of the size of the k-partite sets
  @return: SGraph
  '''
  n = [sum(p[:i]) for i in range(len(p))]
  G = SGraph(set(range(1, sum(p) + 1)))
  for i, r in enumerate(p):
    for j, s in enumerate(p):
      if j > i:
        for v in range(r):
          for w in range(s):
            G.insertEdge(n[i] + v + 1, n[j] + w + 1)
  G.graphtype = graphType.KPARTITE
  return G

def completeBipartite(p, q):
  '''
  Generates a complete bipartite graph
  @param p: Size of the first partition
  @param q: Size of the second partition
  @return: SGraph
  '''
  return SGraph(set(range(1, p + q + 1)),
                set((u, v) for u in range(1, p + 1) for v in range(p + 1, p + q + 1)),
                graphType.COMPBIPARTITE)

def star(n):
  '''
  Generates a star with n vertices
  @param n: Number of vertices
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)),
                set((1, u) for u in range(2, n + 1)), graphType.STAR)

def wheel(n):
  '''
  Generates a wheel with n vertices
  @param n: Number of vertices
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)),
                set((u, u + 1) for u in range(2, n)) | {(2, n)} | set((1, u) for u in range(2, n + 1)),
                graphType.WHEEL)

def fan(n):
  '''
  Generates a fan with n vertices
  @param n: Number of vertices
  @return: SGraph
  '''
  return SGraph(set(range(1, n + 1)),
                set((u, u + 1) for u in range(2, n)) | set((1, u) for u in range(2, n + 1)),
                graphType.FAN)

def friendship(n):
  '''
  Generates the friedship (or Dutch-Windmill) graph F_n with 2n+1 vertices
  @param n: Number of the wings of the Dutch-Windmill
  '''
  g = path(2)
  for _ in range(2, n + 1):
    g = g.disjointUnion(path(2))
  g = g.join(SGraph(V=set([2 * n + 1])))
  g.graphtype = graphType.FRIENDSHIP
  g.normalize()
  return g

def book(n):
  '''
  Generates the book graph B_n with 2n+2 vertices
  @param n: Number of the pages
  '''
  g = path(2)
  for _ in range(2, n + 2):
    g = g.disjointUnion(path(2))

  for i in range(3, 2 * n + 3):
    if i % 2 == 1:
      g.insertEdge(1, i)
    else:
      g.insertEdge(2, i)
  g.graphtype = graphType.BOOK
  return g

def Sun(n):
  '''
  Sun graph with 2n vertices
  @param n: Size of the clique of the sun graph
  '''
  graph = completeGraph(n)
  for i in range(1, n):
    graph.insertVertex(n + i)
    graph.insertEdge(i, n + i)
    graph.insertEdge(i + 1, n + i)
  graph.insertVertex(2 * n)
  graph.insertEdge(1, 2 * n)
  graph.insertEdge(n, 2 * n)

  return graph

def Sunlet(n):
  '''
  Generate a sunlet graph with 2n vertices (cycle with n vertices and
  n pendant eges
  @param n: Size of the cycle
  '''
  graph = cycle(n)
  for i in range(1, n + 1):
    graph.insertVertex(n + i)
    graph.insertEdge(i, n + i)
  return graph

def RisingSun(n):
  '''
  Rising sun graph with 2n-1 vertices
  @param n: Size of the clique of the sun graph
  '''
  graph = completeGraph(n)
  for i in range(1, n):
    graph.insertVertex(n + i)
    graph.insertEdge(i, n + i)
    graph.insertEdge(i + 1, n + i)

  return graph

def Firecracker(n, k):
  '''
  Generates a firecracker with n stars of the size k
  @param n: Number of the stars
  @param k: Size of the stars
  '''
  graph = star(k)
  for _ in range(n - 1):
    graph = graph.disjointUnion(star(k))
  for i in range(0, n - 1):
    graph.insertEdge(i * k + 2, (i + 1) * k + 2)
  graph.graphtype = graphType.FIRECRACKER
  return graph

def BananaTree(n, k):
  '''
  Generates a banana tree with n stars of the size k
  @param n: Number of the stars
  @param k: Size of the stars
  '''
  graph = SGraph(set([1]))
  for i in range(n):
    graph = graph.disjointUnion(star(k))
    graph.insertEdge(1, i * k + 3)
  graph.graphtype = graphType.BANANATREE
  return graph

def Centipede(n):
  '''
  Generates a centipede with 2*n vertices
  @param n: Number of the foots of the centipede
  '''
  graph = path(n)
  for i in range(1, n + 1):
    graph.insertVertex(n + i)
    graph.insertEdge(i, n + i)
  graph.graphtype = graphType.CENTIPEDE
  return graph

def grid(m, n):
  '''
  Generates a grid
  @param m: Number of rows
  @param n: Number of columns
  @return: SGraph
  '''
  return path(m).product(path(n))

def torus(m, n):
  '''
  Generates a torus
  @param m: Number of rows
  @param n: Number of columns
  @return: SGraph
  '''
  return cycle(m).product(path(n))

def queensGraph(n):
  '''
  Generates a nxn queens graph
  @param n: Number of columns and rows
  @return: SGraph
  '''
  g = completeGraph(n).product(completeGraph(n))
  for v in range(0, n ** 2):
    r = int(v / n)
    c = v % n
    for dx in range(1, n):
      if (r + dx < n and c + dx < n and int((v + dx * n + dx) / n) == r + dx
          and (v + dx * n + dx) % n == c + dx):
        g.insertEdge(v + 1, v + dx * n + dx + 1)
      if (r + dx < n and c - dx >= 0 and int((v + dx * n - dx) / n) == r + dx
          and (v + dx * n - dx) % n == c - dx):
        g.insertEdge(v + 1, v + dx * n - dx + 1)

  return g

def rooksGraph(n):
  '''
  Generates a nxn rooks graph
  @param n: Number of columns and rows
  @return: SGraph
  '''
  return completeGraph(n).product(completeGraph(n))

def bishopsGraph(n):
  '''
  Generates a nxn bishops graph
  @param n: Number of columns and rows
  @return: SGraph
  '''
  g = SGraph(list(range(1, n ** 2 + 1)))
  for v in range(0, n ** 2):
    r = int(v / n)
    c = v % n
    for dx in range(1, n):
      if (r + dx < n and c + dx < n and int((v + dx * n + dx) / n) == r + dx
          and (v + dx * n + dx) % n == c + dx):
        g.insertEdge(v + 1, v + dx * n + dx + 1)
      if (r + dx < n and c - dx >= 0 and int((v + dx * n - dx) / n) == r + dx
          and (v + dx * n - dx) % n == c - dx):
        g.insertEdge(v + 1, v + dx * n - dx + 1)

  return g

def kPath(n, k):
  '''
  Generate a simple k-path with n vertices
  @param n: Number of vertices
  @param k: k in the k-path
  @return: SGraph
  '''
  G = SGraph(set(range(1, n + 1)))
  for u in range(1, n - k + 1):
    for i in range(1, k + 1):
      G.insertEdge(u, u + i)
  for u in range(n - k + 1, n):
    for v in range(u + 1, n + 1):
      G.insertEdge(u, v)
  G.graphtype = graphType.SKPATH
  return G

def kStar(n, k):
  '''
  Generate a simple k-star with n vertices
  @param n: Number of vertices
  @param k: k in the k-star
  @return: SGraph
  '''
  G = completeGraph(k)
  for i in range(k + 1, n + 1):
    G.insertVertex(i)
    for j in range(1, k + 1):
      G.insertEdge(i, j)
  G.graphtype = graphType.KSTAR
  return G

def splitGraph(k, l):
  '''
  Generate a split graph
  @param k: Size of the clique
  @param l: Size of the independent set
  @return: Split graph
  '''
  G = completeGraph(k)
  for i in range(1, l + 1):
    G.insertVertex(k + i)
    for j in range(1, k + 1):
      G.insertEdge(j, k + i)
  return G

def kCycle(n, k):
  '''
  Generate a simple k-cycle with n vertices
  @param n: Number of vertices
  @param k: k in the k-cycle
  @return: SGraph
  '''
  G = kPath(n, k)
  for i in range(0, k):
    for j in range(0, k - i):
      G.insertEdge(G.order() - i, j + 1)
  G.graphtype = graphType.KCYCLE
  return G


# ==================================================================
# =========== Random graphs ========================================
# ==================================================================
def randomGraph(n, p):
  '''
  Generates a random graph with n vertices and edge probability p
  or a graph with n vertices and p randomly choosen edges.
  @param n: Number of vertices
  @param p: Edge probability (float) or number of edges (int)
  @return: SGraph
  '''
  G = SGraph(set(range(1, n + 1)))

  # If p is a probability
  if isinstance(p, float):
    for u in range(1, n):
      for v in range(u + 1, n + 1):
        if random_rand() < p:
          G.insertEdge(u, v)
    return G

  # Insert p edges
  elif isinstance(p, int):
    # Check some easy cases
    if p == (n * (n - 1)) / 2:
      return completeGraph(n)
    elif p > (n * (n - 1)) / 2:
      return SGraph()
    else:
      # Insert p random edges
      for _ in range(1, p + 1):
        # Search for two non-adjacent vertices u and v
        while True:
          u = random_choice(list(G.getV()))
          while True:
            v = random_choice(list(G.getV()))
            if v != u:
              break
          if not G.is_adjacent(u, v):
            break
        G.insertEdge(u, v)
      return G

def geometricRandomGraph(n, r=0.25, length=600):
  '''
  Generates a geometric random graph. Two vertices are adjacent if they have
  a distance less or equal r*length.
  @param n: Number of vertices
  @param r: Distance for the adjacency calculation (Between 0 and 1)
  @param length: Size of the square
  @return: SGraph, Coordniates of the vertices (dict)
  '''
  from random import uniform

  # Generate the coordinates
  coord = dict([])
  for v in range(1, n + 1):
    coord[v] = (uniform(0, length), uniform(0, length))

  # Insert an edge if the vertices are close enough
  g = SGraph(list(coord.keys()))
  for v in g.getV():
    for w in g.getV():
      if (w != v and
          sqrt((coord[w][0] - coord[v][0]) ** 2 + (coord[w][1] - coord[v][1]) ** 2) <= r * length):
        g.insertEdge(v, w)

  return g, coord

def randomTree(n):
  '''
  Generates a random tree
  @param n: Number of the vertices of the tree
  '''
  graph = SGraph(list(range(1, n + 1)))
  for v in range(2, n + 1):
    u = randint(1, v - 1)
    graph.insertEdge(u, v)
  return graph

def randomTreePrueferCode(n):
  '''
  Generates a random tree with the Pruefer-Code
  @param n: Number of the vertices of the tree
  @return: SGraph, Pruefer-Code
  '''
  # Generate the Pruefer-code
  import random
  code = []
  vertices = [v for v in range(1, n + 1)]
  for i in range(1, n - 1):
    code.append(random.choice(vertices))
  pruefercode = copy(code)

  # Generate the graph from the code
  g = SGraph()
  b = []
  while len(code) != 0:
    for i in range(1, n + 1):
      if i not in code and i not in b:
        u = i
        break
    g.insertVertex(u)
    g.insertVertex(code[0])
    g.insertEdge(u, code[0])
    code.remove(code[0])
    b.append(u)

  # Insert the last vertex and the corresponding edge
  uv = list(set(range(1, n + 1)) - set(b))
  if uv[0] not in g.getV():
    g.insertVertex(uv[0])
  if uv[1] not in g.getV():
    g.insertVertex(uv[1])
  g.insertEdge(uv[0], uv[1])

  return g, pruefercode

def randomkPath(n, k):
  '''
  Generate a random simple k-path with n vertices
  @param n: Number of vertices
  @param k: k in the k-path
  @return: SGraph
  '''
  G = completeGraph(k + 1)
  handled = [i for i in range(1, k + 2)]
  e = tuple([i for i in range(1, k + 1)])
  j = k + 2
  while True:
    possiblities = kpowerset(handled[len(handled) - k - 1:], k)
    try:
      possiblities.remove(e)
    except Exception:
      pass
    e = random_choice(possiblities)
    G.insertVertex(j)
    for v in e:
      G.insertEdge(j, v)
    if j == n:
      break
    handled = list(e)
    handled.append(j)
    j += 1

  G.graphtype = graphType.KPATH
  return G

def barabasiAlbertGraph(n, m):
  '''
  Return random graph using Barab�si-Albert preferential attachment model.
  A graph of n vertices is grown by attaching new vertices each with m
  edges that are preferentially attached to existing vertices with high degree.
  @param n: Number of vertices
  @param m: Number of edges to attach from a new vertex to existing vertices
  '''
  if m < 1 or  m >= n:
    print("Barab�si-Albert graph must have m>=1 and m<n, m=%d,n=%d" % (m, n))
    return

  # Add m initial vertices
  G = SGraph(list(range(1, m + 1)))
  G.name = "Barabasi-Albert-Graph (%s, %s)" % (n, m)

  # Target vertices for new edges
  targets = list(range(1, m + 1))

  # List of existing vertices, with vertices repeated once for each adjacent edge
  repeated_nodes = []

  # Start adding the other n-m vertices
  source = m + 1
  while source <= n:
    # Insert the vertex and the edges
    G.insertVertex(source)
    for v in targets:
      G.insertEdge(source, v)

    # Add one vertex to the list for each new edge just created.
    repeated_nodes.extend(targets)

    # The new vertex "source" has m edges to add to the list.
    repeated_nodes.extend([source] * m)

    # Now choose m unique vertices from the existing vertices
    targets = randomUniqueSubset(repeated_nodes, m)
    source += 1

  return G

def powerlawClusterGraph(n, m, p):
  ''''
  Holme and Kim algorithm for growing graphs.
  @param n: Number of vertices
  @param m: Number of edges to attach from a new vertex to existing vertices
  @param p: Probability to add a triangle
  '''
  if m < 1 or n < m:
    print("powerlawClusterGraph must have m > 1 and m < n, m= %d, n= %d" % (m, n))
    return

  if p > 1 or p < 0:
    print("p must be in [0,1], p = %f" % (p))
    return

  G = SGraph(list(range(1, m + 1)))
  G.name = "Powerlaw-Cluster-Graph"
  repeated_nodes = G.getV()
  source = m + 1
  while source <= n:
    G.insertVertex(source)

    possible_targets = randomUniqueSubset(repeated_nodes, m)

    # do one preferential attachment for new node
    target = possible_targets.pop()
    G.insertEdge(source, target)
    repeated_nodes.append(target) # add one node to list for each new link
    count = 1
    while count < m:  # add m-1 more new links
      if random_rand() < p:  # clustering step: add triangle
        neighborhood = [nbr for nbr in G.neighbors(target)
                        if not G.is_adjacent(source, nbr) and not nbr == source]
        if neighborhood:  # if there is a neighbor without a link
          nbr = random_choice(neighborhood)
          G.insertEdge(source, nbr)  # add triangle
          repeated_nodes.append(nbr)
          count += 1
          continue  # go to top of while loop

      # else do preferential attachment step if above fails
      target = possible_targets.pop()
      G.insertEdge(source, target)
      repeated_nodes.append(target)
      count += 1

    repeated_nodes.extend([source] * m)  # add source node to list m times
    source += 1
  return G

def randomLobster(n, p1, p2):
  '''
  Generate a random lobster
  @param n: Number of vertices in the backbone
  @param p1: Probability of adding an edge to the backbone
  @param p2: Probability of adding an edge one level beyond backbone
  '''
  llen = int(2 * random_rand() * n + 0.5)
  L = path(llen)
  L.name = "random_lobster(%d, %.2f, %.2f)" % (n, p1, p2)

  # build caterpillar: add edges to path graph with probability p1
  current_vertex = llen
  for i in range(llen):
    if random_rand() < p1: # add fuzzy caterpillar parts
      current_vertex += 1
      L.insertVertex(current_vertex)
      L.insertEdge(i, current_vertex)
      if random_rand() < p2: # add crunchy lobster bits
        current_vertex += 1
        L.insertVertex(current_vertex)
        L.insertEdge(current_vertex - 1, current_vertex)
  return L

def randomRegularGraph(n, d):
  '''
  Generate a random regular graph of n nodes each with degree d.
  @param n: Number of vertices
  @param d: Degree
  '''
  from collections import defaultdict

  if (n * d) % 2 != 0:
    print("n * d must be even")
    return

  if not 0 <= d < n:
    print("the 0 <= d < n inequality must be satisfied")
    return

  def _suitable(edges, potential_edges):
    # Helper subroutine to check if there are suitable edges remaining
    # If False, the generation of the graph has failed
    if not potential_edges:
      return True
    for s1 in potential_edges:
      for s2 in potential_edges:
        # Two iterators on the same dictionary are guaranteed
        # to visit it in the same order if there are no
        # intervening modifications.
        if s1 == s2:
          # Only need to consider s1-s2 pair one time
          break
        if s1 > s2:
          s1, s2 = s2, s1
        if (s1, s2) not in edges:
          return True
    return False

  def _try_creation():
    # Attempt to create an edge set
    edges = set()
    stubs = list(range(1, n + 1)) * d

    while stubs:
      potential_edges = defaultdict(lambda: 0)
      random_shuffle(stubs)
      stubiter = iter(stubs)
      for s1, s2 in zip(stubiter, stubiter):
        if s1 > s2:
          s1, s2 = s2, s1
        if s1 != s2 and ((s1, s2) not in edges):
          edges.add((s1, s2))
        else:
          potential_edges[s1] += 1
          potential_edges[s2] += 1

      if not _suitable(edges, potential_edges):
        return None # failed to find suitable edge set

      stubs = [node for node, potential in potential_edges.items() for _ in range(potential)]
    return edges

  # Even though a suitable edge set exists,
  # the generation of such a set is not guaranteed.
  # Try repeatedly to find one.
  edges = _try_creation()
  while edges is None:
    edges = _try_creation()

  G = SGraph()
  G.name = "random_regular_graph(%s, %s)" % (d, n)
  for e in edges:
    G.insertVertex(e[0])
    G.insertVertex(e[1])
    G.insertEdge(e[0], e[1])

  return G


# ==================================================================
# ========== Tests whether the graph is a special graph ============
# ==================================================================
def isRegular(graph):
  '''
  Tests if the graph is regular
  @param graph: SGraph
  '''
  deg = graph.get_degrees()
  return min(list(deg.values())) == max(list(deg.values()))

def isPlanar(graph):
  '''
  Tests if a graph is planar
  TODO::Implement a test algorithm like Hopcraft-Tarjan-Algorithm or call ogdf
  @param graph: SGraph
  '''
  return graph.size() <= 3 * (graph.order() - 2)

def isEulersch(graph):
  '''
  Tests if the graph is a Euler graph
  @param graph: SGraph
  '''
  if not connected(graph):
    return False
  deg = graph.get_degrees()
  for v in deg:
    if deg[v] % 2 == 1:
      return False
  return True

def isCordal(graph):
  '''
  Using the lex-BFS to test if the graph is cordal
  @param graph: SGraph
  '''
  if not connected(graph):
    return False

  lexbfsorder = lbfs(graph)

  for i, v in enumerate(lexbfsorder):
    for j in range(i - 1, -1, -1):
      w = lexbfsorder[j]
      if graph.is_adjacent(v, w):
        Nv = set([])
        Nw = set([])
        for u in lexbfsorder[0:i]:
          if u != w and graph.is_adjacent(v, u):
            Nv.add(u)

        for u in lexbfsorder[0:j]:
          if graph.is_adjacent(w, u):
            Nw.add(u)

        if not Nv <= Nw:
          return False
        break

  return True

def isSelfcentral(graph):
  '''
  Tests if the graph is selfcentral
  @param graph: SGraph
  '''
  return graph.order() == len(list(center(graph)))

def isComplete(graph):
  '''
  Tests if the graph is a complete graph
  @param graph: SGraph
  '''
  n = graph.order()
  degree = graph.degree
  for v in graph.getVIt():
    if degree(v) != n - 1:
      return False

  return True

def isTree(graph):
  '''
  Tests if the graph is a tree
  @param graph: SGraph
  '''
  return len(components(graph)) == 1 and graph.size() == graph.order() - 1

def isPath(graph):
  '''
  Tests if the graph is a path
  @param graph: SGraph
  '''
  if graph.order() == 1:
    return True
  if graph.order() == 2 and graph.is_adjacent(graph.getV()[0], graph.getV()[1]):
    return True
  degrees = graph.get_degrees()
  return isTree(graph) and list(degrees.values()).count(1) == 2

def isCycle(graph):
  '''
  Tests if the graph is a cycle
  @param graph: SGraph
  '''
  degrees = graph.get_degrees()
  return list(degrees.values()).count(2) == graph.order()

def isAntiCycle(graph):
  '''
  Tests if the graph is a anti-cycle
  @param graph: SGraph
  '''
  g = complement(graph)
  return isCycle(g)

def iskStar(graph, k=1):
  '''
  Tests if the graph is a (n,k)-star
  @param G: SGraph
  @param k: k of the (n,k)-star
  '''
  g = kLineGraph(graph, k + 1)
  return g.order() == graph.order() - k and isComplete(g)

def isBipartite(graph):
  '''
  Tests if the graph is bipartite
  @param graph: SGraph
  '''
  if graph.order() == 0:
    return True
  elif graph.size() == 0:
    return False
  else:
    for g in components(graph):
      v = next(g.getVIt())
      d = distanceVector(g, v)
      for e in g.getEdgeSet():
        if d[e[0]] % 2 == d[e[1]] % 2:
          return False
    graph.graphtype = graphType.BIPARTITE
    return True

def getBipartition(graph):
  '''
  Get the bipartition of the graph
  @param graph: SGraph
  @return: Bool (if bipartite), bipartition
  '''
  colorArr = {}

  # Try to set colors for every component
  for g in components(graph):
    src = g.getV()[0]
    colorArr[src] = 1

    q = queue.LifoQueue()
    q.put(src)

    while not q.empty():
      u = q.get()
      N = g.neighbors(u)
      for v in N:
        if v not in colorArr:
          colorArr[v] = 1 - colorArr[u]
          q.put(v)
        else:
          if colorArr[v] == colorArr[u]:
            return False, {}
  return True, colorArr

def isCompleteBipartite(graph):
  '''
  Tests if the graph is a complete bipartite graph
  @param graph: SGraph
  '''
  if graph.size() == 0:
    return False

  # Calculate the bipartition
  flag, bipartition = getBipartition(graph)
  if not flag:
    return False

  # Calculate the size of the two biparition sets
  set1 = 0
  set2 = 0
  for v in bipartition:
    if bipartition[v] == 0:
      set1 += 1
    else:
      set2 += 1

  # Check if the graph has enough edges
  return graph.size() == set1 * set2

def getkPartiteSizes(graph):
  '''
  Returns the size of the partitions of an complete k-partite graph
  @param graph: SGraph
  @return: List of the sizes
  '''
  partitionsizes = []
  c = 0
  W = set(graph.getV())
  for v in graph.getVIt():
    if v in W:
      c += 1
      W.remove(v)
      for w in graph.getVIt():
        if w in W and not graph.is_adjacent(v, w):
          c += 1
          W.remove(w)
      partitionsizes.append(c)
      c = 0
  return partitionsizes

def iskPath(graph):
  '''
  Tests if the graph is a k-path
  @param graph: SGraph
  '''
  # Tests if the graph is a k-tree
  _, vertexorder = iskTree(graph)
  if vertexorder != False:
    deg = graph.get_degrees()
    return list(deg.values()).count(graph.__maxcliquesize__ - 1) == 2
  else:
    return False

def isskPath(graph):
  '''
  Tests if the graph is a simple k-path
  @param graph: SGraph
  @return: k of the k-path (zero if it is not a k-path)
  '''
  degrees = graph.get_degrees()
  if max(degrees.values()) % 2 != 0 and min(degrees.values()) % 2 != 0:
    return 0

  n = graph.order()
  k = min(degrees.values())

  # Check the adjacency according to the linear ordered vertices
  for u in range(1, n - k + 1):
    for i in range(1, k + 1):
      if not graph.is_adjacent(u, u + i):
        return 0
  for u in range(n - k + 1, n):
    for v in range(u + 1, n + 1):
      if not graph.is_adjacent(u, v):
        return 0

  graph.graphtype = graphType.KPATH
  return k

def iskCycle(graph):
  '''
  Tests if the graph is a k-cycle
  @param graph: SGraph
  @return: k of the k-cycle (zero if it is not a k-cycle)
  '''
  degrees = graph.get_degrees()
  if min(degrees.values()) != max(degrees.values()):
    return 0

  deg = min(degrees.values())
  import math
  k = math.ceil(deg / 2)
  if graph.order() > 2 * k and deg % 2 != 0:
    return 0

  _, flag = iskTree(graph, k)
  if flag:
    return 0

  N = sorted(graph.neighbors(min(graph.getV())))
  C = [v for i, v in enumerate(N) if i < k - 1]
  C.append(min(graph.getV()))
  if not graph.is_clique(C):
    return 0

  if graph.order() > 2 * k:
    g = graph.copy()
    g.deleteVertexSet(C)
    h = kLineGraph(g, k + 1)
    if not isPath(h):
      return 0

  graph.graphtype = graphType.KCYCLE
  return k

def iskTree(graph, k=None):
  '''
  Tests if the graph is a k-tree
  @param graph: SGraph
  @param k: k of the k-tree (default=None)
  @return: False if the graph is not a k-tree, otherwise the list of vertices
  '''
  if k is None:
    cliques = find_all_cliques(graph)
    max_k = 0
    for c in cliques:
      if len(c) > max_k:
        max_k = len(c)
    graph.__maxcliquesize__ = max_k
    k = max_k - 1

  if k == 1 and not isTree(graph):
    return 0, False
  vertexorder = []
  g = graph.copy()

  # Choose the root of the tree
  w = None
  degrees = g.get_degrees()
  if min(degrees.values()) < k:
    return 0, False
  for v in degrees.keys():
    if degrees[v] == k:
      w = v
      N = list(g.neighbors(w))
      if not g.is_clique(N):
        return 0, False
      break

  # If no vertex is found, then it is no k-tree
  if w is None:
    return 0, False

  # Generate decomposition
  while g.order() > k + 1:
    degrees = g.get_degrees()
    if not k in degrees.values():
      return 0, False
    for v in [v for v in degrees.keys() if degrees.get(v) == k]:
      if v != w:
        N = list(g.neighbors(v))
        if not g.is_clique(N):
          return 0, False
        vertexorder.append((v, list(g.getAdja()[v])))
        g.deleteVertex(v)
        break

  if k == 2:
    graph.graphtype = graphType.TWOTREE
  return w, vertexorder


# ==================================================================
# =========== Manipulate the current graph =========================
# ==================================================================
def complement(graph):
  '''
  Returns the complement of graph
  @param graph: SGraph
  @return: SGraph
  '''
  V = graph.getV()
  return SGraph(V, set([(u, v) for u in V for v in V if u < v and not graph.is_adjacent(u, v)]))

def lineGraph(graph):
  '''
  Returns the line graph of a given graph
  @param graph: SGraph
  @return: SGraph
  '''
  X = list(map(set, list(graph.getEdgeSet())))
  m = graph.size()
  H = SGraph(set(range(1, m + 1)))
  for i in range(0, m - 1):
    for j in range(i + 1, m):
      if X[i] & X[j] != set():
        H.insertEdge(i + 1, j + 1)
  return H

def kLineGraph(graph, k):
  '''
  Returns the k-linegraph of a graph
  @param graph: SGraph
  @return: SGraph
  '''
  w, vertexorder = iskTree(graph, k - 1)
  g = SGraph()
  if vertexorder != False:
    vertexorder_new = []
    vertexorder_new.append(graph.neighbors(w).union(set([w])))
    for c in vertexorder:
      tmp = copy(c[1])
      tmp.append(c[0])
      vertexorder_new.append(set(tmp))
    for i, c in enumerate(vertexorder_new):
      g.insertVertex(i + 1)
    for i in range(0, len(vertexorder_new) - 1):
      for j in range(i + 1, len(vertexorder_new)):
        if len(vertexorder_new[i].intersection(vertexorder_new[j])) == k - 1:
          g.insertEdge(i + 1, j + 1)
  return g

def inducedSubgraph(graph, X):
  '''
  Returns the (vertex) induced subgraph
  @param graph: SGraph
  @param X: Vertex subset
  @return: SGraph
  '''
  H = SGraph(X)
  for e in graph.getEdgeSet(withloops=True):
    if set(e) <= X:
      H.insertEdge(*e)
  return H

def edgeInducedSubgraph(F):
  '''
  Returns the edge-induced subgraph
  @param F: Edge subset
  @return: SGraph
  '''
  V = set([])
  for e in F:
    V.add(e[0])
    V.add(e[1])
  return SGraph(V, F)


# ==================================================================
# ============= Informations about the graph =======================
# ==================================================================
def component(graph, v):
  """
  Returns the component containing vertex v.
  @param graph: SGraph
  @param v: Vertex v
  @return: SGraph
  """
  X = set()
  Y = {v}
  Z = {v}
  while Z != set():
    for w in Z:
      Y |= graph.neighbors(w)
      X |= {w}
    Z = Y - X
  return inducedSubgraph(graph, Y)

def components(graph):
  '''
  Returns the components of the graph
  @param graph: SGraph
  @return: List of SGraphs
  '''
  C = []
  V = set(graph.getV())
  while V != set():
    v = V.pop()
    H = component(graph, v)
    C.append(H)
    V -= set(H.getV())
  return C

def connectedComponents(graph):
  '''
  Returns the connected components of the graph
  @param graph: SGraph
  @return: List of SGraphs
  '''
  C = []
  V = set(graph.getV())
  while V != set():
    v = V.pop()
    H = component(graph, v)
    if H.order() > 1:
      C.append(H)
    V -= set(H.getV())
  return C

def is_bridge(graph, e):
  '''
  Tests if the edge e is a bridge in the graph
  @param e: Edge e
  @return: Bool
  '''
  G = graph.copy()
  k = components(G)
  G.deleteEdge(e)
  return len(components(G)) > k

def countTriangles(graph):
  '''
  Counts the number of triangles in the graph
  @param graph: SGraph
  @return: Number of triangles
  '''
  t = 0
  neighbors = graph.neighbors
  is_adjacent = graph.is_adjacent
  for u in graph.getVIt():
    for v in neighbors(u):
      for w in neighbors(v):
        if u != w and is_adjacent(w, u):
          t += 1
  return int(t / 6)

def clusterCoefficient(graph):
  '''
  Returns the cluster coefficient of the graph
  @param graph: SGraph
  @return: Cluster coefficient
  '''
  if graph.order() < 3:
    return 0
  else:
    return countTriangles(graph) / binomial(graph.order(), 3)

def graphdensity(graph):
  '''
  Returns the density of the graph
  @param G: SGraph
  @return: Density
  '''
  n = graph.order()
  if n > 1:
    return 2 * graph.size() / (n * (n - 1))
  else:
    return 0

def numSpanningTree(graph, method="Kirchhoffs theorem", q=None):
  '''
  Returns the number of spanning trees
  @param graph: SGraph
  @param method: Method for the calculation (use Kirchhoffs theorem
  or evaluate the Tutte polynomial)
  @return: Number of spanning trees of the graph
  '''
  N = 1

  if method == "Kirchhoffs theorem":
    try:
      import numpy as np
    except Exception:
      N = "Please install numpy"
    else:
      C = components(graph)
      if len(C) == 1:
        L = laplacianMatrix(graph)
        n = len(L) - 1
        matrix = np.array(L[0][:n])
        for i in range(1, n):
          matrix = np.vstack([matrix, L[i][:n]])
        N = np.linalg.det(np.matrix(matrix))
      else:
        for H in C:
          L = laplacianMatrix(H)
          n = len(L) - 1
          if n > 1:
            matrix = np.array(L[0][:n])
            for i in range(1, n):
              matrix = np.vstack([matrix, L[i][:n]])
            N = N * np.linalg.det(np.matrix(matrix))

  else:
    from lib.TuttePoly import tutte as computeTuttePoly # @UnresolvedImport
    from sympy import symbols
    poly = computeTuttePoly(graph)
    x = symbols('x')
    y = symbols('y')
    N = poly.subs({x: 1, y: 1})

  if q != None:
    q.put(N)
  return N

def minimalSpanningTree(graph):
  '''
  Implements the algorithm from Prim
  @param graph: SWGraph
  '''
  if not isinstance(graph, SWGraph):
    return "Only avaliable for SWGraphs"

  n = graph.order()
  U = [1]
  E = list(graph.getEdgeSetWeight())
  treeEdges = set([])
  treeWeight = 0

  while len(U) != n:
    possibleedges = filter(lambda x: (x[0] in U) ^ (x[1] in U), E)
    sortededges = sorted(possibleedges, key=lambda x: x[2])
    treeWeight += sortededges[0][2]
    treeEdges.add((sortededges[0][0], sortededges[0][1]))
    U += [sortededges[0][1 if sortededges[0][0] in U else 0]]
  return treeEdges, treeWeight

def maximalSpanningTree(graph):
  '''
  Implements the algorithm from Prim
  @param graph: SWGraph
  '''
  if not isinstance(graph, SWGraph):
    return "Only avaliable for SWGraphs"

  n = graph.order()
  U = [1]
  E = list(graph.getEdgeSetWeight())
  treeEdges = set([])
  treeWeight = 0

  while len(U) != n:
    possibleedges = filter(lambda x: (x[0] in U) ^ (x[1] in U), E)
    sortededges = sorted(possibleedges, key=lambda x: x[2], reverse=True)
    treeWeight += sortededges[0][2]
    treeEdges.add((sortededges[0][0], sortededges[0][1]))
    U += [sortededges[0][1 if sortededges[0][0] in U else 0]]
  return treeEdges, treeWeight

def alpha(graph, q=None):
  """
  Returns the independence number of the graph.
  (Needs exponentially many steps!)
  Calculation with max(alpha(G-v), alpha(G-N[v])+1).
  @param graph: SGraph
  """
  a = 0
  if graph.order() == 1:
    a = 1
  elif graph.order() == 0:
    a = 0
  else:
    v = next(graph.getVIt())
    N = graph.neighbors(v) | {v}
    a = max(_alpha(graph - v), _alpha(graph - N) + 1)
  if q:
    q.put(a)
  return a

def _alpha(graph):
  """
  Returns the independence number the graph. (Needs exponentially many steps!)
  @param graph: SGraph
  """
  if graph.order() == 1:
    return 1
  elif graph.order() == 0:
    return 0
  else:
    v = next(graph.getVIt())
    N = graph.neighbors(v) | {v}
    return max(_alpha(graph - v), _alpha(graph - N) + 1)

def omega(graph, q=None):
  """
  Returns the clique number of graph
  @param graph: SGraph
  """
  max_c = 0
  for c in find_all_cliques(graph):
    if len(c) > max_c:
      max_c = len(c)

  if q:
    q.put(max_c)
  return max_c

def Delta(graph):
  """
  Maximum degree of the graph.
  @param graph: SGraph
  """
  return max([graph.degree(v) for v in graph.getVIt()])

def delta(graph):
  """
  Minimum degree of the graph.
  @param graph: SGraph
  """
  return min([graph.degree(v) for v in graph.getVIt()])

def minimumDegreeVertex(graph):
  '''
  Returns a random vertex of minimum degree
  @param graph: SGraph
  '''
  mindegree = graph.get_min_degree()
  for v in graph.getVIt():
    if graph.degree(v) == mindegree:
      return v

def lbfs(graph):
  S = [set(copy(graph.getV()))]
  output = []
  while len(S) != 0:
    v = S[0].pop()
    if len(S[0]) == 0:
      del S[0]

    output.append(v)

    processed = []
    remove = None
    for w in graph.neighbors(v):
      for i, D in enumerate(S):
        if w in D:
          if i not in processed:
            processed.append(i)
            S.insert(i, set([]))
            # Intices der bereits verwendeten Mengen anpassen
            for j in range(0, len(processed)):
              if processed[j] >= i:
                processed[j] += 1

            S[i + 1].remove(w)
            if len(D) == 0:
              remove = i + 1
            S[i].add(w)
            break
          else:
            S[i].remove(w)
            if len(S[i]) == 0:
              remove = i
            S[i - 1].add(w)
            break
      if remove != None:
        del S[remove]
        remove = None
  return output

def find_artikulations(graph, start=1):
  '''
  Find the articulations in the graph
  @param graph: Graph
  @param start: Start vertex for searching (default=1)
  '''
  global v_id
  global startpoint
  global pre_order
  global startcount
  global artikulations

  # set of the articulations
  artikulations = set([])

  # Visitor index for every vertex
  v_id = 0

  # Preorder-numbering
  pre_order = {}
  for v in graph.getVIt():
    pre_order[v] = 0

  # start vertex of the search
  startpoint = start

  # Number of child of the start vertex
  startcount = 0

  # Call the DFS-function
  visit(graph, startpoint)

  if startcount > 1:
    artikulations.add(startpoint)

  return artikulations

def visit(graph, v):
  '''
  Recursiv function for articulation search, uses DFS
  @param graph: SGraph
  @param v: Vertex of the graph
  '''
  global startpoint
  global v_id
  global pre_order
  global startcount

  v_id += 1
  pre_order[v] = v_id
  min_order = v_id

  for n_p in graph.open_neighbors_set([v]):  # getAdja()[v]:
    son = n_p

    if pre_order[son] == 0:
      if v == startpoint:
        startcount += 1

      m = visit(graph, son)
      if m < min_order:
        min_order = m
      if m >= pre_order[v] and v != startpoint:
        artikulations.add(v)

    elif pre_order[son] < min_order:
      min_order = pre_order[son]

  return min_order

def calculateMinEdgeCover(graph, q=None):
  '''Calculate a minimum edge cover of the graph'''
  matchingedges, matchingsize = computeMaxMatching(graph)
  if 2 * matchingsize == graph.order():
    return matchingedges, matchingsize

  uncoveredVertices = set(graph.getV()) - set([v for e in matchingedges for v in e])
  for v in uncoveredVertices:
    for w in graph.neighbors(v):
      matchingedges.add((v, w))
      matchingsize += 1
      break

  if len(uncoveredVertices) != 0:
    return set([]), 0
  return matchingedges, matchingsize

def find_all_cliques(graph, q=None):
  '''
  Implements Bron-Kerbosch algorithm, Version 2
  author: Oleksii Kuchaiev; http://www.kuchaev.com
  @param graph: SGraph
  @return: List with the cliques of the graph
  '''
  cliques = []
  stack = []
  nd = None
  disc_num = graph.order()
  search_node = (set(), set(graph.getV()), set(), nd, disc_num)
  stack.append(search_node)
  while len(stack) != 0:
    (c_compsub, c_candidates, c_not, c_nd, c_disc_num) = stack.pop()
    if len(c_candidates) == 0 and len(c_not) == 0:
      if len(c_compsub) > 2:
        cliques.append(c_compsub)
        continue

    for u in list(c_candidates):
      if (c_nd is None) or (not graph.is_adjacent(u, c_nd)):
        c_candidates.remove(u)
        Nu = graph.neighbors(u)
        new_compsub = set(c_compsub)
        new_compsub.add(u)
        new_candidates = set(c_candidates.intersection(Nu))
        new_not = set(c_not.intersection(Nu))
        if c_nd != None:
          if c_nd in new_not:
            new_disc_num = c_disc_num - 1
            if new_disc_num > 0:
              new_search_node = (new_compsub, new_candidates, new_not,
                                 c_nd, new_disc_num)
              stack.append(new_search_node)
          else:
            new_disc_num = graph.order()
            new_nd = c_nd
            for cand_nd in new_not:
              cand_disc_num = len(new_candidates) - len(new_candidates.intersection(graph.neighbors(cand_nd)))
              if cand_disc_num < new_disc_num:
                new_disc_num = cand_disc_num
                new_nd = cand_nd
            new_search_node = (new_compsub, new_candidates, new_not,
                               new_nd, new_disc_num)
            stack.append(new_search_node)
        else:
          new_search_node = (new_compsub, new_candidates, new_not,
                             c_nd, c_disc_num)
          stack.append(new_search_node)

        c_not.add(u)
        new_disc_num = 0
        for x in c_candidates:
          if not graph.is_adjacent(x, u):
            new_disc_num += 1

        if new_disc_num < c_disc_num and new_disc_num > 0:
          new1_search_node = (c_compsub, c_candidates, c_not, u, new_disc_num)
          stack.append(new1_search_node)
        else:
          new1_search_node = (c_compsub, c_candidates, c_not, c_nd, c_disc_num)
          stack.append(new1_search_node)
  cliques.sort(key=lambda x: len(x), reverse=True)

  if q:
    q.put(cliques)
  return cliques

def find_shortest_path(graph, start, end_list, d=None):
  '''
  Finds a shortest path between a vertex and a vertex subset.
  It is possible to set a bound for the distance.
  @param graph: SGraph
  @param start: Start vertex
  @param end_list: List of the end vertices
  @param d: Bound (default=None)
  @return: Path as list of vertices and length of the list
  '''
  if start in end_list:
    return [], 0
  min_length = float('inf')
  min_path = None
  for v in end_list:
    new_path, length = shortestPath(graph, start, v)

    if length != None and length < min_length:
      min_length = length
      min_path = new_path

    if d != None and d >= min_length:
      break

  return min_path, min_length

def Dijkstra(graph, start, end=None):
  """
  Find shortest paths from the start vertex to all
  vertices nearer than or equal to the end.

  The output is a pair (D,P) where D[v] is the distance
  from start to v and P[v] is the predecessor of v along
  the shortest path from s to v.
  @param graph: SGraph
  @param start: Start vertex
  @param end: End vertex (default=None)
  """
  from modules.priodict import priorityDictionary as priodict  # @UnresolvedImport

  D = {}  # dictionary of final distances
  P = {}  # dictionary of predecessors
  Q = priodict()  # est.dist. of non-final vertices
  Q[start] = 0

  for v in Q:
    D[v] = Q[v]
    if v == end:
      break

    for w in graph.neighbors(v):
      if isinstance(graph, SWGraph):
        vwLength = D[v] + graph.getWeight((v, w))
      else:
        vwLength = D[v] + 1
      if w in D:
        if vwLength < D[w]:
          raise ValueError
      elif w not in Q or vwLength < Q[w]:
        Q[w] = vwLength
        P[w] = v

  return (D, P)

def shortestPath(graph, start, end):
  """
  Find a single shortest path from the given start vertex
  to the given end vertex.
  @param graph: SGraph
  @param start: Start vertex
  @param end: End vertex
  """
  D, P = Dijkstra(graph, start, end)
  if end not in P:
    return None, 0
  Path = []
  endtmp = end
  while 1:
    Path.append(endtmp)
    if endtmp == start:
      break
    endtmp = P[endtmp]
  Path.reverse()
  return Path, D[end]

def shortestPathAStar(graph, start, end, coord):
  """
  Find a single shortest path from the given start vertex
  to the given end vertex using the A* algorithm
  @param graph: SWGraph
  @param start: Start vertex
  @param end: End vertex
  @param coord: Coordinates of the vertices
  """
  frontier = PriorityQueue()
  frontier.put(start, 0)
  came_from = {}
  cost_so_far = {}
  came_from[start] = None
  cost_so_far[start] = 0

  while not frontier.empty():
    current = frontier.get()

    if current == end:
      break

    for nextVertex in graph.neighbors(current):
      new_cost = cost_so_far[current] + graph.getWeight((current, nextVertex))
      if nextVertex not in cost_so_far or new_cost < cost_so_far[nextVertex]:
        cost_so_far[nextVertex] = new_cost
        priority = new_cost + heuristic((coord[end][0], coord[end][1]),
                                        (coord[nextVertex][0],
                                         coord[nextVertex][1]))
        frontier.put(nextVertex, priority)
        came_from[nextVertex] = current

  return came_from, cost_so_far

def heuristic(v, w):
  '''A simple heuristic for the A* algorithm'''
  (x1, y1) = v
  (x2, y2) = w
  return abs(x1 - x2) + abs(y1 - y2)

def countShortestPaths(graph, s, t):
  """
  Counts the number of shortest paths between s and t
  @param graph: SGraph
  @param s: Start vertex
  @param t: End vertex
  """
  d = {v: inf for v in graph.getVIt()}
  p = {v: 0 for v in graph.getVIt()}
  d[s] = 0
  p[s] = 1
  if s == t:
    return 1
  else:
    Q = queue.Queue()
    Q.put(s)
    while not Q.empty():
      v = Q.get()
      for w in graph.neighbors(v):
        if d[w] == inf:
          d[w] = d[v] + 1
          p[w] = sum([p[u] for u in [x for x in graph.neighbors(w) if d[x] == d[v]]])
          if w == t:
            return p[w]
          Q.put(w)

def distanceVector(graph, u):
  """
  Length of a shortest path from u to all other vertices.
  @param graph: SGraph
  @param u: Vertex of the graph
  @return: Dict with distances (keys are vertices)
  """
  X = {u}
  Y = graph.neighbors(u)
  d = {w: float("inf") for w in graph.getVIt()}
  k = 0
  d[u] = k
  while Y - X != set():
    k += 1
    for w in Y - X:
      d[w] = k
    Z = Y - X
    X = X | Y
    Y = Y.union(*[graph.neighbors(x) for x in Z])
  return d

def distance(graph, u, v):
  '''
  Returns the distance between u and v
  @param graph: SGraph
  @param u: Vertex of the graph
  @param v: Vertex of the graph
  '''
  return distanceVector(graph, u)[v]

def eccentricity(graph, v):
  '''
  Returns the eccentricity of a vertex
  @param graph: SGraph
  @param v: Vertex of the graph
  '''
  return max(distanceVector(graph, v).values())

def pagerank(graph, q=None):
  '''
  Compute the PageRank
  '''
  vertices = graph.getV()
  n = graph.order()
  damping_factor = 0.85
  max_iterations = 100
  min_delta = 0.0001

  if n == 0:
    pr = dict([])
  else:
    min_value = (1.0 - damping_factor) / n

    # initialize the page rank with 1/n for all vertices
    pr = dict.fromkeys(vertices, 1.0 / n)

    for _ in range(max_iterations):
      diff = 0
      # computes for each vertex the PageRank based on the adjacent vertices
      for v in vertices:
        rank = min_value
        for w in graph.neighbors(v):
          rank += damping_factor * pr[w] / graph.degree(w)

        diff += abs(pr[v] - rank)
        pr[v] = rank

      # stop if PageRank has converged
      if diff < min_delta:
        break

  if q:
    q.put(pr)
  return pr

def overallDistance(graph, v):
  '''
  Returns the sum of the distances of the vertex
  @param graph: SGraph
  @param v: Vertex of the graph
  '''
  return sum(distanceVector(graph, v).values())

def averageDistance(graph):
  '''
  Calculates the average distance in the graph
  @param graph: SGraph
  '''
  if graph.order() < 2:
    return 0.0
  dist = 0.0
  n = 0
  for i, v in enumerate(graph.getV()):
    for j, w in enumerate(graph.getV()):
      if i < j:
        dist += distance(graph, v, w)
        n += 1
  return dist / n

def distanceMatrix(graph, q=None):
  '''
  Calculates the distance matrix of the graph
  with the algorithm of Floyd
  @param graph: SGraph
  @return: Numpy.matrix
  '''
  import numpy as np
  d = np.matrix([[graph.getWeight([i, j]) for j in range(1, graph.order() + 1)] for i in range(1, graph.order() + 1)])
  for k in range(graph.order()):
    for i in range(graph.order()):
      for j in range(graph.order()):
        if i != j:
          d[i, j] = min(d[i, j], d[i, k] + d[k, j])
        elif k == 0:
          d[i, j] = 0
  if q != None:
    q.put(d)
  return d

def longestIsometricCycle(graph, q=None):
  '''
  Find the longes isometric cycle in the graph
  '''
  def Mk(k, u, v):
    if k % 2 == 0:
      return set([(u, v)])
    else:
      return set([p for p in Vk if u == p[0] and graph.hasEdge((v, p[1]))])

  import math

  # Test if the graph is a tree
  if isTree(graph):
    return 0

  V = graph.getV()
  ans = 0
  for k in range(3, graph.order() + 1):
    Vk = set([])
    for u in V:
      for v in V:
        if distance(graph, u, v) == int(math.floor(k / 2)):
          Vk.add((u, v))
    Ek = set([])
    for p in Vk:
      for s in Vk:
        if graph.hasEdge((p[0], s[0])) and graph.hasEdge((p[1], s[1])):
          Ek.add((p, s))
    Gk = SGraph(Vk, Ek)

    Ek2 = set([])
    for u in Vk:
      for v in Vk:
        if distance(Gk, u, v) <= int(math.floor(k / 2)):
          Ek2.add((u, v))
          Ek2.add((v, u))

    for p in Vk:
      for x in V:
        Mkvu = Mk(k, p[1], p[0])
        if (p[1], x) in Mkvu and (p, (p[1], x)) in Ek2:
          ans = k
  if q != None:
    q.put(ans)
  return ans

def median(graph):
  '''
  Calculates the median of a graph
  @param graph: SGraph
  '''
  dist_v = []
  min_dist = float('inf')
  for v in graph.getVIt():
    dist = overallDistance(graph, v)
    if dist < min_dist:
      dist_v = [v]
      min_dist = dist
    elif dist == min_dist:
      dist_v.append(v)
  return dist_v

def radius(graph):
  '''
  Calculates the radius of the graph
  @param graph: SGraph
  '''
  if graph.order() > 0:
    return min([eccentricity(graph, v) for v in graph.getVIt()])
  else:
    return 0

def diameter(graph):
  '''
  Calculates the diameter of the graph
  @param graph: SGraph
  '''
  if graph.order() > 0:
    return max([eccentricity(graph, v) for v in graph.getVIt()])
  else:
    return 0

def betweennessCentrality(graph, r=None):
  '''
  Calculates the betweenness centrality of the graph
  with the Algrorithm of Brandes
  @param graph: SGraph
  '''
  from collections import deque

  C = {v: 0.0 for v in graph.getVIt()}
  neighbors = graph.neighbors

  for s in graph.getVIt():
    S = []
    P = {w: [] for w in graph.getVIt()}
    sigma = {w: 0.0 for w in graph.getVIt()}
    d = {w: -1.0 for w in graph.getVIt()}
    sigma[s] = 1.0
    d[s] = 0.0
##    for w in graph.getVIt():
##      P[w] = []
##      if w == s:
##        sigma[w] = 1.0
##        d[w] = 0.0
##      else:
##        sigma[w] = 0.0
##        d[w] = -1.0
    q = deque([])
    q.append(s)

    while len(q) != 0:
      v = q.popleft()
      S.append(v)
      for w in neighbors(v):
        if d[w] < 0:
          q.append(w)
          d[w] = d[v] + 1

        if d[w] == d[v] + 1:
          sigma[w] += sigma[v]
          P[w].append(v)

    delta = {v: 0.0 for v in graph.getVIt()}

    while len(S) != 0:
      w = S.pop()
      for v in P[w]:
        delta[v] += sigma[v] / sigma[w] * (1 + delta[w])
      if w != s:
        C[w] += delta[w] / 2

  # Normalize the result
  n = graph.order()
  norm = n**2 - 3 * n + 2
  for v in C:
    C[v] = 2 * C[v] / norm

  if r != None:
    r.put(C)
  return C

def stressCentrality(graph, r=None):
  '''
  Calculates the stress centrality of the graph
  with the Algrorithm of Brandes
  @param graph: SGraph
  '''
  from collections import deque

  C = {v: 0.0 for v in graph.getVIt()}
  neighbors = graph.neighbors

  for s in graph.getVIt():
    S = []
    P = {w: [] for w in graph.getVIt()}
    sigma = {w: 0.0 for w in graph.getVIt()}
    d = {w: -1.0 for w in graph.getVIt()}
    sigma[s] = 1.0
    d[s] = 0.0
##    for w in graph.getVIt():
##      if w == s:
##        sigma[w] = 1.0
##        d[w] = 0.0
##      else:
##        sigma[w] = 0.0
##        d[w] = -1.0
    q = deque([])
    q.append(s)

    while len(q) != 0:
      v = q.popleft()
      S.append(v)
      for w in neighbors(v):
        if d[w] < 0:
          q.append(w)
          d[w] = d[v] + 1

        if d[w] == d[v] + 1:
          sigma[w] += sigma[v]
          P[w].append(v)

    delta = {v: 0.0 for v in graph.getVIt()}

    while len(S) != 0:
      w = S.pop()
      for v in P[w]:
        delta[v] += sigma[v] * (1 + delta[w])
      if w != s:
        C[w] += delta[w] / 2

  if r != None:
    r.put(C)
  return C

def closenessCentrality(graph, q=None):
  '''
  Computes the closeness centrality of a given graph
  @param graph: SGraph
  '''
  C = {}
  for v in graph.getVIt():
    d = distanceVector(graph, v)
    C[v] = 1.0 / (sum([d[w] for w in graph.getVIt() if w != v]) / (len(d) - 1))

  if q != None:
    q.put(C)
  return C

def center(graph):
  '''
  Returns the center of the graph
  @param graph: SGraph
  @return: List of vertices
  '''
  r = radius(graph)
  return filter(lambda v: eccentricity(graph, v) == r, graph.getV())

def peripheral(graph):
  '''
  Returns the peripheral of the graph
  @param graph: SGraph
  @return: List of vertices
  '''
  d = diameter(graph)
  return filter(lambda v: eccentricity(graph, v) == d, graph.getV())

def connected(graph):
  """
  Connectivity test using BFS.
  @param graph: SGraph
  @return: Bool
  """
  u = next(graph.getVIt())
  X = {u}
  Y = graph.neighbors(u)
  k = 1
  while Y - X != set():
    k += len(Y - X)
    Z = Y - X
    X = X | Y
    Y = Y.union(*[graph.neighbors(x) for x in Z])
  return k == graph.order()

def max_flow(graph, source, sink):
  '''
  Calculates a max flow in the undirected graph.
  All edges has capacity 1.
  @param graph: SGraph
  @param source: Source vertex
  @param sink: Sink vertex
  '''
  def convertPath(pth):
    edgepath = []
    for i in range(0, len(pth) - 1):
      edgepath.append((pth[i], pth[i + 1]))
    return edgepath

  flow = {(v, w): 0 for v in graph.getVIt() for w in graph.neighbors(v)}

  g = graph.copy()
  path, _ = shortestPath(g, source, sink)
  while path != None:
    path = convertPath(path)
    flowint = 1  # min(residuals)
    for edge in path:
      flow[edge] += flowint
      g.deleteEdge(edge)
    path, _ = shortestPath(g, source, sink)
  return flow

def maxFlow(graph, source, sink):
  '''
  Calculate the max-flow between the source and the sink with
  the algorithm of Ford-Fulkerson
  @param graph: SGraph or SWGraph
  @param source: Source of the flow
  @param sink: Sink of the flow
  '''
  def find_path(source, sink, path):
    '''Find an flow-expanding path'''
    if source == sink:
      return path
    for v in g.neighbors(source):
      residual = g.getWeight((source, v)) - flow[(source, v)]
      edge = (source, v, residual)
      if residual > 0 and not edge in path:
        result = find_path(v, sink, path + [edge])
        if result != None:
          return result

  # make a copy of the orginal graph
  if isinstance(graph, SWGraph):
    g = graph.copy()
  else:
    g = SWGraph()
    g.convertSGraph(graph)

  # Some initial things
  flow = {(u, v): 0 for u in g.getVIt() for v in g.neighbors(u)}
  path = find_path(source, sink, [])

  # Repeat until we find not expanding path
  while path != None:
    new_flow = min(r for u, v, r in path)
    for u, v, _ in path:
      flow[(u, v)] += new_flow
      flow[(v, u)] -= new_flow
    path = find_path(source, sink, [])

  return sum(flow[(source, v)] for v in g.neighbors(source))

def minimumCut(graph, u=None):
  '''
  Calculates a min-cut in the graph
  @param graph: SGraph or SWGraph
  @param a: Vertex of the graph
  '''
  def min_cut_phase(u):
    def w(A, u):
      assert u not in A
      return sum(g.getWeight((u, v)) for v in g.neighbors(u) if v in A)

    A = set([u])
    V = set(g.getV())
    order = [u]
    while A != V:
      # candidates for most tightly connected
      w_candidates = [(w(A, v), v) for v in V - A]
      _, x = max(w_candidates)
      A.add(x)
      order.append(x)
    t, s = order[-1], order[-2]

    mincutvalue = sum(g.getWeight((t, v)) for v in g.neighbors(t))
    mincut = supervertices[t]

    supervertices[t] = supervertices[t] | supervertices[s]
    del supervertices[s]
    g.merge(t, s)
    return mincutvalue, mincut

  # Choose a random vertex if no vertex is given
  if not u:
    u = graph.randomVertex()

  # dict to save the merge-operation
  supervertices = {v: set([v]) for v in graph.getVIt()}

  # make a copy of the orginal graph
  if isinstance(graph, SWGraph):
    g = graph.copy()
  else:
    g = SWGraph()
    g.convertSGraph(graph)

  # Some initial things
  mincutvalue = sum(g.getWeight((u, v)) for v in g.neighbors(u))
  mincut = supervertices[u]

  # Repeat the process until we get a single vertex
  while g.order() > 1:
    min_cut_candidate, mincutcandidate = min_cut_phase(u)
    if min_cut_candidate < mincutvalue:
      mincutvalue = min_cut_candidate
      mincut = mincutcandidate

  # Get the edge cut
  mincut = graph.delta(mincut)
  return mincutvalue, mincut

def localEdgeConnectivity(graph, s, t):
  '''
  Calculates the local edge connectivity of the
  undirected graph and the vertices s and t
  @param graph: SGraph
  @param s: Startvertex
  @param t: Endvertex
  '''
  def BFS():
    d = {v: inf for v in H.getV()}
    d[s] = 0
    Q = queue.Queue()
    Q.put(s)
    while not Q.empty():
      v = Q.get()
      for w in set(H.neighbors(v)):
        if d[w] == inf:
          d[w] = d[v] + 1
          father[w] = v
          if w == t:
            return True
          Q.put(w)
    return False

  H = graph.copy()
  father = dict()
  while BFS():
    x = t
    while x != s:
      H.deleteEdge([x, father[x]])
      x = father[x]

  return len(graph.neighbors(s)) - len(H.neighbors(s))

def localEdgeConnectivity2(graph, s, t):
  '''
  Calculates the local edge connectivity of the directed
  graph and the vertices s and t.
  Translated function from maple (Author: Peter Tittmann)
  @param graph: SGraph
  @param s: Startvertex
  @param t: Endvertex
  '''
  def BFS():
    d = {v: inf for v in H.getVIt()}
    d[s] = 0
    Q = queue.Queue()
    Q.put(s)
    while not Q.empty():
      v = Q.get()
      for w in set(H.neighbors(v)):
        if d[w] == inf:
          d[w] = d[v] + 1
          father[w] = v
          if w == t:
            return True
          Q.put(w)
    return False

  H = graph.copy()
  father = dict()
  while BFS():
    x = t
    while x != s:
      H.insertEdge(x, father[x])
      H.deleteEdge([father[x], x])
      x = father[x]

  return len(graph.neighbors(s)) - len(H.neighbors(s))

def makeDigraph(graph):
  '''
  Generates a directed graph from the undirected graph.
  Translated function from maple (Author: Peter Tittmann)
  @param graph: SGraph
  '''
  E = set([((v, 1), (v, 2)) for v in graph.getV()]) | set([((v, 2), (w, 1)) for v in graph.getV() for w in graph.neighbors(v)])
  V = [(v, 1) for v in graph.getV()]
  V.extend([(v, 2) for v in graph.getV()])
  return DGraph(V, E)

def localConnectivity(graph, s, t):
  '''
  Calculates the local connectivity of the graph and the vertices s and t.
  Translated function from maple (Author: Peter Tittmann)
  @param graph: SGraph
  @param s: Startvertex
  @param t: Endvertex
  '''
  # return localEdgeConnectivity2(makeDigraph(graph), (s,2), (t,1))
  return localEdgeConnectivity2(graph, s, t)

def connectivity(graph, q=None):
  '''
  Calculates the (vertex) connectivity of the graph.
  Translated function from maple (Author: Peter Tittmann)
  @param graph: SGraph
  '''
  s = minimumDegreeVertex(graph)
  g = makeDigraph(graph)
  W1 = [localConnectivity(g, (s, 2), (t, 1)) for t in set(graph.getV()) - set(graph.neighbors_set([s]))]
  W1.append(graph.order() - 1)
  k1 = min(W1)
  W2 = [localConnectivity(g, (p, 2), (q, 1)) for p in graph.neighbors(s) for q in graph.neighbors(s) if p < q and not graph.is_adjacent(p, q)]
  W2.append(inf)
  k2 = min(W2)
  vc = min(k1, k2)

  if q != None:
    q.put(vc)
  return vc

def edgeconnectivity(graph, q=None):
  '''
  Calculates the edge connectivity of the graph
  @param graph: SGraph
  '''
  s = minimumDegreeVertex(graph)
  w = [localEdgeConnectivity(graph, s, t) for t in graph.getVIt() if t != s]
  ec = min(w)

  if q != None:
    q.put(ec)
  return ec

def adjacencyMatrix(graph):
  '''
  Returns the adjacency matrix of the graph
  @param graph: SGraph
  @return: list of lists
  '''
  return [[int(graph.is_adjacent(u, v)) for v in graph.getVIt()] for u in graph.getVIt()]

def laplacianMatrix(graph):
  '''
  Returns the laplacian matrix of the graph
  @param graph: SGraph
  @return: list of lists
  '''
  A = adjacencyMatrix(graph)
  L = [[-a for a in R] for R in A]
  for i, v in enumerate(graph.getV()):
    L[i][i] = graph.degree(v)
  return L

def adjacencySpectrum(graph, q=None):
  '''
  Return eigenvalues of the adjacency matrix of G
  @param graph: SGraph
  '''
  try:
    import numpy as np
  except Exception:
    ec = "Please install numpy"
  else:
    ec = np.round(np.linalg.eigvals(np.matrix(adjacencyMatrix(graph))), 5).tolist()

  if q != None:
    q.put(ec)
  return ec

def laplacianSpectrum(graph, q=None):
  '''
  Return eigenvalues of the Laplacian of G
  @param graph: SGraph
  '''
  try:
    import numpy as np
  except Exception:
    ec = "Please install numpy"
  else:
    ec = np.round(np.linalg.eigvals(np.matrix(laplacianMatrix(graph))), 5).tolist()

  if q != None:
    q.put(ec)
  return ec


#===========================================================================
#===========================================================================
#===========================================================================
if __name__ == '__main__':
  sys.exit("Hauptmodul zum Starten verwenden")
