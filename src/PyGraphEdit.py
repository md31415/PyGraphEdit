#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
With PyGraphEdit it is possible to draw graphs and
calculate different graph invariants. The graphs can be saved in a xml-file
or in a SQLite-database. It is also possible to export the graph as a
jpg-picture or in TikZ.
For more information see /help/PyGraphEdit.html.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.


Created on 05.12.2012
Most recent amendment: 05.04.2018

@author: Markus Dod
@contact: mdod@hs-mittweida.de
@version: 0.63
@requires: Python 3.x, PyQt5, pydot, sympy; optional: Graphviz, numpy, matplotlib, scipopt, ogdf
@copyright: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import argparse
from collections import deque
from copy import deepcopy
import configparser
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
import math
import multiprocessing
from pathlib import Path
from platform import system
from random import uniform
import re
import sys
import tempfile
import time
import threading

import pydot  # @UnresolvedImport
from PyQt5 import QtWidgets, QtCore, QtGui, QtPrintSupport  # @UnresolvedImport

try:
  _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
  _fromUtf8 = lambda s: s

# Import the dialog windows
from dialogs.formlayout import fedit  # @UnresolvedImport

from dialogs.PyGraphEdit_Broadcasting import Ui_Dialog as Ui_Broadcasting  # @UnresolvedImport
from dialogs.PyGraphEdit_Close import Ui_Dialog as Ui_Close  # @UnresolvedImport
from dialogs.PyGraphEdit_Config import Ui_Dialog as Ui_Config  # @UnresolvedImport
from dialogs.PyGraphEdit_EdgeWeights import Ui_Dialog as Ui_EdgeWeights  # @UnresolvedImport
from dialogs.PyGraphEdit_ExtPython import Ui_Dialog as Ui_ExtPython  # @UnresolvedImport
from dialogs.PyGraphEdit_Graphclasses import Ui_Dialog as Ui_Classes  # @UnresolvedImport
from dialogs.PyGraphEdit_Info import Ui_Dialog as Ui_Infos  # @UnresolvedImport
from dialogs.PyGraphEdit_Kanteninfo import Ui_Dialog as Ui_Kanteninfo  # @UnresolvedImport
from dialogs.PyGraphEdit_Knoteninfo import Ui_Dialog as Ui_Knoteninfo  # @UnresolvedImport
from dialogs.PyGraphEdit_main import Ui_MainWindow  # @UnresolvedImport
from dialogs.PyGraphEdit_ProductGraph import Ui_Dialog as ProductGraph_Dialog  # @UnresolvedImport
from dialogs.PyGraphEdit_Solution import Ui_Dialog as Solution_Info  # @UnresolvedImport
from dialogs.PyGraphEdit_Wait import Ui_Dialog as Ui_Wait  # @UnresolvedImport

from dialogs.TestDialogs import (GenerateTestDialog, StartTestDialog,  # @UnresolvedImport
                                 CompareResultsDialog, TestStatusDialog)  # @UnresolvedImport
from dialogs.SaveandLoadDialogs import (LoadDialog, SaveSimulationDialog,  # @UnresolvedImport
                                        LoadSimulationDialog, Load2GraphsDialog, # @UnresolvedImport
                                        LoadOSMDialog)  # @UnresolvedImport
from dialogs.ExportDialogs import (ExportDialog, SvgExportDialog,  # @UnresolvedImport
                                   TikzExportDialog, Graph6toDBDialog)  # @UnresolvedImport

from SGraph.GraphFunctions import numSpanningTree as calcNumSpanningTree  # @UnresolvedImport
import SGraph.GraphFunctions as GraphFunctions  # @UnresolvedImport
from SGraph.SGraph import SGraph  # @UnresolvedImport
from SGraph.SWGraph import SWGraph  # @UnresolvedImport

from SGraph.GraphParser import (getGraphNamesdb, load_graphdb, GraphMLParser,  # @UnresolvedImport
                                GMLParser, load_graph, LEDAParser, save_graphdb,  # @UnresolvedImport
                                getGraphNamesGraphml, getGraphNamesGML,  # @UnresolvedImport
                                save_graph, getGraphNames, parse_graph6,  # @UnresolvedImport
                                graph6FiletoDB, graphtograph6)  # @UnresolvedImport

from lib.Decompositions import get_tree_decomposition, get_path_decomposition, get_width  # @UnresolvedImport
from lib.DominationReliabilityEDRel import compute_reliability as compute_reliability_edrel  # @UnresolvedImport
from lib.DominationReliabilityPEDRel import compute_reliability as compute_reliability_pedrel  # @UnresolvedImport
from lib.DominationReliabilitySPEDRel import compute_reliability as compute_reliability_spedrel  # @UnresolvedImport

from lib.MaximumMatching import maxMatching as computeMaxMatching  # @UnresolvedImport
from lib.GracefulLabels import compute_labels as compute_gracefulLabels  # @UnresolvedImport

from lib.GraphEmbedding import springEmbedding, circleLayout, gridLayout  # @UnresolvedImport

from lib.VertexColoring import compute_coloring as compute_coloring  # @UnresolvedImport
from lib.EdgeColoring import compute_coloring as compute_edge_coloring  # @UnresolvedImport
from lib.RainbowColoring import compute_coloring as computeRainbowColoring  # @UnresolvedImport
from lib.RainbowVertexColoring import compute_coloring as computeRainbowVertexColoring  # @UnresolvedImport
from lib.TuttePoly import compute_poly as computeTuttePoly  # @UnresolvedImport

from modules.consts import (method, time_stamp_str, simulation_type, CALCNAMES, # @UnresolvedImport
                            COLORRGB, COLORTIKZ, COLORLIST,  # @UnresolvedImport
                            PLOTMODES_GRAPHVIZ, PLOTMODES_INTERN, VERSION)  # @UnresolvedImport
from modules.Dot import Dot  # @UnresolvedImport
from modules.log import PyGraphEditLog, StreamToLogger, EmittingStream  # @UnresolvedImport
from modules.SVGExport import generateSVG  # @UnresolvedImport
from modules.SaveLoadSimulation import saveSimulation, loadSimulation, getSimNames  # @UnresolvedImport
from modules.Simulations import BFS as simulationBFS  # @UnresolvedImport
from modules.Simulations import DFS as simulationDFS  # @UnresolvedImport
from modules.Simulations import broadcasting as simulationBroadcasting  # @UnresolvedImport
from modules.UserfilesMark import MarkData  # @UnresolvedImport
from modules.TestSet import TestUM, generateTestFile, TestsCompare  # @UnresolvedImport
from modules.error import InputError  # @UnresolvedImport
from modules.Osm import isMapLarge
from modules.Osm import parseMapfile
from modules.Osm import reduceOsmGraph

try:
  import modules.ogdf.PlanarityOGDF as testPlanarity  # @UnresolvedImport
  USE_OGDF = True
except:
  USE_OGDF = False

from modules.Globals import Globals  # @UnresolvedImport


# =========================================================================
# ======== MAIN PYGRAPHEDIT-CLASS =========================================
# =========================================================================
class PyGraphEdit(QtWidgets.QMainWindow):
  '''The main PyGraphEdit-class'''
  exportDoneSignal = QtCore.pyqtSignal()

  def __init__(self, parent=None):
    '''
    Init the main window and all dialogs
    '''
    super(PyGraphEdit, self).__init__(parent)
    self.ui = MainWindow()
    self.ui.setupUi(self)
    self.ui.graph = None

    # Setup the GraphicScene
    self.ui.generateStructures()

    # init additional windows
    self.changeEdgeWeightsDialog = EdgeWeightsDialog()
    self.classInfos = ClassInfos()
    self.closeDialog = CloseDialog()
    self.compareResultsDialog = CompareResultsDialog(globalsClass, config)
    self.configDialog = ConfigDialog()
    self.edgeInfo = EdgeInfo()
    self.exportDialog = ExportDialog()
    self.extPython = ExtPythonDialog()
    self.generateTestDialog = GenerateTestDialog(globalsClass, config)
    self.graph6toDB = Graph6toDBDialog(config)
    self.infodialog = GraphInfos()
    self.ladendialog = LoadDialog(globalsClass, config)
    self.ladendialog.setModal(True)
    self.loadosmdialog = LoadOSMDialog(globalsClass, config)
    self.loadosmdialog.setModal(True)
    self.loadDialog2Graphs = Load2GraphsDialog(globalsClass, config)
    self.loadSimulationDialog = LoadSimulationDialog(globalsClass, config)
    self.productDialog = ProductDialog()
    self.saveSimulationDialog = SaveSimulationDialog()
    self.simulationWorker = Simulation()
    self.simBroadcastingDialog = SimBroadcastDialog()
    self.solutiondialog = SolutionDialog()
    self.startTestDialog = StartTestDialog(globalsClass, config)
    self.svgExportDialog = SvgExportDialog(config)
    self.testStatusDialog = TestStatusDialog()
    self.tikzExportDialog = TikzExportDialog(config)
    self.vertexInfo = VertexInfo()
    self.wait = Wait()

    # Install the custom output testStatusDialog
    global STREAMWRITER
    STREAMWRITER = EmittingStream(textStream=self.testStatusDialog.writeStream)
    self.testum = TestUM(STREAMWRITER, PyGraphEdit_log, globalsClass.IMPLEMENTEDMODULES)  # Var for the test class

    # Slots for the buttons
    self.ui.pushButtonNew.clicked.connect(self.onNew)
    self.ui.pushButtonOpen.clicked.connect(self.onLoad)
    self.ui.pushButtonSave.clicked.connect(self.onSave)
    self.ui.pushButtonTikz.clicked.connect(self.onExportTikZ)
    self.ui.pushButtonSettings.clicked.connect(self.onSettings)
    self.ui.pushButtonGraphInfo.clicked.connect(self.onGraphInfo)
    self.ui.pushButtonPython.clicked.connect(self.onExtPython)

    self.ui.pushButtonDomPoly.clicked.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["DominationPoly"],
                                        CALCNAMES["DominationPoly"],
                                        "lib.DominationReliabilityDRel"))
    self.ui.pushButtonBipartPoly.clicked.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["BipartitionPoly"],
                                        CALCNAMES["BipartitionPoly"],
                                        "lib.BipartitionPoly"))
    self.ui.pushButtonIndPoly.clicked.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["IndependencePoly"],
                                        CALCNAMES["IndependencePoly"],
                                        "lib.IndPoly"))
    self.ui.pushButtonMatchingPoly.clicked.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["MatchingPoly"],
                                        CALCNAMES["MatchingPoly"],
                                        "lib.Matching"))
    self.ui.pushButtonTuttePoly.clicked.connect(self.onTuttePoly)
    self.ui.pushButtonChromPoly.clicked.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["ChromaticPoly"],
                                        CALCNAMES["ChromaticPoly"],
                                        "lib.ChromaticPoly"))

    self.ui.pushButtonHelp.clicked.connect(self.onShowHelp)
    self.ui.pushButtonInfo.clicked.connect(self.onShowProginfo)
    self.ui.pushButtonClose.clicked.connect(self.onClose)

    self.ui.pushButtonmarkall.clicked.connect(self.onMarkAll)
    self.ui.pushButtonEntmark.clicked.connect(self.onEntmark)
    self.ui.pushButtonDelmark.clicked.connect(self.onDelmark)
    self.ui.pushButtonAktualisieren.clicked.connect(self.onAkt)
    self.ui.pushButtonClear.clicked.connect(self.onClear)
    self.ui.pushButtonBeenden.clicked.connect(self.onClose)

    # Use graphviz?
    if not globalsClass.USE_GRAPHVIZ:
      self.__deactivateGraphviz__()
    else:
      self.__activateGraphviz__()

    # Signals from the config-dialog
    self.configDialog.activateGraphvizSignal.connect(self.__activateGraphviz__)
    self.configDialog.deactivateGraphvizSignal.connect(self.__deactivateGraphviz__)
    self.configDialog.updateSignal.connect(self.__applySettings)

    # Signal from the load-dialog
    self.ladendialog.accept.connect(self.__load__)
    self.loadosmdialog.accept.connect(self.__loadosm__)
    self.loadDialog2Graphs.accept.connect(self.__load2Graphs)
    self.loadSimulationDialog.accept.connect(self.__loadSimulation__)

    self.saveSimulationDialog.accept.connect(self.__saveSimulation__)

    # Signal from the product dialog
    self.productDialog.accept.connect(self.onProduktGraph)

    # Stop-Signal from the "stop the calculation" dialog
    self.wait.stoppcalculation.connect(self.stopCalc)

    # Signals from the Testset-dialogs
    self.generateTestDialog.start.connect(self.startGeneratingTestFile)
    self.startTestDialog.start.connect(self.StartTestCalculation)
    self.testStatusDialog.save.connect(self.onSaveResult)
    self.compareResultsDialog.start.connect(self.compareResults)
    self.testum.finished.connect(self.finishTestCalc)

    # Signal to set a new vertex or edge weight
    self.vertexInfo.setElementWeight.connect(self.__setElementWeight__)
    self.edgeInfo.setElementWeight.connect(self.__setElementWeight__)

    # OK Signal from the TikZ-Dialog
    self.tikzExportDialog.ok.connect(self.__exportTikZ)

    # OK Signal from the ExtPython-Dialog
    self.extPython.ok.connect(self.__extPython)

    # OK Signal from the SVG-Dialog
    self.svgExportDialog.ok.connect(self.__exportSVG)

    # OK Signal from the graph6toDB-Dialog
    self.graph6toDB.ok.connect(self.__genDataGraph6)

    # Get the Signals from the export-dialog
    self.exportDialog.graphviz.connect(self._savePictureGraphviz)
    self.exportDialog.qt.connect(self._savePictureQt)
    self.exportDialog.intern.connect(self._savePictureIntern)

    self.exportDoneSignal.connect(self.exportDone)

    # Signal from the changeEdgeWeights-Dialog
    self.changeEdgeWeightsDialog.ok.connect(self._changeEdgeWeights)

    # Signal from the simBroadcasting-Dialog
    self.simBroadcastingDialog.ok.connect(self._onSimBroadcasting)

    # Signal if the selected color is changed
    self.ui.comboBoxColor.activated.connect(self.__setColor)

    # Collect the signals from the simulation things
    self.ui.horizontalSliderAnimation.valueChanged.connect(self.__simulate__)
    self.ui.pushButtonPlay.clicked.connect(self.__runSimulation)
    self.ui.pushButtonBreak.clicked.connect(self.__breakSimulation)
    self.ui.pushButtonStop.clicked.connect(self.__stopSimulation)
    self.ui.lineEditSimulation.editingFinished.connect(self.__jumpStepSimulation)
    self.simulationWorker.jumpStep.connect(self.__jumpStepSimulation)
    self.ui.pushButtonExportAnimation.clicked.connect(self.__exportSimulation)
    self.ui.pushButtonLoadAnimation.clicked.connect(self.__loadSimulation)
    self.ui.pushButtonSaveAnimation.clicked.connect(self.__saveSimulation)

    # Add functions to the file menu
    # File
    self.ui.actionNeu.triggered.connect(self.onNew)
    self.ui.actionLoad.triggered.connect(self.onLoad)
    self.ui.actionLoad2.triggered.connect(self.on_Load2Graphs)
    self.ui.actionLoadOSM.triggered.connect(self.onLoadOSM)
    self.ui.actionSpeichern.triggered.connect(self.onSave)
    self.ui.actionExport.triggered.connect(self.onExport)
    self.ui.actionExportTikZ.triggered.connect(self.onExportTikZ)
    self.ui.actionDrucken.triggered.connect(self.onPrint)
    self.ui.actionEinstellungen.triggered.connect(self.onSettings)
    self.ui.actionBeenden.triggered.connect(self.onClose)

    # Generate
    self.ui.actionDiamant.triggered.connect(self.onDiamant)
    self.ui.actionWeg.triggered.connect(self.onPath)
    self.ui.actionVollstaendigerGraph.triggered.connect(self.onCompleteGraph)
    self.ui.actionKreis.triggered.connect(self.onCycle)
    self.ui.actionAntikreis.triggered.connect(self.onAnticycle)
    self.ui.actionStern.triggered.connect(self.onStar)
    self.ui.actionRad.triggered.connect(self.onWheel)
    self.ui.actionFan.triggered.connect(self.onFan)
    self.ui.actionSun.triggered.connect(self.onSun)
    self.ui.actionSunlet.triggered.connect(self.onSunlet)
    self.ui.actionRisingSun.triggered.connect(self.onRisingSun)
    self.ui.actionFriendshipGraph.triggered.connect(self.onFriendshipGraph)
    self.ui.actionBookGraph.triggered.connect(self.onBookGraph)
    self.ui.actionBipartiterGraph.triggered.connect(self.onBipartiteGraph)
    self.ui.actionCompletekPartiteGraph.triggered.connect(self.onkPartiteGraph)

    # Random graphs
    self.ui.actionRandomEdges.triggered.connect(self.onRandomEdges)
    self.ui.actionRandomNumberEdges.triggered.connect(self.onRandomNumberEdges)
    self.ui.actionGeometricRandom.triggered.connect(self.onGeometricRandom)
    self.ui.actionRandomRegularGraph.triggered.connect(self.onRandomRegularGraph)
    self.ui.actionBarabasiAlbert.triggered.connect(self.onBarabasiAlbert)
    self.ui.actionPowerlawClusterGraph.triggered.connect(self.onPowerlawClusterGraph)
    self.ui.actionLobsterTree.triggered.connect(self.onLobsterTree)

    self.ui.actionProdukt.triggered.connect(self.onProduct)
    self.ui.actionkPath.triggered.connect(self.onkPath)
    self.ui.actionRandomkPath.triggered.connect(self.onRandomkPath)
    self.ui.actionkCycle.triggered.connect(self.onkCycle)
    self.ui.actionKStar.triggered.connect(self.onkStar)
    self.ui.actionSplitgraph.triggered.connect(self.onSplitGraph)
    self.ui.actionQueensGraph.triggered.connect(self.onQueensGraph)
    self.ui.actionRooksGraph.triggered.connect(self.onRookGraph)
    self.ui.actionBishopGraph.triggered.connect(self.onBishopGraph)
    self.ui.actionGenFromGraph6.triggered.connect(self.onGenFromGraph6)
    self.ui.actionGenDataGraph6.triggered.connect(self.onGenDataGraph6)

    # Generate Trees
    self.ui.actionFirecracker.triggered.connect(self.onFirecracker)
    self.ui.actionBananaTree.triggered.connect(self.onBananaTree)
    self.ui.actionCentipede.triggered.connect(self.onCentipede)
    self.ui.actionRandomTree.triggered.connect(self.onRandomTree)
    self.ui.actionRandomTreePruefercode.triggered.connect(self.onRandomTreePruefercode)

    # Manipulate
    self.ui.actionRueckgaengig.triggered.connect(self.onUndo)
    self.ui.actionComplementgraph.triggered.connect(self.onComplement)
    self.ui.actionLinegraph.triggered.connect(self.onLineGraph)
    self.ui.actionKlinegraph.triggered.connect(self.onKlineGraph)
    self.ui.actionInduzierten_Untergraph.triggered.connect(
        self.onInducedSubgraph)
    self.ui.actionPathDecomp.triggered.connect(self.onPathDecomposition)
    self.ui.actionTreeDecomp.triggered.connect(self.onTreeDecomposition)
    self.ui.actionVertexWeights.triggered.connect(self.onChanceVertexWeights)
    self.ui.actionEdgeWeights.triggered.connect(self.onChanceEdgeWeights)

    # Analyse
    self.ui.actionGrapheninfos.triggered.connect(self.onGraphInfo)
    self.ui.actionGraphclasses.triggered.connect(self.onGraphClasses)
    self.ui.actionDistancematrix.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["DistanceMatrix"],
                                        CALCNAMES["DistanceMatrix"]))
    self.ui.actionAdjacencySpectrum.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["AdjacencySpectrum"],
                                        CALCNAMES["AdjacencySpectrum"]))
    self.ui.actionLaplacianSpectrum.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["LaplacianSpectrum"],
                                        CALCNAMES["LaplacianSpectrum"]))
    self.ui.actionStressCentrality.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["StressCentrality"],
                                        CALCNAMES["StressCentrality"]))
    self.ui.actionBetweenness.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["BetweennessCentrality"],
                                        CALCNAMES["BetweennessCentrality"]))
    self.ui.actionClosness.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["ClosenessCentrality"],
                                        CALCNAMES["ClosenessCentrality"]))
    self.ui.actionZentrum.triggered.connect(self.onShowCenter)
    self.ui.actionRand.triggered.connect(self.onShowPeripheral)
    self.ui.actionMedian.triggered.connect(self.onShowMedian)
    self.ui.actionMaxCliquen.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["MaxClique"],
                                        CALCNAMES["MaxClique"]))
    self.ui.actionMaxMatching.triggered.connect(self.onMaxMatching)
    self.ui.actionMinEdgeCover.triggered.connect(self.onMinEdgeCover)
    self.ui.actionPageRank.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["PageRank"],
                                        CALCNAMES["PageRank"]))
    self.ui.actionLongestIsometricCycle.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["LongestIsometricCycle"],
                                        CALCNAMES["LongestIsometricCycle"]))
    self.ui.actionMinCut.triggered.connect(self.onMinCut)
    self.ui.actionFordFulkerson.triggered.connect(self.onFordFulkerson)
    self.ui.actionMinstCut.triggered.connect(self.onMinstCut)

    self.ui.actionOpenNeighbors.triggered.connect(self.onOpenNeighbors)
    self.ui.actionTotalOpenNeighbors.triggered.connect(
        self.onTotalOpenNeighbors)
    self.ui.actionClosedNeighbors.triggered.connect(self.onClosedNeighbors)
    self.ui.actionArticulations.triggered.connect(self.onArticulations)

    self.ui.actionShortestPath.triggered.connect(self.onShortestPath)
    self.ui.actionShortestPathAStar.triggered.connect(self.onShortestPathAStar)
    self.ui.actionNumberofshortestpaths.triggered.connect(
        self.onNumberofshortestpaths)
    self.ui.actionNumberofEdgeDisjointPaths.triggered.connect(self.onNumberofEdgeDisjointPaths)

    self.ui.actionGeneralDomPoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["GeneralDominationPoly"],
                                        CALCNAMES["GeneralDominationPoly"],
                                        "lib.GeneralDominationPoly"))
    self.ui.actionBipartitionPoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["BipartitionPoly"],
                                        CALCNAMES["BipartitionPoly"],
                                        "lib.BipartitionPoly"))
    self.ui.actionDominationspolynom.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["DominationPoly"],
                                        CALCNAMES["DominationPoly"],
                                        "lib.DominationReliabilityDRel"))
    self.ui.actionTotalDomPoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["TotalDomPoly"],
                                        CALCNAMES["TotalDomPoly"],
                                        "lib.TotalDominationPoly"))
    self.ui.actionTriTotalDomPoly.triggered.connect(
      lambda: self.onCallFunctionMethodProduct(globalsClass.STARTMETHODS["TriTotalDomPoly"],
                                               CALCNAMES["TriTotalDomPoly"],
                                               "lib.TriTotalDominationPoly"))
    self.ui.actionConnectedDomPoly.triggered.connect(
      lambda: self.onCallFunctionMethodProduct(globalsClass.STARTMETHODS["ConnectedDomPoly"],
                                               CALCNAMES["ConnectedDomPoly"],
                                               "lib.ConnectedDominationPoly"))
    self.ui.actionIndDominationPoly.triggered.connect(
      lambda: self.onCallFunctionMethodProduct(globalsClass.STARTMETHODS["IndDomPoly"],
                                               CALCNAMES["IndDomPoly"],
                                               "lib.IndDominationPoly"))
    self.ui.actionPerfectDomination.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["PerfectDomPoly"],
                                        CALCNAMES["PerfectDomPoly"],
                                        "lib.PerfectDominationPoly"))

    self.ui.actionMatchingpolynom.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["MatchingPoly"],
                                        CALCNAMES["MatchingPoly"],
                                        "lib.Matching"))
    self.ui.actionExtendedMatching.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["ExtMatchingPoly"],
                                        CALCNAMES["ExtMatchingPoly"],
                                        "lib.ExtMatching"))
    self.ui.actionUnabhaenigkeitspoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["IndependencePoly"],
                                        CALCNAMES["IndependencePoly"],
                                        "lib.IndPoly"))
    self.ui.actionGeneralSubgraphCountingPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["GeneralSubgraphCountingPoly"],
                                        CALCNAMES["GeneralSubgraphCountingPoly"]))
    self.ui.actionExtendedSubgraphCountingPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["ExtendedSubgraphCountingPoly"],
                                        CALCNAMES["ExtendedSubgraphCountingPoly"]))
    self.ui.actionSubgraphPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["SubgraphPoly"],
                                        CALCNAMES["SubgraphPoly"]))
    self.ui.actionSubgraphEnumPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["SubgraphEnumeratingPoly"],
                                        CALCNAMES["SubgraphEnumeratingPoly"]))
    self.ui.actionSubgraphComponentPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["SubgraphComponentPoly"],
                                        CALCNAMES["SubgraphComponentPoly"]))
    self.ui.actionAlliancePoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["AlliancePoly"],
                                        CALCNAMES["AlliancePoly"],
                                        "lib.AlliancePoly")) 
    self.ui.actionIrredundancePoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["IrredundancePoly"],
                                        CALCNAMES["IrredundancePoly"]))
    self.ui.actionChromatischesPolynom.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["ChromaticPoly"],
                                        CALCNAMES["ChromaticPoly"],
                                        "lib.ChromaticPoly"))
    self.ui.actionRainbowPoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["RainbowPoly"],
                                        CALCNAMES["RainbowPoly"],
                                        "lib.RainbowPoly"))
    self.ui.actionRainbowGenFunction.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["RainbowGenFunction"],
                                        CALCNAMES["RainbowGenFunction"],
                                        "lib.RainbowGenFunction"))
    self.ui.actionTuttePoly.triggered.connect(self.onTuttePoly)
    self.ui.actionCutPolynomial.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["CutPoly"],
                                        CALCNAMES["CutPoly"]))
    self.ui.actionExtendedCutPolynomial.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["ExtCutPoly"],
                                        CALCNAMES["ExtCutPoly"],
                                        "lib.JPoly"))
    self.ui.actionVertexCoverPolynomial.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["VertexCoverPoly"],
                                        CALCNAMES["VertexCoverPoly"],
                                        "lib.VertexCoverPoly"))
    self.ui.actionEdgeCoverPolynomial.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["EdgeCoverPoly"],
                                        CALCNAMES["EdgeCoverPoly"],
                                        "lib.EdgeCoverPoly"))
    self.ui.actionRankPoly.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["RankPoly"],
                                        CALCNAMES["RankPoly"],
                                        "lib.RankPoly"))
    self.ui.actionBipartiteSubgraphPoly.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["BipartiteSubgraphPoly"],
                                        "Bipartite subgraph polynomial"))
    self.ui.actionDominatedPartitions_d.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["DominatedPartitionsPoly_d"],
                                        CALCNAMES["DominatedPartitionsPoly_d"],
                                        "lib.DominatedPartitionsPoly"))

    self.ui.actionReliability.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["Reliability"],
                                        CALCNAMES["Reliability"]))
    self.ui.actionAllTerminal.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["AllTerminalRelPoly"],
                                        CALCNAMES["AllTerminalRelPoly"],
                                        "lib.AllTerminalRelPoly"))

    self.ui.actionDominationszuverlaessigkeit.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["DomRelPoly"],
                                        CALCNAMES["DomRelPoly"],
                                        "lib.DominationReliabilityDRel"))
    self.ui.actionTotalDomRel.triggered.connect(
      lambda: self.onCallFunctionMethod(globalsClass.STARTMETHODS["TotalDomRelPoly"],
                                        CALCNAMES["TotalDomRelPoly"],
                                        "lib.TotalDominationPoly"))
    self.ui.actionIndDomRelPoly.triggered.connect(
      lambda: self.onCallFunctionMethodProduct(globalsClass.STARTMETHODS["IndDomRelPoly"],
                                               CALCNAMES["IndDomRelPoly"],
                                               "lib.IndDomRelPoly"))
    self.ui.actionVEDRel.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["VEDRel"],
                                        CALCNAMES["VEDRel"]))
    self.ui.actionEDRel.triggered.connect(self.onEDRelInfo)
    self.ui.actionEDRelA.triggered.connect(self.onEDRelAInfo)
    self.ui.actionPEDRel.triggered.connect(self.onPEDRelInfo)
    self.ui.actionSPEDRel.triggered.connect(self.onSPEDRelInfo)

    self.ui.actionConnectivity.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["VertexConnectivity"],
                                        CALCNAMES["VertexConnectivity"]))
    self.ui.actionEdgeConnectivity.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["EdgeConnectivity"],
                                        CALCNAMES["EdgeConnectivity"]))
    self.ui.actionUnabhaengigkeitszahl.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["IndependenceNumber"],
                                        CALCNAMES["IndependenceNumber"]))
    self.ui.actionCliqueNumber.triggered.connect(
      lambda: self.onCallSimpleFunction(globalsClass.STARTMETHODS["CliqueNumber"],
                                        CALCNAMES["CliqueNumber"]))
    self.ui.actionSpanTree.triggered.connect(self.onNumSpanningTrees)
    self.ui.actionMinSpanTree.triggered.connect(self.onMinSpanTree)
    self.ui.actionMaxSpanTree.triggered.connect(self.onMaxSpanTree)
    self.ui.actionTSP.triggered.connect(self.onTSP)
    self.ui.actionVertexColoring.triggered.connect(self.onVertexColoring)
    self.ui.actionEdgeColoring.triggered.connect(self.onEdgeColoring)
    self.ui.actionRainbowcoloring.triggered.connect(self.onRainbowcoloring)
    #self.ui.actionRainbowVertexColoring.triggered.connect(self.onRainbowVertexColoring)
    self.ui.actionGracefulLabeling.triggered.connect(self.onGracefulLabeling)

    self.ui.actionExtPython.triggered.connect(self.onExtPython)

    self.ui.actionPlanarity.triggered.connect(self.onPlanarity)
    self.ui.actionOGDF_FMMM.triggered.connect(self.onOGDF_FMMM)
    self.ui.actionOGDF_PSL.triggered.connect(self.onOGDF_PSL)
    self.ui.actionOGDF_PDL.triggered.connect(self.onOGDF_PDL)
    self.ui.actionOGDF_MML.triggered.connect(self.onOGDF_MML)
    self.ui.actionOGDF_SEFR.triggered.connect(self.onOGDF_SEFR)
    self.ui.actionOGDF_GEM.triggered.connect(self.onOGDF_GEM)

    # Tests
    self.ui.actionGenerateTestfile.triggered.connect(self.onGenerateTestfile)
    self.ui.actionStartTestCalculation.triggered.connect(self.onStartTestCalculation)
    self.ui.actionCompare.triggered.connect(self.onCompare)

    # Simulations
    self.ui.actionSimBFS.triggered.connect(self.onSimBFS)
    self.ui.actionSimDFS.triggered.connect(self.onSimDFS)
    self.ui.actionSimBroadcasting.triggered.connect(self.onSimBroadcasting)

    # Help
    self.ui.actionHilfe.triggered.connect(self.onShowHelp)
    self.ui.actionBugtracker.triggered.connect(self.onBugtracker)
    self.ui.actionInfo.triggered.connect(self.onShowProginfo)

    # Class variables
    self.thread = None  # Var for workingthreads
    self.max_node = 0  # Maximal vertex number in the graph
    self.tag = False  # Is something marked
    self.rel = ""  # Var for the current result
    self.changed = False  # Changed the graph?
    self.operations = deque([], 5)  # Saves the last five operations
    self.time_stamps = []  # Time stamps for time stat
    self.extPythonModule = ''  # Var for calling an extern python module
    self.extPythonFunction = ''  # Var for calling an extern python module
    self.productOne = None  # Var for the first graph in the graph product
    self.productTwo = None  # Var for the second graph in the graph product
    self.randomVertices = ''  # Var for the random graph dialog
    self.randomEdges = ''  # Var for the random graph dialog
    self.randomEdgeProb = ''  # Var for the random graph dialog
    self.randomAdjaFactor = 0.25  # Var for the random graph dialog
    self.randomLength = 600  # Var for the random graph dialog
    self.fname = None  # Var for the export of the graph (filename)
    self.imageformat = None  # Var fot the export of the graph (type of the picture)
    self.simulation = False  # Use the simulation function of PyGraphEdit?
    self.simulateType = None  # Type of the simulation
    self.simulateData = None  # Data for the simulation
    self.simulationTimeStep = 1  # Time in s for simulation step
    self.ui.pushButtonLoadAnimation.setEnabled(True)
    self.lengthPath = 0  # Length of the rainbow list coloring path
    self.numColors = 0  # Number of the colors
    self.lengthList = 0  # Length of the list

  def __activateGraphviz__(self):
    '''Use Graphviz to calculate the embedding of the graph'''
    self.ui.comboBoxDraw.clear()
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(0, _fromUtf8("dot"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(1, _fromUtf8("twopi"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(2, _fromUtf8("neato"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(3, _fromUtf8("circo"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(4, _fromUtf8("fdp"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(5, _fromUtf8("circo-intern"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(6, _fromUtf8("spring embedding"))
    globalsClass.PLOTMODES = deepcopy(PLOTMODES_GRAPHVIZ + PLOTMODES_INTERN)
    globalsClass.USE_GRAPHVIZ = True

  def __deactivateGraphviz__(self):
    '''Do not use Graphviz to calculate the embedding of the graph'''
    self.ui.comboBoxDraw.clear()
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(0, _fromUtf8("circo-intern"))
    self.ui.comboBoxDraw.addItem(_fromUtf8(""))
    self.ui.comboBoxDraw.setItemText(1, _fromUtf8("spring embedding"))
    globalsClass.PLOTMODES = deepcopy(PLOTMODES_INTERN)
    globalsClass.USE_GRAPHVIZ = False

  def __setElementWeight__(self):
    '''Set the weight of a vertex or an edge'''
    self.ui.graph.setWeight(tmp_element, tmp_weight)
    self.ui.graphicsView.scene().changeWeight(tmp_element, tmp_weight)

  def _setChanged(self):
    '''Mark the current graph as changed'''
    self.changed = True
    self.productOne = None
    self.productTwo = None

  def __setColor(self):
    '''Set the color for mark elements'''
    globalsClass.COLOR = str(self.ui.comboBoxColor.currentText())

  def __acitvateSimulation(self, simType=simulation_type.MARK,
                           data=[dict(), {1: "red"}]):
    '''Activate the simulation function'''
    if not set(range(1, self.ui.graph.order() + 1)) == set(self.ui.graph.getV()):
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Vor Benutzung der Simulationsfunktion den Graphen normalisieren!"))
      return

    self.simulateType = simType
    self.simulateData = data
    self.ui.horizontalSliderAnimation.setEnabled(True)
    self.ui.lineEditSimulation.setEnabled(True)
    self.ui.pushButtonPlay.setEnabled(True)
    self.ui.pushButtonBreak.setEnabled(True)
    self.ui.pushButtonStop.setEnabled(True)
    self.ui.pushButtonExportAnimation.setEnabled(True)
    self.ui.pushButtonSaveAnimation.setEnabled(True)
    self.ui.lineEditSimulation.setText(str(0))
    self.ui.horizontalSliderAnimation.setMinimum(0)
    self.ui.horizontalSliderAnimation.setMaximum(self.simulateData.getNumSteps() - 1)
    self.__simulate__()

  def deacitvateSimulation(self, new=False):
    '''Deactivate the simulation function'''
    value = True
    if not new and self.simulateData is not None:
      com = QtWidgets.QApplication.translate("PyGraphEdit",
                                             """Wollen Sie den Graph wirklich �ndern?

      Alle Simulationsdaten gehen dabei verloren!""")
      value = fedit("yesorno", title="Info", comment=com)
    elif new:
      value = True
    else:
      return True

    if value:
      self.ui.horizontalSliderAnimation.setSliderPosition(0)
      self.ui.horizontalSliderAnimation.setMinimum(0)
      self.ui.horizontalSliderAnimation.setMaximum(10)
      self.ui.lineEditSimulation.setText("")
      self.simulateType = None
      self.simulateData = None
      self.ui.horizontalSliderAnimation.setEnabled(False)
      self.ui.lineEditSimulation.setEnabled(False)
      self.ui.pushButtonPlay.setEnabled(False)
      self.ui.pushButtonBreak.setEnabled(False)
      self.ui.pushButtonStop.setEnabled(False)
      self.ui.pushButtonExportAnimation.setEnabled(False)
      self.ui.pushButtonSaveAnimation.setEnabled(False)
      return True
    else:
      return False

  def __simulate__(self):
    '''A new simulation step. Called if the slider is changed.'''
    # Mark vertices and edges of the graph
    if (self.simulateType == simulation_type.MARK or
        self.simulateType == simulation_type.MARKDIFF):
      self.ui.graphicsView.scene().unmarkall()

    # Simulation with show and hide vertices and edges
    if self.simulateType == simulation_type.SHOW:
      self.ui.graphicsView.scene().hideall()
    elif (self.simulateType == simulation_type.SHOWDIFF
          and self.ui.horizontalSliderAnimation.sliderPosition() == 0):
      self.ui.graphicsView.scene().hideall()

    # Show the simulation step
    if self.simulateType == simulation_type.MARK:
      self.tag = True
      value = self.ui.horizontalSliderAnimation.sliderPosition()
      self.ui.lineEditSimulation.setText(str(value))
      step = self.simulateData.getStep(value)
      for v in step:
        if type(v) == int:
          self.ui.graphicsView.scene().markVertex(v, step[v])
        elif type(v) == tuple:
          self.ui.graphicsView.scene().markEdge(v[0], v[1], step[v])

    elif self.simulateType == simulation_type.MARKDIFF:
      self.tag = True
      value = self.ui.horizontalSliderAnimation.sliderPosition()
      self.ui.lineEditSimulation.setText(str(value))
      for i in range(value + 1):
        step = self.simulateData.getStep(i)
        for v in step:
          if type(v) == int:
            self.ui.graphicsView.scene().markVertex(v, step[v])
          elif type(v) == tuple:
            self.ui.graphicsView.scene().markEdge(v[0], v[1], step[v])

    elif (self.simulateType == simulation_type.SHOW
          or self.simulateType == simulation_type.SHOWDIFF):
      self.tag = False
      value = self.ui.horizontalSliderAnimation.sliderPosition()
      self.ui.lineEditSimulation.setText(str(value))
      for v in self.simulateData.getStep(value):
        self.ui.graphicsView.scene().showElement(v)

    else:
      pass

  def __runSimulation(self):
    '''Run the simulation'''
    self.simulationWorker.setValues(self.ui.horizontalSliderAnimation.sliderPosition(),
                                    self.ui.horizontalSliderAnimation.maximum(),
                                    self.simulationTimeStep)
    self.simulationWorker.start()

  def __breakSimulation(self):
    '''Break the simulation'''
    self.simulationWorker.simBreak = True

  def __stopSimulation(self):
    '''Stop the simulation'''
    self.simulationWorker.simBreak = True
    self.ui.horizontalSliderAnimation.setSliderPosition(0)

  def __jumpStepSimulation(self, step=None):
    '''Jump to the position in the text field'''
    if step is None:
      value = int(self.ui.lineEditSimulation.text())
    else:
      value = int(step)
    if value >= 0 and value <= self.ui.horizontalSliderAnimation.maximum():
      self.ui.horizontalSliderAnimation.setSliderPosition(value)

  def __exportSimulation(self):
    '''Start the export of the simulation'''
    self.svgExportDialog.lineEditSimStepTime.setEnabled(True)
    self.svgExportDialog.show()

  def __exportSimulationSVG(self):
    '''Export the simulation to a svg-picture'''
    # get the file name from the dialog
    fname = self.svgExportDialog.file
    if not fname or len(fname) == 0:
      return

    # Get the coordinates and/or colors of the elements
    coord = {}
    colors = {}
    for item in self.ui.graphicsView.scene().items():
      # item is a vertex
      if item.type() == globalsClass.USERTYPE + 1:
        coord[int(item.value)] = (item.pos().x(), -item.pos().y())
        if self.svgExportDialog.checkBoxOwnColors.isChecked():
          colors[int(item.value)] = (COLORTIKZ[item.color], COLORTIKZ[item.color2])
        elif item.marked:
          colors[int(item.value)] = (self.svgExportDialog.fillingColorMarked,
                                     self.svgExportDialog.colorLineMarked)
        else:
          colors[int(item.value)] = (self.svgExportDialog.fillingColor,
                                     self.svgExportDialog.colorLine)

      # item is an edge
      elif item.type() == globalsClass.USERTYPE + 2:
        if self.svgExportDialog.checkBoxOwnColors.isChecked():
          colors[(int(item.source.value), int(item.dest.value))] = COLORTIKZ[item.color]
        elif item.marked:
          colors[(int(item.source.value), int(item.dest.value))] = self.svgExportDialog.edgeColorMarked
        else:
          colors[(int(item.source.value), int(item.dest.value))] = self.svgExportDialog.edgeColor

    # Start the generation of the picture
    generateSVG(self.ui.graph, coord, fname, colors,
                self.svgExportDialog.checkBoxShowVertexNum.isChecked(),
                float(self.svgExportDialog.vertexSize),
                float(self.svgExportDialog.edgeSize),
                self.simulateData,
                float(self.svgExportDialog.simStepTime),
                self.simulateType)

    self.exportDoneSignal.emit()

  def __loadSimulation(self):
    '''Load a simluation'''
    self.loadSimulationDialog.show()

  def __loadSimulation__(self):
    '''This method is called after the ok-event of the LoadSimulationDialog'''
    self.__load__()

    # Get some global variables
    fname = globalsClass.FNAME
    graphname = globalsClass.GRAPHNAME
    simname = globalsClass.SIMNAME

    # Load the simulation
    data, simtype = loadSimulation(fname, graphname, simname)

    # If the loading failed, then exit
    if not data:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Datei existiert nicht oder hat das falsche Format!"))
    else:  # draw the graph
      self.__acitvateSimulation(simtype, data)

  def __saveSimulation(self):
    '''Save the current simulation'''
    # file dialog
    fname, ffilter = QtWidgets.QFileDialog.getSaveFileName(self, 'Save',
                                                           config["Paths"]["data_dir"],
                                                           "Datenbank (*.sdb)",
                                                           options=QtWidgets.QFileDialog.DontConfirmOverwrite)
    if fname is None or len(fname) == 0:
      return

    # check the filetype
    if "sdb" in ffilter:
      # Add the correct filetype
      if ".sdb" not in fname:
        fname += ".sdb"

    # show save dialog
    if self.ui.graph.name:
      self.saveSimulationDialog.graphname = self.ui.graph.name
    self.saveSimulationDialog.fname = fname
    self.saveSimulationDialog.show()

  def __saveSimulation__(self):
    '''This function is called after the OK-event from the saveSimulation-Dialog'''
    # Get some infos from the saveSimulation dialog
    graphname = self.saveSimulationDialog.graphname
    simname = self.saveSimulationDialog.simname
    fname = self.saveSimulationDialog.fname

    # Get the graphnames in the file
    graphnames = getGraphNamesdb(fname)

    # Check if the graph already exists in the file
    if graphnames is not None and graphname in graphnames:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Graph wirklich �berschreiben'),
                   False)]
      values2 = fedit(datalist,
                      title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                             "Graphenname"))
      if values2 is None or values2[0] is False:
        return

      simulations = getSimNames(fname, graphname)
      if simulations and simname in simulations:
        datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                      'Simulation wirklich �berschreiben'),
                     False)]
        values2 = fedit(datalist,
                        title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                               "Simulationsname"))
        if values2 is None or values2[0] is False:
          return

    self.ui.graph.name = graphname

    # Save the current location of the vertices
    coord = {}
    for w in self.ui.graphicsView.items():
      if w.type() == globalsClass.USERTYPE + 1:
        coord[int(w.value)] = (w.pos().x(), w.pos().y())

    # Save the simulation
    saveSimulation(fname, self.ui.graph, coord, simname,
                   self.simulateData, self.simulateType,
                   True, True)

    # Info for the user
    fedit(None, title="Info",
          comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Speichern war erfolgreich"))

  def onDelmark(self):
    '''Delete all selected elements'''
    self.ui.graphicsView.scene().onActionDeleteAll()
    self.operations.clear()

  def onMarkAll(self):
    '''Select all elements'''
    self.ui.graphicsView.scene().markall()
    self.tag = True

  def onEntmark(self):
    '''Unselect all elements'''
    self.ui.graphicsView.scene().unmarkall()
    self.tag = False

  def onAkt(self):
    '''Refresh the view'''
    globalsClass.PLOTSTYLE = str(self.ui.comboBoxDraw.currentText())
    self.ui.graphicsView.scene().clear()
    self.ui.setVertices()

  def onClear(self):
    '''Relabel the vertices'''
    if self.ui.graph.order() == 0:
      return

    # Rename the vertices of the SGraph and the graphic node names
    V = list(self.ui.graph.getV())
    T = dict([(V[i - 1], i) for i in range(1, len(V) + 1)])
    for v in T:
      self.ui.graph.renameVertex(v, T[v])
      self.ui.graphicsView.scene().renameNodes(v, T[v])

    # Reset the variables
    self.max_node = max(self.ui.graph.getV())
    self.operations.clear()

  def onNew(self):
    '''Draw a new graph'''
    # Ask the user
    values = True
    if self.changed:
      if self.ui.graph.order() != 0:
        com = QtWidgets.QApplication.translate("PyGraphEdit",
                                               """Wollen Sie den aktuellen Graph wirklich �berschreiben?

        Alle nicht gespeicherten �nderungen gehen dabei verloren!""")
        values = fedit("yesorno", title="Info", comment=com)

    # Generate a new empty graph
    if values:
      self.productOne = None
      self.productTwo = None
      self.operations.clear()
      self.ui.graph = SWGraph()
      self.ui.graphicsView.scene().clear()
      self.max_node = 0
      self.deacitvateSimulation(new=True)
      return True
    return False

  def onLoad(self):
    '''Load a graph from a database or a xml-file'''
    self.ladendialog.show()

  def onLoadOSM(self):
    '''Load a graph from an Open Street Maps xml-file'''
    self.loadosmdialog.show()

  def __loadosm__(self):
    '''
    Converts the OSM map file into a graph
    this function will be called after the ok-request of the loadOSM-dialog
    '''
    # Ask the user if he want to save the current graph
    if not self.onNew():
      return
    fname = self.loadosmdialog.fname
    priority = self.loadosmdialog.priority
    if not fname:
      return
    if isMapLarge(fname, priority):
      com = """      The size of the mapfile you have chosen is big.

      It may take a long time for the program to parse the file if the priority value is low.
      
      Do you want to continue?"""
      value = fedit("yesorno", title="Info", comment=com)
      if not value:
        return
    V, E, d = parseMapfile(fname, priority)
    G = SGraph(V, E)
    G, coord = reduceOsmGraph(G, d)
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(G)
    self.ui.setVertices(coord)
    self.max_node = self.ui.graph.order()
    self.changed = False

  def __load__(self):
    '''
    Load the graph GRAPHNAME in the file FNAME
    this function will be called after the ok-request of the load-dialog
    '''
    # Ask the user if he want to save the current graph
    if not self.onNew():
      return

    # Get some globalsClass
    fname = globalsClass.FNAME
    graphname = globalsClass.GRAPHNAME

    # Load the graph
    self.ui.graph, coord = self.loadGraph(fname, graphname)

    # If the loading failed, then exit
    if self.ui.graph is None or self.ui.graph.order() == 0:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Datei existiert nicht oder hat das falsche Format!"))
    else:  # Draw the graph
      self.ui.setVertices(coord)
      self.max_node = self.ui.graph.order()
      self.changed = False
      if globalsClass.PLOTSTYLE in PLOTMODES_GRAPHVIZ:
        self.ui.comboBoxDraw.setCurrentIndex(
            PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))
      else:
        self.ui.comboBoxDraw.setCurrentIndex(
            len(PLOTMODES_GRAPHVIZ) +
            PLOTMODES_INTERN.index(globalsClass.PLOTSTYLE))

  def on_Load2Graphs(self):
    '''Load two graphs and display them side by side'''
    self.loadDialog2Graphs.show()

  def __load2Graphs(self):
    '''Load two graphs and display them side by side'''
    # ask the user if he want to save the current graph
    if not self.onNew():
      return

    # Load the first graph
    graph1, coord1 = self.loadGraph(self.loadDialog2Graphs.graph1File,
                                    self.loadDialog2Graphs.graph1Name)
    graph2, coord2 = self.loadGraph(self.loadDialog2Graphs.graph2File,
                                    self.loadDialog2Graphs.graph2Name)

    # If the loading failed, then exit
    if not graph1 or not graph2:
      return

    # If no coordinates exist, then calculate them
    if not coord1:
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1)
      coord1, _, _ = self.ui.calcCoordinates()
    if not coord2:
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph2)
      coord2, _, _ = self.ui.calcCoordinates()

    # Calculate the minimum and maximum coordinates
    # and a delta value for the shift of the second graph
    _, xmax, _, _ = self.ui.get_max_min(coord1)
    xmin2, _, _, _ = self.ui.get_max_min(coord2)
    deltax = xmax - xmin2 + self.loadDialog2Graphs.shift

    # Generate the coresponding SGraph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(graph1 | graph2)

    # Transform the coordinates of the second graph
    n = graph1.order()
    for v in coord2:
      coord1[int(v) + n] = (coord2[v][0] + deltax, coord2[v][1])

    # Draw the graph
    self.ui.setVertices(coord1)
    self.max_node = self.ui.graph.order()
    self.changed = False

  def loadGraph(self, fname, graphname):
    '''
    Load the graph graphname in fname
    @param fname: Filename
    @param graphname: Name of the graph
    @return: SGraph, coordinates
    '''
    if fname is None or len(fname) == 0:
      return

    graph = None
    coord = None

    # check the file extension
    db = False
    graphml = False
    gml = False
    xml = False
    leda = False
    if ".db" in fname or ".sdb" in fname:
      db = True
    elif ".gml" in fname:
      gml = True
    elif ".graphml" in fname:
      graphml = True
    elif ".xml" in fname:
      xml = True
    elif ".leda" in fname or ".LEDA" in fname:
      leda = True

    # Load the graph
    if db:
      return_param = load_graphdb(fname, graphname, graphtype="SWGraph")
    elif graphml:
      parser = GraphMLParser()
      return_param = parser.parse(fname, graphname, graphtype="SWGraph")
    elif gml:
      parser = GMLParser()
      return_param = parser.read_gml(fname, graphtype="SWGraph")
    elif xml:
      return_param = load_graph(fname, graphname, graphtype="SWGraph")
    elif leda:
      parser = LEDAParser()
      return_param = parser.load_graph_LEDA(fname, graphtype="SWGraph")
    else:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Unbekanntes Dateiformat!"))

    # Assigne the graph and coordinate variables
    if graphml or leda:  # graphml stores no information about the coordinates
      graph = return_param
      coord = None
    else:
      graph = return_param[0]
      coord = return_param[1]

    return graph, coord

  def onSave(self):
    '''Save a graph'''
    # file dialog
    fname, ffilter = QtWidgets.QFileDialog.getSaveFileName(self, 'Save',
                                                           config["Paths"]["data_dir"],
                                                           "Datenbank (*.db);;Datenbank (*.sdb);;XML-files (*.xml);;GraphML (*.graphml);;GML (*.gml)",
                                                           options=QtWidgets.QFileDialog.DontConfirmOverwrite)
    if fname is None or len(fname) == 0:
      return

    # check the filetype
    db = False
    graphml = False
    gml = False
    if "db" in ffilter:
      # Add the correct filetype
      if ".db" not in fname:
        fname += ".db"
      db = True
    if "sdb" in ffilter:
      # Add the correct filetype
      if ".sdb" not in fname:
        fname += ".sdb"
      db = True
    if "gml" in ffilter:
      # Add the correct filetype
      if ".gml" not in fname:
        fname += ".gml"
      gml = True

      # Look if the file already exist
      import os.path
      if os.path.isfile(fname):
        com = QtWidgets.QApplication.translate("PyGraphEdit",
                                           """Wollen Sie den aktuellen Graph wirklich �berschreiben?

        Im gml-Format kann nur ein Graph pro Datei gespeichert werden!
        Wenn Sie mehr Graphen in einer Datei speichern wollen, dann
        speichern Sie bitte in einer Datenbank (.db) oder einer xml-Datei.""")
        value = fedit("yesorno", title="Info", comment=com)

        if not value:
          return

    elif "xml" in ffilter and ".xml" not in fname:
      # Add the correct filetype
      fname += ".xml"
    elif "graphml" in ffilter:
      graphml = True
      # Add the correct filetype
      if ".graphml" not in fname:
        fname += '.graphml'

      # Look if the file already exist
      if os.path.isfile(fname):
        com = QtWidgets.QApplication.translate("PyGraphEdit",
                                           """Wollen Sie den aktuellen Graph wirklich �berschreiben?

        Im graphml-Format kann nur ein Graph pro Datei gespeichert werden!
        Wenn Sie mehr Graphen in einer Datei speichern wollen, dann
        speichern Sie bitte in einer Datenbank (.db) oder einer xml-Datei.""")
        value = fedit("yesorno", title="Info", comment=com)

        if not value:
          return

    # get the graphnames in the file
    if db:
      graphnames = getGraphNamesdb(fname)
    elif graphml:
      graphnames = getGraphNamesGraphml(fname)
    elif gml:
      graphnames = getGraphNamesGML(fname)
    else:
      graphnames = getGraphNames(fname)

    # show save dialog
    if self.ui.graph.name is not None and not graphml:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Name des Graphen'),
                   str(self.ui.graph.name)),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'In Datei vorhandene Graphen'),
                   graphnames),
                  ('graph6-Format?', True)]
    elif not graphml and not gml:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Name des Graphen'), "Graph 1"),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'In Datei vorhandene Graphen'),
                   graphnames),
                  ('graph6-Format?', True)]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Name des Graphen'),
                   "Graph 1")]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Graphenname"))

    if not values:
      return

    # check if the graph already exists in the file
    if (not graphml and not gml and graphnames is not None
        and values[0] in graphnames):
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Graph wirklich �berschreiben'),
                   False)]
      values2 = fedit(datalist,
                      title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                             "Graphenname"))
      if values2 is None or values2[0] is False:
        return

    self.ui.graph.name = values[0]

    # Normalize the graph
    self.onClear()

    # Save the current location of the vertices
    coord = {}
    for w in self.ui.graphicsView.items():
      if w.type() == globalsClass.USERTYPE + 1:
        coord[int(w.value)] = (w.pos().x(), w.pos().y())

    # Save the graph
    if db:
      save_graphdb(self.ui.graph, fname, values[0], coord, values[2])
    elif graphml:
      parser = GraphMLParser()
      parser.write(self.ui.graph, fname, name=values[0])
    elif gml:
      parser = GMLParser()
      parser.write_gml(self.ui.graph, fname, coord)
    else:
      save_graph(self.ui.graph, fname, values[0], coord)

    # update the variable
    self.changed = False

    # info for the user
    fedit(None, title="Info",
          comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Speichern war erfolgreich"))

  def onExport(self):
    '''Save the graph as picture'''
    if not globalsClass.USE_GRAPHVIZ:
      fname, ffilter = QtGui.QFileDialog.getSaveFileName(self,
                                                         'Save graph as picture',
                                                         config["Paths"]["output_dir"],
                                                         "PNG-file (*.png);; JPG-file (*.jpg);;SVG-file (*.svg)")
    else:
      fname, ffilter = QtWidgets.QFileDialog.getSaveFileName(self,
                                                             'Save graph as picture',
                                                             config["Paths"]["output_dir"],
                                                             "PNG-file (*.png);; JPG-file (*.jpg);; EPS-file (*.eps);; SVG-file (*.svg);;PDF (*.pdf)")

    if fname is None or len(fname) == 0:
      return

    # check the format for the export
    if "png" in ffilter:
      imageformat = 'png'
      if ".png" not in fname:
        fname += ".png"
    elif "eps" in ffilter:
      imageformat = 'eps'
      if ".eps" not in fname:
        fname += ".eps"
    elif "jpg" in ffilter:
      imageformat = 'jpg'
      if ".jpg" not in fname:
        fname += ".jpg"
    elif "pdf" in ffilter:
      imageformat = 'pdf'
      if ".pdf" not in fname:
        fname += ".pdf"
    elif "svg" in ffilter:
      imageformat = 'svg'
      if ".svg" not in fname:
        fname += ".svg"

    # save the picture
    self._savePicture(fname, imageformat)

  def _savePicture(self, fname, imageformat='png'):
    '''Save the current graph as a picture'''
    self.fname = fname
    self.imageformat = imageformat

    if imageformat == "svg" and globalsClass.USE_GRAPHVIZ:
      self.exportDialog.show(usableButtons=["graphivz", "intern"])
    elif imageformat == "svg":
      self._savePictureIntern(fname, imageformat)
    elif (imageformat == "jpg" or imageformat == "png") and globalsClass.USE_GRAPHVIZ:
      self.exportDialog.show(usableButtons=["graphivz", "Qt"])
    elif imageformat == "jpg" or imageformat == "png":
      self._savePictureQt(fname, imageformat)
    else:
      self._savePictureGraphviz(fname, imageformat)

  def _savePictureQt(self, fname=None, imageformat=None):
    '''Save the current graph with Qt (.jpg, .png)'''
    if fname is None:
      fname = self.fname
    if imageformat is None:
      imageformat = self.imageformat

    if (imageformat == "jpg" or imageformat == "png"):
      # Init the image
      image = QtGui.QImage(2500, 2500, QtGui.QImage.Format_RGB32)
      image.fill(QtCore.Qt.white)

      # Init the painter
      painter = QtGui.QPainter(image)

      # Render the View and save the image
      self.ui.graphicsView.render(painter)
      image.setText("comment", "Generated by PyGraphEdit v%s" % VERSION)
      image.save(fname, quality=100)

      painter.end()

      self.exportDoneSignal.emit()

  def _savePictureIntern(self, fname=None, imageformat=None):
    '''Save the current graph with an intern method (.svg)'''
    if fname is None:
      fname = self.fname
    if imageformat is None:
      imageformat = self.imageformat

    # SVG without graphviz
    self.svgExportDialog.lineEditSimStepTime.setEnabled(False)
    self.svgExportDialog.lineEditFile.setText(fname)
    self.svgExportDialog.show()

  def _savePictureGraphviz(self, fname=None, imageformat=None):
    '''Save the current graph with graphviz (.jpg, .png, .eps, .svg)'''
    if fname is None:
      fname = self.fname
    if imageformat is None:
      imageformat = self.imageformat

    # generate a Dot-graph for the export
    graph = Dot(config, graph_type='graph', outputorder="edgesfirst")
    graph.plotstyle = globalsClass.PLOTSTYLE

    # adding all vertices to the Dot-graph
    for item in self.ui.graphicsView.scene().items():
      if item.type() == globalsClass.USERTYPE + 1:
        color = COLORRGB[item.color]
        if Vertex.drawNumber:
          node = pydot.Node(name=str(item.value),
                            pos="%f,%f!" % (item.pos().x(), -item.pos().y()),
                            width="%f" % (Vertex.vertexSize * 0.01),
                            fillcolor=color,
                            style="filled",
                            margin="-4.0",
                            fontsize="8",
                            shape="circle")
        else:
          node = pydot.Node(name=str(item.value),
                            label="",
                            pos="%f,%f!" % (item.pos().x(), -item.pos().y()),
                            width="%f" % (Vertex.vertexSize * 0.01),
                            fillcolor=color,
                            style="filled",
                            margin="-4.0",
                            fontsize="8",
                            shape="circle")

        graph.add_node(node)

    # adding all edges to the Dot-graph
    for item in self.ui.graphicsView.scene().items():
      if item.type() == globalsClass.USERTYPE + 2:
        linecolor = COLORTIKZ[item.color]
        u = int(item.source.value)
        v = int(item.dest.value)
        edge = pydot.Edge(str(v), str(u), penwidth="0.5", color=linecolor)
        graph.add_edge(edge)

    # Save the picture
    graph.write2_img(fname, imageformat=imageformat)

    self.exportDoneSignal.emit()

  def __exportSVG(self):
    '''Called after the ok-signal from the svg-export dialog'''
    if self.svgExportDialog.lineEditSimStepTime.isEnabled():
      self.__exportSimulationSVG()
      return

    # get the file name from the dialog
    fname = self.svgExportDialog.file
    if not fname or len(fname) == 0:
      return

    # Get the coordinates and the colors
    coord = {}
    colors = {}
    for item in self.ui.graphicsView.scene().items():
      # item is a vertex
      if item.type() == globalsClass.USERTYPE + 1:
        coord[int(item.value)] = (item.pos().x(), -item.pos().y())
        if self.svgExportDialog.checkBoxOwnColors.isChecked():
          colors[int(item.value)] = (COLORTIKZ[item.color], COLORTIKZ[item.color2])
        elif item.marked:
          colors[int(item.value)] = (self.svgExportDialog.fillingColorMarked,
                                     self.svgExportDialog.colorLineMarked)
        else:
          colors[int(item.value)] = (self.svgExportDialog.fillingColor,
                                     self.svgExportDialog.colorLine)

      # item is an edge
      elif item.type() == globalsClass.USERTYPE + 2:
        if self.svgExportDialog.checkBoxOwnColors.isChecked():
          colors[(int(item.source.value), int(item.dest.value))] = COLORTIKZ[item.color]
        elif item.marked:
          colors[(int(item.source.value), int(item.dest.value))] = self.svgExportDialog.edgeColorMarked
        else:
          colors[(int(item.source.value), int(item.dest.value))] = self.svgExportDialog.edgeColor

    # Start the generation of the picture
    generateSVG(self.ui.graph, coord, fname, colors,
                self.svgExportDialog.checkBoxShowVertexNum.isChecked(),
                float(self.svgExportDialog.vertexSize),
                float(self.svgExportDialog.edgeSize))

    self.exportDoneSignal.emit()

  def exportDone(self):
    fedit(None, title="Info",
          comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Export war erfolgreich"))

  def onExportTikZ(self):
    '''Export the graph in TikZ'''
    if self.ui.graph.order() == 0:
      return

    # get the "size" of the graph
    xmin, xmax, _, _ = self.ui.get_max_min()

    # calculate the scale-factor for the TikZ-graph
    x = abs(xmin) + abs(xmax)
    scale = 5.0 / x

    self.tikzExportDialog.scale = scale
    self.tikzExportDialog.show()

  def __exportTikZ(self):
    '''Called after Ok-Signal from tikzExportDialog'''
    # get the file name from the dialog
    fname = self.tikzExportDialog.file
    if not fname or len(fname) == 0:
      return

    # check the format
    if ".tex" not in fname:
      fname += ".tex"

    # strings for the tex-file
    pgfdeclarelayer = "\\pgfdeclarelayer{background}\n\\pgfsetlayers{background,main}\n"
    begin_figure = "\\begin{figure}[h]\n"
    begin_centering = "\\centering\n"
    begin_tikz = "\\begin{tikzpicture}[scale=%.4f]\n"
    tikz_style_vertex1 = "\\tikzstyle{place}=[circle,thick,inner sep=0pt,minimum size=%.1fmm, font=\\fontsize{%d}{%d}\\selectfont];\n"
    tikz_style_edge1 = "\\tikzstyle{edge}=[-, line width=%.1fpt];\n"
    end_tikz = "\\end{tikzpicture}\n"
    caption = "\caption{%s}\n"
    label = "\label{%s}\n"
    end_figure = "\end{figure}"
    vertex = "\\node (%d) at (%.2f,%.2f) [place, draw=%s!50,fill=%s!20]{%s};\n"
    edge = "\\draw [edge, %s] (%d)--(%d);\n"
    background = "\\begin{pgfonlayer}{background}\n"
    background_end = "\\end{pgfonlayer}\n"

    # start writing the file
    with open(fname, 'w') as file:
      if self.tikzExportDialog.edgesBackground:
        file.write(pgfdeclarelayer)

      # write the header
      file.write(begin_figure)
      file.write(begin_centering)
      file.write(begin_tikz % self.tikzExportDialog.scale)
      file.write(tikz_style_vertex1 % (self.tikzExportDialog.vertexSize,
                                       self.tikzExportDialog.fontSize,
                                       self.tikzExportDialog.fontSize))
      file.write(tikz_style_edge1 % (self.tikzExportDialog.edgeSize))
      file.write("\n")

      # get the coordinates and calculate the scale-factor
      scale_factor = 1.0
      xmin, xmax, ymin, ymax = self.ui.get_max_min()
      max_value = max(-xmin, xmax, -ymin, ymax)
      if max_value > 500:
        scale_factor = 500.0 / max_value

      # write the vertices
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          # Use the displayed colors
          if self.tikzExportDialog.checkBoxOwnColors.isChecked():
            if self.tikzExportDialog.showVertexNum:
              file.write(vertex % (int(item.value),
                                   item.pos().x() * scale_factor,
                                   -item.pos().y() * scale_factor,
                                   COLORTIKZ[item.color2],
                                   COLORTIKZ[item.color],
                                   str(item.value)))
            else:
              file.write(vertex % (int(item.value), item.pos().x() * scale_factor,
                                   -item.pos().y() * scale_factor,
                                   COLORTIKZ[item.color2],
                                   COLORTIKZ[item.color], ""))

          # Use the colors from the dialog
          else:
            if item.marked:
              color = self.tikzExportDialog.colorLineMarked
              color2 = self.tikzExportDialog.fillingColorMarked
            else:
              color = self.tikzExportDialog.colorLine
              color2 = self.tikzExportDialog.fillingColor
            if self.tikzExportDialog.showVertexNum:
              file.write(vertex % (int(item.value),
                                   item.pos().x() * scale_factor,
                                   -item.pos().y() * scale_factor,
                                   color, color2, str(item.value)))
            else:
              file.write(vertex % (int(item.value),
                                   item.pos().x() * scale_factor,
                                   -item.pos().y() * scale_factor,
                                   color, color2, ""))
      file.write("\n")

      # write the edges
      if self.tikzExportDialog.edgesBackground:
        file.write(background)
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 2:
          u = int(item.source.value)
          v = int(item.dest.value)
          if not self.tikzExportDialog.checkBoxOwnColors.isChecked():
            if item.marked:
              color = self.tikzExportDialog.edgeColorMarked
            else:
              color = self.tikzExportDialog.edgeColor
            file.write(edge % (color, v, u))
          else:
            file.write(edge % (COLORTIKZ[item.color], v, u))
      if self.tikzExportDialog.edgesBackground:
        file.write(background_end)

      # write the end-strings, caption and label
      file.write(end_tikz)
      if self.tikzExportDialog.checkBoxCaption.isChecked():
        file.write(caption % self.tikzExportDialog.caption)
      if self.tikzExportDialog.checkBoxLabel.isChecked():
        file.write(label % self.tikzExportDialog.label)
      file.write(end_figure)

    fedit(None, title="Info",
          comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Export war erfolgreich"))

  def onPrint(self):
    '''
    Print the current graph.
    This function is still under construction.
    '''
    # Init the printer
    printer = QtPrintSupport.QPrinter(mode=QtPrintSupport.QPrinter.HighResolution)
    printer.setPaperSize(QtPrintSupport.QPrinter.A4)

    # Show the printing dialog
    dialog = QtPrintSupport.QPrintDialog(printer)
    dialog.setModal(True)
    dialog.setWindowTitle("Print Graph")

    # If the dialog is closed, start printing
    if dialog.exec_():
      painter = QtGui.QPainter(printer)
      self.ui.graphicsView.render(painter)
      painter.end()

  def onSettings(self):
    '''Show the preferences'''
    self.configDialog.lineEditVertexsize.setText(str(Vertex.vertexSize))
    self.configDialog.checkVertexNumber.setChecked(Vertex.drawNumber)
    self.configDialog.lineEditEdgeWidth.setText(str(Edge.edgeWidth))
    self.configDialog.lineEditVertexWeight.setText(str(Vertex.defaultVertexWeight))
    self.configDialog.lineEditEdgeWeight.setText(str(Edge.defaultEdgeWeight))
    self.configDialog.checkBoxKnotengewichte.setChecked(Vertex.displayVertexWeight)
    self.configDialog.checkBoxKantengewichte.setChecked(Edge.displayEdgeWeight)

    if globalsClass.USE_GRAPHVIZ:
      self.configDialog.checkBoxGraphviz.setChecked(True)
    else:
      self.configDialog.checkBoxGraphviz.setChecked(False)

    if config["DEFAULT"]["loglevel"] == "Info":
      self.configDialog.comboBoxLoglevel.setCurrentIndex(0)
    elif config["DEFAULT"]["loglevel"] == "Debug":
      self.configDialog.comboBoxLoglevel.setCurrentIndex(1)
    elif config["DEFAULT"]["loglevel"] == "Error":
      self.configDialog.comboBoxLoglevel.setCurrentIndex(2)
    elif config["DEFAULT"]["loglevel"] == "Critical":
      self.configDialog.comboBoxLoglevel.setCurrentIndex(3)
    elif config["DEFAULT"]["loglevel"] == "Warning":
      self.configDialog.comboBoxLoglevel.setCurrentIndex(4)

    self.configDialog.lineEditLogDatei.setText(config["DEFAULT"]["logfile"])
    self.configDialog.show()

  def __applySettings(self):
    '''Apply the settings (e.g. vertex size)'''
    self.updateView()

  def closeEvent(self, event):
    '''Overwrites the close event of the main window'''
    # ask the user
    if self.ui.graph.order() != 0 and self.changed:
      com = QtWidgets.QApplication.translate("PyGraphEdit", """Wollen Sie das Programm wirklich beenden?

      Alle nicht gespeicherten �nderungen gehen dabei verloren!""")
    else:
      self.closeDialog.pushButtonSaveClose.setEnabled(False)
      com = QtWidgets.QApplication.translate("PyGraphEdit",
                                             "Wollen Sie das Programm wirklich beenden?")

    # show the dialog
    self.closeDialog.label.setText(com)
    self.closeDialog.exec_()
    values = self.closeDialog.returnValue

    # look what the user wants
    if values == "Close":
      PyGraphEdit_log.info("bye...")
      event.accept()
    elif values == "SaveClose":
      self.onSave()
      PyGraphEdit_log.info("bye...")
      event.accept()
    elif values == "Abort":
      event.ignore()
    else:
      PyGraphEdit_log.info("bye...")
      event.accept()

  def onClose(self):
    '''Close PyGraphEdit'''
    self.close()

  def onDiamant(self):
    '''Generate the diamant graph'''
    if not self.onNew():
      return

    globalsClass.PLOTSTYLE = 'circo'

    self.ui.graph = SWGraph(V=[1, 2, 3, 4],
                            E=set([(1, 2), (1, 3), (2, 3), (2, 4), (3, 4)]),
                            name="Diamant")
    self.ui.setVertices()
    self._setChanged()
    self.max_node = 4

  def onPath(self):
    '''Generate a path'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil',
                                                  None, -1),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                                    "Weg"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.path(n))
    self.ui.setVertices()
    self._setChanged()

  def onCompleteGraph(self):
    '''Generate the complete graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Vollst�ndiger Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.completeGraph(n))
    self.ui.setVertices()
    self._setChanged()

  def onCycle(self):
    '''Generate a cycle'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [3] + globalsClass.PLOTMODES)]

    values = fedit(datalist, title="Kreis")

    if values is None:
      return
    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.cycle(n))
    self.ui.setVertices()
    self._setChanged()

  def onAnticycle(self):
    '''Generate an anticycle'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [3] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Komplement eines Kreises"))

    if values is None:
      return
    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.anticycle(n))
    self.ui.setVertices()
    self._setChanged()

  def onRandomEdges(self):
    '''Generate an random graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Kantenwahrscheinlichkeit'),
                 self.randomEdgeProb),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zufallsgraph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl angegeben"))
      return

    if len(values[1]) != 0:
      try:
        p = float(values[1])
        if p <= 0 or p > 1.0:
          raise
        self.randomEdgeProb = str(p)
        self.randomEdges = ''
      except:
        fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                           "Achtung"),
              comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Keine g�ltige Wahrscheinlichkeit angegeben"))
        return
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(GraphFunctions.randomGraph(n, p))

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onRandomNumberEdges(self):
    '''Generate an random graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Kanten'),
                 self.randomEdges),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zufallsgraph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl angegeben"))
      return

    if len(values[1]) != 0:
      try:
        m = int(values[1])
        if m <= 0 or m > (n * (n - 1)) / 2:
          raise
        self.randomEdgeProb = ''
        self.randomEdges = str(m)
      except:
        fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                           "Achtung"),
              comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Keine g�ltige Kantenanzahl angegeben"))
        return
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(GraphFunctions.randomGraph(n, m))

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onGeometricRandom(self):
    '''Generate an random graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Adjazensfactor (0,...,1)'),
                 self.randomAdjaFactor),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'L�nge des Quadrats'),
                 self.randomLength),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zufallsgraph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl angegeben"))
      return

    try:
      r = float(values[1])
      if r < 0.0 or r > 1.0:
        raise
      self.randomAdjaFactor = str(r)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Kein g�ltiger Adjazensfactor angegeben"))
      return

    try:
      length = float(values[2])
      if length < 0.0 or length > 10000:
        raise
      self.randomLength = str(length)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige L�nge des Quadrats angegeben"))
      return

    self.ui.graph = SWGraph()
    g, coord = GraphFunctions.geometricRandomGraph(n, r, length)
    self.ui.graph.convertSGraph(g)

    if values[3] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[3]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices(coord)
    self._setChanged()

  def onRandomRegularGraph(self):
    '''Generates a random regular graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Knotengrad'),
                 self.randomEdges),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zuf�lliger d-regul�rer Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      d = int(values[1])
      if n <= 0 or d <= 0 or d >= n or (n * d) % 2 != 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
      self.randomEdges = str(d)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Werte angegeben"))
      return

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.randomRegularGraph(n, d))

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onBarabasiAlbert(self):
    '''Generates a Barabasi-Albert graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Kanten an neuen Knoten'),
                 self.randomEdges),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Barabasi-Albert-Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      m = int(values[1])
      if n <= 0 or m < 1 or m >= n:
        raise
      self.max_node = n
      self.randomVertices = str(n)
      self.randomEdgeProb = ''
      self.randomEdges = str(m)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Werte angegeben (m >= n)"))
      return

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.barabasiAlbertGraph(n, m))

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onPowerlawClusterGraph(self):
    '''Generate a powerlaw-cluster graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Kanten an neuen Knoten'),
                 self.randomEdges),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Wahrscheinlichkeit f�r ein Dreieck'),
                 self.randomEdgeProb),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Powerlaw-cluster-Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl angegeben"))
      return

    try:
      m = int(values[1])
      if m < 1 or m > n:
        raise
      self.randomEdges = str(m)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Kantenanzahl angegeben"))
      return

    try:
      p = float(values[2])
      if p < 0 or p > 1:
        raise
      self.randomEdgeProb = str(p)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Wahrscheinlichkeit angegeben"))
      return

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.powerlawClusterGraph(n, m, p))

    if values[3] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[3]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onLobsterTree(self):
    '''Generate a random lobster tree'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 self.randomVertices),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Kantenwahrscheinlichkeit (Kante an R�ckgrat)'),
                 self.randomEdges),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Kantenwahrscheinlichkeit (Kante ein Level nach R�ckgrat)'),
                 self.randomEdgeProb),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Powerlaw-cluster-Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
      self.randomVertices = str(n)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl angegeben"))
      return

    try:
      p1 = float(values[1])
      p2 = float(values[2])
      if p1 < 0 or p1 > 1 or p2 < 0 or p2 > 1:
        raise
      self.randomEdges = str(p1)
      self.randomEdgeProb = str(p2)
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Wahrscheinlichkeit angegeben"))
      return

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.randomLobster(n, p1, p2))

    if values[3] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[3]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.setVertices()
    self._setChanged()

  def onStar(self):
    '''Generate a stargraph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [3] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Stern"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.star(n))
    self.ui.setVertices()
    self._setChanged()

  def onWheel(self):
    '''Generate wheelgraph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten (>=4)'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Radgraph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 3:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.wheel(n))
    self.ui.setVertices()
    self._setChanged()

  def onFan(self):
    '''Generate fangraph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "F�chergraph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.fan(n))
    self.ui.setVertices()
    self._setChanged()

  def onSun(self):
    '''Generate sun graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Sun graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 0:
        raise ValueError
    except ValueError:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.Sun(n))
    self.ui.setVertices()
    self.max_node = self.ui.graph.order()
    self._setChanged()

  def onSunlet(self):
    '''Generate sunlet graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Sunlet graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise ValueError
    except ValueError:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.Sunlet(n))
    self.ui.setVertices()
    self.max_node = self.ui.graph.order()
    self._setChanged()

  def onRisingSun(self):
    '''Generate rising sun graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Rising sun graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.RisingSun(n))
    self.ui.setVertices()
    self.max_node = self.ui.graph.order()
    self._setChanged()

  def onFriendshipGraph(self):
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Fl�gel'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Friendship graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.friendship(n))
    self.ui.setVertices()
    self.max_node = self.ui.graph.order()
    self._setChanged()

  def onBookGraph(self):
    '''Generates a book graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Seiten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Book graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.book(n))
    self.ui.setVertices()
    self.max_node = self.ui.graph.order()
    self._setChanged()

  def onFirecracker(self):
    '''Generate a firecracker graph'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Sterne'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Gr��e der Sterne'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Firecracker"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      k = int(values[1])
      if n <= 0 or k <= 1:
        raise
      self.max_node = n * k
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.Firecracker(n, k))
    self.ui.graph.data = [n, k]
    self.ui.setVertices()
    self._setChanged()

  def onBananaTree(self):
    '''Generate a banana tree'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Sterne'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Gr��e der Sterne'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Banana tree"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      k = int(values[1])
      if n <= 0 or k <= 1:
        raise
      self.max_node = n * k + 1
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.BananaTree(n, k))
    self.ui.graph.data = [n, k]
    self.ui.setVertices()
    self._setChanged()

  def onCentipede(self):
    '''Generate a centipede'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'L�nge des Tausendf��er'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Tausendf��er"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = 2 * n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.Centipede(n))
    self.ui.setVertices()
    self._setChanged()

  def onRandomTree(self):
    '''Generates a random tree'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zuf�lliger Baum"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.randomTree(n))
    self.ui.setVertices()
    self._setChanged()

  def onRandomTreePruefercode(self):
    '''Generates a random tree'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [1] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Zuf�lliger Baum"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      if n <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    tree, code = GraphFunctions.randomTreePrueferCode(n)
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(tree)
    self.ui.setVertices()
    self._setChanged()

    # Show the result dialog
    self.solutiondialog.result = code
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Pr�fercode des zuf�lligen Baums"))

    self.solutiondialog.show()

  def onBipartiteGraph(self):
    '''Generate a complete bipartite graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Knoten in der ersten Knotenmenge'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Knoten in der zweiten Knotenmenge'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Bipartiter Graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      m = int(values[1])
      self.max_node = n + m
      if n <= 0 or m <= 0:
        raise
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl eingegeben"))
      return

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.completeBipartite(n, m))
    self.ui.setVertices()
    self._setChanged()

  def onkPartiteGraph(self):
    '''Generate a complete k-partite graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Gr��e der Knotenmengen (getrennt durch Komma)'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [0] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "k-partiter Graph"))
    if values is None:
      return

    p = []
    try:
      s = values[0].split(",")
      for v in s:
        try:
          w = int(v)
          p.append(w)
        except:
          pass
      for i in p:
        if i <= 0:
          raise
    except:
      fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahlen eingegeben"))
      return

    if values[1] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[1]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.completekPartite(p))
    self.max_node = max(self.ui.graph.getV())
    self.ui.setVertices()
    self._setChanged()

  def onProduct(self):
    '''Opens the product dialog'''
    self.productDialog.show()

  def onProduktGraph(self):
    '''Generate a productgraph'''
    if not self.overwrite():
      return

    # Current Values choosen in the dialog
    # [product, graph 1, Number of vertices, graph 2, number of vertices, drawing mode]
    values = self.productDialog.getValues()

    if values is None:
      return

    g1 = str(values[1])
    g2 = str(values[3])
    try:
      if len(values[2]) != 0:
        n = int(values[2])
      else:
        n = 0
      if len(values[4]) != 0:
        m = int(values[4])
      else:
        m = 0
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl eingegeben"))
      return

    if g1 == 'Weg':
      graph1 = GraphFunctions.path(n)
      globalsClass.FIRSTORDER = n
    elif g1 == 'Kreis':
      graph1 = GraphFunctions.cycle(n)
      globalsClass.FIRSTORDER = n
    elif g1 == 'Vollst�ndiger Graph':
      graph1 = GraphFunctions.completeGraph(n)
      globalsClass.FIRSTORDER = n
    elif g1 == 'aktueller Graph':
      graph1 = self.ui.graph.copy()
      globalsClass.FIRSTORDER = self.ui.graph.order()
    else:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment="Irgendwas stimmt da net")
      return
    self.productOne = graph1.copy()

    if g2 == 'Weg':
      graph2 = GraphFunctions.path(m)
      globalsClass.SECONDORDER = m
    elif g2 == 'Kreis':
      graph2 = GraphFunctions.cycle(m)
      globalsClass.SECONDORDER = m
    elif g2 == 'Vollst�ndiger Graph':
      graph2 = GraphFunctions.completeGraph(m)
      globalsClass.SECONDORDER = m
    elif g2 == 'aktueller Graph':
      graph2 = self.ui.graph.copy()
      globalsClass.SECONDORDER = self.ui.graph.order()
    else:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment="Irgendwas stimmt da net")
      return
    self.productTwo = graph2.copy()

    # Set the default plot mode
    globalsClass.PLOTSTYLE = 'grid'

    product = str(values[0])
    if product == 'Kartesisches Produkt':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.product(graph2))
    elif product == 'Tensorprodukt':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.tensorProduct(graph2))
    elif product == 'Lexicographisches Produkt':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.lexicographicalProduct(graph2))
    elif product == 'AND Produkt':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.strongProduct(graph2))
    elif product == 'Join':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.join(graph2))
    elif product == 'Corona':
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph1.corona(graph2))
      globalsClass.PLOTSTYLE = 'neato'

    # Set the current plot mode in the box
    self.ui.comboBoxDraw.setCurrentIndex(globalsClass.PLOTMODES.index(globalsClass.PLOTSTYLE))

    self.max_node = self.ui.graph.order()
    self.ui.setVertices()
    self.changed = True
    self.operations.clear()

  def onkPath(self):
    '''Generate a simple k-path'''
    if not self.onNew():
      return

    # dialog for the parameter
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'),
                 ''), ('k', ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "einfacher k-Weg"))
    if values is None:
      return

    # check the given parameter
    try:
      n = int(values[0])
      k = int(values[1])
      if n < k or k <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    # Set the plotmode
    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    # generate the graph
    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.kPath(n, k))
    self.ui.setVertices()
    self._setChanged()

  def onRandomkPath(self):
    '''Generate a random k-path'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                ('k', ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "zuf�lliger k-Weg"))
    if values is None:
      return

    try:
      n = int(values[0])
      k = int(values[1])
      if n < k or k <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.randomkPath(n, k))
    self.ui.setVertices()
    self._setChanged()

  def onkCycle(self):
    '''Generate a k-cycle'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                ('k', ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "k-Kreis"))
    if values is None:
      return

    try:
      n = int(values[0])
      k = int(values[1])
      if n <= k or k <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.kCycle(n, k))
    self.ui.setVertices()
    self._setChanged()

  def onkStar(self):
    '''Generate a k-star'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Anzahl der Knoten'), ''),
                ('k', ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "k-Stern"))
    if values is None:
      return

    try:
      n = int(values[0])
      k = int(values[1])
      if n < k or k <= 0:
        raise
      self.max_node = n
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.kStar(n, k))
    self.ui.setVertices()
    self._setChanged()

  def onSplitGraph(self):
    '''Generate split graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Size of the clique'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Size of the independent set'), ''),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Splitgraph"))
    if values is None:
      return

    try:
      k = int(values[0])
      l = int(values[1])
      if l <= 0 or k <= 0:
        raise
      self.max_node = k + l
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    if values[2] in PLOTMODES_GRAPHVIZ:
      globalsClass.PLOTSTYLE = values[2]
      self.ui.comboBoxDraw.setCurrentIndex(PLOTMODES_GRAPHVIZ.index(globalsClass.PLOTSTYLE))

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.splitGraph(k, l))
    self.ui.setVertices()
    self._setChanged()

  def onQueensGraph(self):
    '''Generate a queens graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Size of the queens graph'),
                 '')]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Queens graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n ** 2
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    globalsClass.PLOTSTYLE = 'grid'

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.queensGraph(n))
    self.ui.setVertices()
    self._setChanged()

  def onRookGraph(self):
    '''Generates the nxn rooks graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Size of the rooks graph'),
                 '')]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Rooks graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n ** 2
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    globalsClass.PLOTSTYLE = 'grid'

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.rooksGraph(n))
    self.ui.setVertices()
    self._setChanged()

  def onBishopGraph(self):
    '''Generates the nxn bishops graph'''
    if not self.onNew():
      return

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Size of the bishops graph'),
                 '')]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Bishops graph"))
    if values is None:
      return

    try:
      n = int(values[0])
      if n <= 1:
        raise
      self.max_node = n ** 2
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Zahl eingegeben"))
      return

    globalsClass.PLOTSTYLE = 'grid'

    self.ui.graph = SWGraph()
    self.ui.graph.convertSGraph(GraphFunctions.bishopsGraph(n))
    self.ui.setVertices()
    self._setChanged()

  def onGenFromGraph6(self):
    '''Generates a graph from the graph6-string'''
    if not self.onNew():
      return

    datalist = [('graph6', ''),
                (QtWidgets.QApplication.translate("PyGraphEdit", 'Zeichenstil'),
                 [2] + globalsClass.PLOTMODES)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Graph6"))
    if values is None:
      return

    graph = parse_graph6(values[0])

    if graph is not False:
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph)
      self.ui.setVertices()
      self._setChanged()
    else:
      values = fedit(None,
                     title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                            "Achtung"),
                     comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                              "Keine g�ltiger graph6-String"))

  def onGenDataGraph6(self):
    '''Generates a database from a file with graph6-strings'''
    self.graph6toDB.show()

  def __genDataGraph6(self):
    '''Generates a database from a file with graph6-strings.
    Called after the ok-signal from graph6toDB-Dialog'''
    numGraphs = graph6FiletoDB(self.graph6toDB.graph6File, self.graph6toDB.DBfile)
    com = "Saved %d graphs." % numGraphs
    fedit(None, title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Done"),
          comment=com)

  def overwrite(self):
    '''Overwrite the current graph?'''
    if self.ui.graph is None:
      return True
    values = True
    if self.ui.graph.order() != 0 and self.changed:
      com = """Wollen Sie den aktuellen Graph wirklich �berschreiben?

      Alle nicht gespeicherten �nderungen gehen dabei verloren!"""
      values = fedit("yesorno", title="Info", comment=com)
    return values

  def onUndo(self):
    '''Undo the last operation'''
    try:
      value, item = self.operations.pop()
    except:
      return

    if value == -1:
      self.ui.graphicsView.scene().restoreItem(item)
    else:
      self.ui.graphicsView.scene().delItem(item)
    self._setChanged()

  def onComplement(self):
    '''Generate the complement of the graph'''
    if self.overwrite():
      self.operations.clear()
      graph = GraphFunctions.complement(self.ui.graph)
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph)
      self.ui.setVertices()
      self._setChanged()

  def onLineGraph(self):
    '''Generate the linegraph'''
    if self.overwrite():
      self.operations.clear()
      graph = GraphFunctions.lineGraph(self.ui.graph)
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph)
      self.ui.setVertices()
      self._setChanged()
      self.max_node = self.ui.graph.order()

  def onKlineGraph(self):
    '''Generate the k-linegraph'''
    if self.overwrite():
      self.operations.clear()
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Cliquengr��e k'), '')]

      values = fedit(datalist,
                     title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                            "k-Kantengraph"))
      if values is None:
        return

      try:
        k = int(values[0])
      except:
        fedit(None,
              title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Achtung"),
              comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Keine g�ltige Zahl eingegeben"))
        return

      graph = GraphFunctions.kLineGraph(self.ui.graph, k)
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph)
      self.ui.setVertices()
      self._setChanged()
      self.max_node = self.ui.graph.order()

  def onInducedSubgraph(self):
    '''Generate an induced subgraph'''
    if self.overwrite():
      if len(self.ui.graphicsView.scene().tmp_item) == 0:
        datalist = [('Knotenliste', '')]
        values = fedit(datalist,
                       title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                              "Induzierter Untergraph"))
        if values[0] is None or values[0] is False:
          fedit(None,
                title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Achtung"),
                comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Keine Knoten markiert oder angegeben"))
          return
        tmp_nodes = [int(v) for v in values[0].split(',')]

      X = set([])
      if len(self.ui.graphicsView.scene().tmp_item) > 0:
        for item in self.ui.graphicsView.scene().tmp_item:
          X.add(int(item.value))
      else:
        for item in tmp_nodes:
          X.add(item)

      graph = GraphFunctions.inducedSubgraph(self.ui.graph, X)
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(graph)
      self.ui.setVertices()
      self._setChanged()
      self.max_node = self.ui.graph.order()
      self.operations.clear()

  def onPathDecomposition(self):
    '''Generate a path decomposition'''
    decomp = get_path_decomposition(self.ui.graph,
                                    min(self.ui.graph.getV()),
                                    max(self.ui.graph.getV()))
    self.solutiondialog.textBrowser.setText(
        str(decomp) + "\n\nWeite der Zerlegung: " +
        str(get_width(decomp)))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Wegzerlegung des Graphen"))
    self.solutiondialog.show()

  def onTreeDecomposition(self):
    '''Generate a tree decomposition'''
    if self.overwrite():
      self.operations.clear()
      tmp_graph = self.ui.graph.copy()
      self.ui.graph = SWGraph()
      self.ui.graph.convertSGraph(get_tree_decomposition(tmp_graph))
      self.ui.setVertices()
      self.max_node = self.ui.graph.order()
      self._setChanged()

  def onChanceVertexWeights(self):
    '''Change the vertex weights'''
    randvalue = []
    marked_nodes = []
    marked_nodes_str = ""
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_nodes.append(item.value)
      for i, v in enumerate(marked_nodes):
        if i != 0:
          marked_nodes_str += "; "
        marked_nodes_str += str(v)

    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Zu �ndernde Knoten'),
                 '%s' % marked_nodes_str),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'Knotengewicht'),
                 '%f' % Vertex.defaultVertexWeight)]

    values = fedit(datalist,
                   title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                          "Knotengewichte �ndern"))
    if values is None:
      return

    try:
      A_str = values[0]
      A = []
      if len(A_str) == 0:
        A = list(self.ui.graph.getV())
      else:
        s = A_str.split(";")
        for v in s:
          try:
            w = int(v)
            A.append(w)
          except:
            pass
      try:
        weight = float(values[1])
      except:
        w = str(values[1])
        limit = w.split("-")
        if len(limit) != 2:
          raise
        randvalue = [float(limit[0]), float(limit[1])]
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Angaben"))
      return

    for v in A:
      if len(randvalue) != 0:
        weight = uniform(randvalue[0], randvalue[1])
      self.ui.graph.setWeight(v, weight)
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1 and item.value == v:
          item.weight = weight

  def onChanceEdgeWeights(self):
    '''Change the edge weights'''
    # If some edges marked, then use them
    marked_edges = []
    marked_edges_str = ""
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 2:
          if item.marked:
            marked_edges.append((int(item.source.value), int(item.dest.value)))
      for i, v in enumerate(marked_edges):
        if i != 0:
          marked_edges_str += ";"
        marked_edges_str += str(v)

        self.changeEdgeWeightsDialog.lineEditChangeEdges.setText(marked_edges_str)

    # Show the dialog
    self.changeEdgeWeightsDialog.show()

  def _changeEdgeWeights(self):
    '''Called after the OK-Signal from the changeEdgeWeightsDialog'''
    weight = 0
    randvalue = []

    # Get the list of the edges to change the weight
    if self.changeEdgeWeightsDialog.allEdges:
      A = list(self.ui.graph.getEdgeSet())
    else:
      A_str = str(self.changeEdgeWeightsDialog.edges)
      A = []
      if len(A_str) == 0:
        A = list(self.ui.graph.getEdgeSet())
      else:
        s = A_str.split(";")
        for v in s:
          try:
            v.strip(" ")
            w = v.split(",")
            A.append((int(w[0]), int(w[1])))
          except:
            pass

    # Exponential weights?
    if self.changeEdgeWeightsDialog.exponential:
        x1 = self.changeEdgeWeightsDialog.expx1
        y1 = self.changeEdgeWeightsDialog.expValue1
        x2 = self.changeEdgeWeightsDialog.expx2
        y2 = self.changeEdgeWeightsDialog.expValue2

        # Calculate the parameter of the function
        a = math.log(y1 / y2) / (x2 - x1)
        b = y2 / math.exp(-a * x2)

    # Linear weights?
    elif self.changeEdgeWeightsDialog.linear:
        x1 = self.changeEdgeWeightsDialog.expx1
        y1 = self.changeEdgeWeightsDialog.expValue1
        x2 = self.changeEdgeWeightsDialog.expx2
        y2 = self.changeEdgeWeightsDialog.expValue2

        # Calculate the parameter of the function
        a = (y2 - y1) / (x2 - x1)
        b = y1 - a * x1

    # Use fixed weight or random weight in an interval
    if (not self.changeEdgeWeightsDialog.euclidian
        and not self.changeEdgeWeightsDialog.exponential
        and not self.changeEdgeWeightsDialog.linear):
      try:
        weight = float(self.changeEdgeWeightsDialog.weight)
      except:
        w = str(self.changeEdgeWeightsDialog.weight)
        limit = w.split("-")
        if len(limit) != 2:
          fedit(None,
                title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Achtung"),
                comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                         "Ung�litge Eingaben"))
          return
        randvalue = [float(limit[0]), float(limit[1])]

    # For every edge
    for v in A:
      if len(randvalue) != 0:  # Use random weights in a given intervall
        weight = uniform(randvalue[0], randvalue[1])
      elif (self.changeEdgeWeightsDialog.euclidian
            or self.changeEdgeWeightsDialog.exponential
            or self.changeEdgeWeightsDialog.linear):  # Calculate the euclidian distance
        for item in self.ui.graphicsView.scene().items():
          if item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(v[0]):
            x1 = item.pos().x()
            y1 = item.pos().y()
          elif item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(v[1]):
            x2 = item.pos().x()
            y2 = item.pos().y()
        distance = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

      if self.changeEdgeWeightsDialog.euclidian:  # Use the euclidian distance
        weight = distance
      elif self.changeEdgeWeightsDialog.exponential:
        weight = b * math.exp(-a * distance)
      elif self.changeEdgeWeightsDialog.linear:
        weight = a * distance + b

      self.ui.graph.setWeight(v, weight)
      for item in self.ui.graphicsView.scene().items():
        if (item.type() == globalsClass.USERTYPE + 2
            and ((int(item.dest.value) == v[0]
                  and int(item.source.value) == v[1])
                 or (int(item.dest.value) == v[1]
                     and int(item.source.value) == v[0]))):
          item.weight = weight

  def onGraphInfo(self):
    '''Show the info dialog'''
    if self.ui.graph.order() > 0:
      # Insert the current informations
      self.infodialog.lineEditNumV.setText(str(self.ui.graph.order()))
      self.infodialog.lineEditNumE.setText(str(self.ui.graph.size()))
      self.infodialog.lineEditDiameter.setText(str(GraphFunctions.diameter(self.ui.graph)))
      self.infodialog.lineEditRadius.setText(str(GraphFunctions.radius(self.ui.graph)))
      self.infodialog.lineEditMinDeg.setText(str(self.ui.graph.get_min_degree()))
      self.infodialog.lineEditMaxDeg.setText(str(self.ui.graph.get_max_degree()))
      self.infodialog.lineEditKomponenten.setText(str(len(GraphFunctions.components(self.ui.graph))))
      self.infodialog.lineEditKnotenabstand.setText("%.3f" % GraphFunctions.averageDistance(self.ui.graph))
      self.infodialog.lineEditDensity.setText("%.3f" % GraphFunctions.graphdensity(self.ui.graph))
      self.infodialog.lineEditTriangles.setText(str(GraphFunctions.countTriangles(self.ui.graph)))
      self.infodialog.lineEditGraph6.setText(graphtograph6(self.ui.graph))

      # Show the dialog
      self.infodialog.show()

  def onGraphClasses(self):
    '''Shows the graphclasses-dialog'''
    if self.ui.graph.order() > 0:
      self.classInfos.checkBoxComplete.setChecked(GraphFunctions.isComplete(self.ui.graph))
      self.classInfos.checkBoxBipartite.setChecked(GraphFunctions.isBipartite(self.ui.graph))
      self.classInfos.checkBoxCompleteBipartit.setChecked(GraphFunctions.isCompleteBipartite(self.ui.graph))
      self.classInfos.checkBoxTree.setChecked(GraphFunctions.isTree(self.ui.graph))
      self.classInfos.checkBoxPath.setChecked(GraphFunctions.isPath(self.ui.graph))
      if not GraphFunctions.iskTree(self.ui.graph)[1]:
        self.classInfos.checkBoxkTree.setChecked(False)
      else:
        self.classInfos.checkBoxkTree.setChecked(True)
      self.classInfos.checkBoxkPath.setChecked(GraphFunctions.iskPath(self.ui.graph))
      if GraphFunctions.isskPath(self.ui.graph) == 0:
        self.classInfos.checkBoxSimplekPath.setChecked(False)
      else:
        self.classInfos.checkBoxSimplekPath.setChecked(True)

      self.classInfos.checkBoxRegGraph.setChecked(GraphFunctions.isRegular(self.ui.graph))
      self.classInfos.checkBoxPlanar.setChecked(GraphFunctions.isPlanar(self.ui.graph))
      self.classInfos.checkBoxEulersch.setChecked(GraphFunctions.isEulersch(self.ui.graph))
      self.classInfos.checkBoxChordal.setChecked(GraphFunctions.isCordal(self.ui.graph))
      self.classInfos.checkBoxSelfcentral.setChecked(GraphFunctions.isSelfcentral(self.ui.graph))

      self.classInfos.show()

  def onCallSimpleFunction(self, fkt, name):
    '''
    Function to call a simple calculation function without a method.
    The called function get only the current graph.
    @param fkt: Function-object to call
    @param name: Name of the calculated object
    '''
    self.calc_value = name
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    self.thread = WorkThread(target=fkt,
                             args=(self.ui.graph,))
    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onCallFunctionMethod(self, fkt, name, methodName):
    '''
    Function to call a simple calculation function with a method.
    The called function get the current graph and the choosen method.
    @param fkt: Function-object to call
    @param name: Name of the calculated object
    '''
    self.calc_value = name
    methods = [0]
    methods.extend([m.name for m in globalsClass.METHODSFORMODULES[methodName]])
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 methods)]

    values = fedit(datalist, title=name)
    if values is None:
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    if method[values[0]] == method.AUTOMATIC:
      self.thread = WorkThread(target=fkt,
                               args=(PyGraphEdit_log, self.ui.graph, None))
    else:
      self.thread = WorkThread(target=fkt,
                               args=(PyGraphEdit_log, self.ui.graph,
                                     method[values[0]]))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onCallFunctionMethodProduct(self, fkt, name, methodName):
    '''
    Function to call a simple calculation function with a method of a product graph.
    The called function get the current graph, the choosen method
    and the product informations.
    @param fkt: Function-object to call
    @param name: Name of the calculated object
    '''
    self.calc_value = name
    methods = [0]
    methods.extend([m.name for m in globalsClass.METHODSFORMODULES[methodName]])
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 methods)]

    values = fedit(datalist, title=name)
    if values is None:
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    if method[values[0]] == method.AUTOMATIC.name:
      self.thread = WorkThread(target=fkt,
                               args=(PyGraphEdit_log, self.ui.graph, None,
                                     self.productOne, self.productTwo))
    else:
      self.thread = WorkThread(target=fkt,
                               args=(PyGraphEdit_log, self.ui.graph,
                                     method[values[0]],
                                     self.productOne, self.productTwo))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onShowCenter(self):
    '''Select the vertices in the center of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.onEntmark()
    c = list(GraphFunctions.center(self.ui.graph))
    for v in c:
      self.ui.graphicsView.scene().markVertex(v)

  def onShowPeripheral(self):
    '''Select the vertices on the peripheral of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.onEntmark()
    per = list(GraphFunctions.peripheral(self.ui.graph))
    for v in per:
      self.ui.graphicsView.scene().markVertex(v)

  def onShowMedian(self):
    '''Select the median of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.onEntmark()
    m = list(GraphFunctions.median(self.ui.graph))
    for v in m:
      self.ui.graphicsView.scene().markVertex(v)

  def onMaxMatching(self):
    '''Calculates a maximum matching of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Maximales Matching im Graphen:")
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    matchingedges, matchingsize = computeMaxMatching(self.ui.graph, {})

    # Mark the matching edges
    for e in matchingedges:
      self.ui.graphicsView.scene().markEdge(e[0], e[1])

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    # Show the result dialog
    self.solutiondialog.result = matchingedges
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nSize of the maximum matching: " + str(matchingsize))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Maximum Matching"))

    self.solutiondialog.show()

  def onMinCut(self):
    '''Calculate the min-cut with the algorithm of Stoer and Wagner'''
    if self.ui.graph.order() == 0:
      return
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit", "Min-cut:")
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    mincutvalue, mincut = GraphFunctions.minimumCut(self.ui.graph)

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
      self.time_stamps = []

    # Mark the cut-edges
    self.ui.graphicsView.scene().unmarkall()
    if mincut is not None:
      for e in mincut:
        self.ui.graphicsView.scene().markEdge(e[0], e[1])

    # Show the result dialog
    self.solutiondialog.result = mincut
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nSize of the min-cut: " + str(mincutvalue))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Min-cut"))

    self.solutiondialog.show()

  def onFordFulkerson(self):
    '''Calculate the min-cut with the algorithm of Ford and Fulkerson'''
    if self.ui.graph.order() == 0:
      return
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Min-cut:")
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    mincutvalue = float('inf')
    for v in self.ui.graph.getVIt():
      for u in self.ui.graph.getVIt():
        if v > u:
          flowvalue = GraphFunctions.maxFlow(self.ui.graph, v, u)
          if flowvalue < mincutvalue:
            mincutvalue = flowvalue

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
      self.time_stamps = []

    # Show the result dialog
    self.solutiondialog.result = ""
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nSize of the min-cut: " + str(mincutvalue))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Min-cut"))

    self.solutiondialog.show()

  def onMinstCut(self):
    '''Calculate the value of an mimimum st-cut with the algortihm of Ford-Fulkerson'''
    if self.ui.graph.order() == 0:
      return
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Min-st-cut:")

    marked_vertices = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))

    if len(marked_vertices) != 2:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte zwei Knoten f�r die Berechnung ausw�hlen!"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    marked_vertices.sort()
    mincutvalue = GraphFunctions.maxFlow(self.ui.graph, marked_vertices[0],
                                         marked_vertices[1])

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
      self.time_stamps = []

    # Show the result dialog
    self.solutiondialog.result = ""
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nSize of the min-st-cut: " + str(mincutvalue))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Min-st-cut"))

    self.solutiondialog.show()

  def onMinEdgeCover(self):
    '''Calculates a maximum matching of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Minimum edge cover im Graphen:")
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    edgecover, edgecoversize = GraphFunctions.calculateMinEdgeCover(self.ui.graph,)

    # Mark the cover-edges
    for e in edgecover:
      self.ui.graphicsView.scene().markEdge(e[0], e[1])

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    # Show the result dialog
    self.solutiondialog.result = edgecover
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nSize of the minimum edge cover: " + str(edgecoversize))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Minimum edge cover"))

    self.solutiondialog.show()

  def onOpenNeighbors(self):
    '''Mark the open neighborhood of the marked vertices'''
    if self.ui.graph.order() == 0:
      return
    marked_vertices = set([])
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.add(int(item.value))

    if len(marked_vertices) == 0:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte Knoten f�r Berechnung der Nachbarschaft ausw�hlen!"))
      return

    self.onEntmark()
    m = list(self.ui.graph.open_neighbors_set(marked_vertices))
    for v in m:
      self.ui.graphicsView.scene().markVertex(v)
    self.tag = True

  def onTotalOpenNeighbors(self):
    '''Mark the total open neighborhood of the marked vertices'''
    if self.ui.graph.order() == 0:
      return
    marked_vertices = set([])
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.add(int(item.value))

    if len(marked_vertices) == 0:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte Knoten f�r Berechnung der Nachbarschaft ausw�hlen!"))
      return

    self.onEntmark()
    m = list(self.ui.graph.open_total_neighbors_set(marked_vertices))
    for v in m:
      self.ui.graphicsView.scene().markVertex(v)
    self.tag = True

  def onClosedNeighbors(self):
    '''Mark the closed neighborhood of the selected vertices'''
    if self.ui.graph.order() == 0:
      return
    # Get the current selected vertices
    marked_vertices = set([])
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.add(int(item.value))

    # Are there any vertices selected?
    if len(marked_vertices) == 0:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte Knoten f�r Berechnung der Nachbarschaft ausw�hlen!"))
      return

    # Mark the vertices
    self.onEntmark()
    m = list(self.ui.graph.neighbors_set(marked_vertices))
    for v in m:
      if v in marked_vertices:
        self.ui.graphicsView.scene().markVertex(v, color="cyan")
      else:
        self.ui.graphicsView.scene().markVertex(v)
    self.tag = True

  def onArticulations(self):
    '''Shows the articulations of the graph'''
    if self.ui.graph.order() == 0:
      return
    self.onEntmark()
    m = list(GraphFunctions.find_artikulations(self.ui.graph))
    for v in m:
      self.ui.graphicsView.scene().markVertex(v)
    self.tag = True

    m.sort()
    self.solutiondialog.result = m
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nNumber of articulations: " + str(len(m)))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Artikulationen des Graphen"))

    self.solutiondialog.show()

  def onShortestPath(self):
    '''Calculates the shortest path between two vertices with the Dijkstra-algorithm'''
    if self.ui.graph.order() == 0:
      return
    marked_vertices = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))

    if len(marked_vertices) != 2:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte zwei Knoten f�r die Berechnung des k�rzesten Weges ausw�hlen!"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    marked_vertices.sort()
    path, length = GraphFunctions.shortestPath(self.ui.graph,
                                               marked_vertices[0],
                                               marked_vertices[1])

    # Mark the shortest path
    for i in range(0, len(path) - 1):
      self.ui.graphicsView.scene().markVertex(path[i])
      self.ui.graphicsView.scene().markEdge(path[i], path[i + 1])

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    # Show the result dialog
    self.solutiondialog.result = path
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nLength of the shortest path: " + str(length))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "K�rzester Weg (Dijkstra)"))

    self.solutiondialog.show()

  def onShortestPathAStar(self):
    '''Calculates the shortest path between two vertices with the A*-algorithm'''
    if self.ui.graph.order() == 0:
      return
    marked_vertices = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))

    if len(marked_vertices) != 2:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte zwei Knoten f�r die Berechnung des k�rzesten Weges ausw�hlen!"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    marked_vertices.sort()
    coord = self.ui.getCoords()  # Get the coordinates of the vertices
    path, length = GraphFunctions.shortestPathAStar(self.ui.graph,
                                                    marked_vertices[0],
                                                    marked_vertices[1], coord)

    # Generate the shortest path as a list of vertices and mark the solution
    currentVertex = marked_vertices[1]
    new_path = [marked_vertices[1]]
    while currentVertex != marked_vertices[0]:
      currentVertex = path[currentVertex]
      new_path.insert(0, currentVertex)
      self.ui.graphicsView.scene().markVertex(currentVertex)
      self.ui.graphicsView.scene().markEdge(new_path[0], new_path[1])

    # get the calculation time
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    # Show the result dialog
    self.solutiondialog.result = new_path
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result) + "\nLength of the shortest path: " + str(length[marked_vertices[1]]))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "K�rzester Weg (A*)"))

    self.solutiondialog.show()

  def onNumberofshortestpaths(self):
    '''Counts the number of shortest paths'''
    marked_vertices = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))

    if len(marked_vertices) != 2:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte zwei Knoten f�r die Berechnung des k�rzesten Weges ausw�hlen!"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    marked_vertices.sort()
    numberofpaths = GraphFunctions.countShortestPaths(self.ui.graph,
                                                      marked_vertices[0],
                                                      marked_vertices[1])

    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    self.solutiondialog.result = numberofpaths
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Anzahl der k�rzesten Wege"))

    self.solutiondialog.show()

  def onNumberofEdgeDisjointPaths(self):
    '''Counts the number of edge disjoint paths'''
    marked_vertices = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))

    if len(marked_vertices) != 2:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte zwei Knoten f�r die Berechnung der Anzahl ausw�hlen!"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    marked_vertices.sort()
    numberofpaths = GraphFunctions.localEdgeConnectivity(self.ui.graph,
                                                         marked_vertices[0],
                                                         marked_vertices[1])

    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    self.solutiondialog.result = numberofpaths
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result))
    self.solutiondialog.label.setText(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                       "Anzahl der kantendisjunkten Wege"))

    self.solutiondialog.show()

  def onEDRelInfo(self):
    '''Edge failture domination reliablity anzeigen'''
    self.calc_value = "EDRel-k:"
    methods = [0]
    methods.extend([m.name for m in globalsClass.METHODSFORMODULES["lib.DominationReliabilityEDRel"]])
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 methods),
                (QtWidgets.QApplication.translate("PyGraphEdit",
                                                  'max. Gr��e der dom. Menge (k)'),
                 '2')]

    values = fedit(datalist, title="EDRel")
    if values is None:
      return
    try:
      k = int(values[1])
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Kein g�ltiges k angegeben"))
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    self.thread = WorkThread(target=compute_reliability_edrel,
                             args=(PyGraphEdit_log, self.ui.graph,
                                   values[0],
                                   None, None, k))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onEDRelAInfo(self):
    '''EDRel(G,A,d,p)'''
    marked_nodes = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_nodes.append(item.value)
      marked_nodes_str = ""
      for i, v in enumerate(marked_nodes):
        if i != 0:
          marked_nodes_str += "; "
        marked_nodes_str += str(v)

    if len(marked_nodes) != 0:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'),
                   '%s' % marked_nodes_str, 'dominierende Knoten mit ; getrennt angeben'),
                  (QtWidgets.QApplication.translate("PyGraphEdit", 'Distanz'),
                   '1', 'maximale Dominationsdistanz')]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'),
                   '', 'dominierende Knoten mit ; getrennt angeben'),
                  (QtWidgets.QApplication.translate("PyGraphEdit", 'Distanz'),
                   '1', 'maximale Dominationsdistanz')]

    values = fedit(datalist, title="EDRel")
    if values is None:
      return

    try:
      A_str = values[1]
      A = []
      if len(A_str) == 0:
        A = list(self.ui.graph.getV())
      else:
        s = A_str.split(";")
        for v in s:
          try:
            w = int(v)
            A.append(w)
          except:
            pass
      d = int(values[2])
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Angaben"))
      return

    self.calc_value = "EDRel(G, %s, %d, p):" % (A, d)
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    self.thread = WorkThread(target=compute_reliability_edrel,
                             args=(PyGraphEdit_log, self.ui.graph,
                                   values[0], A, d, None))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onPEDRelInfo(self):
    '''Perfect edge failture domination reliablity anzeigen'''
    marked_nodes = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_nodes.append(item.value)
      marked_nodes_str = ""
      for i, v in enumerate(marked_nodes):
        if i != 0:
          marked_nodes_str += "; "
        marked_nodes_str += str(v)
    if len(marked_nodes) != 0:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'),
                   '%s' % marked_nodes_str),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Distanz'),
                   '1'),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'k (Dominationsh�ufigkeit)'),
                   '1')]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'),
                   ''),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Distanz'),
                   '1'),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'k (Dominationsh�ufigkeit)'),
                   '1')]

    values = fedit(datalist, title="PEDRel")
    if values is None:
      return

    try:
      A_str = values[1]
      A = []
      if len(A_str) == 0:
        A = list(self.ui.graph.getV())
      else:
        s = A_str.split(";")
        for v in s:
          try:
            w = int(v)
            A.append(w)
          except:
            pass
      d = int(values[2])
      k = int(values[3])
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Angaben"))
      return

    self.calc_value = "PEDRel (d=%d, k=%d):" % (d, k)
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    self.thread = WorkThread(target=compute_reliability_pedrel,
                             args=(PyGraphEdit_log,
                                   self.ui.graph, A, values[0], d, k))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onSPEDRelInfo(self):
    '''Simple perfect edge failture domination reliablity anzeigen'''
    marked_nodes = []
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_nodes.append(item.value)
      marked_nodes_str = ""
      for i, v in enumerate(marked_nodes):
        if i != 0:
          marked_nodes_str += "; "
        marked_nodes_str += str(v)
    if len(marked_nodes) != 0:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'),
                   '%s' % marked_nodes_str),
                  (QtWidgets.QApplication.translate("PyGraphEdit", 'Distanz'), '1'),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'k (Dominationsh�ufigkeit)'), '1')]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Methode'),
                   [0, method.STATE_SPACE.name, method.DECOMP.name]),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'Dominierende Knoten'), ''),
                  (QtWidgets.QApplication.translate("PyGraphEdit", 'Distanz'), '1'),
                  (QtWidgets.QApplication.translate("PyGraphEdit",
                                                    'k (Dominationsh�ufigkeit)'), '1')]

    values = fedit(datalist, title="SPEDRel")
    if values is None:
      return

    try:
      A_str = values[1]
      A = []
      if len(A_str) == 0:
        A = list(self.ui.graph.getV())
      else:
        s = A_str.split(";")
        for v in s:
          try:
            w = int(v)
            A.append(w)
          except:
            pass
      d = int(values[2])
      k = int(values[3])
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltigen Angaben"))
      return

    self.calc_value = "SPEDRel (d=%d, k=%d):" % (d, k)
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    self.thread = WorkThread(target=compute_reliability_spedrel,
                             args=(PyGraphEdit_log, self.ui.graph,
                                   A, values[0], d, k))

    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onTuttePoly(self):
    '''Calculate the Tutte polynomial of the graph'''
    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Tutte polynomial:")
    if system() == "Linux":
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, "tuttepoly", "Barany"])]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, "Barany"])]

    values = fedit(datalist, title="Tutte polynomial")
    if values is None:
      return

    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    self.thread = WorkThread(target=computeTuttePoly,
                             args=(PyGraphEdit_log, self.ui.graph, values[0]))
    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onNumSpanningTrees(self):
    '''Anzahl der Ger�ste des Graphen'''
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 [0, "Kirchhoffs theorem", "Tutte polynomial"])]

    values = fedit(datalist, title="Number of spanning trees")

    if values is None:
      return

    self.calc_value = QtWidgets.QApplication.translate("PyGraphEdit",
                                                       "Ger�stanzahl:")
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    self.thread = WorkThread(target=calcNumSpanningTree,
                             args=(self.ui.graph, values[0]))
    self.thread.mainThread.connect(self.mainThread)
    self.wait.show()
    self.thread.start()

  def onMinSpanTree(self):
    '''Calculates a minimal spanning tree with the algorithm from Prim'''
    self.onEntmark()
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    E, weight = GraphFunctions.minimalSpanningTree(self.ui.graph)
    for e in E:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 2:
          if ((int(item.source.value) == e[0] and int(item.dest.value) == e[1]) or
                  (int(item.source.value) == e[1] and int(item.dest.value) == e[0])):
            item.tag()
            break

    self.solutiondialog.result = weight
    self.solutiondialog.textBrowser.setText("Edges: " + str(E) + "\nweight: " + "%.3f" % weight)
    self.solutiondialog.label.setText("Minimal spanning tree")

    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    self.solutiondialog.show()

    self.time_stamps = []

  def onMaxSpanTree(self):
    '''Calculates a minimal spanning tree with the algorithm from Prim'''
    self.onEntmark()
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    E, weight = GraphFunctions.maximalSpanningTree(self.ui.graph)
    for e in E:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 2:
          if ((int(item.source.value) == e[0]
               and int(item.dest.value) == e[1]) or
                  (int(item.source.value) == e[1]
                   and int(item.dest.value) == e[0])):
            item.tag()
            break

    self.solutiondialog.result = weight
    self.solutiondialog.textBrowser.setText("Edges: " + str(E) + "\nweight: " + "%.3f" % weight)
    self.solutiondialog.label.setText("Minimal spanning tree")

    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    self.solutiondialog.show()

    self.time_stamps = []

  def onTSP(self):
    '''Solves the TSP for the current graph'''
    scipopt = config["DEFAULT"].getboolean("use_scipopt", fallback=False)
    if scipopt and self.ui.graph.order() <= 18:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.NEARESTNEIGHBOR.name, method.NEARESTNEIGHBOR.name,
                    method.NEARESTNEIGHBOR3OPT.name,
                    method.NEARESTNEIGHBOR2OPT3OPT.name,
                    method.SCIPOPT.name])]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.NEARESTNEIGHBOR.name, method.NEARESTNEIGHBOR.name,
                    method.NEARESTNEIGHBOR3OPT.name,
                    method.NEARESTNEIGHBOR2OPT3OPT.name])]

    values = fedit(datalist, title="TSP")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    if values[0] == method.SCIPOPT.name:
      try:
        from lib.tsp import solve_tsp  # @UnresolvedImport
      except:
        print("tsp-solver ist nicht installiert")
        print("installieren sie scipopt")
        return

      distances = []
      V = self.ui.graph.getV()
      for i in range(0, len(V)):
        sub = []
        for j in range(0, len(V)):
          if i > j:
            if self.ui.graph.is_adjacent(V[i], V[j]):
              sub.append(self.ui.graph.getWeight((V[i], V[j])))
            else:
              sub.append(1000000)
          elif i == j:
            sub.append(1000000)
          else:
            break
        distances.append(sub)

      solution, objective = solve_tsp(self.ui.graph, distances)

    elif (values[0] == method.NEARESTNEIGHBOR.name or
          values[0] == method.NEARESTNEIGHBOR.name or
          values[0] == method.NEARESTNEIGHBOR3OPT.name or
          values[0] == method.NEARESTNEIGHBOR2OPT3OPT.name):

      from lib.tsp import NearestNeighbor, twoOpt, threeOpt  # @UnresolvedImport
      self.onClear()
      solution, objective = NearestNeighbor(self.ui.graph)

      # Check if a start solution was found
      if len(solution) != 0:
        if values[0] == method.NEARESTNEIGHBOR.name:
          solution, objective = twoOpt(self.ui.graph, solution)
        elif values[0] == method.NEARESTNEIGHBOR3OPT.name:
          solution, objective = threeOpt(self.ui.graph, solution)
        elif values[0] == method.NEARESTNEIGHBOR2OPT3OPT.name:
          solution, objective = twoOpt(self.ui.graph, solution)
          solution, objective = threeOpt(self.ui.graph, solution)

    else:
      fedit(None, title="Info", comment="A crazy thing happend!!!")
      return

    # endtime
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))

    # mark solution edges
    self.ui.graphicsView.scene().unmarkall()
    if len(solution) != 0:
      for i in range(0, len(solution) - 1):
        self.ui.graphicsView.scene().markEdge(solution[i], solution[i + 1])
      self.ui.graphicsView.scene().markEdge(solution[len(solution) - 1], solution[0])

    # open the solution window
    s = "Zielfunktionswert: %.4f\n%s" % (objective, str(solution))
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("TSP")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onVertexColoring(self):
    '''Calculate a proper (vertex) coloring of the graph'''
    scipopt = config["DEFAULT"].getboolean("use_scipopt", fallback=False)
    if scipopt:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.GREEDY.name, method.DSATUR.name, method.SCIPOPT]), ("k", "")]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.GREEDY.name, method.DSATUR.name]), ("k", "")]
 
    values = fedit(datalist, title="Vertex coloring")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    
    # Get the number of available colors (only for ScipOpt)
    if len(values[1]) == 0:
      k = None
    else:
      k = int(values[1])

    # Start the calculation
    solution, k = compute_coloring(PyGraphEdit_log, self.ui.graph,
                                   k, values[0])

    # endtime
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))

    # color the vertices
    if k <= 7:
      self.ui.graphicsView.scene().unmarkall()
      if solution is not None:
        for v in solution:
          self.ui.graphicsView.scene().markVertex(
              v, COLORLIST[solution[v] - 1])

    # open the solution window
    s = "%s\nUsed colors: %d" % (str(solution), k)
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("Vertex Coloring")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onEdgeColoring(self):
    scipopt = config["DEFAULT"].getboolean("use_scipopt", fallback=False)
    if scipopt:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.GREEDY.name, method.DSATUR.name, method.SCIPOPT]), ("k", "")]
    else:
      datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                   [0, method.GREEDY.name, method.DSATUR.name]), ("k", "")]
 
    values = fedit(datalist, title="Edge coloring")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
    
    # Get the number of available colors (only for ScipOpt)
    if len(values[1]) == 0:
      k = None
    else:
      k = int(values[1])

    # Start the calculation
    solution, k = compute_edge_coloring(PyGraphEdit_log,
                                        self.ui.graph, k, values[0])

    # endtime
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))

    # color the edges
    if k <= 8:
      self.ui.graphicsView.scene().unmarkall()
      if solution is not None:
        for e in solution:
          self.ui.graphicsView.scene().markEdge(
              e[0], e[1], COLORLIST[solution[e] - 1])

    # open the solution window
    s = "%s\nUsed %d colors" % (str(solution), k)
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("Edge Coloring")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onRainbowcoloring(self):
    '''Try to rainbow-color the graph'''
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 [0, method.AUTOMATIC.name, method.GREEDY.name])]

    values = fedit(datalist, title="Rainbow coloring")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    if values[0] == method.AUTOMATIC.name:
      solution, k = computeRainbowColoring(PyGraphEdit_log, self.ui.graph)
    else:
      solution, k = computeRainbowColoring(PyGraphEdit_log,
                                           self.ui.graph, values[0])

    # endtime
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))

    # color the edges
    if k <= 8:
      self.ui.graphicsView.scene().unmarkall()
      if solution is not None:
        for e in solution:
          self.ui.graphicsView.scene().markEdge(
              e[0], e[1], COLORLIST[solution[e] - 1])

    # open the solution window
    s = "%s\nUsed %d colors" % (str(solution), k)
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("Rainbow Coloring")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onRainbowVertexColoring(self):
    '''Try to rainbow-color the graph'''
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 [0, method.AUTOMATIC.name])]

    values = fedit(datalist, title="Rainbow coloring")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    if values[0] == method.AUTOMATIC.name:
      solution, k = computeRainbowVertexColoring(PyGraphEdit_log,
                                                 self.ui.graph)
    else:
      solution, k = computeRainbowVertexColoring(PyGraphEdit_log,
                                                 self.ui.graph, values[0])

    # endtime
    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))

    # color the vertices
    if k <= 8:
      self.ui.graphicsView.scene().unmarkall()
      if solution is not None:
        for v in solution:
          self.ui.graphicsView.scene().markVertex(
              v, COLORLIST[solution[v] - 1])

    # open the solution window
    s = "%s\nUsed %d colors" % (str(solution), k)
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("Rainbow Coloring")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onGracefulLabeling(self):
    datalist = [(QtWidgets.QApplication.translate("PyGraphEdit", 'Methode'),
                 [0, method.AUTOMATIC.name])]

    values = fedit(datalist, title="Graceful labeling")
    if values is None:
      return

    # starttime
    self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))

    if values[0] == method.AUTOMATIC.name:
      labeling = compute_gracefulLabels(PyGraphEdit_log, self.ui.graph)

    s = "Labeling:\n%s" % (str(labeling))
    self.solutiondialog.textBrowser.setText(s)
    self.solutiondialog.label.setText("Graceful labeling")
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
    self.time_stamps = []
    self.solutiondialog.show()

  def onExtPython(self):
    '''
    Call a extern python-script
    TODO:: Start the extern function with the Worker-Class
    '''
    self.extPython.show()

  def __extPython(self):
    '''Called after the OK-Signal from the extPython-Dialog'''
    import importlib
    import imp
    from modules.consts import return_type  # @UnresolvedImport

    # Set some variables
    module = self.extPython.module
    function = self.extPython.function
    self.extPythonModule = module
    self.extPythonFunction = function

    # String for the result window
    self.calc_value = module + '.' + function

    # Generate the function-string; the module must be in the userfiles directory
    function_string = 'userfiles.' + module + '.' + function
    mod_name, func_name = function_string.rsplit('.', 1)

    # Import the module
    mod = importlib.import_module(mod_name)

    # It is nessesary to reload the module
    imp.reload(mod)

    # Get the attributes and start the module
    try:
      func = getattr(mod, func_name)
      self.time_stamps.append((time_stamp_str.START_COMPUTING, time.time()))
      self.rel, returntype = func(self.ui.graph)
      self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Fehler beim Ausf�hren des externen Skripts.\n \
                                                     Ausgabe in PyGraphEdit.log beachten"))
      raise

    # Handle the return value
    if (returntype == return_type.MARK or returntype == return_type.DISMARK):
      if isinstance(self.rel, list):
        for e in self.rel:
          if isinstance(e, int):
            self.ui.graphicsView.scene().markVertex(e)
          elif isinstance(e, tuple):
            self.ui.graphicsView.scene().markEdge(e[0], e[1])

      elif isinstance(self.rel, MarkData):
        data = self.rel.getData()
        if len(set([data[e] for e in data])) <= 8:
          for e in data:
            if isinstance(e, int):
              if isinstance(data[e], int):
                self.ui.graphicsView.scene().markVertex(e,
                                                        COLORLIST[data[e] - 1])
              else:
                self.ui.graphicsView.scene().markVertex(e, data[e])
            elif isinstance(e, tuple):
              if isinstance(data[e], int):
                self.ui.graphicsView.scene().markEdge(e[0], e[1],
                                                      COLORLIST[data[e] - 1])
              else:
                self.ui.graphicsView.scene().markEdge(e[0], e[1], data[e])

    # Show the return value in an window
    if returntype == return_type.DISPLAY or returntype == return_type.DISMARK:
      self.solutiondialog.label.setText("%s" % function_string)
      self.solutiondialog.textBrowser.setText(str(self.rel))
      if len(self.time_stamps) >= 2:
        self.solutiondialog.labelZeit.setText(
          "%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))
      self.solutiondialog.show()

    # Replace the old graph with the new one
    try:
      if (returntype == return_type.SGRAPH and
          (isinstance(self.rel, SGraph) or isinstance(self.rel, list)
           or isinstance(self.rel, tuple))):
        if not self.onNew():
          return

        self.ui.graph = SWGraph()
        if type(self.rel) == list or type(self.rel) == tuple:
          # (SGraph, coord)
          if (not (isinstance(self.rel[0], SGraph)
                   or isinstance(self.rel[0], SWGraph))
              or not isinstance(self.rel[1], dict)):
            raise

          if isinstance(self.rel[0], SWGraph):
            self.ui.graph = self.rel[0].copy()
          else:
            self.ui.graph.convertSGraph(self.rel[0])
          self.ui.setVertices(self.rel[1])
        else:
          self.ui.graph.convertSGraph(self.rel)
          self.ui.setVertices()

        self.max_node = max(self.ui.graph.getV())
        self._setChanged()

      elif returntype == return_type.SGRAPH:
        fedit(None, title="Info",
              comment="Expected return-type is SGraph, but got %s" % str(type(self.rel)))
    except:
      fedit(None, title="Info",
            comment="Expected return-type is SGraph, but got %s" % str(type(self.rel)))

    # If returntype is a simulation, then init the simulation
    if (returntype == simulation_type.MARK
        or returntype == simulation_type.MARKDIFF
        or returntype == simulation_type.SHOW
        or returntype == simulation_type.SHOWDIFF):
      self.__acitvateSimulation(returntype, self.rel)

    self.time_stamps = []

  def onPlanarity(self):
    '''
    Tests if the graph is planar with the algorithm from Boyer-Myrvold
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the planarity test
    bm = testPlanarity.PyBoyerMyrvold()
    r = bm.test_planarity(path.encode('utf-8'))

    # Show the result
    if r == 1:
      fedit(None, title="Info", comment="Graph is planar")
    else:
      fedit(None, title="Info", comment="Graph is not planar")

  def onOGDF_FMMM(self):
    '''
    Embedding with the "Fast Multipole Multilevel Method"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PyFMMM()
    r = bm.start_embedding(path.encode('utf-8'))

    # Show the result
    if r == 1:
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info", comment="An error occured!")

  def onOGDF_PSL(self):
    '''
    Embedding with the "Planar-Straight layout algorithm"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PyPSL()
    r = bm.start_embedding(path.encode('utf-8'))

    if r == 1:
      # Show the result
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords, max_width=750)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info",
            comment="Graph is not planar or an error occured!")

  def onOGDF_PDL(self):
    '''
    Embedding with the "Planar draw layout algorithm"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PyPDL()
    r = bm.start_embedding(path.encode('utf-8'))

    if r == 1:
      # Show the result
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords, max_width=750)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info",
            comment="Graph is not planar or an error occured!")

  def onOGDF_MML(self):
    '''
    Embedding with the "MixedModelLayout algorithm"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PyMML()
    r = bm.start_embedding(path.encode('utf-8'))

    if r == 1:
      # Show the result
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords, max_width=750)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info",
            comment="Graph is not planar or an error occured!")

  def onOGDF_SEFR(self):
    '''
    Embedding with the "SpringEmbedderFR algorithm"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PySEFR()
    r = bm.start_embedding(path.encode('utf-8'))

    if r == 1:
      # Show the result
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords, max_width=750)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info",
            comment="Graph is not planar or an error occured!")

  def onOGDF_GEM(self):
    '''
    Embedding with the "SpringEmbedderFR algorithm"
    from the OGDF-Library
    '''
    if not USE_OGDF:
      fedit(None, title="Info", comment="OGDF is not available")
      return

    self.onClear()

    # Write the graph to an tmp-file (gml)
    _, path = tempfile.mkstemp()
    parser = GMLParser()
    parser.write_gml(self.ui.graph, path)

    # Start the embedding
    bm = testPlanarity.PyGEM()
    r = bm.start_embedding(path.encode('utf-8'))

    if r == 1:
      # Show the result
      g, coords = parser.read_gml(path.encode('utf-8'))
      if g is not None and coords is not None:
        coords = self.ui.scaleVertices(coords, max_width=750)
        self.ui.setVertices(coords)
    else:
      fedit(None, title="Info",
            comment="Graph is not planar or an error occured!")

  def onGenerateTestfile(self):
    self.generateTestDialog.show()

  def startGeneratingTestFile(self):
    """Generate a test file"""
    database = self.generateTestDialog.database
    outputfile = self.generateTestDialog.outputfile
    module = self.generateTestDialog.module
    method = self.generateTestDialog.method
    function = self.generateTestDialog.function

    if generateTestFile(database, outputfile, module, method, function):
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Erzeugen des Testfiles erfolgreich!"))
    else:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Erzeugen des Testfiles nicht erfolgreich!"))

  def onStartTestCalculation(self):
    '''Show the dialog to start the test calculation'''
    self.startTestDialog.show()

  def StartTestCalculation(self):
    '''Start the test calculation in a new thread'''
    self.startTestDialog.close()
    PyGraphEdit_log.setLevel(ERROR)
    self.testStatusDialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog",
                                                                          "Tests running ..."))
    self.testStatusDialog.textEditStatus.clear()
    self.testStatusDialog.hideResultButton()
    self.testStatusDialog.show()

    threading.Thread(target=self.testum.setup_tests,
                     args=(self.startTestDialog.testfile,
                           self.startTestDialog.module,
                           self.startTestDialog.function,
                           self.startTestDialog.method)).start()
    threading.Thread(target=self.testum.test_poly).start()

  def finishTestCalc(self):
    self.testStatusDialog.showResultButton()
    PyGraphEdit_log.setLevel(INFO)

  def onSaveResult(self):
    '''Save the result from the test calculation'''
    # file dialog
    fname, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Save',
                                                     config["Paths"]["test_dir"],
                                                     "XML-files (*.xml)")
    if fname is None or len(fname) == 0:
      return

    if ".xml" not in fname:
      # Add the correct filetype
      fname += ".xml"

    self.testum.saveResult(filename=fname)
    fedit(None, title="Info",
          comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Speichern erfolgreich"))

  def onCompare(self):
    """Compare two results"""
    self.compareResultsDialog.show()

  def compareResults(self):
    '''Compare the result files and show the output or write a LaTeX-file'''
    # TODO::hier noch die Klasse aufrufen
    self.compare = TestsCompare()
    self.compare.CompareResults(self.compareResultsDialog.testFiles)
    if self.compareResultsDialog.generateLatexFile:
      self.compare.writeLaTeXTable(self.compareResultsDialog.latexOutputfile,
                                   self.compareResultsDialog.key)
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Speichern erfolgreich"))
    else:
      self.testStatusDialog.setWindowTitle("Compare results")
      self.testStatusDialog.textEditStatus.clear()
      self.testStatusDialog.hideResultButton()
      self.testStatusDialog.show()
      self.compare.writeResult(self.compareResultsDialog.key)
      self.testStatusDialog.pushButtonOk.setEnabled(True)

  def onSimBFS(self):
    '''Simulation of the BFS-algorithm'''
    marked_vertices = []
    start = None
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))
            start = int(item.value)

    if len(marked_vertices) != 1:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte einen Knoten f�r den Start des BFS ausw�hlen!"))
      return

    simdata, returntype = simulationBFS(self.ui.graph, start)

    if (returntype == simulation_type.MARK
        or returntype == simulation_type.MARKDIFF
        or returntype == simulation_type.SHOW
        or returntype == simulation_type.SHOWDIFF):
      self.__acitvateSimulation(returntype, simdata)

  def onSimDFS(self):
    '''Simulation of the DFS-algorithm'''
    marked_vertices = []
    start = None
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked:
            marked_vertices.append(int(item.value))
            start = int(item.value)

    if len(marked_vertices) != 1:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte einen Knoten f�r den Start des DFS ausw�hlen!"))
      return

    simdata, returntype = simulationDFS(self.ui.graph, start)

    if (returntype == simulation_type.MARK
        or returntype == simulation_type.MARKDIFF
        or returntype == simulation_type.SHOW
        or returntype == simulation_type.SHOWDIFF):
      self.__acitvateSimulation(returntype, simdata)

  def onSimBroadcasting(self):
    '''Simulation of message broadcasting'''
    marked_vertices = ""
    if self.tag:
      for item in self.ui.graphicsView.scene().items():
        if item.type() == globalsClass.USERTYPE + 1:
          if item.marked and len(marked_vertices) != 0:
            marked_vertices = marked_vertices + "," + str(item.value)
          elif item.marked:
            marked_vertices = str(item.value)

    self.simBroadcastingDialog.lineEditStartVertices.setText(marked_vertices)
    self.simBroadcastingDialog.show()

  def _onSimBroadcasting(self):
    '''Called after the OK-Signal from the broadcasting-dialog'''
    simdata, returntype = simulationBroadcasting(self.ui.graph,
                                                 self.simBroadcastingDialog.startVertices,
                                                 everyVertex=self.simBroadcastingDialog.everyVertex,
                                                 steps=self.simBroadcastingDialog.steps,
                                                 probability=self.simBroadcastingDialog.probability,
                                                 wholeNeighborhood=self.simBroadcastingDialog.wholeNeighborhood,
                                                 randomNeighbor=self.simBroadcastingDialog.randomNeighbor,
                                                 hiddenNode=self.simBroadcastingDialog.hiddenNode)

    if (returntype == simulation_type.MARK
        or returntype == simulation_type.MARKDIFF
        or returntype == simulation_type.SHOW
        or returntype == simulation_type.SHOWDIFF):
      self.__acitvateSimulation(returntype, simdata)

  def stopCalc(self):
    '''Stop the calculation'''
    self.thread.thread.terminate()
    self.time_stamps = []
    self.wait.close()

  def mainThread(self, code):
    '''Callback to save the result and display it'''
    self.rel = code

    from sympy import expand
    try:
      self.solutiondialog.result = expand(self.rel)
    except:
      self.solutiondialog.result = self.rel
    self.solutiondialog.textBrowser.setText(str(self.solutiondialog.result))
    self.solutiondialog.label.setText(self.calc_value)

    self.time_stamps.append((time_stamp_str.END_COMPUTING, time.time()))
    if len(self.time_stamps) >= 2:
      self.solutiondialog.labelZeit.setText("%.4f s" % (self.time_stamps[1][1] - self.time_stamps[0][1]))

    self.solutiondialog.show()

    self.wait.close()
    self.time_stamps = []
    self.rel = None

  def updateView(self):
    '''Update the graph'''
    for item in self.ui.graphicsView.scene().items():
      item.update()

  @staticmethod
  def onShowHelp():
    '''Show the help in the webbrowser'''
    import webbrowser
    webbrowser.open(config["Paths"]["help"])

  @staticmethod
  def onBugtracker():
    '''Show the help in the webbrowser'''
    import webbrowser
    webbrowser.open("https://gitlab.com/md31415/PyGraphEdit/issues")

  @staticmethod
  def onShowProginfo():
    '''Show program infos'''
    info = """PyGraphEdit v%s

    Copyright (c) 2013-2017 Markus Dod
    
    PyGraphEdit is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    PyGraphEdit is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>."""

    fedit(None, title="Info", comment=info % str(VERSION))


# =========================================================================
# =========== WINDOW-CLASS ================================================
# =========================================================================
class MainWindow(Ui_MainWindow):
  '''Class for the window'''

  def generateStructures(self):
    '''Setup the GraphicScene'''
    self.graph = SWGraph()

    scene = GraphicsScene(self.graphicsView)
    scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
    scene.setSceneRect(-365, -285, 730, 570)
    self.graphicsView.setScene(scene)

    self.graphicsView.setViewportUpdateMode(
        QtWidgets.QGraphicsView.BoundingRectViewportUpdate)
    self.graphicsView.setTransformationAnchor(
        QtWidgets.QGraphicsView.AnchorUnderMouse)
    self.graphicsView.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)
    self.graphicsView.setDragMode(2)

  def scaleView(self, scaleFactor):
    '''Scale the view'''
    self.graphicsView.scale(scaleFactor, scaleFactor)

  def calcCoordinates(self):
    '''Calculate the coordinates of the vertices'''
    # Use graphviz to calculate the embedding
    if globalsClass.USE_GRAPHVIZ and globalsClass.PLOTSTYLE in PLOTMODES_GRAPHVIZ:
      # Generate a Dot-graph object
      graph = Dot(config["Paths"].get("graphviz_dir", fallback=None),
                  graph_type='graph')
      graph.plotstyle = globalsClass.PLOTSTYLE

      # Add the vertices to the Dot-graph
      for v in self.graph.getVIt():
        node = pydot.Node(str(v))
        graph.add_node(node)

      # Add the edges to the Dot-graph
      for e in self.graph.getEdgeSet():
        edge = pydot.Edge(str(e[0]), str(e[1]))
        graph.add_edge(edge)

      # Call graphviz
      lines = graph.write_plain(None)
      coords, width, height = get_coords(lines)

    elif globalsClass.PLOTSTYLE == "spring embedding":
      # spring embedding
      coords = {}
      for v in self.graph.getVIt():
        coords[v] = (uniform(-385, 730), uniform(-285, 570))
      coords = springEmbedding(self.graph, coords)
      xmin, xmax, ymin, ymax = self.get_max_min(coords)
      width = math.fabs(xmin) + math.fabs(xmax)
      height = math.fabs(ymin) + math.fabs(ymax)

    elif globalsClass.PLOTSTYLE == 'grid':
      # Grid-layout for product graphs
      coords = gridLayout(self.graph, numberx=globalsClass.SECONDORDER,
                          numbery=globalsClass.FIRSTORDER)

      globalsClass.FIRSTORDER = 0
      globalsClass.SECONDORDER = 0

      xmin, xmax, ymin, ymax = self.get_max_min(coords)
      width = math.fabs(xmin) + math.fabs(xmax)
      height = math.fabs(ymin) + math.fabs(ymax)

    else:
      # embedding on a circle
      coords = circleLayout(self.graph)
      xmin, xmax, ymin, ymax = self.get_max_min(coords)
      width = math.fabs(xmin) + math.fabs(xmax)
      height = math.fabs(ymin) + math.fabs(ymax)

    return coords, width, height

  def scaleVertices(self, coords, max_width=1000.0):
    '''Scale the coordinates of the vertices'''
    # Calculate the width and the height of the graph
    xmin, xmax, ymin, ymax = self.get_max_min(coords)
    width = math.fabs(xmin) + math.fabs(xmax)
    height = math.fabs(ymin) + math.fabs(ymax)

    # Calculate the scale factor
    scale = 1.0
    if width > height:
      scale = max_width / width
    else:
      scale = max_width / height

    # Scale the coordinates
    for v in coords.keys():
      coords[v] = (scale * coords[v][0], scale * coords[v][1])

    return coords

  def repaint(self):
    '''
    Repaint the graph
    '''
    coords = self.getCoords()

    scene = GraphicsScene(self.graphicsView)
    scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
    self.graphicsView.setScene(scene)
    
    for v in coords.keys():
      node = Vertex(self, str(v), weight=self.graph.getWeight(int(v)))
      k = coords[v]
      node.setPos(k[0], k[1])
      scene.addItem(node)

    # Insert the edges
    for e in self.graph.getEdgeSet(withloops=True):
      start = None
      end = None
      for w in scene.items():
        if w.type() == globalsClass.USERTYPE + 1 and w.value == str(e[0]):
          start = w
        elif w.type() == globalsClass.USERTYPE + 1 and w.value == str(e[1]):
          end = w
        if start and end:
          break

      if e[0] != e[1]:
        scene.addItem(Edge(start, end,
                           weight=self.graph.getWeight((int(start.value),
                                                        int(end.value)))))
      else:
        scene.addItem(Loop(start,
                           weight=self.graph.getWeight((int(start.value),
                                                        int(start.value)))))

  def setVertices(self, coords=None):
    '''
    Calculate an embedding if no coordinates exist and draw the vertices
    @param coords: Dict of the coordinates (keys: vertices, values: (x,y))
    '''
    scene = GraphicsScene(self.graphicsView)
    scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
    self.graphicsView.setScene(scene)

    # If no coordinates given, then calculate an embedding
    if coords is None:
      coords, width, height = self.calcCoordinates()
      if height is not None and height != 0:
        scale = 1.0
        if width > height:
          scale = 500 / width
        else:
          scale = 500 / height

        # Center the vertices and add it to the scene
        dx, dy = self.centerVertices(coords)
        for v in coords.keys():
          node = Vertex(self, str(v), weight=self.graph.getWeight(int(v)))
          node.setPos(scale * (coords[v][0] - dx), scale * (coords[v][1] - dy))
          scene.addItem(node)

    # Draw the vertices with the given coordinates
    else:
      xmin, xmax, ymin, ymax = self.get_max_min(coords)
      if xmax - xmin < 5 or ymax - ymin < 5:
        coords = self.scaleVertices(coords)
      for v in coords.keys():
        node = Vertex(self, str(v))
        k = coords[v]
        node.setPos(k[0], k[1])
        scene.addItem(node)

    # Insert the edges
    for e in self.graph.getEdgeSet(withloops=True):
      start = None
      end = None
      for w in scene.items():
        if w.type() == globalsClass.USERTYPE + 1 and w.value == str(e[0]):
          start = w
        if w.type() == globalsClass.USERTYPE + 1 and w.value == str(e[1]):
          end = w
        if start and end:
          break

      if e[0] != e[1]:
        scene.addItem(Edge(start, end,
                           weight=self.graph.getWeight((int(start.value),
                                                        int(end.value)))))
      else:
        scene.addItem(Loop(start,
                           weight=self.graph.getWeight((int(start.value),
                                                        int(start.value)))))

  def centerVertices(self, coords):
    '''Center the vertices'''
    xmin, xmax, ymin, ymax = self.get_max_min(coords)
    return xmin + (xmax - xmin) / 2, ymin + (ymax - ymin) / 2

  def get_max_min(self, coords=None):
    '''Return the maximal and minimal x and y of the coordinates of the vertices'''
    if coords is None:
      coords = self.getCoords()

    xmin = float('inf')
    xmax = -float('inf')
    ymin = float('inf')
    ymax = -float('inf')
    for v in coords.keys():
      if coords[v][0] > xmax:
        xmax = coords[v][0]
      if coords[v][0] < xmin:
        xmin = coords[v][0]
      if coords[v][1] > ymax:
        ymax = coords[v][1]
      if coords[v][1] < ymin:
        ymin = coords[v][1]
    return xmin, xmax, ymin, ymax

  def getCoords(self):
    '''Returns a dictonary with the coordinates of the vertices of the graph.'''
    coords = {}
    for w in self.graphicsView.scene().items():
      if w.type() == globalsClass.USERTYPE + 1:
        coords[int(w.value)] = (w.pos().x(), w.pos().y())
    return coords


# =========================================================================
# =========== CLASS FOR THE SCENE =========================================
# =========================================================================
class GraphicsScene(QtWidgets.QGraphicsScene):
  '''Graphicscene'''
  def __init__(self, parent=None):
    super(GraphicsScene, self).__init__(parent)
    self.tmp_item = []
    self.gui = ((self.parent()).parent()).parent()

    self.popMenu = None
    self.actionAddLoop = None
    self.actionContract = None
    self.actionDelete = None
    self.actionNeighbors = None
    self.actionProperties = None
    self.actionSettings = None
    self.actionSubdivision = None
    self.moveItem = None
    self.startPos = None

  def renameNodes(self, u, v):
    '''Rename the vertex u to v'''
    for w in self.items():
      if w.type() == globalsClass.USERTYPE + 1 and int(w.value) == int(u):
        w.value = str(v)
        w.update()
        break
    self.gui.changed = True
    self.gui.deacitvateSimulation()

  def changeWeight(self, element, weight):
    '''Change the weight of an vertex or edge'''
    if type(element) == int:  # Vertex
      for item in self.items():  # Search for the node
        if item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(element):
          item.setWeight(weight)
          break
    else:  # Edge
      u = element[0]
      v = element[1]
      for item in self.items():  # Search for the edge
        if item.type() == globalsClass.USERTYPE + 2:
          if (int(item.source.value) == int(u)
              and int(item.dest.value) == int(v)
              or int(item.source.value) == int(v)
              and int(item.dest.value) == int(u)):
            item.setWeight(weight)
            break

  def wheelEvent(self, event):
    '''Scale the view with the mouse wheel'''
    self.gui.ui.scaleView(math.pow(2.0, -event.delta() / 240.0))

  def showall(self):
    '''Show all items'''
    for item in self.items():
      item.setVisible(True)

  def hideall(self):
    '''Hide all items'''
    for item in self.items():
      item.setVisible(False)

  def showElement(self, v):
    '''Show an element'''
    if type(v) == int:  # Vertex
      for item in self.items():
        if item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(v):
          item.show()
          return
    elif type(v) == tuple:  # Edge
      for item in self.items():
        if item.type() == globalsClass.USERTYPE + 2:
          if (int(item.source.value) == int(v[0])
              and int(item.dest.value) == int(v[1])
              or int(item.source.value) == int(v[1])
              and int(item.dest.value) == int(v[0])):
            item.show()
            return

  def hideElement(self, v):
    '''Hide an element (and his adjacent edges if it is a vertex)'''
    if type(v) == int:
      for item in self.items():
        if item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(v):
          item.hide()
          return
    elif type(v) == tuple:
      for item in self.items():
        if item.type() == globalsClass.USERTYPE + 2:
          if (int(item.source.value) == int(v[0])
              and int(item.dest.value) == int(v[1])
              or int(item.source.value) == int(v[1])
              and int(item.dest.value) == int(v[0])):
            item.hide()
            return

  def showElements(self, elements):
    '''Show all elements in the list'''
    for el in elements:
      self.showElement(el)

  def hideElements(self, elements):
    '''
    Hide all elements in the list
    (and his adjacent edges if it is a vertex)
    '''
    for el in elements:
      self.hideElement(el)

  def markall(self):
    '''Select all elements'''
    for item in self.items():
      if not item.marked:
        item.tag()
        self.tmp_item.append(item)

  def unmarkall(self):
    '''Deselect all elements'''
    self.tmp_item = []
    for item in self.items():
      if item.marked:
        item.untag()

  def markVertex(self, u, color="red"):
    '''Select a vertex of the graph'''
    for item in self.items():
      if item.type() == globalsClass.USERTYPE + 1 and int(item.value) == int(u):
        item.tag(color)
        self.tmp_item.append(item)
        return

  def markEdge(self, u, v, color="red"):
    '''Select an edge of the graph'''
    for item in self.items():
      if item.type() == globalsClass.USERTYPE + 2:
        if (int(item.source.value) == int(u)
            and int(item.dest.value) == int(v)
            or int(item.source.value) == int(v)
            and int(item.dest.value) == int(u)):
          item.tag(color)
          self.tmp_item.append(item)
          return

  def keyPressEvent(self, e):
    '''Overwrites the key-press-event'''
    if e.key() == QtCore.Qt.Key_Escape:
      self.unmarkall()
    elif e.key() == QtCore.Qt.Key_Enter or e.key() == QtCore.Qt.Key_Return:
      pass
    elif e.key() == QtCore.Qt.Key_Delete and len(self.tmp_item) > 0:
      self.onActionDeleteAll()
      self.tmp_item = []

  def onActionDeleteAll(self):
    '''Delete all elements in self.tmp_item (these are the selected ones)'''
    if not self.gui.deacitvateSimulation():
      return

    for item in self.tmp_item:
      # If the item is a vertex
      if item.type() == globalsClass.USERTYPE + 1:
        for edge in item.edges():
          self.removeItem(edge)
        self.removeItem(item)
        self.gui.ui.graph.deleteVertex(int(item.value))
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)

      # If the item is an edge
      elif item.type() == globalsClass.USERTYPE + 2:
        self.gui.ui.graph.deleteEdge((int(item.source.value),
                                      int(item.dest.value)))
        item.source.removeEdge(item)
        item.dest.removeEdge(item)
        self.removeItem(item)
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)

      # If the item is a loop
      elif item.type() == globalsClass.USERTYPE + 3:
        self.gui.ui.graph.deleteEdge((int(item.source.value),
                                      int(item.dest.value)))
        item.source.removeEdge(item)
        self.removeItem(item)
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)

    # Clean the list
    self.tmp_item = []
    self.gui.changed = True

  def contextMenuEvent(self, event):
    '''Show the context menu'''
    # TODO:: Erstellung der Events in eigene Funktion und nur beim erzeugen
    # der Szene aufrufen und anlegen.
    pos = event.scenePos()
    transform = QtGui.QTransform()
    item = self.itemAt(pos, transform)

    self.popMenu = QtWidgets.QMenu()

    # No element
    if item is None:
      # Actions
      toolbar = QtWidgets.QToolBar()
      self.actionSettings = toolbar.addAction(QtWidgets.QApplication.translate("PyGraphEdit",
                                                                               "Einstellungen"),
                                              self.gui.onSettings)

      # Show the menu
      self.popMenu.addAction(self.actionSettings)

    # Choosen a vertex
    elif item.type() == globalsClass.USERTYPE + 1:
      # Actions
      toolbar = QtWidgets.QToolBar()
      self.actionNeighbors = toolbar.addAction("Nachbarn markieren",
                                               lambda who=item: self.onActionNeighbors(who))
      self.actionDelete = toolbar.addAction("L�schen",
                                            lambda who=item: self.onActionDelete(who))
      self.actionProperties = toolbar.addAction("Eigenschaften",
                                                lambda who=item: self.onActionProperties(who))
      self.actionAddLoop = toolbar.addAction("Schlinge einf�gen",
                                             lambda who=item: self.onActionAddLoop(who))

      # Show the menu
      self.popMenu.addAction(self.actionNeighbors)
      self.popMenu.addAction(self.actionDelete)
      self.popMenu.addAction(self.actionAddLoop)
      self.popMenu.addSeparator()
      self.popMenu.addAction(self.actionProperties)

    # Choosen an edge
    elif item.type() == globalsClass.USERTYPE + 2:
      # Actions
      toolbar = QtWidgets.QToolBar()
      self.actionDelete = toolbar.addAction("L�schen",
                                            lambda who=item: self.onActionDelete(who))
      self.actionContract = toolbar.addAction("Kontrahieren",
                                              lambda who=item: self.onActionContract(who))
      self.actionSubdivision = toolbar.addAction("Subdivision",
                                                 lambda who=item: self.onActionSubdivision(who))
      self.actionProperties = toolbar.addAction("Eigenschaften",
                                                lambda who=item: self.onActionPropertiesEdge(who))

      # Show the menu
      self.popMenu.addAction(self.actionContract)
      self.popMenu.addAction(self.actionDelete)
      self.popMenu.addAction(self.actionSubdivision)
      self.popMenu.addSeparator()
      self.popMenu.addAction(self.actionProperties)

    # Choosen a loop
    elif item.type() == globalsClass.USERTYPE + 3:
      # Actions
      toolbar = QtWidgets.QToolBar()
      self.actionDelete = toolbar.addAction("L�schen",
                                            lambda who=item: self.onActionDelete(who))
      self.actionProperties = toolbar.addAction("Eigenschaften",
                                                lambda who=item: self.onActionPropertiesEdge(who))

      # Show the menu
      self.popMenu.addAction(self.actionDelete)
      self.popMenu.addSeparator()
      self.popMenu.addAction(self.actionProperties)

    self.popMenu.exec_(event.screenPos())

  def onActionNeighbors(self, item):
    '''Select the neighbors of item'''
    # Select the clicked vertex and mark it blue
    tmp_item = item
    self.tmp_item.append(item)
    self.unmarkall()
    tmp_item.tag(color="blue")

    # Get the neighbors from the SGraph and mark it
    neighbors = self.gui.ui.graph.neighbors(int(tmp_item.value))
    for item in self.items():
      if item.type() == globalsClass.USERTYPE + 1 and int(item.value) in neighbors:
        item.tag()
        self.tmp_item.append(item)
    self.gui.tag = True

  def onActionAddLoop(self, node):
    '''Add a loop to the graph'''
    for item in self.items():
      if item.type() == globalsClass.USERTYPE + 3 and item.source.value == node.value:
        return False

    edge = Loop(node, weight=Edge.defaultEdgeWeight)
    self.addItem(edge)
    self.gui.ui.graph.insertEdge(int(node.value), int(node.value),
                                 weight=Edge.defaultEdgeWeight)
    self.unmarkall()
    self.update()
    self.gui.changed = True

  def onActionProperties(self, item):
    '''Show vertex infos'''
    tmp_item = item
    value = int(tmp_item.value)
    self.gui.vertexInfo.lineEditNummer.setText(str(value))
    self.gui.vertexInfo.lineEditKnotengrad.setText(str(self.gui.ui.graph.degree(value, withloops=True)))
    self.gui.vertexInfo.lineEditGewicht.setText("%.3f" % self.gui.ui.graph.getWeight(value))
    self.gui.vertexInfo.lineEditExcentrizitaet.setText("%.3f" % GraphFunctions.eccentricity(self.gui.ui.graph, value))
    self.gui.vertexInfo.lineEditGesDist.setText("%.3f" % GraphFunctions.overallDistance(self.gui.ui.graph, value))
    self.gui.vertexInfo.show()

  def onActionPropertiesEdge(self, item):
    '''Show edge infos'''
    value = (int(item.source.value), int(item.dest.value))
    if int(item.source.value) < int(item.dest.value):
      self.gui.edgeInfo.lineEditStart.setText(str(item.source.value))
      self.gui.edgeInfo.lineEditEnd.setText(str(item.dest.value))
    else:
      self.gui.edgeInfo.lineEditEnd.setText(str(item.source.value))
      self.gui.edgeInfo.lineEditStart.setText(str(item.dest.value))

    self.gui.edgeInfo.lineEditGewicht.setText("%.3f" % self.gui.ui.graph.getWeight(value))
    self.gui.edgeInfo.show()

  def onActionDelete(self, item=None):
    '''Delete all elements in self.tmp_item (these are the selected ones)'''
    if not self.gui.deacitvateSimulation():
      return

    if item is not None:
      # If the item is a vertex
      if item.type() == globalsClass.USERTYPE + 1:
        for edge in item.edges():
          self.removeItem(edge)
        self.removeItem(item)
        self.gui.ui.graph.deleteVertex(int(item.value))
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)

      # If the item is an edge
      elif item.type() == globalsClass.USERTYPE + 2:
        self.gui.ui.graph.deleteEdge((int(item.source.value),
                                      int(item.dest.value)))
        item.source.removeEdge(item)
        item.dest.removeEdge(item)
        self.removeItem(item)
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)

      # If the item is a loop
      elif item.type() == globalsClass.USERTYPE + 3:
        self.gui.ui.graph.deleteEdge((int(item.source.value),
                                      int(item.dest.value)))
        item.source.removeEdge(item)
        self.removeItem(item)
        self.gui.operations.append((-1, item))
        self.gui.ui.actionRueckgaengig.setEnabled(True)
        self.gui.ui.repaint()

    if item in self.tmp_item:
      self.tmp_item.remove(item)

    # Set changed
    self.gui.changed = True

  def onActionSubdivision(self, item):
    '''Subdivision of the edge'''
    if not self.gui.deacitvateSimulation():
      return

    edge = (int(item.source.value), int(item.dest.value))
    self.gui.max_node = self.gui.max_node + 1

    # Apply the operation to the unterlying graph
    self.gui.ui.graph.subdivision(edge, self.gui.max_node)

    # Add the new vertex
    newVertex = Vertex(self, self.gui.max_node,
                       weight=Vertex.defaultVertexWeight)
    newVertex.setPos(item.sourcePoint.x() + (item.destPoint.x() - item.sourcePoint.x()) / 2,
                     item.sourcePoint.y() + (item.destPoint.y() - item.sourcePoint.y()) / 2)
    self.addItem(newVertex)

    # Add the new edges
    edge = Edge(item.source, newVertex, weight=Edge.defaultEdgeWeight)
    self.addItem(edge)
    edge = Edge(item.dest, newVertex, weight=Edge.defaultEdgeWeight)
    self.addItem(edge)

    # Remove the edge from the gui
    item.source.removeEdge(item)
    item.dest.removeEdge(item)
    self.removeItem(item)

    self.unmarkall()
    self.gui.changed = True
    self.gui.operations.clear()
    self.gui.ui.actionRueckgaengig.setEnabled(False)

  def restoreItem(self, item):
    '''Restore a deleted element (undo command)'''
    if not self.gui.deacitvateSimulation():
      return

    if item.type() == globalsClass.USERTYPE + 1:
      item_new = Vertex(self, item.value)
      position = QtCore.QPointF(item.pos())
      item_new.setPos(position.x(), position.y())
      self.addItem(item_new)
      self.gui.ui.graph.insertVertex(int(item.value))
      for v in item.neighbors:
        for w in self.items():
          if w.type() == globalsClass.USERTYPE + 1 and int(w.value) == v:
            self.addItem(Edge(item_new, w))
        self.gui.ui.graph.insertEdge(int(item.value), v)
    else:
      if (int(item.source.value) not in self.gui.ui.graph.getV()
          and int(item.dest.value) not in self.gui.ui.graph.getV()):
        self.addItem(item)
        self.gui.ui.graph.insertEdge(int(item.source.value),
                                     int(item.dest.value))

    if len(self.gui.operations) == 0:
      self.gui.ui.actionRueckgaengig.setEnabled(False)
    self.unmarkall()
    self.gui.changed = True

  def delItem(self, item):
    '''Delete an element (undo command)'''
    self.tmp_item = []
    if item.type() == globalsClass.USERTYPE + 1:
      for edge in item.edges():
        self.removeItem(edge)
      self.removeItem(item)
      self.gui.ui.graph.deleteVertex(int(item.value))

    elif item.type() == globalsClass.USERTYPE + 2:
      self.gui.ui.graph.deleteEdge((int(item.source.value),
                                    int(item.dest.value)))
      item.source.removeEdge(item)
      item.dest.removeEdge(item)
      self.removeItem(item)

    if len(self.gui.operations) == 0:
      self.gui.ui.actionRueckgaengig.setEnabled(False)

  def onActionContract(self, item):
    '''Contract an edge'''
    if not self.gui.deacitvateSimulation():
      return

    # Remove the edge from the gui-graph
    u = item.source
    v = item.dest
    self.removeItem(item)
    u.removeEdge(item)
    v.removeEdge(item)

    # Change the position of u
    x = -(u.pos().x() - v.pos().x()) / 2
    y = -(u.pos().y() - v.pos().y()) / 2
    u.moveBy(x, y)

    # Add new edges to u
    neighbors = set(self.gui.ui.graph.neighbors(int(u.value))) - set([int(v.value)])
    for edge in v.edges():
      if edge.source == v and edge.dest != v:
        if int(edge.dest.value) not in neighbors:
          self.addItem(Edge(u, edge.dest))
      elif edge.dest == v and edge.source != v:
        if int(edge.source.value) not in neighbors:
          self.addItem(Edge(u, edge.source))
      elif edge.source == v and edge.dest == v:
        if not self.gui.ui.graph.hasLoop(int(u.value)):
          self.addItem(Loop(u, u))

    # Delete the incident edges of v
    for edge in v.edges():
      if edge.source.value != v.value:
        edge.source.removeEdge(edge)
      elif edge.dest.value != v.value:
        edge.dest.removeEdge(edge)
      self.removeItem(edge)

    # Remove the vertex v
    self.removeItem(v)

    # Contract the edge in the underlying SGraph
    self.gui.ui.graph.contractEdge((int(u.value), int(v.value)))

    self.unmarkall()
    self.gui.ui.repaint()
    self.gui.changed = True
    self.gui.operations.clear()
    self.gui.ui.actionRueckgaengig.setEnabled(False)

  def mousePressEvent(self, event):
    '''Overwrite the mouse press event'''
    super(GraphicsScene, self).mousePressEvent(event)
    pos = event.scenePos()
    transform = QtGui.QTransform()
    item = self.itemAt(pos, transform)

    if event.button() == QtCore.Qt.LeftButton:
      # Insert a vertex
      if self.gui.ui.radioButtonEinfuegen.isChecked():
        if item is None:
          if not self.gui.deacitvateSimulation():
            return
          self.gui.max_node = self.gui.max_node + 1
          item = Vertex(self, self.gui.max_node,
                        weight=Vertex.defaultVertexWeight)
          position = QtCore.QPointF(event.scenePos())
          item.setPos(position.x(), position.y())
          self.addItem(item)
          self.gui.ui.graph.insertVertex(self.gui.max_node,
                                         weight=Vertex.defaultVertexWeight)
          self.unmarkall()
          self.gui.changed = True
          self.gui.operations.append((1, item))
          self.gui.ui.actionRueckgaengig.setEnabled(True)

      # Insert an edge
      elif self.gui.ui.radioButtonKanteEinfuegen.isChecked():
        if item is not None and item.type() == globalsClass.USERTYPE + 1:
          if len(self.tmp_item) > 1:  # if more than one vertex is marked, then unmark all
            self.unmarkall()
          elif len(self.tmp_item) == 1:
            if not self.gui.deacitvateSimulation():
              return
            self.tmp_item[0].untag()
            self.gui.tag = False
            edge = Edge(item, self.tmp_item[0], weight=Edge.defaultEdgeWeight)
            self.addItem(edge)
            self.gui.ui.graph.insertEdge(int(item.value),
                                         int(self.tmp_item[0].value),
                                         weight=Edge.defaultEdgeWeight)
            self.unmarkall()
            self.gui.changed = True
            self.gui.operations.append((1, edge))
            self.gui.ui.actionRueckgaengig.setEnabled(True)
          else:
            self.tmp_item.append(item)
            item.tag()
            self.gui.tag = True

      # Contract an edge
      elif self.gui.ui.radioButtonKanteKontra.isChecked():
        if item is not None and item.type() == globalsClass.USERTYPE + 2:
          self.unmarkall()
          self.tmp_item.append(item)
          self.onActionContract(item)
          self.gui.operations.clear()
          self.update()

      # Select a vertex or an edge
      elif self.gui.ui.radioButtonElementmarkieren.isChecked():
        if item is not None:
          if item.marked:
            item.untag()
            self.tmp_item.remove(item)
            if len(self.tmp_item) == 0:
              self.gui.tag = False
          else:
            item.tag(globalsClass.COLOR)
            self.gui.tag = True
            self.tmp_item.append(item)

        else:
          self.startPos = event.scenePos()

      # Move all selected elements
      elif self.gui.ui.radioButtonElementMove.isChecked():
        if item is not None:
          self.moveItem = item
          self.startPos = item.pos()

  def mouseReleaseEvent(self, event):
    '''Overwrites the mouse release event'''
    super(GraphicsScene, self).mouseReleaseEvent(event)
    pos = event.scenePos()
    transform = QtGui.QTransform()
    item = self.itemAt(pos, transform)

    # Move all selected elements
    if self.gui.ui.radioButtonElementMove.isChecked() and item is not None:
      newpos = item.pos()
      xval = newpos.x() - self.startPos.x()
      yval = newpos.y() - self.startPos.y()
      for vertex in self.tmp_item:
        if vertex.type() == globalsClass.USERTYPE + 1 and int(vertex.value) != int(item.value):
          vertex.move(xval, yval)

    # Show an rectangle to select more than one vertex
    elif self.gui.ui.radioButtonElementmarkieren.isChecked():
        if item is None:
          newpos = event.scenePos()
          x_left = min(newpos.x(), self.startPos.x())
          x_right = max(newpos.x(), self.startPos.x())
          y_top = min(newpos.y(), self.startPos.y())
          y_floor = max(newpos.y(), self.startPos.y())

          for item in self.items():
            if item.type() == globalsClass.USERTYPE + 1:
              x = item.pos().x()
              y = item.pos().y()
              if x_left <= x and x_right >= x and y >= y_top and y <= y_floor:
                item.tag()
                self.gui.tag = True
                self.tmp_item.append(item)


# =========================================================================
# ============= WORKER ====================================================
# =========================================================================
class WorkThread(QtCore.QThread):
  '''Class for a working thread'''
  mainThread = QtCore.pyqtSignal(str)

  def __init__(self, parent=None, target=None, args=()):
    '''
    Init the class
    @param parent: Parent class (default=None)
    @param target: Function to start
    @param args: Tuple of parameters of the function
    '''
    QtCore.QThread.__init__(self, parent)
    self.target = target
    self.args = args
    self.thread = None

  def run(self):
    '''Start the worker'''
    q = multiprocessing.Queue()
    largs = list(self.args)
    largs.append(q)

    if system() == "Windows":
      rel = self.target(*self.args)
      self.mainThread.emit(str(rel))
    else:
      # In the target function the result must be inserted in the queue q (with q.put(result)).
      # The return value will be ignored.
      # TODO:: Use the returnvalue for user feedback
      self.thread = multiprocessing.Process(target=self.target,
                                            args=tuple(largs))
      self.thread.start()
      self.thread.join()
      rel = q.get()

      self.mainThread.emit(str(rel))


# =========================================================================
# ============= VERTEX CLASS ==============================================
# =========================================================================
class Vertex(QtWidgets.QGraphicsItem):
  '''
  Class for a vertex in the graphic scene
  '''
  Type = 65001
  vertexSize = 20
  drawNumber = True
  defaultVertexWeight = 1.0
  displayVertexWeight = False

  def __init__(self, graphWidget, nr=None, weight=None):
    super(Vertex, self).__init__()

    # vertex informations
    self.value = nr

    if weight:
      self.weight = weight
    else:
      self.weight = Vertex.defaultVertexWeight

    self.graph = graphWidget
    self.edgeList = []  # incident edges
    self.neighbors = []  # list of the neighbors
    self.newPos = QtCore.QPointF()
    self.marked = False
    self.color = QtCore.Qt.yellow
    self.color2 = QtCore.Qt.darkYellow

    # Set some properties of this QGraphicsItem
    self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
    self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
    self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
    self.setZValue(1)

  @staticmethod
  def type():
    '''Returns the type'''
    return Vertex.Type

  def setWeight(self, weight):
    '''Set the vertex weight'''
    self.weight = weight
    self.update()

  def addEdge(self, edge):
    '''Adds an incident edge'''
    self.edgeList.append(edge)
    if int(edge.source.value) != int(self.value):
      self.neighbors.append(int(edge.source.value))
    else:
      self.neighbors.append(int(edge.dest.value))
    edge.adjust()

  def removeEdge(self, edge):
    '''Deletes an edge'''
    if int(edge.source.value) != int(self.value):
      self.neighbors.remove(int(edge.source.value))
    else:
      self.neighbors.remove(int(edge.dest.value))
    self.edgeList.remove(edge)

  def edges(self):
    '''Returns the list of incident edges'''
    return self.edgeList

  def move(self, xval, yval):
    '''Moves a vertex'''
    pos = self.pos()
    self.setPos(pos.x() + xval, pos.y() + yval)

  def advance(self):
    if self.newPos == self.pos():
      return False

    self.setPos(self.newPos)
    return True

  @staticmethod
  def boundingRect():
    '''Returns the bounding rect'''
    adjust = math.floor(Vertex.vertexSize * 0.35)
    if Vertex.displayVertexWeight:
      return QtCore.QRectF(-(Vertex.vertexSize / 2), -(Vertex.vertexSize / 2) - adjust,
                           Vertex.vertexSize + 1.75 * adjust, Vertex.vertexSize + adjust)
    else:
      return QtCore.QRectF(-(Vertex.vertexSize / 2), -(Vertex.vertexSize / 2),
                           Vertex.vertexSize, Vertex.vertexSize)

  def tag(self, color="red"):
    '''Select this vertex'''
    if color not in COLORLIST:
      return
    if color == "red":
      self.color = QtCore.Qt.red
      self.color2 = QtCore.Qt.darkRed
    elif color == "blue":
      self.color = QtCore.Qt.blue
      self.color2 = QtCore.Qt.darkBlue
    elif color == "green":
      self.color = QtCore.Qt.green
      self.color2 = QtCore.Qt.darkGreen
    elif color == "gray":
      self.color = QtCore.Qt.gray
      self.color2 = QtCore.Qt.darkGray
    elif color == "magenta":
      self.color = QtCore.Qt.magenta
      self.color2 = QtCore.Qt.darkMagenta
    elif color == "cyan":
      self.color = QtCore.Qt.cyan
      self.color2 = QtCore.Qt.darkCyan
    elif color == "yellow":
      self.color = QtCore.Qt.yellow
      self.color2 = QtCore.Qt.darkYellow
    self.update()
    self.marked = True

  def untag(self):
    '''Deselect this vertex'''
    self.color = QtCore.Qt.yellow
    self.color2 = QtCore.Qt.darkYellow
    self.update()
    self.marked = False

  def paint(self, painter, option, widget):
    '''Overwrite the paint method'''
    # Draw the vertex
    painter.setPen(QtCore.Qt.NoPen)
    gradient = QtGui.QRadialGradient(-3, -3, Vertex.vertexSize / 2)
    gradient.setColorAt(0, self.color)
    gradient.setColorAt(1, self.color2)
    painter.setBrush(QtGui.QBrush(gradient))
    painter.setPen(QtGui.QPen(QtCore.Qt.black, 0))
    painter.drawEllipse(int(math.floor(-Vertex.vertexSize * 0.5)),
                        int(math.floor(-Vertex.vertexSize * 0.5)),
                        Vertex.vertexSize, Vertex.vertexSize)

    # Show the vertex number?
    if self.value is not None and Vertex.drawNumber:
      painter.setFont(QtGui.QFont('DejaVu Sans',
                                  int(math.floor(Vertex.vertexSize * 0.4))))
      if int(self.value) <= 9:
        painter.drawText(int(math.floor(-Vertex.vertexSize * 0.2)),
                         int(math.floor(Vertex.vertexSize * 0.2)),
                         str(self.value))
      else:
        painter.drawText(int(math.floor(-Vertex.vertexSize * 0.3)),
                         int(math.floor(Vertex.vertexSize * 0.2)),
                         str(self.value))

    # Show the vertex weight?
    if self.weight is not None and Vertex.displayVertexWeight:
      painter.setFont(QtGui.QFont('DejaVu Sans',
                                  int(math.floor(Vertex.vertexSize * 0.3))))
      painter.drawText(int(math.floor(Vertex.vertexSize * 0.2)),
                       int(math.floor(-Vertex.vertexSize * 0.5)),
                       "%.2f" % float(self.weight))

  def itemChange(self, change, value):
    '''Overwrites the itemChange-method to move a vertex'''
    if change == QtWidgets.QGraphicsItem.ItemPositionHasChanged:
      # place on a grid
      if globalsClass.USE_GRID:
        pos = self.pos()
        self.setPos(math.floor(pos.x()), math.floor(pos.y()))

      # adjust the edges
      for edge in self.edgeList:
        edge.adjust()

    return super(Vertex, self).itemChange(change, value)


# =========================================================================
# ========== EDGE CLASS ===================================================
# =========================================================================
class Edge(QtWidgets.QGraphicsItem):
  '''
  Class for an edge in the graphic scene
  '''
  Type = 65002
  edgeWidth = 1.0
  defaultEdgeWeight = 1.0
  displayEdgeWeight = False

  def __init__(self, sourceNode, destNode, weight=None):
    '''
    Init the class
    @param sourceVertex: First endvertex of the edge
    @param destVertex: Second endvertex of the edge
    @param weight: Weight of the edge (default=1.0)
    '''
    super(Edge, self).__init__()

    self.sourcePoint = QtCore.QPointF()
    self.destPoint = QtCore.QPointF()
    self.arrowSize = 10
    self.edgeWidth = 1.0

    if weight:
      self.weight = weight
    else:
      self.weight = Edge.defaultEdgeWeight

    self.marked = False

    self.source = sourceNode
    self.dest = destNode
    self.source.addEdge(self)
    self.dest.addEdge(self)
    self.adjust()
    self.color = QtCore.Qt.black

    self.setAcceptedMouseButtons(QtCore.Qt.NoButton)

  def tag(self, color="red"):
    '''Select the edge'''
    if color not in COLORLIST:
      return
    if color == "red":
      self.color = QtCore.Qt.red
    elif color == "blue":
      self.color = QtCore.Qt.blue
    elif color == "green":
      self.color = QtCore.Qt.green
    elif color == "gray":
      self.color = QtCore.Qt.gray
    elif color == "magenta":
      self.color = QtCore.Qt.magenta
    elif color == "cyan":
      self.color = QtCore.Qt.cyan
    elif color == "yellow":
      self.color = QtCore.Qt.yellow
    elif color == "black":
      self.color = QtCore.Qt.black
    self.edgeWidth = 1.5
    self.adjust()
    self.marked = True

  def untag(self):
    '''Deselect the edge'''
    self.color = QtCore.Qt.black
    self.edgeWidth = 1
    self.adjust()
    self.marked = False

  def setWeight(self, weight):
    '''Set the edge weight'''
    self.weight = weight
    self.update()

  @staticmethod
  def type():
    '''Returns the item type'''
    return Edge.Type

  def sourceVertex(self):
    '''Returns the start vertex'''
    return self.source

  def setSourceVertex(self, node):
    '''Set the start vertex'''
    self.source = node
    self.adjust()

  def destVertex(self):
    '''Returns the end vertex'''
    return self.dest

  def setDestVertex(self, node):
    '''Sets the end vertex'''
    self.dest = node
    self.adjust()

  def adjust(self):
    '''Redraw the edge if the position of one end vertex has changed'''
    if not self.source or not self.dest:
      return

    line = QtCore.QLineF(self.mapFromItem(self.source, 0, 0),
                         self.mapFromItem(self.dest, 0, 0))
    length = line.length()

    self.prepareGeometryChange()

    if length > 20.0:
      self.sourcePoint = line.p1()
      self.destPoint = line.p2()
    else:
      self.sourcePoint = line.p1()
      self.destPoint = line.p1()

  def boundingRect(self):
    '''Returns the bounding rect of the edge'''
    if not self.source or not self.dest:
      return QtCore.QRectF()

    extra = (Edge.edgeWidth + self.arrowSize) / 2.0

    return QtCore.QRectF(self.sourcePoint,
                         QtCore.QSizeF(self.destPoint.x() - self.sourcePoint.x(),
                                       self.destPoint.y() - self.sourcePoint.y())).normalized().adjusted(-extra, -extra, extra, extra)

  def shape(self):
    '''Set the select area of the edge'''
    extra = (Edge.edgeWidth + self.arrowSize) / 3

    path = QtGui.QPainterPath()

    # Get the right and the left end point of the edge
    if self.sourceVertex().pos().x() <= self.destVertex().pos().x():
      left_point = self.sourceVertex().pos()
      right_point = self.destVertex().pos()
    else:
      right_point = self.sourceVertex().pos()
      left_point = self.destVertex().pos()

    left = left_point.x()
    right = right_point.x()
    top = max([self.sourceVertex().pos().y(), self.destVertex().pos().y()])
    botton = min([self.sourceVertex().pos().y(), self.destVertex().pos().y()])

    # Calculates the angle of the edge
    if math.fabs(right - left) == 0.0:
      alpha = math.pi / 2 - math.atan((math.fabs(top - botton)) / (0.01))
    else:
      alpha = math.pi / 2 - math.atan((math.fabs(top - botton)) / (math.fabs(right - left)))
    shiftx = math.fabs(extra * math.cos(alpha))
    shifty = math.fabs(extra * math.sin(alpha))

    # Calculates the four corners of the rectangle
    if left_point.y() >= right_point.y():
      left_top = QtCore.QPointF(left + shiftx, top + shifty)
      right_top = QtCore.QPointF(right + shiftx, botton + shifty)
      left_botton = QtCore.QPointF(left - shiftx, top - shifty)
      right_botton = QtCore.QPointF(right - shiftx, botton - shifty)
    elif left_point.y() < right_point.y():
      left_top = QtCore.QPointF(left - shiftx, botton + shifty)
      right_top = QtCore.QPointF(right - shiftx, top + shifty)
      left_botton = QtCore.QPointF(left + shiftx, botton - shifty)
      right_botton = QtCore.QPointF(right + shiftx, top - shifty)

    # Adds the calculated polygon to the path and return it
    myPolygon = QtGui.QPolygonF(
        [left_top, right_top, right_botton, left_botton])
    path.addPolygon(myPolygon)
    return path

  def paint(self, painter, option, widget):
    '''Overwrites the paint method'''
    # Bei Mehrfachkanten m�ssten B�gen mit drawArc gezeichnet werden
    # QPainter.drawArc (self, int x, int y, int w, int h, int a, int alen)
    # This is an overloaded function.
    # Draws the arc defined by the rectangle beginning at (x, y) with the
    # specified width and height, and the given startAngle and spanAngle.

    if not self.source or not self.dest:
      return

    # Draw the line itself.
    line = QtCore.QLineF(self.sourcePoint, self.destPoint)

    if line.length() == 0.0:
      return

    painter.setPen(QtGui.QPen(self.color, self.edgeWidth, QtCore.Qt.SolidLine,
                              QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
    painter.drawLine(line)

    # Show the edge weight?
    if self.weight is not None and Edge.displayEdgeWeight:
      painter.setFont(QtGui.QFont('DejaVu Sans',
                                  int(math.floor(Vertex.vertexSize * 0.3))))
      # if float(self.weight) < 10.0:
      painter.drawText(int((self.sourceVertex().pos().x() + self.destVertex().pos().x()) / 2),
                       int((self.sourceVertex().pos().y() + self.destVertex().pos().y()) / 2),
                       "%.2f" % float(self.weight))
      # else:
      #  painter.drawText(int(math.floor(Vertex.vertexSize * 0.2)), int(math.floor(-Vertex.vertexSize * 0.5)), "%.2f" % float(self.weight))

  def __str__(self):
    '''Returns a string representation of the edge'''
    return "{%s, %s}" % (str(self.source.value), str(self.dest.value))


class Loop(QtWidgets.QGraphicsItem):
  '''
  Class for an edge in the graphic scene
  '''
  Type = 65003
  edgeWidth = 1.0
  defaultEdgeWeight = 1.0
  displayEdgeWeight = False

  def __init__(self, sourceNode, weight=None):
    '''
    Init the class
    @param sourceVertex: First endvertex of the edge
    @param destVertex: Second endvertex of the edge
    @param weight: Weight of the edge (default=1.0)
    '''
    super(Loop, self).__init__()

    self.sourcePoint = QtCore.QPointF()
    self.destPoint = QtCore.QPointF()
    self.arrowSize = 10

    if weight:
      self.weight = weight
    else:
      self.weight = Edge.defaultEdgeWeight

    self.marked = False

    self.source = sourceNode
    self.dest = sourceNode
    self.source.addEdge(self)
    self.adjust()
    self.color = QtCore.Qt.black

    self.setAcceptedMouseButtons(QtCore.Qt.NoButton)

  def tag(self, color="red"):
    '''Select the edge'''
    if color not in COLORLIST:
      return
    if color == "red":
      self.color = QtCore.Qt.red
    elif color == "blue":
      self.color = QtCore.Qt.blue
    elif color == "green":
      self.color = QtCore.Qt.green
    elif color == "gray":
      self.color = QtCore.Qt.gray
    elif color == "magenta":
      self.color = QtCore.Qt.magenta
    elif color == "cyan":
      self.color = QtCore.Qt.cyan
    elif color == "yellow":
      self.color = QtCore.Qt.yellow
    elif color == "black":
      self.color = QtCore.Qt.black
    self.adjust()
    self.marked = True

  def untag(self):
    '''Deselect the edge'''
    self.color = QtCore.Qt.black
    self.adjust()
    self.marked = False

  def setWeight(self, weight):
    '''Set the edge weight'''
    self.weight = weight
    self.update()

  @staticmethod
  def type():
    '''Returns the item type'''
    return Loop.Type

  def sourceVertex(self):
    '''Returns the start vertex'''
    return self.source

  def setSourceVertex(self, node):
    '''Set the start vertex'''
    self.source = node
    self.adjust()

  def destVertex(self):
    '''Returns the end vertex'''
    return self.dest

  def setDestVertex(self, node):
    '''Sets the end vertex'''
    self.dest = node
    self.adjust()

  def adjust(self):
    '''Redraw the edge if the position of one end vertex has changed'''
    if not self.source or not self.dest:
      return

    line = QtCore.QLineF(self.mapFromItem(self.source, 0, 0),
                         self.mapFromItem(self.dest, 0, 0))
    length = line.length()

    self.prepareGeometryChange()

    if length > 20.0:
      self.sourcePoint = line.p1()
      self.destPoint = line.p2()
    else:
      self.sourcePoint = line.p1()
      self.destPoint = line.p1()

  def boundingRect(self):
    '''Returns the bounding rect of the edge'''
    if not self.source or not self.dest:
      return QtCore.QRectF()

    extra = (Edge.edgeWidth + self.arrowSize) / 2.0

    return QtCore.QRectF(self.sourcePoint,
                         QtCore.QSizeF(20, 20)).normalized().adjusted(-extra,
                                                                      -extra,
                                                                      extra,
                                                                      extra)

  def paint(self, painter, option, widget):
    '''Overwrites the paint method'''
    if not self.source:
      return

    path = QtGui.QPainterPath()
    path.addRoundedRect(self.sourceVertex().pos().x(),
                        self.sourceVertex().pos().y(),
                        20, 20, 70, 70)

    painter.setPen(QtGui.QPen(self.color, Edge.edgeWidth, QtCore.Qt.SolidLine,
                              QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
    painter.drawPath(path)


# =========================================================================
class ConfigDialog(QtWidgets.QDialog, Ui_Config):
  '''Configuration dialog'''
  activateGraphvizSignal = QtCore.pyqtSignal()
  deactivateGraphvizSignal = QtCore.pyqtSignal()
  updateSignal = QtCore.pyqtSignal()
  
  def __init__(self):
    super(ConfigDialog, self).__init__(None)
    self.setupUi(self)

    # Slot einrichten
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonAbbrechen.clicked.connect(self.onClose)

    if config["DEFAULT"].get("language", "de") == "de":
      self.comboBoxSprache.setCurrentIndex(0)
    elif config["DEFAULT"].get("language", "de") == "en":
      self.comboBoxSprache.setCurrentIndex(1)
    else:
      self.comboBoxSprache.setCurrentIndex(0)

  def onOk(self):
    '''Close the dialog and send the infos to the main window'''
    if self.checkBoxGraphviz.isChecked():
      config["DEFAULT"]["use_graphviz"] = "yes"
      self.activateGraphvizSignal.emit()
    else:
      config["DEFAULT"]["use_graphviz"] = "no"
      self.deactivateGraphvizSignal.emit()

    if config["DEFAULT"]["logfile"] != str(self.lineEditLogDatei.text()):
      config["DEFAULT"]["logfile"] = str(self.lineEditLogDatei.text())
      PyGraphEdit_log.changeFile(config["DEFAULT"]["logfile"])

    if (self.comboBoxSprache.currentIndex() == 0
        and config["DEFAULT"]["language"] != "de"):
      config["DEFAULT"]["language"] = "de"
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Die �nderungen sind nach dem n�chsten Neustart verf�gbar."))
    elif (self.comboBoxSprache.currentIndex() == 1
          and config["DEFAULT"]["language"] != "en"):
      config["DEFAULT"]["language"] = "en"
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Die �nderungen sind nach dem n�chsten Neustart verf�gbar."))
    else:
      config["DEFAULT"]["language"] = "de"

    if self.comboBoxLoglevel.currentIndex() == 0:
      config["DEFAULT"]["loglevel"] = "Info"
      PyGraphEdit_log.setLevel(INFO)
    elif self.comboBoxLoglevel.currentIndex() == 1:
      config["DEFAULT"]["loglevel"] = "Debug"
      PyGraphEdit_log.setLevel(DEBUG)
    elif self.comboBoxLoglevel.currentIndex() == 2:
      config["DEFAULT"]["loglevel"] = "Error"
      PyGraphEdit_log.setLevel(ERROR)
    elif self.comboBoxLoglevel.currentIndex() == 3:
      config["DEFAULT"]["loglevel"] = "Critical"
      PyGraphEdit_log.setLevel(CRITICAL)
    elif self.comboBoxLoglevel.currentIndex() == 4:
      config["DEFAULT"]["loglevel"] = "Warning"
      PyGraphEdit_log.setLevel(WARNING)

    Vertex.vertexSize = int(self.lineEditVertexsize.text())
    Vertex.drawNumber = self.checkVertexNumber.isChecked()
    Edge.edgeWidth = float(self.lineEditEdgeWidth.text())

    Vertex.defaultVertexWeight = float(self.lineEditVertexWeight.text())
    Edge.defaultEdgeWeight = float(self.lineEditEdgeWeight.text())

    Vertex.displayVertexWeight = self.checkBoxKnotengewichte.isChecked()
    Edge.displayEdgeWeight = self.checkBoxKantengewichte.isChecked()

    # Save the current settings
    with open(settingsfile, 'w') as configfile:
      config.write(configfile)

    self.updateSignal.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.close()


# ===========================================================================
class SimBroadcastDialog(QtWidgets.QDialog, Ui_Broadcasting):
  '''Implementation of the broadcasting dialog'''
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self):
    super(SimBroadcastDialog, self).__init__(None)
    self.setupUi(self)

    self.startVertices = []

    # set the slots
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonAbort.clicked.connect(self.onCancel)

    self.checkBoxRandomNeighbor.stateChanged.connect(self.changedRandomNeighbor)

  def changedRandomNeighbor(self):
    if self.checkBoxRandomNeighbor.isChecked():
      self.lineEditProb.setEnabled(False)
    else:
      self.lineEditProb.setEnabled(True)

  def onOk(self):
    '''Save the inputs and change the weights'''
    # Get the start-vertices
    vertices_str = str(self.lineEditStartVertices.text())
    vertices_str = vertices_str.split(",")
    self.startVertices = []
    for v_str in vertices_str:
      v = None
      try:
        if v_str[0] == "[":
          v = int(v_str[1:])
        elif v_str[len(v_str) - 1] == "]":
          v = int(v_str[:len(v_str) - 1])
        else:
          v = int(v_str)
      except:
        pass
      if v:
        self.startVertices.append(v)

    if len(self.startVertices) == 0:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Bitte Startknoten angeben"))
      return

    self.everyVertex = self.checkBoxInfosEveryTime.isChecked()
    try:
      self.steps = int(self.lineEditMaxNumSteps.text())
      if self.steps <= 0:
        raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Unzul�ssige maximale Schrittanzahl"))
      return

    try:
      self.probability = float(self.lineEditProb.text())
      if self.probability > 1.0 or self.probability < 0.0:
        raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Unzul�ssige Wahrscheinlickeit"))
      return

    self.wholeNeighborhood = self.checkBoxAllNeighbors.isChecked()
    self.randomNeighbor = self.checkBoxRandomNeighbor.isChecked()
    self.hiddenNode = self.checkBoxHiddenNode.isChecked()

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ============================================================================
class EdgeWeightsDialog(QtWidgets.QDialog, Ui_EdgeWeights):
  '''Implementation of the edge weights dialog'''
  trigger = QtCore.pyqtSignal()
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()
  
  def __init__(self):
    super(EdgeWeightsDialog, self).__init__(None)
    self.setupUi(self)
    
    # set the slots
    self.pushButtonOK.clicked.connect(self.onOk)
    self.pushButtonAbort.clicked.connect(self.onCancel)

    self.checkBoxAllEdges.clicked.connect(self.changeAllEdges)
    self.checkBoxFixedWeight.clicked.connect(self.changeFixedWeight)
    self.checkBoxEuclidian.clicked.connect(self.changeEuclidian)
    self.checkBoxExponential.clicked.connect(self.changeExponential)
    self.checkBoxLinear.clicked.connect(self.changeLinear)

    if self.checkBoxFixedWeight.isChecked():
      self.changeFixedWeight()

  def changeAllEdges(self):
    '''Clicked on checkbox "Change all edges"'''
    if self.checkBoxAllEdges.isChecked():
      self.lineEditChangeEdges.setEnabled(False)
    else:
      self.lineEditChangeEdges.setEnabled(True)

  def changeFixedWeight(self):
    '''Clicked on checkbox "Fixed weight"'''
    if self.checkBoxFixedWeight.isChecked():
      self.lineEditEdgeWeight.setEnabled(True)
      self.deactivateExp()

  def changeEuclidian(self):
    '''Clicked on checkbox "euclidian weight"'''
    if self.checkBoxEuclidian.isChecked():
      self.lineEditEdgeWeight.setEnabled(False)
      self.deactivateExp()
      self.deactivateLinear()

  def changeExponential(self):
    '''Clicked on checkbox "exponential weight"'''
    if self.checkBoxExponential.isChecked():
      self.lineEditEdgeWeight.setEnabled(False)
      self.deactivateExp(True)
      self.deactivateLinear()

  def deactivateExp(self, flag=False):
    '''Deactivate the text fields of the exponential function'''
    self.lineEditx1.setEnabled(flag)
    self.lineEditValue1.setEnabled(flag)
    self.lineEditx2.setEnabled(flag)
    self.lineEditValue2.setEnabled(flag)

  def changeLinear(self):
    '''Clicked on checkbox "linear weight"'''
    if self.checkBoxLinear.isChecked():
      self.lineEditEdgeWeight.setEnabled(False)
      self.deactivateExp()
      self.deactivateLinear(True)

  def deactivateLinear(self, flag=False):
    '''Deactivate the text fields of the linear function'''
    self.lineEditLinx1.setEnabled(flag)
    self.lineEditLinValue1.setEnabled(flag)
    self.lineEditLinx2.setEnabled(flag)
    self.lineEditLinValue2.setEnabled(flag)

  def onOk(self):
    '''Save the inputs and change the weights'''
    try:
      if not self.checkBoxAllEdges.isChecked():
        self.edges = str(self.lineEditChangeEdges.text())
      self.allEdges = self.checkBoxAllEdges.isChecked()

      if (not self.checkBoxEuclidian.isChecked()
          and not self.checkBoxExponential.isChecked()
          and not self.checkBoxLinear.isChecked()):
        self.weight = str(self.lineEditEdgeWeight.text())
      self.euclidian = self.checkBoxEuclidian.isChecked()
      self.exponential = self.checkBoxExponential.isChecked()
      self.linear = self.checkBoxLinear.isChecked()

      # Save the inputs and check if they valid
      if self.checkBoxExponential.isChecked():
        self.expx1 = float(self.lineEditx1.text())
        self.expValue1 = float(self.lineEditValue1.text())
        self.expx2 = float(self.lineEditx2.text())
        self.expValue2 = float(self.lineEditValue2.text())
        if (self.expx1 == self.expx2 or self.expValue2 == 0.0 or
            self.expValue1 == self.expValue2):
          raise

      elif self.checkBoxLinear.isChecked():
        self.expx1 = float(self.lineEditLinx1.text())
        self.expValue1 = float(self.lineEditLinValue1.text())
        self.expx2 = float(self.lineEditLinx2.text())
        self.expValue2 = float(self.lineEditLinValue2.text())
        if self.expx1 == self.expx2:
          raise
    except:
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit", "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Ung�litge Eingaben"))
      return

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ===========================================================================
class ExtPythonDialog(QtWidgets.QDialog, Ui_ExtPython):
  '''Load a extern python function'''
  ok = QtCore.pyqtSignal()
  cancel = QtCore.pyqtSignal()

  def __init__(self):
    super(ExtPythonDialog, self).__init__(None)
    self.setupUi(self)

    self.module = ""
    self.function = ""

    # set the slots
    self.pushButtonOk.clicked.connect(self.onOk)
    self.pushButtonClose.clicked.connect(self.onCancel)
    self.comboBoxModules.currentIndexChanged.connect(self.aktFunctions)

    self.lineEditFunction.hide()

  def show(self):
    '''Overwrites the show-function'''
    # Get the modules in userfiles
    import pkgutil
    modules = [name for _, name, _ in pkgutil.iter_modules(['userfiles'])]
    self.comboBoxModules.clear()
    for m in modules:
      self.comboBoxModules.addItem(m)
    self.aktFunctions()

    super(ExtPythonDialog, self).show()

  def aktFunctions(self):
    '''Refresh the list of functions'''
    module = self.comboBoxModules.currentText()

    try:
      import importlib
      import imp

      # Generate the function-string; the module must be in the userfiles directory
      function_string = 'userfiles.' + module + '.' + '__all__'
      mod_name, func_name = function_string.rsplit('.', 1)

      # Import the module
      mod = importlib.import_module(mod_name)

      # It is nessesary to reload the module
      imp.reload(mod)

      # Get the attributes and start the module
      func = getattr(mod, func_name)
      self.comboBoxFunctions.clear()
      for m in func:
        self.comboBoxFunctions.addItem(m)
      self.lineEditFunction.hide()
      self.comboBoxFunctions.show()

    except:
      self.comboBoxFunctions.hide()
      self.lineEditFunction.show()

  def onOk(self):
    '''Return the selected values'''
    self.module = str(self.comboBoxModules.currentText())
    if self.comboBoxFunctions.isVisible():
      self.function = str(self.comboBoxFunctions.currentText())
    else:
      self.function = str(self.lineEditFunction.text())

    # send the ok-signal
    self.ok.emit()
    self.close()

  def onCancel(self):
    '''Close the dialog'''
    self.cancel.emit()
    self.close()


# ===========================================================================
class ProductDialog(QtWidgets.QDialog, ProductGraph_Dialog):
  '''Generate the product graph'''
  accept = QtCore.pyqtSignal()

  def __init__(self):
    super(ProductDialog, self).__init__(None)
    self.setupUi(self)

    # Slot einrichten
    self.pushButtonOk.clicked.connect(self.onOk)
    self.pushButtonClose.clicked.connect(self.onClose)

    self.comboBoxFirstGraph.currentIndexChanged.connect(self.__aktFirst__)
    self.comboBoxSecondGraph.currentIndexChanged.connect(self.__aktSecond__)

    self.comboBoxDrawMode.clear()
    for mode in PLOTMODES_GRAPHVIZ:
      self.comboBoxDrawMode.addItem(mode)
    for mode in PLOTMODES_INTERN:
      self.comboBoxDrawMode.addItem(mode)

    self.comboBoxFirstGraph.clear()
    self.comboBoxSecondGraph.clear()
    self.graphs = ['aktueller Graph', 'Weg', 'Kreis', 'Vollst�ndiger Graph']
    for graph in self.graphs:
      self.comboBoxFirstGraph.addItem(graph)
      self.comboBoxSecondGraph.addItem(graph)

    self.comboBoxProduct.clear()
    for product in ['Kartesisches Produkt', 'Tensorprodukt',
                    'Lexicographisches Produkt', 'AND Produkt',
                    'Join', 'Corona']:
      self.comboBoxProduct.addItem(product)

  def __aktFirst__(self):
    '''Upate the text line'''
    if str(self.comboBoxFirstGraph.currentText()) == 'aktueller Graph':
      self.lineEditFirstGraphVertices.setEnabled(False)
      self.lineEditFirstGraphVertices.clear()
      self.lineEditFirstGraphVertices.setReadOnly(True)
    else:
      self.lineEditFirstGraphVertices.setEnabled(True)
      self.lineEditFirstGraphVertices.setReadOnly(False)

  def __aktSecond__(self):
    '''Update the text line'''
    if str(self.comboBoxSecondGraph.currentText()) == 'aktueller Graph':
      self.lineEditSecondGraphVertices.setEnabled(False)
      self.lineEditSecondGraphVertices.clear()
      self.lineEditSecondGraphVertices.setReadOnly(True)
    else:
      self.lineEditSecondGraphVertices.setEnabled(True)
      self.lineEditSecondGraphVertices.setReadOnly(False)

  def markFirst(self):
    '''Mark the lineEditFirstGraphVertices red'''
    palette = self.lineEditFirstGraphVertices.palette()
    palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Text,
                     QtGui.QColor(255, 0, 0))
    #palette.setColor(QPalette.Active, QPalette.Base, QColor(50, 50, 50))
    self.lineEditFirstGraphVertices.setPalette(palette)

  def markSecond(self):
    '''Mark the lineEditSecondGraphVertices red'''
    palette = self.lineEditSecondGraphVertices.palette()
    palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Text,
                     QtGui.QColor(255, 0, 0))
    self.lineEditSecondGraphVertices.setPalette(palette)

  def demarkFirst(self):
    '''Demark the lineEditFirstGraphVertices'''
    palette = self.lineEditFirstGraphVertices.palette()
    palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Text,
                     QtGui.QColor(0, 0, 0))
    self.lineEditFirstGraphVertices.setPalette(palette)

  def demarkSecond(self):
    '''Demark the lineEditSecondGraphVertices'''
    palette = self.lineEditSecondGraphVertices.palette()
    palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Text,
                     QtGui.QColor(0, 0, 0))
    self.lineEditSecondGraphVertices.setPalette(palette)

  def getValues(self):
    '''Returns the choosen values'''
    # [product, graph 1, Number of vertices, graph 2, number of vertices, drawing mode]
    return [str(self.comboBoxProduct.currentText()),
            str(self.comboBoxFirstGraph.currentText()),
            str(self.lineEditFirstGraphVertices.text()),
            str(self.comboBoxSecondGraph.currentText()),
            str(self.lineEditSecondGraphVertices.text()),
            str(self.comboBoxDrawMode.currentText())]

  def onOk(self):
    '''Close the dialog'''
    globalsClass.PLOTSTYLE = str(self.comboBoxDrawMode.currentText())

    self.demarkFirst()
    self.demarkSecond()

    # Test if the values are valid
    g1 = str(self.comboBoxFirstGraph.currentText())
    g2 = str(self.comboBoxSecondGraph.currentText())
    try:
      # Check the first graph
      if (len(self.lineEditFirstGraphVertices.text()) != 0 and
          self.comboBoxFirstGraph.currentText() != 'aktueller Graph'):
        n = int(self.lineEditFirstGraphVertices.text())
      else:
        n = 0
      if n <= 0 and g1 != 'aktueller Graph':
        self.markFirst()

      # Check the second graph
      if (len(self.lineEditSecondGraphVertices.text()) != 0 and
          self.comboBoxSecondGraph.currentText() != 'aktueller Graph'):
        m = int(self.lineEditSecondGraphVertices.text())
      else:
        m = 0
      if m <= 0 and g2 != 'aktueller Graph':
        self.markSecond()

      if (n <= 0 and g1 != 'aktueller Graph' or m <= 0
          and g2 != 'aktueller Graph'):
        raise InputError
    except (InputError, ValueError):
      fedit(None,
            title=QtWidgets.QApplication.translate("PyGraphEdit",
                                                   "Achtung"),
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Keine g�ltige Knotenzahl eingegeben"))

      return

    self.accept.emit()
    self.close()

  def onClose(self):
    '''Close the dialog'''
    self.demarkFirst()
    self.demarkSecond()
    self.close()


# ============================================================================
class ClassInfos(QtWidgets.QDialog, Ui_Classes):
  '''Show infos about the graph classes'''
  def __init__(self):
    super(ClassInfos, self).__init__(None)
    self.setupUi(self)
    self.pushButtonClose.clicked.connect(self.onClose)

  def onClose(self):
    '''Close the dialog'''
    self.close()


# ===========================================================================
class VertexInfo(QtWidgets.QDialog, Ui_Knoteninfo):
  '''Show vertex infos'''
  setElementWeight = QtCore.pyqtSignal()

  def __init__(self):
    super(VertexInfo, self).__init__(None)
    self.setupUi(self)
    self.pushClose.clicked.connect(self.onClose)

  def onClose(self):
    '''Close the dialog'''
    global tmp_weight
    global tmp_element
    tmp_element = int(self.lineEditNummer.text())
    try:
      tmp_weight = float(self.lineEditGewicht.text())
    except ValueError:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Kantengewicht hat falsches Zahlformat!"))
      return
    self.setElementWeight.emit()
    self.close()


# ============================================================================
class EdgeInfo(QtWidgets.QDialog, Ui_Kanteninfo):
  '''Show edge infos'''
  setElementWeight = QtCore.pyqtSignal()

  def __init__(self):
    super(EdgeInfo, self).__init__(None)
    self.setupUi(self)
    self.pushClose.clicked.connect(self.onClose)

  def onClose(self):
    '''Close the dialog'''
    global tmp_weight
    global tmp_element
    tmp_element = (int(self.lineEditStart.text()),
                   int(self.lineEditEnd.text()))
    try:
      tmp_weight = float(self.lineEditGewicht.text())
    except ValueError:
      fedit(None, title="Info",
            comment=QtWidgets.QApplication.translate("PyGraphEdit",
                                                     "Kantengewicht hat falsches Zahlformat!"))
      return
    self.setElementWeight.emit()
    self.close()


# ===========================================================================
class GraphInfos(QtWidgets.QDialog, Ui_Infos):
  '''Show the graph infos'''
  def __init__(self):
    super(GraphInfos, self).__init__(None)
    self.setupUi(self)
    self.pushButtonOK.clicked.connect(self.onClose)

  def onClose(self):
    '''Close the dialog'''
    self.close()


# ============================================================================
class CloseDialog(QtWidgets.QDialog, Ui_Close):
  '''Ask if the graph should be saved before closing PyGraphEdit'''
  def __init__(self):
    super(CloseDialog, self).__init__(None)
    self.setupUi(self)

    self.pushButtonClose.clicked.connect(self.onClose)
    self.pushButtonAbort.clicked.connect(self.onAbort)
    self.pushButtonSaveClose.clicked.connect(self.onSaveClose)

    self.returnValue = 0

  def onClose(self):
    '''Close PyGraphEdit without saving'''
    self.returnValue = "Close"
    self.close()

  def onAbort(self):
    '''Close the dialog'''
    self.returnValue = "Abort"
    self.close()

  def onSaveClose(self):
    '''Save the graph and then exit'''
    self.returnValue = "SaveClose"
    self.close()


# ============================================================================
class Wait(QtWidgets.QDialog, Ui_Wait):
  '''Show the "Please wait"-dialog'''
  stoppcalculation = QtCore.pyqtSignal()

  def __init__(self):
    super(Wait, self).__init__(None)
    self.setupUi(self)

    self.pushButtonStopp.clicked.connect(self.onStopp)

    if system() == "Windows":
      self.pushButtonStopp.setEnabled(False)

  def onStopp(self):
    '''
    Stop the current calculation (only under Linux)
    '''
    self.stoppcalculation.emit()


# ============================================================================
class SolutionDialog(QtWidgets.QDialog, Solution_Info):
  '''Show the result of the calculation'''
  def __init__(self):
    super(SolutionDialog, self).__init__(None)
    self.setupUi(self)

    self.result = None
    self.latex = False

    if globalsClass.USE_MATPLOTLIB:
      self.pushButtonShowPlot.setEnabled(True)
    else:
      self.pushButtonShowPlot.setEnabled(False)

    self.pushButtonOK.clicked.connect(self.onClose)
    self.pushButtonCopy.clicked.connect(self.onCopy)
    self.pushButtonCopyLaTeX.clicked.connect(self.onCopyLatex)
    self.pushButtonShowPlot.clicked.connect(self.showPlot)

    self.pushButtonX1.clicked.connect(self.substxwithOne)
    self.pushButtonXM1.clicked.connect(self.substxwithMinusOne)
    self.pushButtonSub.clicked.connect(self.substVariable)

  def onCopy(self):
    '''Copy the result to the clipboard'''
    if self.latex:
      self.textBrowser.clear()
      self.textBrowser.setText(str(self.result))
      self.latex = False
    self.textBrowser.selectAll()
    self.textBrowser.copy()

  def onCopyLatex(self):
    '''Copy the result in LaTeX-syntax to the clipboard'''
    from sympy import latex
    self.textBrowser.clear()
    self.textBrowser.setText(str(latex(self.result)))
    self.textBrowser.selectAll()
    self.textBrowser.copy()
    self.latex = True

  def showPlot(self):
    '''Show the plot of the reliabilty function'''
    if re.search("p", str(self.result)):
      from sympy.utilities.lambdify import lambdify
      from sympy import symbols
      import numpy as np
      import matplotlib.pyplot as plt
      p = symbols('p')

      evalfunc = lambdify(p, self.result, modules=['numpy'])
      t = np.linspace(0, 1, 100)
      plt.plot(t, evalfunc(t), 'b')
      plt.title(self.label.text())
      plt.xlabel('p')
      plt.show()
    else:
      fedit(None, title="Info",
            comment="Only available for reliability functions!")

  def substVariable(self):
    '''
    Substitute one or more variables in the result polynomial
    Syntax: x=-1 or x=y or x=y;z=x*y**2 or x=y+z
    '''
    from sympy import sympify

    osubststr = self.lineEditSub.text()
    subststr = osubststr.split(";")
    substdict = dict()
    for s in subststr:
      var, sub = s.split("=")
      if not re.search(var, str(self.result)):
        fedit(None, title="Info",
              comment="Can not find variable %s in the polynomial!" % var)
        return
      else:
        substdict[var] = sympify(sub)

    newpoly = self.result.subs(substdict)
    self.textBrowser.append("\nPoly with %s:\n%s" % (osubststr, str(newpoly)))

  def substxwithOne(self):
    '''
    Substitute x with one in the result-polynomial
    '''
    if not re.search("x", str(self.result)):
        fedit(None, title="Info",
              comment="Can not find variable x in the polynomial!")
        return

    from sympy import symbols
    x = symbols('x')
    newpoly = self.result.subs({x: 1})

    self.textBrowser.append("\nPoly with x=1:\n%s" % str(newpoly))

  def substxwithMinusOne(self):
    '''
    Substitute x with -1 in the result-polynomial
    '''
    if not re.search("x", str(self.result)):
        fedit(None, title="Info",
              comment="Can not find variable x in the polynomial!")
        return

    from sympy import symbols
    x = symbols('x')
    newpoly = self.result.subs({x: -1})

    self.textBrowser.append("\nPoly with x=-1:\n%s" % str(newpoly))

  def onClose(self):
    '''Close the dialog'''
    self.textBrowser.clear()
    self.close()

# =====================================================================
class Simulation(QtCore.QThread):
  '''
  Class for the simulation functionality.
  '''
  jumpStep = QtCore.pyqtSignal(int)

  def __init__(self, parent=None):
    QtCore.QThread.__init__(self, parent)

    # Var to break the simulation
    self.simBreak = False

    self.startposition = None
    self.maxvalue = None
    self.simulationTimeStep = None

  def setValues(self, startposition, maxvalue, simulationTimeStep):
    '''
    Set the simulation values.
    @param startposition: Current position in the simulation
    @param maxvalue: Number of steps in the simulation
    @param simulationTimeStep: Length of a step
    '''
    self.startposition = startposition
    self.maxvalue = maxvalue
    self.simulationTimeStep = simulationTimeStep

  def run(self):
    '''Overwrite the running function'''
    self.simBreak = False
    for s in range(self.startposition, self.maxvalue + 1):
      # Emit the signal to the main class
      self.jumpStep.emit(s)

      # Sleep some time
      time.sleep(self.simulationTimeStep)

      # Check if a break is requested
      if self.simBreak:
        self.simBreak = False
        break


# =====================================================================
def get_coords(lines):
  '''
  Read coordinates from lines
  @param lines: Output of Dot.create
  @return: Coordinates, width and height of the embedding
  '''
  coords = {}
  width = None
  height = None
  for line in lines:
    l = line.split(" ")
    if l[0] == "node":
      coords[str(l[1])] = (float(l[2]), float(l[3]))
    elif l[0] == "b'graph":
      width = float(l[2])
      height = float(l[3].strip('\\r'))

  return coords, width, height


# =====================================================================
# =====================================================================
# =====================================================================
# =====================================================================
if __name__ == "__main__":
  multiprocessing.freeze_support()

  parser = argparse.ArgumentParser(description='PyGraphEdit.')
  parser.add_argument('--config', nargs='?',
                      default="settings.ini",
                      help='Config file')
  args = parser.parse_args()

  qtApp = QtWidgets.QApplication(sys.argv)

  # Read the config-file
  config = configparser.ConfigParser()
  if system() == "Windows":
    if Path(args.config).is_file():
      settingsfile = args.config
    else:
      settingsfile = 'settings_win.ini'
  elif system() == "Linux":
    if Path(args.config).is_file():
      settingsfile = args.config
    else:
      settingsfile = 'settings.ini'
  else:
    print("Unknwon operating system! Trying to use the default config file.")
    settingsfile = 'settings.ini'
  config.read(settingsfile)

  # Setup the logging-function
  PyGraphEdit_log = PyGraphEditLog("stream")  # "file", config["DEFAULT"].get("logfile", "PyGraphEdit.log"))
  PyGraphEdit_log.setLevel(INFO)
  PyGraphEdit_log.info("PyGraphEdit v%s - Copyright (c) 2013-2017 Markus Dod" % VERSION)
  PyGraphEdit_log.info("")
  PyGraphEdit_log.info("")
  if config["DEFAULT"].get("loglevel", "Info") == "Info":
    PyGraphEdit_log.setLevel(INFO)
  elif config["DEFAULT"].get("loglevel") == "Error":
    PyGraphEdit_log.setLevel(ERROR)
  elif config["DEFAULT"].get("loglevel") == "Debug":
    PyGraphEdit_log.setLevel(DEBUG)
  elif config["DEFAULT"].get("loglevel") == "Warning":
    PyGraphEdit_log.setLevel(WARNING)
  elif config["DEFAULT"].get("loglevel") == "Critical":
    PyGraphEdit_log.setLevel(CRITICAL)
  PyGraphEdit_log.debug("Detected OS: %s" % system())
  if not Path(args.config).is_file():
    PyGraphEdit_log.warning("Config file '%s' does not exist." % args.config)
  PyGraphEdit_log.debug("Using settings file: %s" % settingsfile)

  # Redirect stdout and stderr to the logging-function
  s1 = StreamToLogger(PyGraphEdit_log, INFO)
  sys.stdout = s1
  s2 = StreamToLogger(PyGraphEdit_log, ERROR)
  sys.stderr = s2

  globalsClass = Globals()

  # Use graphviz?
  globalsClass.USE_GRAPHVIZ = config["DEFAULT"].getboolean("use_graphviz")

  # Use matplotlib?
  globalsClass.USE_MATPLOTLIB = bool(config["DEFAULT"].get("use_matplotlib") == "yes")

  # Use ogdf?
  globalsClass.USE_OGDF = bool(config["DEFAULT"].get("use_ogdf") == "yes")

  # Set the language
  language = config["DEFAULT"].get("language", "de")
  if language == "de":
    pass
  else:
    translator = QtCore.QTranslator()
    translator.load("dialogs/PyGraphEdit_" + str(language) + ".qm")
    qtApp.installTranslator(translator)

  # Create the main window and show it
  graphEdit = PyGraphEdit()
  graphEdit.setWindowTitle("PyGraphEdit v%s" % str(VERSION))
  graphEdit.ui.statusbar.showMessage("Started ... Have fun!", 10000)
  graphEdit.show()
  qtApp.exec()
