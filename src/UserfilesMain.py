#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Script for starting the userfiles-modules without PyGraphEdit

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 26.05.2014

@author: Markus Dod
@license: MIT (see LICENSE.txt)
'''

import platform
import importlib
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL


from SGraph.SGraph import SGraph
from SGraph.GraphFunctions import (completeGraph, randomGraph, grid,  # @UnresolvedImport
                                   torus, path, cycle, kCycle, kPath,  # @UnresolvedImport
                                   completeBipartite)  # @UnresolvedImport
from SGraph.GraphParser import load_graph, load_graphdb  # @UnresolvedImport

from modules.log import PyGraphEditLog  # @UnresolvedImport
from modules.consts import VERSION  # @UnresolvedImport


# ==========================================================================
# ==========================================================================
# ==========================================================================
if __name__ == '__main__':

  MDNetwork_log = PyGraphEditLog()

  # ==========================================================================
  # ======================= BENUTZEREINSTELLUNGEN ============================
  # ==========================================================================

  # Logging-Level festlegen
  MDNetwork_log.setLevel(INFO)          # DEBUG, INFO, WARNING, ERROR, CRITICAL

  # Module
  mod_name = "userfiles.test"

  # Function
  func_name = "AStar"

  # Generate the SGraph
  g = SGraph()
  #g = completeBipartite(4, 4)
  #g = randomGraph(15, 0.2)
  #g = completeGraph(6)
  #g = kPath(4,2)
  #g = grid(4,4)
  #g = torus(4,2)
  #g = cycle(8)
  #g = path(6)
  #g = SGraph([1,2,3,4],[[1,2],[1,3],[2,3],[2,4],[3,4]])


  # Or load the SGraph from a file
  if platform.system() == "Windows":
    datei = "..\\data\\graphs.xml"
  elif platform.system() == "Linux":
    datei = "../data/graphs.xml"

  # Name of the graph in the file
  name = "Bridge"


  # ==========================================================================
  # ===================== ENDE-BENUTZEREINSTELLUNGEN =========================
  # ==========================================================================

  MDNetwork_log.info("PyGraphEdit v%s - Copyright (c) 2013-2017 Markus Dod" % VERSION)
  MDNetwork_log.info("")

  # Load the graph
  g, koords = load_graph(datei, name)

  # Load the module and start it
  mod = importlib.import_module(mod_name)
  func = getattr(mod, func_name)
  result, returntype = func(g)

  # Print the result
  print(result)

  MDNetwork_log.info("")
  MDNetwork_log.info("bye...")
