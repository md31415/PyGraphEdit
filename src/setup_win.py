'''
Script to create the executalbes for Windows.

Copyright (c) 2013-2017 Markus Dod

This file is part of PyGraphEdit.

PyGraphEdit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyGraphEdit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.

Created on 14.01.2013

@author: Markus Dod
@license: GPL (see LICENSE.txt)
'''

import sys
from cx_Freeze import setup, Executable  # @UnresolvedImport

base = 'Win32GUI' if sys.platform=='win32' else None

EXECUTABLES = [Executable(script="PyGraphEdit.py",
                          base=base)] #, compress=True)]
INCLUDESLIST = ["sys", "locale", "os", "subprocess", "tempfile", "platform",
                "PyQt5.QtCore", "PyQt5.QtGui", "PyQt5.QtWidgets",
                "sip", "itertools", "PyQt5.QtPrintSupport", "encodings",
                "xml.dom.minidom", "math", "operator", "functools", "copy",
                "sqlite3", "pydot", "logging", "configparser", "decimal",
                "numpy", "sympy", "matplotlib",
                "matplotlib.backends.backend_qt4agg"]

BUILDOPTIONS = dict(#compressed=True,
                    includes=INCLUDESLIST,
                    include_files=["dialogs/icons", "dialogs/PyGraphEdit_en.qm",
                                   "settings_win.ini"],
                    path=sys.path + ["modules_ext", "modules", "lib", "SGraph"],
                    excludes=['Tkinter', 'scipy'])

setup(name="PyGraphEdit",
      version="0.63",
      author="Markus Dod",
      author_email="mdod@hs-mittweida.de",
      description="Grapheneditor",
      options=dict(build_exe=BUILDOPTIONS),
      executables=EXECUTABLES)
